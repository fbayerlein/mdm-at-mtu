## Short manual for the restructure.sh BASH script.

The restructure.sh script is intended for use in the Eclipse openMDM Working Group to merge multiple Git repositories into one.
See: https://bugs.eclipse.org/bugs/show_bug.cgi?id=561698

### First of all, change the variables
Change the variables inside of restructure.sh if the current values don't fit your needs

* COMMIT_USER for the Git user
* COMMIT_MAIL for the Git email address
* COMMIT_PREFIX for the Prefix used in the merge commit messages
* REMOTE_NAME the name of the git remote for the new repository.
* SUBDIR the sub directory which will be used to clone the necessary git repositories
* NEW_REPO_NAME the name of the new git repository which will only be created on your local maschine
* NEW_REPO_DIR the name of the new git bare repository which will only be created on your local maschine (should end with .git)
* BASE, DEFAULT, ODSADAPTER and NUCLEUS as the used Git repositories
* REPOS as the list of the current Git repositories

### How does the script work?
The script will create a new sub directory (variable SUBDIR) on do all the work inside that directory.

The script creates two local Git repositories, a bare one (will be used as remote) and a normal one (will be used as working copy).

After that, the script will add all defined repositories (variable REPOS) as new remotes to the working copy.
For each added remote the script will detect the exising branches and create temporary mirror branches in the new working copy.

Then every project will be merged into the working copy (with flag --allow-unrelated-histories) and moved into an own project directory. This step will be repeated for every branch detected.

If a branch does not exits in a project, the master branch will be used to create that project directory on that branch.

> Tags must be "recreated" by hand.

### Possible handling of Tags
And the end of the script, all Tags found in the Git repositories will be listed with the corresponding commit hashes.

To create the correct state of a previous Tag take this steps:

1. Go to the new wokring copy directory (e.g. restructuring/org.eclipse.mdm)
2. Checkout the clean branch init/empty
3. Create a new branch to work on the Tag (e.g. tag-work/5.1.0M8)
4. Switch to the branch
5. Merge the first tag form a project by hash (e.g merge --allow-unrelated-histories -m "Some message" 7545d24dae04b733cf34843adaa8754941952e8a)
6. Create a new subfolder with the name of the project (e.g. org.eclipse.mdm.nucleus)
7. Move everything besides the .git and the new subfolder directory into that directory (use git move to keep Git history)
8. Commit the change to that branch and repeat from step 5. for the same Tag from the other projects
   
At the end you can create a new Tag on the current branch (e.g. tag-work/5.1.0M8) and switch back to the init/empty branch (maybe you have to use git reset --hard to get the clean branch back) and repeat the steps for another Tag