#!/bin/bash
#*******************************************************************************
# Copyright (c) 2019-2020 Contributors to the Eclipse Foundation
#
# See the NOTICE file(s) distributed with this work for additional
# information regarding copyright ownership.
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License v. 2.0 which is available a
# thttp://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#*******************************************************************************

# This script is related to the openMDM requirement 560684 - https://bugs.eclipse.org/bugs/show_bug.cgi?id=560684



##### Configuration begin

# COMMIT_USER, COMMIT_MAIL will be used as User and Mail adress for the commits (merge) in the new repository
# COMMIT_PREFIX will be used as part of the commit message
# Please change COMMIT_USER, COMMIT_MAIL and COMMIT_PREFIX if necessary
COMMIT_USER="Simon Skoczylas"
COMMIT_MAIL="simon.skoczylas@karakun.com"
COMMIT_PREFIX="561698 - "

# The name of the git remote for the new repository.
REMOTE_NAME=restructuring
# The sub directory which will be used to clone the necessary git repositories
SUBDIR=restructuring
# The name of the new git repository which will only be created on your local maschine
NEW_REPO_NAME="org.eclipse.mdm"
# The name of the new git bare repository which will only be created on your local maschine (should end with .git)
NEW_REPO_DIR="$NEW_REPO_NAME.git"

PROJECT_ORDER=("org.eclipse.mdm.api.base" "org.eclipse.mdm.api.default" "org.eclipse.mdm.api.odsadapter" "org.eclipse.mdm.nucleus")
TAG_520M1=("5.2.0M1" "63439c6c25d38a7ef59e49175b0268ad130b2a85" "b667b1a5db701d6d136a2eae67027e9d234aa4b6" "6caabd454c817022de33df705c873b14906cd11b" "c7163c74e59bde6e27194cde183f1e7440b61960")
TAG_510=("5.1.0" "4ac0ec4f7269b20f4c4a71cb190b8d3e18857adb" "172df8bf70a2060cf79b6736553e3344aeae3074" "6bb90c7829ee8d07dcca76e2102e3ff568531fa1" "80f4e05ebe1a229e46801a9d05f29b7ec9fe6ad8")
TAG_500=("5.0.0" "9294001445e38f6b63f1ba0d853fdd555a65d832" "e9c319b1917d7d13919375342f646a04129f2dc4" "b1ea88258827bf4fe72b102965dcb9b2fb280b98" "b2c3eec059904212b85385dcc8030ed5b35231a0")

TAGS=(TAG_520M1 TAG_510 TAG_500)

##### Configuration end!

echo "Will work with the following tags:"
for tag in ${TAGS[*]}; do
    temp="$tag[@]"
    list=("${!temp}")
    echo "Tag ${list[0]}"
done

cd $SUBDIR

read -p "Press Enter to continue"

cd $NEW_REPO_NAME
# Set the user and mail for the working repository
git config user.name "\"$COMMIT_USER\""
git config user.email "\"$COMMIT_MAIL\""

echo "Working inside:"
pwd

shopt -s extglob

for tag in ${TAGS[*]}; do
    temp="$tag[@]"
    list=("${!temp}")
    echo "Working on tag ${list[0]}"
    git checkout init/empty
    git checkout -b taggy/${list[0]}
    omit=".git"
    echo
    i=0
    for project in ${PROJECT_ORDER[*]}; do
        i=$((i+1))
        echo "Merge ${list[0]} for ${project} with ${list[i]}"
        # git merge --allow-unrelated-histories -m "Get 5.2.0M1 for org.eclipse.mdm.api.default" b667b1a5db701d6d136a2eae67027e9d234aa4b6
        git merge --signoff -q --allow-unrelated-histories -m "Get ${list[0]} for ${project}" ${list[i]}
        mkdir ${project}
        # git mv !(.git/|org.eclipse.mdm.api.base) org.eclipse.mdm.api.base/
        omit="${omit}|${project}"
        echo "Move ${project} to separate directory"
        git mv !(${omit}) ${project}/
        git mv .gitignore ${project}/
        git commit -s -q -m "Move ${project} to separate directory"
    done
    echo "Create tag ${list[0]}"
    git tag ${list[0]}
    git checkout master
    #git reset
    #git clean -df
    git branch -D taggy/${list[0]}
    echo
done
git checkout master
git branch -D init/empty

shopt -u extglob