/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.testutils;

import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;

import org.opentest4j.TestAbortedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.images.RemoteDockerImage;
import org.testcontainers.utility.DockerImageName;

public abstract class OdsServerContainer extends GenericContainer<OdsServerContainer> {

	private static Logger LOG = LoggerFactory.getLogger(OdsServerContainer.class);

	public OdsServerContainer(RemoteDockerImage image) {
		super(image);
	}

	public OdsServerContainer(DockerImageName dockerImageName) {
		super(dockerImageName);
	}

	public OdsServerContainer(String dockerImageName) {
		super(dockerImageName);
	}

	public abstract Map<String, String> getConnectionParameters();

	public static OdsServerContainer create() {
		ServiceLoader<OdsServerContainer> loader = ServiceLoader.load(OdsServerContainer.class);

		Iterator<OdsServerContainer> it = loader.iterator();

		while (it.hasNext()) {
			OdsServerContainer container = it.next();
			return container;
		}

		LOG.warn("No OdsServerContainer implementation found!");
		return new OdsServerContainer("busybox") {

			@Override
			public Map<String, String> getConnectionParameters() {
				throw new TestAbortedException(
						"Could not find any service implementation for " + OdsServerContainer.class.getName());
			}
		};
	}
}
