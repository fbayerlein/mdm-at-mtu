package org.eclipse.mdm.api.odsadapter.model;

import java.time.Instant;
import java.util.List;

public class TestContextData {

	private String strVal;
	private Instant dateVal;
	private Long longVal;
	private List<String[]> dirAndAoFile;
	private String[] dirAndFile;
	private List<String[]> arrayDirAndFile;

	public TestContextData(String strVal, Instant dateVal, Long longVal, List<String[]> dirAndAoFile,
			String[] dirAndFile, List<String[]> arrayDirAndFile) {
		this.strVal = strVal;
		this.dateVal = dateVal;
		this.longVal = longVal;
		this.dirAndAoFile = dirAndAoFile;
		this.dirAndFile = dirAndFile;
		this.arrayDirAndFile = arrayDirAndFile;
	}

	public String getStrVal() {
		return strVal;
	}

	public Instant getDateVal() {
		return dateVal;
	}

	public Long getLongVal() {
		return longVal;
	}

	public List<String[]> getDirAndAoFile() {
		return dirAndAoFile;
	}

	public String[] getDirAndFile() {
		return dirAndFile;
	}

	public List<String[]> getArrayDirAndFile() {
		return arrayDirAndFile;
	}
}
