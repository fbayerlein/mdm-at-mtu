/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.UUID;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class MoveTestStepTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;

	public static TemplateTest templateTest1;
	public static TemplateTest templateTest2;

	public static TemplateTestStep templateTestStep;

	public static Test mytest_tpl1;
	public static Test anotherTest_tpl1;
	public static Test myTest_tpl2;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		ef.createCatalogAttribute("vin", ValueType.STRING, ccVehicle);

		TemplateRoot trUut = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uut");
		ef.createTemplateComponent("vehicle", trUut, ccVehicle, true);

		templateTestStep = ef.createTemplateTestStep("MyTemplateTestStep");
		templateTestStep.setTemplateRoot(trUut);

		templateTest1 = ef.createTemplateTest("TplTest1");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest1, templateTestStep);

		templateTest2 = ef.createTemplateTest("TplTest2");

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle));
			t.create(Arrays.asList(trUut));
			t.create(Arrays.asList(templateTestStep));
			t.create(Arrays.asList(templateTest1, templateTest2));
			t.commit();
		}

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);

		mytest_tpl1 = ef.createTest("MyTest_tpl1", pool, templateTest1, false);
		anotherTest_tpl1 = ef.createTest("AnotherTest_tpl1", pool, templateTest1, false);
		myTest_tpl2 = ef.createTest("MyTest_tpl2", pool, templateTest2, false);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testMoveTestStepToCorrectTest() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new test...
		TestStep ts_to_move1 = ef.createTestStep("ts_to_move1", mytest_tpl1, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ts_to_move1));
			t.commit();
		}

		// ...and move it to anaother test with the same template
		EntityFactory.getCore(ts_to_move1).getMutableStore().set(anotherTest_tpl1);

		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(ts_to_move1));
			t.commit();
		}

		assertThat(em.loadChildren(mytest_tpl1, TestStep.class)).isEmpty();
		assertThat(em.loadChildren(anotherTest_tpl1, TestStep.class)).extracting(TestStep::getName)
				.containsExactly("ts_to_move1");
		assertThat(em.loadParent(ts_to_move1, Test.class)).map(Test::getName).contains("AnotherTest_tpl1");
	}

	@org.junit.jupiter.api.Test
	public void testMoveTestStepToIncorrectTest() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new test...
		TestStep ts_to_move1 = ef.createTestStep("ts_to_move1", mytest_tpl1, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ts_to_move1));
			t.commit();
		}

		// ...and move it to test with another, incompatible template
		EntityFactory.getCore(ts_to_move1).getMutableStore().set(myTest_tpl2);

		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(ts_to_move1))).isInstanceOf(IllegalStateException.class)
					.hasMessage("Relation 'Test' is incompatible with update statement for entity type 'TestStep'");
		}
	}
}
