/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.eclipse.mdm.api.odsadapter.utils.ValuesFilterUtil.dateMatch;
import static org.eclipse.mdm.api.odsadapter.utils.ValuesFilterUtil.intMatch;
import static org.eclipse.mdm.api.odsadapter.utils.ValuesFilterUtil.stringMatch;

import java.time.Instant;
import java.util.Arrays;
import java.util.BitSet;

import org.eclipse.mdm.api.base.massdata.ChannelValues;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.junit.jupiter.api.Test;

class ValuesFilterUtilTest {

	@Test
	void testMatchInteger() {
		ChannelValues cv = new ChannelValues(null, "MyChannel", null,
				ValueType.INTEGER_SEQUENCE.create("Values", new int[] { 1, 2, 3, 4, 5, 3 }));

		BitSet expected = new BitSet(6);
		expected.set(2);
		expected.set(5);

		assertThat(ValuesFilterUtil.match(cv, ComparisonOperator.EQUAL, Arrays.asList("3"))).isEqualTo(expected);
	}

	@Test
	void testMatchLong() {
		ChannelValues cv = new ChannelValues(null, "MyChannel", null,
				ValueType.LONG_SEQUENCE.create("Values", new long[] { 1, 2, 1, 4, 1 }));

		BitSet expected = new BitSet(5);
		expected.set(0);
		expected.set(2);
		expected.set(4);

		assertThat(ValuesFilterUtil.match(cv, ComparisonOperator.EQUAL, Arrays.asList("1"))).isEqualTo(expected);
	}

	@Test
	void testStringMatch() {
		assertThat(stringMatch(ComparisonOperator.IS_NULL, "test123", Arrays.asList("test123"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.IS_NULL, "test123", Arrays.asList("test123"), false)).isFalse();

		assertThat(stringMatch(ComparisonOperator.IS_NOT_NULL, "test123", Arrays.asList("x"), true)).isFalse();
		assertThat(stringMatch(ComparisonOperator.IS_NOT_NULL, "test123", Arrays.asList("x"), false)).isTrue();

		assertThat(stringMatch(ComparisonOperator.EQUAL, "test123", Arrays.asList("test123"), false)).isTrue();
		assertThat(stringMatch(ComparisonOperator.EQUAL, "test123", Arrays.asList("Test123"), false)).isFalse();

		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_EQUAL, "test123", Arrays.asList("Test123"), false))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_EQUAL, "test123", Arrays.asList("x123"), false))
				.isFalse();

		assertThat(stringMatch(ComparisonOperator.LESS_THAN, "abc", Arrays.asList("def"), false)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LESS_THAN, "abc", Arrays.asList("abc"), false)).isFalse();
		assertThat(stringMatch(ComparisonOperator.LESS_THAN, "abc", Arrays.asList("aaa"), false)).isFalse();

		assertThat(stringMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, "abc", Arrays.asList("def"), false)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, "abc", Arrays.asList("abc"), false)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, "abc", Arrays.asList("aaa"), false)).isFalse();

		// For LIKE and ILIKE, see seperate test

		// TODO add more tests
	}

	@Test
	void testDateMatch() {
		Instant before = Instant.parse("2024-07-01T12:00:00Z");
		Instant now = Instant.parse("2024-08-01T12:00:00Z");
		Instant after = Instant.parse("2024-08-01T18:00:00Z");

		assertThat(dateMatch(ComparisonOperator.IS_NULL, now, new Instant[] {}, true)).isTrue();
		assertThat(dateMatch(ComparisonOperator.IS_NULL, now, new Instant[] {}, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.IS_NOT_NULL, now, new Instant[] {}, true)).isFalse();
		assertThat(dateMatch(ComparisonOperator.IS_NOT_NULL, now, new Instant[] {}, false)).isTrue();

		assertThat(dateMatch(ComparisonOperator.EQUAL, now, new Instant[] { now }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.EQUAL, now, new Instant[] { after }, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.CASE_INSENSITIVE_EQUAL, now, new Instant[] { now }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.CASE_INSENSITIVE_EQUAL, now, new Instant[] { after }, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.LESS_THAN, now, new Instant[] { after }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.LESS_THAN, now, new Instant[] { now }, false)).isFalse();
		assertThat(dateMatch(ComparisonOperator.LESS_THAN, now, new Instant[] { before }, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, now, new Instant[] { after }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, now, new Instant[] { now }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, now, new Instant[] { before }, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.GREATER_THAN, now, new Instant[] { before }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.GREATER_THAN, now, new Instant[] { now }, false)).isFalse();
		assertThat(dateMatch(ComparisonOperator.GREATER_THAN, now, new Instant[] { after }, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.GREATER_THAN_OR_EQUAL, now, new Instant[] { before }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.GREATER_THAN_OR_EQUAL, now, new Instant[] { now }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.GREATER_THAN_OR_EQUAL, now, new Instant[] { after }, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.BETWEEN, now, new Instant[] { before, after }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.BETWEEN, before, new Instant[] { before, after }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.BETWEEN, after, new Instant[] { before, after }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.BETWEEN, now,
				new Instant[] { after, Instant.parse("2024-09-01T17:00:00Z") }, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.IN_SET, now, new Instant[] { now }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.IN_SET, now, new Instant[] { before, now, after }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.IN_SET, now, new Instant[] { before, after }, false)).isFalse();
		assertThat(dateMatch(ComparisonOperator.IN_SET, now, new Instant[] { Instant.parse("2024-06-01T12:00:00Z") },
				false)).isFalse();
		assertThat(dateMatch(ComparisonOperator.IN_SET, now, new Instant[] {}, false)).isFalse();

		assertThat(dateMatch(ComparisonOperator.NOT_IN_SET, now, new Instant[] { now }, false)).isFalse();
		assertThat(dateMatch(ComparisonOperator.NOT_IN_SET, now, new Instant[] { before, now, after }, false))
				.isFalse();
		assertThat(dateMatch(ComparisonOperator.NOT_IN_SET, now, new Instant[] { before, after }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.NOT_IN_SET, now,
				new Instant[] { Instant.parse("2024-06-01T12:00:00Z") }, false)).isTrue();
		assertThat(dateMatch(ComparisonOperator.NOT_IN_SET, now, new Instant[] {}, false)).isTrue();

		assertThatThrownBy(() -> dateMatch(ComparisonOperator.LIKE, now, new Instant[] { now }, false))
				.hasMessage("ComparisonOperator lk not supported for valuetype DATE_SEQUENCE");

		assertThatThrownBy(() -> dateMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, now, new Instant[] { now }, false))
				.hasMessage("ComparisonOperator ci_lk not supported for valuetype DATE_SEQUENCE");

		assertThatThrownBy(() -> dateMatch(ComparisonOperator.NOT_LIKE, now, new Instant[] { now }, false))
				.hasMessage("ComparisonOperator not_lk not supported for valuetype DATE_SEQUENCE");

		assertThatThrownBy(
				() -> dateMatch(ComparisonOperator.CASE_INSENSITIVE_NOT_LIKE, now, new Instant[] { now }, false))
						.hasMessage("ComparisonOperator ci_not_lk not supported for valuetype DATE_SEQUENCE");

		// TODO add more tests
	}

	@Test
	void testIntMatch() {
		assertThat(intMatch(ComparisonOperator.IS_NULL, 3, new int[] {}, true)).isTrue();
		assertThat(intMatch(ComparisonOperator.IS_NULL, 3, new int[] {}, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.IS_NOT_NULL, 3, new int[] {}, true)).isFalse();
		assertThat(intMatch(ComparisonOperator.IS_NOT_NULL, 3, new int[] {}, false)).isTrue();

		assertThat(intMatch(ComparisonOperator.EQUAL, 3, new int[] { 3 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.EQUAL, 3, new int[] { 4 }, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.CASE_INSENSITIVE_EQUAL, 3, new int[] { 3 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.CASE_INSENSITIVE_EQUAL, 3, new int[] { 4 }, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.LESS_THAN, 3, new int[] { 4 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.LESS_THAN, 3, new int[] { 3 }, false)).isFalse();
		assertThat(intMatch(ComparisonOperator.LESS_THAN, 3, new int[] { 2 }, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, 3, new int[] { 4 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, 3, new int[] { 3 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.LESS_THAN_OR_EQUAL, 3, new int[] { 2 }, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.GREATER_THAN, 3, new int[] { 2 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.GREATER_THAN, 3, new int[] { 3 }, false)).isFalse();
		assertThat(intMatch(ComparisonOperator.GREATER_THAN, 3, new int[] { 4 }, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.GREATER_THAN_OR_EQUAL, 3, new int[] { 2 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.GREATER_THAN_OR_EQUAL, 3, new int[] { 3 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.GREATER_THAN_OR_EQUAL, 3, new int[] { 4 }, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.BETWEEN, 3, new int[] { 2, 4 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.BETWEEN, 2, new int[] { 2, 4 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.BETWEEN, 4, new int[] { 2, 4 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.BETWEEN, 3, new int[] { 4, 10 }, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.IN_SET, 3, new int[] { 3 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.IN_SET, 3, new int[] { 2, 3, 4 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.IN_SET, 3, new int[] { 2, 4 }, false)).isFalse();
		assertThat(intMatch(ComparisonOperator.IN_SET, 3, new int[] { 1 }, false)).isFalse();
		assertThat(intMatch(ComparisonOperator.IN_SET, 3, new int[] {}, false)).isFalse();

		assertThat(intMatch(ComparisonOperator.NOT_IN_SET, 3, new int[] { 3 }, false)).isFalse();
		assertThat(intMatch(ComparisonOperator.NOT_IN_SET, 3, new int[] { 2, 3, 4 }, false)).isFalse();
		assertThat(intMatch(ComparisonOperator.NOT_IN_SET, 3, new int[] { 2, 4 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.NOT_IN_SET, 3, new int[] { 1 }, false)).isTrue();
		assertThat(intMatch(ComparisonOperator.NOT_IN_SET, 3, new int[] {}, false)).isTrue();

		assertThatThrownBy(() -> intMatch(ComparisonOperator.LIKE, 3, new int[] { 3 }, false))
				.hasMessage("ComparisonOperator lk not supported for valuetype INTEGER_SEQUENCE");

		assertThatThrownBy(() -> intMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, 3, new int[] { 3 }, false))
				.hasMessage("ComparisonOperator ci_lk not supported for valuetype INTEGER_SEQUENCE");

		assertThatThrownBy(() -> intMatch(ComparisonOperator.NOT_LIKE, 3, new int[] { 3 }, false))
				.hasMessage("ComparisonOperator not_lk not supported for valuetype INTEGER_SEQUENCE");

		assertThatThrownBy(() -> intMatch(ComparisonOperator.CASE_INSENSITIVE_NOT_LIKE, 3, new int[] { 3 }, false))
				.hasMessage("ComparisonOperator ci_not_lk not supported for valuetype INTEGER_SEQUENCE");

		// TODO add more tests
	}

	@Test
	void testContainsIgnoreCase() {
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_IN_SET, "aBc", Arrays.asList("abc", "def"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_IN_SET, "dEF", Arrays.asList("abc", "def"), true))
				.isTrue();
	}

	@Test
	void testLike() {

		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("abc*"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("abc*f"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("abc*d"), true)).isFalse();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("abc*g"), true)).isFalse();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("a*d*f"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("d*a"), true)).isFalse();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("*def"), true)).isTrue();

		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("ab?def"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("?b?d?f"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("abc?"), true)).isFalse();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("?def"), true)).isFalse();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("c?adef"), true)).isFalse();

		assertThat(stringMatch(ComparisonOperator.LIKE, "abc*def", Arrays.asList("abc\\*def"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abc*def", Arrays.asList("abcdef"), true)).isFalse();

		assertThat(stringMatch(ComparisonOperator.LIKE, "abc?def", Arrays.asList("abc\\?def"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abc?def", Arrays.asList("abcdef"), true)).isFalse();

		assertThat(stringMatch(ComparisonOperator.LIKE, "abc*def", Arrays.asList("abc?def"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abc?def", Arrays.asList("abc*def"), true)).isTrue();

		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("a?cd*"), true)).isTrue();
		assertThat(stringMatch(ComparisonOperator.LIKE, "abcdef", Arrays.asList("a*e?"), true)).isTrue();
	}

	@Test
	void testIlike() {
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("abc*"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("abc*f"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("abc*d"), true))
				.isFalse();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("abc*g"), true))
				.isFalse();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("a*d*f"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("d*a"), true))
				.isFalse();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("*def"), true))
				.isTrue();

		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("ab?def"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("?b?d?f"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("abc?"), true))
				.isFalse();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("?def"), true))
				.isFalse();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("c?adef"), true))
				.isFalse();

		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABC*DEF", Arrays.asList("abc\\*def"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABC*DEF", Arrays.asList("abcdef"), true))
				.isFalse();

		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABC?DEF", Arrays.asList("abc\\?def"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABC?DEF", Arrays.asList("abcdef"), true))
				.isFalse();

		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABC*DEF", Arrays.asList("abc?def"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABC?DEF", Arrays.asList("abc*def"), true))
				.isTrue();

		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("a?cd*"), true))
				.isTrue();
		assertThat(stringMatch(ComparisonOperator.CASE_INSENSITIVE_LIKE, "ABCDEF", Arrays.asList("a*e?"), true))
				.isTrue();
	}

}
