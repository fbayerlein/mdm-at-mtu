/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assumptions;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.ExternalComponentData;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ExtCompFile;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.TypeSpecification;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class MeasurementTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;
	public static TestStep testStep;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory entityFactory = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Project project = entityFactory.createProject("MyProject");
		Pool pool = entityFactory.createPool("MyPool", project);
		Test test = entityFactory.createTest("MyTest", pool);
		testStep = entityFactory.createTestStep("MyTestStep", test);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void storeMeasurementExtCompFile() throws DataAccessException, IOException {
		EntityFactory entityFactory = context.getEntityFactory().get();
		EntityManager entityManager = context.getEntityManager().get();

		String time = LocalDateTime.now().toString();

		Measurement measurement = entityFactory.createMeasurement("MyMeasurement_" + time, testStep);
		ChannelGroup channelGroup = entityFactory.createChannelGroup("MyChannelGroup", 10, measurement);

		Quantity quantity = entityFactory.createQuantity("MyQuantity_" + time,
				entityManager.loadAll(Unit.class, Arrays.asList("m")).get(0));

		Channel channel = entityFactory.createChannel("MyChannel", measurement, quantity);

		float[] floatValues = new float[] { 0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f };
		short[] flags = new short[] { 15, 15, 15, 15, 0, 15, 15, 15, 15, 7 };

		ByteBuffer buffer = ByteBuffer.allocate(60).order(ByteOrder.LITTLE_ENDIAN);
		buffer.position(0);
		buffer.asFloatBuffer().put(floatValues);

		buffer.position(40);
		buffer.asShortBuffer().put(flags);
		buffer.rewind();

		FileLink fileLink = FileLink.newLocal(new ByteArrayInputStream(buffer.array()), "extComp.btf", 60,
				new MimeType("application/octet-stream"), "", null, FileServiceType.AOFILE);

		ExtCompFile extCompFile = entityFactory.createExtCompFile();
		extCompFile.setName("MyExtCompFile");
		extCompFile.setFileLink(fileLink);

		ExternalComponentData extComp = new ExternalComponentData();
		extComp.setTypeSpecification(TypeSpecification.FLOAT);
		extComp.setStartOffset(0L);
		extComp.setLength(10);
		extComp.setBlocksize(40);
		extComp.setValuesPerBlock(10);
		extComp.setValueOffset(0);
		extComp.setFlagsStartOffset(40L);
		extComp.setValuesExtCompFile(extCompFile);
		extComp.setFlagsExtCompFile(extCompFile);

		WriteRequest wr = WriteRequest.create(channelGroup, channel, AxisType.Y_AXIS).explicitExternal()
				.externalComponent(ScalarType.FLOAT, extComp).build();

		try (Transaction t = entityManager.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(extCompFile));
			t.upload(Arrays.asList(extCompFile), null);
			t.create(Arrays.asList(measurement));

			t.writeMeasuredValues(Arrays.asList(wr));

			t.commit();
		}

		try {
			List<MeasuredValues> measuredValues = entityManager
					.readMeasuredValues(ReadRequest.create(channelGroup).allChannels().allValues());

			assertThat(measuredValues).usingRecursiveFieldByFieldElementComparator()
					.contains(ScalarType.FLOAT.createMeasuredValues("MyChannel", "m", SequenceRepresentation.EXPLICIT,
							new double[0], false, AxisType.Y_AXIS, floatValues, flags));
		} finally {
			try (Transaction t = entityManager.startTransaction()) {
				t.delete(Arrays.asList(measurement));
				t.delete(Arrays.asList(extCompFile));
				t.delete(Arrays.asList(quantity));

				t.commit();
			}
		}
	}

	@org.junit.jupiter.api.Test
	public void storeMeasurementExtCompUrl() throws DataAccessException, IOException {
		Assumptions.assumeThat(odsServer.getConnectionParameters().get("url")).isNull();

		EntityFactory entityFactory = context.getEntityFactory().get();
		EntityManager entityManager = context.getEntityManager().get();

		String time = LocalDateTime.now().toString();

		Measurement measurement = entityFactory.createMeasurement("MyMeasurement_" + time, testStep);
		ChannelGroup channelGroup = entityFactory.createChannelGroup("MyChannelGroup", 10, measurement);

		Quantity quantity = entityFactory.createQuantity("MyQuantity_" + time,
				entityManager.loadAll(Unit.class, Arrays.asList("m")).get(0));

		Channel channel = entityFactory.createChannel("MyChannel", measurement, quantity);

		float[] floatValues = new float[] { 0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f };
		short[] flags = new short[] { 15, 15, 15, 15, 0, 15, 15, 15, 15, 7 };

		ByteBuffer buffer = ByteBuffer.allocate(60).order(ByteOrder.LITTLE_ENDIAN);
		buffer.position(0);
		buffer.asFloatBuffer().put(floatValues);

		buffer.position(40);
		buffer.asShortBuffer().put(flags);
		buffer.rewind();

		FileLink fileLink = FileLink.newLocal(new ByteArrayInputStream(buffer.array()), "extComp.btf", 60,
				new MimeType("application/octet-stream"), "", null, FileServiceType.AOFILE);

		ExternalComponentData extComp = new ExternalComponentData();
		extComp.setTypeSpecification(TypeSpecification.FLOAT);
		extComp.setStartOffset(0L);
		extComp.setLength(10);
		extComp.setBlocksize(40);
		extComp.setValuesPerBlock(10);
		extComp.setValueOffset(0);
		extComp.setFlagsStartOffset(40L);
		extComp.setFileLink(fileLink);
		extComp.setFlagsFileLink(fileLink);

		WriteRequest wr = WriteRequest.create(channelGroup, channel, AxisType.Y_AXIS).explicitExternal()
				.externalComponent(ScalarType.FLOAT, extComp).build();

		try (Transaction t = entityManager.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.getFileService(FileServiceType.EXTREF).uploadParallel(null, Arrays.asList(fileLink), t, null);
			t.create(Arrays.asList(measurement));

			t.writeMeasuredValues(Arrays.asList(wr));

			t.commit();
		}

		try {
			List<MeasuredValues> measuredValues = entityManager
					.readMeasuredValues(ReadRequest.create(channelGroup).allChannels().allValues());

			assertThat(measuredValues).usingRecursiveFieldByFieldElementComparator()
					.contains(ScalarType.FLOAT.createMeasuredValues("MyChannel", "m", SequenceRepresentation.EXPLICIT,
							new double[0], false, AxisType.Y_AXIS, floatValues, flags));
		} finally {
			try (Transaction t = entityManager.startTransaction()) {
				t.delete(Arrays.asList(measurement));
				t.delete(Arrays.asList(quantity));

				t.commit();
			}
		}
	}

}
