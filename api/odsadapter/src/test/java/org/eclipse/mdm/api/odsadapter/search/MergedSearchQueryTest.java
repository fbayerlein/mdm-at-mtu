/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.api.odsadapter.search;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.junit.Ignore;
import org.mockito.ArgumentMatchers;

@Ignore
public class MergedSearchQueryTest {

	private static final String ID_NAME = "Id";

	private EntityType fooEntityType;
	private EntityType barEntityType;

	private Attribute idAttribute;
	private Attribute aAttribute;
	private List<Attribute> attributes;
	private ODSModelManager mockedODSModelManager;

	@org.junit.Before
	public void setup() {
		idAttribute = mock(Attribute.class);
		doReturn(ID_NAME).when(idAttribute).getName();

		Relation fooParentRelation = mock(Relation.class);
		doReturn(ContextType.UNITUNDERTEST.typeName()).when(fooParentRelation).getName();
		fooEntityType = mock(EntityType.class, "Foo");
		doReturn(Collections.singletonList(fooParentRelation)).when(fooEntityType).getParentRelations();
		doReturn(idAttribute).when(fooEntityType).getIDAttribute();

		Relation barParentRelation = mock(Relation.class);
		doReturn(ContextType.UNITUNDERTEST.typeName()).when(barParentRelation).getName();
		barEntityType = mock(EntityType.class, "Bar");
		doReturn(Collections.singletonList(barParentRelation)).when(barEntityType).getParentRelations();
		doReturn(idAttribute).when(barEntityType).getIDAttribute();

		aAttribute = mock(Attribute.class);
		doReturn("a").when(aAttribute).getName();
		doReturn(barEntityType).when(aAttribute).getEntityType();
		attributes = Collections.singletonList(aAttribute);

		mockedODSModelManager = mock(ODSModelManager.class);
		EntityConfig<?> entityConfig = mock(EntityConfig.class);
		doReturn(entityConfig).when(mockedODSModelManager).getEntityConfig(ArgumentMatchers.<EntityConfig.Key<?>>any());

	}

	@org.junit.Test
	public void justByResult() {

		List<Result> byResult = Arrays.asList(createResult("a", "0", "a"), createResult("a", "1", "a"),
				createResult("a", "2", "a"));
		List<Result> byOrder = Collections.emptyList();

		MergedSearchQuery searchQuery = new MergedSearchQuery(fooEntityType,
				createSearchQueryFactory(byResult, byOrder));

		Filter mockedFilter = mock(Filter.class);
		doReturn(null).when(mockedFilter).getContext();

		List<Result> results = searchQuery.fetch(attributes, mockedFilter);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertContainsValue(results, 3, "a", null);
	}

	@org.junit.Test
	public void justByResultMeasured() {

		List<Result> byResult = Arrays.asList(createResult("a", "0", "a"), createResult("a", "1", "a"),
				createResult("a", "2", "a"));
		List<Result> byOrder = Collections.emptyList();

		MergedSearchQuery searchQuery = new MergedSearchQuery(fooEntityType,
				createSearchQueryFactory(byResult, byOrder));

		Filter mockedFilter = mock(Filter.class);
		doReturn(ContextState.MEASURED).when(mockedFilter).getContext();

		List<Result> results = searchQuery.fetch(attributes, mockedFilter);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertContainsValue(results, 3, "a", null);
	}

	@org.junit.Test
	public void justByResultOrdered() {

		List<Result> byResult = Arrays.asList(createResult("a", "0", "a"), createResult("a", "1", "b"),
				createResult("a", "2", "a"));
		List<Result> byOrder = Collections.emptyList();

		MergedSearchQuery searchQuery = new MergedSearchQuery(fooEntityType,
				createSearchQueryFactory(byResult, byOrder));

		Filter mockedFilter = mock(Filter.class);
		doReturn(ContextState.ORDERED).when(mockedFilter).getContext();

		List<Result> results = searchQuery.fetch(attributes, mockedFilter);
		assertNotNull(results);
		assertEquals(0, results.size());

	}

	@org.junit.Test
	public void justByOrder() {

		List<Result> byResult = Collections.emptyList();

		List<Result> byOrder = Arrays.asList(createResult("a", "0", "a"), createResult("a", "1", "c"),
				createResult("a", "2", "a"));

		MergedSearchQuery searchQuery = new MergedSearchQuery(fooEntityType,
				createSearchQueryFactory(byResult, byOrder));

		Filter mockedFilter = mock(Filter.class);
		doReturn(null).when(mockedFilter).getContext();

		List<Result> results = searchQuery.fetch(attributes, mockedFilter);
		assertNotNull(results);
		assertEquals(3, results.size());

		assertThat((String) results.get(0).getValue(attributes.get(0)).extract(ContextState.ORDERED)).isEqualTo("a");
		assertThat((String) results.get(0).getValue(attributes.get(0)).extract(ContextState.MEASURED)).isNull();

		assertThat((String) results.get(1).getValue(attributes.get(0)).extract(ContextState.ORDERED)).isEqualTo("c");
		assertThat((String) results.get(1).getValue(attributes.get(0)).extract(ContextState.MEASURED)).isNull();

		assertThat((String) results.get(2).getValue(attributes.get(0)).extract(ContextState.ORDERED)).isEqualTo("a");
		assertThat((String) results.get(2).getValue(attributes.get(0)).extract(ContextState.MEASURED)).isNull();
	}

	@org.junit.Test
	public void bothMeasured() {

		List<Result> byResult = Arrays.asList(createResult("a", "0", "a"), createResult("a", "1", "a"),
				createResult("a", "2", "a"));

		List<Result> byOrder = Arrays.asList(createResult("a", "0", "b"), createResult("a", "1", "b"),
				createResult("a", "2", "b"));

		MergedSearchQuery searchQuery = new MergedSearchQuery(fooEntityType,
				createSearchQueryFactory(byResult, byOrder));

		Filter mockedFilter = mock(Filter.class);
		doReturn(ContextState.MEASURED).when(mockedFilter).getContext();

		List<Result> results = searchQuery.fetch(attributes, mockedFilter);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertContainsValue(results, 3, "a", "b");
	}

	@org.junit.Test
	public void bothOrdered() {

		List<Result> byResult = Arrays.asList(createResult("a", "0", "a"), createResult("a", "1", "a"),
				createResult("a", "2", "a"));

		List<Result> byOrder = Arrays.asList(createResult("a", "0", "b"), createResult("a", "1", "b"),
				createResult("a", "2", "b"));

		MergedSearchQuery searchQuery = new MergedSearchQuery(fooEntityType,
				createSearchQueryFactory(byResult, byOrder));

		Filter mockedFilter = mock(Filter.class);
		doReturn(ContextState.ORDERED).when(mockedFilter).getContext();

		List<Result> results = searchQuery.fetch(attributes, mockedFilter);
		assertNotNull(results);
		assertEquals(3, results.size());
		assertContainsValue(results, 3, "a", "b");
	}

	private void assertContainsValue(List<Result> results, int size, String measured, String ordered) {
		List<Value> values = results.stream().map(records -> records.getValue(aAttribute)).filter(v -> {
			String currentMeasured = v.extract(ContextState.MEASURED);
			String currentOrdered = v.extract(ContextState.ORDERED);

			boolean matchMeasured = (measured == null && currentMeasured == null) || (measured.equals(currentMeasured));
			boolean matchOrdered = (ordered == null && currentOrdered == null) || (ordered.equals(currentOrdered));
			return matchMeasured && matchOrdered;
		}).collect(Collectors.toList());
		assertNotNull(values);
		assertFalse(values.isEmpty());
		assertEquals(size, values.size());
	}

	private Result createResult(String name, String id, String testValue) {
		Result result = new Result();

		Record barRecord = createRecord(barEntityType, name, testValue);
		result.addRecord(barRecord);

		Record fooRecord = createRecord(fooEntityType, ID_NAME, id);
		result.addRecord(fooRecord);

		return result;
	}

	private Record createRecord(EntityType entityType, String name, String value) {

		Record record = new Record(entityType);
		Value val = ValueType.STRING.create(name, value);
		record.addValue(val);
		return record;
	}

	private Function<ContextState, BaseEntitySearchQuery> createSearchQueryFactory(List<Result> byResult,
			List<Result> byOrder) {
		return contextState -> {
			if (ContextState.ORDERED.equals(contextState)) {
				return new MockedSearchQuery(mockedODSModelManager, byOrder);
			} else {
				return new MockedSearchQuery(mockedODSModelManager, byResult);
			}
		};
	}

	private static class MockedSearchQuery extends BaseEntitySearchQuery {

		List<Result> results;

		public MockedSearchQuery(ODSModelManager odsModelManager, List<Result> results) {
			super(odsModelManager, null, null, null);
			this.results = results;
		}

		@Override
		public List<Value> getFilterValues(final Attribute attribute) throws DataAccessException {
			return Collections.emptyList();
		}

		@Override
		public List<Result> fetchComplete(final List<EntityType> entityTypes, final Filter filter, int resultOffset,
				int resultLimit) throws DataAccessException {
			return results;
		}

		@Override
		public List<Result> fetch(final List<Attribute> attributes, final Filter filter, int resultOffset,
				int resultLimit) throws DataAccessException {
			return results;
		}
	}
}
