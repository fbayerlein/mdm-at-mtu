/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.updatestatement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ChangeTemplateOfTestStepObligatoryTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;

	public static TemplateTest templateTest;

	public static TemplateTestStep templateTestStepCar0;
	public static TemplateTestStep templateTestStepCar1;
	public static TemplateTestStep templateTestStepCar2;
	public static TemplateTestStep templateTestStepCar3;
	public static TemplateTestStep templateTestStepCar4;

	public static Test myTest;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		CatalogComponent ccWheel = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "wheel");
		CatalogAttribute caSize = ef.createCatalogAttribute("size", ValueType.INTEGER, ccWheel);
		CatalogAttribute caColor = ef.createCatalogAttribute("color", ValueType.STRING, ccWheel);
		CatalogAttribute caAttr1 = ef.createCatalogAttribute("attr1", ValueType.STRING, ccWheel);

		TemplateComponent comp;
		TemplateAttribute compAttr;

		TemplateRoot trUutCar0 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar0");
		ef.createTemplateComponent("wheel", trUutCar0, ccWheel, true);

		TemplateRoot trUutCar1 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar1");
		comp = ef.createTemplateComponent("wheel", trUutCar1, ccWheel, false);
		compAttr = ef.createTemplateAttribute(caSize, comp);
		compAttr.setOptional(false);
		compAttr.getValue(TemplateAttribute.ATTR_DEFAULT_VALUE).set("1");
		compAttr = ef.createTemplateAttribute(caColor, comp);
		compAttr = ef.createTemplateAttribute(caAttr1, comp);

		TemplateRoot trUutCar2 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar2");
		comp = ef.createTemplateComponent("wheel", trUutCar2, ccWheel, false);
		compAttr = ef.createTemplateAttribute(caSize, comp);
		compAttr.setOptional(false);
		compAttr.getValue(TemplateAttribute.ATTR_DEFAULT_VALUE).set("2");
		compAttr = ef.createTemplateAttribute(caColor, comp);
		compAttr.setOptional(false);
		compAttr.getValue(TemplateAttribute.ATTR_DEFAULT_VALUE).set("blue");

		TemplateRoot trUutCar3 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar3");
		comp = ef.createTemplateComponent("wheel", trUutCar3, ccWheel, false);
		compAttr = ef.createTemplateAttribute(caSize, comp);
		compAttr.setOptional(false);
		compAttr.getValue(TemplateAttribute.ATTR_DEFAULT_VALUE).set("3");
		compAttr = ef.createTemplateAttribute(caColor, comp);
		compAttr.setOptional(false);
		compAttr.getValue(TemplateAttribute.ATTR_DEFAULT_VALUE).set("red");
		compAttr = ef.createTemplateAttribute(caAttr1, comp);
		compAttr.setOptional(false);
		compAttr.getValue(TemplateAttribute.ATTR_DEFAULT_VALUE).set("fish");

		TemplateRoot trUutCar4 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar4");
		comp = ef.createTemplateComponent("wheel", trUutCar4, ccWheel, false);
		compAttr = ef.createTemplateAttribute(caSize, comp);
		compAttr.setOptional(false);
		compAttr.getValue(TemplateAttribute.ATTR_DEFAULT_VALUE).set("4");

		templateTestStepCar0 = ef.createTemplateTestStep("TplCar0");
		templateTestStepCar0.setTemplateRoot(trUutCar0);

		templateTestStepCar1 = ef.createTemplateTestStep("TplCar1");
		templateTestStepCar1.setTemplateRoot(trUutCar1);

		templateTestStepCar2 = ef.createTemplateTestStep("TplCar2");
		templateTestStepCar2.setTemplateRoot(trUutCar2);

		templateTestStepCar3 = ef.createTemplateTestStep("TplCar3");
		templateTestStepCar3.setTemplateRoot(trUutCar3);

		templateTestStepCar4 = ef.createTemplateTestStep("TplCar4");
		templateTestStepCar4.setTemplateRoot(trUutCar4);

		templateTest = ef.createTemplateTest("TplTest");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar0);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar1);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar2);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar3);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar4);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccWheel));
			t.create(Arrays.asList(trUutCar0, trUutCar1, trUutCar2, trUutCar3, trUutCar4));
			t.create(Arrays.asList(templateTestStepCar0, templateTestStepCar1, templateTestStepCar2,
					templateTestStepCar3, templateTestStepCar4));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);

		myTest = ef.createTest("MyTest", pool, templateTest, false);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateObligatoryToOptional() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		TestStep testStep = ef.createTestStep("testSetTemplateObligatoryToOptional", myTest, templateTestStepCar1);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar0);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar0");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
			assertThat(uutContextRoot.getContextComponent("wheel")).isPresent();
			ContextComponent wheelComp = uutContextRoot.getContextComponent("wheel").get();
			assertThat(wheelComp.getValue("size").extract().equals(1)).isTrue();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateOptionalToObligatory() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		TestStep testStep = ef.createTestStep("testSetTemplateOptionalToObligatory", myTest, templateTestStepCar0);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar1);
		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"Optional attribute size at TplUnitUnderTestComp:wheel can not be changed to obligatory.");
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateMoreObligatory() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		TestStep testStep = ef.createTestStep("testSetTemplateMoreObligatory", myTest, templateTestStepCar4);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar4");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
			assertThat(uutContextRoot.getContextComponent("wheel")).isPresent();
			ContextComponent wheelComp = uutContextRoot.getContextComponent("wheel").get();
			assertThat(wheelComp.getValue("size").extract().equals(4)).isTrue();
			assertThat(wheelComp.getValue("color", true)).isNull();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar2);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar2");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
			assertThat(uutContextRoot.getContextComponent("wheel")).isPresent();
			ContextComponent wheelComp = uutContextRoot.getContextComponent("wheel").get();
			assertThat(wheelComp.getValue("size").extract().equals(4)).isTrue();
			assertThat(wheelComp.getValue("color").extract().equals("blue")).isTrue();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateMoreObligatory2() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		TestStep testStep = ef.createTestStep("testSetTemplateMoreObligatory2", myTest, templateTestStepCar4);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar4");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
			assertThat(uutContextRoot.getContextComponent("wheel")).isPresent();
			ContextComponent wheelComp = uutContextRoot.getContextComponent("wheel").get();
			assertThat(wheelComp.getValue("size").extract().equals(4)).isTrue();
			assertThat(wheelComp.getValue("color", true)).isNull();
			assertThat(wheelComp.getValue("attr1", true)).isNull();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar2);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar2");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
			assertThat(uutContextRoot.getContextComponent("wheel")).isPresent();
			ContextComponent wheelComp = uutContextRoot.getContextComponent("wheel").get();
			assertThat(wheelComp.getValue("size").extract().equals(4)).isTrue();
			assertThat(wheelComp.getValue("color").extract().equals("blue")).isTrue();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar3);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar3");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
			assertThat(uutContextRoot.getContextComponent("wheel")).isPresent();
			ContextComponent wheelComp = uutContextRoot.getContextComponent("wheel").get();
			assertThat(wheelComp.getValue("size").extract().equals(4)).isTrue();
			assertThat(wheelComp.getValue("color").extract().equals("blue")).isTrue();
			assertThat(wheelComp.getValue("attr1").extract().equals("fish")).isTrue();
		}
	}
}
