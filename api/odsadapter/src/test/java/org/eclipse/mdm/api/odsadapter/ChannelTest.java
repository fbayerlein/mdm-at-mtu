/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInfo;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ChannelTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;

	public static Quantity quantity;
	public static TestStep testStep;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		quantity = ef.createQuantity("MyQuantity");

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		testStep = ef.createTestStep("MyTestStep", test);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project));
			t.commit();
		}
	}

	/**
	 * Tests that updates to Channel name also update the corresponding LocalColumn
	 * name and thus the MeasuredValues can still be read.
	 */
	@org.junit.jupiter.api.Test
	public void testRenameColumn(TestInfo testInfo) {
		EntityManager em = context.getEntityManager().get();
		EntityFactory ef = context.getEntityFactory().get();

		Measurement measurement = ef.createMeasurement(testInfo.getDisplayName(), testStep);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);
		Channel channel = ef.createChannel("MyChannel1", measurement, quantity);

		WriteRequest writeRequest = WriteRequest.create(channelGroup, channel, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 1.1f, 42.0f, 1f }, new boolean[] { true, true, true }).build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(writeRequest));
			t.commit();
		}
		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).allChannels().allValues()))
				.extracting(MeasuredValues::getName).contains("MyChannel1");

		Channel toRename = em.load(Channel.class, channel.getID());
		toRename.setName("Channel1_Renamed");

		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(toRename));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).allChannels().allValues()))
				.extracting(MeasuredValues::getName).contains("Channel1_Renamed");
	}

	/**
	 * Renames Channels from different ChannelGroups including the independent
	 * Channel. For the independent Channel, both associated LocalColumn names have
	 * to be updated.
	 */
	@org.junit.jupiter.api.Test
	public void testRenameMultipleColumnsAccrossGroups(TestInfo testInfo) {
		EntityManager em = context.getEntityManager().get();
		EntityFactory ef = context.getEntityFactory().get();

		Measurement measurement = ef.createMeasurement(testInfo.getDisplayName(), testStep);
		ChannelGroup channelGroup1 = ef.createChannelGroup("MyChannelGroup1", 3, measurement);
		ChannelGroup channelGroup2 = ef.createChannelGroup("MyChannelGroup2", 5, measurement);

		Channel time = ef.createChannel("Time", measurement, quantity);
		Channel channel1_1 = ef.createChannel("MyChannel1_1", measurement, quantity);
		Channel channel2_1 = ef.createChannel("MyChannel2_1", measurement, quantity);
		Channel channel2_2 = ef.createChannel("MyChannel2_2", measurement, quantity);

		WriteRequest writeRequestTime1 = WriteRequest.create(channelGroup1, time, AxisType.X_AXIS).explicit()
				.integerValues(new int[] { 2, 4, 6 }, new boolean[] { true, true, true }).independent().build();
		WriteRequest writeRequest1_1 = WriteRequest.create(channelGroup1, channel1_1, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 1.1f, 42.0f, 1f }, new boolean[] { true, true, true }).build();

		WriteRequest writeRequestTime2 = WriteRequest.create(channelGroup2, time, AxisType.X_AXIS).explicit()
				.integerValues(new int[] { 1, 2, 3, 4, 5 }, new boolean[] { true, true, true, true, true })
				.independent().build();
		WriteRequest writeRequest2_1 = WriteRequest.create(channelGroup2, channel2_1, AxisType.Y_AXIS).explicit()
				.doubleValues(new double[] { 9.9, 8.8, 7.7, 6.6, 5.5 }, new boolean[] { true, true, true, true, true })
				.build();
		WriteRequest writeRequest2_2 = WriteRequest.create(channelGroup2, channel2_2, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 21, 346, 2, 90, 234 }, new boolean[] { true, true, true, true, true })
				.build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(writeRequestTime1, writeRequest1_1, writeRequestTime2, writeRequest2_1,
					writeRequest2_2));
			t.commit();
		}

		// Check if Channel names were saved correctly
		assertThat(em.load(Channel.class,
				Arrays.asList(time.getID(), channel1_1.getID(), channel2_1.getID(), channel2_2.getID())))
						.extracting(Channel::getName)
						.containsExactlyInAnyOrder("Time", "MyChannel1_1", "MyChannel2_1", "MyChannel2_2");

		// Check if LocalColumn names and values were saved correctly
		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup1).allChannels().allValues()))
				.usingRecursiveFieldByFieldElementComparator().containsExactlyInAnyOrder(
						ScalarType.INTEGER.createMeasuredValues("Time", "", SequenceRepresentation.EXPLICIT_EXTERNAL,
								new double[0], true, AxisType.X_AXIS, writeRequestTime1.getValues(),
								writeRequestTime1.getFlags()),
						ScalarType.FLOAT.createMeasuredValues("MyChannel1_1", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], false, AxisType.Y_AXIS,
								writeRequest1_1.getValues(), writeRequest1_1.getFlags()));

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup2).allChannels().allValues()))
				.usingRecursiveFieldByFieldElementComparator().containsExactlyInAnyOrder(
						ScalarType.INTEGER.createMeasuredValues("Time", "", SequenceRepresentation.EXPLICIT_EXTERNAL,
								new double[0], true, AxisType.X_AXIS, writeRequestTime2.getValues(),
								writeRequestTime2.getFlags()),
						ScalarType.DOUBLE.createMeasuredValues("MyChannel2_1", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], false, AxisType.Y_AXIS,
								writeRequest2_1.getValues(), writeRequest2_1.getFlags()),
						ScalarType.INTEGER.createMeasuredValues("MyChannel2_2", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], false, AxisType.Y_AXIS,
								writeRequest2_2.getValues(), writeRequest2_2.getFlags()));

		// Renaming independent channel "Time" -> "TimeOffset"
		// Renaming "MyChannel1_1"->"Channel1_1_Renamed" and
		// "MyChannel2_2"->"Channel2_2_Renamed"
		Channel renamedTime = em.load(Channel.class, time.getID());
		renamedTime.setName("TimeOffset");

		Channel renamed1_1 = em.load(Channel.class, channel1_1.getID());
		renamed1_1.setName("Channel1_1_Renamed");

		Channel renamed2_2 = em.load(Channel.class, channel2_2.getID());
		renamed2_2.setName("Channel2_2_Renamed");

		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(renamedTime, renamed1_1, renamed2_2));
			t.commit();
		}

		// Check if Channel names changed
		assertThat(em.load(Channel.class,
				Arrays.asList(time.getID(), channel1_1.getID(), channel2_1.getID(), channel2_2.getID())))
						.extracting(Channel::getName).containsExactlyInAnyOrder("TimeOffset", "Channel1_1_Renamed",
								"MyChannel2_1", "Channel2_2_Renamed");

		// Check if LocalColumn names changed and values are loaded
		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup1).allChannels().allValues()))
				.usingRecursiveFieldByFieldElementComparator().containsExactlyInAnyOrder(
						ScalarType.INTEGER.createMeasuredValues("TimeOffset", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], true, AxisType.X_AXIS,
								writeRequestTime1.getValues(), writeRequestTime1.getFlags()),
						ScalarType.FLOAT.createMeasuredValues("Channel1_1_Renamed", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], false, AxisType.Y_AXIS,
								writeRequest1_1.getValues(), writeRequest1_1.getFlags()));

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup2).allChannels().allValues()))
				.usingRecursiveFieldByFieldElementComparator().containsExactlyInAnyOrder(
						ScalarType.INTEGER.createMeasuredValues("TimeOffset", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], true, AxisType.X_AXIS,
								writeRequestTime2.getValues(), writeRequestTime2.getFlags()),
						ScalarType.DOUBLE.createMeasuredValues("MyChannel2_1", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], false, AxisType.Y_AXIS,
								writeRequest2_1.getValues(), writeRequest2_1.getFlags()),
						ScalarType.INTEGER.createMeasuredValues("Channel2_2_Renamed", "",
								SequenceRepresentation.EXPLICIT_EXTERNAL, new double[0], false, AxisType.Y_AXIS,
								writeRequest2_2.getValues(), writeRequest2_2.getFlags()));
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}
}
