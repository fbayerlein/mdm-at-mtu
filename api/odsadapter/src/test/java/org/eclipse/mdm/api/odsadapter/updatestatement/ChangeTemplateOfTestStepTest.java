/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.updatestatement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ChangeTemplateOfTestStepTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;

	public static TemplateTest templateTest;

	public static TemplateTestStep templateTestStepFull;
	public static TemplateTestStep templateTestStepVehicleOnly;
	public static TemplateTestStep templateTestStepVehicleOneAttr;
	public static TemplateTestStep templateTestStepFullLooseEnd;
	public static TemplateTestStep templateTestStepOther;
	public static TemplateTestStep templateTestStepVehicleOtherAttr;

	public static Test myTest;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		CatalogAttribute caVin = ef.createCatalogAttribute("vin", ValueType.STRING, ccVehicle);
		CatalogAttribute caModel = ef.createCatalogAttribute("model", ValueType.STRING, ccVehicle);

		CatalogComponent ccEngine = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "engine");
		ef.createCatalogAttribute("power", ValueType.INTEGER, ccEngine);

		CatalogComponent ccOther = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "other");
		ef.createCatalogAttribute("my_name", ValueType.INTEGER, ccOther);

		TemplateRoot trUutFull = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutFull");
		ef.createTemplateComponent("uutFullVehicle", trUutFull, ccVehicle, true);
		ef.createTemplateComponent("uutFullEngine", trUutFull, ccEngine, true);

		TemplateRoot trUutVehicleOnly = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutVehicleOnly");
		ef.createTemplateComponent("uutVehicleOnlyVehicle", trUutVehicleOnly, ccVehicle, true);

		TemplateRoot trUutVehicleOneAttr = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutVehicleOneAttr");
		TemplateComponent tplCompVehicleOneAttr = ef.createTemplateComponent("uutVehicleOneAttrVehicle",
				trUutVehicleOneAttr, ccVehicle, false);
		ef.createTemplateAttribute(caVin, tplCompVehicleOneAttr);

		TemplateRoot trUutVehicleOtherAttr = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutVehicleOtherAttr");
		TemplateComponent tplCompVehicleOtherAttr = ef.createTemplateComponent("uutVehicleOtherAttrVehicle",
				trUutVehicleOtherAttr, ccVehicle, false);
		ef.createTemplateAttribute(caModel, tplCompVehicleOtherAttr);

		TemplateRoot trUutFullLooseEnd = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutFullLooseEnd");
		ef.createTemplateComponent("uutFullLooseEndVehicle", trUutFullLooseEnd, ccVehicle, true);
		ef.createTemplateComponent("uutFullLooseEndEngine", trUutFullLooseEnd, ccEngine, true);

		TemplateRoot trUutOther = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutOther");
		ef.createTemplateComponent("uutOtherOther", trUutOther, ccOther, true);

		templateTestStepFull = ef.createTemplateTestStep("TplFull");
		templateTestStepFull.setTemplateRoot(trUutFull);

		templateTestStepVehicleOnly = ef.createTemplateTestStep("TplVehicleOnly");
		templateTestStepVehicleOnly.setTemplateRoot(trUutVehicleOnly);

		templateTestStepVehicleOneAttr = ef.createTemplateTestStep("TplVehicleOneAttr");
		templateTestStepVehicleOneAttr.setTemplateRoot(trUutVehicleOneAttr);

		templateTestStepFullLooseEnd = ef.createTemplateTestStep("TplFullLooseEnd");
		templateTestStepFullLooseEnd.setTemplateRoot(trUutFullLooseEnd);

		templateTestStepOther = ef.createTemplateTestStep("TplOther");
		templateTestStepOther.setTemplateRoot(trUutOther);

		templateTestStepVehicleOtherAttr = ef.createTemplateTestStep("TplVehicleOtherAttr");
		templateTestStepVehicleOtherAttr.setTemplateRoot(trUutVehicleOtherAttr);

		templateTest = ef.createTemplateTest("TplTest");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepFull);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepVehicleOnly);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepVehicleOneAttr);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepOther);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepVehicleOtherAttr);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle, ccEngine, ccOther));
			t.create(Arrays.asList(trUutFull, trUutVehicleOnly, trUutVehicleOneAttr, trUutFullLooseEnd, trUutOther,
					trUutVehicleOtherAttr));
			t.create(Arrays.asList(templateTestStepFull, templateTestStepVehicleOnly, templateTestStepVehicleOneAttr,
					templateTestStepFullLooseEnd, templateTestStepOther, templateTestStepVehicleOtherAttr));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);

		myTest = ef.createTest("MyTest", pool, templateTest, false);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateEqual() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep ts_full = ef.createTestStep("testSetTemplateEqual", myTest, templateTestStepFull);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ts_full));
			t.commit();
		}

		EntityFactory.getCore(ts_full).getMutableStore().set(templateTestStepFull);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(ts_full));
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateNotInUsagesFails() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		TestStep testStep = ef.createTestStep("testSetTemplateNotRelatedWithTest", myTest, templateTestStepFull);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepFullLooseEnd);
		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage("TplTestStep:TplFullLooseEnd is not compatible with parent template TplTest:TplTest");
			t.commit();
		}
	}

	// OK template changes: VehicleOneAttr -> VehicleOnly -> Full
	// Invalid template changes: Full -> VehicleOnly -> VehicleOneAttr

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithMoreAttributes() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep ts_full = ef.createTestStep("testSetTemplateWithMoreAttributes", myTest,
				templateTestStepVehicleOneAttr);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ts_full));
			t.commit();
		}

		{
			final TestStep testStep_loaded = em.load(TestStep.class, ts_full.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplVehicleOneAttr");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot.getContextComponents().size()).isEqualTo(1);
			Optional<ContextComponent> engineContextComponent = uutContextRoot
					.getContextComponent("uutVehicleOneAttrVehicle");
			assertThat(engineContextComponent.isPresent()).isTrue();
			final Map<String, Value> attributes = engineContextComponent.get().getValues();
			assertThat(attributes.containsKey("vin")).isTrue();
			assertThat(attributes.containsKey("model")).isFalse();
		}

		EntityFactory.getCore(ts_full).getMutableStore().set(templateTestStepVehicleOnly);

		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(ts_full));
			t.commit();
		}

		{
			final TestStep testStep_loaded = em.load(TestStep.class, ts_full.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplVehicleOnly");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot.getContextComponents().size()).isEqualTo(1);
			Optional<ContextComponent> engineContextComponent = uutContextRoot
					.getContextComponent("uutVehicleOnlyVehicle");
			assertThat(engineContextComponent.isPresent()).isTrue();
			final Map<String, Value> attributes = engineContextComponent.get().getValues();
			assertThat(attributes.containsKey("vin")).isTrue();
			assertThat(attributes.containsKey("model")).isTrue();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithMoreComponents() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template VehicleOnly
		TestStep testStep = ef.createTestStep("testSetTemplateWithMoreComponents", myTest, templateTestStepVehicleOnly);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplVehicleOnly");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot.getContextComponents().size()).isEqualTo(1);
			Optional<ContextComponent> vehicleContextComponent = uutContextRoot
					.getContextComponent("uutVehicleOnlyVehicle");
			assertThat(vehicleContextComponent.isPresent()).isTrue();
			final Map<String, Value> vehicleAttributes = vehicleContextComponent.get().getValues();
			assertThat(vehicleAttributes.containsKey("vin")).isTrue();
			assertThat(vehicleAttributes.containsKey("model")).isTrue();
			assertThat(uutContextRoot.getContextComponent("engine").isPresent()).isFalse();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepFull);

		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}

		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplFull");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot.getContextComponents().size()).isEqualTo(2);
			Optional<ContextComponent> vehicleContextComponent = uutContextRoot.getContextComponent("uutFullVehicle");
			assertThat(vehicleContextComponent.isPresent()).isTrue();
			final Map<String, Value> vehicleAttributes = vehicleContextComponent.get().getValues();
			assertThat(vehicleAttributes.containsKey("vin")).isTrue();
			assertThat(vehicleAttributes.containsKey("model")).isTrue();
			assertThat(uutContextRoot.getContextComponent("uutFullEngine").isPresent()).isTrue();
			Optional<ContextComponent> engineContextComponent = uutContextRoot.getContextComponent("uutFullEngine");
			assertThat(engineContextComponent.isPresent()).isTrue();
			final Map<String, Value> engineAttributes = engineContextComponent.get().getValues();
			assertThat(engineAttributes.containsKey("power")).isTrue();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithMoreAttributesAndComponents() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep ts_full = ef.createTestStep("testSetTemplateWithMoreComponents", myTest,
				templateTestStepVehicleOneAttr);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ts_full));
			t.commit();
		}

		EntityFactory.getCore(ts_full).getMutableStore().set(templateTestStepFull);

		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(ts_full));
			t.commit();
		}

		assertThat(em.load(TestStep.class, ts_full.getID())).extracting(ts -> TemplateTestStep.of(ts).get())
				.extracting(TemplateTestStep::getName).isEqualTo("TplFull");
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithLessAttributes() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template VehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateWithLessAttributes", myTest, templateTestStepVehicleOnly);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepVehicleOneAttr);

		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"TplUnitUnderTestComp:uutVehicleOneAttrVehicle does not have the attribute 'model' that is available in TplUnitUnderTestComp:uutVehicleOnlyVehicle");
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithLessComponents() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template Full
		TestStep testStep = ef.createTestStep("testSetTemplateWithLessComponents", myTest, templateTestStepFull);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepVehicleOnly);

		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"There is no matching TemplateComponent for TplUnitUnderTestComp:uutFullEngine pointing to CatUnitUnderTestComp:engine");
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithLessAttributesAndComponents() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template Full
		TestStep testStep = ef.createTestStep("testSetTemplateWithLessAttributesAndComponents", myTest,
				templateTestStepFull);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepVehicleOneAttr);

		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"TplUnitUnderTestComp:uutVehicleOneAttrVehicle does not have the attribute 'model' that is available in TplUnitUnderTestComp:uutFullVehicle");
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithOtherComponents() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateWithOtherComponents", myTest, templateTestStepFull);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepOther);
		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"There is no matching TemplateComponent for TplUnitUnderTestComp:uutFullVehicle pointing to CatUnitUnderTestComp:vehicle");
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateWithOtherAttributes() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateWithOtherAttributes", myTest,
				templateTestStepVehicleOneAttr);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepVehicleOtherAttr);

		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"TplUnitUnderTestComp:uutVehicleOtherAttrVehicle does not have the attribute 'vin' that is available in TplUnitUnderTestComp:uutVehicleOneAttrVehicle");
			t.commit();
		}
	}
}
