/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;

import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.assertj.core.util.DoubleComparator;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.PhysicalDimension;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class UnitConversionTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;
	public static TestStep testStep;
	public static Quantity quantity;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		quantity = ef.createQuantity("MyQuantity");

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		testStep = ef.createTestStep("MyTest", test);

		Unit unitKmperH = em.loadAll(Unit.class, "km/h").get(0);
		// there is a wrong factor configured in the default model
		unitKmperH.setFactor(0.277777777777778);

		// add a unit with both non trivial factor and offset values
		Unit gradF = ef.createUnit("gradF", em.loadAll(PhysicalDimension.class, "temperature").get(0));
		gradF.setFactor(1.8);
		gradF.setOffset(32.0);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity, gradF));
			t.update(Arrays.asList(unitKmperH));
			t.create(Arrays.asList(project));
			t.commit();
		}
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSimpleUnitConversion() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitKg = em.loadAll(Unit.class, "kg").get(0);
		Unit unitG = em.loadAll(Unit.class, "g").get(0);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		Channel channel1 = ef.createChannel("mass", measurement, quantity, unitKg);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		// Write initial values
		WriteRequest writeRequest = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 1.1f, 42.0f, 1f }, new boolean[] { true, true, true }).sourceUnit(unitKg)
				.build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(writeRequest));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitKg).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS, new float[] { 1.1f, 42.0f, 1f }, validFlags(3)));

		// Append in matching unit
		WriteRequest appendRequestInMatchingUnit = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS)
				.explicit().floatValues(new float[] { 3.3f, 0.003f }, new boolean[] { true, true }).sourceUnit(unitKg)
				.build();

		try (Transaction t = em.startTransaction()) {
			t.appendMeasuredValues(Arrays.asList(appendRequestInMatchingUnit));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitKg).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS, new float[] { 1.1f, 42.0f, 1f, 3.3f, 0.003f },
						validFlags(5)));

		// Append in different unit
		WriteRequest appendRequestWithUnitConversion = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS)
				.explicit().floatValues(new float[] { 250f, 1234.5f }).sourceUnit(unitG).build();

		try (Transaction t = em.startTransaction()) {
			t.appendMeasuredValues(Arrays.asList(appendRequestWithUnitConversion));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitKg).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS,
						new float[] { 1.1f, 42.0f, 1f, 3.3f, 0.003f, 0.25f, 1.2345f }, validFlags(7)));
	}

	@org.junit.jupiter.api.Test
	public void testUnitConversionOnInitialWrite() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitKg = em.loadAll(Unit.class, "kg").get(0);
		Unit unitG = em.loadAll(Unit.class, "g").get(0);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		Channel channel1 = ef.createChannel("mass", measurement, quantity, unitKg);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		// Write initial values in other unit
		WriteRequest writeRequest = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 1125f, 42.2f, 375f }, new boolean[] { true, true, true }).sourceUnit(unitG)
				.build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(writeRequest));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitKg).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS, new float[] { 1.125f, 0.0422f, 0.375f }, validFlags(3)));

	}

	@org.junit.jupiter.api.Test
	public void testAppendMultipleChannelsVariableLength() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitKg = em.loadAll(Unit.class, "kg").get(0);

		Unit unitS = em.loadAll(Unit.class, "s").get(0);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		Channel chMass = ef.createChannel("mass", measurement, quantity, unitKg);
		Channel chTime = ef.createChannel("time", measurement, quantity, unitS);

		// Write initial values
		WriteRequest valuesMass1 = WriteRequest.create(channelGroup, chMass, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 3.3f, 0.001f, 0.123f }, new boolean[] { true, true, true })
				.sourceUnit(unitKg).build();
		WriteRequest valuesTime1 = WriteRequest.create(channelGroup, chTime, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 1, 2, 3 }, new boolean[] { true, true, true }).sourceUnit(unitS).build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(valuesMass1, valuesTime1));
			t.commit();
		}

		assertThat(em.readMeasuredValues(
				ReadRequest.create(channelGroup).channel(chMass, unitKg).channel(chTime, unitS).allValues()))
						.usingRecursiveFieldByFieldElementComparator().contains(
								ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS, new float[] { 3.3f, 0.001f, 0.123f },
										validFlags(3)),
								ScalarType.INTEGER.createMeasuredValues("time", "s", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS, new int[] { 1, 2, 3 }, validFlags(3)));

		// Append values; chTime has only 2 values; chMass has 3 values
		WriteRequest valuesMass2 = WriteRequest.create(channelGroup, chMass, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 1.1f, 42.0f, 1f }, new boolean[] { true, true, true }).sourceUnit(unitKg)
				.build();
		WriteRequest valuesTime2 = WriteRequest.create(channelGroup, chTime, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 2, 1 }, new boolean[] { true, true }).sourceUnit(unitS).build();

		try (Transaction t = em.startTransaction()) {
			t.appendMeasuredValues(Arrays.asList(valuesMass2, valuesTime2));
			t.commit();
		}

		assertThat(em.readMeasuredValues(
				ReadRequest.create(channelGroup).channel(chMass, unitKg).channel(chTime, unitS).allValues()))
						.usingRecursiveFieldByFieldElementComparator()
						.contains(ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS,
								new float[] { 3.3f, 0.001f, 0.123f, 1.1f, 42.0f, 1f }, validFlags(6)))
						.contains(ScalarType.INTEGER.createMeasuredValues("time", "s", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new int[] { 1, 2, 3, 2, 1, 0 },
								new short[] { (short) 15, (short) 15, (short) 15, (short) 15, (short) 15, (short) 0 }));
	}

	@org.junit.jupiter.api.Test
	public void testAppendMultipleChannelsUnitConversion() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitKg = em.loadAll(Unit.class, "kg").get(0);
		Unit unitG = em.loadAll(Unit.class, "g").get(0);

		Unit unitS = em.loadAll(Unit.class, "s").get(0);
		Unit unitH = em.loadAll(Unit.class, "h").get(0);

		Unit unitMperS = em.loadAll(Unit.class, "m/s").get(0);
		Unit unitKmperH = em.loadAll(Unit.class, "km/h").get(0);

		Unit unitK = em.loadAll(Unit.class, "K").get(0);
		Unit unitGradC = em.loadAll(Unit.class, "gradC").get(0);

		Unit unitGrad = em.loadAll(Unit.class, "grad").get(0);
		Unit unitRad = em.loadAll(Unit.class, "rad").get(0);

		Unit unitW = em.loadAll(Unit.class, "W").get(0);
		Unit unitPS = em.loadAll(Unit.class, "PS").get(0);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		Channel chVelocity = ef.createChannel("velocity", measurement, quantity, unitMperS);
		Channel chMass = ef.createChannel("mass", measurement, quantity, unitKg);
		Channel chTemperature = ef.createChannel("temperature", measurement, quantity, unitK);
		Channel chTime = ef.createChannel("time", measurement, quantity, unitS);
		Channel chPlaneAngle = ef.createChannel("plane_angle", measurement, quantity, unitGrad);
		Channel chOutput = ef.createChannel("output", measurement, quantity, unitW);

		// Write initial values
		WriteRequest valuesVelocity1 = WriteRequest.create(channelGroup, chVelocity, AxisType.Y_AXIS).explicit()
				.doubleValues(new double[] { 1.1, -2.2, 3.3 }, new boolean[] { true, true, true }).sourceUnit(unitMperS)
				.build();
		WriteRequest valuesMass1 = WriteRequest.create(channelGroup, chMass, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { -3.3f, 0.001f, 0.123f }, new boolean[] { true, true, true })
				.sourceUnit(unitKg).build();
		WriteRequest valuesTemperature1 = WriteRequest.create(channelGroup, chTemperature, AxisType.Y_AXIS).explicit()
				.longValues(new long[] { 1, 273, 300 }, new boolean[] { true, true, true }).sourceUnit(unitK).build();
		WriteRequest valuesTime1 = WriteRequest.create(channelGroup, chTime, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 1, 60, -120 }, new boolean[] { true, true, true }).sourceUnit(unitS).build();
		WriteRequest valuesOutput1 = WriteRequest.create(channelGroup, chOutput, AxisType.Y_AXIS).explicit()
				.shortValues(new short[] { 1, -2, 3 }, new boolean[] { true, true, true }).sourceUnit(unitW).build();
		WriteRequest valuesPlaneAngle1 = WriteRequest.create(channelGroup, chPlaneAngle, AxisType.Y_AXIS).explicit()
				.byteValues(new byte[] { 90, -45, 120 }, new boolean[] { true, true, true }).sourceUnit(unitGrad)
				.build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(valuesMass1, valuesTime1, valuesVelocity1, valuesTemperature1,
					valuesOutput1, valuesPlaneAngle1));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).allChannels().allValues()))
				.usingRecursiveFieldByFieldElementComparator().contains(
						ScalarType.DOUBLE.createMeasuredValues("velocity", "m/s", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new double[] { 1.1, -2.2, 3.3 }, validFlags(3)),
						ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new float[] { -3.3f, 0.001f, 0.123f },
								validFlags(3)),
						ScalarType.LONG.createMeasuredValues("temperature", "K", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new long[] { 1, 273, 300 }, validFlags(3)),
						ScalarType.INTEGER.createMeasuredValues("time", "s", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new int[] { 1, 60, -120 }, validFlags(3)),
						ScalarType.SHORT.createMeasuredValues("output", "W", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new short[] { 1, -2, 3 }, validFlags(3)),
						ScalarType.BYTE.createMeasuredValues("plane_angle", "grad", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new byte[] { 90, -45, 120 }, validFlags(3)));

		// Append in different unit
		WriteRequest valuesVelocity2 = WriteRequest.create(channelGroup, chVelocity, AxisType.Y_AXIS).explicit()
				.doubleValues(new double[] { 9.9, 1.1 }, new boolean[] { true, true }).sourceUnit(unitKmperH).build();
		WriteRequest valuesMass2 = WriteRequest.create(channelGroup, chMass, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 1624.9f, -450.1f }, new boolean[] { true, true }).sourceUnit(unitG).build();
		WriteRequest valuesTemperature2 = WriteRequest.create(channelGroup, chTemperature, AxisType.Y_AXIS).explicit()
				.longValues(new long[] { -20, 23 }, new boolean[] { true, true }).sourceUnit(unitGradC).build();
		WriteRequest valuesTime2 = WriteRequest.create(channelGroup, chTime, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 24, 1 }, new boolean[] { true, true }).sourceUnit(unitH).build();
		WriteRequest valuesOutput2 = WriteRequest.create(channelGroup, chOutput, AxisType.Y_AXIS).explicit()
				.shortValues(new short[] { 3, -1 }, new boolean[] { true, true }).sourceUnit(unitPS).build();
		WriteRequest valuesPlaneAngle2 = WriteRequest.create(channelGroup, chPlaneAngle, AxisType.Y_AXIS).explicit()
				.byteValues(new byte[] { -2, 1 }, new boolean[] { true, true }).sourceUnit(unitRad).build();

		try (Transaction t = em.startTransaction()) {
			t.appendMeasuredValues(Arrays.asList(valuesVelocity2, valuesMass2, valuesTemperature2, valuesTime2,
					valuesOutput2, valuesPlaneAngle2));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).allChannels().allValues()))
				.usingRecursiveFieldByFieldElementComparator(RecursiveComparisonConfiguration.builder()
						.withComparatorForType(new DoubleComparator(1E-6), Double.class).build())
				.contains(
						ScalarType.DOUBLE.createMeasuredValues("velocity", "m/s", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new double[] { 1.1, -2.2, 3.3, 2.75, 0.3055556 },
								validFlags(5)),
						ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS,
								new float[] { -3.3f, 0.001f, 0.123f, 1.6249f, -0.4501f }, validFlags(5)),
						ScalarType.LONG.createMeasuredValues("temperature", "K", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new long[] { 1, 273, 300, 253, 296 },
								validFlags(5)),
						ScalarType.INTEGER.createMeasuredValues("time", "s", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new int[] { 1, 60, -120, 86400, 3600 },
								validFlags(5)),
						ScalarType.SHORT.createMeasuredValues("output", "W", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new short[] { 1, -2, 3, 2206, -735 },
								validFlags(5)),
						ScalarType.BYTE.createMeasuredValues("plane_angle", "grad", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new byte[] { 90, -45, 120, -114, 57 },
								validFlags(5)));
	}

	@org.junit.jupiter.api.Test
	public void testTryToConvertStringChannel() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitS = em.loadAll(Unit.class, "s").get(0);
		Unit unitH = em.loadAll(Unit.class, "h").get(0);

		Quantity q = ef.createQuantity("StringQuantity", unitS);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		Channel channel1 = ef.createChannel("time", measurement, q, unitS);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		// Write initial values (builder pattern prevents to specify unit on
		// WriteRequests with String values
		WriteRequest writeRequest = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS).explicit()
				.stringValues(new String[] { "a", "b", "c" }, new boolean[] { true, true, true }).build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(q));
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(writeRequest));
			t.commit();
		}

		// Reading string values in the same unit is ok, since no unit conversion has to
		// be applied
		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitS).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.STRING.createMeasuredValues("time", "s", SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS, new String[] { "a", "b", "c" }, validFlags(3)));

		// Reading string values in a different unit throws exception, since string
		// values cannot be converted to another unit
		assertThatThrownBy(
				() -> em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitH).allValues()))
						.hasMessage("Cannot convert Channel of datatype STRING from unit 's' to unit 'h'.");

		// Null means persisted unit
		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, null).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.STRING.createMeasuredValues("time", "s", SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS, new String[] { "a", "b", "c" }, validFlags(3)));
	}

	@org.junit.jupiter.api.Test
	public void testWritingStringChannelWithoutUnit() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitS = em.loadAll(Unit.class, "s").get(0);
		Unit unitH = em.loadAll(Unit.class, "h").get(0);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		Channel channel1 = ef.createChannel("strings", measurement, quantity);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		// Write initial values (builder pattern prevents to specify unit on
		// WriteRequests with String values
		WriteRequest writeRequest = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS).explicit()
				.stringValues(new String[] { "a", "b", "c" }, new boolean[] { true, true, true }).build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(writeRequest));
			t.commit();
		}

		// Reading string values in different units than stored throws an exception.

		// read channel in default unit (in this case no unit at all)
		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channels(channel1).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.STRING.createMeasuredValues("strings", null, SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS, new String[] { "a", "b", "c" }, validFlags(3)));

		// read channel in unit s --> Exception
		assertThatThrownBy(
				() -> em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitS).allValues()))
						.isInstanceOf(DataAccessException.class)
						.hasMessage("Cannot convert Channel of datatype STRING from no unit to unit 's'.");

		// read channel in unit h --> Exception
		assertThatThrownBy(
				() -> em.readMeasuredValues(ReadRequest.create(channelGroup).channel(channel1, unitH).allValues()))
						.isInstanceOf(DataAccessException.class)
						.hasMessage("Cannot convert Channel of datatype STRING from no unit to unit 'h'.");

		// Append in matching unit
		WriteRequest appendRequestInMatchingUnit = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS)
				.explicit().stringValues(new String[] { "d", "e" }, new boolean[] { true, true }).build();

		try (Transaction t = em.startTransaction()) {
			t.appendMeasuredValues(Arrays.asList(appendRequestInMatchingUnit));
			t.commit();
		}

		assertThat(em.readMeasuredValues(ReadRequest.create(channelGroup).channels(channel1).allValues()))
				.usingRecursiveFieldByFieldElementComparator()
				.contains(ScalarType.STRING.createMeasuredValues("strings", "", SequenceRepresentation.EXPLICIT,
						new double[0], false, AxisType.Y_AXIS, new String[] { "a", "b", "c", "d", "e" },
						validFlags(5)));
	}

	@org.junit.jupiter.api.Test
	public void testReadChannelsInUnit() {

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitKg = em.loadAll(Unit.class, "kg").get(0);
		Unit unitG = em.loadAll(Unit.class, "g").get(0);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		Channel channel1 = ef.createChannel("massKg", measurement, quantity, unitKg);
		Channel channel2 = ef.createChannel("massG", measurement, quantity, unitG);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		WriteRequest writeRequest1 = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 1.1f, 42.0f, 1f }, new boolean[] { true, true, true }).sourceUnit(unitKg)
				.build();
		WriteRequest writeRequest2 = WriteRequest.create(channelGroup, channel2, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 200.23f, 142.0f, 10_001.99f }, new boolean[] { true, true, true })
				.sourceUnit(unitG).build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(writeRequest1, writeRequest2));
			t.commit();
		}

		// Read in stored unit
		assertThat(em.readMeasuredValues(
				ReadRequest.create(channelGroup).channels(Arrays.asList(channel1, channel2)).allValues()))
						.usingRecursiveFieldByFieldElementComparator().containsExactly(
								ScalarType.FLOAT.createMeasuredValues("massKg", "kg", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS, new float[] { 1.1f, 42.0f, 1f },
										validFlags(3)),
								ScalarType.FLOAT.createMeasuredValues("massG", "g", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS,
										new float[] { 200.23f, 142.0f, 10_001.99f }, validFlags(3)));

		// Read in stored unit explicitly
		assertThat(em.readMeasuredValues(
				ReadRequest.create(channelGroup).channel(channel1, unitKg).channel(channel2, unitG).allValues()))
						.usingRecursiveFieldByFieldElementComparator().containsExactly(
								ScalarType.FLOAT.createMeasuredValues("massKg", "kg", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS, new float[] { 1.1f, 42.0f, 1f },
										validFlags(3)),
								ScalarType.FLOAT.createMeasuredValues("massG", "g", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS,
										new float[] { 200.23f, 142.0f, 10_001.99f }, validFlags(3)));

		// Read in different units
		assertThat(em.readMeasuredValues(
				ReadRequest.create(channelGroup).channel(channel1, unitG).channel(channel2, unitKg).allValues()))
						.usingRecursiveFieldByFieldElementComparator().containsExactly(
								ScalarType.FLOAT.createMeasuredValues("massKg", "g", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS, new float[] { 1_100f, 42_000f, 1_000f },
										validFlags(3)),
								ScalarType.FLOAT.createMeasuredValues("massG", "kg", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS,
										new float[] { 0.20023f, 0.1420f, 10.00199f }, validFlags(3)));

		// Read providing only unit for one channel
		assertThat(em.readMeasuredValues(
				ReadRequest.create(channelGroup).channel(channel1, unitG).channels(channel2).allValues()))
						.usingRecursiveFieldByFieldElementComparator().containsExactly(
								ScalarType.FLOAT.createMeasuredValues("massKg", "g", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS, new float[] { 1_100f, 42_000f, 1_000f },
										validFlags(3)),
								ScalarType.FLOAT.createMeasuredValues("massG", "g", SequenceRepresentation.EXPLICIT,
										new double[0], false, AxisType.Y_AXIS,
										new float[] { 200.23f, 142.0f, 10_001.99f }, validFlags(3)));
	}

	@org.junit.jupiter.api.Test
	public void testAppendMultipleChannelsWithFlags() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Unit unitKg = em.loadAll(Unit.class, "kg").get(0);

		Unit unitS = em.loadAll(Unit.class, "s").get(0);

		Measurement measurement = ef.createMeasurement("MyMeaResult" + System.currentTimeMillis(), testStep);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		Channel chMass = ef.createChannel("mass", measurement, quantity, unitKg);
		Channel chTime = ef.createChannel("time", measurement, quantity, unitS);

		WriteRequest valuesMass1 = WriteRequest.create(channelGroup, chMass, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 3.3f, 0.001f, 0.123f }, new short[] { 21845, -32768, 0 }).sourceUnit(unitKg)
				.build();
		WriteRequest valuesTime1 = WriteRequest.create(channelGroup, chTime, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 1, 2, 3 }, new short[] { 15, 4369, 0 }).sourceUnit(unitS).build();

		// Write initial values
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(valuesMass1, valuesTime1));
			t.commit();
		}

		// Append values
		try (Transaction t = em.startTransaction()) {
			t.appendMeasuredValues(Arrays.asList(valuesMass1, valuesTime1));
			t.commit();
		}

		assertThat(em.readMeasuredValues(
				ReadRequest.create(channelGroup).channel(chMass, unitKg).channel(chTime, unitS).allValues()))
						.usingRecursiveFieldByFieldElementComparator()
						.contains(ScalarType.FLOAT.createMeasuredValues("mass", "kg", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS,
								new float[] { 3.3f, 0.001f, 0.123f, 3.3f, 0.001f, 0.123f },
								new short[] { 21845, -32768, 0, 21845, -32768, 0 }))
						.contains(ScalarType.INTEGER.createMeasuredValues("time", "s", SequenceRepresentation.EXPLICIT,
								new double[0], false, AxisType.Y_AXIS, new int[] { 1, 2, 3, 1, 2, 3 },
								new short[] { 15, 4369, 0, 15, 4369, 0 }));
	}

	short[] validFlags(int count) {
		short[] flags = new short[count];
		Arrays.fill(flags, (short) 15);
		return flags;
	}
}
