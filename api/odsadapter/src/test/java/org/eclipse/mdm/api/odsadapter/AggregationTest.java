/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Aggregation;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Aggregation test
 *
 * @since 1.0.0
 * @author maf, Peak Solution GmbH
 */
@Testcontainers(disabledWithoutDocker = true)
public class AggregationTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testQueryIdAndNameNoAggregation() throws DataAccessException {
		ModelManager modelManager = context.getModelManager().get();
		QueryService queryService = context.getQueryService().get();

		EntityType unitEntityType = modelManager.getEntityType(Unit.class);
		Attribute idAttribute = unitEntityType.getAttribute("Id");
		Attribute nameAttribute = unitEntityType.getAttribute("Name");

		List<Result> listRes = queryService.createQuery().select(idAttribute).select(nameAttribute).fetch();

		assertThat(listRes.size()).isGreaterThanOrEqualTo(1);
		assertThat(listRes.get(0).getValue(idAttribute).getValueType()).isEqualTo(ValueType.STRING);
		// Test retrieving attribute value with Aggregation.NONE (should yield the same
		// result as with no aggregation):
		assertThat(listRes.get(0).getValue(idAttribute, Aggregation.NONE).getValueType()).isEqualTo(ValueType.STRING);
		assertThat(listRes.get(0).getValue(nameAttribute).getValueType()).isEqualTo(ValueType.STRING);
	}

	@org.junit.jupiter.api.Test
	public void testQueryIdWithAggregation() throws DataAccessException {
		ModelManager modelManager = context.getModelManager().get();
		QueryService queryService = context.getQueryService().get();

		EntityType unitEntityType = modelManager.getEntityType(Unit.class);
		Attribute idAttribute = unitEntityType.getAttribute("Id");

		List<Result> listRes = queryService.createQuery().select(idAttribute, Aggregation.MAXIMUM) // should be a string
																									// in result, just
																									// like
																									// non-aggregated Id
																									// attribute
				.select(idAttribute, Aggregation.AVERAGE) // should be a numeric value in result
				.fetch();

		assertThat(listRes.size()).isGreaterThanOrEqualTo(1);
		assertThat(listRes.get(0).getValue(idAttribute, Aggregation.MAXIMUM).getValueType())
				.isEqualTo(ValueType.STRING);
		assertThat(listRes.get(0).getValue(idAttribute, Aggregation.AVERAGE).getValueType()).isEqualTo(ValueType.LONG);
	}

	@org.junit.jupiter.api.Test
	public void testQueryFactorWithAggregation() throws DataAccessException {
		ModelManager modelManager = context.getModelManager().get();
		QueryService queryService = context.getQueryService().get();

		EntityType unitEntityType = modelManager.getEntityType(Unit.class);
		Attribute factorAttribute = unitEntityType.getAttribute("Factor");

		List<Result> listRes = queryService.createQuery().select(factorAttribute, Aggregation.COUNT)
				.group(factorAttribute).fetch();

		assertThat(listRes.size()).isGreaterThanOrEqualTo(1);
		assertThat(listRes.get(0).getValue(factorAttribute, Aggregation.COUNT).getValueType())
				.isEqualTo(ValueType.INTEGER);
	}

	@org.junit.jupiter.api.Test
	public void testQueryFactorWithAndWithoutAggregation() throws DataAccessException {
		ModelManager modelManager = context.getModelManager().get();
		QueryService queryService = context.getQueryService().get();

		EntityType unitEntityType = modelManager.getEntityType(Unit.class);
		Attribute factorAttribute = unitEntityType.getAttribute("Factor");

		List<Result> listRes = queryService.createQuery().select(factorAttribute)
				.select(factorAttribute, Aggregation.SUM).group(factorAttribute).fetch();

		assertThat(listRes.size()).isGreaterThanOrEqualTo(1);
		assertThat(listRes.get(0).getValue(factorAttribute).getValueType()).isEqualTo(ValueType.DOUBLE);
		assertThat(listRes.get(0).getValue(factorAttribute, Aggregation.SUM).getValueType())
				.isEqualTo(ValueType.DOUBLE);
	}

}
