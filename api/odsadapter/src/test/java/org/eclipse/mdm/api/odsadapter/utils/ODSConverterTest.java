/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.utils;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.time.Instant;
import java.time.ZoneId;

import org.asam.ods.TS_UnionSeq;
import org.asam.ods.TS_ValueSeq;
import org.eclipse.mdm.api.base.query.Aggregation;
import org.eclipse.mdm.api.odsadapter.query.ODSAttribute;
import org.junit.Test;

public class ODSConverterTest {

	@Test
	public void testFromODSValueSeqODSDateYear() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("2017"), ZoneId.of("UTC"));

		verify(attr).createValue(eq(""), eq(true), eq(Instant.parse("2017-01-01T00:00:00Z")));
	}

	@Test
	public void testFromODSValueSeqODSDateMonth() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("201710"), ZoneId.of("UTC"));

		verify(attr).createValue(eq(""), eq(true), eq(Instant.parse("2017-10-01T00:00:00Z")));
	}

	@Test
	public void testFromODSValueSeqODSDate() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("20171004"), ZoneId.of("UTC"));

		verify(attr).createValue(eq(""), eq(true), eq(Instant.parse("2017-10-04T00:00:00Z")));
	}

	@Test
	public void testFromODSValueSeqODSDateHour() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("2017100412"),
				ZoneId.of("UTC"));

		verify(attr).createValue(eq(""), eq(true), eq(Instant.parse("2017-10-04T12:00:00Z")));
	}

	@Test
	public void testFromODSValueSeqODSDateMinute() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("201710041213"),
				ZoneId.of("UTC"));

		verify(attr).createValue(eq(""), eq(true), eq(Instant.parse("2017-10-04T12:13:00Z")));
	}

	@Test
	public void testFromODSValueSeqODSDateSecond() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("20171004121314"),
				ZoneId.of("UTC"));

		verify(attr).createValue(eq(""), eq(true), eq(Instant.parse("2017-10-04T12:13:14Z")));
	}

	@Test
	public void testFromODSValueSeqODSDateMillisecond() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("20171004121314123"),
				ZoneId.of("UTC"));

		verify(attr).createValue(eq(""), eq(true), eq(Instant.parse("2017-10-04T12:13:14.123Z")));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFromODSValueSeqInvalidLength() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("201710041"),
				ZoneId.of("UTC"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFromODSValueSeqInvalidMonth() throws Exception {
		ODSAttribute attr = mock(ODSAttribute.class);

		ODSConverter.fromODSValueSeq(attr, Aggregation.NONE, "", getTS_ValueSeqFromDates("20171304"), ZoneId.of("UTC"));
	}

	private TS_ValueSeq getTS_ValueSeqFromDates(String... dates) {
		TS_UnionSeq u = new TS_UnionSeq();
		u.dateVal(dates);
		return new TS_ValueSeq(u, new short[] { 15 });
	}
}
