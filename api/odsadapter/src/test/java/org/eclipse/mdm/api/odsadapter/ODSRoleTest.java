/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Role;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ODSRoleTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testLoadRelatedUsers()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		EntityManager em = context.getEntityManager().get();

		User user = em.loadAll(User.class, "sa").get(0);
		List<Role> role = em.loadRelatedEntities(user, "users2groups", Role.class);

		assertThat(role).hasSize(1).extracting(Role::getName).containsExactly("MDMSystemAdministrator");
	}

	@org.junit.jupiter.api.Test
	public void testLoadRelatedRoles()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		EntityManager em = context.getEntityManager().get();

		Role role = em.loadAll(Role.class, "MDMSystemAdministrator").get(0);
		List<User> user = em.loadRelatedEntities(role, "groups2users", User.class);

		assertThat(user).hasSize(1).extracting(User::getName).containsExactly("sa");
	}

	@org.junit.jupiter.api.Test
	public void testCreateUserAndRole()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		EntityManager em = context.getEntityManager().get();
		EntityFactory entityFactory = context.getEntityFactory().get();

		String userName = "CreateUserAndRole";
		String roleName = "CreateUserAndRole";

		try {
			Transaction transaction = em.startTransaction();
			User user = entityFactory.createUser(userName, "Test", "User");
			Role role = entityFactory.createRole(roleName);
			role.addUser(user);

			transaction.create(Arrays.asList(user, role));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role, "groups2users", User.class)).hasSize(1).extracting(User::getName)
					.contains(userName);

			assertThat(em.loadRelatedEntities(user, "users2groups", Role.class)).hasSize(1).extracting(Role::getName)
					.contains(roleName);
		} finally {
			Transaction transaction = em.startTransaction();
			List<User> users = em.loadAll(User.class, userName);
			transaction.delete(users);
			List<Role> roles = em.loadAll(Role.class, roleName);
			transaction.delete(roles);
			transaction.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testAddMultipleUsersToNewRole()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		EntityManager em = context.getEntityManager().get();
		EntityFactory entityFactory = context.getEntityFactory().get();

		String roleName = "AddMultipleUsersToNewRole";
		try {
			Transaction transaction = em.startTransaction();
			List<User> users = em.loadAll(User.class);
			Role role = entityFactory.createRole(roleName);

			users.forEach(u -> role.addUser(u));
			transaction.create(Arrays.asList(role));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role, "groups2users", User.class)).hasSize(users.size());
		} finally {
			Transaction transaction = em.startTransaction();
			List<Role> roles = em.loadAll(Role.class, roleName);
			transaction.delete(roles);
			transaction.commit();
		}
	}

	@org.junit.jupiter.api.Test
	public void testRemoveUserFromRole()
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		EntityManager em = context.getEntityManager().get();
		EntityFactory entityFactory = context.getEntityFactory().get();

		String userName = "RemoveUserFromRole";
		try {
			Transaction transaction = em.startTransaction();
			Role role = em.load(Role.class, "1");
			User user = entityFactory.createUser(userName, "User", "User");

			role.addUser(user);

			transaction.create(Arrays.asList(user));
			transaction.update(Arrays.asList(role));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role, "groups2users", User.class)).hasSize(2);

			Role role2 = em.load(Role.class, "1");
			transaction = em.startTransaction();
			role2.removeUser(user);
			transaction.update(Arrays.asList(role2));
			transaction.commit();

			assertThat(em.loadRelatedEntities(role2, "groups2users", User.class)).hasSize(1);
		} finally {
			Transaction transaction = em.startTransaction();
			List<User> users = em.loadAll(User.class, userName);
			transaction.delete(users);
			transaction.commit();
		}
	}
}
