/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.updatestatement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ChangeTemplateOfTestStepMultiTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext context;

	public static TemplateTest templateTest;

	public static TemplateTestStep templateTestStepCar0;
	public static TemplateTestStep templateTestStepCar1;
	public static TemplateTestStep templateTestStepCar2;
	public static TemplateTestStep templateTestStepCar3;
	public static TemplateTestStep templateTestStepCar4;

	public static Test myTest;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		CatalogComponent ccEngine = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "engine");
		ef.createCatalogAttribute("power", ValueType.INTEGER, ccEngine);

		CatalogComponent ccWheel = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "wheel");
		ef.createCatalogAttribute("size", ValueType.INTEGER, ccWheel);
		CatalogAttribute caColor = ef.createCatalogAttribute("color", ValueType.STRING, ccWheel);

		TemplateRoot trUutCar0 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar0");
		TemplateComponent comp = ef.createTemplateComponent("front_l", trUutCar0, ccWheel, false);
		ef.createTemplateAttribute(caColor, comp);
		comp = ef.createTemplateComponent("front_r", trUutCar0, ccWheel, false);
		ef.createTemplateAttribute(caColor, comp);

		TemplateRoot trUutCar1 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar1");
		ef.createTemplateComponent("front_l", trUutCar1, ccWheel, true);
		ef.createTemplateComponent("front_r", trUutCar1, ccWheel, true);

		TemplateRoot trUutCar2 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar2");
		ef.createTemplateComponent("front_l", trUutCar2, ccWheel, true);
		ef.createTemplateComponent("front_r", trUutCar2, ccWheel, true);
		ef.createTemplateComponent("rear_l", trUutCar2, ccWheel, true);
		ef.createTemplateComponent("rear_r", trUutCar2, ccWheel, true);
		ef.createTemplateComponent("engine", trUutCar2, ccEngine, true);

		TemplateRoot trUutCar3 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar3");
		ef.createTemplateComponent("engine", trUutCar3, ccEngine, true);

		TemplateRoot trUutCar4 = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutCar4");
		ef.createTemplateComponent("rear_l", trUutCar4, ccWheel, true);
		ef.createTemplateComponent("rear_r", trUutCar4, ccWheel, true);

		templateTestStepCar0 = ef.createTemplateTestStep("TplCar0");
		templateTestStepCar0.setTemplateRoot(trUutCar0);

		templateTestStepCar1 = ef.createTemplateTestStep("TplCar1");
		templateTestStepCar1.setTemplateRoot(trUutCar1);

		templateTestStepCar2 = ef.createTemplateTestStep("TplCar2");
		templateTestStepCar2.setTemplateRoot(trUutCar2);

		templateTestStepCar3 = ef.createTemplateTestStep("TplCar3");
		templateTestStepCar3.setTemplateRoot(trUutCar3);

		templateTestStepCar4 = ef.createTemplateTestStep("TplCar4");
		templateTestStepCar4.setTemplateRoot(trUutCar4);

		templateTest = ef.createTemplateTest("TplTest");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar0);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar1);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar2);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar3);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepCar4);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccWheel, ccEngine));
			t.create(Arrays.asList(trUutCar0, trUutCar1, trUutCar2, trUutCar3, trUutCar4));
			t.create(Arrays.asList(templateTestStepCar0, templateTestStepCar1, templateTestStepCar2,
					templateTestStepCar3, templateTestStepCar4));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);

		myTest = ef.createTest("MyTest", pool, templateTest, false);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateEqual() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateEqual", myTest, templateTestStepCar1);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar1);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar1");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateNoMatching() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateNoMatching", myTest, templateTestStepCar3);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar1);
		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"There is no matching TemplateComponent for TplUnitUnderTestComp:engine pointing to CatUnitUnderTestComp:engine");
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateNoNameMatching() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateNoNameMatching", myTest, templateTestStepCar1);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar4);
		try (Transaction t = em.startTransaction()) {
			assertThatThrownBy(() -> t.update(Arrays.asList(testStep))).isInstanceOf(IllegalArgumentException.class)
					.hasMessage(
							"There is no matching TemplateComponent with same name for TplUnitUnderTestComp:front_l");
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateMoreAttributes() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateMoreAttributes", myTest, templateTestStepCar0);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar1);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar1");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
		}
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateMoreAttributesAndComponents() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateMoreAttributesAndComponents", myTest,
				templateTestStepCar0);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}

		EntityFactory.getCore(testStep).getMutableStore().set(templateTestStepCar2);
		try (Transaction t = em.startTransaction()) {
			t.update(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplCar2");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot).isNotNull();
		}
	}

}
