/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_NAMESERVICE;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_PASSWORD;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_SERVICENAME;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_USER;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.assertj.core.util.DoubleComparator;
import org.assertj.core.util.FloatComparator;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequestBuilder;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.DescriptiveFilesAttachable;
import org.eclipse.mdm.api.base.model.DoubleComplex;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.FilesAttachable;
import org.eclipse.mdm.api.base.model.FloatComplex;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.PhysicalDimension;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap;

@Testcontainers(disabledWithoutDocker = true)
public class ODSAdapterTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSAdapterTest.class);

	public static boolean isHttp = true;
	public static FileServiceType fileServiceType = FileServiceType.AOFILE;

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static ParameterResolver resolver = new ParameterResolver() {
		@Override
		public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
				throws ParameterResolutionException {
			if (parameterContext.getParameter().getType() == ApplicationContext.class) {
				try {
					if (isHttp) {
						return new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());
					} else {
						Map<String, String> connectionParameters = ImmutableMap.<String, String>builder()
								.put(PARAM_NAMESERVICE,
										String.format("corbaloc::1.2@%s:%s/NameService", "localhost", "2809"))
								.put(PARAM_SERVICENAME, "MDM.ASAM-ODS").put(PARAM_USER, "sa").put(PARAM_PASSWORD, "sa")
								.build();
						return new ODSContextFactory().connect("MDM", connectionParameters);
					}
				} catch (Exception e) {
					throw new ParameterResolutionException("Could not resolve ApplicationContext", e);
				}
			} else {
				return null;
			}
		}

		@Override
		public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
				throws ParameterResolutionException {
			return parameterContext.getParameter().getType() == ApplicationContext.class;
		}
	};

	private static final Float FLOAT_EPSILON = 0.01f;
	private static final Double DOUBLE_EPSILON = 0.00001;

	private final Comparator<? super FloatComplex> floatComplexComparator = Comparator
			.comparing(FloatComplex::real, new FloatComparator(FLOAT_EPSILON))
			.thenComparing(Comparator.comparing(FloatComplex::imaginary, new FloatComparator(FLOAT_EPSILON)));

	private final Comparator<? super DoubleComplex> doubleComplexComparator = Comparator
			.comparing(DoubleComplex::real, new DoubleComparator(DOUBLE_EPSILON))
			.thenComparing(Comparator.comparing(DoubleComplex::imaginary, new DoubleComparator(DOUBLE_EPSILON)));

	@org.junit.jupiter.api.Test
	void runtTestScript(ApplicationContext context) throws DataAccessException {
		EntityManager entityManager = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		EntityFactory entityFactory = context.getEntityFactory()
				.orElseThrow(() -> new IllegalStateException("Entity manager factory not available."));

		List<CatalogComponent> catalogComponents = createCatalogComponents(context);
		List<TemplateRoot> templateRoots = createTemplateRoots(entityFactory, catalogComponents);
		List<TemplateTestStep> templateTestSteps = createTemplateTestSteps(entityFactory, templateRoots);
		TemplateTest templateTest = createTemplateTest(entityFactory, templateTestSteps);
		PhysicalDimension physicalDimension = entityFactory.createPhysicalDimension("any_physical_dimension");
		Unit unit = entityFactory.createUnit("any_unit", physicalDimension);
		Quantity quantity = entityFactory.createQuantity("any_quantity", unit);

		Transaction transaction = entityManager.startTransaction();
		try {
			create(transaction, "catalog components", catalogComponents);
			create(transaction, "template roots", templateRoots);
			create(transaction, "template test steps", templateTestSteps);
			create(transaction, "template test", Collections.singletonList(templateTest));
			create(transaction, "physical dimension", Collections.singletonList(physicalDimension));
			create(transaction, "unit", Collections.singletonList(unit));
			create(transaction, "quantity", Collections.singletonList(quantity));

			transaction.commit();
		} catch (RuntimeException e) {
			transaction.abort();
			e.printStackTrace();
			fail("Unable to create test structure due to: " + e.getMessage());
		}

		List<Project> projects = Collections.emptyList();
		try {
			projects = createTestData(context, templateTest, quantity);
		} catch (RuntimeException e) {
			e.printStackTrace();
			fail("Unable to create test data due to: " + e.getMessage());
		}

		// Load and assert data
		// TODO assertions are incomplete
		assertThat(projects).hasSize(1);
		List<Test> tests = entityManager.loadAll(Test.class, "*");
		assertThat(tests).hasSize(2);
		assertThat(tests.get(0).getMDMFiles(entityManager)).allSatisfy(file -> {
			assertThat(file.getName()).matches("myFile[0-9]*\\.txt");
			try (Transaction t = entityManager.startTransaction()) {
				assertThat(
						context.getFileService(fileServiceType).get().openStream(tests.get(0), file.getFileLink(), t))
								.hasBinaryContent("some text".getBytes());
			}
		});

		List<Measurement> measurements = entityManager.loadAll(Measurement.class, "*");
		assertThat(measurements).hasSize(8);
		assertThat(measurements.get(0).loadContexts(entityManager, ContextType.UNITUNDERTEST))
				.containsKey(ContextType.UNITUNDERTEST).satisfiesAnyOf(e -> {
					for (ContextRoot r : e.values()) {
						for (ContextComponent c : r.getContextComponents()) {
							assertContextComponent(context, c);
						}
					}
				});

//		transaction = entityManager.startTransaction();
//		try {
//			// delete in reverse order!
//			if (!projects.isEmpty()) {
//				delete(transaction, "projects and their children", projects);
//			}
//
//			delete(transaction, "quantity", Collections.singletonList(quantity));
//			delete(transaction, "unit", Collections.singletonList(unit));
//			delete(transaction, "physical dimension", Collections.singletonList(physicalDimension));
//			delete(transaction, "template test", Collections.singletonList(templateTest));
//			delete(transaction, "template test steps", templateTestSteps);
//			delete(transaction, "template roots", templateRoots);
//			delete(transaction, "catalog components", catalogComponents);
//			transaction.commit();
//		} catch (RuntimeException e) {
//			transaction.abort();
//			fail("Unable to delete test data due to: " + e.getMessage());
//		}
//
//		if (projects.isEmpty()) {
//			fail("Was unable to create test data.");
//		}
	}

	private List<Project> createTestData(ApplicationContext context, TemplateTest templateTest, Quantity quantity)
			throws DataAccessException {
		EntityManager entityManager = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		EntityFactory entityFactory = context.getEntityFactory()
				.orElseThrow(() -> new IllegalStateException("Entity manager factory not available."));

		Project project = entityFactory.createProject("simple_project");
		Pool pool = entityFactory.createPool("simple_pool", project);

		List<Test> tests = createTests(entityFactory, 2, pool, templateTest);

		// create measurement test data
//		List<De>
		List<WriteRequest> writeRequests = new ArrayList<>();
		for (Test test : tests) {
			for (TestStep testStep : test.getCommissionedTestSteps()) {
				Optional<TemplateTestStep> templateTestStep = TemplateTestStep.of(testStep);
				List<ContextRoot> contextRoots = new ArrayList<>();
				if (templateTestStep.isPresent()) {
					contextRoots = templateTestStep.get().getTemplateRoots().stream()
							.map(templateRoot -> entityFactory.createContextRoot(templateRoot))
							.map(contextRoot -> fillAttributes(context, contextRoot)).collect(Collectors.toList());
//					contextRoots.stream().flatMap(cr -> cr.getContextComponents().stream().flatMap(cc -> {
//						ModelManager modelManager = context.getModelManager().get();
//
//						DescriptiveFileManager descriptiveFileManager = new DescriptiveFileManager(modelManager);
//						Relation fileRelation = descriptiveFileManager.getDescriptiveFileRelation(cc, "file_relation");
//						return contextComp.getDescriptiveFile(fileRelation);
//					})); // .forEach();
				}
				for (int i = 1; i < 3; i++) {
					Measurement measurement = entityFactory.createMeasurement("measurement_" + i, testStep,
							contextRoots);

					// create channels
					List<Channel> channels = new ArrayList<>();
					for (int j = 0; j < 9; j++) {
						channels.add(entityFactory.createChannel("channel_ " + j, measurement, quantity));
					}

					// create channel group
					ChannelGroup channelGroup = entityFactory.createChannelGroup("group", 10, measurement);
					writeRequests.addAll(createMeasurementData(measurement, channelGroup, channels));
				}
			}
		}
//		createFileAttachments(entityFactory, tests);

		Transaction transaction = entityManager.startTransaction();
		try {
			create(transaction, "project and pool with tests based on templates with measurements and mass data",
					Collections.singleton(project));

			transaction.writeMeasuredValues(writeRequests);
			transaction.commit();
			return Collections.singletonList(project);
		} catch (DataAccessException e) {
			e.printStackTrace();
			transaction.abort();
		}

		return Collections.emptyList();
	}

	private void createFileAttachments(EntityFactory factory, List<? extends FilesAttachable<?>> attachables) {
		for (FilesAttachable<?> attachable : attachables) {
			attachable.addFileLink(randomFileLink(factory, attachable, null));
			if (attachable instanceof Test) {
				createFileAttachments(factory, attachable.getChildren(TestStep.class));
			} else if (attachable instanceof TestStep) {
				createFileAttachments(factory, attachable.getChildren(Measurement.class));
			}
		}

	}

	private ContextRoot fillAttributes(ApplicationContext context, ContextRoot contextRoot) {
		contextRoot.getContextComponents().forEach(cc -> this.fillAttributes(context, cc));
		return contextRoot;
	}

	private ContextComponent fillAttributes(ApplicationContext context, ContextComponent contextComp) {

		Enumeration<?> enumeration = EnumRegistry.getInstance().get(context.getSourceName(), EnumRegistry.SCALAR_TYPE);

		contextComp.getValue("string").set("myString");
		contextComp.getValue("date").set(Instant.parse("2024-12-12T12:13:15Z"));
		contextComp.getValue("boolean").set(true);
		contextComp.getValue("byte").set((byte) 127);
		contextComp.getValue("short").set((short) 5);
		contextComp.getValue("int").set(23_123);
		contextComp.getValue("long").set(54_234_324L);
		contextComp.getValue("float").set(1.2f);
		contextComp.getValue("double").set(2.781);
		contextComp.getValue("float_complex").set(new FloatComplex(4.5f, 6.7f));
		contextComp.getValue("double_complex").set(new DoubleComplex(3.14159, 2.781));
		contextComp.getValue("scalar_type").set(enumeration.valueOf(2));

		contextComp.getValue("string_array").set(new String[] { "myString1", "myString1" });
		contextComp.getValue("date_array")
				.set(new Instant[] { Instant.parse("2024-12-12T12:13:15Z"), Instant.parse("2024-12-13T03:04:05Z") });
		contextComp.getValue("boolean_array").set(new boolean[] { true, false, true });
		contextComp.getValue("byte_array").set("abc".getBytes());
		contextComp.getValue("short_array").set(new short[] { (short) 5, (short) -12 });
		contextComp.getValue("int_array").set(new int[] { 23_123, -12_089 });
		contextComp.getValue("long_array").set(new long[] { 54_234_324L, -89_234_090L, 0L });
		contextComp.getValue("float_array").set(new float[] { 1.2f, -4.5f });
		contextComp.getValue("double_array").set(new double[] { 2.781, 3.14159 });
		contextComp.getValue("float_complex_array").set(new FloatComplex[] { new FloatComplex(4.5f, 6.7f) });
		contextComp.getValue("double_complex_array").set(new DoubleComplex[] { new DoubleComplex(3.14159, 2.781) });
		contextComp.getValue("scalar_type_array")
				.set(new EnumerationValue[] { enumeration.valueOf(2), enumeration.valueOf(8) });

//		if (hasDescriptiveFile(context)) {
//			EntityFactory entityFactory = context.getEntityFactory().get();
//			ModelManager modelManager = context.getModelManager().get();
//
//			DescriptiveFileManager descriptiveFileManager = new DescriptiveFileManager(modelManager);
//			Relation fileRelation = descriptiveFileManager.getDescriptiveFileRelation(contextComp, "file_relation");
//			contextComp.addDescriptiveFile(fileRelation,
//					createDescriptiveFile(entityFactory, contextComp, fileRelation));
//		}

		// file_link(_array)
//		contextComp.getValue("file_relation").set(randomFileLink(entityFactory, attachable, fileRelation));

//		Relation relationFileLink = descriptiveFileManager.getDescriptiveFileRelation(contextComp, "file_link");
//		Relation relationFileLinkArray = descriptiveFileManager.getDescriptiveFileRelation(contextComp,
//				"file_link_array");
//
//		contextComp.getValue("file_link").set(randomFileLink(entityFactory, attachable, relationFileLink));
//		contextComp.getValue("file_link_array")
//				.set(new FileLink[] { randomFileLink(entityFactory, attachable, relationFileLinkArray),
//						randomFileLink(entityFactory, attachable, relationFileLinkArray) });
		return contextComp;
	}

	private void assertContextComponent(ApplicationContext context, ContextComponent contextComp) {
		Enumeration<?> enumeration = EnumRegistry.getInstance().get(context.getSourceName(), EnumRegistry.SCALAR_TYPE);

		assertThat(contextComp.getValue("string").extract(ValueType.STRING)).isEqualTo("myString");
		assertThat(contextComp.getValue("date").extract(ValueType.DATE))
				.isEqualTo(Instant.parse("2024-12-12T12:13:15Z"));
		assertThat(contextComp.getValue("boolean").extract(ValueType.BOOLEAN)).isTrue();
		assertThat(contextComp.getValue("byte").extract(ValueType.BYTE)).isEqualTo((byte) 127);
		assertThat(contextComp.getValue("short").extract(ValueType.SHORT)).isEqualTo((short) 5);
		assertThat(contextComp.getValue("int").extract(ValueType.INTEGER)).isEqualTo(23_123);
		assertThat(contextComp.getValue("long").extract(ValueType.LONG)).isEqualTo(54_234_324L);
		assertThat(contextComp.getValue("float").extract(ValueType.FLOAT)).isEqualTo(1.2f);
		assertThat(contextComp.getValue("double").extract(ValueType.DOUBLE)).isEqualTo(2.781);
		assertThat(contextComp.getValue("float_complex").extract(ValueType.FLOAT_COMPLEX))
				.usingComparator(floatComplexComparator).isEqualTo(new FloatComplex(4.5f, 6.7f));
		assertThat(contextComp.getValue("double_complex").extract(ValueType.DOUBLE_COMPLEX))
				.usingComparator(doubleComplexComparator).isEqualTo(new DoubleComplex(3.14159, 2.781));
//			assertThat(contextComp.getValue("file_link").extract(ValueType.FILELINK)).isEqualTo(randomFileLink());
		assertThat(contextComp.getValue("scalar_type").extract(ValueType.ENUMERATION))
				.isEqualTo(enumeration.valueOf(2));

		assertThat(contextComp.getValue("string_array").extract(ValueType.STRING_SEQUENCE))
				.isEqualTo(new String[] { "myString1", "myString1" });
		assertThat(contextComp.getValue("date_array").extract(ValueType.DATE_SEQUENCE)).isEqualTo(
				new Instant[] { Instant.parse("2024-12-12T12:13:15Z"), Instant.parse("2024-12-13T03:04:05Z") });
		assertThat(contextComp.getValue("boolean_array").extract(ValueType.BOOLEAN_SEQUENCE))
				.isEqualTo(new boolean[] { true, false, true });
		assertThat(contextComp.getValue("byte_array").extract(ValueType.BYTE_SEQUENCE)).isEqualTo("abc".getBytes());
		assertThat(contextComp.getValue("short_array").extract(ValueType.SHORT_SEQUENCE))
				.isEqualTo(new short[] { (short) 5, (short) -12 });
		assertThat(contextComp.getValue("int_array").extract(ValueType.INTEGER_SEQUENCE))
				.isEqualTo(new int[] { 23_123, -12_089 });
		assertThat(contextComp.getValue("long_array").extract(ValueType.LONG_SEQUENCE))
				.isEqualTo(new long[] { 54_234_324L, -89_234_090L, 0L });
		assertThat(contextComp.getValue("float_array").extract(ValueType.FLOAT_SEQUENCE))
				.isEqualTo(new float[] { 1.2f, -4.5f });
		assertThat(contextComp.getValue("double_array").extract(ValueType.DOUBLE_SEQUENCE))
				.isEqualTo(new double[] { 2.781, 3.14159 });
		assertThat(contextComp.getValue("float_complex_array").extract(ValueType.FLOAT_COMPLEX_SEQUENCE))
				.usingElementComparator(floatComplexComparator)
				.isEqualTo(new FloatComplex[] { new FloatComplex(4.5f, 6.7f) });
		assertThat(contextComp.getValue("double_complex_array").extract(ValueType.DOUBLE_COMPLEX_SEQUENCE))
				.usingElementComparator(doubleComplexComparator).containsExactly(new DoubleComplex(3.14159, 2.781));
//			assertThat(contextComp.getValue("file_link_array").extract(ValueType.FILELINK)).isEqualTo(new FileLink[] { randomFileLink(), randomFileLink() });
		assertThat(contextComp.getValue("scalar_type_array").extract(ValueType.ENUMERATION_SEQUENCE))
				.isEqualTo(new EnumerationValue[] { enumeration.valueOf(2), enumeration.valueOf(8) });

//		if (hasDescriptiveFile(context)) {
//			DescriptiveFileManager descriptiveFileManager = new DescriptiveFileManager(context.getModelManager().get());
//			Relation fileRelation = descriptiveFileManager.getDescriptiveFileRelation(contextComp, "file_relation");
//			List<DescriptiveFile> files = contextComp.getDescriptiveFiles(fileRelation,
//					context.getEntityManager().get());
//
//			assertThat(files).hasSize(1).element(0).hasFieldOrPropertyWithValue("OriginalFileName", "test123.txt");
//		}
	}

	private FileLink randomFileLink(EntityFactory factory, FilesAttachable<?> attachable, Relation relMDMFile) {
		try {
			File file = randomFile();

			switch (fileServiceType) {
			case AOFILE:
				MDMFile mdmFile;
				if (attachable instanceof DescriptiveFilesAttachable) {
					// Create DescriptiveFile for TemplateAttribute
					mdmFile = factory.createDescriptiveFile((DescriptiveFilesAttachable) attachable, relMDMFile);
				} else {
					// Create MDMFile for fileAttachable:
					mdmFile = factory.createMDMFile(attachable);
				}
				mdmFile.setName(file.getName());
				mdmFile.setDescription("desc");
				mdmFile.setFileMimeType("text/plain");
				mdmFile.setOriginalFileName("test123.txt");
				mdmFile.setOriginalFileDate(Instant.ofEpochMilli(file.lastModified()));

				FileLink fileLink = FileLink.newLocal(new FileInputStream(file), file.getName(),
						Files.size(file.toPath()), new MimeType("text/plain"), "", mdmFile, fileServiceType);
				mdmFile.setFileLink(fileLink);

				return fileLink;
			case EXTREF:
				return FileLink.newLocal(new FileInputStream(file), file.getName(), Files.size(file.toPath()),
						new MimeType("text/plain"), "", null, fileServiceType);
			default:
				throw new RuntimeException("Unsopported FileServiceType: " + fileServiceType);
			}

		} catch (IOException e) {
			throw new RuntimeException("Could not fill attributes.", e);
		}
	}

	private File randomFile() {
		try {
			File myFile = File.createTempFile("myFile", ".txt");
			myFile.deleteOnExit();

			try (OutputStream os = Files.newOutputStream(myFile.toPath(), StandardOpenOption.CREATE,
					StandardOpenOption.TRUNCATE_EXISTING)) {

				os.write("some text".getBytes());
			}
			return myFile;
		} catch (IOException e) {
			throw new RuntimeException("Could not write file.", e);
		}
	}

	private List<WriteRequest> createMeasurementData(Measurement measurement, ChannelGroup channelGroup,
			List<Channel> channels) {
		// set length of the channel value sequence
		List<WriteRequest> writeRequests = new ArrayList<>();

		// populate channel value write requests - one per channel
		Collections.sort(channels, (c1, c2) -> c1.getName().compareTo(c2.getName()));

		WriteRequestBuilder wrb = WriteRequest.create(channelGroup, channels.get(0), AxisType.X_AXIS);
		writeRequests.add(wrb.implicitLinear(ScalarType.FLOAT, 0, 1).independent().build());

		wrb = WriteRequest.create(channelGroup, channels.get(1), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit()
				.booleanValues(new boolean[] { true, true, false, true, true, false, true, false, false, false })
				.build());

		wrb = WriteRequest.create(channelGroup, channels.get(2), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit().byteValues(new byte[] { 5, 32, 42, 9, 17, 65, 13, 8, 15, 21 }).build());

		wrb = WriteRequest.create(channelGroup, channels.get(3), AxisType.Y_AXIS);
		writeRequests.add(
				wrb.explicit().integerValues(new int[] { 423, 645, 221, 111, 675, 353, 781, 582, 755, 231 }).build());

		wrb = WriteRequest.create(channelGroup, channels.get(4), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit()
				.stringValues(new String[] { "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10" }).build());

		Instant now = Instant.now();
		wrb = WriteRequest.create(channelGroup, channels.get(5), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit()
				.dateValues(new Instant[] { now, now.plus(1, ChronoUnit.DAYS), now.plus(2, ChronoUnit.DAYS),
						now.plus(3, ChronoUnit.DAYS), now.plus(4, ChronoUnit.DAYS), now.plus(5, ChronoUnit.DAYS),
						now.plus(6, ChronoUnit.DAYS), now.plus(7, ChronoUnit.DAYS), now.plus(8, ChronoUnit.DAYS),
						now.plus(9, ChronoUnit.DAYS) })
				.build());

		wrb = WriteRequest.create(channelGroup, channels.get(6), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit().byteStreamValues(new byte[][] { { 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 9, 10 },
				{ 11 }, { 12, 13, 14 }, { 15, 16 }, { 17, 18, 19, 20 }, { 21, 22 }, { 23 } }).build());

		wrb = WriteRequest.create(channelGroup, channels.get(7), AxisType.Y_AXIS);
		writeRequests.add(wrb.implicitConstant(ScalarType.SHORT, Short.MAX_VALUE).build());

		wrb = WriteRequest.create(channelGroup, channels.get(8), AxisType.Y_AXIS);
		writeRequests.add(wrb.implicitSaw(ScalarType.FLOAT, 0, 1, 4).build());

		return writeRequests;
	}

	private static void delete(Transaction transaction, String key, Collection<? extends Deletable> entities)
			throws DataAccessException {
		LOGGER.info(">>>>>>>>>>>>>>>>> deleting {} ...", key);
		long start = System.currentTimeMillis();
		transaction.delete(entities);
		LOGGER.info(">>>>>>>>>>>>>>>>> {} deleted in {} ms", key, System.currentTimeMillis() - start);
	}

	private static void create(Transaction transaction, String key, Collection<? extends Entity> entities)
			throws DataAccessException {
		LOGGER.info(">>>>>>>>>>>>>>>>> creating {}...", key);
		long start = System.currentTimeMillis();
		transaction.create(entities);
		LOGGER.info(">>>>>>>>>>>>>>>>> {} written in {} ms", key, System.currentTimeMillis() - start);
	}

	private List<Test> createTests(EntityFactory entityFactory, int count, Pool pool, TemplateTest templateTest) {
		return IntStream.range(1, ++count)
				.mapToObj(i -> entityFactory.createTest("simple_test_" + i, pool, templateTest))
				.collect(Collectors.toList());
	}

	private TemplateTest createTemplateTest(EntityFactory entityFactory, List<TemplateTestStep> templateTestSteps) {
		TemplateTest templateTest = entityFactory.createTemplateTest("tpl_test");
		templateTestSteps.forEach(tts -> {
			entityFactory.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, tts);
		});
		return templateTest;
	}

	private List<TemplateTestStep> createTemplateTestSteps(EntityFactory entityFactory,
			List<TemplateRoot> templateRoots) {
		// make sure each context type is given only once
		templateRoots.stream().collect(Collectors.toMap(TemplateRoot::getContextType, Function.identity()));

		List<TemplateTestStep> templateTestSteps = new ArrayList<>();
		TemplateTestStep templateTestStep1 = entityFactory.createTemplateTestStep("tpl_test_step_1");
		templateRoots.forEach(tr -> templateTestStep1.setTemplateRoot(tr));
		templateTestSteps.add(templateTestStep1);
		TemplateTestStep templateTestStep2 = entityFactory.createTemplateTestStep("tpl_test_step_2");
		templateRoots.forEach(tr -> templateTestStep2.setTemplateRoot(tr));
		templateTestSteps.add(templateTestStep2);

		return templateTestSteps;
	}

	private List<TemplateRoot> createTemplateRoots(EntityFactory entityFactory,
			List<CatalogComponent> catalogComponents) {
		Map<ContextType, List<CatalogComponent>> groups = catalogComponents.stream()
				.collect(Collectors.groupingBy(CatalogComponent::getContextType));

		List<TemplateRoot> templateRoots = new ArrayList<>();
		groups.forEach((contextType, catalogComps) -> {
			TemplateRoot templateRoot = entityFactory.createTemplateRoot(contextType,
					"tpl_" + toLower(contextType.name()) + "_root");
			// create child template components for template root
			catalogComps.forEach(catalogComp -> {
				TemplateComponent templateComponent = entityFactory
						.createTemplateComponent("tpl_" + catalogComp.getName() + "_parent", templateRoot, catalogComp);
				entityFactory.createTemplateComponent("tpl_" + catalogComp.getName() + "_child", templateComponent,
						catalogComp);
			});

			templateRoots.add(templateRoot);
		});

		return templateRoots;
	}

	private List<CatalogComponent> createCatalogComponents(ApplicationContext context) {

		// TODO create catalog sensors
		// TODO test change of unit of a catalog attribute
		List<CatalogComponent> catalogComponents = new ArrayList<>();
		catalogComponents.add(createCatalogComponent(context, ContextType.UNITUNDERTEST));
		catalogComponents.add(createCatalogComponent(context, ContextType.TESTSEQUENCE));
		catalogComponents.add(createCatalogComponent(context, ContextType.TESTEQUIPMENT));
		return catalogComponents;
	}

	private CatalogComponent createCatalogComponent(ApplicationContext context, ContextType contextType) {
		EntityFactory entityFactory = context.getEntityFactory()
				.orElseThrow(() -> new IllegalStateException("Entity manager factory not available."));

		CatalogComponent catalogComponent = entityFactory.createCatalogComponent(contextType,
				"Cat_" + toLower(contextType.name()));

		EnumRegistry er = EnumRegistry.getInstance();

		entityFactory.createCatalogAttribute("string", ValueType.STRING, catalogComponent);
		entityFactory.createCatalogAttribute("date", ValueType.DATE, catalogComponent);
		entityFactory.createCatalogAttribute("boolean", ValueType.BOOLEAN, catalogComponent);
		entityFactory.createCatalogAttribute("byte", ValueType.BYTE, catalogComponent);
		entityFactory.createCatalogAttribute("short", ValueType.SHORT, catalogComponent);
		entityFactory.createCatalogAttribute("int", ValueType.INTEGER, catalogComponent);
		entityFactory.createCatalogAttribute("long", ValueType.LONG, catalogComponent);
		entityFactory.createCatalogAttribute("float", ValueType.FLOAT, catalogComponent);
		entityFactory.createCatalogAttribute("double", ValueType.DOUBLE, catalogComponent);
		entityFactory.createCatalogAttribute("float_complex", ValueType.FLOAT_COMPLEX, catalogComponent);
		entityFactory.createCatalogAttribute("double_complex", ValueType.DOUBLE_COMPLEX, catalogComponent);
		entityFactory.createCatalogAttribute("file_link", ValueType.FILE_LINK, catalogComponent);
		entityFactory.createCatalogAttribute("scalar_type", ValueType.ENUMERATION,
				er.get(context.getSourceName(), EnumRegistry.SCALAR_TYPE), catalogComponent);

		entityFactory.createCatalogAttribute("string_array", ValueType.STRING_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("date_array", ValueType.DATE_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("boolean_array", ValueType.BOOLEAN_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("byte_array", ValueType.BYTE_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("short_array", ValueType.SHORT_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("int_array", ValueType.INTEGER_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("long_array", ValueType.LONG_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("float_array", ValueType.FLOAT_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("double_array", ValueType.DOUBLE_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("float_complex_array", ValueType.FLOAT_COMPLEX_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("double_complex_array", ValueType.DOUBLE_COMPLEX_SEQUENCE,
				catalogComponent);
		entityFactory.createCatalogAttribute("file_link_array", ValueType.FILE_LINK_SEQUENCE, catalogComponent);
		entityFactory.createCatalogAttribute("scalar_type_array", ValueType.ENUMERATION_SEQUENCE,
				er.get(context.getSourceName(), EnumRegistry.SCALAR_TYPE), catalogComponent);

		if (hasDescriptiveFile(context)) {
			// only supported on application models with version >= 5.1.0 which have
			// DescriptiveFile
			entityFactory.createCatalogAttribute("file_relation", ValueType.FILE_RELATION, catalogComponent);
		}

		// BLOB, BYTE_STREAM and BYTE_STREAM_SEQUENCE not supported

		return catalogComponent;
	}

	boolean hasDescriptiveFile(ApplicationContext context) {
		try {
			context.getModelManager().get().getEntityType(DescriptiveFile.class);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private static String toLower(String name) {
		return name.toLowerCase(Locale.ROOT);
	}

}
