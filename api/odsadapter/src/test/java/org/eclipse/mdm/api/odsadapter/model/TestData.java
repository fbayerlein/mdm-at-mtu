package org.eclipse.mdm.api.odsadapter.model;

public class TestData {

	private String name;
	private TestStepData[] testSteps;

	public TestData(String name, TestStepData[] testSteps) {
		this.name = name;
		this.testSteps = testSteps;
	}

	public String getName() {
		return name;
	}

	public TestStepData[] getTestSteps() {
		return testSteps;
	}
}
