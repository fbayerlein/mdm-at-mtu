/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.UpdateRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.LocalColumn;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateAttribute;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.ODSCorbaContext;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.corba.CorbaCatalogManager;
import org.eclipse.mdm.api.odsadapter.transaction.http.HttpCatalogManager;
import org.eclipse.mdm.api.odsadapter.utils.ODSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ODS implementation of the {@link Transaction} interface.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public final class ODSTransaction implements Transaction {

	// TODO: it should be possible to a attach a progress listener
	// -> progress notification updates while uploading files
	// -> any other useful informations?!
	// -> splitting of tasks into subtasks may be required...

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSTransaction.class);

	// this one is stored in case of application model modifications
	private final ODSContext parentContext;

	// this one is used to access the application model and execute queries
	// instance is decoupled from its parent
	private final ODSContext context;
	private final OdsTransactionHelper contextHelper;

	// only for logging
	private final String id = UUID.randomUUID().toString();

	// need to write version == instanceID -> update after create
	private final List<ContextRoot> contextRoots = new ArrayList<>();

	// reset instance IDs on abort
	private final List<Core> created = new ArrayList<>();

	// apply changes
	private final List<Core> modified = new ArrayList<>();

	private final Entity entity;

	private UploadService uploadService;

	private CatalogManager catalogManager;

	private boolean isCommited = false;
	private boolean isAborted = false;

	/**
	 * Constructor.
	 *
	 * @param parentModelManager Used to access the persistence.
	 * @param entity             Used for security checks
	 * @throws OdsException Thrown if unable to start a co-session.
	 */
	public ODSTransaction(ODSContext parentContext, Entity entity) throws OdsException {
		this.parentContext = parentContext;
		this.entity = entity;
		context = parentContext.newContext();
		this.contextHelper = OdsTransactionHelper.get(context);
		this.contextHelper.startTransaction();
		LOGGER.debug("Transaction '{}' started.", id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T extends Entity> void create(Collection<T> entities) throws DataAccessException {
		if (entities.isEmpty()) {
			return;
		} else if (entities.stream().filter(e -> ODSUtils.isValidID(e.getID())).findAny().isPresent()) {
			throw new IllegalArgumentException("At least one given entity is already persisted.");
		}

		try {
			Map<Class<?>, List<T>> entitiesByClassType = entities.stream()
					.collect(Collectors.groupingBy(e -> e.getClass()));

			getCatalogManager().createCatalog(entitiesByClassType);

			List<TemplateAttribute> templateAttributes = (List<TemplateAttribute>) entitiesByClassType
					.get(TemplateAttribute.class);
			if (templateAttributes != null) {
				List<TemplateAttribute> filtered = getFileLinkTemplateAttributes(templateAttributes);
				if (!filtered.isEmpty()) {
					getUploadService().upload(filtered, this, null);
				}
			}

			List<TestStep> testSteps = (List<TestStep>) entitiesByClassType.get(TestStep.class);
			if (testSteps != null) {
				create(testSteps.stream().map(ContextRoot::of).collect(ArrayList::new, List::addAll, List::addAll));
			}

			List<Measurement> measurements = (List<Measurement>) entitiesByClassType.get(Measurement.class);
			if (measurements != null) {
				// Use set here, since measurement siblings point to the same
				// context roots. Only create those ContextRoots that haven't been created yet:
				create(measurements.stream().map(ContextRoot::of)
						.collect(HashSet<ContextRoot>::new, Set<ContextRoot>::addAll, Set<ContextRoot>::addAll).stream()
						.filter(cr -> !ODSUtils.isValidID(cr.getID())).collect(Collectors.toSet()));
			}

			executeStatements(et -> contextHelper.getInsertStatement(this, et), entities);

//			List<ExtCompFile> extCompFiles = (List<ExtCompFile>) entitiesByClassType.get(ExtCompFile.class);
//			if (extCompFiles != null) {
//				upload(extCompFiles, null);
//			}

			contextHelper.processNtoMRelations(entities);

			List<ContextRoot> roots = (List<ContextRoot>) entitiesByClassType.get(ContextRoot.class);
			if (roots != null) {
				roots.forEach(contextRoot -> {
					contextRoot.setVersion(contextRoot.getID().toString());
				});

				// this will restore the ASAM path of each context root
				executeStatements(et -> contextHelper.getUpdateStatement(this, et, true), roots);

				contextRoots.addAll(roots);
			}
		} catch (IOException | OdsException e) {
			throw new DataAccessException("Unable to write new entities due to: " + e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T extends Entity> void update(Collection<T> entities) throws DataAccessException {
		if (entities.isEmpty()) {
			return;
		} else if (entities.stream().filter(e -> !ODSUtils.isValidID(e.getID())).findAny().isPresent()) {
			throw new IllegalArgumentException("At least one given entity is not yet persisted.");
		}

		try {
			Map<Class<?>, List<T>> entitiesByClassType = entities.stream()
					.collect(Collectors.groupingBy(e -> e.getClass()));
			List<CatalogAttribute> catalogAttributes = (List<CatalogAttribute>) entitiesByClassType
					.get(CatalogAttribute.class);
			if (catalogAttributes != null) {
				getCatalogManager().updateCatalogAttributes(catalogAttributes);
			}

			List<TemplateAttribute> templateAttributes = (List<TemplateAttribute>) entitiesByClassType
					.get(TemplateAttribute.class);
			if (templateAttributes != null) {
				List<TemplateAttribute> filtered = getFileLinkTemplateAttributes(templateAttributes);
				if (!filtered.isEmpty()) {
					getUploadService().upload(filtered, this, null);
				}
			}

			List<T> additionEntities = new ArrayList<>();
			additionEntities.addAll(entities);

			List<Channel> channels = (List<Channel>) entitiesByClassType.get(Channel.class);
			if (channels != null && !channels.isEmpty()) {
				additionEntities.addAll(renameCorrespondingLocalColumns(channels));
			}

			executeStatements(et -> contextHelper.getUpdateStatement(this, et, false), additionEntities);

			contextHelper.processNtoMRelations(entities);
		} catch (IOException | OdsException e) {
			throw new DataAccessException("Unable to update entities due to: " + e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends Deletable> void delete(Collection<T> entities) throws DataAccessException {
		if (entities.isEmpty()) {
			return;
		}

		List<T> filteredEntities = entities.stream().filter(e -> ODSUtils.isValidID(e.getID()))
				.collect(Collectors.toList());

		try {
			getCatalogManager().deleteCatalog(filteredEntities);

			/*
			 * TODO: for any template that has to be deleted it is required to ensure there
			 * are no links to it...
			 */

			executeStatements(et -> contextHelper.getDeleteStatement(this, et, true), filteredEntities);

		} catch (IOException | OdsException e) {
			throw new DataAccessException("Unable to delete entities due to: " + e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeMeasuredValues(Collection<WriteRequest> writeRequests) throws DataAccessException {
		if (writeRequests.isEmpty()) {
			return;
		}

		try {
			// Delete existing LocalColumns
			delete(loadExistingLocalColumns(writeRequests));

			Map<ScalarType, List<WriteRequest>> writeRequestsByRawType = writeRequests.stream()
					.collect(Collectors.groupingBy(WriteRequest::getRawScalarType));

			for (List<WriteRequest> writeRequestGroup : writeRequestsByRawType.values()) {
				WriteRequestHandler writeRequestHandler = new WriteRequestHandler(this);
				List<Channel> channels = new ArrayList<>();

				for (WriteRequest writeRequest : writeRequestGroup) {
					Channel channel = writeRequest.getChannel();
					channel.setScalarType(writeRequest.getCalculatedScalarType());
					// TODO it might be required to change relation to another
					// unit?!??
					channels.add(channel);
					writeRequestHandler.addRequest(writeRequest);
				}

				update(channels);
				writeRequestHandler.execute();
			}
		} catch (IOException | OdsException e) {
			throw new DataAccessException("Unable to write measured values due to: " + e.getMessage(), e);
		}
	}

	@Override
	public void appendMeasuredValues(Collection<WriteRequest> writeRequests) throws DataAccessException {
		if (writeRequests.isEmpty()) {
			return;
		}

		try {
			AppendMeasuredValuesHandler appendHandler = new AppendMeasuredValuesHandler(this);

			for (WriteRequest writeRequest : writeRequests) {
				appendHandler.addRequest(writeRequest);
			}

			appendHandler.execute();
		} catch (IOException | OdsException e) {
			throw new DataAccessException("Unable to write measured values due to: " + e.getMessage(), e);
		}

	}

	@Override
	public void updateMeasuredValues(Collection<UpdateRequest> updateRequests) throws DataAccessException {
		if (updateRequests.isEmpty()) {
			return;
		}

		try {
			UpdateRequestHandler updateHandler = new UpdateRequestHandler(this);

			for (UpdateRequest updateRequest : updateRequests) {

				updateHandler.addRequest(updateRequest);
			}

			updateHandler.execute();
		} catch (IOException | OdsException e) {
			throw new DataAccessException("Unable to write measured values due to: " + e.getMessage(), e);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void commit() throws DataAccessException {
		try {
			contextHelper.commitTransaction();

			// commit succeed -> apply changes in entity cores
			modified.forEach(Core::apply);

			// remove deleted remote files
			if (uploadService != null) {
				uploadService.commit(this);
			}

			if (catalogManager != null) {
				// application model has been modified -> reload
				parentContext.getODSModelManager().reloadApplicationModel();
			}

			LOGGER.debug("Transaction '{}' committed.", id);
			isCommited = true;
		} catch (OdsException e) {
			throw new DataAccessException("Unable to commit transaction '" + id + "' due to: " + e.getMessage(), e);
		} finally {
			closeSession();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void abort() {
		try {
			if (uploadService != null) {
				uploadService.abort(this);
			}

			// reset version, since creation failed or was aborted
			contextRoots.forEach(cr -> cr.setVersion(null));

			// reset instance IDs
			String virtualID = "0";
			created.forEach(c -> c.setID(virtualID));

			contextHelper.abortTransaction();

			LOGGER.debug("Transaction '{}' aborted.", id);
			isAborted = true;
		} catch (OdsException e) {
			LOGGER.error("Unable to abort transaction '" + id + "' due to: " + e.getMessage(), e);
		} finally {
			closeSession();
		}
	}

	/**
	 * Once {@link #abort()} is called instance ID of given {@link Core} will be
	 * reset to {@code 0} which indicates a virtual {@link Entity}, not yet
	 * persisted, entity.
	 *
	 * @param core The {@code Core} of a newly written {@code Entity}.
	 */
	void addCreated(Core core) {
		created.add(core);
	}

	/**
	 * Once {@link #commit()} is {@link Core#apply()} will be called to apply
	 * modified {@link Value} contents and removed related entities.
	 *
	 * @param core The {@code Core} of an updated {@code Entity}.
	 */
	void addModified(Core core) {
		modified.add(core);
	}

	/**
	 * Returns the {@link ODSContext}.
	 *
	 * @return The {@code ODSContext} is returned.
	 */
	public ODSContext getContext() {
		return context;
	}

	/**
	 * Returns the {@link ODSCorbaModelManager}.
	 *
	 * @return The {@code ODSModelManager} is returned.
	 */
	public ODSModelManager getModelManager() {
		return context.getODSModelManager();
	}

	/**
	 * Returns the {@link UploadService}.
	 *
	 * @return The {@code UploadService} is returned.
	 * @throws DataAccessException Thrown if file transfer is not possible.
	 */
	public UploadService getUploadService() throws DataAccessException {
		if (uploadService == null) {
			// upload service starts a periodic session refresh task -> lazy
			// instantiation
			uploadService = new UploadService(context, entity);
		}

		return uploadService;
	}

	/**
	 * Returns the {@link CorbaCatalogManager}.
	 *
	 * @return The {@code CatalogManager} is returned.
	 */
	private CatalogManager getCatalogManager() {
		if (catalogManager == null) {
			if (context instanceof ODSCorbaContext) {
				catalogManager = new CorbaCatalogManager(this);
			} else {
				catalogManager = new HttpCatalogManager(this);
			}
		}

		return catalogManager;
	}

	/**
	 * Collects {@link TemplateAttribute}s with a valid default {@link Value} of
	 * type {@link ValueType#FILE_LINK} or {@link ValueType#FILE_LINK_SEQUENCE}.
	 *
	 * @param templateAttributes The processed {@code TemplateAttribute}s.
	 * @return Returns {@link TemplateAttribute} which have {@link FileLink}s stored
	 *         as default {@code Value}.
	 */
	private List<TemplateAttribute> getFileLinkTemplateAttributes(List<TemplateAttribute> templateAttributes) {
		return templateAttributes.stream().filter(ta -> {
			Value value = ta.getDefaultValue();
			return value.getValueType().isFileLinkType() && value.isValid();
		}).collect(Collectors.toList());
	}

	/**
	 * Executes statements for given entities by using given statement factory.
	 *
	 * @param <T>              The entity type.
	 * @param statementFactory Used to create a new statement for a given
	 *                         {@link EntityType}.
	 * @param entities         The processed {@code Entity}s.
	 * @throws OdsException        Thrown if the execution fails.
	 * @throws DataAccessException Thrown if the execution fails.
	 * @throws IOException         Thrown if a file transfer operation fails.
	 */
	private <T extends Entity> void executeStatements(Function<EntityType, BaseStatement> statementFactory,
			Collection<T> entities) throws OdsException, DataAccessException, IOException {
		Map<EntityType, List<Entity>> entitiesByType = entities.stream()
				.collect(Collectors.groupingBy(context.getODSModelManager()::getEntityType));
		for (Entry<EntityType, List<Entity>> entry : entitiesByType.entrySet()) {
			statementFactory.apply(entry.getKey()).execute(entry.getValue());
		}
	}

	/**
	 * Channels (MeaQuantities) and their associated LocalColumns must have the same
	 * Name. This method checks if the Name attribute a the given channels has
	 * changed and if so, the related LocalColumn's Name will also be updated.
	 * 
	 * @param <T>
	 * @param channels
	 * @return modified LocalColumns
	 */
	@SuppressWarnings("unchecked")
	private <T extends Entity> List<T> renameCorrespondingLocalColumns(List<Channel> channels) {
		List<T> renamedLocalColumns = new ArrayList<>();

		// create a map with all Channels with changed names indexed by their IDs
		Map<String, Channel> channelsWithChangedNames = channels.stream()
				.filter(c -> c.getValue(BaseEntity.ATTR_NAME).isModified()).collect(Collectors
						.groupingBy(Channel::getID, Collectors.reducing(null, (c1, c2) -> c1 == null ? c2 : c1)));

		if (channelsWithChangedNames.isEmpty()) {
			return Collections.emptyList();
		}

		// Query the IDs of the Channels and their LocalColumns
		EntityType channelEt = getModelManager().getEntityType(Channel.class);
		EntityType localColumnEt = getModelManager().getEntityType(LocalColumn.class);
		Relation lc2MqRelation = localColumnEt.getRelation(channelEt, "MeaQuantity");

		Filter filter = Filter.and().add(ComparisonOperator.IN_SET.create(lc2MqRelation.getAttribute(),
				channelsWithChangedNames.keySet().toArray(new String[0])));

		List<Result> results = context.getQueryService().get().createQuery()
				.select(channelEt.getIDAttribute(), localColumnEt.getIDAttribute(), lc2MqRelation.getAttribute())
				.fetch(filter);

		// Create a map to lookup Channels by their LocalColumn ID
		Map<String, Channel> localColumn2Channel = new HashMap<>();
		for (Result r : results) {
			String channelId = r.getRecord(channelEt).getID();
			String lcId = r.getRecord(localColumnEt).getID();
			Channel channel = channelsWithChangedNames.get(channelId);
			if (channel == null) {
				throw new DataAccessException("No Channel found for with ID " + channelId);
			}
			localColumn2Channel.put(lcId, channel);
		}

		// Load all necessary LocalColumns and change their names to the name of the
		// corresponding Channel
		List<LocalColumn> lcs = context.getEntityManager().get().loadAll(LocalColumn.class, filter);
		for (LocalColumn lc : lcs) {
			Channel channel = localColumn2Channel.get(lc.getID());
			if (channel == null) {
				throw new DataAccessException("No Channel found for LocalColumn ID " + lc.getID());
			}
			lc.setName(channel.getName());
			renamedLocalColumns.add((T) lc);
		}
		return renamedLocalColumns;
	}

	/**
	 * Loads all existing LocalColumns, that will be replaced the given
	 * {@link WriteRequest}s.
	 * 
	 * @param writeRequests
	 * @return List with {@link LocalColumn}s affected by the given
	 *         {@link WriteRequest}s.
	 */
	public List<LocalColumn> loadExistingLocalColumns(Collection<WriteRequest> writeRequests) {
		EntityManager em = this.context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));

		Map<ChannelGroup, List<WriteRequest>> writeRequestsByChannelGroup = writeRequests.stream()
				.collect(Collectors.groupingBy(WriteRequest::getChannelGroup));

		EntityType channelEt = getModelManager().getEntityType(Channel.class);
		EntityType channelGroupEt = getModelManager().getEntityType(ChannelGroup.class);
		EntityType localColumnEt = getModelManager().getEntityType(LocalColumn.class);
		Relation lc2MqRelation = localColumnEt.getRelation(channelEt, "MeaQuantity");
		Relation lc2SmRelation = localColumnEt.getRelation(channelGroupEt, "SubMatrix");

		List<LocalColumn> localColumns = new ArrayList<>();
		for (Entry<ChannelGroup, List<WriteRequest>> writeRequestGroup : writeRequestsByChannelGroup.entrySet()) {

			String[] channelIds = writeRequestGroup.getValue().stream().map(WriteRequest::getChannel)
					.map(Channel::getID).toArray(String[]::new);

			if (channelIds.length > 0) {
				Filter filter = Filter.and()
						.add(ComparisonOperator.IN_SET.create(lc2MqRelation.getAttribute(), channelIds))
						.add(ComparisonOperator.EQUAL.create(lc2SmRelation.getAttribute(),
								writeRequestGroup.getKey().getID()));
				localColumns.addAll(em.loadAll(LocalColumn.class, filter));
			}
		}
		return localColumns;
	}

	/**
	 * Loads all existing LocalColumn IDs, that will be effected by the given
	 * {@link WriteRequest}s.
	 * 
	 * @param writeRequests
	 * @return Map of Measurement Quantity ID to LocalColumn ID affected by the
	 *         given {@link WriteRequest}s.
	 */
	public Map<String, String> loadMappingToLocalColumnIds(Collection<WriteRequest> writeRequests) {
		QueryService queryService = this.context.getQueryService()
				.orElseThrow(() -> new ServiceNotProvidedException(QueryService.class));

		Map<ChannelGroup, List<WriteRequest>> writeRequestsByChannelGroup = writeRequests.stream()
				.collect(Collectors.groupingBy(WriteRequest::getChannelGroup));

		EntityType channelEt = getModelManager().getEntityType(Channel.class);
		EntityType channelGroupEt = getModelManager().getEntityType(ChannelGroup.class);
		EntityType localColumnEt = getModelManager().getEntityType(LocalColumn.class);
		Relation lc2MqRelation = localColumnEt.getRelation(channelEt, "MeaQuantity");
		Relation lc2SmRelation = localColumnEt.getRelation(channelGroupEt, "SubMatrix");

		Map<String, String> map = new HashMap<>();
		for (Entry<ChannelGroup, List<WriteRequest>> writeRequestGroup : writeRequestsByChannelGroup.entrySet()) {
			String[] channelIds = writeRequestGroup.getValue().stream().map(WriteRequest::getChannel)
					.map(Channel::getID).toArray(String[]::new);

			if (channelIds.length > 0) {
				Filter filter = Filter.and()
						.add(ComparisonOperator.IN_SET.create(lc2MqRelation.getAttribute(), channelIds))
						.add(ComparisonOperator.EQUAL.create(lc2SmRelation.getAttribute(),
								writeRequestGroup.getKey().getID()));

				List<Result> results = queryService.createQuery()
						.select(localColumnEt, LocalColumn.ATTR_ID, lc2MqRelation.getName()).fetch(filter);

				for (Result result : results) {
					Record r = result.getRecord(localColumnEt);
					String meaQuantityId = r.getValues().get(lc2MqRelation.getName()).extract(ValueType.STRING);
					String localColumnId = r.getID();
					map.put(meaQuantityId, localColumnId);
				}
			}
		}
		return map;
	}

	@Override
	public void close() {
		if (!isCommited && !isAborted) {
			LOGGER.debug("Aborting transaction '{}' before closing.", id);
			abort();
		}
	}

	/**
	 * Closes the co-session of this transaction.
	 */
	private void closeSession() {
		if (catalogManager != null) {
			catalogManager.clear();
		}
		if (uploadService != null) {
			uploadService.close();
		}

		context.close();
		LOGGER.debug("Transaction '{}' closed.", id);
	}

	@Override
	public FileService getFileService(FileServiceType fileServiceType) {
		return context.getFileService(fileServiceType).get();
	}
}
