/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.http;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Booleans;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Longs;

import ods.Ods.DataMatrices;
import ods.Ods.DataMatrix;
import ods.Ods.DataMatrix.Column;
import ods.Ods.DataMatrix.Column.DoubleArrays;
import ods.Ods.DataMatrix.Column.LongArrays;
import ods.Ods.DataMatrix.Column.UnknownArray;
import ods.Ods.DataMatrix.Column.UnknownArrays;
import ods.Ods.DataMatrix.UpdateModeEnum;
import ods.Ods.DoubleArray;
import ods.Ods.LongArray;
import ods.Ods.LonglongArray;

/**
 * Appends values to existing measured values.
 *
 * @author Alexander Knoblauch, Peak Solution GmbH
 */
public final class HttpAppendMeasuredValuesHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpAppendMeasuredValuesHandler.class);

	private final ODSEntityType localColumnEntityType;
	private final ODSTransaction transaction;

	/**
	 * Constructor.
	 *
	 * @param transaction The owning {@link ODSTransaction}.
	 */
	public HttpAppendMeasuredValuesHandler(ODSTransaction transaction) {
		this.transaction = transaction;
		localColumnEntityType = (ODSEntityType) transaction.getModelManager().getEntityType("LocalColumn");
	}

	public void append(String channelGroupId, List<WriteRequest> writeRequests, Map<String, String> mq2LcMapping) {
		try {
			List<Column> valuesToAppend = convertToColumns(channelGroupId, writeRequests, mq2LcMapping);
			appendToChannelGroup(channelGroupId, valuesToAppend);
		} catch (OdsException e) {
			throw new DataAccessException(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Getting all values to append as {@link Column} for the specific channelgroup
	 * id
	 * 
	 * @param channelGroupId unique id of a channelgroup
	 * @param writeRequests  {@link WriteRequest}s
	 * @return
	 */
	private List<Column> convertToColumns(String channelGroupId, List<WriteRequest> writeRequests,
			Map<String, String> mq2LcMapping) {

		int maxLength = 0;
		int[] length = new int[writeRequests.size()];
		for (int i = 0; i < writeRequests.size(); i++) {
			WriteRequest wr = writeRequests.get(i);
			length[i] = ODSHttpConverter.getLength(wr);
			maxLength = Math.max(maxLength, length[i]);
		}

		long[] ids = new long[writeRequests.size()];
		UnknownArray[] values = new UnknownArray[writeRequests.size()];

		LongArray[] flags = new LongArray[writeRequests.size()];
		boolean[] flagsIsNull = new boolean[writeRequests.size()];

		DoubleArray[] generationParameters = new DoubleArray[writeRequests.size()];
		boolean[] generationParametersIsNull = new boolean[writeRequests.size()];

		List<Pair<Column, Integer>> columns = new ArrayList<>();

		for (int i = 0; i < writeRequests.size(); i++) {
			WriteRequest wr = writeRequests.get(i);

			ids[i] = ODSHttpConverter.toODSID(mq2LcMapping.get(wr.getChannel().getID()));
			if (ids[i] == 0) {
				throw new DataAccessException(
						"No LocalColumn found for Channel with ID " + wr.getChannel().getID() + ".");
			}
			values[i] = ODSHttpConverter.writeRequestToUnknownArray(wr, maxLength);

			if (wr.areAllValid() || wr.getFlags() == null) {
				flagsIsNull[i] = true;
				flags[i] = LongArray.getDefaultInstance();
			} else {
				flagsIsNull[i] = false;

				LongArray.Builder builder = LongArray.newBuilder();
				for (short flag : wr.getFlags()) {
					builder.addValues(flag);
				}
				for (int j = wr.getFlags().length; j < maxLength; j++) {
					builder.addValues(0);
				}
				flags[i] = builder.build();
			}

			if (wr.getGenerationParameters() == null || wr.getGenerationParameters().length == 0) {
				generationParametersIsNull[i] = true;
				generationParameters[i] = DoubleArray.getDefaultInstance();
			} else {
				generationParametersIsNull[i] = false;
				generationParameters[i] = DoubleArray.newBuilder()
						.addAllValues(Doubles.asList(wr.getGenerationParameters())).build();
			}
		}

		// Load all Channels a add missing Columns
		EntityManager em = transaction.getContext().getEntityManager().get();
		ChannelGroup channelGroup = em.load(ChannelGroup.class, channelGroupId);

		List<Channel> channels = em.loadChildren(channelGroup, Channel.class);

		for (Channel c : channels) {
			if (!isChannelInColumnList(columns, c)) {
				columns.add(ImmutablePair.of(ODSHttpConverter.channelToColumn(c, null), 0));
			}
		}

		List<Column> cols = new ArrayList<>();

		cols.add(Column.newBuilder().setName("Id")
				.setLonglongArray(LonglongArray.newBuilder().addAllValues(Longs.asList(ids))).build());

		cols.add(Column.newBuilder().setName("Values")
				.setUnknownArrays(UnknownArrays.newBuilder().addAllValues(Arrays.asList(values))).build());

		cols.add(Column.newBuilder().setName("Flags")
				.setLongArrays(LongArrays.newBuilder().addAllValues(Arrays.asList(flags)))
				.addAllIsNull(Booleans.asList(flagsIsNull)).build());

		cols.add(Column.newBuilder().setName("GenerationParameters")
				.setDoubleArrays(DoubleArrays.newBuilder().addAllValues(Arrays.asList(generationParameters)))
				.addAllIsNull(Booleans.asList(generationParametersIsNull)).build());

		return cols;
	}

	/**
	 * Check if a {@link Column} for the channel exists in the list
	 * 
	 * @param columns
	 * @param channel
	 * @return true if exists, otherwise false
	 */
	private boolean isChannelInColumnList(List<Pair<Column, Integer>> columns, Channel channel) {

		String channelName = channel.getName();

		for (Pair<Column, Integer> column : columns) {
			if (column.getLeft().getName().equals(channelName)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Appending the ValuesToAppend to the value matrix
	 * 
	 * @param channelGroupId
	 * @param valuesToAppend
	 * @throws OdsException
	 */
	private void appendToChannelGroup(String channelGroupId, List<Column> valuesToAppend) throws OdsException {
		DataMatrices dms = DataMatrices.newBuilder()
				.addMatrices(DataMatrix.newBuilder().setAid(localColumnEntityType.getODSID())
						.addAllColumns(valuesToAppend).setValuesUpdateMode(UpdateModeEnum.VU_APPEND))
				.build();

		long start = System.currentTimeMillis();
		ODSHttpModelManager mm = ((ODSHttpModelManager) transaction.getContext().getODSModelManager());

		OdsHttpRequests.request(mm.getSession(), "data-update", dms, DataMatrices.class);

		long stop = System.currentTimeMillis();

		LOGGER.debug("Appended values to {} columns in {} ms.", valuesToAppend, stop - start);
	}
}