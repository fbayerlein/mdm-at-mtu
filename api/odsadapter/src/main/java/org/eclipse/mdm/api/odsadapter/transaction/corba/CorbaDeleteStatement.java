/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.corba;

import java.util.Collection;

import org.asam.ods.AoException;
import org.asam.ods.ApplElemAccess;
import org.asam.ods.T_LONGLONG;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.DeleteStatement;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;

/**
 * Delete statement is used to delete entities with their children.
 *
 */
public final class CorbaDeleteStatement extends DeleteStatement {

	/**
	 * Constructor.
	 *
	 * @param transaction   The owning {@link ODSTransaction}.
	 * @param entityType    The associated {@link EntityType}.
	 * @param useAutoDelete If {@code true} child relations of {@link Measurement}
	 *                      entities are not followed.
	 */
	public CorbaDeleteStatement(ODSTransaction transaction, EntityType entityType, boolean useAutoDelete) {
		super(transaction, entityType, useAutoDelete);
	}

	@Override
	protected void executeDelete(EntityType entityType, Collection<String> instanceIDs) throws OdsException {
		try {
			ApplElemAccess applElemAccess = ((ODSCorbaModelManager) getTransaction().getContext().getODSModelManager())
					.getApplElemAccess();

			applElemAccess.deleteInstances(ODSConverter.toODSLong(((ODSEntityType) entityType).getODSID()),
					toODSIDs(instanceIDs));
		} catch (AoException e) {
			throw new OdsException(e.reason, e);
		}
	}

	/**
	 * Converts given {@code Collection} of instance IDs to ODS a {@link T_LONGLONG}
	 * array.
	 *
	 * @param instanceIDs The instance IDs.
	 * @return The corresponding ODS {@code T_LONGLONG[]} is returned.
	 */
	private T_LONGLONG[] toODSIDs(Collection<String> instanceIDs) {
		return instanceIDs.stream().map(ODSConverter::toODSID).toArray(T_LONGLONG[]::new);
	}

}
