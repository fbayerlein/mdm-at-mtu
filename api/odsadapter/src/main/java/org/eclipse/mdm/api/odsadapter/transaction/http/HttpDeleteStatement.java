/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.http;

import java.util.Collection;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.DeleteStatement;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Longs;

import ods.Ods.DataMatrices;
import ods.Ods.DataMatrix;
import ods.Ods.DataMatrix.Column;
import ods.Ods.LonglongArray;

/**
 * Delete statement is used to delete entities with their children.
 *
 */
public final class HttpDeleteStatement extends DeleteStatement {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpDeleteStatement.class);

	/**
	 * Constructor.
	 *
	 * @param transaction   The owning {@link ODSTransaction}.
	 * @param entityType    The associated {@link EntityType}.
	 * @param useAutoDelete If {@code true} child relations of {@link Measurement}
	 *                      entities are not followed.
	 */
	public HttpDeleteStatement(ODSTransaction transaction, EntityType entityType, boolean useAutoDelete) {
		super(transaction, entityType, useAutoDelete);
	}

	@Override
	protected void executeDelete(EntityType entityType, Collection<String> instanceIDs) throws OdsException {
		ODSEntityType et = ((ODSEntityType) entityType);

		DataMatrices dms = DataMatrices.newBuilder()
				.addMatrices(DataMatrix.newBuilder().setAid(et.getODSID()).addColumns(Column.newBuilder()
						.setName(et.getIDAttribute().getName()).setLonglongArray(toODSIDs(instanceIDs)).build()))
				.build();

		ODSHttpModelManager mm = ((ODSHttpModelManager) getTransaction().getContext().getODSModelManager());

		OdsHttpRequests.request(mm.getSession(), "data-delete", dms, DataMatrices.class);
	}

	/**
	 * Converts given {@code Collection} of instance IDs to ODS LonglongArray.
	 *
	 * @param instanceIDs The instance IDs.
	 * @return The corresponding ODS {@link LonglongArray} is returned.
	 */
	private LonglongArray toODSIDs(Collection<String> instanceIDs) {
		return LonglongArray.newBuilder()
				.addAllValues(Longs.asList(instanceIDs.stream().mapToLong(ODSHttpConverter::toODSID).toArray()))
				.build();
	}

}
