/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.corba;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.asam.ods.AoException;
import org.asam.ods.Column;
import org.asam.ods.ElemId;
import org.asam.ods.NameValueSeqUnit;
import org.asam.ods.T_LONGLONG;
import org.asam.ods.ValueMatrix;
import org.asam.ods.ValueMatrixMode;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.transaction.ColumnAttributes;
import org.eclipse.mdm.api.odsadapter.transaction.ReadRequestHandler;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reads mass data specified in {@link ReadRequest}s.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public final class CorbaReadRequestHandler extends ReadRequestHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(CorbaReadRequestHandler.class);

	/**
	 * Constructor.
	 *
	 * @param modelManager Used to gain access to value matrices.
	 * @param timeZone     TODO
	 */
	public CorbaReadRequestHandler(ODSCorbaModelManager modelManager, QueryService queryService) {
		super(modelManager, queryService);
	}

	protected List<MeasuredValues> loadByValueMatrix(ReadRequest readRequest,
			Map<String, ColumnAttributes> mapColumnAttributes) {
		if (mapColumnAttributes.isEmpty()) {
			return Collections.emptyList();
		}

		// if there are still localcolumns left we load them be valuematrix
		LOGGER.debug("Loading {} localcolumns by value matrix.", mapColumnAttributes.size());

		ValueMatrix valueMatrix = null;
		Column[] arrColumns = null;

		try {
			valueMatrix = getValueMatrix(readRequest);
			List<Pair<Column, ColumnAttributes>> listColumnPairs = getODSColumns(readRequest, valueMatrix,
					mapColumnAttributes);

			// Get array of all ODS columns for later release:
			arrColumns = listColumnPairs.stream().map(Pair::getLeft).toArray(Column[]::new);

			// Create MeasuredValues for the remaining local columns:
			NameValueSeqUnit[] nvsus = valueMatrix.getValue(arrColumns, readRequest.getStartIndex(),
					readRequest.getRequestSize());
			return ODSConverter.fromODSMeasuredValuesSeq(nvsus,
					listColumnPairs.stream().map(Pair::getRight).toArray(ColumnAttributes[]::new),
					modelManager.getTimeZone());
		} catch (AoException aoe) {
			throw new DataAccessException(aoe.reason, aoe);
		} finally {
			releaseColumns(arrColumns);
			releaseValueMatrix(valueMatrix);
		}
	}

	/**
	 * Gets the {@link Column}s from a {@link ValueMatrix} whose names are present
	 * in the [@link ColumnAttributes} of mapColumnAttributes.
	 * 
	 * @param readRequest
	 * @param valueMatrix
	 * @param mapColumnAttributes
	 * @return
	 * @throws AoException
	 * @throws DataAccessException
	 */
	private List<Pair<Column, ColumnAttributes>> getODSColumns(ReadRequest readRequest, ValueMatrix valueMatrix,
			Map<String, ColumnAttributes> mapColumnAttributes) throws AoException, DataAccessException {
		List<Pair<Column, ColumnAttributes>> listColumnPairs = new ArrayList<>();
		Map<String, Column> mapColumns = new HashMap<>();

		// We consciously refrain from using valueMatrix.getColumns("*") if
		// readRequest.isLoadAllChannels() == true
		// and mapColumnAttributes contains entries for all columns of the submatrix as
		// we would have to call
		// column.getName() for every column and this would actually result in n + 1
		// calls to the ODS source
		// rather than n (n: number of entries in mapColumnAttributes) in the loop
		// below:

		for (ColumnAttributes ca : mapColumnAttributes.values()) {
			String columnName = ca.getName();

			Column[] columns = valueMatrix.getColumns(columnName);
			if (columns == null || columns.length != 1) {
				List<Column> colsToRelease = new ArrayList<>();
				if (columns != null) {
					colsToRelease.addAll(Arrays.asList(columns));
				}
				colsToRelease.addAll(mapColumns.values());
				releaseColumns(colsToRelease.toArray(new Column[colsToRelease.size()]));
				throw new DataAccessException(
						String.format("Zero or more than one column with name '%s' found within submatrix ID %d!",
								columnName, Long.valueOf(readRequest.getChannelGroup().getID())));
			}

			Column column = columns[0];

			if (!readRequest.isLoadAllChannels()) {
				for (Map.Entry<Channel, Unit> me : readRequest.getChannels().entrySet().stream()
						.filter(e -> e.getKey().getName().equals(columnName)).collect(Collectors.toList())) {
					Channel channel = me.getKey();
					Unit unit = me.getValue();
					if (channel.getScalarType().isNumericalType() && unit != null
							&& !unit.nameEquals(channel.getUnit().getName())) {
						column.setUnit(unit.getName());
					}
				}
			}

			mapColumns.put(columnName, column);
		}

		if (mapColumns.size() > 0) {
			for (Map.Entry<String, ColumnAttributes> me : mapColumnAttributes.entrySet()) {
				ColumnAttributes ca = me.getValue();
				listColumnPairs.add(new ImmutablePair<Column, ColumnAttributes>(mapColumns.get(ca.getName()), ca));
			}
		}

		return listColumnPairs;
	}

	/**
	 * Returns the {@link ValueMatrix} CORBA service object associated with given
	 * {@link ReadRequest}.
	 *
	 * @param readRequest The {@code ReadRequest}.
	 * @return The {@code ValueMatrix} is returned.
	 * @throws AoException Thrown if unable to load the {@code ValueMatrix}.
	 */
	private ValueMatrix getValueMatrix(ReadRequest readRequest) throws AoException {
		Entity entity = readRequest.getChannelGroup();
		T_LONGLONG iid = ODSConverter.toODSID(entity.getID());
		T_LONGLONG aid = ODSConverter.toODSLong(((ODSEntityType) modelManager.getEntityType(entity)).getODSID());
		ValueMatrixMode valueMatrixMode = ValueMatrixMode.CALCULATED;
		switch (readRequest.getValuesMode()) {
		case CALCULATED:
			valueMatrixMode = ValueMatrixMode.CALCULATED;
			break;
		case STORAGE:
		case STORAGE_PRESERVE_EXTCOMPS:
			valueMatrixMode = ValueMatrixMode.STORAGE;
			break;
		default:
			throw new DataAccessException(
					String.format("Unsupported ValueMode %s!", readRequest.getValuesMode().name()));
		}

		return ((ODSCorbaModelManager) modelManager).getApplElemAccess().getValueMatrixInMode(new ElemId(aid, iid),
				valueMatrixMode);
	}

	/**
	 * Releases given {@link ValueMatrix} CORBA object.
	 *
	 * @param valueMatrix Will be released.
	 */
	private void releaseValueMatrix(ValueMatrix valueMatrix) {
		if (valueMatrix == null) {
			return;
		}

		try {
			valueMatrix.destroy();
		} catch (AoException aoe) {
			// ignore
		} finally {
			valueMatrix._release();
		}
	}

	/**
	 * Releases each CORBA {@link Column} object.
	 *
	 * @param columns Will be released.
	 */
	private void releaseColumns(Column[] columns) {
		if (columns == null) {
			return;
		}

		for (Column column : columns) {
			try {
				column.destroy();
			} catch (AoException e) {
				// ignore
			} catch (Exception e) {
				// ignore
			} finally {
				column._release();
			}
		}
	}

}
