/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.lookup;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextSensor;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.Query;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.model.TemplateAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateSensor;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig.Key;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;

/**
 * Recursively loads entities for a given {@link EntityConfig} with all resolved
 * dependencies (optional, mandatory children).
 *
 * @param <T> The entity type.
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public class EntityRequest<T extends Entity> {

	// ======================================================================
	// Instance variables
	// ======================================================================

	final ODSContext odsContext;
	final QueryService queryService;
	EntityConfig<T> entityConfig;
	final EntityResult<T> entityResult = new EntityResult<>(this);

	final Cache cache;

	boolean filtered;

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param modelManager The {@link ODSModelManager}.
	 * @param config       The {@link EntityConfig}.
	 */
	public EntityRequest(ODSContext odsContext, QueryService queryService, Key<T> key) {
		this.odsContext = odsContext;
		this.queryService = queryService;
		this.entityConfig = odsContext.getODSModelManager().getEntityConfig(key);
		cache = new Cache();
	}

	/**
	 * Constructor.
	 *
	 * @param modelManager The {@link ODSModelManager}.
	 * @param config       The {@link EntityConfig}.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public EntityRequest(ODSContext odsContext, QueryService queryService, Key<T> key, String compName) {
		this.odsContext = odsContext;
		this.queryService = queryService;
		Key<ContextRoot> keyRoot = new Key<>(ContextRoot.class, key.getContextType());
		EntityConfig<ContextRoot> entityConfigRoot = odsContext.getODSModelManager().getEntityConfig(keyRoot);

		for (EntityConfig ec : entityConfigRoot.getChildConfigs()) {
			if (ec.getEntityType().toString().equals(compName)) {
				this.entityConfig = ec;
				break;
			}
		}

		cache = new Cache();
	}

	/**
	 * Constructor.
	 *
	 * @param parentRequest The parent {@link EntityRequest}.
	 * @param entityConfig  The {@link EntityConfig}.
	 */
	protected EntityRequest(EntityRequest<?> parentRequest, EntityConfig<T> entityConfig) {
		odsContext = parentRequest.odsContext;
		queryService = parentRequest.queryService;
		cache = parentRequest.cache;
		this.entityConfig = entityConfig;
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Loads all entities matching given instance IDs.
	 *
	 * @param instanceIDs The instance IDs.
	 * @return A sorted {@link List} with queried entities is returned.
	 * @throws DataAccessException Thrown if unable to load entities.
	 */
	public List<T> load(Collection<String> instanceIDs) throws DataAccessException {
		return load(instanceIDs, true);
	}
	
	/**
	 * Loads all entities matching given instance IDs.
	 *
	 * @param instanceIDs The instance IDs.
	 * @param loadOptionalRelations if true optional relations will be loaded
	 * @return A sorted {@link List} with queried entities is returned.
	 * @throws DataAccessException Thrown if unable to load entities.
	 */
	public List<T> load(Collection<String> instanceIDs, boolean loadOptionalRelations) throws DataAccessException {
		if (instanceIDs.isEmpty()) {
			// just to be sure...
			return Collections.emptyList();
		}

		return load(Filter.idsOnly(entityConfig.getEntityType(), instanceIDs), loadOptionalRelations).getSortedEntities();
	}

	/**
	 * Loads all entities matching given name pattern.
	 *
	 * @param pattern Is always case sensitive and may contain wildcard characters
	 *                as follows: "?" for one matching character and "*" for a
	 *                sequence of matching characters.
	 * @return A sorted {@link List} with queried entities is returned.
	 * @throws DataAccessException Thrown if unable to load entities.
	 */
	public List<T> loadAll(String pattern) throws DataAccessException {
		return load(Filter.nameOnly(entityConfig.getEntityType(), pattern)).getSortedEntities();
	}

	public List<T> loadAllByIds(Collection<String> ids) throws DataAccessException {
		return load(Filter.idsOnly(entityConfig.getEntityType(), ids)).getSortedEntities();
	}

	/**
	 * Loads all entities matching one of the given names.
	 *
	 * @param names A list of names.
	 * @return A sorted {@link List} with queried entities is returned.
	 * @throws DataAccessException Thrown if unable to load entities.
	 */
	public List<T> loadAll(Collection<String> names) throws DataAccessException {
		if (names.isEmpty()) {
			// just to be sure...
			return Collections.emptyList();
		}
		return load(Filter.namesOnly(entityConfig.getEntityType(), names)).getSortedEntities();
	}

	/**
	 * Loads all entities matching given {@link Filter}.
	 *
	 * @param filter The {@link Filter}.
	 * @return A sorted {@link List} with queried entities is returned.
	 * @throws DataAccessException Thrown if unable to load entities.
	 */
	public List<T> loadAll(Filter filter) throws DataAccessException {
		return load(filter).getSortedEntities();
	}

	public ODSContext getODSContext() {
		return odsContext;
	}

	// ======================================================================
	// Protected methods
	// ======================================================================

	/**
	 * Adds foreign key select statements to given {@link Query} for each given
	 * {@link EntityConfig}.
	 *
	 * @param query          The {@link Query}.
	 * @param relatedConfigs The {@code EntityConfig}s.
	 * @param mandatory      Flag indicates whether given {@code EntityConfig}s are
	 *                       mandatory or not.
	 * @return For each {@code EntityConfig} a corresponding {@code
	 * 		RelationConfig} is returned in a {@code List}.
	 */
	protected List<RelationConfig> selectRelations(Query query, List<EntityConfig<?>> relatedConfigs,
			boolean mandatory) {
		List<RelationConfig> relationConfigs = new ArrayList<>();
		EntityType entityType = entityConfig.getEntityType();
		for (EntityConfig<?> relatedEntityConfig : relatedConfigs) {
			if (isDescriptiveFile(relatedEntityConfig.getEntityType())) {
				for (Relation r : entityType.getRelations(relatedEntityConfig.getEntityType())) {
					RelationConfig relationConfig = new RelationConfig(r, relatedEntityConfig, mandatory);
					// DescriptiveFile is NtoM, no select on attribute
					relationConfigs.add(relationConfig);
				}
			} else if (!entityType.getRelations(relatedEntityConfig.getEntityType()).isEmpty()
					&& (isRelationUnique(entityType, relatedEntityConfig.getEntityType())
							|| isPreferred(entityType.getRelation(relatedEntityConfig.getEntityType())))) { // TODO test
																											// if
																											// isPreferred
																											// is
																											// met
				RelationConfig relationConfig = new RelationConfig(entityType, relatedEntityConfig, mandatory);
				if (!relationConfig.relation.isNtoM()) {
					query.select(relationConfig.relation.getAttribute());
				}
				relationConfigs.add(relationConfig);
			}
		}

		return relationConfigs;
	}

	private boolean isPreferred(Relation relation) {
		return "MeaQuantity".equals(relation.getSource().getName()) && "Quantity".equals(relation.getTarget().getName())
				&& "Quantity".equals(relation.getName());
	}

	private boolean isDescriptiveFile(EntityType entityType) {
		return "DescriptiveFile".equals(entityType.getName());
	}

	private boolean isRelationUnique(EntityType source, EntityType target) {
		List<Relation> relations = new ArrayList<>();
		for (Relation r : source.getRelations()) {
			if (r.getTarget().equals(target)) {
				relations.add(r);
			}
		}
		return relations.size() <= 1;
	}

	/**
	 * Convenience method collects the queried {@link Record} from each
	 * {@link Result}.
	 *
	 * @param results The {@code Result}s.
	 * @return The queried {@link Record}s are returned.
	 */
	protected List<Record> collectRecords(List<Result> results) {
		return results.stream().map(r -> r.getRecord(entityConfig.getEntityType())).collect(toList());
	}

	/**
	 * Loads and maps related entities for each given {@link RelationConfig}.
	 *
	 * @param relationConfigs The {@code RelationConfig}s.
	 * @throws DataAccessException Thrown if unable to load related entities.
	 */
	protected void loadRelatedEntities(List<RelationConfig> relationConfigs) throws DataAccessException {
		for (RelationConfig relationConfig : relationConfigs) {
			EntityConfig<?> relatedConfig = relationConfig.entityConfig;

			boolean isContextTypeDefined = entityConfig.getContextType().isPresent();
			for (Entity relatedEntity : new EntityRequest<>(this, relatedConfig)
					.load(relationConfig.dependants.keySet())) {
				boolean setByContextType = !isContextTypeDefined && relatedConfig.getContextType().isPresent();
				for (EntityRecord<?> entityRecord : relationConfig.dependants.remove(relatedEntity.getID())) {
					setRelatedEntity(entityRecord, relatedEntity,
							setByContextType ? relatedConfig.getContextType().get() : null);
				}
			}

			if (!relationConfig.dependants.isEmpty()) {
				// this may occur if the instance id of the related entity
				// is defined, but the entity itself does not exist
				throw new IllegalStateException(
						"Unable to load related entities: " + relationConfig.dependants.toString());
			}
		}
	}

	/**
	 * Assigns given related {@link Entity} to given {@link EntityRecord}.
	 *
	 * @param entityRecord  The {@code EntityRecord} which references given
	 *                      {@code Entity}.
	 * @param relatedEntity The related {@code Entity}.
	 * @param contextType   Used as qualifier for relation assignment.
	 */
	protected void setRelatedEntity(EntityRecord<?> entityRecord, Entity relatedEntity, ContextType contextType) {
		if (contextType == null) {
			entityRecord.core.getMutableStore().set(relatedEntity);
		} else {
			entityRecord.core.getMutableStore().set(relatedEntity, contextType);
		}

		List<TemplateAttribute> templateAttributes = new ArrayList<>();
		if (entityRecord.entity instanceof ContextComponent && relatedEntity instanceof TemplateComponent) {
			templateAttributes.addAll(((TemplateComponent) relatedEntity).getTemplateAttributes());
		} else if (entityRecord.entity instanceof ContextSensor && relatedEntity instanceof TemplateSensor) {
			templateAttributes.addAll(((TemplateSensor) relatedEntity).getTemplateAttributes());
		}

		if (!templateAttributes.isEmpty()) {
			// hide Value containers that are missing in the template
			Set<String> names = new HashSet<>(entityRecord.core.getValues().keySet());
			names.remove(Entity.ATTR_NAME);
			names.remove(Entity.ATTR_MIMETYPE);
			templateAttributes.stream().map(Entity::getName).forEach(names::remove);
			// Don't hide FILE_RELATION values:
			entityRecord.core.getValues().values().stream()
					.filter(v -> ValueType.FILE_RELATION.equals(v.getValueType())).map(Value::getName)
					.forEach(names::remove);
			entityRecord.core.hideValues(names);
		}
	}

	// ======================================================================
	// Private methods
	// ======================================================================

	/**
	 * Loads all entities matching given {@link Filter} including all of related
	 * entities (optional, mandatory and children).
	 *
	 * @param filter The {@link Filter}.
	 * @return Returns the queried {@code EntityResult}.
	 * @throws DataAccessException Thrown if unable to load entities.
	 */
	private EntityResult<T> load(Filter filter) throws DataAccessException {
		return load(filter, true);
	}
	
	
	/**
	 * Loads all entities matching given {@link Filter} including all of related
	 * entities (optional, mandatory and children).
	 *
	 * @param filter The {@link Filter}.
	 * @param loadOptionalRelations if true optional relations will be loaded
	 * @return Returns the queried {@code EntityResult}.
	 * @throws DataAccessException Thrown if unable to load entities.
	 */
	private EntityResult<T> load(Filter filter, boolean loadOptionalRelations) throws DataAccessException {
		filtered = !filter.isEmtpty() || entityConfig.isReflexive();

		EntityType entityType = entityConfig.getEntityType();
		Relation reflexiveRelation = entityConfig.isReflexive() ? entityType.getRelation(entityType) : null;

		Query query = queryService.createQuery().selectAll(entityConfig.getEntityType());

		if (entityConfig.isReflexive()) {
			query.select(reflexiveRelation.getAttribute());
			// entities with children have to be processed before their
			// children!
			query.order(entityType.getIDAttribute());
		}

		// prepare relations select statements
		List<RelationConfig> optionalRelations = Collections.emptyList();
		
		if (loadOptionalRelations) {
			optionalRelations = selectRelations(query, entityConfig.getOptionalConfigs(), false);
		}
		List<RelationConfig> mandatoryRelations = selectRelations(query, entityConfig.getMandatoryConfigs(), true);

		// configure filter
		Filter adjustedFilter = Filter.or();
		if (filtered) {
			// preserve current conditions
			adjustedFilter.merge(filter);
			if (entityConfig.isReflexive()) {
				// extend to retrieve all reflexive child candidates
				adjustedFilter.add(ComparisonOperator.IS_NOT_NULL.create(reflexiveRelation.getAttribute(), 0L));
			}
		}

		// load entities and prepare mappings for required related entities
		List<EntityRecord<?>> parentRecords = new ArrayList<>();
		for (Record record : collectRecords(query.fetch(adjustedFilter))) {
			record.getEntityType().getRelations().stream()
					.filter(r -> r.isNtoM() && DescriptiveFile.TYPE_NAME.equals(r.getTarget().getName()))
					.forEach(r -> record.addValue(ValueType.FILE_LINK_SEQUENCE.create(r.getName(), new FileLink[0])));

			Optional<String> reflexiveParentID = Optional.empty();
			if (entityConfig.isReflexive()) {
				reflexiveParentID = record.getID(reflexiveRelation);
			}
			EntityRecord<T> entityRecord;

			if (entityConfig.isReflexive() && reflexiveParentID.isPresent()) {
				Optional<EntityRecord<T>> parentRecord = entityResult.get(reflexiveParentID.get());
				if (!parentRecord.isPresent()) {
					// this entity's parent was not loaded -> skip
					continue;
				}

				entityRecord = entityResult.add(parentRecord.get(), record);
				parentRecords.add(parentRecord.get());
			} else {
				entityRecord = entityResult.add(record);
			}

			// collect related instance IDs
			Stream.concat(optionalRelations.stream(), mandatoryRelations.stream()).filter(rc -> !rc.relation.isNtoM())
					.forEach(rc -> rc.add(entityRecord, record));
		}

		if (entityResult.isEmpty()) {
			// no entities found -> neither related nor child entities required
			return entityResult;
		}

		// load and map related entities
		loadRelatedEntities(optionalRelations);
		loadRelatedEntities(mandatoryRelations);

		// sort children of parent
		if (entityConfig.isReflexive()) {
			@SuppressWarnings("unchecked")
			EntityConfig<Deletable> childConfig = (EntityConfig<Deletable>) entityConfig;
			for (EntityRecord<?> entityRecord : parentRecords) {
				entityRecord.core.getChildrenStore().sort(childConfig.getEntityClass(), childConfig.getComparator());
			}
		}

		// load children
		for (EntityConfig<? extends Deletable> childConfig : entityConfig.getChildConfigs()) {
			cache.add(new ChildRequest<>(this, childConfig).load());
		}

		return entityResult;
	}

}
