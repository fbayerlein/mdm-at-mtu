/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.adapter.EntityStore;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.FilesAttachable;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.dflt.model.TemplateTestStepUsage;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityFactory;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.utils.ODSUtils;

import com.google.common.collect.ImmutableMap;

/**
 * Insert statement is used to update entities and their children.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public abstract class UpdateStatement extends BaseStatement {

	protected final Map<Class<? extends Entity>, List<Entity>> childrenToCreate = new HashMap<>();
	protected final Map<Class<? extends Entity>, List<Entity>> childrenToUpdate = new HashMap<>();
	protected final Map<Class<? extends Deletable>, List<Deletable>> childrenToRemove = new HashMap<>();
	protected Map<String, List<Value>> updateMap = new HashMap<>();

	protected Map<String, Predicate<Core>> updateParent = ImmutableMap.of("TestStep",
			core -> isParentUpdateAllowedTestStep(core));

	protected final List<FileLink> fileLinkToUpload = new ArrayList<>();
	protected final List<String> nonUpdatableRelationNames;
	protected final boolean ignoreChildren;
	protected final boolean isFilesAttachable;

	/**
	 * Constructor.
	 *
	 * @param transaction    The owning {@link ODSTransaction}.
	 * @param entityType     The associated {@link EntityType}.
	 * @param ignoreChildren If {@code true}, then child entities won't be
	 *                       processed.
	 */
	public UpdateStatement(ODSTransaction transaction, EntityType entityType, boolean ignoreChildren) {
		super(transaction, entityType);

		nonUpdatableRelationNames = entityType.getInfoRelations().stream().map(Relation::getName)
				.collect(Collectors.toList());
		this.ignoreChildren = ignoreChildren;

		EntityConfig<?> entityConfig = getModelManager().getEntityConfig(getEntityType());
		isFilesAttachable = FilesAttachable.class.isAssignableFrom(entityConfig.getEntityClass());
	}

	/**
	 * Constructor.
	 *
	 * @param transaction The owning {@link ODSTransaction}.
	 * @param entityType  The associated {@link EntityType}.
	 */
	public UpdateStatement(ODSTransaction transaction, EntityType entityType) {
		super(transaction, entityType);

		nonUpdatableRelationNames = entityType.getInfoRelations().stream().map(Relation::getName)
				.collect(Collectors.toList());
		this.ignoreChildren = true;

		isFilesAttachable = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Collection<Entity> entities) throws OdsException, DataAccessException, IOException {
		for (Entity entity : entities) {
			readEntityCore(ODSEntityFactory.extract(entity));
		}
		execute(entities.size());
	}

	/**
	 * Executes this statement for given {@link Core}s.
	 *
	 * @param cores The processed {@code Core}s.
	 * @throws OdsException        Thrown if the execution fails.
	 * @throws DataAccessException Thrown if the execution fails.
	 * @throws IOException         Thrown if a file transfer operation fails.
	 */
	public void executeWithCores(Collection<Core> cores) throws OdsException, DataAccessException, IOException {
		cores.forEach(this::readEntityCore);
		execute(1);
	}

	protected abstract void execute(int entitySize) throws OdsException, DataAccessException, IOException;

	/**
	 * Reads given {@link Core} and prepares its data to be written:
	 *
	 * <ul>
	 * <li>collect new and removed {@link FileLink}s</li>
	 * <li>collect property {@link Value}s</li>
	 * <li>collect foreign key {@code Value}s</li>
	 * <li>collect child entities for recursive create/update/delete</li>
	 * </ul>
	 *
	 * @param core The {@code Core}.
	 * @throws DataAccessException Thrown in case of errors.
	 */
	private void readEntityCore(Core core) throws DataAccessException {
		if (!core.getTypeName().equals(getEntityType().getName())) {
			throw new IllegalArgumentException("Entity core '" + core.getTypeName()
					+ "' is incompatible with current update statement for entity type '" + getEntityType() + "'.");
		}

		// add all entity values
		for (Value value : core.getAllValues().values()) {
			updateMap.computeIfAbsent(value.getName(), k -> new ArrayList<>()).add(value);
		}

		// collect file links
		fileLinkToUpload.addAll(core.getAddedFileLinks());
		List<FileLink> fileLinksToRemove = core.getRemovedFileLinks();
		if (isFilesAttachable && !fileLinksToRemove.isEmpty()) {
			getTransaction().getUploadService().addToRemove(fileLinksToRemove);
		}

		updateMap.computeIfAbsent(getEntityType().getIDAttribute().getName(), k -> new ArrayList<>())
				.add(getEntityType().getIDAttribute().createValue(core.getID()));

		// define "empty" values for ALL informative relations
		for (Relation relation : getEntityType().getInfoRelations()) {
			updateMap.computeIfAbsent(relation.getName(), k -> new ArrayList<>()).add(relation.createValue());
		}

		if (isParentUpdateAllowed(core)) {
			for (Relation relation : getEntityType().getParentRelations()) {
				updateMap.computeIfAbsent(relation.getName(), k -> new ArrayList<>()).add(relation.createValue());
			}
		}

		// try to change templates if requested
		UpdateStatementChangeTemplate.update(core, getTransaction(), childrenToUpdate);

		// preserve "empty" relation values for removed related entities
		EntityStore mutableStore = core.getMutableStore();
		mutableStore.getRemovedMap().values().stream().map(e -> getModelManager().getEntityType(e))
				.map(getEntityType()::getRelation).map(Relation::getName).forEach(nonUpdatableRelationNames::remove);

		// replace "empty" relation values with corresponding instance IDs
		setRelationIDs(mutableStore.getCurrentMap());

		collectChildEntities(core);

		getTransaction().addModified(core);
	}

	/**
	 * Collects child entities for recursive processing.
	 *
	 * @param core The {@link Core}.
	 */
	private void collectChildEntities(Core core) {
		if (ignoreChildren) {
			return;
		}

		for (Entry<Class<? extends Deletable>, List<? extends Deletable>> entry : core.getChildrenStore().getCurrent()
				.entrySet()) {
			Map<Boolean, List<Entity>> partition = entry.getValue().stream()
					.collect(Collectors.partitioningBy(e -> ODSUtils.isValidID(e.getID())));
			List<Entity> virtualEntities = partition.get(Boolean.FALSE);
			if (virtualEntities != null && !virtualEntities.isEmpty()) {
				childrenToCreate.computeIfAbsent(entry.getKey(), k -> new ArrayList<>()).addAll(virtualEntities);
			}
			List<Entity> existingEntities = partition.get(Boolean.TRUE);
			if (existingEntities != null && !existingEntities.isEmpty()) {
				childrenToUpdate.computeIfAbsent(entry.getKey(), k -> new ArrayList<>()).addAll(existingEntities);
			}
		}

		for (Entry<Class<? extends Deletable>, List<? extends Deletable>> entry : core.getChildrenStore().getRemoved()
				.entrySet()) {
			List<Deletable> toDelete = entry.getValue().stream().filter(e -> ODSUtils.isValidID(e.getID()))
					.collect(Collectors.toList());
			childrenToRemove.computeIfAbsent(entry.getKey(), k -> new ArrayList<>()).addAll(toDelete);
		}
	}

	/**
	 * Overwrites empty foreign key {@link Value} containers.
	 *
	 * @param relatedEntities The related {@link Entity}s.
	 */
	private void setRelationIDs(Map<String, Entity> relatedEntities) {
		for (Map.Entry<String, Entity> entry : relatedEntities.entrySet()) {
			String relationName = entry.getKey();
			Entity relatedEntity = entry.getValue();
			if (!ODSUtils.isValidID(relatedEntity.getID())) {
				throw new IllegalArgumentException("Related entity must be a persited entity.");
			}

			final Relation relation;
			if (relatedEntity.getClass().getSimpleName().equals(relationName)) {
				relation = getEntityType().getRelation(getModelManager().getEntityType(relatedEntity));
			} else {
				relation = getEntityType().getRelation(getModelManager().getEntityType(relatedEntity), relationName);
			}

			List<Value> relationValues = updateMap.get(relation.getName());
			if (relationValues == null) {
				throw new IllegalStateException("Relation '" + relation
						+ "' is incompatible with update statement for entity type '" + getEntityType() + "'");
			}
			relationValues.get(relationValues.size() - 1).set(relatedEntity.getID());
			nonUpdatableRelationNames.remove(relation.getName());
		}
	}

	/**
	 * @param core
	 * @return is it possible to update the parent? true or false
	 */
	private boolean isParentUpdateAllowed(Core core) {
		return updateParent.getOrDefault(core.getTypeName(), c -> false).test(core);
	}

	/**
	 * This function checks the possibility to change the parent for the
	 * {@link TestStep}.
	 * 
	 * @param core of the TestStep to check
	 * @return true, if it is allowed to move the given TestStep to the new Test,
	 *         e.g. the TemplateTest allows for the TemplateTestStep of the TestStep
	 */
	private boolean isParentUpdateAllowedTestStep(Core core) {
		Test test = core.getMutableStore().get(Test.class);
		if (test == null) {
			return false;
		}
		TemplateTest templateTest = ODSEntityFactory.extract(test).getMutableStore().get(TemplateTest.class);
		TemplateTestStep templateTestStep = core.getMutableStore().get(TemplateTestStep.class);
		if (templateTest != null && templateTestStep != null) {
			for (TemplateTestStepUsage tts : templateTest.getTemplateTestStepUsages()) {
				if (tts.getTemplateTestStep().getID().equals(templateTestStep.getID())) {
					return true;
				}
			}
		}
		return false;
	}
}
