/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.CatalogSensor;
import org.eclipse.mdm.api.odsadapter.query.OdsException;

public interface CatalogManager {

	/**
	 * Creates for each given {@link CatalogComponent} and {@link CatalogSensor} a
	 * corresponding application element including all required application
	 * relations. Creates for each given {@link CatalogAttribute} a corresponding
	 * application attribute.
	 *
	 * @param catalogComponents The {@code CatalogComponent}s.
	 * @throws OdsException Thrown in case of errors.
	 */
	<T extends Entity> void createCatalog(Map<Class<?>, List<T>> entitiesByClassType) throws OdsException;

	/**
	 * Deletes the corresponding application element for each given
	 * {@link CatalogComponent}, {@link CatalogSensor} and {@link CatalogAttribute}.
	 * Deleting a {@code CatalogComponent} is only allowed if it is not used in
	 * templates and all of its children could be deleted. So at first it is tried
	 * to delete its {@link CatalogAttribute}s and {@link CatalogSensor}s. On
	 * success it is ensured none of the given {@code
	 * CatalogComponent}s is used in templates. Finally the corresponding
	 * application elements are deleted.
	 *
	 * @param catalogComponents The {@code CatalogComponent}s.
	 * @throws OdsException        Thrown in case of errors.
	 * @throws DataAccessException Thrown in case of errors.
	 */

	<T extends Deletable> void deleteCatalog(List<T> catalogEntities) throws OdsException;

	/**
	 * Updates the application attribute for each given {@link CatalogAttribute}.
	 *
	 * @param catalogAttributes The {@code CatalogAttribute}s.
	 * @throws OdsException Thrown in case of errors.
	 */
	void updateCatalogAttributes(List<CatalogAttribute> catalogAttributes) throws OdsException;

	/**
	 * Releases cached resources.
	 */
	void clear();

}