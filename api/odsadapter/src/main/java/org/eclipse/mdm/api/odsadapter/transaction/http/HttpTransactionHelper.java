/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.http;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.ODSContextFactory;
import org.eclipse.mdm.api.odsadapter.ODSHttpContext;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityFactory;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.DeleteStatement;
import org.eclipse.mdm.api.odsadapter.transaction.InsertStatement;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.transaction.OdsTransactionHelper;
import org.eclipse.mdm.api.odsadapter.transaction.UpdateStatement;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;

import com.google.common.primitives.Longs;

import ods.Ods.AsamPath;
import ods.Ods.Instance;
import ods.Ods.NtoMRelatedInstances;
import ods.Ods.NtoMRelationIdentifier;
import ods.Ods.NtoMWriteRelatedInstances;
import ods.Ods.NtoMWriteRelatedInstances.WriteTypeEnum;

public class HttpTransactionHelper implements OdsTransactionHelper {

	private final ODSHttpContext context;

	public HttpTransactionHelper(ODSHttpContext context) {
		this.context = context;
	}

	@Override
	public List<String> loadRelatedIds(EntityType entityType, String iid, String relationName) {
		// TODO only works for n-to-m relations. Do we need to support 1-to-n relations
		// in this method?
		Relation relation = entityType.getRelations().stream()
				.filter(r -> r.getName().equals(relationName) || r.getInverseName().equals(relationName)).findFirst()
				.orElseThrow(() -> new DataAccessException(
						"No relation '" + relationName + "' found at Entity '" + entityType.getName() + "'."));

		NtoMRelationIdentifier x = NtoMRelationIdentifier.newBuilder()
				.setAid(ODSHttpConverter.toODSID(relation.getSource().getId())).setIid(ODSHttpConverter.toODSID(iid))
				.setRelationName(relation.getName())
				.setRelatedAid(ODSHttpConverter.toODSID(relation.getTarget().getId())).build();

		try {
			NtoMRelatedInstances y = OdsHttpRequests.request(context.getSession(), "n-m-relation-read", x,
					NtoMRelatedInstances.class);

			return y.getRelatedIidsList().stream().map(l -> Long.toString(l)).collect(Collectors.toList());
		} catch (OdsException e) {
			throw new DataAccessException("Cannot load related IDs!", e);
		}
	}

	@Override
	public <T extends Entity> void processNtoMRelations(Collection<T> entities) {
		for (Entity e : entities) {
			context.getODSModelManager().getEntityType(e).getRelations().stream().filter(Relation::isNtoM)
					.forEach(r -> processNtoMRelation(e, r));
		}
	}

	private void processNtoMRelation(Entity entity, Relation relation) {

		NtoMRelationIdentifier relationIdentifier = NtoMRelationIdentifier.newBuilder()
				.setAid(ODSHttpConverter.toODSID(relation.getSource().getId()))
				.setRelatedAid(ODSHttpConverter.toODSID(relation.getTarget().getId()))
				.setIid(ODSHttpConverter.toODSID(entity.getID())).setRelationName(relation.getName()).build();

		long[] removedInstIds = ODSEntityFactory.extract(entity).getNtoMStore().getRemoved()
				.getOrDefault(relation.getName(), Collections.emptyList()).stream().map(Entity::getID)
				.mapToLong(ODSHttpConverter::toODSID).toArray();

		if (removedInstIds.length > 0) {
			NtoMWriteRelatedInstances nToM = NtoMWriteRelatedInstances.newBuilder()
					.setChangeMode(WriteTypeEnum.WT_REMOVE).setRelationInfo(NtoMRelatedInstances.newBuilder()
							.addAllRelatedIids(Longs.asList(removedInstIds)).setIdentifier(relationIdentifier))
					.build();

			try {
				OdsHttpRequests.request(context.getSession(), "n-m-relation-write", nToM);
			} catch (OdsException e) {
				throw new DataAccessException("Cannot remove related IDs!", e);
			}
		}

		long[] addedInstIds = ODSEntityFactory.extract(entity).getNtoMStore().getAdded()
				.getOrDefault(relation.getName(), Collections.emptyList()).stream().map(Entity::getID)
				.mapToLong(ODSHttpConverter::toODSID).distinct().toArray();

		if (addedInstIds.length > 0) {
			NtoMWriteRelatedInstances nToM = NtoMWriteRelatedInstances.newBuilder().setChangeMode(WriteTypeEnum.WT_ADD)
					.setRelationInfo(NtoMRelatedInstances.newBuilder().addAllRelatedIids(Longs.asList(addedInstIds))
							.setIdentifier(relationIdentifier))
					.build();

			try {
				OdsHttpRequests.request(context.getSession(), "n-m-relation-write", nToM);
			} catch (OdsException e) {
				throw new DataAccessException("Cannot add related IDs!", e);
			}
		}
	}

	@Override
	public void startTransaction() throws OdsException {
		OdsHttpRequests.request(context.getSession(), "transaction-create", null);
	}

	@Override
	public void commitTransaction() throws OdsException {
		OdsHttpRequests.request(context.getSession(), "transaction-commit", null);
	}

	@Override
	public void abortTransaction() throws OdsException {
		OdsHttpRequests.request(context.getSession(), "transaction-abort", null);
	}

	@Override
	public InsertStatement getInsertStatement(ODSTransaction transaction, EntityType entityType) {
		return new HttpInsertStatement(transaction, entityType);
	}

	@Override
	public UpdateStatement getUpdateStatement(ODSTransaction transaction, EntityType entityType,
			boolean ignoreChildren) {
		return new HttpUpdateStatement(transaction, entityType, ignoreChildren);
	}

	@Override
	public DeleteStatement getDeleteStatement(ODSTransaction transaction, EntityType entityType,
			boolean ignoreChildren) {
		return new HttpDeleteStatement(transaction, entityType, ignoreChildren);
	}

	@Override
	public Map<? extends Entity, ? extends String> getLinks(ODSEntityType et, List<Entity> entities) {
		Map<Entity, String> linkMap = new HashMap<>();

		String serverRoot = context.getParameters().get(ODSContextFactory.PARAM_NAMESERVICE) + "/"
				+ context.getParameters().get(ODSContextFactory.PARAM_SERVICENAME);

		try {
			for (Entity entity : entities) {
				AsamPath asamPath = OdsHttpRequests.request(context.getSession(), "asampath-create", Instance
						.newBuilder().setAid(et.getODSID()).setIid(ODSHttpConverter.toODSID(entity.getID())).build(),
						AsamPath.class);
				linkMap.put(entity, serverRoot + asamPath.getPath());
			}
		} catch (OdsException e) {
			throw new DataAccessException(
					"Could not load links for entities: " + entities + ". Reason: " + e.getMessage(), e);
		}
		return linkMap;
	}
}
