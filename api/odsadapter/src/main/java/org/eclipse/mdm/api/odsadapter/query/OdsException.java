/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.query;

public class OdsException extends Exception {

	private static final long serialVersionUID = 7123321692640706680L;

	/**
	 * Constructor.
	 *
	 * @param message The error message.
	 */
	public OdsException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 *
	 * @param message   The error message.
	 * @param throwable The origin cause.
	 */
	public OdsException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
