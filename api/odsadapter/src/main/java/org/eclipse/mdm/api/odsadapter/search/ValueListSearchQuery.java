/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.search;

import java.util.Collections;
import java.util.List;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.base.search.SearchQuery;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;

/**
 * {@link SearchQuery} implementation for {@link ValueList} as source entity
 * type.
 *
 * @since 1.0.0
 * @author sch, Peak Solution GmbH
 */
final class ValueListSearchQuery extends BaseEntitySearchQuery {

	private ODSModelManager modelManager;

	/**
	 * Constructor.
	 *
	 * @param modelManager Used to load {@link EntityType}s.
	 * @param contextState The {@link ContextState}.
	 */
	ValueListSearchQuery(ODSModelManager modelManager, QueryService queryService, ContextState contextState) {
		super(modelManager, queryService, ValueList.class, ValueList.class);

		this.modelManager = modelManager;
	}

	@Override
	public List<EntityType> listEntityTypes() {
		return Collections.singletonList(this.modelManager.getEntityType(ValueList.class));
	}
}
