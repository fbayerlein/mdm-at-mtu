/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.search;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.base.search.SearchQuery;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.eclipse.mdm.api.odsadapter.search.JoinTree.JoinNode;

/**
 * {@link SearchQuery} implementation for {@link ContextComponent} as source
 * entity type.
 *
 * @since 1.0.0
 * @author jz, Peak Solution GmbH
 */
public class ContextComponentSearchQuery extends BaseEntitySearchQuery {

	/**
	 * Constructor.
	 *
	 * @param modelManager Used to load {@link EntityType}s.
	 * @param contextState The {@link ContextState}.
	 */
	ContextComponentSearchQuery(ODSModelManager modelManager, QueryService queryService, String refEntity,
			JoinTree joinTree, String entityName, List<EntityType> listEntityTypes) {
		super(modelManager, queryService, ContextComponent.class, ContextComponent.class);
		List<String> names = new ArrayList<>();
		this.getNodesToChange(names, entityName, refEntity, joinTree);
		for (String name : names) {
			JoinNode node = joinTree.getJoinNode(name);
			this.addJoinNode(this.getEntityType(listEntityTypes, node.target),
					this.getEntityType(listEntityTypes, node.source), false, node.joinType);
		}
		for (String name : joinTree.getNodeNames()) {
			if (!names.contains(name) && !refEntity.equals(name)) {
				JoinNode node = joinTree.getJoinNode(name);
				this.addJoinNode(this.getEntityType(listEntityTypes, node.source),
						this.getEntityType(listEntityTypes, node.target), false, node.joinType);
			}
		}
	}

	private void getNodesToChange(List<String> names, String start, String ende, JoinTree joinTree) {
		JoinNode node = joinTree.getJoinNode(start);
		names.add(node.target);
		if (!node.source.equalsIgnoreCase(ende)) {
			this.getNodesToChange(names, node.source, ende, joinTree);
		}
	}

	private EntityType getEntityType(List<EntityType> listEntityTypes, String name) {
		for (EntityType entityType : listEntityTypes) {
			if (name.equalsIgnoreCase(entityType.getName())) {
				return entityType;
			}
		}
		// this case should not be possible
		throw new IllegalArgumentException("The entity \"" + name + "\" is not defined.");
	}
}
