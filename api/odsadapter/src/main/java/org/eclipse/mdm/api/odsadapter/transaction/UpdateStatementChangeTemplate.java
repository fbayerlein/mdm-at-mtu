/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.TemplateAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for UpdateStatement to change the template of an existing
 * TestStep.
 */
class UpdateStatementChangeTemplate {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateStatement.class);

	/**
	 * Check if template should be changed and adjust accordingly or throw error.
	 * 
	 * Compatibility Rules:
	 * 
	 * TemplateTestStep must already be allowed at TemplateTest. New Components and
	 * attributes are allowed. New obligatory attributes must have a default value.
	 * 
	 * @param core             core of element to be checked
	 * @param transaction      used for retrieving EntityFactory if necessary
	 * @param childrenToUpdate will add changed children to this list
	 */
	public static void update(final Core core, final ODSTransaction transaction,
			final Map<Class<? extends Entity>, List<Entity>> childrenToUpdate) {

		if (!"TestStep".equals(core.getTypeName())) {
			// only allowed for TestStep
			return;
		}
		final TemplateTestStep templateTestStepOld = TemplateTestStep.class
				.cast(core.getMutableStore().getRemovedMap().get(TemplateTestStep.class.getSimpleName()));
		if (null == templateTestStepOld) {
			return;
		}
		final TemplateTestStep templateTestStepNew = core.getMutableStore().get(TemplateTestStep.class);
		if (null == templateTestStepNew) {
			return;
		}
		if (templateTestStepNew.getID().equals(templateTestStepOld.getID())) {
			// not changed
			return;
		}
		final Test test = core.getPermanentStore().get(Test.class);
		if (test == null) {
			// no parent Test given
			return;
		}
		final TemplateTest templateTest = ODSEntityFactory.extract(test).getMutableStore().get(TemplateTest.class);
		if (null == templateTest) {
			return;
		}

		validForTestTemplate(templateTest, templateTestStepNew);

		// TestStep template should be changed. Lets try.
		changeTemplate(core, templateTestStepOld, templateTestStepNew, transaction, childrenToUpdate);
	}

	/**
	 * Will try to change the template of the given TestStep
	 * 
	 * @param testStep            TestStep instance to work on
	 * @param templateTestStepOld Determined old template of the TestStep
	 * @param templateTestStepNew Target template to be used for the TestStep
	 *                            instance
	 * @param transaction         Used to retrieve the EntityFactory if necessary
	 * @param childrenToUpdate    Will add changed children to this list
	 * @throws IllegalArgumentException If compatibility issues are found
	 */
	private static void changeTemplate(final Core testStep, final TemplateTestStep templateTestStepOld,
			final TemplateTestStep templateTestStepNew, final ODSTransaction transaction,
			final Map<Class<? extends Entity>, List<Entity>> childrenToUpdate) throws IllegalArgumentException {

		LOGGER.debug("TestStep Template should be changed from '{}' to '{}'. Check compatibility.",
				templateTestStepNew.getName(), templateTestStepOld.getName());

		for (final TemplateRoot templateRootOld : templateTestStepOld.getTemplateRoots()) {
			final ContextType contextType = templateRootOld.getContextType();
			final TemplateRoot templateRootNew = templateTestStepNew.getTemplateRoot(contextType)
					.orElseThrow(() -> new IllegalArgumentException(toString(templateTestStepNew)
							+ " has no context root for the context type:" + contextType));

			final ContextRoot contextRoot = testStep.getMutableStore().get(ContextRoot.class, contextType);
			final Core contextRootCore = EntityFactory.getCore(contextRoot);
			contextRootCore.getMutableStore().set(templateRootNew);
			childrenToUpdate.computeIfAbsent(TemplateRoot.class, k -> new ArrayList<>()).add(contextRoot);

			final Map<String, TemplateComponent> mapTemplateOldToNew = new HashMap<>();
			final Collection<TemplateComponent> addedTemplateComponents = findComponentMapping(
					templateRootOld.getTemplateComponents(), templateRootNew.getTemplateComponents(),
					mapTemplateOldToNew);

			final List<ContextComponent> contextComponents = contextRootCore.getChildrenStore()
					.get(ContextComponent.class);
			for (ContextComponent contextComponent : contextComponents) {
				// All context components are bound to context root.
				// The tree found in the Templates is not modeled here.
				// They are bound by name
				final Core contextComponentCore = EntityFactory.getCore(contextComponent);
				final TemplateComponent templateComponentOld = contextComponentCore.getMutableStore()
						.get(TemplateComponent.class);
				if (null == templateComponentOld) {
					throw new IllegalArgumentException(toString(contextComponentCore) + " has no template component");
				}

				final TemplateComponent templateComponentNew = mapTemplateOldToNew.get(templateComponentOld.getID());
				if (null == templateComponentNew) {
					throw new IllegalArgumentException(
							toString(templateComponentOld) + " has no matching template component in target.");
				}

				// assign new template
				contextComponentCore.getMutableStore().set(templateComponentNew);
				// set name if differs
				if (!templateComponentOld.getName().equals(templateComponentNew.getName())) {
					// equal target component but different name
					contextComponent.setName(templateComponentNew.getName());
				}
				// assign new obligatory attributes
				final Collection<TemplateAttribute> obligatoryAttributesToBeSet = areComponentAttributesCompatibity(
						templateComponentOld, templateComponentNew);
				for (TemplateAttribute obligatoryAttributeToBeSet : obligatoryAttributesToBeSet) {
					final String attrName = obligatoryAttributeToBeSet.getName();
					final Object attrValue = obligatoryAttributeToBeSet.getDefaultValue().extract();
					contextComponentCore.getAllValues().get(attrName).set(attrValue);
				}
				childrenToUpdate.computeIfAbsent(TemplateRoot.class, k -> new ArrayList<>()).add(contextComponent);
			}

			generateAddedContextComponents(transaction, contextRoot, addedTemplateComponents);
		}
		LOGGER.debug("TestStep Template changed.");

	}

	/**
	 * create new context components and attach them to their context root
	 * 
	 * @param transaction             used to fetch EntityFactory
	 * @param contextRoot             context root to attach new elements to
	 * @param addedTemplateComponents components to be added to the context root
	 */
	private static void generateAddedContextComponents(final ODSTransaction transaction, final ContextRoot contextRoot,
			final Collection<TemplateComponent> addedTemplateComponents) {

		final EntityFactory entityFactory = transaction.getContext().getEntityFactory()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityFactory.class));
		addedTemplateComponents.stream().filter(TemplateComponent.IS_DEFAULT_ACTIVE.or(TemplateComponent.IS_MANDATORY))
				.forEach(templateComponent -> {
					entityFactory.createContextComponent(templateComponent.getName(), contextRoot);
				});
	}

	/**
	 * Check if TestSteps of this kind can be attached to this Test
	 * 
	 * @param templateTest     Test template
	 * @param templateTestStep TestStep template to be checked
	 * @throws IllegalArgumentException If not valid for Test
	 */
	private static void validForTestTemplate(final TemplateTest templateTest, final TemplateTestStep templateTestStep)
			throws IllegalArgumentException {
		templateTest.getTemplateTestStepUsages().stream()
				.filter(tts -> tts.getTemplateTestStep().getID().equals(templateTestStep.getID())).findFirst()
				.orElseThrow(() -> new IllegalArgumentException(toString(templateTestStep)
						+ " is not compatible with parent template " + toString(templateTest)));
	}

	/**
	 * @param entity Entity to be logged
	 * @return String to be used in error logging
	 */
	private static String toString(final BaseEntity entity) {
		return entity.getTypeName() + ":" + entity.getName();
	}

	/**
	 * @param core Core to be logged
	 * @return String to be used in error logging
	 */
	private static String toString(final Core core) {
		return core.getTypeName() + ":" + core.getID();
	}

	/**
	 * Try to find a mapping between the TemplateComponents of the old and the new
	 * Template. Mapping is success if catalog component matches. If multiple
	 * components match the name of the template must be equal.
	 * 
	 * !!! This method works recursive
	 * 
	 * @param templateComponentsOld old template components to map
	 * @param templateComponentsNew new template components available
	 * @param mapTemplateOldToNew   map to be filled
	 * @return
	 */
	private static Collection<TemplateComponent> findComponentMapping(
			final List<TemplateComponent> templateComponentsOld,
			final Collection<TemplateComponent> templateComponentsNewIn,
			final Map<String, TemplateComponent> mapTemplateOldToNew) {

		if (null == templateComponentsOld) {
			return Collections.emptySet();
		}

		final Collection<TemplateComponent> templateComponentsNew = null != templateComponentsNewIn
				? templateComponentsNewIn
				: Collections.emptyList();

		final Map<String, TemplateComponent> newTemplateComponents = templateComponentsNew.stream()
				.collect(Collectors.toMap(TemplateComponent::getID, Function.identity()));

		List<TemplateComponent> returnValues = new ArrayList<>();

		for (TemplateComponent templateComponentOld : templateComponentsOld) {
			final TemplateComponent templateComponentNew = getCorrespondingTemplate(templateComponentsNew,
					templateComponentOld);

			areComponentAttributesCompatibity(templateComponentOld, templateComponentNew);
			if (null != mapTemplateOldToNew.put(templateComponentOld.getID(), templateComponentNew)) {
				throw new IllegalArgumentException(toString(templateComponentOld) + " mapped twice");
			}
			newTemplateComponents.remove(templateComponentNew.getID());

			returnValues.addAll(findComponentMapping(templateComponentOld.getTemplateComponents(),
					templateComponentNew.getTemplateComponents(), mapTemplateOldToNew));
		}
		returnValues.addAll(newTemplateComponents.values());

		return returnValues;
	}

	private static TemplateComponent getCorrespondingTemplate(final Collection<TemplateComponent> templateComponentsNew,
			final TemplateComponent templateComponentOld) {

		final CatalogComponent catalogComponentOld = templateComponentOld.getCatalogComponent();

		final List<TemplateComponent> templateComponentsCandidatesNew = templateComponentsNew.stream().filter(item -> {
			CatalogComponent itemCatalogComponent = item.getCatalogComponent();
			return itemCatalogComponent.getTypeName().equals(catalogComponentOld.getTypeName())
					&& itemCatalogComponent.getID().equals(catalogComponentOld.getID());
		}).collect(Collectors.toList());
		if (templateComponentsCandidatesNew.isEmpty()) {
			throw new IllegalArgumentException("There is no matching TemplateComponent for "
					+ toString(templateComponentOld) + " pointing to " + toString(catalogComponentOld));
		}

		if (1 == templateComponentsCandidatesNew.size()) {
			// if a single one is pointing to the same catalog component we guess this is
			// the right one.
			return templateComponentsCandidatesNew.get(0);
		}

		final String templateComponentNameOld = templateComponentOld.getName();
		// more than one pointing to the same catalog component. So names must match.
		return templateComponentsCandidatesNew.stream().filter(item -> item.getName().equals(templateComponentNameOld))
				.findAny().orElseThrow(() -> new IllegalArgumentException(
						"There is no matching TemplateComponent with same name for " + toString(templateComponentOld)));
	}

	/**
	 * Check if the attributes of the two templates are compatible. This is used to
	 * figure out if they can be mapped.
	 * 
	 * @param templateOld source component template that should be mapped to a new
	 *                    template
	 * @param templateNew destination template
	 * @param errorInfo   If given it will be used to log error info to help
	 *                    identifying the issue.
	 * @return list of new obligatory attributes to be set
	 * @throws IllegalArgumentException If attributes not compatible
	 */
	private static Collection<TemplateAttribute> areComponentAttributesCompatibity(final TemplateComponent templateOld,
			final TemplateComponent templateNew) throws IllegalArgumentException {

		final List<TemplateAttribute> attributesOld = templateOld.getTemplateAttributes();
		final List<TemplateAttribute> attributesUnusedNew = new ArrayList<>(templateNew.getTemplateAttributes());

		for (TemplateAttribute attributeOld : attributesOld) {

			final String attributeNameOld = attributeOld.getName();
			final TemplateAttribute attributeNew = attributesUnusedNew.stream()
					.filter(item -> attributeNameOld.equals(item.getName())).findFirst().orElseThrow(
							() -> new IllegalArgumentException(toString(templateNew) + " does not have the attribute '"
									+ attributeNameOld + "' that is available in " + toString(templateOld)));

			if (attributeOld.isOptional() && !attributeNew.isOptional()) {
				throw new IllegalArgumentException("Optional attribute " + attributeNameOld + " at "
						+ toString(templateOld) + " can not be changed to obligatory.");
			}
			attributesUnusedNew.remove(attributeNew);
		}

		List<TemplateAttribute> newObligatoryAttributes = attributesUnusedNew.stream().filter(a -> !a.isOptional())
				.collect(Collectors.toList());
		for (TemplateAttribute newObligatoryAttribute : newObligatoryAttributes) {
			// obligatory attributes need default value. getDefaultValue will throw if none
			// is given.
			newObligatoryAttribute.getDefaultValue();
		}
		return newObligatoryAttributes;
	}
}
