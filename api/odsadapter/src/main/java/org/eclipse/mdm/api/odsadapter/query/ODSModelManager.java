/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.query;

import java.util.List;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig.Key;

public interface ODSModelManager extends ModelManager {

	public static final String DEFAULT_TIMEZONE = "UTC";

	/**
	 * Returns the non root {@link EntityConfig} for given {@link Key}.
	 *
	 * @param <T> The concrete entity type.
	 * @param key Used as identifier.
	 * @return The non root {@code EntityConfig} is returned.
	 */
	<T extends Entity> EntityConfig<T> findEntityConfig(Key<T> key);

	/**
	 * Returns the root {@link EntityConfig} for given {@link Key}.
	 *
	 * @param <T> The concrete entity type.
	 * @param key Used as identifier.
	 * @return The root {@code EntityConfig} is returned.
	 */
	<T extends Entity> EntityConfig<T> getEntityConfig(Key<T> key);

	/**
	 * Returns the {@link EntityConfig} associated with given {@link EntityType}.
	 *
	 * @param entityType Used as identifier.
	 * @return The {@code EntityConfig} is returned.
	 */
	EntityConfig<?> getEntityConfig(EntityType entityType);

	/**
	 * {@inheritDoc}
	 */
	List<EntityType> listEntityTypes();

	/**
	 * {@inheritDoc}
	 */
	EntityType getEntityType(Class<? extends Entity> entityClass);

	/**
	 * {@inheritDoc}
	 */
	EntityType getEntityType(Class<? extends Entity> entityClass, ContextType contextType);

	/**
	 * {@inheritDoc}
	 */
	EntityType getEntityType(String name);

	EntityType getEntityTypeById(String id);

	String getMimeType(EntityType entityType);

	String getSourceName();

	/**
	 * Closes the ODS connection.
	 *
	 * @throws OdsException Thrown on errors.
	 */
	void close() throws OdsException;

	/**
	 * Reloads the complete session context.
	 */
	void reloadApplicationModel();

	/**
	 * Add an configuration, e.g. for an ODSEntity as result
	 * 
	 * @param entityConfig
	 */
	void addEntity(EntityConfig<?> entityConfig);

	long getUserId();
}