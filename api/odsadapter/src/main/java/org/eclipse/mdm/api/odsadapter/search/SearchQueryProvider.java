/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.search;

import java.util.List;

import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;

/**
 * Interface for providing {@link BaseEntitySearchQuery}s for {@link Entity}.
 * 
 *
 */
public interface SearchQueryProvider {

	/**
	 * @return list of {@link Entity} for which SearchQueries can be provided.
	 */
	List<Class<? extends Entity>> supportedEntityClasses();

	/**
	 * Provides a {@link BaseEntitySearchQuery} for the specified {@link Entity}
	 * class.
	 * 
	 * @param entityClass  class of {@link Entity} the SearchQuery should be
	 *                     registered to.
	 * @param modelManager model manager
	 * @param queryService query service
	 * @param contextState context state
	 * @return the {@link BaseEntitySearchQuery} for the given {@link Entity} class
	 */
	BaseEntitySearchQuery provide(Class<? extends Entity> entityClass, ODSModelManager modelManager,
			QueryService queryService, ContextState contextState);
}
