/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.http;

import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import javax.ws.rs.WebApplicationException;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.Versionable;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.InsertStatement;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ods.Ods.DataMatrices;
import ods.Ods.DataMatrix;
import ods.Ods.DataMatrix.Column;
import ods.Ods.DataTypeEnum;
import ods.Ods.ErrorInfo;
import ods.Ods.LonglongArray;
import ods.Ods.StringArray;

/**
 * Insert statement is used to write new entities and their children.
 *
 */
public final class HttpInsertStatement extends InsertStatement {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpInsertStatement.class);

	/**
	 * Constructor.
	 *
	 * @param transaction The owning {@link ODSTransaction}.
	 * @param entityType  The associated {@link EntityType}.
	 */
	public HttpInsertStatement(ODSTransaction transaction, EntityType entityType) {
		super(transaction, entityType);
	}

	@Override
	protected void execute() throws OdsException, DataAccessException, IOException {
		long aID = getEntityType().getODSID();

		// TODO tracing progress in this method...

		if (loadSortIndex && !sortIndexTestSteps.isEmpty()) {
			adjustMissingSortIndices();
		}

		if (!fileLinkToUpload.isEmpty()) {
			getTransaction().getUploadService().uploadParallel(fileLinkToUpload, getTransaction(), null);
		}

		boolean isContextRoot = isContextRoot();

		DataMatrix.Builder dm = DataMatrix.newBuilder();
		dm.setAid(aID);

		for (Entry<String, List<Value>> entry : insertMap.entrySet()) {
			Attribute attribute = getEntityType().getAttribute(entry.getKey());

			if (ValueType.FILE_RELATION.equals(attribute.getValueType()) || entry.getValue().stream()
					.filter(v -> ValueType.FILE_RELATION.equals(v.getValueType())).count() > 0) {
				// skip FILE_RELATION pseudo-attributes
				continue;
			}

			Column column = ODSHttpConverter
					.toODSColumn(attribute, entry.getValue(),
							getTransaction().getContext().getODSModelManager().getTimeZone())
					.setName(entry.getKey()).setUnitId(0L).build();

			if (isContextRoot && Versionable.ATTR_VERSION.equals(attribute.getName())) {
				/*
				 * Template roots have to be unique for name and version. We temporarily set a
				 * random version and later on update the version with the id.
				 */
				StringArray.Builder stringArray = StringArray.newBuilder();
				for (int i = 0; i < column.getStringArray().getValuesCount(); i++) {
					stringArray.addValues(UUID.randomUUID().toString().substring(0, 20));
				}

				column = column.toBuilder().setStringArray(stringArray).setDataType(DataTypeEnum.DT_STRING).build();
			}

			// If all values are invalid, skip this attribute
			if (allTrue(column.getIsNullList())) {
				LOGGER.trace("skipping " + column.getName() + ", because all values have are null.");
				continue;
			}

			dm.addColumns(column);
		}

		long start = System.currentTimeMillis();
		try {

			ODSHttpModelManager mm = ((ODSHttpModelManager) getTransaction().getContext().getODSModelManager());

			DataMatrices dms = OdsHttpRequests.request(mm.getSession(), "data-create",
					DataMatrices.newBuilder().addMatrices(dm).build(), DataMatrices.class);

			List<Long> ids = dms.getMatrices(0).getColumns(0).getLonglongArray().getValuesList();

			if (isContextRoot) {
				/*
				 * Template roots have to be unique for name and version. By convention we use
				 * the id as version to make (name, version) unique.
				 */
				LonglongArray.Builder idValues = LonglongArray.newBuilder();
				StringArray.Builder versionValues = StringArray.newBuilder();
				for (int i = 0; i < ids.size(); i++) {
					idValues.addValues(ids.get(i));
					versionValues.addValues(Long.toString(ids.get(i)));
				}

				DataMatrix.Builder versionUpdate = DataMatrix.newBuilder().setAid(aID)
						.addColumns(Column.newBuilder().setName(Entity.ATTR_ID).setDataType(DataTypeEnum.DT_LONGLONG)
								.setLonglongArray(idValues))
						.addColumns(Column.newBuilder().setName(Versionable.ATTR_VERSION)
								.setDataType(DataTypeEnum.DT_STRING).setStringArray(versionValues));

				OdsHttpRequests.request(mm.getSession(), "data-update",
						DataMatrices.newBuilder().addMatrices(versionUpdate).build(), DataMatrices.class);

				for (int i = 0; i < ids.size(); i++) {
					cores.get(i).setID(Long.toString(ids.get(i)));
					cores.get(i).getValues().put(Versionable.ATTR_VERSION,
							ValueType.STRING.create(Versionable.ATTR_VERSION, Long.toString(ids.get(i))));
				}
			} else {
				for (int i = 0; i < ids.size(); i++) {
					cores.get(i).setID(Long.toString(ids.get(i)));
				}
			}

			long stop = System.currentTimeMillis();

			LOGGER.debug("{} " + getEntityType() + " instances created in {} ms.", ids.size(), stop - start);

			for (List<Entity> children : childrenMap.values()) {
				getTransaction().create(children);
			}
		} catch (WebApplicationException e) {
			ErrorInfo error = e.getResponse().readEntity(ErrorInfo.class);

			throw new OdsException(error.getErrCode().name() + ": " + error.getReason(), e);
		}
	}

	private boolean isContextRoot() {
		if ("AoUnitUnderTest".equalsIgnoreCase(getEntityType().getBaseName())
				|| "AoTestSequence".equalsIgnoreCase(getEntityType().getBaseName())
				|| "AoTestEquipment".equalsIgnoreCase(getEntityType().getBaseName())) {
			return true;
		}
		return false;
	}

	private boolean allTrue(List<Boolean> isNullList) {
		if (isNullList == null || isNullList.isEmpty()) {
			return false;
		}

		for (boolean isNull : isNullList) {
			if (!isNull) {
				return false;
			}
		}
		return true;
	}
}
