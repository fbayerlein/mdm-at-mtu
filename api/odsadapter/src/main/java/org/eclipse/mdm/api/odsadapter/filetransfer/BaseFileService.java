/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.filetransfer;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.slf4j.Logger;

/**
 * Base file service implementation of the {@link FileService} interface.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public abstract class BaseFileService implements FileService {

	private static final int THREAD_POOL_SIZE = 5;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void downloadSequential(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {

		Map<String, List<FileLink>> groups = fileLinks.stream().filter(FileLink::isRemote)
				.collect(Collectors.groupingBy(FileLink::getRemotePath));

		long totalSize = calculateDownloadSize(entity, groups, transaction);
		final AtomicLong transferred = new AtomicLong();
		LocalTime start = LocalTime.now();
		UUID id = UUID.randomUUID();
		getLogger().debug("Sequential download of {} file(s) with id '{}' started.", groups.size(), id);
		for (List<FileLink> group : groups.values()) {
			FileLink fileLink = group.get(0);

			download(entity, target, fileLink, transaction, (b, p) -> {
				double tranferredBytes = transferred.addAndGet(b);
				if (progressListener != null) {
					progressListener.progress(b, (float) (tranferredBytes / totalSize));
				}
			});

			for (FileLink other : group.subList(1, group.size())) {
				other.setLocalPath(fileLink.getLocalPath());
			}
		}
		getLogger().debug("Sequential download with id '{}' finished in {}.", id,
				Duration.between(start, LocalTime.now()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void downloadParallel(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {

		Map<String, List<FileLink>> groups = fileLinks.stream().filter(FileLink::isRemote)
				.collect(Collectors.groupingBy(FileLink::getRemotePath));

		long totalSize = calculateDownloadSize(entity, groups, transaction);
		final AtomicLong transferred = new AtomicLong();
		List<Callable<Void>> downloadTasks = new ArrayList<>();
		for (List<FileLink> group : groups.values()) {
			downloadTasks.add(() -> {
				FileLink fileLink = group.get(0);

				download(entity, target, fileLink, transaction, (b, p) -> {
					double tranferredBytes = transferred.addAndGet(b);
					if (progressListener != null) {
						progressListener.progress(b, (float) (tranferredBytes / totalSize));
					}
				});

				for (FileLink other : group.subList(1, group.size())) {
					other.setLocalPath(fileLink.getLocalPath());
				}

				return null;
			});
		}

		ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		LocalTime start = LocalTime.now();
		UUID id = UUID.randomUUID();
		getLogger().debug("Parallel download of {} file(s) with id '{}' started.", groups.size(), id);
		try {
			List<Throwable> errors = executorService.invokeAll(downloadTasks).stream().map(future -> {
				try {
					future.get();
					return null;
				} catch (ExecutionException | InterruptedException e) {
					getLogger().error("Download of failed due to: " + e.getMessage(), e);
					return e;
				}
			}).filter(Objects::nonNull).collect(Collectors.toList());

			if (!errors.isEmpty()) {
				throw new IOException("Download faild for '" + errors.size() + "' files.");
			}
			getLogger().debug("Parallel download with id '{}' finished in {}.", id,
					Duration.between(start, LocalTime.now()));
		} catch (InterruptedException e) {
			throw new IOException("Unable to download files due to: " + e.getMessage(), e);
		} finally {
			executorService.shutdown();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void download(Entity entity, Path target, FileLink fileLink, Transaction transaction,
			ProgressListener progressListener) throws IOException {

		if (Files.exists(target)) {
			if (!Files.isDirectory(target)) {
				throw new IllegalArgumentException("Target path is not a directory.");
			}
		} else {
			Files.createDirectory(target);
		}

		try (InputStream inputStream = openStream(entity, fileLink, transaction, progressListener)) {
			Path absolutePath = target.resolve(fileLink.getFileName()).toAbsolutePath();
			String remotePath = fileLink.getRemotePath();
			getLogger().debug("Starting download of file '{}' to '{}'.", remotePath, absolutePath);
			LocalTime start = LocalTime.now();
			Files.copy(inputStream, absolutePath);
			getLogger().debug("File '{}' successfully downloaded in {} to '{}'.", remotePath,
					Duration.between(start, LocalTime.now()), absolutePath);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream openStream(Entity entity, FileLink fileLink, Transaction transaction,
			ProgressListener progressListener) throws IOException {

		InputStream sourceStream;
		if (fileLink.isLocal()) {
			// file is locally available -> USE this shortcut!
			sourceStream = fileLink.getLocalStream();
		} else if (fileLink.isRemote()) {
			sourceStream = openRemoteStream(entity, fileLink, transaction);
		} else {
			throw new IllegalArgumentException("File link is neither in local nor remote state: " + fileLink);
		}

		// NOTE: Access to immediate input stream is buffered.
		if (progressListener != null) {
			loadSize(entity, fileLink, transaction);
			// NOTE: Progress updates immediately triggered by the stream
			// consumer.
			return new TracedInputStream(sourceStream, progressListener, fileLink.getSize());
		}
		return sourceStream;
	}

	protected abstract InputStream openRemoteStream(Entity entity, FileLink fileLink, Transaction transaction)
			throws IOException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uploadSequential(Entity entity, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		Map<Pair<String, Integer>, List<FileLink>> groups = fileLinks.stream().filter(FileLink::isLocal)
				.collect(Collectors.groupingBy(fl -> new ImmutablePair<>((fl.isLocalStream() ? "STREAM" : "PATH"),
						(fl.isLocalStream() ? fl.getLocalStream().hashCode() : fl.getLocalPath().hashCode()))));

		long totalSize = groups.values().stream().map(l -> l.get(0)).mapToLong(FileLink::getSize).sum();
		final AtomicLong transferred = new AtomicLong();
		LocalTime start = LocalTime.now();
		UUID id = UUID.randomUUID();
		getLogger().debug("Sequential upload of {} file(s) with id '{}' started.", groups.size(), id);
		for (List<FileLink> group : groups.values()) {
			FileLink fileLink = group.get(0);

			upload(entity, fileLink, transaction, (b, p) -> {
				double transferredBytes = transferred.addAndGet(b);
				if (progressListener != null) {
					progressListener.progress(b, (float) (transferredBytes / totalSize));
				}
			});

			for (FileLink other : group.subList(1, group.size())) {
				other.setRemotePath(fileLink.getRemotePath());
			}
		}
		getLogger().debug("Sequential upload with id '{}' finished in {}.", id,
				Duration.between(start, LocalTime.now()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uploadParallel(Entity entity, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		Map<InputStream, List<FileLink>> groups = fileLinks.stream().filter(FileLink::isLocal)
				.collect(Collectors.groupingBy(FileLink::getLocalStream));

		long totalSize = groups.values().stream().map(l -> l.get(0)).mapToLong(FileLink::getSize).sum();
		final AtomicLong transferred = new AtomicLong();
		List<Callable<Void>> downloadTasks = new ArrayList<>();
		for (List<FileLink> group : groups.values()) {
			downloadTasks.add(() -> {
				FileLink fileLink = group.get(0);

				upload(entity, fileLink, transaction, (b, p) -> {
					double transferredBytes = transferred.addAndGet(b);
					if (progressListener != null) {
						progressListener.progress(b, (float) (transferredBytes / totalSize));
					}
				});

				for (FileLink other : group.subList(1, group.size())) {
					other.setRemotePath(fileLink.getRemotePath());
				}

				return null;
			});
		}

		ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		LocalTime start = LocalTime.now();
		UUID id = UUID.randomUUID();
		getLogger().debug("Parallel upload of {} file(s) with id '{}' started.", groups.size(), id);
		try {
			List<Throwable> errors = executorService.invokeAll(downloadTasks).stream().map(future -> {
				try {
					future.get();
					return null;
				} catch (ExecutionException | InterruptedException e) {
					getLogger().error("Upload of failed due to: " + e.getMessage(), e);
					return e;
				}
			}).filter(Objects::nonNull).collect(Collectors.toList());

			if (!errors.isEmpty()) {
				throw new IOException("Upload failed for '" + errors.size() + "' files.");
			}
			getLogger().debug("Parallel upload with id '{}' finished in {}.", id,
					Duration.between(start, LocalTime.now()));
		} catch (InterruptedException e) {
			throw new IOException("Unable to upload files due to: " + e.getMessage(), e);
		} finally {
			executorService.shutdown();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Entity entity, Collection<FileLink> fileLinks, Transaction transaction) {
		fileLinks.stream().filter(FileLink::isRemote)
				.collect(groupingBy(FileLink::getRemotePath, reducing((fl1, fl2) -> fl1))).values().stream()
				.filter(Optional::isPresent).map(Optional::get).forEach(fl -> delete(entity, fl, transaction));
	}

	protected abstract Logger getLogger();

	/**
	 * Uploads given {@link FileLink}. The upload progress may be traced with a
	 * progress listener.
	 *
	 * @param elemId           Used for security checks.
	 * @param fileLink         The {@code FileLink} to upload.
	 * @param transaction      The {@link Transaction} to use.
	 * @param progressListener The progress listener.
	 * @throws IOException Thrown if unable to upload file.
	 */
	protected void upload(Entity entity, FileLink fileLink, Transaction transaction, ProgressListener progressListener)
			throws IOException {
		if (fileLink.isRemote()) {
			// nothing to do
			return;
		} else if (!fileLink.isLocal()) {
			throw new IllegalArgumentException("File link does not have a local path.");
		}

		try (InputStream sourceStream = getTracedStream(fileLink, progressListener)) {
			getLogger().debug("Starting upload of file '{}'.", fileLink.getFileName());
			LocalTime start = LocalTime.now();
			upload(entity, fileLink, sourceStream, transaction);
			getLogger().debug("File '{}' successfully uploaded in {} to '{}'.", fileLink.getFileName(),
					Duration.between(start, LocalTime.now()), fileLink.getRemotePath());
		}
	}

	protected abstract void upload(Entity entity, FileLink fileLink, InputStream inputStream, Transaction transaction)
			throws IOException;

	/**
	 * Retrieves an {@link InputStream} from a {@link FileLink} and wraps it into a
	 * {@link TracedInputStream} if a {@link ProgressListener} is available.
	 * 
	 * @param fileLink
	 * @param progressListener
	 * @return
	 * @throws FileNotFoundException
	 */
	private InputStream getTracedStream(FileLink fileLink, ProgressListener progressListener)
			throws FileNotFoundException {
		if (progressListener == null) {
			return fileLink.getInputStream();
		} else {
			return new TracedInputStream(fileLink.getInputStream(), progressListener, fileLink.getSize());
		}
	}

	/**
	 * Calculates the total download size for given {@link FileLink} groups.
	 *
	 * @param entity Used for security checks.
	 * @param groups The {@code FileLink} groups.
	 * @return The total download size is returned.
	 * @throws IOException Thrown if unable to load the file size.
	 */
	protected long calculateDownloadSize(Entity entity, Map<String, List<FileLink>> groups, Transaction transaction)
			throws IOException {
		List<FileLink> links = groups.values().stream().map(l -> l.get(0)).collect(Collectors.toList());
		long totalSize = 0;
		for (FileLink fileLink : links) {
			loadSize(entity, fileLink, transaction);
			// overflow may occur in case of total size exceeds 9223 PB!
			totalSize = Math.addExact(totalSize, fileLink.getSize());
		}

		return totalSize;
	}

}
