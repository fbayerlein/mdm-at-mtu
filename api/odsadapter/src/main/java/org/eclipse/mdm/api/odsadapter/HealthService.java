package org.eclipse.mdm.api.odsadapter;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.DataSourceHealth;
import org.eclipse.mdm.api.dflt.model.ExtendedHealth;
import org.eclipse.mdm.api.dflt.model.Health;
import org.eclipse.mdm.api.odsadapter.utils.OdsJsonMessageBodyProvider;

public class HealthService {

	private WebTarget odsHealthEndpoint;

	public HealthService(String healthURL) {

		try {
			Client client = ClientBuilder.newBuilder().register(OdsJsonMessageBodyProvider.class).build();
			odsHealthEndpoint = client.target(healthURL);
			odsHealthEndpoint.request().get(DataSourceHealth.class);
		} catch (Exception e) {
			// set endpoint to null, if ods health endpoint is not available
			odsHealthEndpoint = null;
		}

	}

	public Health getCurrentHealth() {

		try {

			if (odsHealthEndpoint == null) {
				Health health = new Health();
				health.setHost(InetAddress.getLocalHost().getHostName());
				health.setStatus("OK");
				return health;

			} else {
				ExtendedHealth health = new ExtendedHealth();
				health.setHost(InetAddress.getLocalHost().getHostName());
				health.setStatus("OK");

				long start = System.currentTimeMillis();
				DataSourceHealth odsHealth = odsHealthEndpoint.queryParam("include", "sessions").request()
						.get(DataSourceHealth.class);
				long odsLatency = System.currentTimeMillis() - start;
				health.setActiveODSSessions(odsHealth.getSessions().getCurrent());
				health.setMaxODSSessions(odsHealth.getSessions().getMax());
				health.setOdsLatency(odsLatency);
				health.setOdsServerUptime(getODsServerUptimeFromSeconds(odsHealth.getUptimeSeconds()));
				return health;
			}

		} catch (Exception e) {
			throw new DataAccessException("OdsServer not available! ", e);
		}

	}

	private String getODsServerUptimeFromSeconds(long uptimeSeconds) {
		int days = (int) TimeUnit.SECONDS.toDays(uptimeSeconds);
		long hours = TimeUnit.SECONDS.toHours(uptimeSeconds) - (days * 24);
		long minutes = TimeUnit.SECONDS.toMinutes(uptimeSeconds) - (TimeUnit.SECONDS.toHours(uptimeSeconds) * 60);
		long seconds = TimeUnit.SECONDS.toSeconds(uptimeSeconds) - (TimeUnit.SECONDS.toMinutes(uptimeSeconds) * 60);

		return String.format("%1sd %2sh %3sm %4ss", days, hours, minutes, seconds);
	}

}
