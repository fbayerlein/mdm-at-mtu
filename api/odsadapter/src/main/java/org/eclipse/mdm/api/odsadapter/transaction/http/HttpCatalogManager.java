/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.http;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.Query;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.CatalogSensor;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.CatalogManager;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSEnumerations;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpUtils;
import org.eclipse.mdm.api.odsadapter.utils.ODSUtils;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;

import ods.Ods;
import ods.Ods.DataTypeEnum;
import ods.Ods.Model;
import ods.Ods.Model.Attribute;
import ods.Ods.Model.Builder;
import ods.Ods.Model.RelationTypeEnum;

/**
 * Used to create, update or delete {@link CatalogComponent},
 * {@link CatalogSensor} and {@link CatalogAttribute} entities. Modifications of
 * the listed types results in modifications of the application model.
 *
 */
public final class HttpCatalogManager implements CatalogManager {

	private final ODSTransaction transaction;

	private Model model;

	/**
	 * Constructor.
	 *
	 * @param transaction The {@link ODSTransaction}.
	 */
	public HttpCatalogManager(ODSTransaction transaction) {
		this.transaction = transaction;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends Entity> void createCatalog(Map<Class<?>, List<T>> entitiesByClassType) throws OdsException {

		Ods.Model.Builder m = Ods.Model.newBuilder();

		List<CatalogComponent> catalogComponents = (List<CatalogComponent>) entitiesByClassType
				.get(CatalogComponent.class);
		if (catalogComponents != null) {
			createCatalogComponents(m, catalogComponents);
		}

		List<CatalogSensor> catalogSensors = (List<CatalogSensor>) entitiesByClassType.get(CatalogSensor.class);
		if (catalogSensors != null) {
			createCatalogSensors(m, catalogSensors);
		}

		List<CatalogAttribute> catalogAttributes = (List<CatalogAttribute>) entitiesByClassType
				.get(CatalogAttribute.class);
		if (catalogAttributes != null) {
			createCatalogAttributes(m, catalogAttributes);
		}

		if (m.getEntitiesCount() > 0) {
			ODSHttpModelManager mm = (ODSHttpModelManager) transaction.getContext().getODSModelManager();

			OdsHttpRequests.request(mm.getSession(), "model-update", m.build());

			model = null;
		}

	}

	@Override
	public void updateCatalogAttributes(List<CatalogAttribute> catalogAttributes) throws OdsException {

		if (catalogAttributes != null) {
			Ods.Model.Builder m = Ods.Model.newBuilder();

			updateCatalogAttributes(m, catalogAttributes);

			if (m.getEntitiesCount() > 0) {
				ODSHttpModelManager mm = (ODSHttpModelManager) transaction.getContext().getODSModelManager();

				OdsHttpRequests.request(mm.getSession(), "model-update", m.build());

				model = null;
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends Deletable> void deleteCatalog(List<T> catalogEntities) throws OdsException {

		Ods.Model.Builder m = Ods.Model.newBuilder();

		Map<Class<?>, List<T>> entitiesByClassType = catalogEntities.stream()
				.collect(Collectors.groupingBy(e -> e.getClass()));

		List<CatalogComponent> catalogComponents = (List<CatalogComponent>) entitiesByClassType
				.get(CatalogComponent.class);
		if (catalogComponents != null) {
			deleteCatalogComponents(m, catalogComponents);
		}

		List<CatalogSensor> catalogSensors = (List<CatalogSensor>) entitiesByClassType.get(CatalogSensor.class);
		if (catalogSensors != null) {
			deleteCatalogSensors(m, catalogSensors);
		}

		List<CatalogAttribute> catalogAttributes = (List<CatalogAttribute>) entitiesByClassType
				.get(CatalogAttribute.class);
		if (catalogAttributes != null) {
			deleteCatalogAttributes(m, catalogAttributes);
		}

		if (m.getEntitiesCount() > 0) {
			ODSHttpModelManager mm = (ODSHttpModelManager) transaction.getContext().getODSModelManager();

			OdsHttpRequests.request(mm.getSession(), "model-delete", m.build());
			model = null;
		}

	}

	/**
	 * Creates for each given {@link CatalogComponent} a corresponding application
	 * element including all required application relations.
	 * 
	 * @param m
	 *
	 * @param catalogComponents The {@code CatalogComponent}s.
	 * @throws OdsException Thrown in case of errors
	 */
	private void createCatalogComponents(Builder m, Collection<CatalogComponent> catalogComponents)
			throws OdsException {
		Map<ContextType, List<CatalogComponent>> catalogComponentsByContextType = catalogComponents.stream()
				.collect(Collectors.groupingBy(CatalogComponent::getContextType));

		for (Entry<ContextType, List<CatalogComponent>> entry : catalogComponentsByContextType.entrySet()) {
			String odsContextTypeName = ODSUtils.CONTEXTTYPES.get(entry.getKey());
			Model.Entity contextRootApplicationElement = getApplicationModel().getEntitiesMap().get(odsContextTypeName);
			Model.Entity contextTemplateComponentApplicationElement = getApplicationModel().getEntitiesMap()
					.get("Tpl" + odsContextTypeName + "Comp");
			String baseElement = "Ao" + odsContextTypeName + "Part";

			for (CatalogComponent catalogComponent : entry.getValue()) {
				Model.Entity entity = createApplicationElement(catalogComponent.getName(), baseElement);

				Model.Relation relation1 = Model.Relation.newBuilder()
						.setEntityAid(contextRootApplicationElement.getAid()).setName(odsContextTypeName)
						.setInverseName(catalogComponent.getName())
						.setBaseName(ODSUtils.CONTEXTTYPES_base.get(entry.getKey())).setInverseBaseName("children")
						.setRelationType(RelationTypeEnum.RT_FATHER_CHILD).setRangeMin(1).setRangeMax(1)
						.setInverseRangeMin(0).setInverseRangeMax(-1).build();

				Model.Relation relation2 = Model.Relation.newBuilder()
						.setEntityAid(contextTemplateComponentApplicationElement.getAid())
						.setName("Tpl" + odsContextTypeName + "Comp").setInverseName(catalogComponent.getName())
						.setRelationType(RelationTypeEnum.RT_INFO).setRangeMin(1).setRangeMax(1).setInverseRangeMin(0)
						.setInverseRangeMax(-1).build();

				m.putEntities(entity.getName(), entity.toBuilder().putRelations(relation1.getName(), relation1)
						.putRelations(relation2.getName(), relation2).build());
			}
		}
	}

	/**
	 * Creates for each given {@link CatalogSensor} a corresponding application
	 * element including all required application relations.
	 *
	 * @param catalogSensors The {@code CatalogSensor}s.
	 * @throws OdsException Thrown in case of errors.
	 */
	private void createCatalogSensors(Builder m, Collection<CatalogSensor> catalogSensors) throws OdsException {
		Map<String, List<CatalogSensor>> catalogSensorsByCatalogComponent = catalogSensors.stream()
				.collect(Collectors.groupingBy(cs -> cs.getCatalogComponent().getName()));

		Model.Entity channelApplicationElement = getApplicationModel().getEntitiesMap().get("MeaQuantity");

		for (Entry<String, List<CatalogSensor>> entry : catalogSensorsByCatalogComponent.entrySet()) {
			Model.Entity contextComponentApplicationElement = getApplicationModel().getEntitiesMap()
					.get(entry.getKey());
			Model.Entity contextTemplateSensorApplicationElement = getApplicationModel().getEntitiesMap()
					.get("TplSensor");
			String baseElement = "AoTestEquipmentPart";

			for (CatalogSensor catalogSensor : entry.getValue()) {
				Model.Entity entity = createApplicationElement(catalogSensor.getName(), baseElement);

				// relation context sensor to context component
				Model.Relation relation1 = Model.Relation.newBuilder()
						.setEntityAid(contextComponentApplicationElement.getAid()).setName(entry.getKey())
						.setInverseName(catalogSensor.getName()).setBaseName("parent_equipment_part")
						.setInverseBaseName("children").setRelationType(RelationTypeEnum.RT_INFO).setRangeMin(1)
						.setRangeMax(1).setInverseRangeMin(0).setInverseRangeMax(-1).build();

				// relation context sensor to template sensor
				Model.Relation relation2 = Model.Relation.newBuilder()
						.setEntityAid(contextTemplateSensorApplicationElement.getAid()).setName("TplSensor")
						.setInverseName(catalogSensor.getName()).setRelationType(RelationTypeEnum.RT_INFO)
						.setRangeMin(1).setRangeMax(1).setInverseRangeMin(0).setInverseRangeMax(-1).build();

				// relation context sensor to channel
				Model.Relation relation3 = Model.Relation.newBuilder().setEntityAid(channelApplicationElement.getAid())
						.setName("MeaQuantity").setInverseName(catalogSensor.getName()).setBaseName("channel")
						.setInverseBaseName("measurement_quantities").setRelationType(RelationTypeEnum.RT_INFO)
						/*
						 * .setRangeMin(0).setRangeMax(1).setInverseRangeMin(0) .setInverseRangeMax(1)
						 */.build();

				m.putEntities(entity.getName(),
						entity.toBuilder().putRelations(relation1.getName(), relation1)
								.putRelations(relation2.getName(), relation2)
								.putRelations(relation3.getName(), relation3).build());
			}
		}
	}

	/**
	 * Creates for each given {@link CatalogAttribute} a corresponding application
	 * attribute.
	 * 
	 * @param m
	 *
	 * @param catalogAttributes The {@code CatalogAttribute}s.
	 * @throws OdsException Thrown in case of errors.
	 */
	private void createCatalogAttributes(Builder m, Collection<CatalogAttribute> catalogAttributes)
			throws OdsException {
		Map<String, List<CatalogAttribute>> catalogAttributesByCatalogComponent = catalogAttributes.stream()
				.collect(Collectors.groupingBy(HttpCatalogManager::getParentName));

		for (Entry<String, List<CatalogAttribute>> entry : catalogAttributesByCatalogComponent.entrySet()) {
			Model.Entity applicationElement = m.getEntitiesMap().get(entry.getKey());
			if (applicationElement == null) {
				applicationElement = getApplicationModel().getEntitiesMap().get(entry.getKey());
			}

			Model.Entity.Builder updatedElement = applicationElement.toBuilder().clearAttributes().clearRelations();

			for (CatalogAttribute catalogAttribute : entry.getValue()) {

				if (ValueType.FILE_RELATION.equals(catalogAttribute.getValueType())) {
					Model.Entity descriptiveFile = getApplicationModel().getEntitiesMap()
							.get(DescriptiveFile.TYPE_NAME);

					if (descriptiveFile == null) {
						throw new OdsException(
								"Catalog attributes of type FILE_RELATION require an existing application element "
										+ DescriptiveFile.TYPE_NAME
										+ ". Please update your openMDM application model version to at least 5.1.0.");
					}

					updatedElement.putRelations(catalogAttribute.getName(),
							Model.Relation.newBuilder().setEntityAid(descriptiveFile.getAid())
									.setName(catalogAttribute.getName())
									.setInverseName(String.format("%s_%s", entry.getKey(), catalogAttribute.getName()))
									.setRelationType(RelationTypeEnum.RT_INFO).setRangeMin(-1).setRangeMax(-1)
									.setInverseRangeMin(-1).setInverseRangeMax(-1).build());

				} else {
					DataTypeEnum dataType = ODSHttpUtils.VALUETYPES.get(catalogAttribute.getValueType());

					Model.Attribute.Builder applicationAttribute = Model.Attribute.newBuilder()
							.setName(catalogAttribute.getName()).setDataType(dataType);

					if (dataType == DataTypeEnum.DT_ENUM || dataType == DataTypeEnum.DS_ENUM) {
						applicationAttribute
								.setEnumeration(ODSEnumerations.getEnumName(catalogAttribute.getEnumerationObject()));
					}
					Optional<Unit> unit = catalogAttribute.getUnit();
					if (unit.isPresent()) {
						applicationAttribute.setUnitId(Long.parseLong(unit.get().getID()));
					} else {
						applicationAttribute.setUnitId(0L);
					}
					updatedElement.putAttributes(applicationAttribute.getName(), applicationAttribute.build());

				}
			}
			if (updatedElement.getAttributesCount() + updatedElement.getRelationsCount() > 0) {
				m.putEntities(updatedElement.getName(), updatedElement.build());
			}
		}
	}

	/**
	 * Updates the application attribute for each given {@link CatalogAttribute}.
	 *
	 * @param catalogAttributes The {@code CatalogAttribute}s.
	 * @throws OdsException Thrown in case of errors.
	 */
	private void updateCatalogAttributes(Builder m, List<CatalogAttribute> catalogAttributes) throws OdsException {
		Map<String, List<CatalogAttribute>> catalogAttributesByCatalogComponent = catalogAttributes.stream()
				.collect(Collectors.groupingBy(HttpCatalogManager::getParentName));

		for (Entry<String, List<CatalogAttribute>> entry : catalogAttributesByCatalogComponent.entrySet()) {
			Model.Entity applicationElement = m.getEntitiesMap().get(entry.getKey());
			if (applicationElement == null) {
				applicationElement = getApplicationModel().getEntitiesMap().get(entry.getKey());
			}

			Model.Entity.Builder updatedElement = applicationElement.toBuilder().clearAttributes().clearRelations();

			for (CatalogAttribute catalogAttribute : entry.getValue()) {

				if (ValueType.FILE_RELATION.equals(catalogAttribute.getValueType())) {

					Model.Relation existingRelation = applicationElement.getRelationsMap()
							.get(catalogAttribute.getName());

					updatedElement.putRelations(existingRelation.getName(), existingRelation);
				} else {
					Model.Attribute.Builder applicationAttribute = applicationElement.getAttributesMap()
							.get(catalogAttribute.getName()).toBuilder();

					Optional<Unit> unit = catalogAttribute.getUnit();
					if (unit.isPresent()) {
						applicationAttribute.setUnitId(ODSHttpConverter.toODSID(unit.get().getID()));
					} else {
						applicationAttribute.setUnitId(ODSHttpConverter.toODSID("0"));
					}

					updatedElement.putAttributes(applicationAttribute.getName(), applicationAttribute.build());
				}
			}

			if (updatedElement.getAttributesCount() + updatedElement.getRelationsCount() > 0) {
				m.putEntities(updatedElement.getName(), updatedElement.build());
			}

		}
	}

	/**
	 * Deletes the corresponding application element for each given
	 * {@link CatalogComponent}. Deleting a {@code CatalogComponent} is only allowed
	 * if it is not used in templates and all of its children could be deleted. So
	 * at first it is tried to delete its {@link CatalogAttribute}s and
	 * {@link CatalogSensor}s. On success it is ensured none of the given {@code
	 * CatalogComponent}s is used in templates. Finally the corresponding
	 * application elements are deleted.
	 *
	 * @param catalogComponents The {@code CatalogComponent}s.
	 * @throws OdsException        Thrown in case of errors.
	 * @throws DataAccessException Thrown in case of errors.
	 */
	private void deleteCatalogComponents(Builder m, Collection<CatalogComponent> catalogComponents)
			throws DataAccessException, OdsException {
		List<CatalogAttribute> attributes = new ArrayList<>();
		List<CatalogSensor> sensors = new ArrayList<>();
		for (CatalogComponent catalogComponent : catalogComponents) {
			attributes.addAll(catalogComponent.getCatalogAttributes());
			sensors.addAll(catalogComponent.getCatalogSensors());
		}
		transaction.delete(sensors);
		transaction.delete(attributes);

		if (areReferencedInTemplates(catalogComponents)) {
			throw new DataAccessException(
					"Unable to delete given catalog components since at least " + "one is used in templates.");
		}

		for (CatalogComponent catalogComponent : catalogComponents) {
			Model.Entity applicationElement = getApplicationModel().getEntitiesMap().get(catalogComponent.getName());

			Model.Entity.Builder toDelete = applicationElement.toBuilder().clearAttributes().clearRelations();

			m.putEntities(toDelete.getName(), toDelete.build());
		}
	}

	/**
	 * Deletes the corresponding application element for each given
	 * {@link CatalogSensor}. Deleting a {@code CatalogSensor} is only allowed if it
	 * is not used in templates and all of its children could be deleted. So at
	 * first it is tried to delete its {@link CatalogAttribute}s. On success it is
	 * ensured none of the given {@code CatalogSensor}s is used in templates.
	 * Finally the corresponding application elements are deleted.
	 *
	 * @param catalogSensors The {@code CatalogSensor}s.
	 * @throws OdsException        Thrown in case of errors.
	 * @throws DataAccessException Thrown in case of errors.
	 */
	private void deleteCatalogSensors(Builder m, Collection<CatalogSensor> catalogSensors)
			throws OdsException, DataAccessException {
		List<CatalogAttribute> attributes = new ArrayList<>();
		for (CatalogSensor catalogSensor : catalogSensors) {
			attributes.addAll(catalogSensor.getCatalogAttributes());
		}
		transaction.delete(attributes);

		if (areReferencedInTemplates(catalogSensors)) {
			throw new DataAccessException(
					"Unable to delete given catalog sensors since at " + "least one is used in templates.");
		}

		for (CatalogSensor catalogSensor : catalogSensors) {
			Model.Entity applicationElement = getApplicationModel().getEntitiesMap().get(catalogSensor.getName());

			Model.Entity.Builder toDelete = applicationElement.toBuilder().clearAttributes().clearRelations();

			m.putEntities(toDelete.getName(), toDelete.build());
		}
	}

	/**
	 * Deletes the corresponding application attributes for each given
	 * {@link CatalogAttribute}. Deleting a {@code CatalogAttribute} is only allowed
	 * if it is not used in templates. So at first it is ensured none of the given
	 * {@code CatalogAttribute}s is used in templates and finally the corresponding
	 * application attributes are deleted.
	 *
	 * @param catalogAttributes The {@code CatalogAttribute}s.
	 * @throws OdsException        Thrown in case of errors.
	 * @throws DataAccessException Thrown in case of errors.
	 */
	private void deleteCatalogAttributes(Builder m, Collection<CatalogAttribute> catalogAttributes)
			throws OdsException, DataAccessException {
		if (areReferencedInTemplates(catalogAttributes)) {
			throw new DataAccessException(
					"Unable to delete given catalog attributes since at least " + "one is used in templates.");
		}

		Map<String, List<CatalogAttribute>> catalogAttributesByParent = catalogAttributes.stream()
				.collect(Collectors.groupingBy(HttpCatalogManager::getParentName));

		for (Entry<String, List<CatalogAttribute>> entry : catalogAttributesByParent.entrySet()) {
			Model.Entity applicationElement = getApplicationModel().getEntitiesMap().get(entry.getKey());

			Model.Entity.Builder toDelete = applicationElement.toBuilder().clearAttributes().clearRelations();

			for (CatalogAttribute catalogAttribute : entry.getValue()) {
				if (ValueType.FILE_RELATION.equals(catalogAttribute.getValueType())) {
					Model.Relation existingRelation = applicationElement.getRelationsMap()
							.get(catalogAttribute.getName());

					if (existingRelation != null) {
						toDelete.putRelations(catalogAttribute.getName(), existingRelation);
					}
				} else {
					Model.Attribute existingAttribute = applicationElement.getAttributesMap()
							.get(catalogAttribute.getName());

					if (existingAttribute != null) {
						toDelete.putAttributes(catalogAttribute.getName(), existingAttribute);
					}
				}
			}
			if (toDelete.getAttributesCount() + toDelete.getRelationsCount() > 0) {
				m.putEntities(toDelete.getName(), toDelete.build());
			}
		}
	}

	/**
	 * Releases cached resources.
	 */
	@Override
	public void clear() {
	}

	/**
	 * Creates a new {@link Entity} with given name and
	 * {@link ods.Ods.BaseModel.Entity}. The returned {@link Entity} will be created
	 * with the three mandatory {@link Attribute}s for 'Id', 'Name' and 'MimeType'.
	 *
	 * @param name        The name of the application element.
	 * @param baseElement The {@link ods.Ods.BaseModel.Entity} the created
	 *                    {@link Attribute} will be derived from.
	 * @return The created {@link Entity} is returned.
	 * @throws OdsException Thrown in case of errors.
	 */
	private Model.Entity createApplicationElement(String name, String baseElement) {
		return Model.Entity.newBuilder().setBaseName(baseElement).setName(name)
				.putAttributes("Id",
						Attribute.newBuilder().setName("Id").setBaseName("id").setDataType(DataTypeEnum.DT_LONGLONG)
								.setUnique(true).setObligatory(true).setAutogenerated(true).build())
				.putAttributes("Name",
						Attribute.newBuilder().setName("Name").setBaseName("name").setLength(255).setObligatory(true)
								.build())
				.putAttributes("MimeType",
						Attribute.newBuilder().setBaseName("mime_type").setName("MimeType")
								.setDataType(DataTypeEnum.DT_STRING).setLength(256).setObligatory(true).build())
				.build();
	}

	/**
	 * Returns the cached {@link Model}.
	 *
	 * @return The {@code Model} is returned.
	 * @throws OdsException Thrown if unable to access the {@code Model}.
	 */
	private Model getApplicationModel() throws OdsException {
		if (model == null) {
			ODSHttpModelManager mm = ((ODSHttpModelManager) transaction.getContext().getODSModelManager());

			model = OdsHttpRequests.request(mm.getSession(), "model-read", null, Model.class);
		}

		return model;
	}

	/**
	 * Checks whether given {@link Entity}s are referenced in templates.
	 *
	 * @param entities The checked entities ({@link CatalogComponent},
	 *                 {@link CatalogSensor} or {@link CatalogAttribute}).
	 * @return Returns {@code true} if at least one entity is referenced in a
	 *         template.
	 * @throws OdsException        Thrown on errors.
	 * @throws DataAccessException Thrown on errors.
	 */
	private boolean areReferencedInTemplates(Collection<? extends Entity> entities) throws DataAccessException {
		Map<EntityType, List<Entity>> entitiesByEntityType = entities.stream()
				.collect(Collectors.groupingBy(transaction.getModelManager()::getEntityType));

		for (Entry<EntityType, List<Entity>> entry : entitiesByEntityType.entrySet()) {
			EntityType source = entry.getKey();
			EntityType target = transaction.getModelManager().getEntityType(source.getName().replace("Cat", "Tpl"));

			Query query = transaction.getContext().getQueryService()
					.orElseThrow(() -> new ServiceNotProvidedException(QueryService.class)).createQuery()
					.selectID(target).join(source, target);

			List<Result> results = query.fetch(Filter.and().add(
					ComparisonOperator.IN_SET.create(source.getIDAttribute(), collectInstanceIDs(entry.getValue()))));
			if (results.size() > 0) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Collect the instance IDs of all given {@link Entity}s.
	 *
	 * @param entities The {@link Entity}s.
	 * @return The instance IDs a {@code String[]} are turned.
	 */
	private static String[] collectInstanceIDs(List<Entity> entities) {
		String[] ids = new String[entities.size()];

		for (int i = 0; i < ids.length; i++) {
			ids[i] = entities.get(i).getID();
		}

		return ids;
	}

	/**
	 * Returns the parent name for given {@link CatalogAttribute}.
	 *
	 * @param catalogAttribute The {@code CatalogAttribute}.
	 * @return The parent name is returned.
	 */
	private static String getParentName(CatalogAttribute catalogAttribute) {
		Optional<CatalogComponent> catalogComponent = catalogAttribute.getCatalogComponent();
		if (catalogComponent.isPresent()) {
			return catalogComponent.get().getName();
		}

		return catalogAttribute.getCatalogSensor()
				.orElseThrow(() -> new IllegalStateException("Parent entity is unknown.")).getName();
	}

}
