/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.eclipse.mdm.api.odsadapter.utils.OdsProtobufMessageBodyProvider.protobuf;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.ApplicationContextFactory;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfigRepositoryLoader;
import org.eclipse.mdm.api.odsadapter.utils.OdsJsonMessageBodyProvider;
import org.eclipse.mdm.api.odsadapter.utils.OdsProtobufMessageBodyProvider;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.highqsoft.corbafileserver.generated.CORBAFileServerIF;

import ods.Ods.ContextVariables;
import ods.Ods.ContextVariables.ContextVariableValue;
import ods.Ods.ErrorInfo;
import ods.Ods.StringArray;

/**
 * ASAM ODS implementation of the {@link ApplicationContextFactory} interface.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public class ODSHttpContextFactory implements ApplicationContextFactory {

	public static final String PARAM_URL = "url";

	public static final String PARAM_USER = "user";

	public static final String PARAM_PASSWORD = "password";

	public static final String PARAM_FOR_USER = "for_user";

	public static final String PARAM_ENTITY_CONFIG_REPO_LOADER = "entity_config_repo_loader";

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSHttpContextFactory.class);

	public ODSHttpContextFactory() {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * <b>Note:</b> Given parameters {@code Map} must contain values for each of the
	 * following keys:
	 *
	 * <ul>
	 * <li>{@value #PARAM_URL}</li>
	 * <li>{@value #PARAM_USER}</li>
	 * <li>{@value #PARAM_PASSWORD}</li>
	 * </ul>
	 *
	 * <b>Note:</b> The following parameters are optional:
	 * <ul>
	 * <li>{@value #PARAM_FOR_USER}</li>
	 * <li>{@value #PARAM_ENTITY_CONFIG_REPO_LOADER}</li>
	 * </ul>
	 *
	 * Listed names are available via public fields of this class.
	 */
	@Override
	public ApplicationContext connect(String sourceName, Map<String, String> parameters) throws ConnectionException {

		// Create a parameters map without password (which should not be visible from
		// this point onwards),
		// leaving the original map untouched:
		Map<String, String> parametersWithoutPassword = new HashMap<>(parameters);
		if (LOGGER.isDebugEnabled()) {
			parametersWithoutPassword.put(PARAM_PASSWORD, "****");
			LOGGER.debug("Connecting to ODS using the connection parameters: {}", Joiner.on(", ")
					.withKeyValueSeparator("=").join(sessionParametersAsMap((parametersWithoutPassword))));
		}
		parametersWithoutPassword.remove(PARAM_PASSWORD);

		WebTarget session = null;
		try {
			Client client = createClient(parameters);
			session = createSession(sourceName, client, sessionParametersAsMap(parameters));

			CORBAFileServerIF fileServer = null; // serviceLocator.resolveFileServer(nameOfService);

			EntityConfigRepositoryLoader repositoryLoader = getEntityConfigRepositoryLoader(
					parametersWithoutPassword.get(PARAM_ENTITY_CONFIG_REPO_LOADER));

			return new ODSHttpContext(sourceName, session, client, fileServer, parameters, repositoryLoader);
		} catch (ConnectionException e) {
			closeSession(session);
			throw e;
		} catch (Exception e) {
			closeSession(session);
			throw new RuntimeException(e);
		}

	}

	private Client createClient(Map<String, String> parameters) {
		return ClientBuilder.newBuilder().register(OdsProtobufMessageBodyProvider.class)
				.register(OdsJsonMessageBodyProvider.class)
				.register(HttpAuthenticationFeature.basic(parameters.get(PARAM_USER), parameters.get(PARAM_PASSWORD)))
				.build();
	}

	public WebTarget createSession(String sourceName, Client client, Map<String, String> parameters)
			throws ConnectionException {

		WebTarget target = client.target(getParameter(parameters, PARAM_URL));

		LOGGER.info("Connecting to ODS Server (url: {}) ...", target.getUri());

		ContextVariables.Builder contextVariables = ContextVariables.newBuilder();
		for (Map.Entry<String, String> e : parameters.entrySet()) {
			if (e.getValue() == null) {
				continue;
			}
			contextVariables.putVariables(e.getKey(), ContextVariableValue.newBuilder()
					.setStringArray(StringArray.newBuilder().addValues(e.getValue())).build());
		}

		Response response = null;
		try {
			response = target.path("ods").request().post(protobuf(contextVariables.build()));
			if (response.getStatus() == 201) {

				WebTarget session = client.target(response.getLocation());

				LOGGER.info("Connection to ODS server '{}' established: {}", sourceName, session.getUri());
				return session;
			} else {
				String responseBody;
				if (OdsProtobufMessageBodyProvider.MEDIA_TYPE.equals(response.getMediaType())
						|| OdsJsonMessageBodyProvider.MEDIA_TYPE.equals(response.getMediaType())) {
					ErrorInfo errorInfo = response.readEntity(ErrorInfo.class);
					responseBody = "(" + errorInfo.getErrCode().name() + ")" + errorInfo.getReason();
				} else {
					responseBody = response.readEntity(String.class);
				}
				throw new ConnectionException(
						"Unable to connect to ODS server due to: HTTP " + response.getStatus() + " - " + responseBody);
			}
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	private EntityConfigRepositoryLoader getEntityConfigRepositoryLoader(String className) {
		if (className != null) {
			try {
				Class<?> clazz = Class.forName(className);

				if (EntityConfigRepositoryLoader.class.isAssignableFrom(clazz)) {
					EntityConfigRepositoryLoader repositoryLoader = (EntityConfigRepositoryLoader) clazz.newInstance();
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Using custom EntityConfigRepositoryLoader of type {}", className);
					}
					return repositoryLoader;
				}
				LOGGER.warn("Class '{}' must implement EntityConfigRepositoryLoader", className);

			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				LOGGER.warn("Class '{}' could not be used as EntityConfigRepositoryLoader", className, e);
				return null;
			}
		}

		return null;
	}

	private Map<String, String> sessionParametersAsMap(Map<String, String> parameters) throws ConnectionException {
		ImmutableMap.Builder<String, String> builder = ImmutableMap.<String, String>builder()
				.put("url", getParameter(parameters, PARAM_URL)).put("USER", getParameter(parameters, PARAM_USER))
				.put("PASSWORD", getParameter(parameters, PARAM_PASSWORD)).put("CREATE_COSESSION_ALLOWED", "TRUE");

		String forUserName = parameters.get(PARAM_FOR_USER);
		if (!Strings.isNullOrEmpty(forUserName)) {
			builder.put("FOR_USER", forUserName);
		}
		return builder.build();
	}

	/**
	 * Closes given ODS Session with catching and logging errors.
	 *
	 * @param sessionUrl The ODS Session that shall be closed.
	 */
	private static void closeSession(WebTarget sessionUrl) {
		if (sessionUrl == null) {
			return;
		}

		try {
			sessionUrl.request().delete(Void.class);
		} catch (WebApplicationException e) {
			LOGGER.warn("Unable to close sesssion due to: " + e.getMessage(), e);
		}
	}

	/**
	 * Reads the property identified by given property name.
	 *
	 * @param parameters The properties {@code Map}.
	 * @param name       The property name.
	 * @return The property value is returned.
	 * @throws ConnectionException Thrown if property does not exist or is empty.
	 */
	private static String getParameter(Map<String, String> parameters, String name) throws ConnectionException {
		String value = parameters.get(name);
		if (value == null || value.isEmpty()) {
			throw new ConnectionException("Connection parameter with name '" + name + "' is either missing or empty.");
		}

		return value;
	}

	@Override
	public ApplicationContext connect(String sourceName, Map<String, String> connectionParameters,
			ApplicationContext contextDst) throws ConnectionException {
		return this.connect(sourceName, connectionParameters);
	}

}