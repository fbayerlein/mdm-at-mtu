/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.filetransfer;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.asam.ods.AoException;
import org.asam.ods.ElemId;
import org.asam.ods.NameValueUnit;
import org.asam.ods.ODSFile;
import org.asam.ods.ODSWriteTransfer;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.query.Aggregation;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AoFileService extends BaseFileService {

	// ======================================================================
	// Class variables
	// ======================================================================

	private static final Logger LOGGER = LoggerFactory.getLogger(AoFileService.class);

	private static final int WRITE_BUFFER_SIZE = 1024 * 1024;

	// ======================================================================
	// Instance variables
	// ======================================================================

	private final ODSContext context;
	private final ODSModelManager modelManager;

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param context  Used for {@link Entity} to {@link ElemId} conversion.
	 * @param transfer The transfer type for up- and downloads.
	 */
	public AoFileService(ODSContext context) {
		this.context = context;
		this.modelManager = context.getODSModelManager();
	}

	// ======================================================================
	// Protected methods
	// ======================================================================

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	public void loadSize(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		try {
			if (fileLink.getSize() > -1) {
				// file size is already known
				return;
			} else if (fileLink.isRemote()) {
				fileLink.setFileSize(ODSConverter.fromODSLong(getODSFile(fileLink, transaction).getSize()));
			} else {
				throw new IllegalArgumentException("File link is neither in local nor remote state: " + fileLink);
			}
		} catch (AoException exc) {
			throw new DataAccessException("Error accessing ODSFile: " + exc.getMessage(), exc);
		}
	}

	@Override
	protected InputStream openRemoteStream(Entity entity, FileLink fileLink, Transaction transaction)
			throws IOException {
		try {
			return new ODSReadTransferInputStream(getODSFile(fileLink, transaction).read());
		} catch (AoException exc) {
			throw new DataAccessException("Error opening remote stream: " + exc.getMessage(), exc);
		}
	}

	@Override
	public void delete(Entity entity, FileLink fileLink, Transaction transaction) {
		if (!fileLink.isRemote()) {
			// nothing to do
			return;
		}

		try {
			getODSFile(fileLink, transaction).remove();
			getLogger().debug("File '{}' sucessfully deleted.", fileLink.getRemotePath());
		} catch (AoException exc) {
			getLogger().warn("Failed to delete remote file.", exc);
		}
	}

	@Override
	protected void upload(Entity entity, FileLink fileLink, InputStream inputStream, Transaction transaction)
			throws IOException {
		try {
			ODSFile odsFile = getODSFile(fileLink, transaction);
			ODSWriteTransfer writeTransfer = odsFile.create();

			try {
				byte[] buffer = new byte[WRITE_BUFFER_SIZE];
				int bytesRead = -1;

				while ((bytesRead = inputStream.read(buffer)) > -1) {
					if (bytesRead > 0) {
						try {
							writeTransfer.putOctectSeq(
									bytesRead < WRITE_BUFFER_SIZE ? ArrayUtils.subarray(buffer, 0, bytesRead) : buffer);
						} catch (AoException exc) {
							throw new DataAccessException("Error writing to ODSFile: " + exc.reason, exc);
						}
					}
				}
			} finally {
				// Close writeTransfer before copying values from ODS storage to entity (see
				// below), as
				// auto-generated values may not be written by the ODS server until close has
				// been called.
				if (writeTransfer != null) {
					try {
						writeTransfer.close();
					} catch (AoException exc) {
						throw new DataAccessException("Error closing ODSWriteTransfer: " + exc.reason, exc);
					}
				}
			}

			MDMFile mdmFile = (MDMFile) fileLink.getRemoteObject();

			EntityType etMdmFile = context.getModelManager()
					.orElseThrow(() -> new DataAccessException("No ModelManager available in context!"))
					.listEntityTypes().stream().filter(et -> et.getName().equals(mdmFile.getTypeName())).findFirst()
					.orElseThrow(() -> new DataAccessException("Cannot find entity '" + mdmFile.getTypeName() + "'."));

			List<String> attrnames = etMdmFile.getAttributes().stream()
					.filter(attr -> !attr.getName().equals(etMdmFile.getIDAttribute().getName()))
					.map(attr -> attr.getName()).collect(Collectors.toList());

			// Copy values (including autogenerated values) from ODS storage to entity:
			for (NameValueUnit nvu : odsFile.getValueSeq(attrnames.toArray(new String[attrnames.size()]))) {
				mdmFile.getValue(nvu.valName).set(ODSConverter
						.fromODSValueSeq(etMdmFile.getAttribute(nvu.valName), Aggregation.NONE, nvu.unit,
								ODSConverter.toODSValueSeq(nvu.value), context.getODSModelManager().getTimeZone())
						.get(0).extractWithDefault(null));
			}

			fileLink.setRemotePath(mdmFile.getLocation());
			fileLink.setFileSize(mdmFile.getSize());
			fileLink.setFileServiceType(FileServiceType.AOFILE);
		} catch (DataAccessException exc) {
			throw exc;
		} catch (Exception exc) {
			throw new DataAccessException("Error uploading ODSFile: " + exc.getMessage(), exc);
		}
	}

	private ODSFile getODSFile(FileLink fileLink, Transaction transaction) {
		Object remoteObject = fileLink.getRemoteObject();
		if (remoteObject == null || !(remoteObject instanceof MDMFile)) {
			throw new DataAccessException("No MDMFile attached to FileLink!");
		}

		if (!(transaction instanceof ODSTransaction)) {
			throw new DataAccessException(String.format("Transaction class is %s, expected ODSTransaction!",
					transaction.getClass().getSimpleName()));
		}
		MDMFile mdmFile = (MDMFile) remoteObject;

		ODSModelManager mm = ((ODSTransaction) transaction).getModelManager();
		if (mm instanceof ODSCorbaModelManager) {
			try {
				return ((ODSCorbaModelManager) mm).getApplElemAccess().getODSFile(toElemID(mdmFile));
			} catch (AoException e) {
				throw new DataAccessException("Cannot get ODSFile for FileLink " + fileLink + ": " + e.reason, e); // TODO
			}
		} else {
			throw new DataAccessException(String.format("ModelManager class is %s, expected ODSCorbaModelManager!",
					mm.getClass().getSimpleName()));
		}
	}

	/**
	 * Creates an ODS entity identity {@link ElemId} object for given
	 * {@link Entity}.
	 *
	 * @param entity The {@code Entity}.
	 * @return The created {@code ElemId} is returned.
	 */
	protected ElemId toElemID(Entity entity) {
		return new ElemId(ODSConverter.toODSLong(((ODSEntityType) modelManager.getEntityType(entity)).getODSID()),
				ODSConverter.toODSID(entity.getID()));
	}
}
