/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.massdata.ExternalComponentData;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.ReadRequest.ValuesMode;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ExtCompFile;
import org.eclipse.mdm.api.base.model.ExternalComponent;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.Query;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

/**
 * Reads mass data specified in {@link ReadRequest}s.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public abstract class ReadRequestHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReadRequestHandler.class);

	protected final ODSModelManager modelManager;
	protected final QueryService queryService;

	/**
	 * Constructor.
	 *
	 * @param modelManager Used to gain access to value matrices.
	 * @param timezone
	 */
	public ReadRequestHandler(ODSModelManager modelManager, QueryService queryService) {
		this.modelManager = modelManager;
		this.queryService = queryService;
	}

	/**
	 * Loads {@link MeasuredValues} as defined in given {@link ReadRequest}.
	 *
	 * @param readRequest The {@code MeasuredValues} request configuration.
	 * @return The loaded {@code MeasuredValues} are returned.
	 * @throws DataAccessException Thrown if unable to load {@code
	 * 		MeasuredValues}     .
	 */
	public List<MeasuredValues> execute(ReadRequest readRequest) throws DataAccessException {
		if (ValuesMode.STORAGE_PRESERVE_EXTCOMPS == readRequest.getValuesMode()) {
			if (readRequest.getStartIndex() != 0) {
				throw new DataAccessException(String.format(
						"Start index of read request is %d, must be 0 for ValuesMode STORAGE_PRESERVE_EXTCOMPS!",
						readRequest.getStartIndex()));
			}

			if (readRequest.getRequestSize() != 0) {
				throw new DataAccessException(String.format(
						"Request size of read request is %d, must be 0 for ValuesMode STORAGE_PRESERVE_EXTCOMPS!",
						readRequest.getRequestSize()));
			}
		}

		// Load ColumnAttributes:
		Map<String, ColumnAttributes> mapColumnAttributes = getColumnAttributes(readRequest);

		if (mapColumnAttributes.isEmpty()) {
			return Collections.emptyList();
		}

		List<MeasuredValues> listMeasuredValues = new ArrayList<>();

		if (ValuesMode.STORAGE_PRESERVE_EXTCOMPS == readRequest.getValuesMode() && externalComponentExists()) {
			List<String> processedLcIds = new ArrayList<>();
			Map<String, List<ExternalComponentData>> mapExternalComponents = loadExternalComponents(
					mapColumnAttributes);
			LOGGER.debug("Loaded external components for {} localcolumns.", mapExternalComponents.size());
			// Create MeasuredValues for local columns whose external components have been
			// loaded above:
			for (Map.Entry<String, List<ExternalComponentData>> entry : mapExternalComponents.entrySet()) {
				String lcId = entry.getKey();
				List<ExternalComponentData> listExtComps = entry.getValue();
				listMeasuredValues
						.add(ODSConverter.fromExternalComponents(listExtComps, mapColumnAttributes.get(lcId)));
				processedLcIds.add(lcId);
			}

			// implict
			for (Map.Entry<String, ColumnAttributes> entry : mapColumnAttributes.entrySet()) {
				String lcId = entry.getKey();
				ColumnAttributes ca = entry.getValue();
				if (entry.getValue().getSequenceRepresentation().isImplicit()) {

					ScalarType scalarType = ca.getRawDataType();
					if (scalarType == ScalarType.UNKNOWN) {
						scalarType = ca.getDataType();
					}

					listMeasuredValues.add(scalarType.createMeasuredValues(ca.getName(), ca.getUnitName(),
							ca.getSequenceRepresentation(), ca.getGenerationParameters(), ca.isIndependentColumn(),
							ca.getAxisType(), ca.getGlobalFlag(), Collections.emptyList(), ca.getMimeType()));
					processedLcIds.add(lcId);
				}
			}
			// remove processed localcolumns
			processedLcIds.forEach(lcId -> mapColumnAttributes.remove(lcId));
		}

		listMeasuredValues.addAll(loadByValueMatrix(readRequest, mapColumnAttributes));

		return listMeasuredValues;
	}

	protected abstract List<MeasuredValues> loadByValueMatrix(ReadRequest readRequest,
			Map<String, ColumnAttributes> mapColumnAttributes);

	/**
	 * Loads External Components for all ColumnAttributes which satisfy
	 * {@link SequenceRepresentation#isExternal()}
	 * 
	 * @param mapColumnAttributes
	 * @return a map with the LocalColumn.Id as key and a list of its
	 *         ExternalComponents as value
	 */
	private Map<String, List<ExternalComponentData>> loadExternalComponents(
			Map<String, ColumnAttributes> mapColumnAttributes) {
		Map<String, List<ExternalComponentData>> mapExternalComponents;
		EntityType localColumnEntityType = modelManager.getEntityType("LocalColumn");
		EntityType externalComponentEntityType = modelManager.getEntityType("ExternalComponent");
		boolean hasExtCompFiles = modelManager.listEntityTypes().stream()
				.filter(et -> ExtCompFile.TYPE_NAME.equals(et.getName())).findFirst().isPresent();

		Relation relExternalComponentToLocalColumn = externalComponentEntityType.getRelation(localColumnEntityType);

		List<Attribute> listExtCompAttributes = externalComponentEntityType.getAttributes();
		Optional<Relation> optRelValuesExtCompFile = hasExtCompFiles
				? externalComponentEntityType.getRelations().stream()
						.filter(r -> ExternalComponent.REL_VALUESFILE.equals(r.getName())).findFirst()
				: Optional.empty();
		Optional<Relation> optRelFlagsExtCompFile = hasExtCompFiles
				? externalComponentEntityType.getRelations().stream()
						.filter(r -> ExternalComponent.REL_FLAGSFILE.equals(r.getName())).findFirst()
				: Optional.empty();

		boolean hasExtCompBitCount = (listExtCompAttributes.stream()
				.filter(a -> ExternalComponent.ATTR_BITCOUNT.equals(a.getName())).count() > 0);
		boolean hasExtCompBitOffset = (listExtCompAttributes.stream()
				.filter(a -> ExternalComponent.ATTR_BITOFFSET.equals(a.getName())).count() > 0);

		List<String> ids = new ArrayList<>();
		for (Map.Entry<String, ColumnAttributes> me : mapColumnAttributes.entrySet()) {
			if (me.getValue().getSequenceRepresentation().isExternal()) {
				ids.add(me.getKey());
			}
		}

		if (ids.isEmpty()) {
			// no external sequence representations -> no external components to load
			return Collections.emptyMap();
		}

		Query query = queryService.createQuery().select(externalComponentEntityType.getAttributes())
				.select(relExternalComponentToLocalColumn.getAttribute())
				.order(relExternalComponentToLocalColumn.getAttribute(),
						externalComponentEntityType.getAttribute(ExternalComponent.ATTR_ORDINALNUMBER));
		if (optRelValuesExtCompFile.isPresent()) {
			query.select(optRelValuesExtCompFile.get().getAttribute());
		}
		if (optRelFlagsExtCompFile.isPresent()) {
			query.select(optRelFlagsExtCompFile.get().getAttribute());
		}

		mapExternalComponents = query
				.fetch(Filter.and()
						.add(ComparisonOperator.IN_SET
								.create(relExternalComponentToLocalColumn.getAttribute(), ids.toArray(new String[0]))))
				.stream()
				.map(r -> new ImmutablePair<String, ExternalComponentData>(
						r.getValue(relExternalComponentToLocalColumn.getAttribute()).extract(),
						new ExternalComponentData(
								r.getValue(externalComponentEntityType
										.getAttribute(ExternalComponent.ATTR_TYPESPECIFICATION)).extract(),
								r.getValue(externalComponentEntityType.getAttribute(ExternalComponent.ATTR_LENGTH))
										.extract(),
								r.getValue(externalComponentEntityType.getAttribute(ExternalComponent.ATTR_STARTOFFSET))
										.extractWithDefault(null),
								r.getValue(externalComponentEntityType.getAttribute(ExternalComponent.ATTR_BLOCKSIZE))
										.extractWithDefault(null),
								r.getValue(
										externalComponentEntityType.getAttribute(ExternalComponent.ATTR_VALUESPERBLOCK))
										.extractWithDefault(null),
								r.getValue(externalComponentEntityType.getAttribute(ExternalComponent.ATTR_VALUEOFFSET))
										.extractWithDefault(null),
								r.getValue(externalComponentEntityType.getAttribute(ExternalComponent.ATTR_FILENAMEURL))
										.extractWithDefault(null),
								r.getValue(externalComponentEntityType
										.getAttribute(ExternalComponent.ATTR_FLAGSFILENAMEURL))
										.extractWithDefault(null),
								optRelValuesExtCompFile.isPresent()
										? r.getRecord(externalComponentEntityType).getID(optRelValuesExtCompFile.get())
												.orElse(null)
										: null,
								optRelFlagsExtCompFile.isPresent()
										? r.getRecord(externalComponentEntityType).getID(optRelFlagsExtCompFile.get())
												.orElse(null)
										: null,
								r.getValue(externalComponentEntityType
										.getAttribute(ExternalComponent.ATTR_FLAGSSTARTOFFSET))
										.extractWithDefault(null),
								(hasExtCompBitCount
										? r.getValue(externalComponentEntityType
												.getAttribute(ExternalComponent.ATTR_BITCOUNT)).extractWithDefault(null)
										: null),
								(hasExtCompBitOffset ? r
										.getValue(externalComponentEntityType
												.getAttribute(ExternalComponent.ATTR_BITOFFSET))
										.extractWithDefault(null) : null))))
				.collect(Collectors.groupingBy(Pair::getKey, Collectors.mapping(Pair::getValue, Collectors.toList())));
		return mapExternalComponents;
	}

	private boolean externalComponentExists() {
		try {
			modelManager.getEntityType("ExternalComponent");
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	boolean contains(Set<String> keys, String key) {
		return keys.contains(key);
	}

	/**
	 * Gets the {@link ColumnAttributes} for the local columns (channels) specified
	 * in a {@link ReadRequest}.
	 * 
	 * @param readRequest The {@link ReadRequest} to use.
	 * @return A map with the LocalColumnIDs as keys.
	 */
	protected Map<String, ColumnAttributes> getColumnAttributes(ReadRequest readRequest) {
		Map<String, ColumnAttributes> mapColumnAttributes = new HashMap<>();

		EntityType localColumnEntityType = modelManager.getEntityType("LocalColumn");
		Attribute idAttr = localColumnEntityType.getIDAttribute();
		Attribute nameAttr = localColumnEntityType.getNameAttribute();
		Attribute submatrixAttr = localColumnEntityType.getAttribute("SubMatrix");

		// Don't query GenerationParameters together with other non-ID attributes as
		// Avalon dislikes this:
		Query query1 = queryService.createQuery().select(Lists.newArrayList(idAttr, nameAttr,
				localColumnEntityType.getAttribute("SequenceRepresentation"),
				localColumnEntityType.getAttribute("IndependentFlag"), localColumnEntityType.getAttribute("GlobalFlag"),
				localColumnEntityType.getAttribute("axistype"), localColumnEntityType.getAttribute("RawDatatype"),
				localColumnEntityType.getAttribute("MeaQuantity"), localColumnEntityType.getAttribute("MimeType")));

		// OpenATFX can't handle multiple search conditions, so use just one...
		Filter filter = Filter.and().add(
				ComparisonOperator.EQUAL.create(submatrixAttr, Long.valueOf(readRequest.getChannelGroup().getID())));

		Set<String> setColumnNames = readRequest.isLoadAllChannels() ? null
				: readRequest.getChannels().keySet().stream().map(c -> c.getName()).collect(Collectors.toSet());

		// ...and pick the columns of interest "manually":
		for (Result result : query1.fetch(filter)) {
			Map<String, Value> mapValues = result.getRecord(localColumnEntityType).getValues();

			String columnName = mapValues.get("Name").extract();

			if (null == setColumnNames || setColumnNames.contains(columnName)) {
				String columnId = result.getRecord(localColumnEntityType).getID();
				SequenceRepresentation seqRep = ValuesMode.CALCULATED == readRequest.getValuesMode()
						? SequenceRepresentation.EXPLICIT
						: mapValues.get("SequenceRepresentation").extract();

				ColumnAttributes ca = new ColumnAttributes(columnName, columnId, seqRep, new double[0],
						((short) mapValues.get("IndependentFlag").extract() != 0), mapValues.get("axistype").extract(),
						mapValues.get("GlobalFlag").extractWithDefault(null), mapValues.get("RawDatatype").extract(),
						mapValues.get("MeaQuantity").extract(), mapValues.get("MimeType").extract());

				// ca does not yet have the correct unit id and name set. This will be done
				// later in applyUnitNamesAndDataTypesFallback
				mapColumnAttributes.put(mapValues.get("Id").extract(), ca);
			}
		}

		if (ValuesMode.CALCULATED != readRequest.getValuesMode()) {
			Query query2 = queryService.createQuery().select(idAttr,
					localColumnEntityType.getAttribute("GenerationParameters"));

			for (Result result : query2.fetch(filter)) {
				Map<String, Value> mapValues = result.getRecord(localColumnEntityType).getValues();

				ColumnAttributes ca = mapColumnAttributes.get(mapValues.get("Id").extract());

				if (ca != null) {
					ca.setGenerationParameters(mapValues.get("GenerationParameters").extract());
				}
			}
		}

		applyUnitNamesAndDataTypesFallback(mapColumnAttributes.values(), readRequest);

		return mapColumnAttributes;
	}

	/**
	 * Loads and sets the unit property of the given ColumnAttributes. This method
	 * does not use Queries with joins as openATFX does not support it. Instead, two
	 * queries are used. (LocalColumn.Id, LocalColumn.MeaQuantity) ->
	 * (MeaQuantity.Id, MeaQuantity.Unit) -> (Unit.Id, Unit.Name)
	 * 
	 * @param readRequest
	 * 
	 * @param mapColumnAttributes ColumnAttributes to set the unit.
	 */
	private void applyUnitNamesAndDataTypesFallback(Collection<ColumnAttributes> columnAttributes,
			ReadRequest readRequest) {
		if (columnAttributes.isEmpty()) {
			return;
		}

		EntityType meaQuantityEntityType = modelManager.getEntityType(Channel.class);

		Query queryMq = queryService.createQuery().select(meaQuantityEntityType.getIDAttribute(),
				meaQuantityEntityType.getAttribute("DataType"), meaQuantityEntityType.getAttribute("Unit"));
		Filter filterMq = Filter.and().add(ComparisonOperator.IN_SET.create(meaQuantityEntityType.getIDAttribute(),
				columnAttributes.stream().map(ca -> ca.getMeaQuantityId()).toArray(String[]::new)));

		Map<String, String> mqId2requestedUnitId = new HashMap<>();
		Map<String, String> mqId2persistentUnitId = new HashMap<>();
		Map<String, ScalarType> mqId2datatype = new HashMap<>();

		for (Result result : queryMq.fetch(filterMq)) {
			Map<String, Value> mapValues = result.getRecord(meaQuantityEntityType).getValues();
			String meaquantityId = result.getRecord(meaQuantityEntityType).getID();
			String persistedUnitId = mapValues.get("Unit").extract();

			mqId2persistentUnitId.put(meaquantityId, persistedUnitId);

			// read actually requested Unit id and apply
			mqId2requestedUnitId.put(meaquantityId, persistedUnitId);
			readRequest.getChannels().entrySet().stream().filter(e -> e.getKey().getID().equals(meaquantityId))
					.filter(e -> e.getValue() != null).map(e -> e.getValue()).findFirst()
					.ifPresent(u -> mqId2requestedUnitId.put(meaquantityId, u.getID()));

			mqId2datatype.put(result.getRecord(meaQuantityEntityType).getID(), mapValues.get("DataType").extract());
		}

		EntityType unitEntityType = modelManager.getEntityType(Unit.class);

		Query queryUnit = queryService.createQuery().select(unitEntityType.getIDAttribute(),
				unitEntityType.getNameAttribute());

		// Load requested and persisted Units
		Filter filterUnit = Filter.and()
				.add(ComparisonOperator.IN_SET.create(unitEntityType.getIDAttribute(),
						Stream.concat(mqId2requestedUnitId.values().stream(), mqId2persistentUnitId.values().stream())
								.distinct().toArray(String[]::new)));

		Map<String, String> mqId2unitName = new HashMap<>();

		for (Result result : queryUnit.fetch(filterUnit)) {
			Map<String, Value> mapValues = result.getRecord(unitEntityType).getValues();

			mqId2unitName.put(result.getRecord(unitEntityType).getID(), mapValues.get("Name").extract());
		}

		columnAttributes.forEach(ca -> {
			ca.setDataType(mqId2datatype.get(ca.getMeaQuantityId()));

			String unitId = mqId2requestedUnitId.get(ca.getMeaQuantityId());
			ca.setUnitId(ODSHttpConverter.toODSID(unitId));
			ca.setUnitName(mqId2unitName.get(unitId));

			if (!ca.getDataType().isNumericalType()) {
				String persistedUnitId = mqId2persistentUnitId.get(ca.getMeaQuantityId());

				if (!unitId.equals(persistedUnitId))
					throw new DataAccessException("Cannot convert Channel of datatype " + ca.getDataType() + " from "
							+ toString(mqId2unitName.get(persistedUnitId)) + " to unit '" + mqId2unitName.get(unitId)
							+ "'.");
			}

		});
	}

	private String toString(String unitName) {
		if (unitName == null) {
			return "no unit";
		} else {
			return "unit '" + unitName + "'";
		}
	}
}
