/*******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.mdm.api.odsadapter.search;

import java.util.Collections;
import java.util.List;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.dflt.model.SystemParameter;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;

/**
 * @author jz
 *
 */
public class SystemParameterSearchQuery extends BaseEntitySearchQuery {

	private ODSModelManager modelManager;

	protected SystemParameterSearchQuery(ODSModelManager modelManager, QueryService queryService, ContextState contextState) {
		super(modelManager, queryService, SystemParameter.class, SystemParameter.class);
		this.modelManager = modelManager;
		// context
//		addJoinConfig(contextState);
	}
	
	@Override
	public List<EntityType> listEntityTypes() {
		return Collections.singletonList(this.modelManager.getEntityType(SystemParameter.class));
	}
}
