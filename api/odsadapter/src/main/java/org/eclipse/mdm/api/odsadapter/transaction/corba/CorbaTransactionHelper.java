/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.corba;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.asam.ods.AoException;
import org.asam.ods.ApplicationStructure;
import org.asam.ods.ElemId;
import org.asam.ods.InstanceElement;
import org.asam.ods.SetType;
import org.asam.ods.T_LONGLONG;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.ODSContextFactory;
import org.eclipse.mdm.api.odsadapter.ODSCorbaContext;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityFactory;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.DeleteStatement;
import org.eclipse.mdm.api.odsadapter.transaction.InsertStatement;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.transaction.OdsTransactionHelper;
import org.eclipse.mdm.api.odsadapter.transaction.UpdateStatement;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;

public class CorbaTransactionHelper implements OdsTransactionHelper {

	private final ODSCorbaContext context;

	public CorbaTransactionHelper(ODSCorbaContext context) {
		this.context = context;
	}

	@Override
	public List<String> loadRelatedIds(EntityType entityType, String iid, String relationName) {
		// TODO only works for n-to-m relations. Do we need to support 1-to-n relations
		// in this method?
		try {
			ElemId elemId = new ElemId(ODSConverter.toODSID(entityType.getId()), ODSConverter.toODSID(iid));
			T_LONGLONG[] instanceIds = context.getAoSession().getApplElemAccess().getRelInst(elemId, relationName);

			return Stream.of(instanceIds).map(ODSConverter::fromODSLong).map(l -> l.toString())
					.collect(Collectors.toList());
		} catch (AoException e) {
			throw new DataAccessException("Error loading related entities" + e.reason, e);
		}
	}

	@Override
	public <T extends Entity> void processNtoMRelations(Collection<T> entities) {
		for (Entity e : entities) {
			context.getODSModelManager().getEntityType(e).getRelations().stream().filter(Relation::isNtoM)
					.forEach(r -> processNtoMRelation(e, r));
		}
	}

	private void processNtoMRelation(Entity entity, Relation relation) {

		List<? extends Deletable> removedRelatedEntities = ODSEntityFactory.extract(entity).getNtoMStore().getRemoved()
				.getOrDefault(relation.getName(), Collections.emptyList());
		List<? extends Deletable> addedRelatedEntities = ODSEntityFactory.extract(entity).getNtoMStore().getAdded()
				.getOrDefault(relation.getName(), Collections.emptyList());

		try {
			ODSEntityType entityType = ((ODSEntityType) context.getODSModelManager().getEntityType(entity));

			T_LONGLONG[] removedInstIds = removedRelatedEntities.stream().map(Entity::getID).map(ODSConverter::toODSID)
					.toArray(T_LONGLONG[]::new);

			ElemId elemId = new ElemId(ODSConverter.toODSLong(entityType.getODSID()),
					ODSConverter.toODSID(entity.getID()));

			if (removedInstIds.length > 0) {
				context.getAoSession().getApplElemAccess().setRelInst(elemId, relation.getName(), removedInstIds,
						SetType.REMOVE);
			}

			T_LONGLONG[] addedInstIds = addedRelatedEntities.stream().map(Entity::getID).map(ODSConverter::toODSID)
					.toArray(T_LONGLONG[]::new);

			if (addedInstIds.length > 0) {
				context.getAoSession().getApplElemAccess().setRelInst(elemId, relation.getName(), addedInstIds,
						SetType.APPEND);
			}
		} catch (AoException e) {
			throw new DataAccessException("Cannot process NtoM relations" + e.reason, e);
		}
	}

	@Override
	public void startTransaction() throws OdsException {
		try {
			context.getAoSession().startTransaction();
		} catch (AoException e) {
			throw new OdsException(e.reason, e);
		}
	}

	@Override
	public void commitTransaction() throws OdsException {
		try {
			context.getAoSession().commitTransaction();
		} catch (AoException e) {
			throw new OdsException(e.reason, e);
		}
	}

	@Override
	public void abortTransaction() throws OdsException {
		try {
			context.getAoSession().abortTransaction();
		} catch (AoException e) {
			throw new OdsException(e.reason, e);
		}
	}

	@Override
	public InsertStatement getInsertStatement(ODSTransaction transaction, EntityType entityType) {
		return new CorbaInsertStatement(transaction, entityType);
	}

	@Override
	public UpdateStatement getUpdateStatement(ODSTransaction transaction, EntityType entityType,
			boolean ignoreChildren) {
		return new CorbaUpdateStatement(transaction, entityType, ignoreChildren);
	}

	@Override
	public DeleteStatement getDeleteStatement(ODSTransaction transaction, EntityType entityType,
			boolean ignoreChildren) {
		return new CorbaDeleteStatement(transaction, entityType, ignoreChildren);
	}

	@Override
	public Map<? extends Entity, ? extends String> getLinks(ODSEntityType et, List<Entity> entities) {
		Map<Entity, String> linkMap = new HashMap<>();

		List<ElemId> elemIds = entities.stream().map(e -> new ElemId(ODSConverter.toODSLong(et.getODSID()),
				ODSConverter.toODSLong(Long.parseLong(e.getID())))).collect(Collectors.toList());

		String serverRoot = context.getParameters().get(ODSContextFactory.PARAM_NAMESERVICE) + "/"
				+ context.getParameters().get(ODSContextFactory.PARAM_SERVICENAME);

		try {
			ApplicationStructure appStructure;
			try {
				appStructure = context.getODSModelManager().getAoSession().getApplicationStructure();
			} catch (AoException e) {
				throw new DataAccessException("Could not load application structure! Reason: " + e.reason, e);
			}
			InstanceElement[] instances = appStructure.getInstancesById(elemIds.toArray(new ElemId[0]));

			for (InstanceElement ie : instances) {
				String id = Long.toString(ODSConverter.fromODSLong(ie.getId()));
				String asamPath = serverRoot + ie.getAsamPath();
				entities.stream().filter(e -> e.getID().equals(id)).findFirst()
						.ifPresent(e -> linkMap.put(e, asamPath));
			}
		} catch (AoException e) {
			throw new DataAccessException("Could not load links for entities: " + entities + ". Reason: " + e.reason,
					e);
		}
		return linkMap;
	}
}
