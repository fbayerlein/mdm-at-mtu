/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.http;

import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.Role;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.transaction.UpdateStatement;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ods.Ods.DataMatrices;
import ods.Ods.DataMatrix;

/**
 * Insert statement is used to update entities and their children.
 *
 */
public final class HttpUpdateStatement extends UpdateStatement {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpUpdateStatement.class);

	/**
	 * Constructor.
	 *
	 * @param transaction    The owning {@link ODSTransaction}.
	 * @param entityType     The associated {@link EntityType}.
	 * @param ignoreChildren If {@code true}, then child entities won't be
	 *                       processed.
	 */
	public HttpUpdateStatement(ODSTransaction transaction, EntityType entityType, boolean ignoreChildren) {
		super(transaction, entityType, ignoreChildren);
	}

	/**
	 * Constructor.
	 *
	 * @param transaction The owning {@link ODSTransaction}.
	 * @param entityType  The associated {@link EntityType}.
	 */
	public HttpUpdateStatement(ODSTransaction transaction, EntityType entityType) {
		super(transaction, entityType);
	}

	@Override
	protected void execute(int entitySize) throws OdsException, DataAccessException, IOException {
		// TODO tracing progress in this method...

		long aID = getEntityType().getODSID();

		if (!fileLinkToUpload.isEmpty()) {
			getTransaction().getUploadService().uploadParallel(fileLinkToUpload, getTransaction(), null);
		}

		DataMatrix.Builder dm = DataMatrix.newBuilder();
		dm.setAid(aID);

		for (Entry<String, List<Value>> entry : updateMap.entrySet()) {
			if (nonUpdatableRelationNames.contains(entry.getKey())) {
				// skip "empty" informative relation sequence
				continue;
			}
			if (entry.getKey().equalsIgnoreCase(Role.ATTR_SUPERUSER_FLAG)) {
				// skip superuser flag as it cannot be written through the ODS API
				continue;
			}

			Attribute attribute = getEntityType().getAttribute(entry.getKey());

			if (ValueType.FILE_RELATION.equals(attribute.getValueType()) || entry.getValue().stream()
					.filter(v -> ValueType.FILE_RELATION.equals(v.getValueType())).count() > 0) {
				// skip FILE_RELATION pseudo-attributes
				continue;
			}

			// If all values are unmodified, skip this attribute, unless it is the
			// ID-Attribute
			if (!attribute.getName().equals(getEntityType().getIDAttribute().getName())
					&& !entry.getValue().stream().filter(Value::isModified).findAny().isPresent()) {
				LOGGER.trace("skipping " + entry.getKey() + ", because all values are unmodified.");
				continue;
			}

			dm.addColumns(ODSHttpConverter
					.toODSColumn(attribute, entry.getValue(),
							getTransaction().getContext().getODSModelManager().getTimeZone())
					.setName(entry.getKey()).setUnitId(0L).build());
		}

		long start = System.currentTimeMillis();
		ODSHttpModelManager mm = ((ODSHttpModelManager) getTransaction().getContext().getODSModelManager());

		OdsHttpRequests.request(mm.getSession(), "data-update", DataMatrices.newBuilder().addMatrices(dm).build(),
				DataMatrices.class);

		long stop = System.currentTimeMillis();

		LOGGER.debug("{} " + getEntityType() + " instances updated in {} ms.", entitySize, stop - start);

		// delete first to make sure naming collisions do not occur!
		for (List<Deletable> children : childrenToRemove.values()) {
			getTransaction().delete(children);
		}
		for (List<Entity> children : childrenToCreate.values()) {
			getTransaction().create(children);
		}
		for (List<Entity> children : childrenToUpdate.values()) {
			getTransaction().update(children);
		}
	}
}
