/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.adapter.DefaultCore;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.massdata.UpdateRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.ODSHttpContext;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.corba.CorbaUpdateStatement;
import org.eclipse.mdm.api.odsadapter.transaction.http.HttpUpdateStatement;

import com.google.common.base.Strings;

/**
 * Writes mass data specified in {@link WriteRequest}s.
 *
 * @since 5.0.2
 * @author Joachim Zeyn, Peak Solution GmbH
 */
public final class UpdateRequestHandler {

	// ======================================================================
	// Class variables
	// ======================================================================

	private static final String AE_LC_ATTR_INDEPENDENT = "IndependentFlag";
	private static final String AE_LC_ATTR_AXISTYPE = "axistype";
	private static final String AE_LC_ATTR_FLAGS = "Flags";
	private static final String AE_LC_ATTR_GLOBAL_FLAG = "GlobalFlag";

	private static final String AE_LC_ATTR_REPRESENTATION = "SequenceRepresentation";
	private static final String AE_LC_ATTR_PARAMETERS = "GenerationParameters";
	private static final String AE_LC_ATTR_RAWDATATYPE = "RawDatatype";
	private static final String AE_LC_ATTR_VALUES = "Values";
	private static final String AE_EC_ATTR_BITCOUNT = "BitCount";
	private static final String AE_EC_ATTR_BITOFFSET = "BitOffset";
	private static final String[] AE_REMOVE = { AE_LC_ATTR_REPRESENTATION, AE_LC_ATTR_PARAMETERS,
			AE_LC_ATTR_RAWDATATYPE, AE_LC_ATTR_VALUES, AE_EC_ATTR_BITCOUNT, AE_EC_ATTR_BITOFFSET };

	// ======================================================================
	// Instance variables
	// ======================================================================

	private final Collection<Core> cores = new ArrayList<>();
	private final EntityType localColumnEntityType;
	private final UpdateStatement updateStatement;
	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param transaction The owning {@link ODSTransaction}.
	 */
	public UpdateRequestHandler(ODSTransaction transaction) {
		localColumnEntityType = transaction.getModelManager().getEntityType("LocalColumn");

		if (transaction.getContext() instanceof ODSHttpContext) {
			updateStatement = new HttpUpdateStatement(transaction, localColumnEntityType);
		} else {
			updateStatement = new CorbaUpdateStatement(transaction, localColumnEntityType);
		}
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Adds given {@link WriteRequest} to be processed.
	 *
	 * @param writeRequest The {@code WriteRequest}.
	 */
	public void addRequest(UpdateRequest updateRequest) {
		cores.add(createCore(updateRequest));
	}

	/**
	 * Imports given mass data configurations.
	 *
	 * @throws OdsException        Thrown if the execution fails.
	 * @throws DataAccessException Thrown if the execution fails.
	 * @throws IOException         Thrown if a file transfer operation fails.
	 */
	public void execute() throws OdsException, DataAccessException, IOException {
		updateStatement.executeWithCores(cores);
	}

	// ======================================================================
	// Private methods
	// ======================================================================

	/**
	 * Reads given {@link WriteRequest} and prepares a corresponding {@link Core}
	 * for import.
	 *
	 * @param updateRequest The mass data configuration.
	 * @return The created {@code Core} is returned.
	 */
	private Core createCore(UpdateRequest updateRequest) {
		Core core = new DefaultCore(localColumnEntityType);

		core.setID(updateRequest.getInstanceID());
		core.getPermanentStore().set(updateRequest.getChannelGroup());
		core.getMutableStore().set(updateRequest.getChannel());

		Map<String, Value> values = core.getValues();
		values.get(Entity.ATTR_NAME).set(updateRequest.getChannel().getName());
		if (Strings.isNullOrEmpty(updateRequest.getMimeType())) {
			values.remove(Entity.ATTR_MIMETYPE);
		} else {
			values.get(Entity.ATTR_MIMETYPE).set(updateRequest.getMimeType());
		}
		for (String key : AE_REMOVE) {
			values.remove(key);
		}
		values.get(AE_LC_ATTR_INDEPENDENT).set((short) (updateRequest.isIndependent() ? 1 : 0));
		values.get(AE_LC_ATTR_AXISTYPE).set(updateRequest.getAxisType());
		if (updateRequest.areAllValid()) {
			values.get(AE_LC_ATTR_GLOBAL_FLAG).set(updateRequest.getGlobalFlag());
		} else {
			values.get(AE_LC_ATTR_FLAGS).set(updateRequest.getFlags());
			values.get(AE_LC_ATTR_GLOBAL_FLAG).set((short) 15);
		}

		return core;
	}

}
