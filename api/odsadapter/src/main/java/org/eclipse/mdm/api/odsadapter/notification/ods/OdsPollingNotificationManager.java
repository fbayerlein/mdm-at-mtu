/********************************************************************************
 * Copyright (c) 2015-2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.notification.ods;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.base.notification.NotificationException;
import org.eclipse.mdm.api.base.notification.NotificationFilter;
import org.eclipse.mdm.api.base.notification.NotificationListener;
import org.eclipse.mdm.api.base.notification.NotificationService;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig.Key;
import org.eclipse.mdm.api.odsadapter.notification.NotificationEntityLoader;
import org.eclipse.mdm.api.odsadapter.utils.ODSUtils;
import org.eclipse.mdm.api.odsadapter.utils.OdsJsonMessageBodyProvider;
import org.eclipse.mdm.api.odsadapter.utils.OdsProtobufMessageBodyProvider;
import org.glassfish.jersey.media.sse.SseFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ods.notification.OdsNotification.Notification;
import ods.notification.OdsNotification.NotificationPool;

/**
 * Notification manager for handling notifications from the ODS HTTP API using
 * polling.
 * 
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
public class OdsPollingNotificationManager implements NotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OdsPollingNotificationManager.class);

	private final Client client;
	private final WebTarget endpoint;
	private final long pollingInterval;

	private final Map<String, ScheduledFuture<?>> pollers = new HashMap<>();

	private final Map<String, NotificationFilter> registrations = new HashMap<>();

	private final ODSContext context;

	private final NotificationEntityLoader loader;

	private final static int POOL_SIZE = Integer
			.parseInt(System.getProperty("org.eclipse.mdm.api.odsadapter.notifcation.polling_pool_size", "5"));

	private final static ScheduledExecutorService EXECUTOR = new ScheduledThreadPoolExecutor(POOL_SIZE,
			new ThreadFactory() {
				private final String namePrefix = "pool-OdsPollingNotification-thread-";
				private final AtomicInteger threadNumber = new AtomicInteger(1);

				@Override
				public Thread newThread(Runnable r) {
					return new Thread(r, namePrefix + threadNumber.getAndIncrement());
				}
			});

	/**
	 * @param modelManager
	 * @param url                    URL of the notification plugin
	 * @param loadContextDescribable if true, the corresponding context describable
	 *                               is loaded if a notification for a context root
	 *                               or context component is received.
	 * @param pollingInterval        polling interval in milliseconds
	 * @throws NotificationException Thrown if the manager cannot connect to the
	 *                               notification server.
	 */
	public OdsPollingNotificationManager(ODSContext context, QueryService queryService, String url,
			boolean loadContextDescribable, long pollingInterval) throws NotificationException {
		this.context = context;
		this.pollingInterval = pollingInterval;

		loader = new NotificationEntityLoader(context, queryService, loadContextDescribable);

		try {
			client = ClientBuilder.newBuilder().register(SseFeature.class).register(OdsJsonMessageBodyProvider.class)
					.register(OdsProtobufMessageBodyProvider.class).build();

			endpoint = client.target(url).path("events");
		} catch (Exception e) {
			throw new NotificationException("Could not create " + OdsPollingNotificationManager.class.getName() + "!",
					e);
		}
		LOGGER.info("OdsPollingNotificationManager for context [{}] initialized.", context.getSessionId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.notification.NotificationManager#register(java.
	 * lang.String, org.eclipse.mdm.api.base.notification.NotificationFilter,
	 * org.eclipse.mdm.api.base.notification.NotificationListener)
	 */
	@Override
	public void register(String registration, NotificationFilter filter, NotificationListener listener)
			throws NotificationException {
		LOGGER.info("Starting registration for with name: {}", registration);

		Response response = endpoint.path(registration).request().post(javax.ws.rs.client.Entity
				.entity(ProtobufConverter.from(filter), OdsJsonMessageBodyProvider.MEDIA_TYPE));

		if (response.getStatusInfo().getStatusCode() == Status.CONFLICT.getStatusCode()) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("A registration with the name '{}' already exists. Trying to reregister...",
						response.readEntity(String.class));
			}
			deregister(registration);
			LOGGER.info("Deregisteration successful.");
			register(registration, filter, listener);
			return;
		}

		if (response.getStatusInfo().getStatusCode() != Status.OK.getStatusCode()) {
			throw new NotificationException(
					"Could not create registration at notification service: " + response.readEntity(String.class));
		}

		reattach(registration, listener);
		registrations.put(registration, filter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.notification.NotificationManager#deregister(java
	 * .lang.String)
	 */
	@Override
	public void deregister(String registration) {
		closeRegistration(registration);

		try {
			Response r = endpoint.path(registration).request().delete();
			try {
				if (r.getStatusInfo().getStatusCode() != 200) {
					throw new RuntimeException("Cannot deregister: HTTP " + r.getStatusInfo().getStatusCode() + ": "
							+ r.readEntity(String.class));
				}
			} finally {
				r.close();
			}
		} catch (Exception e) {
			LOGGER.info("Cannot deregister: " + e.getMessage(), e);
		}
	}

	@Override
	public void close(boolean isDeregisterAll) throws NotificationException {
		LOGGER.info("Closing NotificationManager...");

		for (String registration : pollers.keySet()) {
			if (isDeregisterAll) {
				LOGGER.debug("Deregistering '{}'.", registration);
				deregister(registration);
			} else {
				LOGGER.debug("Disconnecting '{}'.", registration);
				closeRegistration(registration);
			}
		}
	}

	@Override
	public void reattach(String registration, NotificationListener listener) throws NotificationException {
		EventPoller poller = new EventPoller(this, endpoint.path(registration), listener);
		ScheduledFuture<?> feature = EXECUTOR.scheduleAtFixedRate(poller::pollEvents, 100L, pollingInterval,
				TimeUnit.MILLISECONDS);
		pollers.put(registration, feature);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.notification.NotificationService#getRegistrations()
	 */
	@Override
	public Map<String, NotificationFilter> getRegistrations() throws NotificationException {
		return registrations;
	}

	static class EventPoller {
		private WeakReference<OdsPollingNotificationManager> manager;
		private WebTarget endpoint;
		private NotificationListener listener;

		EventPoller(OdsPollingNotificationManager manager, WebTarget endpoint, NotificationListener listener) {
			this.manager = new WeakReference<>(manager);
			this.endpoint = endpoint;
			this.listener = listener;
		}

		public void pollEvents() {
			OdsPollingNotificationManager m = manager.get();
			if (m == null || !m.context.isValid()) {
				// if OdsPollingNotificationManager was GCed or if the context is not valid
				// anymore, we terminate the polling task by throwing an exception.
				LOGGER.info(
						"OdsPollingNotificationManager does not exist any more. Cancelling task for polling events.");
				throw new OdsPollingException(
						"OdsPollingNotificationManager does not exist any more. Cancelling task for polling events.");
			}

			try {
				NotificationPool n = endpoint.request(OdsProtobufMessageBodyProvider.MEDIA_TYPE_STRING)
						.get(NotificationPool.class);

				if (n != null && n.getEventsCount() > 0) {
					LOGGER.trace("Received notification for session [{}]: {}", m.context.getSessionId(), n);
					m.processNotification(n, listener);
				} else {
					LOGGER.trace("No notification events available for session [{}]", m.context.getSessionId());
				}
			} catch (ProcessingException e) {
				m.processException(new NotificationException(
						"Cannot deserialize notification event for session " + m.context.getSessionId(), e));
				return;
			} catch (Throwable e) {
				m.processException(new NotificationException(
						"Cannot deserialize notification event for session " + m.context.getSessionId(), e));
				return;
			}
		}
	}

	private void closeRegistration(String registration) {
		if (pollers.containsKey(registration)) {
			ScheduledFuture<?> future = pollers.get(registration);
			if (!future.cancel(false)) {
				LOGGER.warn("Could not cancel poller for registration '{}' and session [{}].", registration,
						context.getSessionId());
			} else {
				LOGGER.info("Cancelled poller for registration '{}' and session [{}].", registration,
						context.getSessionId());
			}
			pollers.remove(registration);
		}
		registrations.remove(registration);
	}

	/**
	 * Handler for Exceptions during event processing.
	 * 
	 * @param e Exception which occured during event processing.
	 */
	void processException(Exception e) {
		LOGGER.error("Exception during notification processing!", e);
	}

	/**
	 * Handler for notifications.
	 * 
	 * @param notificationPool     notification to process.
	 * @param notificationListener notification listener for handling the
	 *                             notification.
	 */
	void processNotification(NotificationPool notificationPool, NotificationListener notificationListener) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Processing notification event: " + notificationPool);
		}

		try {

			Map<Long, List<Notification>> byAid = notificationPool.getEventsList().stream()
					.collect(Collectors.groupingBy(e -> e.getAid()));

			List<String> userIds = notificationPool.getEventsList().stream().map(e -> e.getUserId()).distinct()
					.map(id -> Long.toString(id)).collect(Collectors.toList());
			Map<String, List<User>> userMap = loader.loadAll(new Key<>(User.class), userIds).stream()
					.collect(Collectors.groupingBy(u -> u.getID()));

			for (Entry<Long, List<Notification>> entry : byAid.entrySet()) {
				EntityType entityType = context.getODSModelManager().getEntityTypeById(Long.toString(entry.getKey()));

				if (LOGGER.isTraceEnabled()) {
					LOGGER.trace("Notification event with: entityType=" + entityType + ", user=" + userIds);
				}

				for (Notification n : entry.getValue()) {
					switch (n.getType()) {
					case NT_NEW:
						notificationListener
								.instanceCreated(
										loader.loadEntities(entityType,
												n.getIidsList().stream().map(id -> id.toString())
														.filter(ODSUtils::isValidID).collect(Collectors.toList())),
										getUser(userMap, n.getUserId()));
						break;
					case NT_MODIFY:
						notificationListener
								.instanceModified(
										loader.loadEntities(entityType,
												n.getIidsList().stream().map(id -> id.toString())
														.filter(ODSUtils::isValidID).collect(Collectors.toList())),
										getUser(userMap, n.getUserId()));
						break;
					case NT_DELETE:
						notificationListener.instanceDeleted(
								entityType, n.getIidsList().stream().map(id -> id.toString())
										.filter(ODSUtils::isValidID).collect(Collectors.toList()),
								getUser(userMap, n.getUserId()));
						break;
					case NT_MODEL:
						notificationListener.modelModified(entityType, getUser(userMap, n.getUserId()));
						break;
					case NT_SECURITY:
						notificationListener.securityModified(
								entityType, n.getIidsList().stream().map(id -> id.toString())
										.filter(ODSUtils::isValidID).collect(Collectors.toList()),
								getUser(userMap, n.getUserId()));
						break;
					default:
						processException(new NotificationException("Invalid notification type!"));
					}
				}
			}

		} catch (Exception e) {
			processException(new NotificationException("Could not process notification!", e));
		}
	}

	private User getUser(Map<String, List<User>> userMap, long userId) {
		List<User> list = userMap.get(Long.toString(userId));
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}
}
