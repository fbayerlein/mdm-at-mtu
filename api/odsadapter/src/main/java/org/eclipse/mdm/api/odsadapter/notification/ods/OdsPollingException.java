/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.notification.ods;

public class OdsPollingException extends RuntimeException {

	private static final long serialVersionUID = -5143128698341940619L;

	/**
	 * Constructor.
	 *
	 * @param message The error message.
	 */
	public OdsPollingException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 *
	 * @param message   The error message.
	 * @param throwable The origin cause.
	 */
	public OdsPollingException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
