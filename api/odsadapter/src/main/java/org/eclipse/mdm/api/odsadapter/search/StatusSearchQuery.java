package org.eclipse.mdm.api.odsadapter.search;

import java.util.Collections;
import java.util.List;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.dflt.model.Status;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;

public class StatusSearchQuery extends BaseEntitySearchQuery {


	private ODSModelManager modelManager;

	/**
	 * Constructor.
	 *
	 * @param modelManager Used to load {@link EntityType}s.
	 * @param contextState The {@link ContextState}.
	 */
	StatusSearchQuery(ODSModelManager modelManager, QueryService queryService, ContextState contextState) {
		super(modelManager, queryService, Status.class, Status.class);
		this.modelManager = modelManager;

//		// layers
//		addJoinConfig(JoinConfig.up(Pool.class, Project.class));
//		addJoinConfig(JoinConfig.up(Test.class, Pool.class));
//		addJoinConfig(JoinConfig.up(Classification.class, Test.class));
//		addJoinConfig(JoinConfig.up(Status.class, Classification.class));
//
//		// context
//		addJoinConfig(contextState);
	}
	
	@Override
	public List<EntityType> listEntityTypes() {
		return Collections.singletonList(this.modelManager.getEntityType(Status.class));
	}
}
