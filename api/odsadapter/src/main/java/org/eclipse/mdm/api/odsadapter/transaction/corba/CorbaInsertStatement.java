/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.corba;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.asam.ods.AIDName;
import org.asam.ods.AIDNameValueSeqUnitId;
import org.asam.ods.AoException;
import org.asam.ods.ApplElemAccess;
import org.asam.ods.ElemId;
import org.asam.ods.T_LONGLONG;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.InsertStatement;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Insert statement is used to write new entities and their children.
 *
 */
public class CorbaInsertStatement extends InsertStatement {

	public CorbaInsertStatement(ODSTransaction transaction, EntityType entityType) {
		super(transaction, entityType);
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(CorbaInsertStatement.class);

	@Override
	protected void execute() throws OdsException, DataAccessException, IOException {
		List<AIDNameValueSeqUnitId> anvsuList = new ArrayList<>();
		T_LONGLONG aID = ODSConverter.toODSLong(getEntityType().getODSID());

		// TODO tracing progress in this method...

		if (loadSortIndex && !sortIndexTestSteps.isEmpty()) {
			adjustMissingSortIndices();
		}

		if (!fileLinkToUpload.isEmpty()) {
			getTransaction().getUploadService().uploadParallel(fileLinkToUpload, getTransaction(), null);
		}

		for (Entry<String, List<Value>> entry : insertMap.entrySet()) {
			Attribute attribute = getEntityType().getAttribute(entry.getKey());

			if (ValueType.FILE_RELATION.equals(attribute.getValueType()) || entry.getValue().stream()
					.filter(v -> ValueType.FILE_RELATION.equals(v.getValueType())).count() > 0) {
				// skip FILE_RELATION pseudo-attributes
				continue;
			}

			AIDNameValueSeqUnitId anvsu = new AIDNameValueSeqUnitId();
			anvsu.attr = new AIDName(aID, entry.getKey());
			anvsu.unitId = ODSConverter.toODSLong(0);
			anvsu.values = ODSConverter.toODSValueSeq(attribute, entry.getValue(),
					getTransaction().getContext().getODSModelManager().getTimeZone());

			// If all values are invalid, skip this attribute
			if (allFlags(anvsu.values.flag, (short) 0)) {
				LOGGER.trace("skipping " + anvsu.attr.aaName + ", because all values have invalid flag.");
				continue;
			}

			anvsuList.add(anvsu);
		}

		long start = System.currentTimeMillis();
		try {
			ApplElemAccess applElemAccess = ((ODSCorbaModelManager) getTransaction().getContext().getODSModelManager())
					.getApplElemAccess();

			ElemId[] elemIds = applElemAccess
					.insertInstances(anvsuList.toArray(new AIDNameValueSeqUnitId[anvsuList.size()]));
			for (int i = 0; i < elemIds.length; i++) {
				cores.get(i).setID(Long.toString(ODSConverter.fromODSLong(elemIds[i].iid)));
			}
			long stop = System.currentTimeMillis();

			LOGGER.debug("{} " + getEntityType() + " instances created in {} ms.", elemIds.length, stop - start);

			for (List<Entity> children : childrenMap.values()) {
				getTransaction().create(children);
			}
		} catch (AoException e) {
			throw new OdsException(e.reason, e);
		}
	}

	private boolean allFlags(short[] flag, short value) {
		for (int i = 0; i < flag.length; i++) {
			if (flag[i] != value) {
				return false;
			}
		}
		return true;
	}
}
