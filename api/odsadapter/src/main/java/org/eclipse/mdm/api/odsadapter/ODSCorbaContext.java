/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import java.util.Map;
import java.util.Optional;

import org.asam.ods.AoException;
import org.asam.ods.AoSession;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.notification.NotificationException;
import org.eclipse.mdm.api.base.notification.NotificationService;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Health;
import org.eclipse.mdm.api.odsadapter.filetransfer.AoFileService;
import org.eclipse.mdm.api.odsadapter.filetransfer.CORBAFileService;
import org.eclipse.mdm.api.odsadapter.filetransfer.Transfer;
import org.eclipse.mdm.api.odsadapter.lookup.EntityLoader;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfigRepositoryLoader;
import org.eclipse.mdm.api.odsadapter.notification.ODSNotificationServiceFactory;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityFactory;
import org.eclipse.mdm.api.odsadapter.query.ODSQueryService;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.search.ODSSearchService;
import org.omg.CORBA.ORB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.highqsoft.corbafileserver.generated.CORBAFileServerIF;

/**
 * ODSContext encapsulates a session to the ASAM ODS CORBA API and provides the
 * ODS specific services implementations.
 *
 * @since 1.0.0
 */
public class ODSCorbaContext implements ODSContext {

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSCorbaContext.class);

	private Map<String, String> parameters;
	public final Transfer transfer = Transfer.SOCKET;

	private CORBAFileServerIF fileServer;
	private ODSCorbaModelManager modelManager;
	private ODSQueryService queryService;
	private EntityLoader entityLoader;
	private ODSEntityManager entityManager;
	private ODSSearchService searchService;
	private EntityConfigRepositoryLoader repositoryLoader;
	private Optional<NotificationService> notificationService;
	private final boolean isCoSession;
	private HealthService healthService;

	/**
	 * Creates a new ODS application context.
	 * 
	 * @param orb              the CORBA ORB used to connect to the ODS API
	 * @param aoSession
	 * @param fileServer
	 * @param parameters
	 * @param repositoryLoader
	 * @param timeZone
	 *
	 * @throws AoException
	 *
	 * @since 5.2.0
	 */
	public ODSCorbaContext(String sourceName, ORB orb, AoSession aoSession, CORBAFileServerIF fileServer,
			Map<String, String> parameters, EntityConfigRepositoryLoader repositoryLoader) throws OdsException {
		this.fileServer = fileServer;
		this.parameters = parameters;

		this.repositoryLoader = repositoryLoader;
		if (repositoryLoader == null) {
			this.modelManager = new ODSCorbaModelManager(sourceName, orb, aoSession);
		} else {
			this.modelManager = new ODSCorbaModelManager(sourceName, orb, aoSession, repositoryLoader);
		}

		this.entityManager = new ODSEntityManager(this);
		// this.entityManager.loadEnvironment().getTimezone();
		this.queryService = new ODSQueryService(this.modelManager);
		this.entityLoader = new EntityLoader(this, queryService);
		this.searchService = new ODSSearchService(this, queryService, entityLoader);
		try {
			this.isCoSession = false;
			this.notificationService = new ODSNotificationServiceFactory().create(this, parameters);
		} catch (ConnectionException e) {
			throw new IllegalStateException("Unable to create notification manager.", e);
		}

		String healthURL = getParameters().get(PARA_HEALTH_URL);		
		healthService = new HealthService(healthURL);

		LOGGER.info("ODSContext [{}] initialized.", getSessionId());
	}

	public ODSCorbaContext(String sourceName, ORB orb, AoSession aoSession, CORBAFileServerIF fileServer,
			Optional<NotificationService> notificationService, Map<String, String> parameters,
			EntityConfigRepositoryLoader repositoryLoader) throws OdsException {
		this.fileServer = fileServer;
		this.isCoSession = true;
		this.notificationService = notificationService;
		this.parameters = parameters;

		this.repositoryLoader = repositoryLoader;
		if (repositoryLoader == null) {
			this.modelManager = new ODSCorbaModelManager(sourceName, orb, aoSession);
		} else {
			this.modelManager = new ODSCorbaModelManager(sourceName, orb, aoSession, repositoryLoader);
		}
		this.entityManager = new ODSEntityManager(this);
		this.queryService = new ODSQueryService(this.modelManager);

		this.entityLoader = new EntityLoader(this, queryService);
		this.searchService = new ODSSearchService(this, queryService, entityLoader);

		LOGGER.info("Co-ODSContext [{}] initialized.", getSessionId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSourceName() {
		return modelManager.getSourceName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<EntityManager> getEntityManager() {
		return Optional.of(entityManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<EntityFactory> getEntityFactory() {
		try {
			return Optional.of(new ODSEntityFactory(modelManager, entityManager::loadLoggedOnUser));
		} catch (DataAccessException e) {
			throw new IllegalStateException("Unable to load instance of the logged in user.", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ModelManager> getModelManager() {
		return Optional.of(modelManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<QueryService> getQueryService() {
		// TODO
		// java docs: cache this service for ONE request!
		return Optional.of(new ODSQueryService(modelManager));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<SearchService> getSearchService() {
		return Optional.of(searchService);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<FileService> getFileService(FileServiceType fileServiceType) {
		switch (fileServiceType) {
		case AOFILE:
			return Optional.of(new AoFileService(this));
		case EXTREF:
			return (fileServer == null ? Optional.empty() : Optional.of(new CORBAFileService(this, transfer)));
		default:
			return Optional.empty();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<NotificationService> getNotificationService() {
		return this.notificationService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getParameters() {
		return parameters;
	}

	/**
	 * @returns the string "ods"
	 */
	@Override
	public String getAdapterType() {
		return "ods";
	}

	@Override
	public String getSessionId() {
		try {
			return "" + getAoSession().getId();
		} catch (AoException e) {
			return "0";
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		LOGGER.info("Closing ODSContext [{}]", getSessionId());

		try {
			modelManager.close();
		} catch (OdsException e) {
			LOGGER.warn("Unable to close sesssion [" + getSessionId() + "] due to: " + e.getMessage(), e);
		}

		if (!isCoSession) {
			notificationService.ifPresent(n -> {
				try {
					n.close(false);
				} catch (NotificationException e) {
					LOGGER.warn("Unable to close notification service for session  [" + getSessionId() + "]  due to: "
							+ e.getLocalizedMessage(), e);
				}
			});
		}

	}

	/**
	 * Returns the {@link ODSCorbaModelManager} used for this context.
	 * 
	 * @return the {@link ODSCorbaModelManager}
	 */
	@Override
	public ODSCorbaModelManager getODSModelManager() {
		return modelManager;
	}

	/**
	 * Returns the {@link AoSession} used for this context.
	 * 
	 * @return {@link AoSession} used for this context.
	 */
	public AoSession getAoSession() {
		return modelManager.getAoSession();
	}

	/**
	 * Returns the ORB used for this context
	 * 
	 * @return ORB used for this context
	 */
	public ORB getORB() {
		return modelManager.getORB();
	}

	/**
	 * Returns the {@link CORBAFileServerIF}.
	 *
	 * @return The {@code CORBAFileServerIF} is returned or null, if missing.
	 */
	public CORBAFileServerIF getFileServer() {
		return fileServer;
	}

	/**
	 * Returns a new {@link ODSCorbaContext} with a new ODS co-session.
	 *
	 * @return The created {@code ODSContext} is returned.
	 * @throws AoException Thrown on errors.
	 */
	@Override
	public ODSCorbaContext newContext() throws OdsException {
		try {
			return new ODSCorbaContext(modelManager.getSourceName(), modelManager.getORB(),
					getAoSession().createCoSession(), fileServer, notificationService, parameters, repositoryLoader);
		} catch (AoException e) {
			throw new OdsException("Cannot create CoSession: " + e.reason, e);
		}
	}

	@Override
	public boolean isValid() {
		try {
			modelManager.getAoSession().getName();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	public Health getCurrentHealth() {		
		return healthService.getCurrentHealth();
	}
}
