/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.filetransfer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.asam.ods.ElemId;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.odsadapter.ODSHttpContext;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ods.Ods.FileIdentifier;

public class HttpAoFileService extends BaseFileService {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpAoFileService.class);

	/**
	 * Constructor.
	 *
	 * @param context  Used for {@link Entity} to {@link ElemId} conversion.
	 * @param transfer The transfer type for up- and downloads.
	 */
	public HttpAoFileService(ODSHttpContext context) {
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	public void loadSize(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		if (fileLink.getSize() > -1) {
			// file size is already known
			return;
		} else if (fileLink.isRemote()) {
			MDMFile mdmFile = getMDMFile(fileLink, transaction);

//				ODSHttpUtils.request(mm.getSession(), "file-delete",
//						FileIdentifier.newBuilder().setAid(mdmFile.get).setIid(iid).build(), DataMatrices.class);
			fileLink.setFileSize(mdmFile.getSize());
		} else {
			throw new IllegalArgumentException("File link is neither in local nor remote state: " + fileLink);
		}
	}

	@Override
	public InputStream openRemoteStream(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		try {
			MDMFile mdmFile = getMDMFile(fileLink, transaction);

			WebTarget fileAccessTarget = getFileAccessTarget(mdmFile, (ODSTransaction) transaction);

			return fileAccessTarget.request().get(InputStream.class);
		} catch (OdsException exc) {
			throw new DataAccessException("Error opening remote stream: " + exc.getMessage(), exc);
		}
	}

	@Override
	public void delete(Entity entity, FileLink fileLink, Transaction transaction) {
		if (!fileLink.isRemote()) {
			// nothing to do
			return;
		}

		try {
			MDMFile mdmFile = getMDMFile(fileLink, transaction);

			WebTarget fileAccessTarget = getFileAccessTarget(mdmFile, (ODSTransaction) transaction);

			fileAccessTarget.request().delete(Void.class);
			getLogger().debug("File '{}' sucessfully deleted.", fileLink.getRemotePath());
		} catch (Exception exc) {
			getLogger().warn("Failed to delete remote file.", exc);
		}
	}

	@Override
	public void upload(Entity entity, FileLink fileLink, InputStream inputStream, Transaction transaction)
			throws IOException {
		try {
			MDMFile mdmFile = getMDMFile(fileLink, transaction);

			WebTarget fileAccessTarget = getFileAccessTarget(mdmFile, (ODSTransaction) transaction);

			LOGGER.trace("Uploading filelink {} to {}", fileLink, fileAccessTarget.getUri());
			fileAccessTarget.request()
					.put(javax.ws.rs.client.Entity.entity(inputStream, MediaType.APPLICATION_OCTET_STREAM), Void.class);

			refresh(mdmFile, fileLink, (ODSTransaction) transaction);

		} catch (DataAccessException exc) {
			throw exc;
		} catch (Exception exc) {
			throw new DataAccessException("Error uploading ODSFile: " + exc.getMessage(), exc);
		}
	}

	private WebTarget getFileAccessTarget(MDMFile mdmFile, ODSTransaction transaction) throws OdsException {

		ODSHttpModelManager modelManager = (ODSHttpModelManager) transaction.getModelManager();

		long aid = ((ODSEntityType) modelManager.getEntityType(mdmFile)).getODSID();
		long iid = ODSHttpConverter.toODSID(mdmFile.getID());

		URI uri = OdsHttpRequests.getLocation(modelManager.getSession(), "file-access",
				FileIdentifier.newBuilder().setAid(aid).setIid(iid).build());
		WebTarget fileAccessTarget = modelManager.getClient().target(uri);
		return fileAccessTarget;
	}

	private void refresh(MDMFile mdmFile, FileLink fileLink, ODSTransaction transaction) {
		// Copy values (including autogenerated values) from ODS storage to entity:
		EntityType etMdmFile = transaction.getModelManager().listEntityTypes().stream()
				.filter(et -> et.getName().equals(mdmFile.getTypeName())).findFirst()
				.orElseThrow(() -> new DataAccessException("Cannot find entity '" + mdmFile.getTypeName() + "'."));

		List<Attribute> nonIdAttributes = etMdmFile.getAttributes().stream()
				.filter(attr -> !attr.getName().equals(etMdmFile.getIDAttribute().getName()))
				.collect(Collectors.toList());

		List<Result> results = transaction.getContext().getQueryService()
				.orElseThrow(() -> new ServiceNotProvidedException(QueryService.class)).createQuery()
				.select(nonIdAttributes).fetch(Filter.idOnly(etMdmFile, mdmFile.getID()));

		if (results.isEmpty()) {
			throw new DataAccessException("Could not find " + etMdmFile.getName() + " with ID " + mdmFile.getID());
		}
		if (results.size() > 1) {
			throw new DataAccessException(results.size() + "results found for " + etMdmFile.getName() + " with ID "
					+ mdmFile.getID() + ", but only expected one.");
		}

		Record record = results.get(0).getRecord(etMdmFile);
		for (Map.Entry<String, Value> v : record.getValues().entrySet()) {
			mdmFile.getValue(v.getKey()).set(v.getValue().extract());
		}
		LOGGER.trace("Refreshed MDMFile {}", mdmFile);

		fileLink.setRemotePath(mdmFile.getLocation());
		fileLink.setFileSize(mdmFile.getSize());
		fileLink.setFileServiceType(FileServiceType.AOFILE);
	}

	private MDMFile getMDMFile(FileLink fileLink, Transaction transaction) {
		Object remoteObject = fileLink.getRemoteObject();
		if (remoteObject == null || !(remoteObject instanceof MDMFile)) {
			throw new DataAccessException("No MDMFile attached to FileLink!");
		}

		if (!(transaction instanceof ODSTransaction)) {
			throw new DataAccessException(String.format("Transaction class is %s, expected ODSTransaction!",
					transaction.getClass().getSimpleName()));
		}
		return (MDMFile) remoteObject;
	}
}
