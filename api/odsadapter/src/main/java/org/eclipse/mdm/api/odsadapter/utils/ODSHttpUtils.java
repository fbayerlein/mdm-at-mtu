/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.utils;

import org.eclipse.mdm.api.base.adapter.RelationType;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Aggregation;
import org.eclipse.mdm.api.base.query.BooleanOperator;
import org.eclipse.mdm.api.base.query.BracketOperator;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.JoinType;

import com.google.common.collect.ImmutableBiMap;

import ods.Ods.AggregateEnum;
import ods.Ods.DataTypeEnum;
import ods.Ods.Model.RelationTypeEnum;
import ods.Ods.SelectStatement.ConditionItem.Condition.OperatorEnum;
import ods.Ods.SelectStatement.ConditionItem.ConjuctionEnum;
import ods.Ods.SelectStatement.JoinItem.JoinTypeEnum;

/**
 * Utility class provides bidirectional mappings for ODS types
 *
 */
public abstract class ODSHttpUtils {
	/**
	 * Maps {@link RelationType} to the corresponding ODS {@link RelationTypeEnum}.
	 */
	public static final ImmutableBiMap<RelationType, RelationTypeEnum> RELATIONSHIPS = ImmutableBiMap
			.<RelationType, RelationTypeEnum>builder().put(RelationType.FATHER_CHILD, RelationTypeEnum.RT_FATHER_CHILD)
			.put(RelationType.INFO, RelationTypeEnum.RT_INFO)
			.put(RelationType.INHERITANCE, RelationTypeEnum.RT_INHERITANCE).build();

	/**
	 * Maps {@link Aggregation} to the corresponding ODS {@link AggregateEnum}.
	 */
	public static final ImmutableBiMap<Aggregation, AggregateEnum> AGGREGATIONS = ImmutableBiMap
			.<Aggregation, AggregateEnum>builder().put(Aggregation.NONE, AggregateEnum.AG_NONE)
			.put(Aggregation.COUNT, AggregateEnum.AG_COUNT).put(Aggregation.DISTINCT_COUNT, AggregateEnum.AG_DCOUNT)
			.put(Aggregation.MINIMUM, AggregateEnum.AG_MIN).put(Aggregation.MAXIMUM, AggregateEnum.AG_MAX)
			.put(Aggregation.AVERAGE, AggregateEnum.AG_AVG).put(Aggregation.DEVIATION, AggregateEnum.AG_STDDEV)
			.put(Aggregation.SUM, AggregateEnum.AG_SUM).put(Aggregation.DISTINCT, AggregateEnum.AG_DISTINCT).build();

	/**
	 * Maps {@link ComparisonOperator} to the corresponding ODS
	 * {@link OperatorEnum}.
	 */
	public static final ImmutableBiMap<ComparisonOperator, OperatorEnum> OPERATIONS = ImmutableBiMap
			.<ComparisonOperator, OperatorEnum>builder().put(ComparisonOperator.LIKE, OperatorEnum.OP_LIKE)
			.put(ComparisonOperator.CASE_INSENSITIVE_LIKE, OperatorEnum.OP_CI_LIKE)
			.put(ComparisonOperator.NOT_LIKE, OperatorEnum.OP_NOTLIKE)
			.put(ComparisonOperator.CASE_INSENSITIVE_NOT_LIKE, OperatorEnum.OP_CI_NOTLIKE)
			.put(ComparisonOperator.EQUAL, OperatorEnum.OP_EQ)
			.put(ComparisonOperator.CASE_INSENSITIVE_EQUAL, OperatorEnum.OP_CI_EQ)
			.put(ComparisonOperator.NOT_EQUAL, OperatorEnum.OP_NEQ)
			.put(ComparisonOperator.CASE_INSENSITIVE_NOT_EQUAL, OperatorEnum.OP_CI_NEQ)
			.put(ComparisonOperator.GREATER_THAN, OperatorEnum.OP_GT)
			.put(ComparisonOperator.CASE_INSENSITIVE_GREATER_THAN, OperatorEnum.OP_CI_GT)
			.put(ComparisonOperator.GREATER_THAN_OR_EQUAL, OperatorEnum.OP_GTE)
			.put(ComparisonOperator.CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL, OperatorEnum.OP_CI_GTE)
			.put(ComparisonOperator.LESS_THAN, OperatorEnum.OP_LT)
			.put(ComparisonOperator.CASE_INSENSITIVE_LESS_THAN, OperatorEnum.OP_CI_LT)
			.put(ComparisonOperator.LESS_THAN_OR_EQUAL, OperatorEnum.OP_LTE)
			.put(ComparisonOperator.CASE_INSENSITIVE_LESS_THAN_OR_EQUAL, OperatorEnum.OP_CI_LTE)
			.put(ComparisonOperator.IS_NULL, OperatorEnum.OP_IS_NULL)
			.put(ComparisonOperator.IS_NOT_NULL, OperatorEnum.OP_IS_NOT_NULL)
			.put(ComparisonOperator.IN_SET, OperatorEnum.OP_INSET)
			.put(ComparisonOperator.CASE_INSENSITIVE_IN_SET, OperatorEnum.OP_CI_INSET)
			.put(ComparisonOperator.NOT_IN_SET, OperatorEnum.OP_NOTINSET)
			.put(ComparisonOperator.CASE_INSENSITIVE_NOT_IN_SET, OperatorEnum.OP_CI_NOTINSET)
			.put(ComparisonOperator.BETWEEN, OperatorEnum.OP_BETWEEN).build();

	/**
	 * Maps {@link BooleanOperator} to the corresponding ODS {@link ConjuctionEnum}.
	 */
	public static final ImmutableBiMap<BooleanOperator, ConjuctionEnum> OPERATORS = ImmutableBiMap
			.<BooleanOperator, ConjuctionEnum>builder().put(BooleanOperator.AND, ConjuctionEnum.CO_AND)
			.put(BooleanOperator.OR, ConjuctionEnum.CO_OR).put(BooleanOperator.NOT, ConjuctionEnum.CO_NOT).build();

	/**
	 * Maps {@link BracketOperator} to the corresponding ODS {@link ConjuctionEnum}.
	 */
	public static final ImmutableBiMap<BracketOperator, ConjuctionEnum> BRACKETOPERATORS = ImmutableBiMap
			.<BracketOperator, ConjuctionEnum>builder().put(BracketOperator.OPEN, ConjuctionEnum.CO_OPEN)
			.put(BracketOperator.CLOSE, ConjuctionEnum.CO_CLOSE).build();

	/**
	 * Maps {@link ValueType} to the corresponding ODS {@link DataTypeEnum}.
	 */
	public static final ImmutableBiMap<ValueType<?>, DataTypeEnum> VALUETYPES = ImmutableBiMap
			.<ValueType<?>, DataTypeEnum>builder().put(ValueType.UNKNOWN, DataTypeEnum.DT_UNKNOWN)
			.put(ValueType.STRING, DataTypeEnum.DT_STRING).put(ValueType.STRING_SEQUENCE, DataTypeEnum.DS_STRING)
			.put(ValueType.DATE, DataTypeEnum.DT_DATE).put(ValueType.DATE_SEQUENCE, DataTypeEnum.DS_DATE)
			.put(ValueType.BOOLEAN, DataTypeEnum.DT_BOOLEAN).put(ValueType.BOOLEAN_SEQUENCE, DataTypeEnum.DS_BOOLEAN)
			.put(ValueType.BYTE, DataTypeEnum.DT_BYTE).put(ValueType.BYTE_SEQUENCE, DataTypeEnum.DS_BYTE)
			.put(ValueType.SHORT, DataTypeEnum.DT_SHORT).put(ValueType.SHORT_SEQUENCE, DataTypeEnum.DS_SHORT)
			.put(ValueType.INTEGER, DataTypeEnum.DT_LONG).put(ValueType.INTEGER_SEQUENCE, DataTypeEnum.DS_LONG)
			.put(ValueType.LONG, DataTypeEnum.DT_LONGLONG).put(ValueType.LONG_SEQUENCE, DataTypeEnum.DS_LONGLONG)
			.put(ValueType.FLOAT, DataTypeEnum.DT_FLOAT).put(ValueType.FLOAT_SEQUENCE, DataTypeEnum.DS_FLOAT)
			.put(ValueType.DOUBLE, DataTypeEnum.DT_DOUBLE).put(ValueType.DOUBLE_SEQUENCE, DataTypeEnum.DS_DOUBLE)
			.put(ValueType.BYTE_STREAM, DataTypeEnum.DT_BYTESTR)
			.put(ValueType.BYTE_STREAM_SEQUENCE, DataTypeEnum.DS_BYTESTR)
			.put(ValueType.FLOAT_COMPLEX, DataTypeEnum.DT_COMPLEX)
			.put(ValueType.FLOAT_COMPLEX_SEQUENCE, DataTypeEnum.DS_COMPLEX)
			.put(ValueType.DOUBLE_COMPLEX, DataTypeEnum.DT_DCOMPLEX)
			.put(ValueType.DOUBLE_COMPLEX_SEQUENCE, DataTypeEnum.DS_DCOMPLEX)
			.put(ValueType.ENUMERATION, DataTypeEnum.DT_ENUM).put(ValueType.ENUMERATION_SEQUENCE, DataTypeEnum.DS_ENUM)
			.put(ValueType.FILE_LINK, DataTypeEnum.DT_EXTERNALREFERENCE)
			.put(ValueType.FILE_LINK_SEQUENCE, DataTypeEnum.DS_EXTERNALREFERENCE)
			.put(ValueType.BLOB, DataTypeEnum.DT_BLOB).build();

	/**
	 * Maps {@link JoinType} to the corresponding ODS {@link JoinTypeEnum}.
	 */
	public static final ImmutableBiMap<JoinType, JoinTypeEnum> JOINS = ImmutableBiMap.<JoinType, JoinTypeEnum>builder()
			.put(JoinType.INNER, JoinTypeEnum.JT_DEFAULT).put(JoinType.OUTER, JoinTypeEnum.JT_OUTER).build();
}
