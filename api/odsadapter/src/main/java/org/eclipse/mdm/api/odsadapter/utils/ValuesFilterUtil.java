/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.utils;

import java.nio.channels.Channel;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.massdata.ChannelValues;
import org.eclipse.mdm.api.base.massdata.ValuesFilter;
import org.eclipse.mdm.api.base.massdata.ValuesFilter.ValuesCondition;
import org.eclipse.mdm.api.base.massdata.ValuesFilter.ValuesConjunction;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.DataAccessException;

import com.google.common.base.Splitter;
import com.google.common.collect.ListMultimap;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;

public class ValuesFilterUtil {

	private ValuesFilterUtil() {

	}

	/**
	 * Apply {@link ValuesFilter} to {@link ChannelValues} of the values of the
	 * MultiMap
	 * 
	 * @param channelValuesByChannelGroup Multimap with ChannelGroup-Ids as keys and
	 *                                    their {@link Channel}s as values
	 * @param filter                      {@link ValuesFilter} to apply
	 */
	public static void applyFilters(ListMultimap<String, ChannelValues> channelValuesByChannelGroup,
			ValuesFilter filter) {

		for (String smId : channelValuesByChannelGroup.keySet()) {
			applyFilters(channelValuesByChannelGroup.get(smId), filter);
		}
	}

	/**
	 * Apply {@link ValuesFilter} to {@link ChannelValues} of one
	 * {@link ChannelGroup}
	 * 
	 * @param channelValues {@link ChannelValues} of one {@link ChannelGroup}
	 * @param filter        {@link ValuesFilter} to apply
	 */
	static void applyFilters(List<ChannelValues> channelValues, ValuesFilter filter) {
		Map<String, ChannelValues> byName = channelValues.stream()
				.collect(Collectors.toMap(ChannelValues::getName, cv -> cv));

		BitSet matches = toBitSet(filter, byName);

		for (ChannelValues cv : channelValues) {
			filter(cv, matches);
		}
	}

	/**
	 * Creates a BitSet with the length of the ChannelValues values. If the
	 * {@link ValuesFilter} matches on all values of index i, the i-th bit in the
	 * BitSet is set to 1.
	 * 
	 * @param filter {@link ValuesFilter}
	 * @param byName Map with ChannelValues from on ChannelGroup (all with same
	 *               number of values) indexed by {@link Channel#getName()}.
	 * @return BitSet with matching indexes set to true
	 */
	static BitSet toBitSet(ValuesFilter filter, Map<String, ChannelValues> byName) {
		if (filter instanceof ValuesCondition) {
			ValuesCondition leaf = (ValuesCondition) filter;
			ChannelValues cv = byName.get(leaf.getName());
			return match(cv, leaf.getOperator(), leaf.getValues());
		} else if (filter instanceof ValuesConjunction) {
			ValuesConjunction node = (ValuesConjunction) filter;
			if ("and".equals(node.getConjunction())) {
				BitSet bitSet = toBitSet(node.getLeft(), byName);
				bitSet.and(toBitSet(node.getRight(), byName));
				return bitSet;
			} else if ("or".equals(node.getConjunction())) {
				BitSet bitSet = toBitSet(node.getLeft(), byName);
				bitSet.or(toBitSet(node.getRight(), byName));
				return bitSet;
			} else if ("not".equals(node.getConjunction())) {
				BitSet bitSet = toBitSet(node.getLeft(), byName);
				bitSet.flip(0, bitSet.length());
				return bitSet;
			} else {
				throw new DataAccessException("Unknown conjunction: " + node.getConjunction());
			}
		} else {
			throw new DataAccessException("Unknown node type: " + filter.getClass());
		}
	}

	static BitSet match(ChannelValues channelValues, ComparisonOperator operator, List<String> values) {
		if (channelValues == null || channelValues.values == null) {
			// no channelValues -> no match
			return new BitSet();
		}

		Object o = channelValues.values.extract();

		if (o instanceof int[]) {
			int[] expIntValues = values.stream().mapToInt(Integer::parseInt).toArray();
			int[] intValues = (int[]) o;

			BitSet bitSet = new BitSet(intValues.length);
			for (int i = 0; i < intValues.length; i++) {
				bitSet.set(i, intMatch(operator, intValues[i], expIntValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof long[]) {
			long[] expLongValues = values.stream().mapToLong(Long::parseLong).toArray();
			long[] longValues = (long[]) o;

			BitSet bitSet = new BitSet(longValues.length);
			for (int i = 0; i < longValues.length; i++) {
				bitSet.set(i, longMatch(operator, longValues[i], expLongValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof float[]) {
			float[] expValues = Floats.toArray(values.stream().map(Double::parseDouble).collect(Collectors.toList()));
			float[] floatValues = (float[]) o;

			BitSet bitSet = new BitSet(floatValues.length);
			for (int i = 0; i < floatValues.length; i++) {
				bitSet.set(i, floatMatch(operator, floatValues[i], expValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof double[]) {
			double[] expDoubleValues = values.stream().mapToDouble(Double::parseDouble).toArray();
			double[] doubleValues = (double[]) o;

			BitSet bitSet = new BitSet(doubleValues.length);
			for (int i = 0; i < doubleValues.length; i++) {
				bitSet.set(i, doubleMatch(operator, doubleValues[i], expDoubleValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof String[]) {
			String[] stringValues = (String[]) o;

			BitSet bitSet = new BitSet(stringValues.length);
			for (int i = 0; i < stringValues.length; i++) {
				bitSet.set(i, stringMatch(operator, stringValues[i], values, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof Instant[]) {
			Instant[] expDateValues = values.stream().map(ValuesFilterUtil::parseDateFromString)
					.toArray(Instant[]::new);
			Instant[] dateValues = (Instant[]) o;

			BitSet bitSet = new BitSet(dateValues.length);
			for (int i = 0; i < dateValues.length; i++) {
				bitSet.set(i, dateMatch(operator, dateValues[i], expDateValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof short[]) {
			short[] expShortValues = Shorts
					.toArray(values.stream().map(Short::parseShort).collect(Collectors.toList()));
			short[] shortValues = (short[]) o;

			BitSet bitSet = new BitSet(shortValues.length);
			for (int i = 0; i < shortValues.length; i++) {
				bitSet.set(i, shortMatch(operator, shortValues[i], expShortValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof byte[]) {
			byte[] expByteValues = Bytes.toArray(values.stream().map(Byte::parseByte).collect(Collectors.toList()));
			byte[] byteValues = (byte[]) o;

			BitSet bitSet = new BitSet(byteValues.length);
			for (int i = 0; i < byteValues.length; i++) {
				bitSet.set(i, byteMatch(operator, byteValues[i], expByteValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else if (o instanceof boolean[]) {
			boolean[] expBooleanValues = Booleans
					.toArray(values.stream().map(Boolean::parseBoolean).collect(Collectors.toList()));
			boolean[] booleanValues = (boolean[]) o;

			BitSet bitSet = new BitSet(booleanValues.length);
			for (int i = 0; i < booleanValues.length; i++) {

				bitSet.set(i, booleanMatch(operator, booleanValues[i], expBooleanValues, channelValues.isValid(i)));
			}
			return bitSet;
		} else {
			throw new DataAccessException("Unsupported value object: " + o.getClass());
		}
	}

	static boolean intMatch(ComparisonOperator op, int value, int[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value == exprValues[0];
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return value != exprValues[0];
		case LESS_THAN:
		case CASE_INSENSITIVE_LESS_THAN:
			return value < exprValues[0];
		case LESS_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return value <= exprValues[0];
		case GREATER_THAN:
		case CASE_INSENSITIVE_GREATER_THAN:
			return value > exprValues[0];
		case GREATER_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return value >= exprValues[0];
		case BETWEEN:
			return exprValues[0] <= value && value <= exprValues[1];
		case IN_SET:
		case CASE_INSENSITIVE_IN_SET:
			return Ints.contains(exprValues, value);
		case NOT_IN_SET:
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !Ints.contains(exprValues, value);
		case LIKE:
		case CASE_INSENSITIVE_LIKE:
		case NOT_LIKE:
		case CASE_INSENSITIVE_NOT_LIKE:
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype INTEGER_SEQUENCE");
		}
	}

	static boolean longMatch(ComparisonOperator op, long value, long[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value == exprValues[0];
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return value != exprValues[0];
		case LESS_THAN:
		case CASE_INSENSITIVE_LESS_THAN:
			return value < exprValues[0];
		case LESS_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return value <= exprValues[0];
		case GREATER_THAN:
		case CASE_INSENSITIVE_GREATER_THAN:
			return value > exprValues[0];
		case GREATER_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return value >= exprValues[0];
		case BETWEEN:
			return exprValues[0] <= value && value <= exprValues[1];
		case IN_SET:
		case CASE_INSENSITIVE_IN_SET:
			return Longs.contains(exprValues, value);
		case NOT_IN_SET:
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !Longs.contains(exprValues, value);
		case LIKE:
		case CASE_INSENSITIVE_LIKE:
		case NOT_LIKE:
		case CASE_INSENSITIVE_NOT_LIKE:
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype LONG_SEQUENCE");
		}
	}

	static boolean floatMatch(ComparisonOperator op, float value, float[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value == exprValues[0];
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return value != exprValues[0];
		case LESS_THAN:
		case CASE_INSENSITIVE_LESS_THAN:
			return value < exprValues[0];
		case LESS_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return value <= exprValues[0];
		case GREATER_THAN:
		case CASE_INSENSITIVE_GREATER_THAN:
			return value > exprValues[0];
		case GREATER_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return value >= exprValues[0];
		case BETWEEN:
			return exprValues[0] <= value && value <= exprValues[1];
		case IN_SET:
		case CASE_INSENSITIVE_IN_SET:
			return Floats.contains(exprValues, value);
		case NOT_IN_SET:
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !Floats.contains(exprValues, value);
		case LIKE:
		case CASE_INSENSITIVE_LIKE:
		case NOT_LIKE:
		case CASE_INSENSITIVE_NOT_LIKE:
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype FLOAT_SEQUENCE");
		}
	}

	static boolean doubleMatch(ComparisonOperator op, double value, double[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value == exprValues[0];
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return value != exprValues[0];
		case LESS_THAN:
		case CASE_INSENSITIVE_LESS_THAN:
			return value < exprValues[0];
		case LESS_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return value <= exprValues[0];
		case GREATER_THAN:
		case CASE_INSENSITIVE_GREATER_THAN:
			return value > exprValues[0];
		case GREATER_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return value >= exprValues[0];
		case BETWEEN:
			return exprValues[0] <= value && value <= exprValues[1];
		case IN_SET:
		case CASE_INSENSITIVE_IN_SET:
			return Doubles.contains(exprValues, value);
		case NOT_IN_SET:
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !Doubles.contains(exprValues, value);
		case LIKE:
		case CASE_INSENSITIVE_LIKE:
		case NOT_LIKE:
		case CASE_INSENSITIVE_NOT_LIKE:
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype DOUBLE_SEQUENCE");
		}
	}

	static boolean stringMatch(ComparisonOperator op, String value, List<String> exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
			return value.equals(exprValues.get(0));
		case NOT_EQUAL:
			return !value.equals(exprValues.get(0));
		case CASE_INSENSITIVE_EQUAL:
			return value.equalsIgnoreCase(exprValues.get(0));
		case CASE_INSENSITIVE_NOT_EQUAL:
			return !value.equalsIgnoreCase(exprValues.get(0));
		case LESS_THAN:
			return value.compareTo(exprValues.get(0)) < 0;
		case CASE_INSENSITIVE_LESS_THAN:
			return Objects.compare(value, exprValues.get(0), String.CASE_INSENSITIVE_ORDER) < 0;
		case LESS_THAN_OR_EQUAL:
			return value.compareTo(exprValues.get(0)) <= 0;
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return Objects.compare(value, exprValues.get(0), String.CASE_INSENSITIVE_ORDER) <= 0;
		case GREATER_THAN:
			return value.compareTo(exprValues.get(0)) > 0;
		case CASE_INSENSITIVE_GREATER_THAN:
			return Objects.compare(value, exprValues.get(0), String.CASE_INSENSITIVE_ORDER) > 0;
		case GREATER_THAN_OR_EQUAL:
			return value.compareTo(exprValues.get(0)) >= 0;
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return Objects.compare(value, exprValues.get(0), String.CASE_INSENSITIVE_ORDER) >= 0;
		case LIKE:
			return like(value, exprValues.get(0));
		case CASE_INSENSITIVE_LIKE:
			return ilike(value, exprValues.get(0));
		case NOT_LIKE:
			return !like(value, exprValues.get(0));
		case CASE_INSENSITIVE_NOT_LIKE:
			return !ilike(value, exprValues.get(0));
		case BETWEEN:
			return Objects.compare(exprValues.get(0), value, String.CASE_INSENSITIVE_ORDER) >= 0
					&& Objects.compare(value, exprValues.get(1), String.CASE_INSENSITIVE_ORDER) >= 0;
		case IN_SET:
			return exprValues.contains(value);
		case NOT_IN_SET:
			return !exprValues.contains(value);
		case CASE_INSENSITIVE_IN_SET:
			return containsIgnoreCase(exprValues, value);
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !containsIgnoreCase(exprValues, value);
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype STRING_SEQUENCE");
		}
	}

	static boolean dateMatch(ComparisonOperator op, Instant value, Instant[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value.equals(exprValues[0]);
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return !value.equals(exprValues[0]);
		case LESS_THAN:
		case CASE_INSENSITIVE_LESS_THAN:
			return value.isBefore(exprValues[0]);
		case LESS_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return !value.isAfter(exprValues[0]);
		case GREATER_THAN:
		case CASE_INSENSITIVE_GREATER_THAN:
			return value.isAfter(exprValues[0]);
		case GREATER_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return !value.isBefore(exprValues[0]);
		case BETWEEN:
			return !value.isBefore(exprValues[0]) && !value.isAfter(exprValues[1]);
		case IN_SET:
		case CASE_INSENSITIVE_IN_SET:
			return Arrays.asList(exprValues).contains(value);
		case NOT_IN_SET:
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !Arrays.asList(exprValues).contains(value);
		case LIKE:
		case CASE_INSENSITIVE_LIKE:
		case NOT_LIKE:
		case CASE_INSENSITIVE_NOT_LIKE:
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype DATE_SEQUENCE");
		}
	}

	static boolean shortMatch(ComparisonOperator op, short value, short[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value == exprValues[0];
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return value != exprValues[0];
		case LESS_THAN:
		case CASE_INSENSITIVE_LESS_THAN:
			return value < exprValues[0];
		case LESS_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return value <= exprValues[0];
		case GREATER_THAN:
		case CASE_INSENSITIVE_GREATER_THAN:
			return value > exprValues[0];
		case GREATER_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return value >= exprValues[0];
		case BETWEEN:
			return exprValues[0] <= value && value <= exprValues[1];
		case IN_SET:
		case CASE_INSENSITIVE_IN_SET:
			return Shorts.contains(exprValues, value);
		case NOT_IN_SET:
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !Shorts.contains(exprValues, value);
		case LIKE:
		case CASE_INSENSITIVE_LIKE:
		case NOT_LIKE:
		case CASE_INSENSITIVE_NOT_LIKE:
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype SHORT_SEQUENCE");
		}
	}

	static boolean byteMatch(ComparisonOperator op, byte value, byte[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value == exprValues[0];
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return value != exprValues[0];
		case LESS_THAN:
		case CASE_INSENSITIVE_LESS_THAN:
			return value < exprValues[0];
		case LESS_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return value <= exprValues[0];
		case GREATER_THAN:
		case CASE_INSENSITIVE_GREATER_THAN:
			return value > exprValues[0];
		case GREATER_THAN_OR_EQUAL:
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return value >= exprValues[0];
		case BETWEEN:
			return exprValues[0] <= value && value <= exprValues[1];
		case IN_SET:
		case CASE_INSENSITIVE_IN_SET:
			return Bytes.contains(exprValues, value);
		case NOT_IN_SET:
		case CASE_INSENSITIVE_NOT_IN_SET:
			return !Bytes.contains(exprValues, value);
		case LIKE:
		case CASE_INSENSITIVE_LIKE:
		case NOT_LIKE:
		case CASE_INSENSITIVE_NOT_LIKE:
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype BYTE_SEQUENCE");
		}
	}

	static boolean booleanMatch(ComparisonOperator op, boolean value, boolean[] exprValues, boolean isValid) {
		switch (op) {
		case IS_NULL:
			return isValid;
		case IS_NOT_NULL:
			return !isValid;
		case EQUAL:
		case CASE_INSENSITIVE_EQUAL:
			return value == exprValues[0];
		case NOT_EQUAL:
		case CASE_INSENSITIVE_NOT_EQUAL:
			return value != exprValues[0];
		default:
			throw new DataAccessException("ComparisonOperator " + op + " not supported for valuetype BOOLEAN_SEQUENCE");
		}
	}

	static void filter(ChannelValues cv, BitSet bitFilter) {
		Object v = cv.values.extract(ValueType.UNKNOWN);
		if (v instanceof int[]) {
			cv.values.set(filterInts(bitFilter, (int[]) v));
		} else if (v instanceof long[]) {
			cv.values.set(filterLongs(bitFilter, (long[]) v));
		} else if (v instanceof float[]) {
			cv.values.set(filterFloats(bitFilter, (float[]) v));
		} else if (v instanceof double[]) {
			cv.values.set(filterDoubles(bitFilter, (double[]) v));
		} else if (v instanceof String[]) {
			cv.values.set(filterStrings(bitFilter, (String[]) v));
		} else if (v instanceof Instant[]) {
			cv.values.set(filterInstants(bitFilter, (Instant[]) v));
		} else if (v instanceof short[]) {
			cv.values.set(filterShorts(bitFilter, (short[]) v));
		} else if (v instanceof byte[]) {
			cv.values.set(filterBytes(bitFilter, (byte[]) v));
		} else if (v instanceof boolean[]) {
			cv.values.set(filterBooleans(bitFilter, (boolean[]) v));
		} else if (v instanceof byte[][]) {
			cv.values.set(filterByteArrays(bitFilter, (byte[][]) v));
		} else {
			throw new DataAccessException("Datatype " + v.getClass().getSimpleName() + " of Channel " + cv.channelId
					+ " not supported in values filter!");
		}

		cv.flags = filterShorts(bitFilter, cv.flags);
	}

	private static int[] filterInts(BitSet bitFilter, int[] ints) {
		int[] filteredInts = new int[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < ints.length; i++) {
			if (bitFilter.get(i)) {
				filteredInts[f] = ints[i];
				f++;
			}
		}
		return filteredInts;
	}

	private static long[] filterLongs(BitSet bitFilter, long[] longs) {
		long[] filteredLongs = new long[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < longs.length; i++) {
			if (bitFilter.get(i)) {
				filteredLongs[f] = longs[i];
				f++;
			}
		}
		return filteredLongs;
	}

	private static float[] filterFloats(BitSet bitFilter, float[] floats) {
		float[] filteredfloats = new float[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < floats.length; i++) {
			if (bitFilter.get(i)) {
				filteredfloats[f] = floats[i];
				f++;
			}
		}
		return filteredfloats;
	}

	private static double[] filterDoubles(BitSet bitFilter, double[] doubles) {
		double[] filteredDoubles = new double[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < doubles.length; i++) {
			if (bitFilter.get(i)) {
				filteredDoubles[f] = doubles[i];
				f++;
			}
		}
		return filteredDoubles;
	}

	private static String[] filterStrings(BitSet bitFilter, String[] strings) {
		String[] filteredStrings = new String[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < strings.length; i++) {
			if (bitFilter.get(i)) {
				filteredStrings[f] = strings[i];
				f++;
			}
		}
		return filteredStrings;
	}

	private static Instant[] filterInstants(BitSet bitFilter, Instant[] instants) {
		Instant[] filteredInstants = new Instant[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < instants.length; i++) {
			if (bitFilter.get(i)) {
				filteredInstants[f] = instants[i];
				f++;
			}
		}
		return filteredInstants;
	}

	private static short[] filterShorts(BitSet bitFilter, short[] shorts) {
		short[] filteredShorts = new short[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < shorts.length; i++) {
			if (bitFilter.get(i)) {
				filteredShorts[f] = shorts[i];
				f++;
			}
		}
		return filteredShorts;
	}

	private static byte[] filterBytes(BitSet bitFilter, byte[] bytes) {
		byte[] filteredBytes = new byte[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < bytes.length; i++) {
			if (bitFilter.get(i)) {
				filteredBytes[f] = bytes[i];
				f++;
			}
		}
		return filteredBytes;
	}

	private static boolean[] filterBooleans(BitSet bitFilter, boolean[] booleans) {
		boolean[] filteredBooleans = new boolean[bitFilter.cardinality()];
		int f = 0;
		for (int i = 0; i < booleans.length; i++) {
			if (bitFilter.get(i)) {
				filteredBooleans[f] = booleans[i];
				f++;
			}
		}
		return filteredBooleans;
	}

	private static byte[][] filterByteArrays(BitSet bitFilter, byte[][] byteArrays) {
		byte[][] filteredByteArrays = new byte[bitFilter.cardinality()][];
		int f = 0;
		for (int i = 0; i < byteArrays.length; i++) {
			if (bitFilter.get(i)) {
				filteredByteArrays[f] = byteArrays[i];
				f++;
			}
		}
		return filteredByteArrays;
	}

	private static Instant parseDateFromString(String valueAsString) {
		try {
			return Instant.parse(valueAsString);
		} catch (DateTimeParseException e) {
			throw new IllegalArgumentException(
					"Unsupported value for date: '" + valueAsString + "'. Expected format: '2007-12-03T10:15:30Z'");
		}
	}

	static boolean containsIgnoreCase(List<String> list, String str) {
		for (String i : list) {
			if (i.equalsIgnoreCase(str))
				return true;
		}
		return false;
	}

	static boolean like(String value, String pattern) {
		// TODO: Creating the pattern is rather expensive and is created for each value
		// comparison.
		String regex = convertToRegexPattern(pattern);
		return Pattern.matches(regex, value);
	}

	static String convertToRegexPattern(String pattern) {
		// first convert to regex, e.g.
		// abc*d -> abc.*d
		// abc?d -> abc.d
		// abc\*d -> abc\*d
		// abc\?d -> abc\?d

		List<String> regexFragments = new ArrayList<>();

		// split at unescaped *
		for (String fragment : Splitter.on(Pattern.compile("(?<!\\\\)\\*")).split(pattern)) {
			// split at unescaped ?
			List<String> split = Splitter.on(Pattern.compile("(?<!\\\\)\\?")).splitToList(fragment);
			regexFragments.add(split.stream().map(s -> s.replace("\\*", "*").replace("\\?", "?")).map(Pattern::quote)
					.collect(Collectors.joining(".")));
		}

		return regexFragments.stream().collect(Collectors.joining(".*"));
	}

	static boolean ilike(String value, String pattern) {
		return like(value.toLowerCase(), pattern.toLowerCase());
	}

}
