/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction.http;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.ReadRequest.ValuesMode;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.ColumnAttributes;
import org.eclipse.mdm.api.odsadapter.transaction.ReadRequestHandler;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ods.Ods.DataMatrices;
import ods.Ods.ValueMatrixRequestStruct;
import ods.Ods.ValueMatrixRequestStruct.ColumnItem;
import ods.Ods.ValueMatrixRequestStruct.ModeEnum;

/**
 * Reads mass data specified in {@link ReadRequest}s.
 *
 */
public final class HttpReadRequestHandler extends ReadRequestHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpReadRequestHandler.class);

	/**
	 * Constructor.
	 *
	 * @param modelManager Used to gain access to value matrices.
	 * @param timezone
	 */
	public HttpReadRequestHandler(ODSHttpModelManager modelManager, QueryService queryService) {
		super(modelManager, queryService);
	}

	protected List<MeasuredValues> loadByValueMatrix(ReadRequest readRequest,
			Map<String, ColumnAttributes> mapColumnAttributes) {
		if (mapColumnAttributes.isEmpty()) {
			return Collections.emptyList();
		}

		// if there are still localcolumns left we load them be valuematrix
		LOGGER.debug("Loading {} localcolumns by value matrix.", mapColumnAttributes.size());

		Entity entity = readRequest.getChannelGroup();

		long iid = ODSHttpConverter.toODSID(entity.getID());
		long aid = ((ODSEntityType) modelManager.getEntityType(entity)).getODSID();

		/*
		 * Attributes in the valuematrix-read request can either be base names or
		 * application attribute names according to ODS spec. Peak ODS Server since
		 * 3.2.0 only accepts application attribute names. Since 3.6.19 both are
		 * excepted.
		 */
		List<String> attributes = Arrays.asList("Name", "Values", "Flags", "GenerationParameters");

		ValueMatrixRequestStruct.Builder valueMatrix = ValueMatrixRequestStruct.newBuilder().setAid(aid).setIid(iid)
				.addAllAttributes(attributes).setMode(convert(readRequest.getValuesMode()))
				.setValuesStart(readRequest.getStartIndex()).setValuesLimit(readRequest.getRequestSize());

		for (ColumnAttributes ca : mapColumnAttributes.values()) {

			if (ca.getDataType().isNumericalType()) {
				valueMatrix.addColumns(ColumnItem.newBuilder().setName(ca.getName()).setUnitId(ca.getUnitId()));
			} else {
				// no unit conversion for non numerical datatypes
				valueMatrix.addColumns(ColumnItem.newBuilder().setName(ca.getName()));
			}

		}

		try {
			DataMatrices dms = OdsHttpRequests.request(((ODSHttpModelManager) modelManager).getSession(),
					"valuematrix-read", valueMatrix.build(), DataMatrices.class);

			return ODSHttpConverter.fromODSMeasuredValuesSeq(dms.getMatrices(0), mapColumnAttributes.values(),
					modelManager.getTimeZone());
		} catch (OdsException aoe) {
			throw new DataAccessException(aoe.getMessage(), aoe);
		}
	}

	private ModeEnum convert(ValuesMode valuesMode) {
		switch (valuesMode) {
		case CALCULATED:
			return ModeEnum.MO_CALCULATED;
		case STORAGE:
		case STORAGE_PRESERVE_EXTCOMPS:
			return ModeEnum.MO_STORAGE;
		default:
			throw new DataAccessException(String.format("Unsupported ValueMode %s!", valuesMode.name()));
		}
	}
}
