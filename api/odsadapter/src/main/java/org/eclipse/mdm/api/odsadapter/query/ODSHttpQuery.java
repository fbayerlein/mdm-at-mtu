/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.query;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import javax.ws.rs.client.WebTarget;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Aggregation;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.api.base.query.ConnectionLostException;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.FilterItem;
import org.eclipse.mdm.api.base.query.JoinType;
import org.eclipse.mdm.api.base.query.Query;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpConverter;
import org.eclipse.mdm.api.odsadapter.utils.ODSHttpUtils;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Booleans;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import com.google.protobuf.ByteString;

import ods.Ods.BooleanArray;
import ods.Ods.ByteArray;
import ods.Ods.DataMatrices;
import ods.Ods.DataMatrix;
import ods.Ods.DataMatrix.Column;
import ods.Ods.DoubleArray;
import ods.Ods.FloatArray;
import ods.Ods.LongArray;
import ods.Ods.LonglongArray;
import ods.Ods.SelectStatement;
import ods.Ods.SelectStatement.AttributeItem;
import ods.Ods.SelectStatement.ConditionItem;
import ods.Ods.SelectStatement.GroupByItem;
import ods.Ods.SelectStatement.JoinItem;
import ods.Ods.SelectStatement.OrderByItem;
import ods.Ods.SelectStatement.OrderByItem.OrderEnum;
import ods.Ods.StringArray;

/**
 * ODS implementation of the {@link Query} interface.
 *
 */
public class ODSHttpQuery implements ODSQuery {

	// ======================================================================
	// Class variables
	// ======================================================================

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSHttpQuery.class);

	// ======================================================================
	// Instance variables
	// ======================================================================

	private final Map<String, EntityType> entityTypesByID = new HashMap<>();
	private final Set<EntityType> queriedEntityTypes = new HashSet<>();
	private final SelectStatement.Builder select = SelectStatement.newBuilder();

	private final WebTarget session;
	private final ZoneId timezone;

	private int limit = 0;
	private int offset = 0;

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param session Used to execute the query.
	 */
	ODSHttpQuery(WebTarget session, ZoneId timeZone) {
		this.session = session;
		this.timezone = timeZone;
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isQueried(EntityType entityType) {
		return queriedEntityTypes.contains(entityType);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Query limit(int limit) {
		this.limit = limit;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Query offset(int offset) {
		this.offset = offset;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Query select(Attribute attribute, Aggregation aggregation) {
		EntityType entityType = attribute.getEntityType();
		long aid = ((ODSEntityType) entityType).getODSID();

		entityTypesByID.put(Long.toString(aid), entityType);
		queriedEntityTypes.add(entityType);

		AttributeItem item = AttributeItem.newBuilder().setAggregate(ODSHttpUtils.AGGREGATIONS.get(aggregation))
				.setAid(aid).setAttribute(attribute.getName()).setUnitId(0L).build();

		select.addColumns(item);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Query join(Relation relation, JoinType join) {
		queriedEntityTypes.add(relation.getSource());
		queriedEntityTypes.add(relation.getTarget());
		select.addJoins(createJoin(relation, join));
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Query group(List<Attribute> attributes) {
		for (Attribute attribute : attributes) {
			group(attribute);
		}
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Query group(Attribute attribute) {
		select.addGroupBy(GroupByItem.newBuilder().setAid(getAid(attribute)).setAttribute(attribute.getName()));
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Query order(Attribute attribute, boolean ascending) {
		select.addOrderBy(OrderByItem.newBuilder().setAid(getAid(attribute)).setAttribute(attribute.getName())
				.setOrder(ascending ? OrderEnum.OD_ASCENDING : OrderEnum.OD_DESCENDING));
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Result> fetchSingleton(Filter filter) throws DataAccessException {
		List<Result> results = fetch(filter);
		if (results.isEmpty()) {
			return Optional.empty();
		} else if (results.size() > 1) {
			throw new DataAccessException("Multiple results found after executing the singleton query!");
		}

		return Optional.of(results.get(0));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Result> fetch(Filter filter) throws DataAccessException {
		try {

			for (FilterItem conditionItem : filter) {
				ConditionItem.Builder condItem = ConditionItem.newBuilder();
				if (conditionItem.isCondition()) {
					condItem.setCondition(createCondition(conditionItem.getCondition()));
				} else if (conditionItem.isBracketOperator()) {
					condItem.setConjunction(ODSHttpUtils.BRACKETOPERATORS.get(conditionItem.getBracketOperator()));
				} else if (conditionItem.isBooleanOperator()) {
					condItem.setConjunction(ODSHttpUtils.OPERATORS.get(conditionItem.getBooleanOperator()));
				} else {
					throw new IllegalArgumentException("Passed filter item is neither an operator nor a condition.");
				}

				select.addWhere(condItem);
			}

			select.setRowStart(offset);
			select.setRowLimit(limit);

			logQuery();
			List<Result> results = new ArrayList<>();
			long start = System.currentTimeMillis();

			DataMatrices dms = OdsHttpRequests.request(session, "data-read", select.build(), DataMatrices.class);

			for (Result result : new ResultFactory(entityTypesByID, dms, timezone)) {
				results.add(result);
			}
			long stop = System.currentTimeMillis();

			LOGGER.debug(
					"Query executed in {} ms and retrieved {} result rows ({} selections, {} conditions, "
							+ "{} joins).",
					stop - start, results.size(), select.getColumnsCount(), select.getWhereCount(),
					select.getJoinsCount());
			return results;
		} catch (ConnectionLostException aoe) {
			throw aoe;
		} catch (Exception aoe) {
			throw new DataAccessException(aoe.getMessage(), aoe);
		}
	}

	/**
	 * Removes already joined {@link Relation}. The inverse Relation is also checked
	 * and removed.
	 * 
	 * @param relation {@link Relation} to remove.
	 */
	public void removeJoin(Relation relation) {
		long sourceAid = ((ODSEntityType) relation.getSource()).getODSID();
		long targetAid = ((ODSEntityType) relation.getTarget()).getODSID();

		List<JoinItem> joinSeq = new ArrayList<>(select.getJoinsList());
		joinSeq.removeIf(jd -> jd.getAidFrom() == sourceAid && jd.getAidTo() == targetAid
				&& jd.getRelation().equals(relation.getName()));

		joinSeq.removeIf(jd -> jd.getAidTo() == sourceAid && jd.getAidFrom() == targetAid
				&& jd.getRelation().equals(relation.getInverseName()));

		select.clearJoins();
		select.addAllJoins(joinSeq);
	}

	// ======================================================================
	// Private methods
	// ======================================================================

	private long getAid(Attribute attribute) {
		return ((ODSEntityType) attribute.getEntityType()).getODSID();
	}

	private void logQuery() {
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("Executing query: {}", OdsHttpRequests.toString(select));
		}
	}

	/**
	 * Converts given {@link Condition} to an ODS {@link ConditionItem.Condition}.
	 *
	 * @param condition The {@code Condition}.
	 * @return The corresponding {@code ConditionItem.Condition} is returned.
	 * @throws DataAccessException Thrown in case of errors.
	 */
	private ConditionItem.Condition createCondition(Condition condition) throws DataAccessException {
		ConditionItem.Condition.Builder c = ConditionItem.Condition.newBuilder()
				.setOperator(ODSHttpUtils.OPERATIONS.get(condition.getComparisonOperator()))
				.setAid(getAid(condition.getAttribute())).setAttribute(condition.getAttribute().getName())
				.setUnitId(0L);

		ValueType<?> type = condition.getValue().getValueType();
		Value value = condition.getValue();

		if (ValueType.STRING == type) {
			String string = value.extract(ValueType.STRING);

			if (((ODSAttribute) condition.getAttribute()).isIdAttribute()) {
				c.setLonglongArray(LonglongArray.newBuilder().addValues(ODSHttpConverter.toODSID(string)));
			} else {
				c.setStringArray(StringArray.newBuilder().addValues(string));
			}
		} else if (ValueType.STRING_SEQUENCE == type) {
			Stream<String> strings = Stream.of(value.extract(ValueType.STRING_SEQUENCE));

			if (((ODSAttribute) condition.getAttribute()).isIdAttribute()) {
				Iterable<Long> it = () -> strings.map(s -> ODSHttpConverter.toODSID(s)).iterator();
				c.setLonglongArray(LonglongArray.newBuilder().addAllValues(it));
			} else {
				Iterable<String> it = () -> strings.iterator();
				c.setStringArray(StringArray.newBuilder().addAllValues(it));
			}
		} else if (ValueType.DATE == type) {
			c.setStringArray(StringArray.newBuilder().addValues(ODSConverter.toODSDate(value.extract(), timezone)));
		} else if (ValueType.DATE_SEQUENCE == type) {
			c.setStringArray(StringArray.newBuilder()
					.addAllValues(Arrays.asList(ODSConverter.toODSDateSeq(value.extract(), timezone))));
		} else if (ValueType.BOOLEAN == type) {
			c.setBooleanArray(BooleanArray.newBuilder().addValues(value.extract()));
		} else if (ValueType.BOOLEAN_SEQUENCE == type) {
			c.setBooleanArray(BooleanArray.newBuilder().addAllValues(Booleans.asList(value.extract())));
		} else if (ValueType.BYTE == type) {
			c.setByteArray(ByteArray.newBuilder()
					.setValues(ByteString.copyFrom(new byte[] { value.extract(ValueType.BYTE) })));
		} else if (ValueType.BYTE_SEQUENCE == type) {
			c.setByteArray(
					ByteArray.newBuilder().setValues(ByteString.copyFrom(value.extract(ValueType.BYTE_SEQUENCE))));
		} else if (ValueType.SHORT == type) {
			c.setLongArray(LongArray.newBuilder().addValues(value.extract(ValueType.SHORT)));
		} else if (ValueType.SHORT_SEQUENCE == type) {
			c.setLongArray(LongArray.newBuilder().addAllValues(Ints.asList(
					Shorts.asList(value.extract(ValueType.SHORT_SEQUENCE)).stream().mapToInt(s -> (int) s).toArray())));
		} else if (ValueType.INTEGER == type) {
			c.setLongArray(LongArray.newBuilder().addValues(value.extract(ValueType.INTEGER)));
		} else if (ValueType.INTEGER_SEQUENCE == type) {
			c.setLongArray(LongArray.newBuilder().addAllValues(Ints.asList(value.extract(ValueType.INTEGER_SEQUENCE))));
		} else if (ValueType.LONG == type) {
			c.setLonglongArray(LonglongArray.newBuilder().addValues(value.extract(ValueType.LONG)));
		} else if (ValueType.LONG_SEQUENCE == type) {
			c.setLonglongArray(
					LonglongArray.newBuilder().addAllValues(Longs.asList(value.extract(ValueType.LONG_SEQUENCE))));
		} else if (ValueType.FLOAT == type) {
			c.setFloatArray(FloatArray.newBuilder().addValues(value.extract(ValueType.FLOAT)));
		} else if (ValueType.FLOAT_SEQUENCE == type) {
			c.setFloatArray(
					FloatArray.newBuilder().addAllValues(Floats.asList(value.extract(ValueType.FLOAT_SEQUENCE))));
		} else if (ValueType.DOUBLE == type) {
			c.setDoubleArray(DoubleArray.newBuilder().addValues(value.extract(ValueType.DOUBLE)));
		} else if (ValueType.DOUBLE_SEQUENCE == type) {
			c.setDoubleArray(
					DoubleArray.newBuilder().addAllValues(Doubles.asList(value.extract(ValueType.DOUBLE_SEQUENCE))));
//			} else if (ValueType.BYTE_STREAM == type) {
//				odsValue.u.bytestrVal(value.extract());
//			} else if (ValueType.BYTE_STREAM_SEQUENCE == type) {
//				odsValue.u.bytestrSeq(value.extract());
//			} else if (ValueType.FLOAT_COMPLEX == type) {
//				odsValue.u.complexVal(toODSFloatComplex(value.extract()));
//			} else if (ValueType.FLOAT_COMPLEX_SEQUENCE == type) {
//				odsValue.u.complexSeq(toODSFloatComplexSeq(value.extract()));
//			} else if (ValueType.DOUBLE_COMPLEX == type) {
//				odsValue.u.dcomplexVal(toODSDoubleComplex(value.extract()));
//			} else if (ValueType.DOUBLE_COMPLEX_SEQUENCE == type) {
//				odsValue.u.dcomplexSeq(toODSDoubleComplexSeq(value.extract()));
		} else if (ValueType.ENUMERATION == type) {
			EnumerationValue enumValue = value.extract(ValueType.ENUMERATION);

			if (enumValue == null) {
				c.setLongArray(LongArray.getDefaultInstance());
			} else {
				c.setLongArray(LongArray.newBuilder().addValues(enumValue.ordinal()));
			}
		} else if (ValueType.ENUMERATION_SEQUENCE == type) {

			Iterable<Integer> it = () -> Stream.of(value.extract(ValueType.ENUMERATION_SEQUENCE))
					.filter(Objects::nonNull).map(EnumerationValue::ordinal).iterator();

			c.setLongArray(LongArray.newBuilder().addAllValues(it));
		} else if (ValueType.FILE_LINK == type) {
			if (((ODSAttribute) condition.getAttribute()).isUrlAttribute()) {
				c.setStringArray(StringArray.newBuilder().addValues(ODSConverter.toURLString(value.extract())));
			} else {
				c.setStringArray(StringArray.newBuilder()
						.addAllValues(toODSExternalReference(value.extract(ValueType.FILE_LINK))));
			}
		} else if (ValueType.FILE_LINK_SEQUENCE == type) {
			c.setStringArray(StringArray.newBuilder()
					.addAllValues(toODSExternalReference(value.extract(ValueType.FILE_LINK_SEQUENCE))));
//			} else if (ValueType.BLOB == type) {
//				odsValue.u.blobVal(toODSBlob(value.extract()));
		} else {
			throw new DataAccessException("Mapping for value type '" + type + "' does not exist.");
		}

		return c.build();
	}

	private List<String> toODSExternalReference(FileLink[] input) {
		List<String> result = new ArrayList<>(input.length * 3);
		for (FileLink value : input) {
			result.addAll(toODSExternalReference(value));
		}

		return result;
	}

	private List<String> toODSExternalReference(FileLink input) {
		if (input == null) {
			return Collections.emptyList();
		}
		String remotePath = input.isRemote() ? input.getRemotePath() : "";
		return Arrays.asList(input.getDescription(), input.getMimeType().toString(), remotePath);
	}

	/**
	 * Converts given {@link Relation} and {@link JoinType} to an ODS
	 * {@link JoinItem}.
	 *
	 * @param relation The {@code Relation}.
	 * @param join     The {@code JoinType}.
	 * @return The corresponding {@code JoinItem} is returned.
	 */
	private JoinItem createJoin(Relation relation, JoinType join) {
		return JoinItem.newBuilder().setAidFrom(((ODSEntityType) relation.getSource()).getODSID())
				.setAidTo(((ODSEntityType) relation.getTarget()).getODSID()).setRelation(relation.getName())
				.setJoinType(ODSHttpUtils.JOINS.get(join)).build();
	}

	// ======================================================================
	// Inner classes
	// ======================================================================

	/**
	 * Traverses the ODS {@link DataMatrices} and creates a {@link Result} for each
	 * row.
	 */
	private static final class ResultFactory implements Iterable<Result>, Iterator<Result> {

		// ======================================================================
		// Instance variables
		// ======================================================================

		private final List<RecordFactory> recordFactories = new ArrayList<>();
		private final int length;
		private int index;

		// ======================================================================
		// Constructors
		// ======================================================================

		/**
		 * Constructor.
		 *
		 * @param entityTypes       Used to access {@link EntityType} by its ODS ID.
		 * @param elemResultSetExts The ODS values sequence containers.
		 * @throws DataAccessException Thrown on conversion errors.
		 */
		public ResultFactory(Map<String, EntityType> entityTypes, DataMatrices dataMatrices, ZoneId timezone)
				throws DataAccessException {
			for (DataMatrix dataMatrix : dataMatrices.getMatricesList()) {
				EntityType entityType = entityTypes.get(Long.toString(dataMatrix.getAid()));
				recordFactories.add(new RecordFactory(entityType, dataMatrix.getColumnsList(), timezone));
			}

			length = recordFactories.isEmpty() ? 0 : recordFactories.get(0).getLength();
		}

		// ======================================================================
		// Public methods
		// ======================================================================

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return index < length;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Result next() {
			if (!hasNext()) {
				throw new NoSuchElementException("No such element available.");
			}
			Result result = new Result();

			for (RecordFactory recordFactory : recordFactories) {
				result.addRecord(recordFactory.createRecord(index));
			}

			index++;
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterator<Result> iterator() {
			return this;
		}

	}

	/**
	 * Creates a {@link Record} for given index from the original ODS values
	 * sequence for a given {@link EntityType}.
	 */
	private static final class RecordFactory {

		// ======================================================================
		// Instance variables
		// ======================================================================

		private final List<ValueFactory> valueFactories = new ArrayList<>();
		private final EntityType entityType;

		// ======================================================================
		// Constructors
		// ======================================================================

		/**
		 * Constructor.
		 *
		 * @param entityType The associated {@link EntityType}.
		 * @param nvsuis     The ODS value sequence containers.
		 * @throws DataAccessException Thrown on conversion errors.
		 */
		private RecordFactory(EntityType entityType, List<Column> columnsList, ZoneId timezone)
				throws DataAccessException {
			this.entityType = entityType;
			for (Column column : columnsList) {
				String attributeName = column.getName();
				Aggregation aggregation = ODSHttpUtils.AGGREGATIONS.inverse().getOrDefault(column.getAggregate(),
						Aggregation.NONE);

				valueFactories
						.add(new ValueFactory(entityType.getAttribute(attributeName), aggregation, column, timezone));
			}
		}

		// ======================================================================
		// Private methods
		// ======================================================================

		private int getLength() {
			return valueFactories.isEmpty() ? 0 : valueFactories.get(0).getLength();
		}

		private Record createRecord(int index) {
			Record record = new Record(entityType);
			for (ValueFactory valueFactory : valueFactories) {
				record.addValue(valueFactory.createValue(index));
			}

			return record;
		}

	}

	/**
	 * Creates a {@link Value} container for given index from the original ODS value
	 * sequence for a given {@link Attribute}.
	 */
	private static final class ValueFactory {

		// ======================================================================
		// Instance variables
		// ======================================================================

		private final List<Value> values;
		private final String unit;
		private final int length;

		// ======================================================================
		// Constructors
		// ======================================================================

		/**
		 * Constructor.
		 *
		 * @param attribute The associated {@link Attribute}.
		 * @param nvsui     The ODS value sequence container.
		 * @throws DataAccessException Thrown on conversion errors.
		 */
		private ValueFactory(Attribute attribute, Aggregation aggregation, Column column, ZoneId timezone)
				throws DataAccessException {
			unit = attribute.getUnit();
			values = ODSHttpConverter.fromODSColumn(attribute, aggregation, unit, column, timezone);
			length = values.size();
		}

		// ======================================================================
		// Private methods
		// ======================================================================

		/**
		 * Returns the length of the sequence.
		 *
		 * @return Length of the sequence is returned.
		 */
		private int getLength() {
			return length;
		}

		/**
		 * Returns the {@link Value} for given index.
		 *
		 * @param index Index within the sequence.
		 * @return The corresponding {@code Value} is returned.
		 */
		private Value createValue(int index) {
			return values.get(index);
		}

	}

}
