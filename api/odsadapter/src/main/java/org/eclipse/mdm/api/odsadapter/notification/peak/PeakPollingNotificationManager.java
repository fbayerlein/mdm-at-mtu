/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.notification.peak;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.base.notification.NotificationException;
import org.eclipse.mdm.api.base.notification.NotificationFilter;
import org.eclipse.mdm.api.base.notification.NotificationListener;
import org.eclipse.mdm.api.base.notification.NotificationService;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig.Key;
import org.eclipse.mdm.api.odsadapter.notification.NotificationEntityLoader;
import org.eclipse.mdm.api.odsadapter.notification.ods.OdsPollingNotificationManager;
import org.eclipse.mdm.api.odsadapter.utils.ODSUtils;
import org.glassfish.jersey.media.sse.SseFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.peaksolution.ods.notification.protobuf.NotificationProtos.Notification;

/**
 * Notification manager for handling notifications from the Peak ODS Server
 * Notification Plugin using polling.
 * 
 * @author Matthias Koller, Peak Solution GmbH
 * @deprecated Use {@link OdsPollingNotificationManager}
 */
@Deprecated
public class PeakPollingNotificationManager implements NotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PeakPollingNotificationManager.class);

	private final Client client;
	private final WebTarget endpoint;
	private final long pollingInterval;

	private final Map<String, ScheduledFuture<?>> pollers = new HashMap<>();

	private final Map<String, NotificationFilter> registrations = new HashMap<>();

	private final ODSContext context;

	private final NotificationEntityLoader loader;

	private final static int POOL_SIZE = Integer
			.parseInt(System.getProperty("org.eclipse.mdm.api.odsadapter.notifcation.peak.polling_pool_size", "5"));

	private final static ScheduledExecutorService EXECUTOR = new ScheduledThreadPoolExecutor(POOL_SIZE,
			new ThreadFactory() {
				private final String namePrefix = "pool-PollingNotification-thread-";
				private final AtomicInteger threadNumber = new AtomicInteger(1);

				@Override
				public Thread newThread(Runnable r) {
					return new Thread(r, namePrefix + threadNumber.getAndIncrement());
				}
			});

	/**
	 * @param modelManager
	 * @param url                    URL of the notification plugin
	 * @param loadContextDescribable if true, the corresponding context describable
	 *                               is loaded if a notification for a context root
	 *                               or context component is received.
	 * @param pollingInterval        polling interval in milliseconds
	 * @throws NotificationException Thrown if the manager cannot connect to the
	 *                               notification server.
	 */
	public PeakPollingNotificationManager(ODSContext context, QueryService queryService, String url,
			boolean loadContextDescribable, long pollingInterval) throws NotificationException {
		this.context = context;
		this.pollingInterval = pollingInterval;

		loader = new NotificationEntityLoader(context, queryService, loadContextDescribable);

		try {
			client = ClientBuilder.newBuilder().register(SseFeature.class).register(JsonMessageBodyProvider.class)
					.build();

			endpoint = client.target(url).path("events");
		} catch (Exception e) {
			throw new NotificationException("Could not create " + PeakPollingNotificationManager.class.getName() + "!",
					e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.notification.NotificationManager#register(java.
	 * lang.String, org.eclipse.mdm.api.base.notification.NotificationFilter,
	 * org.eclipse.mdm.api.base.notification.NotificationListener)
	 */
	@Override
	public void register(String registration, NotificationFilter filter, NotificationListener listener)
			throws NotificationException {
		LOGGER.info("Starting registration for with name: {}", registration);

		Response response = endpoint.path(registration).request().post(
				javax.ws.rs.client.Entity.entity(ProtobufConverter.from(filter), MediaType.APPLICATION_JSON_TYPE));

		if (response.getStatusInfo().getStatusCode() == Status.CONFLICT.getStatusCode()) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("A registration with the name already exists: {}", response.readEntity(String.class));
				LOGGER.info("Trying to reregister...");
			}
			deregister(registration);
			LOGGER.info("Deregisteration successful.");
			register(registration, filter, listener);
			return;
		}

		if (response.getStatusInfo().getStatusCode() != Status.OK.getStatusCode()) {
			throw new NotificationException(
					"Could not create registration at notification service: " + response.readEntity(String.class));
		}

		reattach(registration, listener);
		registrations.put(registration, filter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.notification.NotificationManager#deregister(java
	 * .lang.String)
	 */
	@Override
	public void deregister(String registration) {
		if (pollers.containsKey(registration)) {
			close(registration);
		}

		Response r = endpoint.path(registration).request().delete();
		try {
			if (r.getStatusInfo().getStatusCode() != 200) {
				throw new RuntimeException("Cannot deregister: HTTP " + r.getStatusInfo().getStatusCode() + ": "
						+ r.readEntity(String.class));
			}
		} finally {
			r.close();
		}
	}

	@Override
	public void close(boolean isDeregisterAll) throws NotificationException {
		LOGGER.info("Closing NotificationManager...");

		for (String registration : pollers.keySet()) {
			if (isDeregisterAll) {
				LOGGER.debug("Deregistering '{}'.", registration);
				deregister(registration);
			} else {
				LOGGER.debug("Disconnecting '{}'.", registration);
				close(registration);
			}
		}
	}

	@Override
	public void reattach(String registration, NotificationListener listener) throws NotificationException {
		EventPoller poller = new EventPoller(this, endpoint.path(registration), listener);
		ScheduledFuture<?> feature = EXECUTOR.scheduleAtFixedRate(poller::pollEvents, pollingInterval, pollingInterval,
				TimeUnit.MILLISECONDS);
		pollers.put(registration, feature);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.notification.NotificationService#getRegistrations()
	 */
	@Override
	public Map<String, NotificationFilter> getRegistrations() throws NotificationException {
		return registrations;
	}

	static class EventPoller {
		private PeakPollingNotificationManager manager;
		private WebTarget endpoint;
		private NotificationListener listener;

		EventPoller(PeakPollingNotificationManager manager, WebTarget endpoint, NotificationListener listener) {
			this.manager = manager;
			this.endpoint = endpoint;
			this.listener = listener;
		}

		public void pollEvents() {

			try {
				Notification n = endpoint.queryParam("timeout", "10").request(MediaType.APPLICATION_JSON_TYPE)
						.get(Notification.class);
				if (n != null) {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.trace("Received notification: " + n);
					}
					manager.processNotification(n, listener);
				}
			} catch (ProcessingException e) {
				manager.processException(new NotificationException("Cannot deserialize notification event!", e));
				return;
			} catch (Throwable e) {
				manager.processException(new NotificationException("Cannot deserialize notification event!", e));
				return;
			}
		}
	}

	private void close(String registration) {
		if (pollers.containsKey(registration)) {
			ScheduledFuture<?> future = pollers.get(registration);
			if (!future.cancel(false)) {
				LOGGER.warn("Could not cancel poller for registration '" + registration + "'");
			}
			pollers.remove(registration);
			registrations.remove(registration);
		}
	}

	/**
	 * Handler for Exceptions during event processing.
	 * 
	 * @param e Exception which occured during event processing.
	 */
	void processException(Exception e) {
		LOGGER.error("Exception during notification processing!", e);
	}

	/**
	 * Handler for notifications.
	 * 
	 * @param n                    notification to process.
	 * @param notificationListener notification listener for handling the
	 *                             notification.
	 */
	void processNotification(Notification n, NotificationListener notificationListener) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Processing notification event: " + n);
		}

		try {
			User user = loader.load(new Key<>(User.class), Long.toString(n.getUserId()));

			EntityType entityType = context.getODSModelManager().getEntityTypeById(Long.toString(n.getAid()));

			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace("Notification event with: entityType=" + entityType + ", user=" + user);
			}
			switch (n.getType()) {
			case NEW:
				notificationListener.instanceCreated(loader.loadEntities(entityType, n.getIidList().stream()
						.map(id -> id.toString()).filter(ODSUtils::isValidID).collect(Collectors.toList())), user);
				break;
			case MODIFY:
				notificationListener.instanceModified(loader.loadEntities(entityType, n.getIidList().stream()
						.map(id -> id.toString()).filter(ODSUtils::isValidID).collect(Collectors.toList())), user);
				break;
			case DELETE:
				notificationListener.instanceDeleted(entityType, n.getIidList().stream().map(id -> id.toString())
						.filter(ODSUtils::isValidID).collect(Collectors.toList()), user);
				break;
			case MODEL:
				notificationListener.modelModified(entityType, user);
				break;
			case SECURITY:
				notificationListener.securityModified(entityType, n.getIidList().stream().map(id -> id.toString())
						.filter(ODSUtils::isValidID).collect(Collectors.toList()), user);
				break;
			default:
				processException(new NotificationException("Invalid notification type!"));
			}
		} catch (Exception e) {
			processException(new NotificationException("Could not process notification!", e));
		}
	}

}
