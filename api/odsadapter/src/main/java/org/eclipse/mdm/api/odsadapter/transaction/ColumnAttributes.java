/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;

public final class ColumnAttributes {
	private String name;
	private String id;
	private SequenceRepresentation sequenceRepresentation;
	private double[] generationParameters;
	private boolean independent;
	private AxisType axisType;
	private Short globalFlag;
	private ScalarType dataType;
	private ScalarType rawDataType;

	private long unitId;
	private String unitName;
	private String meaQuantityId;
	private String mimeType;

	public ColumnAttributes(String name, String columnId, SequenceRepresentation sequenceRepresentation,
			double[] generationParameters, boolean independent, AxisType axisType, Short globalFlag,
			ScalarType rawDataType, String meaQuantityId, String mimeType) {
		this.name = name;
		this.id = columnId;
		this.sequenceRepresentation = sequenceRepresentation;
		this.generationParameters = generationParameters;
		this.independent = independent;
		this.axisType = axisType;
		this.globalFlag = globalFlag;
		this.rawDataType = rawDataType;
		this.meaQuantityId = meaQuantityId;
		this.mimeType = mimeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SequenceRepresentation getSequenceRepresentation() {
		return sequenceRepresentation;
	}

	public void setSequenceRepresentation(SequenceRepresentation sequenceRepresentation) {
		this.sequenceRepresentation = sequenceRepresentation;
	}

	public double[] getGenerationParameters() {
		return generationParameters;
	}

	public void setGenerationParameters(double[] generationParameters) {
		this.generationParameters = generationParameters;
	}

	public boolean isIndependentColumn() {
		return independent;
	}

	public void setIndependentColumn(boolean independent) {
		this.independent = independent;
	}

	public AxisType getAxisType() {
		return axisType;
	}

	public void setAxisType(AxisType axisType) {
		this.axisType = axisType;
	}

	public Short getGlobalFlag() {
		return globalFlag;
	}

	public void setGlobalFlag(Short globalFlag) {
		this.globalFlag = globalFlag;
	}

	public ScalarType getRawDataType() {
		return rawDataType;
	}

	public void setRawDataType(ScalarType rawDataType) {
		this.rawDataType = rawDataType;
	}

	public ScalarType getDataType() {
		return dataType;
	}

	public void setDataType(ScalarType dataType) {
		this.dataType = dataType;
	}

	public String getMeaQuantityId() {
		return meaQuantityId;
	}

	public long getUnitId() {
		return unitId;
	}

	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}