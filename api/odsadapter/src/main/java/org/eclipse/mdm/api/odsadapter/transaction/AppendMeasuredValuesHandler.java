/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.corba.CorbaAppendMeasuredValuesHandler;
import org.eclipse.mdm.api.odsadapter.transaction.http.HttpAppendMeasuredValuesHandler;

/**
 * Appends values to existing measured values.
 *
 * @author Alexander Knoblauch, Peak Solution GmbH
 */
public final class AppendMeasuredValuesHandler {

	private final ODSTransaction transaction;

	private List<WriteRequest> writeRequests = new ArrayList<>();

	/**
	 * Constructor.
	 *
	 * @param transaction The owning {@link ODSTransaction}.
	 */
	public AppendMeasuredValuesHandler(ODSTransaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * Adds given {@link WriteRequest} to be processed.
	 *
	 * @param writeRequest The {@code WriteRequest}.
	 */
	public void addRequest(WriteRequest writeRequest) {
		writeRequests.add(writeRequest);
	}

	/**
	 * Appends measured values to existing columns from registered
	 * {@link WriteRequest}s
	 *
	 * @throws OdsException        Thrown if the execution fails.
	 * @throws DataAccessException Thrown if the execution fails.
	 * @throws IOException         Thrown if a file transfer operation fails.
	 */
	public void execute() throws OdsException, DataAccessException, IOException {

		Map<String, List<WriteRequest>> channelGroupToWriteRequests = getChannelGroupToWriteRequests(writeRequests);

		Map<String, String> mq2LcMapping = transaction.loadMappingToLocalColumnIds(writeRequests);
		if (mq2LcMapping.isEmpty()) {
			// No localcolumns exist yet. Actually this should have been a write instead of append
			WriteRequestHandler writeRequestHandler = new WriteRequestHandler(transaction);
			for (WriteRequest wr : writeRequests) {
				writeRequestHandler.addRequest(wr);
			}
			writeRequestHandler.execute();
			this.writeRequests.clear();
		} else {
			if (transaction.getModelManager() instanceof ODSCorbaModelManager) {
				CorbaAppendMeasuredValuesHandler h = new CorbaAppendMeasuredValuesHandler(transaction);

				channelGroupToWriteRequests.forEach(h::append);
			} else {
				HttpAppendMeasuredValuesHandler h = new HttpAppendMeasuredValuesHandler(transaction);

				channelGroupToWriteRequests.forEach(
						(channelGroupId, writeRequestList) -> h.append(channelGroupId, writeRequestList, mq2LcMapping));
			}
		}

		this.writeRequests.clear();
	}

	/**
	 * Generate a map, where the registered {@link WriteRequest}s are mapped to the
	 * channel group id
	 * 
	 * @param writeRequests
	 * @return
	 */
	private Map<String, List<WriteRequest>> getChannelGroupToWriteRequests(List<WriteRequest> writeRequests) {
		Map<String, List<WriteRequest>> returnVal = new HashMap<>();

		writeRequests.forEach(wr -> {
			List<WriteRequest> list = returnVal.computeIfAbsent(wr.getChannelGroup().getID(), v -> new ArrayList<>());
			list.add(wr);

		});

		return returnVal;
	}
}