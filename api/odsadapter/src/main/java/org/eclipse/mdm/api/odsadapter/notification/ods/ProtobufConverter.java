/********************************************************************************
 * Copyright (c) 2015-2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.notification.ods;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.notification.NotificationFilter;
import org.eclipse.mdm.api.base.notification.NotificationFilter.ModificationType;

import ods.notification.OdsNotification.NotificationTypeEnum;
import ods.notification.OdsNotification.RegistrationRequest;
import ods.notification.OdsNotification.RegistrationRequest.NotificationModeEnum;
import ods.notification.OdsNotification.RegistrationRequest.TypeElement;

/**
 * Helper class for converting between protobuf and mdm types.
 * 
 * @since 1.0.0
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
public class ProtobufConverter {

	/**
	 * Convert a notification filter to a registration.
	 * 
	 * @param filter notification filter.
	 * @return registration corresponding to the given filter.
	 */
	public static RegistrationRequest from(NotificationFilter filter) {
		List<Long> aids = filter.getEntityTypes().stream().map(e -> Long.valueOf(e.getId()))
				.collect(Collectors.toList());
		RegistrationRequest.Builder builder = RegistrationRequest.newBuilder().setMode(NotificationModeEnum.NM_PUSH);

		for (ModificationType type : filter.getTypes()) {
			builder.addEntries(TypeElement.newBuilder().addAllAids(aids).setType(ProtobufConverter.from(type)).build());
		}

		return builder.build();
	}

	/**
	 * @param t mdm modification type.
	 * @return protobuf notification type.
	 */
	public static NotificationTypeEnum from(ModificationType t) {
		switch (t) {
		case INSTANCE_CREATED:
			return NotificationTypeEnum.NT_NEW;
		case INSTANCE_MODIFIED:
			return NotificationTypeEnum.NT_MODIFY;
		case INSTANCE_DELETED:
			return NotificationTypeEnum.NT_DELETE;
		case MODEL_MODIFIED:
			return NotificationTypeEnum.NT_MODEL;
		case SECURITY_MODIFIED:
			return NotificationTypeEnum.NT_SECURITY;
		default:
			throw new IllegalArgumentException("Invalid enum value!"); // TODO
		}
	}

	/**
	 * @param t protobuf notification type
	 * @return mdm notification type
	 */
	public static ModificationType to(NotificationTypeEnum t) {
		switch (t) {
		case NT_NEW:
			return ModificationType.INSTANCE_CREATED;
		case NT_MODIFY:
			return ModificationType.INSTANCE_MODIFIED;
		case NT_DELETE:
			return ModificationType.INSTANCE_DELETED;
		case NT_MODEL:
			return ModificationType.MODEL_MODIFIED;
		case NT_SECURITY:
			return ModificationType.SECURITY_MODIFIED;
		default:
			throw new IllegalArgumentException("Invalid enum value!"); // TODO
		}
	}
}
