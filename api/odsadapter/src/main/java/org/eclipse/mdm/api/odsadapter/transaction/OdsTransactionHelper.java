/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.transaction;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.ODSCorbaContext;
import org.eclipse.mdm.api.odsadapter.ODSHttpContext;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.corba.CorbaTransactionHelper;
import org.eclipse.mdm.api.odsadapter.transaction.http.HttpTransactionHelper;

public interface OdsTransactionHelper {

	static OdsTransactionHelper get(ODSContext context) {
		if (context instanceof ODSHttpContext) {
			return new HttpTransactionHelper((ODSHttpContext) context);
		} else if (context instanceof ODSCorbaContext) {
			return new CorbaTransactionHelper((ODSCorbaContext) context);
		} else {
			throw new DataAccessException("Expected ODSContext of ODSHttpContext or ODSCorbaContext, but got: "
					+ context.getClass().getSimpleName());
		}
	}

	void startTransaction() throws OdsException;

	void commitTransaction() throws OdsException;

	void abortTransaction() throws OdsException;

	InsertStatement getInsertStatement(ODSTransaction transaction, EntityType entityType);

	UpdateStatement getUpdateStatement(ODSTransaction transaction, EntityType entityType, boolean ignoreChildren);

	DeleteStatement getDeleteStatement(ODSTransaction transaction, EntityType entityType, boolean ignoreChildren);

	List<String> loadRelatedIds(EntityType entityType, String iid, String relationName);

	/**
	 * Processes N-to-M relations for the given entities
	 * 
	 * @param entities The processed {@code Entity}s.
	 * @throws DataAccessException Thrown if the execution fails.
	 */
	<T extends Entity> void processNtoMRelations(Collection<T> entities);

	Map<? extends Entity, ? extends String> getLinks(ODSEntityType et, List<Entity> entities);
}
