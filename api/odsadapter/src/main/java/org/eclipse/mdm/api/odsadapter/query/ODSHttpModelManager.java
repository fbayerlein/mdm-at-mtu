/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.query;

import static java.util.stream.Collectors.groupingBy;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.ODSHttpContext;
import org.eclipse.mdm.api.odsadapter.lookup.config.DefaultEntityConfigRepositoryLoader;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfig.Key;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfigRepository;
import org.eclipse.mdm.api.odsadapter.lookup.config.EntityConfigRepositoryLoader;
import org.eclipse.mdm.api.odsadapter.utils.ODSEnum;
import org.eclipse.mdm.api.odsadapter.utils.OdsHttpRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ods.Ods.ContextVariables;
import ods.Ods.ContextVariables.ContextVariableValue.ValueOneOfCase;
import ods.Ods.ContextVariablesFilter;
import ods.Ods.DataMatrices;
import ods.Ods.DataTypeEnum;
import ods.Ods.Model;
import ods.Ods.SelectStatement;
import ods.Ods.SelectStatement.AttributeItem;
import ods.Ods.SelectStatement.ConditionItem;
import ods.Ods.SelectStatement.ConditionItem.Condition;
import ods.Ods.SelectStatement.ConditionItem.Condition.OperatorEnum;
import ods.Ods.StringArray;

/**
 * ODS implementation of the {@link ModelManager} interface.
 *
 * 
 */
public class ODSHttpModelManager implements ODSModelManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSHttpModelManager.class);

	private final Map<String, EntityType> entityTypesByName = new HashMap<>();

	private ZoneId timeZone;

	private final ODSHttpContext context;
	private final Lock write;
	private final Lock read;

	private EntityConfigRepository entityConfigRepository;
	private EntityConfigRepositoryLoader entityConfigRepositoryLoader;

	private Client client;
	private WebTarget session;
	private String sourceName;

	/**
	 * Constructor.
	 *
	 * @param orb       Used to activate CORBA service objects.
	 * @param aoSession The underlying ODS session.
	 * @throws Exception Thrown on errors.
	 */
	public ODSHttpModelManager(String sourceName, ODSHttpContext context, WebTarget session, Client client)
			throws OdsException {
		this(sourceName, context, session, client, new DefaultEntityConfigRepositoryLoader());
	}

	/**
	 * Constructor.
	 *
	 * @param orb       Used to activate CORBA service objects.
	 * @param aoSession The underlying ODS session.
	 * @throws OdsException Thrown on errors.
	 */
	public ODSHttpModelManager(String sourceName, ODSHttpContext context, WebTarget session, Client client,
			EntityConfigRepositoryLoader entityConfigRepositoryLoader) throws OdsException {
		try {
			this.context = context;
			this.session = session;
			this.client = client;
			this.sourceName = sourceName;
			this.entityConfigRepositoryLoader = entityConfigRepositoryLoader;

			// setup locks
			ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
			write = reentrantReadWriteLock.writeLock();
			read = reentrantReadWriteLock.readLock();

			initialize();
		} catch (Exception e) {
			throw new OdsException("Could not initialize ModelManager: " + e.getMessage(), e);
		}
	}

	/**
	 * Returns the non root {@link EntityConfig} for given {@link Key}.
	 *
	 * @param <T> The concrete entity type.
	 * @param key Used as identifier.
	 * @return The non root {@code EntityConfig} is returned.
	 */
	public <T extends Entity> EntityConfig<T> findEntityConfig(Key<T> key) {
		read.lock();

		try {
			return entityConfigRepository.find(key);
		} finally {
			read.unlock();
		}
	}

	/**
	 * Returns the root {@link EntityConfig} for given {@link Key}.
	 *
	 * @param <T> The concrete entity type.
	 * @param key Used as identifier.
	 * @return The root {@code EntityConfig} is returned.
	 */
	public <T extends Entity> EntityConfig<T> getEntityConfig(Key<T> key) {
		read.lock();

		try {
			return entityConfigRepository.findRoot(key);
		} finally {
			read.unlock();
		}
	}

	/**
	 * Returns the {@link EntityConfig} associated with given {@link EntityType}.
	 *
	 * @param entityType Used as identifier.
	 * @return The {@code EntityConfig} is returned.
	 */
	public EntityConfig<?> getEntityConfig(EntityType entityType) {
		read.lock();

		try {
			return entityConfigRepository.find(entityType);
		} finally {
			read.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EntityType> listEntityTypes() {
		read.lock();

		try {
			return Collections.unmodifiableList(new ArrayList<>(entityTypesByName.values()));
		} finally {
			read.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityType getEntityType(Class<? extends Entity> entityClass) {
		read.lock();

		try {
			return getEntityConfig(new Key<>(entityClass)).getEntityType();
		} finally {
			read.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityType getEntityType(Class<? extends Entity> entityClass, ContextType contextType) {
		read.lock();

		try {
			return getEntityConfig(new Key<>(entityClass, contextType)).getEntityType();
		} finally {
			read.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityType getEntityType(String name) {
		read.lock();

		try {
			EntityType entityType = entityTypesByName.get(name);
			if (entityType == null) {
				throw new IllegalArgumentException("Entity with name '" + name + "' not found.");
			}

			return entityType;
		} finally {
			read.unlock();
		}
	}

	@Override
	public EntityType getEntityTypeById(String id) {
		Optional<EntityType> entityType = listEntityTypes().stream().filter(et -> et.getId().equals(id)).findFirst();
		if (!entityType.isPresent()) {
			throw new IllegalArgumentException("Entity with id '" + id + "' not found.");
		}

		return entityType.get();
	}

	@Override
	public String getMimeType(EntityType entityType) {
		try {
			return getEntityConfig(entityType).getMimeType();
		} catch (IllegalArgumentException e) {
			return "";
		}
	}

	/**
	 * Returns the {@link WebTarget} of this model manager.
	 *
	 * @return The {@code WebTarget} is returned.
	 */
	public WebTarget getSession() {
		write.lock();

		try {
			return session;
		} finally {
			write.unlock();
		}
	}

	public Client getClient() {
		return client;
	}

	public String getSourceName() {
		return sourceName;
	}

	/**
	 * Closes the ODS connection.
	 *
	 * @throws OdsException Thrown on errors.
	 */
	public void close() throws OdsException {
		closeSession(session);
	}

	/**
	 * Reloads the complete session context.
	 */
	public void reloadApplicationModel() {
		write.lock();
		WebTarget sessionOld = session;
		try {
			entityTypesByName.clear();

			session = context.newSession();
			initialize();
		} catch (ConnectionException e) {
			LOGGER.error("Unable to reload the application model due to: " + e.getMessage(), e);
		} catch (OdsException e) {
			LOGGER.error("Unable to reload the application model due to: " + e.getMessage(), e);
		} finally {
			write.unlock();
		}

		try {
			closeSession(sessionOld);
		} catch (Exception e) {
			LOGGER.debug("Unable to close replaced session due to: " + e.getMessage(), e);
		}
	}

	private void closeSession(WebTarget session) throws OdsException {
		try {
			read.lock();

			LOGGER.info("Closing connection to ODS Server '{}': {}", context.getSourceName(), session.getUri());
			session.request().delete(Void.class);
		} catch (Exception e) {
			throw new OdsException("Could not close session: " + e.getMessage(), e);
		} finally {
			read.unlock();
		}
	}

	/**
	 * Add an configuration, e.g. for an ODSEntity as result
	 * 
	 * @param entityConfig
	 */
	public void addEntity(EntityConfig<?> entityConfig) {
		this.entityConfigRepository.register(entityConfig);
	}

	/**
	 * Initializes this model manager by caching the application model and loading
	 * the {@link EntityConfig}s.
	 *
	 * @throws OdsException Thrown on errors.
	 */
	private void initialize() throws OdsException {
		loadApplicationModel();
		entityConfigRepository = entityConfigRepositoryLoader.loadEntityConfigurations(this);
		timeZone = loadTimeZone();
	}

	/**
	 * Caches the whole application model as provided by the ODS session.
	 *
	 * @throws OdsException Thrown on errors.
	 */
	private void loadApplicationModel() throws OdsException {
		LOGGER.debug("Reading the application model...");

		long start = System.currentTimeMillis();
		Model model = OdsHttpRequests.request(session, "model-read", null, Model.class);
		Map<String, String> units = getUnitMapping(model.getEntitiesMap());

		loadODSEnumerations(model);

		// create entity types (incl. attributes)
		Map<String, ODSEntityType> entityTypesByID = new HashMap<>();
		for (Model.Entity applElem : model.getEntitiesMap().values()) {
			String odsID = Long.toString(applElem.getAid());

			ODSEntityType entityType = new ODSEntityType(sourceName, applElem, units);
			entityTypesByName.put(applElem.getName(), entityType);
			entityTypesByID.put(odsID, entityType);
		}

		for (Model.Entity applElem : model.getEntitiesMap().values()) {

			// create relations
			List<Relation> relations = new ArrayList<>();
			for (ods.Ods.Model.Relation applRel : applElem.getRelationsMap().values()) {
				EntityType source = entityTypesByID.get(Long.toString(applElem.getAid()));
				EntityType target = entityTypesByID.get(Long.toString(applRel.getEntityAid()));
				relations.add(new ODSRelation(applRel, source, target));
			}

			// assign relations to their source entity types
			relations.stream().collect(groupingBy(Relation::getSource))
					.forEach((e, r) -> ((ODSEntityType) e).setRelations(r));
		}

		long stop = System.currentTimeMillis();
		LOGGER.debug("{} entity types loaded in {} ms.", entityTypesByName.size(), stop - start);
	}

	private void loadODSEnumerations(Model model) throws OdsException {
		LOGGER.debug("Loading ODS enumerations...");
		long t1 = System.currentTimeMillis();

		EnumRegistry er = EnumRegistry.getInstance();

		for (Model.Enumeration eas : model.getEnumerationsMap().values()) {

			Enumeration<ODSEnum> enumdyn = new Enumeration<>(sourceName, ODSEnum.class, eas.getName());
			for (Map.Entry<String, Integer> enumItemStruct : eas.getItemsMap().entrySet()) {
				enumdyn.addValue(new ODSEnum(enumItemStruct.getKey(), enumItemStruct.getValue()));
			}
			er.add(enumdyn);

		}
		LOGGER.debug("Loading enumerations took {} ms", System.currentTimeMillis() - t1);
	}

	/**
	 * Loads all available {@link Unit} names mapped by their instance IDs.
	 *
	 * @param applElems The application element meta data instances.
	 * @return The unit names mapped by the corresponding instance IDs.
	 * @throws OdsException Thrown if unable to load the unit mappings.
	 */
	private Map<String, String> getUnitMapping(Map<String, Model.Entity> entities) {
		Model.Entity unitEntity = entities.values().stream().filter(ae -> "AoUnit".equals(ae.getBaseName())).findAny()
				.orElseThrow(() -> new IllegalStateException("Application element 'Unit' is not defined."));

		SelectStatement select = SelectStatement.newBuilder()
				.addColumns(AttributeItem.newBuilder().setAid(unitEntity.getAid()).setAttribute("Id"))
				.addColumns(AttributeItem.newBuilder().setAid(unitEntity.getAid()).setAttribute("Name")).build();

		try {
			DataMatrices dms = OdsHttpRequests.request(session, "data-read", select, DataMatrices.class);

			Map<String, String> units = new HashMap<>();
			for (int i = 0; i < dms.getMatrices(0).getColumns(0).getLonglongArray().getValuesCount(); i++) {

				String unitID = Long.toString(dms.getMatrices(0).getColumns(0).getLonglongArray().getValues(i));
				String unitName = dms.getMatrices(0).getColumns(1).getStringArray().getValues(i);
				units.put(unitID, unitName);
			}

			return units;
		} catch (OdsException e) {
			throw new DataAccessException("Cannot load unit mapping.", e);
		}
	}

	private ZoneId loadTimeZone() throws OdsException {
		String timezone = DEFAULT_TIMEZONE;

		ODSEntityType etEnv = (ODSEntityType) getEntityType(Environment.class);
		if (etEnv == null) {
			throw new OdsException("Cannot find EntityType for Entity " + Environment.class + "!");
		}
		SelectStatement.Builder select = SelectStatement.newBuilder();
		select.addColumns(AttributeItem.newBuilder().setAid(etEnv.getODSID()).setAttribute(Environment.ATTR_TIMEZONE));

		DataMatrices dms = OdsHttpRequests.request(session, "data-read", select.build(), DataMatrices.class);

		if (dms.getMatricesCount() > 0 && dms.getMatrices(0).getColumnsCount() > 0
				&& dms.getMatrices(0).getColumns(0).getStringArray().getValuesCount() > 0) {
			timezone = dms.getMatrices(0).getColumns(0).getStringArray().getValues(0);
		}

		return ZoneId.of(timezone);
	}

	@Override
	public long getUserId() {
		try {
			// context variable name that contains the username of the current ods session
			final String USER = "USER";

			ODSEntityType userType = (ODSEntityType) getEntityType(User.class);

			ContextVariables contextVariables = OdsHttpRequests.request(session, "context-read",
					ContextVariablesFilter.newBuilder().setPattern(USER).build(), ContextVariables.class);
			if (!contextVariables.getVariablesMap().containsKey(USER)
					|| contextVariables.getVariablesMap().get(USER).getValueOneOfCase() != ValueOneOfCase.STRING_ARRAY
					|| contextVariables.getVariablesMap().get(USER).getStringArray().getValuesCount() != 1) {
				throw new OdsException("Expected context variable " + USER
						+ " to contain exactly 1 string value, but got: " + contextVariables);
			}
			String username = contextVariables.getVariablesOrThrow(USER).getStringArray().getValues(0);

			SelectStatement select = SelectStatement.newBuilder()
					.addColumns(AttributeItem.newBuilder().setAid(userType.getODSID()).setAttribute(User.ATTR_ID))
					.addWhere(ConditionItem.newBuilder()
							.setCondition(Condition.newBuilder().setAid(userType.getODSID())
									.setAttribute(User.ATTR_NAME).setOperator(OperatorEnum.OP_EQ)
									.setStringArray(StringArray.newBuilder().addValues(username))))
					.build();

			DataMatrices dms = OdsHttpRequests.request(session, "data-read", select, DataMatrices.class);

			if (dms.getMatricesCount() != 1 || dms.getMatrices(0).getColumnsCount() != 1
					|| dms.getMatrices(0).getColumns(0).getDataType() != DataTypeEnum.DT_LONGLONG
					|| dms.getMatrices(0).getColumns(0).getLonglongArray().getValuesCount() != 1) {
				throw new OdsException(
						"Expected exactly 1 DataMatrix with 1 DT_LONGONG Column and 1 value for query " + select);
			}

			return dms.getMatrices(0).getColumns(0).getLonglongArray().getValues(0);
		} catch (OdsException e) {
			throw new DataAccessException("Cannot load user id.", e);
		}
	}

	@Override
	public ZoneId getTimeZone() {
		return timeZone;
	}
}
