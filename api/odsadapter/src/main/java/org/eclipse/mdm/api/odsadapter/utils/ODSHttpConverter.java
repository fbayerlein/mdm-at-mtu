/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.WebApplicationException;

import org.asam.ods.NameValueSeqUnit;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.DoubleComplex;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.FloatComplex;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Aggregation;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.odsadapter.query.ODSAttribute;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.eclipse.mdm.api.odsadapter.transaction.ColumnAttributes;

import com.google.common.collect.Sets;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import com.google.protobuf.ByteString;

import ods.Ods.BooleanArray;
import ods.Ods.ByteArray;
import ods.Ods.BytestrArray;
import ods.Ods.DataMatrix;
import ods.Ods.DataMatrix.Column;
import ods.Ods.DataMatrix.Column.BooleanArrays;
import ods.Ods.DataMatrix.Column.ByteArrays;
import ods.Ods.DataMatrix.Column.BytestrArrays;
import ods.Ods.DataMatrix.Column.DoubleArrays;
import ods.Ods.DataMatrix.Column.FloatArrays;
import ods.Ods.DataMatrix.Column.LongArrays;
import ods.Ods.DataMatrix.Column.LonglongArrays;
import ods.Ods.DataMatrix.Column.StringArrays;
import ods.Ods.DataMatrix.Column.UnknownArray;
import ods.Ods.DataMatrix.Column.UnknownArrays;
import ods.Ods.DataTypeEnum;
import ods.Ods.DoubleArray;
import ods.Ods.ErrorInfo;
import ods.Ods.FloatArray;
import ods.Ods.LongArray;
import ods.Ods.LonglongArray;
import ods.Ods.StringArray;

public class ODSHttpConverter {

	public static Optional<ErrorInfo> readErrorInfo(WebApplicationException e) {
		if (OdsProtobufMessageBodyProvider.MEDIA_TYPE.equals(e.getResponse().getMediaType())
				|| OdsJsonMessageBodyProvider.MEDIA_TYPE.equals(e.getResponse().getMediaType())) {
			ErrorInfo error = e.getResponse().readEntity(ErrorInfo.class);

			return Optional.of(error);
		} else {
			return Optional.empty();
		}
	}

	public static OdsException toOdsException(WebApplicationException e) {
		if (OdsProtobufMessageBodyProvider.MEDIA_TYPE.equals(e.getResponse().getMediaType())
				|| OdsJsonMessageBodyProvider.MEDIA_TYPE.equals(e.getResponse().getMediaType())) {
			ErrorInfo error = e.getResponse().readEntity(ErrorInfo.class);

			return new OdsException(error.getErrCode().name() + ": " + error.getReason(), e);
		} else {
			return new OdsException(
					e.getResponse().getStatus() + " " + e.getResponse().getStatusInfo().getReasonPhrase() + ": "
							+ e.getResponse().readEntity(String.class),
					e);
		}

	}

	public static Column.Builder toODSIdColumn(Attribute attribute, Collection<String> values) {
		Column.Builder c = Column.newBuilder();

		c.setLonglongArray(LonglongArray.newBuilder()
				.addAllValues(() -> values.stream().map(v -> ODSHttpConverter.toODSID(v)).iterator()).build());

		return c;
	}

	public static Column.Builder toODSColumn(Attribute attribute, List<Value> values, ZoneId timeZone) {
		Column.Builder c = Column.newBuilder();

		boolean isNullNeeded = false;
		boolean[] isNull = new boolean[values.size()];

		for (int i = 0; i < values.size(); i++) {
			if (!values.get(i).isValid()) {
				isNullNeeded = true;
				isNull[i] = true;
			}
		}

		if (isNullNeeded) {
			c.addAllIsNull(Booleans.asList(isNull));
		}

		if (attribute.getValueType() == ValueType.STRING) {
			if (((ODSAttribute) attribute).isIdAttribute()) {
				return c.setLonglongArray(
						LonglongArray.newBuilder()
								.addAllValues(() -> values.stream()
										.map(v -> ODSHttpConverter.toODSID(v.extract(ValueType.STRING))).iterator())
								.build());
			} else {
				return c.setStringArray(toStringArray(values));
			}
		} else if (attribute.getValueType() == ValueType.STRING_SEQUENCE) {
			if (((ODSAttribute) attribute).isIdAttribute()) {
				return c.setLonglongArrays(
						LonglongArrays.newBuilder()
								.addAllValues(() -> values.stream()
										.map(v -> toLonglongArray(v.extract(ValueType.STRING_SEQUENCE))).iterator())
								.build());
			} else {

				return c.setStringArrays(
						toStringArrays(values.stream().map(v -> toStringArray(v.extract(ValueType.STRING_SEQUENCE)))));
			}
		} else if (attribute.getValueType() == ValueType.DATE) {
			return c.setStringArray(dateToStringArray(values, timeZone));
		} else if (attribute.getValueType() == ValueType.DATE_SEQUENCE) {
			return c.setStringArrays(toStringArrays(
					values.stream().map(v -> toStringArray(v.extract(ValueType.DATE_SEQUENCE), timeZone))));
		} else if (attribute.getValueType() == ValueType.BOOLEAN) {
			return c.setBooleanArray(toBooleanArray(values));
		} else if (attribute.getValueType() == ValueType.BOOLEAN_SEQUENCE) {
			return c.setBooleanArrays(
					toBooleanArrays(values.stream().map(v -> toBooleanArray(v.extract(ValueType.BOOLEAN_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.BYTE) {
			return c.setByteArray(ByteArray.newBuilder().setValues(toByteString(values)).build());
		} else if (attribute.getValueType() == ValueType.BYTE_SEQUENCE) {
			return c.setByteArrays(
					toByteArrays(values.stream().map(v -> toByteArray(v.extract(ValueType.BYTE_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.SHORT) {
			return c.setLongArray(shortToLongArray(values));
		} else if (attribute.getValueType() == ValueType.SHORT_SEQUENCE) {
			return c.setLongArrays(
					toLongArrays(values.stream().map(v -> toLongArray(v.extract(ValueType.SHORT_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.INTEGER) {
			return c.setLongArray(integerToLongArray(values));
		} else if (attribute.getValueType() == ValueType.INTEGER_SEQUENCE) {
			return c.setLongArrays(
					toLongArrays(values.stream().map(v -> toLongArray(v.extract(ValueType.INTEGER_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.LONG) {
			return c.setLonglongArray(toLonglongArray(values));
		} else if (attribute.getValueType() == ValueType.LONG_SEQUENCE) {
			return c.setLonglongArrays(
					toLonglongArrays(values.stream().map(v -> toLonglongArray(v.extract(ValueType.LONG_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.FLOAT) {
			return c.setFloatArray(toFloatArray(values));
		} else if (attribute.getValueType() == ValueType.FLOAT_SEQUENCE) {
			return c.setFloatArrays(
					toFloatArrays(values.stream().map(v -> toFloatArray(v.extract(ValueType.FLOAT_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.DOUBLE) {
			return c.setDoubleArray(toDoubleArray(values));
		} else if (attribute.getValueType() == ValueType.DOUBLE_SEQUENCE) {
			return c.setDoubleArrays(
					toDoubleArrays(values.stream().map(v -> toDoubleArray(v.extract(ValueType.DOUBLE_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.FLOAT_COMPLEX) {
			return c.setFloatArray(complexToFloatArray(values));
		} else if (attribute.getValueType() == ValueType.FLOAT_COMPLEX_SEQUENCE) {
			return c.setFloatArrays(toFloatArrays(
					values.stream().map(v -> toFloatComplexArray(v.extract(ValueType.FLOAT_COMPLEX_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.DOUBLE_COMPLEX) {
			return c.setDoubleArray(complexToDoubleArray(values));
		} else if (attribute.getValueType() == ValueType.DOUBLE_COMPLEX_SEQUENCE) {
			return c.setDoubleArrays(toDoubleArrays(
					values.stream().map(v -> toDoubleComplexArray(v.extract(ValueType.DOUBLE_COMPLEX_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.ENUMERATION) {
			return c.setLongArray(enumToLongArray(values));
		} else if (attribute.getValueType() == ValueType.ENUMERATION_SEQUENCE) {
			return c.setLongArrays(
					toLongArrays(values.stream().map(v -> toLongArray(v.extract(ValueType.ENUMERATION_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.FILE_LINK) {
			if (((ODSAttribute) attribute).isUrlAttribute()) {
				List<String> s = values.stream().map(v -> toURLString(v.extract(ValueType.FILE_LINK)))
						.collect(Collectors.toList());

				return c.setStringArray(StringArray.newBuilder().addAllValues(s).build());
			} else {
				return c.setStringArray(StringArray.newBuilder()
						.addAllValues(() -> values.stream()
								.flatMap(v -> toStringArray(v.extract(ValueType.FILE_LINK)).getValuesList().stream())
								.iterator())
						.build());
			}

		} else if (attribute.getValueType() == ValueType.FILE_LINK_SEQUENCE) {
			// TODO consider isUrlAttribute?
			return c.setStringArrays(StringArrays.newBuilder().addAllValues(
					() -> values.stream().map(v -> toStringArray(v.extract(ValueType.FILE_LINK_SEQUENCE))).iterator())
					.build());

		} else if (attribute.getValueType() == ValueType.BYTE_STREAM) {
			return c.setBytestrArray(toBytestrArray(values));
		} else if (attribute.getValueType() == ValueType.BYTE_STREAM_SEQUENCE) {
			return c.setBytestrArrays(toBytestrArrays(
					values.stream().map(v -> toBytestrArray(v.extract(ValueType.BYTE_STREAM_SEQUENCE)))));
		} else if (attribute.getValueType() == ValueType.UNKNOWN) {
			return c.setUnknownArrays(toUnknownArrays(values, timeZone));
		} else {
			throw new RuntimeException("Not implemented " + attribute.getValueType());
		}

	}

	private static String toURLString(FileLink input) {
		if (input == null) {
			return "";
		}

		return input.getRemotePath();
	}

	private static UnknownArrays toUnknownArrays(List<Value> values, ZoneId timeZone) {
		return UnknownArrays.newBuilder()
				.addAllValues(() -> values.stream().map(v -> toUnknownArray(v, timeZone)).iterator()).build();
	}

	private static UnknownArray toUnknownArray(Value value, ZoneId timeZone) {
		if (value.getValueType() == ValueType.BYTE_STREAM_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_BYTESTR)
					.setBytestrArray(toBytestrArray(value.extract(ValueType.BYTE_STREAM_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.STRING_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_STRING)
					.setStringArray(toStringArray(value.extract(ValueType.STRING_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.DATE_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_DATE)
					.setStringArray(toStringArray(value.extract(ValueType.DATE_SEQUENCE), timeZone)).build();
		} else if (value.getValueType() == ValueType.BOOLEAN_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_BOOLEAN)
					.setBooleanArray(toBooleanArray(value.extract(ValueType.BOOLEAN_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.BYTE_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_BYTE)
					.setByteArray(toByteArray(value.extract(ValueType.BYTE_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.SHORT_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_SHORT)
					.setLongArray(toLongArray(value.extract(ValueType.SHORT_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.INTEGER_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_LONG)
					.setLongArray(toLongArray(value.extract(ValueType.INTEGER_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.LONG_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_LONGLONG)
					.setLonglongArray(toLonglongArray(value.extract(ValueType.LONG_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.FLOAT_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_FLOAT)
					.setFloatArray(toFloatArray(value.extract(ValueType.FLOAT_SEQUENCE))).build();
		} else if (value.getValueType() == ValueType.DOUBLE_SEQUENCE) {
			return UnknownArray.newBuilder().setDataType(DataTypeEnum.DT_DOUBLE)
					.setDoubleArray(toDoubleArray(value.extract(ValueType.DOUBLE_SEQUENCE))).build();
		} else {
			throw new RuntimeException("Not implemented " + value.getValueType());
		}
	}

	private static ByteString toByteString(List<Value> values) {
		return ByteString.copyFrom(
				Bytes.toArray(values.stream().map(v -> v.extract(ValueType.BYTE)).collect(Collectors.toList())));
	}

	private static StringArray toStringArray(List<Value> values) {
		return StringArray.newBuilder()
				.addAllValues(() -> values.stream().map(v -> v.extract(ValueType.STRING)).iterator()).build();
	}

	private static StringArray dateToStringArray(List<Value> values, ZoneId timeZone) {
		return StringArray.newBuilder().addAllValues(
				() -> values.stream().map(v -> ODSConverter.toODSDate(v.extract(ValueType.DATE), timeZone)).iterator())
				.build();
	}

	private static BooleanArray toBooleanArray(List<Value> values) {
		return BooleanArray.newBuilder()
				.addAllValues(() -> values.stream().map(v -> v.extract(ValueType.BOOLEAN)).iterator()).build();
	}

	private static LongArray shortToLongArray(List<Value> list) {
		return LongArray.newBuilder()
				.addAllValues(() -> list.stream().map(v -> (int) v.extract(ValueType.SHORT)).iterator()).build();
	}

	private static LongArray integerToLongArray(List<Value> list) {
		return LongArray.newBuilder()
				.addAllValues(() -> list.stream().map(v -> v.extract(ValueType.INTEGER)).iterator()).build();
	}

	private static LongArray enumToLongArray(List<Value> values) {
		return LongArray.newBuilder()
				.addAllValues(() -> values.stream()
						.map(v -> v.isValid() ? ODSEnumerations.toODSEnum(v.extract(ValueType.ENUMERATION)) : 0)
						.iterator())
				.build();
	}

	private static LonglongArray toLonglongArray(List<Value> list) {
		return LonglongArray.newBuilder()
				.addAllValues(() -> list.stream().map(v -> v.extract(ValueType.LONG)).iterator()).build();
	}

	private static FloatArray toFloatArray(List<Value> values) {
		return FloatArray.newBuilder()
				.addAllValues(() -> values.stream().map(v -> v.extract(ValueType.FLOAT)).iterator()).build();
	}

	private static FloatArray complexToFloatArray(List<Value> values) {

		FloatArray.Builder builder = FloatArray.newBuilder();
		for (Value v : values) {
			FloatComplex f = v.extract(ValueType.FLOAT_COMPLEX);
			if (f == null) {
				builder.addValues(0f).addValues(0f);
			} else {
				builder.addValues(f.real()).addValues(f.imaginary());
			}
		}
		return builder.build();
	}

	private static DoubleArray toDoubleArray(List<Value> values) {
		return DoubleArray.newBuilder()
				.addAllValues(() -> values.stream().map(v -> v.extract(ValueType.DOUBLE)).iterator()).build();
	}

	private static DoubleArray complexToDoubleArray(List<Value> values) {

		DoubleArray.Builder builder = DoubleArray.newBuilder();
		for (Value v : values) {
			DoubleComplex d = v.extract(ValueType.DOUBLE_COMPLEX);
			if (d == null) {
				builder.addValues(0.0).addValues(0.0);
			} else {
				builder.addValues(d.real()).addValues(d.imaginary());
			}
		}
		return builder.build();
	}

	private static BytestrArray toBytestrArray(List<Value> values) {
		return BytestrArray.newBuilder().addAllValues(
				() -> values.stream().map(v -> ByteString.copyFrom(v.extract(ValueType.BYTE_STREAM))).iterator())
				.build();
	}

	private static StringArray toStringArray(String[] strings) {
		if (strings == null) {
			return StringArray.getDefaultInstance();
		}
		StringArray.Builder builder = StringArray.newBuilder();
		for (String v : strings) {
			builder.addValues(v);
		}
		return builder.build();
	}

	private static StringArray toStringArray(Instant[] dates, ZoneId timeZone) {
		if (dates == null) {
			return StringArray.getDefaultInstance();
		}
		StringArray.Builder builder = StringArray.newBuilder();
		for (Instant v : dates) {
			builder.addValues(ODSConverter.toODSDate(v, timeZone));
		}
		return builder.build();
	}

	private static BooleanArray toBooleanArray(boolean[] booleans) {
		if (booleans == null) {
			return BooleanArray.getDefaultInstance();
		}
		BooleanArray.Builder builder = BooleanArray.newBuilder();
		for (boolean v : booleans) {
			builder.addValues(v);
		}
		return builder.build();
	}

	private static ByteArray toByteArray(byte[] bytes) {
		if (bytes == null) {
			return ByteArray.getDefaultInstance();
		}
		return ByteArray.newBuilder().setValues(ByteString.copyFrom(bytes)).build();
	}

	private static LongArray toLongArray(short[] shorts) {
		if (shorts == null) {
			return LongArray.getDefaultInstance();
		}
		LongArray.Builder builder = LongArray.newBuilder();
		for (short v : shorts) {
			builder.addValues(v);
		}
		return builder.build();
	}

	private static LongArray toLongArray(int[] ints) {
		if (ints == null) {
			return LongArray.getDefaultInstance();
		}
		return LongArray.newBuilder().addAllValues(Ints.asList(ints)).build();
	}

	private static LongArray toLongArrays(EnumerationValue[] enums) {
		if (enums == null) {
			return LongArray.getDefaultInstance();
		}
		LongArray.Builder builder = LongArray.newBuilder();
		for (EnumerationValue v : enums) {
			builder.addValues(ODSEnumerations.toODSEnum(v));
		}
		return builder.build();
	}

	private static LonglongArray toLonglongArray(long[] longs) {
		if (longs == null) {
			return LonglongArray.getDefaultInstance();
		}
		return LonglongArray.newBuilder().addAllValues(Longs.asList(longs)).build();
	}

	private static FloatArray toFloatArray(float[] floats) {
		if (floats == null) {
			return FloatArray.getDefaultInstance();
		}
		return FloatArray.newBuilder().addAllValues(Floats.asList(floats)).build();
	}

	private static DoubleArray toDoubleArray(double[] doubles) {
		if (doubles == null) {
			return DoubleArray.getDefaultInstance();
		}
		return DoubleArray.newBuilder().addAllValues(Doubles.asList(doubles)).build();
	}

	private static FloatArray toFloatComplexArray(FloatComplex[] floatComplex) {
		if (floatComplex == null) {
			return FloatArray.getDefaultInstance();
		}
		FloatArray.Builder builder = FloatArray.newBuilder();
		for (FloatComplex v : floatComplex) {
			builder.addValues(v.real()).addValues(v.imaginary());
		}
		return builder.build();
	}

	private static DoubleArray toDoubleComplexArray(DoubleComplex[] doubleComplex) {
		if (doubleComplex == null) {
			return DoubleArray.getDefaultInstance();
		}
		DoubleArray.Builder builder = DoubleArray.newBuilder();
		for (DoubleComplex v : doubleComplex) {
			builder.addValues(v.real()).addValues(v.imaginary());
		}
		return builder.build();
	}

	private static BytestrArray toBytestrArray(byte[][] bytestrs) {
		if (bytestrs == null) {
			return BytestrArray.getDefaultInstance();
		}
		BytestrArray.Builder builder = BytestrArray.newBuilder();
		for (byte[] v : bytestrs) {
			builder.addValues(ByteString.copyFrom(v));
		}
		return builder.build();
	}

	private static ByteArrays toByteArrays(Stream<ByteArray> byteArrays) {
		return ByteArrays.newBuilder().addAllValues(() -> byteArrays.iterator()).build();
	}

	private static StringArrays toStringArrays(Stream<StringArray> stringArrays) {
		return StringArrays.newBuilder().addAllValues(() -> stringArrays.iterator()).build();
	}

	private static BooleanArrays toBooleanArrays(Stream<BooleanArray> booleanArrays) {
		return BooleanArrays.newBuilder().addAllValues(() -> booleanArrays.iterator()).build();
	}

	private static LongArrays toLongArrays(Stream<LongArray> longArrays) {
		return LongArrays.newBuilder().addAllValues(() -> longArrays.iterator()).build();
	}

	private static LonglongArrays toLonglongArrays(Stream<LonglongArray> longlongArrays) {
		return LonglongArrays.newBuilder().addAllValues(() -> longlongArrays.iterator()).build();
	}

	private static FloatArrays toFloatArrays(Stream<FloatArray> floatArrays) {
		return FloatArrays.newBuilder().addAllValues(() -> floatArrays.iterator()).build();
	}

	private static DoubleArrays toDoubleArrays(Stream<DoubleArray> doubleArrays) {
		return DoubleArrays.newBuilder().addAllValues(() -> doubleArrays.iterator()).build();
	}

	private static BytestrArrays toBytestrArrays(Stream<BytestrArray> bytestrArrays) {
		return BytestrArrays.newBuilder().addAllValues(() -> bytestrArrays.iterator()).build();
	}

	private static LonglongArray toLonglongArray(String[] stringValues) {
		LonglongArray.Builder b = LonglongArray.newBuilder();
		for (String s : stringValues) {
			b.addValues(ODSHttpConverter.toODSID(s));
		}
		return b.build();
	}

	private static LongArray toLongArray(EnumerationValue[] enumValues) {
		LongArray.Builder b = LongArray.newBuilder();
		for (EnumerationValue ev : enumValues) {
			if (ev == null) {
				b.addValues(0);
			} else {
				b.addValues(ODSEnumerations.toODSEnum(ev));
			}
		}
		return b.build();
	}

	public static List<Value> fromODSColumn(Attribute attribute, Aggregation aggregation, String unit, Column column,
			ZoneId timezone) {
		DataTypeEnum dataType = column.getDataType();
		List<Boolean> flags = column.getIsNullList();
		List<Value> values = new ArrayList<>();

		if (((ODSAttribute) attribute).isIdAttribute() && Sets
				.immutableEnumSet(Aggregation.MINIMUM, Aggregation.MAXIMUM, Aggregation.DISTINCT, Aggregation.NONE)
				.contains(aggregation)) {
			if (DataTypeEnum.DT_LONGLONG == dataType) {
				LonglongArray odsValues = column.getLonglongArray();
				for (int i = 0; i < odsValues.getValuesCount(); i++) {
					values.add(createValue(attribute, aggregation, DataTypeEnum.DT_STRING, unit, isValid(flags, i),
							Long.toString(odsValues.getValues(i))));
				}
				return values;
			} else if (DataTypeEnum.DS_LONGLONG == dataType) {
				LonglongArray odsValues = column.getLonglongArray();
				for (int i = 0; i < odsValues.getValuesCount(); i++) {
					values.add(createValue(attribute, aggregation, DataTypeEnum.DS_STRING, unit, isValid(flags, i),
							Long.toString(odsValues.getValues(i))));
				}
				return values;
			} else if (DataTypeEnum.DT_LONG == dataType) {
				LonglongArray odsValues = column.getLonglongArray();
				for (int i = 0; i < odsValues.getValuesCount(); i++) {
					values.add(createValue(attribute, aggregation, DataTypeEnum.DT_STRING, unit, isValid(flags, i),
							Long.toString(odsValues.getValues(i))));
				}
				return values;
			} else if (DataTypeEnum.DS_LONG == dataType) {
				LonglongArray odsValues = column.getLonglongArray();
				for (int i = 0; i < odsValues.getValuesCount(); i++) {
					values.add(createValue(attribute, aggregation, DataTypeEnum.DS_STRING, unit, isValid(flags, i),
							Long.toString(odsValues.getValues(i))));
				}
				return values;
			}
		}

		if (((ODSAttribute) attribute).isUrlAttribute() && aggregation == Aggregation.NONE
				&& DataTypeEnum.DT_STRING == dataType) {
			StringArray odsValues = column.getStringArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						fromURLString(odsValues.getValues(i))));
			}
			return values;
		}

		if (DataTypeEnum.DT_STRING == dataType) {
			StringArray odsValues = column.getStringArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(
						createValue(attribute, aggregation, dataType, unit, isValid(flags, i), odsValues.getValues(i)));
			}
		} else if (DataTypeEnum.DS_STRING == dataType) {
			StringArrays odsValues = column.getStringArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						odsValues.getValues(i).getValuesList().toArray(new String[0])));
			}
		} else if (DataTypeEnum.DT_DATE == dataType) {
			StringArray odsValues = column.getStringArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						ODSConverter.fromODSDate(odsValues.getValues(i), timezone)));
			}
		} else if (DataTypeEnum.DS_DATE == dataType) {
			StringArrays odsValues = column.getStringArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i), ODSConverter
						.fromODSDateSeq(odsValues.getValues(i).getValuesList().toArray(new String[0]), timezone)));
			}
		} else if (DataTypeEnum.DT_BOOLEAN == dataType) {
			BooleanArray odsValues = column.getBooleanArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(
						createValue(attribute, aggregation, dataType, unit, isValid(flags, i), odsValues.getValues(i)));
			}
		} else if (DataTypeEnum.DS_BOOLEAN == dataType) {
			BooleanArrays odsValues = column.getBooleanArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						Booleans.toArray(odsValues.getValues(i).getValuesList())));
			}
		} else if (DataTypeEnum.DT_BYTE == dataType) {
			ByteArray odsValues = column.getByteArray();
			for (int i = 0; i < odsValues.getValues().size(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						odsValues.getValues().byteAt(i)));
			}
		} else if (DataTypeEnum.DS_BYTE == dataType) {
			ByteArrays odsValues = column.getByteArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						odsValues.getValues(i).getValues().toByteArray()));
			}
		} else if (DataTypeEnum.DT_SHORT == dataType) {
			LongArray odsValues = column.getLongArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						(short) odsValues.getValues(i)));
			}
		} else if (DataTypeEnum.DS_SHORT == dataType) {
			LongArrays odsValues = column.getLongArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						Shorts.toArray(odsValues.getValues(i).getValuesList())));
			}
		} else if (DataTypeEnum.DT_LONG == dataType) {
			LongArray odsValues = column.getLongArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(
						createValue(attribute, aggregation, dataType, unit, isValid(flags, i), odsValues.getValues(i)));
			}
		} else if (DataTypeEnum.DS_LONG == dataType) {
			LongArrays odsValues = column.getLongArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						Ints.toArray(odsValues.getValues(i).getValuesList())));
			}
		} else if (DataTypeEnum.DT_LONGLONG == dataType) {
			LonglongArray odsValues = column.getLonglongArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(
						createValue(attribute, aggregation, dataType, unit, isValid(flags, i), odsValues.getValues(i)));
			}
		} else if (DataTypeEnum.DS_LONGLONG == dataType) {
			LonglongArrays odsValues = column.getLonglongArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						Longs.toArray(odsValues.getValues(i).getValuesList())));
			}
		} else if (DataTypeEnum.DT_FLOAT == dataType) {
			FloatArray odsValues = column.getFloatArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(
						createValue(attribute, aggregation, dataType, unit, isValid(flags, i), odsValues.getValues(i)));
			}
		} else if (DataTypeEnum.DS_FLOAT == dataType) {
			FloatArrays odsValues = column.getFloatArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						Floats.toArray(odsValues.getValues(i).getValuesList())));
			}
		} else if (DataTypeEnum.DT_DOUBLE == dataType) {
			DoubleArray odsValues = column.getDoubleArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(
						createValue(attribute, aggregation, dataType, unit, isValid(flags, i), odsValues.getValues(i)));
			}
		} else if (DataTypeEnum.DS_DOUBLE == dataType) {
			DoubleArrays odsValues = column.getDoubleArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						Doubles.toArray(odsValues.getValues(i).getValuesList())));
			}
		} else if (DataTypeEnum.DT_BYTESTR == dataType) {
			BytestrArray odsValues = column.getBytestrArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						odsValues.getValues(i).toByteArray()));
			}
		} else if (DataTypeEnum.DS_BYTESTR == dataType) {
			BytestrArrays odsValues = column.getBytestrArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i), odsValues.getValues(i)
						.getValuesList().stream().map(bs -> bs.toByteArray()).toArray(byte[][]::new)));
			}
		} else if (DataTypeEnum.DT_COMPLEX == dataType) {
			FloatArray odsValues = column.getFloatArray();
			for (int i = 0; i < odsValues.getValuesCount() / 2; i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						readFloatComplex(odsValues, i)));
			}
		} else if (DataTypeEnum.DS_COMPLEX == dataType) {
			FloatArrays odsValues = column.getFloatArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						readFloatComplex(odsValues.getValues(i))));
			}
		} else if (DataTypeEnum.DT_DCOMPLEX == dataType) {
			DoubleArray odsValues = column.getDoubleArray();
			for (int i = 0; i < odsValues.getValuesCount() / 2; i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						readDoubleComplex(odsValues, i)));
			}
		} else if (DataTypeEnum.DS_DCOMPLEX == dataType) {
			DoubleArrays odsValues = column.getDoubleArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						readDoubleComplex(odsValues.getValues(i))));
			}
		} else if (DataTypeEnum.DT_ENUM == dataType) {
			LongArray odsValues = column.getLongArray();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				EnumerationValue enumValue = ODSEnumerations.fromODSEnum(attribute.getEnumObj(),
						odsValues.getValues(i));
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i), enumValue));
			}
		} else if (DataTypeEnum.DS_ENUM == dataType) {
			LongArrays odsValues = column.getLongArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				EnumerationValue[] enumValues = odsValues.getValues(i).getValuesList().stream()
						.map(ordinal -> ODSEnumerations.fromODSEnum(attribute.getEnumObj(), ordinal))
						.toArray(EnumerationValue[]::new);
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i), enumValues));
			}
		} else if (DataTypeEnum.DT_EXTERNALREFERENCE == dataType) {
			StringArray odsValues = column.getStringArray();
			for (int i = 0; i < odsValues.getValuesCount() / 3; i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						readExternalReference(odsValues)[i]));
			}
		} else if (DataTypeEnum.DS_EXTERNALREFERENCE == dataType) {
			StringArrays odsValues = column.getStringArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						readExternalReference(odsValues.getValues(i))));
			}
		} else if (DataTypeEnum.DT_UNKNOWN == dataType) {
			UnknownArrays odsValues = column.getUnknownArrays();
			for (int i = 0; i < odsValues.getValuesCount(); i++) {
				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
						toValues(odsValues.getValues(i), timezone)));
			}
//		} else if (DataTypeEnum.DT_BLOB == dataType) {
//			Blob[] odsValues = odsValueSeq.u.blobVal();
//			for (int i = 0; i < flags.length; i++) {
//				values.add(createValue(attribute, aggregation, dataType, unit, isValid(flags, i),
//						fromODSBlob(odsValues[i])));
//			}
		} else {
			throw new DataAccessException("Conversion for ODS data type '" + dataType.toString() + "' does not exist.");
		}

		return values;
	}

	private static StringArray toStringArray(FileLink[] fileLinks) {
		if (fileLinks == null) {
			return StringArray.getDefaultInstance();
		}
		StringArray.Builder builder = StringArray.newBuilder();
		for (FileLink filelink : fileLinks) {
			builder.addValues(filelink.getDescription());
			builder.addValues(filelink.getMimeType().toString());
			builder.addValues(filelink.getRemotePath());
		}
		return builder.build();
	}

	private static StringArray toStringArray(FileLink fileLink) {
		if (fileLink == null) {
			return StringArray.newBuilder().addValues("").addValues("").addValues("").build();
		} else {
			return StringArray.newBuilder().addValues(fileLink.getDescription())
					.addValues(fileLink.getMimeType().toString()).addValues(fileLink.getRemotePath()).build();
		}
	}

	private static FileLink[] readExternalReference(StringArray odsValues) {
		FileLink[] values = new FileLink[odsValues.getValuesCount() / 3];
		for (int i = 0; i < values.length; i++) {

			values[i] = FileLink.newRemote(odsValues.getValues(3 * i + 2), new MimeType(odsValues.getValues(3 * i + 1)),
					odsValues.getValues(3 * i), -1L, null, FileServiceType.EXTREF);
		}

		return values;
	}

	private static DoubleComplex readDoubleComplex(DoubleArray odsValues, int i) {
		return new DoubleComplex(odsValues.getValues(i * 2), odsValues.getValues(i * 2 + 1));
	}

	private static FloatComplex readFloatComplex(FloatArray odsValues, int i) {
		return new FloatComplex(odsValues.getValues(i * 2), odsValues.getValues(i * 2 + 1));
	}

	private static FloatComplex[] readFloatComplex(FloatArray odsValues) {
		FloatComplex[] values = new FloatComplex[odsValues.getValuesCount() / 2];
		for (int i = 0; i < values.length; i++) {
			values[i] = new FloatComplex(odsValues.getValues(i * 2), odsValues.getValues(i * 2 + 1));
		}
		return values;
	}

	private static DoubleComplex[] readDoubleComplex(DoubleArray odsValues) {
		DoubleComplex[] values = new DoubleComplex[odsValues.getValuesCount() / 2];
		for (int i = 0; i < values.length; i++) {
			values[i] = new DoubleComplex(odsValues.getValues(i * 2), odsValues.getValues(i * 2 + 1));
		}
		return values;
	}

	/**
	 * Creates a {@link Value} from the input.
	 * 
	 * @param attribute   The {@link Attribute}
	 * @param aggregation The {@link Aggregation} used when the input values were
	 *                    obtained
	 * @param dataType    The {@link DataTypeEnum} associated with the input value,
	 *                    evaluated only if aggregation != {@code Aggregation.NONE},
	 *                    otherwise that of the attribute is used
	 * @param unit        The unit of the input value
	 * @param valid       The validity flag of the input value
	 * @param input       The input value
	 * @return The {@link Value} created.
	 */
	private static Value createValue(Attribute attribute, Aggregation aggregation, DataTypeEnum dataType, String unit,
			boolean valid, Object input) {
		if (Aggregation.NONE == aggregation) {
			return attribute.createValue(unit, valid, input);
		} else {
			// TODO use ODSEnumerations.fromODSEnum(null, 0);
			ValueType<?> valueType = ODSHttpUtils.VALUETYPES.inverse().get(dataType);
			if (valueType.isEnumerationType() && attribute.getValueType().isEnumerationType()
					&& Sets.immutableEnumSet(Aggregation.MINIMUM, Aggregation.MAXIMUM, Aggregation.DISTINCT)
							.contains(aggregation)) {
				return valueType.create(getColumnName(attribute, aggregation), unit, valid, input,
						attribute.getEnumObj().getName());
			} else if (((ODSAttribute) attribute).isUrlAttribute()) {
				return ValueType.FILE_LINK.create(getColumnName(attribute, aggregation), unit, valid, input);
			} else {
				return valueType.create(getColumnName(attribute, aggregation), unit, valid, input);
			}
		}
	}

	/**
	 * Returns the name of the attribute with its applied aggregation as returned by
	 * the ODS Server, for example: MAXIMUM(sortIndex)
	 * 
	 * @param attribute
	 * @param aggregation
	 * @return the name of the attribute with the applied aggragation
	 */
	public static String getColumnName(Attribute attribute, Aggregation aggregation) {
		return String.format("%s(%s)", aggregation.name(), attribute.getName());
	}

	private static boolean isValid(List<Boolean> flags, int i) {
		if (i >= flags.size()) {
			return true;
		} else {
			return !flags.get(i);
		}
	}

	/**
	 * Converts a given MDM ID string to long
	 * 
	 * @param input The MDM ID string.
	 * @return The converted long is returned.
	 */
	public static long toODSID(String input) {
		try {
			return Long.valueOf(input);
		} catch (NumberFormatException e) {
			return 0L;
		}
	}

	/**
	 * Converts given {@link NameValueSeqUnit[]} to {@link MeasuredValues}s.
	 * 
	 * @param timezone             of the Environment
	 * @param odsMeasuredValuesSeq The {@code NameValueSeqUnit}s.
	 *
	 * @return The converted {@code MeasuredValues}s are returned.
	 * @throws DataAccessException Thrown on conversion errors.
	 */
	public static List<MeasuredValues> fromODSMeasuredValuesSeq(DataMatrix dataMatrix,
			Collection<ColumnAttributes> columnAttributesArray, ZoneId timezone) throws DataAccessException {
		List<MeasuredValues> measuredValues = new ArrayList<>();

		Map<String, ColumnAttributes> mapColumnAttributes = new HashMap<>();

		if (null != columnAttributesArray) {
			columnAttributesArray.forEach(ca -> mapColumnAttributes.put(ca.getName(), ca));
		}

		Column nameCol = dataMatrix.getColumnsList().get(0);
		Column valuesCol = dataMatrix.getColumnsList().get(1);
		Column flagsCol = dataMatrix.getColumnsList().get(2);
		Column genParamsCol = dataMatrix.getColumnsList().get(3);

		for (int i = 0; i < nameCol.getStringArray().getValuesCount(); i++) {
			String name = nameCol.getStringArray().getValues(i);

			UnknownArray ua = valuesCol.getUnknownArrays().getValuesList().get(i);
			short[] flags = Shorts.toArray(flagsCol.getLongArrays().getValues(i).getValuesList());
			double[] genParams = Doubles.toArray(genParamsCol.getDoubleArrays().getValues(i).getValuesList());

			ColumnAttributes ca = mapColumnAttributes.get(name);

			ScalarType scalarType;
			Object values;
			if (DataTypeEnum.DT_STRING == ua.getDataType()) {
				scalarType = ScalarType.STRING;
				values = ua.getStringArray().getValuesList().toArray(new String[0]);
			} else if (DataTypeEnum.DT_DATE == ua.getDataType()) {
				scalarType = ScalarType.DATE;
				values = ODSConverter.fromODSDateSeq(ua.getStringArray().getValuesList().toArray(new String[0]),
						timezone);
			} else if (DataTypeEnum.DT_BOOLEAN == ua.getDataType()) {
				scalarType = ScalarType.BOOLEAN;
				values = Booleans.toArray(ua.getBooleanArray().getValuesList());
			} else if (DataTypeEnum.DT_BYTE == ua.getDataType()) {
				scalarType = ScalarType.BYTE;
				values = ua.getByteArray().getValues().toByteArray();
			} else if (DataTypeEnum.DT_SHORT == ua.getDataType()) {
				scalarType = ScalarType.SHORT;
				values = Shorts.toArray(ua.getLongArray().getValuesList());
			} else if (DataTypeEnum.DT_LONG == ua.getDataType()) {
				scalarType = ScalarType.INTEGER;
				values = Ints.toArray(ua.getLongArray().getValuesList());
			} else if (DataTypeEnum.DT_LONGLONG == ua.getDataType()) {
				if (ca.getMimeType().contains("+timestamp")) {
					scalarType = ScalarType.DATE;
					values = datesFromODSLongSeq(ua.getLonglongArray().getValuesList());
				} else {
					scalarType = ScalarType.LONG;
					values = Longs.toArray(ua.getLonglongArray().getValuesList());
				}
			} else if (DataTypeEnum.DT_FLOAT == ua.getDataType()) {
				scalarType = ScalarType.FLOAT;
				values = Floats.toArray(ua.getFloatArray().getValuesList());
			} else if (DataTypeEnum.DT_DOUBLE == ua.getDataType()) {
				scalarType = ScalarType.DOUBLE;
				values = Doubles.toArray(ua.getDoubleArray().getValuesList());
			} else if (DataTypeEnum.DT_BYTESTR == ua.getDataType()) {
				scalarType = ScalarType.BYTE_STREAM;
				values = ua.getBytestrArray().getValuesList().stream().map(b -> b.toByteArray()).toArray(byte[][]::new);
			} else if (DataTypeEnum.DT_COMPLEX == ua.getDataType()) {
				scalarType = ScalarType.FLOAT_COMPLEX;

				values = fromODSFloatComplexSeq(ua.getFloatArray());
			} else if (DataTypeEnum.DT_DCOMPLEX == ua.getDataType()) {
				scalarType = ScalarType.DOUBLE_COMPLEX;
				values = fromODSDoubleComplexSeq(ua.getDoubleArray());
			} else if (DataTypeEnum.DT_EXTERNALREFERENCE == ua.getDataType()) {
				scalarType = ScalarType.FILE_LINK;
				values = fromODSExternalReferenceSeq(ua.getStringArray());
			} else {
				throw new DataAccessException("Conversion for ODS measured points of type '"
						+ ua.getDataType().toString() + "' does not exist.");
			}
			measuredValues.add(scalarType.createMeasuredValues(name, ca.getUnitName(), ca.getSequenceRepresentation(),
					genParams, ca.isIndependentColumn(), ca.getAxisType(), values, flags));

		}

		return measuredValues;
	}

	private static Object toValues(UnknownArray ua, ZoneId timezone) {
		if (DataTypeEnum.DT_STRING == ua.getDataType()) {
			return ua.getStringArray().getValuesList().toArray(new String[0]);
		} else if (DataTypeEnum.DT_DATE == ua.getDataType()) {
			return ODSConverter.fromODSDateSeq(ua.getStringArray().getValuesList().toArray(new String[0]), timezone);
		} else if (DataTypeEnum.DT_BOOLEAN == ua.getDataType()) {
			return Booleans.toArray(ua.getBooleanArray().getValuesList());
		} else if (DataTypeEnum.DT_BYTE == ua.getDataType()) {
			return ua.getByteArray().getValues().toByteArray();
		} else if (DataTypeEnum.DT_SHORT == ua.getDataType()) {
			return Shorts.toArray(ua.getLongArray().getValuesList());
		} else if (DataTypeEnum.DT_LONG == ua.getDataType()) {
			return Ints.toArray(ua.getLongArray().getValuesList());
		} else if (DataTypeEnum.DT_LONGLONG == ua.getDataType()) {
			return Longs.toArray(ua.getLonglongArray().getValuesList());
		} else if (DataTypeEnum.DT_FLOAT == ua.getDataType()) {
			return Floats.toArray(ua.getFloatArray().getValuesList());
		} else if (DataTypeEnum.DT_DOUBLE == ua.getDataType()) {
			return Doubles.toArray(ua.getDoubleArray().getValuesList());
		} else if (DataTypeEnum.DT_BYTESTR == ua.getDataType()) {
			return ua.getBytestrArray().getValuesList().stream().map(b -> b.toByteArray()).toArray(byte[][]::new);
		} else if (DataTypeEnum.DT_COMPLEX == ua.getDataType()) {
			return fromODSFloatComplexSeq(ua.getFloatArray());
		} else if (DataTypeEnum.DT_DCOMPLEX == ua.getDataType()) {
			return fromODSDoubleComplexSeq(ua.getDoubleArray());
		} else if (DataTypeEnum.DT_EXTERNALREFERENCE == ua.getDataType()) {
			return fromODSExternalReferenceSeq(ua.getStringArray());
		} else {
			throw new DataAccessException(
					"Conversion for ODS measured points of type '" + ua.getDataType().toString() + "' does not exist.");
		}
	}

	private static ScalarType toScalarType(DataTypeEnum dataType) {
		if (DataTypeEnum.DT_STRING == dataType) {
			return ScalarType.STRING;
		} else if (DataTypeEnum.DT_DATE == dataType) {
			return ScalarType.DATE;
		} else if (DataTypeEnum.DT_BOOLEAN == dataType) {
			return ScalarType.BOOLEAN;
		} else if (DataTypeEnum.DT_BYTE == dataType) {
			return ScalarType.BYTE;
		} else if (DataTypeEnum.DT_SHORT == dataType) {
			return ScalarType.SHORT;
		} else if (DataTypeEnum.DT_LONG == dataType) {
			return ScalarType.INTEGER;
		} else if (DataTypeEnum.DT_LONGLONG == dataType) {
			return ScalarType.LONG;
		} else if (DataTypeEnum.DT_FLOAT == dataType) {
			return ScalarType.FLOAT;
		} else if (DataTypeEnum.DT_DOUBLE == dataType) {
			return ScalarType.DOUBLE;
		} else if (DataTypeEnum.DT_BYTESTR == dataType) {
			return ScalarType.BYTE_STREAM;
		} else if (DataTypeEnum.DT_COMPLEX == dataType) {
			return ScalarType.FLOAT_COMPLEX;
		} else if (DataTypeEnum.DT_DCOMPLEX == dataType) {
			return ScalarType.DOUBLE_COMPLEX;
		} else if (DataTypeEnum.DT_EXTERNALREFERENCE == dataType) {
			return ScalarType.FILE_LINK;
		} else {
			throw new DataAccessException(
					"Conversion from ODS DataTypeEnum value '" + dataType.toString() + "' does not exist.");
		}
	}

	/**
	 * Converts given List with timestamps to {@link LocalDateTime[]}.
	 *
	 * @param input list of timestamps.
	 * @return The converted {@code LocalDateTime}s are returned.
	 */
	private static Instant[] datesFromODSLongSeq(List<Long> input) {
		Instant[] result = new Instant[input.size()];
		for (int i = 0; i < result.length; i++) {
			result[i] = dateFromODSLong(input.get(i));
		}

		return result;
	}

	/**
	 * Converts given timestamp to {@link Instant}.
	 *
	 * @param input The timestamp
	 * @return The converted {@code Instant} is returned.
	 */
	private static Instant dateFromODSLong(long input) {
		return Instant.ofEpochSecond(input / 1000_000_000L, (int) (input % 1000_000_000L));
	}

	/**
	 * Converts given {@link FloatArray} to {@link FloatComplex[]}.
	 *
	 * @param input The {@code FloatArray}.
	 * @return The converted {@code FloatComplex}s are returned.
	 */
	private static FloatComplex[] fromODSFloatComplexSeq(FloatArray input) {
		List<FloatComplex> result = new ArrayList<>();
		if (input != null) {
			for (int i = 0; i < input.getValuesCount() / 2; i++) {
				result.add(new FloatComplex(input.getValues(2 * i), input.getValues(2 * i + 1)));
			}
		}

		return result.toArray(new FloatComplex[0]);
	}

	/**
	 * Converts given {@link DoubleArray} to {@link DoubleComplex[]}.
	 *
	 * @param input The {@code DoubleArray}.
	 * @return The converted {@code DoubleComplex}s are returned.
	 */
	private static DoubleComplex[] fromODSDoubleComplexSeq(DoubleArray input) {
		List<DoubleComplex> result = new ArrayList<>();
		if (input != null) {
			for (int i = 0; i < input.getValuesCount() / 2; i++) {
				result.add(new DoubleComplex(input.getValues(2 * i), input.getValues(2 * i + 1)));
			}
		}

		return result.toArray(new DoubleComplex[0]);
	}

	/**
	 * Converts given {@link T_ExternalReference[]} to {@link FileLink[]}.
	 *
	 * @param input The {@code T_ExternalReference}s.
	 * @return The converted {@code FileLink}s are returned.
	 */
	private static FileLink[] fromODSExternalReferenceSeq(StringArray input) {
		List<FileLink> result = new ArrayList<>();
		if (input != null) {
			for (int i = 0; i < input.getValuesCount() / 3; i++) {
				String location = input.getValues(3 * i);
				String mimeType = input.getValues(3 * i + 1);
				String description = input.getValues(3 * i + 2);

				result.add(FileLink.newRemote(location, new MimeType(mimeType), description, -1L, null,
						FileServiceType.EXTREF));
			}
		}

		return result.toArray(new FileLink[result.size()]);
	}

	/**
	 * Converts given URL string to {@link FileLink}.
	 *
	 * @param input The URL string.
	 * @return The converted {@code FileLink} is returned.
	 */
	private static FileLink fromURLString(String input) {
		if (input == null || input.length() == 0) {
			return null;
		}
		return FileLink.newRemote(input, new MimeType(""), "", -1L, null, FileServiceType.EXTREF);
	}

	/**
	 * Converting the values to a {@link NameValueSeqUnit}, if values are null the
	 * {@link NameValueSeqUnit} will contains only invalid values
	 * 
	 * @param channel {@link Channel}
	 * @param values  to convert
	 * @param length  of the {@link NameValueSeqUnit}
	 * @return
	 */
	public static Column channelToColumn(Channel channel, Object values) {
		Column.Builder column = Column.newBuilder().setName(channel.getName());

		if (channel.getUnit() != null) {
			column.setUnitId(ODSHttpConverter.toODSID(channel.getUnit().getID()));
		}

		if (ScalarType.FLOAT.equals(channel.getScalarType())) {
			column.setFloatArray(toFloatArray((float[]) values));
		} else if (ScalarType.FLOAT_COMPLEX.equals(channel.getScalarType())) {
			column.setFloatArray(toFloatComplexArray((FloatComplex[]) values));
		} else if (ScalarType.INTEGER.equals(channel.getScalarType())) {
			column.setLongArray(toLongArray((int[]) values));
		} else if (ScalarType.BOOLEAN.equals(channel.getScalarType())) {
			column.setBooleanArray(toBooleanArray((boolean[]) values));
		} else if (ScalarType.BYTE.equals(channel.getScalarType())) {
			column.setByteArray(toByteArray((byte[]) values));
		} else if (ScalarType.DATE.equals(channel.getScalarType())) {
			column.setStringArray(toStringArray((String[]) values));
		} else if (ScalarType.DOUBLE.equals(channel.getScalarType())) {
			column.setDoubleArray(toDoubleArray((double[]) values));
		} else if (ScalarType.DOUBLE_COMPLEX.equals(channel.getScalarType())) {
			column.setDoubleArray(toDoubleComplexArray((DoubleComplex[]) values));
		} else if (ScalarType.FILE_LINK.equals(channel.getScalarType())) {
			column.setStringArray(toStringArray((FileLink[]) values));
		} else if (ScalarType.LONG.equals(channel.getScalarType())) {
			column.setLonglongArray(toLonglongArray((long[]) values));
		} else if (ScalarType.SHORT.equals(channel.getScalarType())) {
			column.setLongArray(toLongArray((short[]) values));
		} else if (ScalarType.STRING.equals(channel.getScalarType())) {
			column.setStringArray(toStringArray((String[]) values));
		} else {
			throw new DataAccessException("Not supported DataType " + values.getClass());
		}

		return column.build();
	}

	/**
	 * Converting the values to a {@link NameValueSeqUnit}, if values are null the
	 * {@link NameValueSeqUnit} will contains only invalid values
	 * 
	 * @param wr     {@link Channel}
	 * @param values to convert
	 * @param length of the {@link NameValueSeqUnit}
	 * @return
	 */
	public static UnknownArray writeRequestToUnknownArray(WriteRequest wr, int expectedLength) {
		UnknownArray.Builder unknown = UnknownArray.newBuilder();

		Object values = wr.getValues();

		if (ScalarType.FLOAT.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_FLOAT);
			unknown.setFloatArray(ensureLength(toFloatArray((float[]) values), expectedLength));
		} else if (ScalarType.FLOAT_COMPLEX.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_COMPLEX);
			unknown.setFloatArray(ensureLength(toFloatComplexArray((FloatComplex[]) values), expectedLength * 2));
		} else if (ScalarType.INTEGER.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_LONG);
			unknown.setLongArray(ensureLength(toLongArray((int[]) values), expectedLength));
		} else if (ScalarType.BOOLEAN.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_BOOLEAN);
			unknown.setBooleanArray(ensureLength(toBooleanArray((boolean[]) values), expectedLength));
		} else if (ScalarType.BYTE.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_BYTE);
			unknown.setByteArray(ensureLength(toByteArray((byte[]) values), expectedLength));
		} else if (ScalarType.DATE.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_DATE);
			unknown.setStringArray(ensureLength(toStringArray((String[]) values), expectedLength));
		} else if (ScalarType.DOUBLE.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_DOUBLE);
			unknown.setDoubleArray(ensureLength(toDoubleArray((double[]) values), expectedLength));
		} else if (ScalarType.DOUBLE_COMPLEX.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_DCOMPLEX);
			unknown.setDoubleArray(ensureLength(toDoubleComplexArray((DoubleComplex[]) values), expectedLength));
		} else if (ScalarType.FILE_LINK.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_EXTERNALREFERENCE);
			unknown.setStringArray(ensureLength(toStringArray((FileLink[]) values), expectedLength));
		} else if (ScalarType.LONG.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_LONGLONG);
			unknown.setLonglongArray(ensureLength(toLonglongArray((long[]) values), expectedLength));
		} else if (ScalarType.SHORT.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_SHORT);
			unknown.setLongArray(ensureLength(toLongArray((short[]) values), expectedLength));
		} else if (ScalarType.STRING.equals(wr.getRawScalarType())) {
			unknown.setDataType(DataTypeEnum.DT_STRING);
			unknown.setStringArray(ensureLength(toStringArray((String[]) values), expectedLength));
		} else {
			throw new DataAccessException("Unsupported DataType " + values.getClass());
		}

		return unknown.build();
	}

	private static LonglongArray ensureLength(LonglongArray longlongArray, int expectedLength) {
		if (longlongArray.getValuesCount() == expectedLength) {
			return longlongArray;
		} else if (longlongArray.getValuesCount() > expectedLength) {
			throw new IllegalArgumentException("LonglongArray is expected to have length " + expectedLength
					+ ", but has actualLength " + longlongArray.getValuesCount());
		} else {
			LonglongArray.Builder builder = longlongArray.toBuilder();
			for (int i = longlongArray.getValuesCount(); i < expectedLength; i++) {
				builder.addValues(0);
			}
			return builder.build();
		}
	}

	private static DoubleArray ensureLength(DoubleArray doubleArray, int expectedLength) {
		if (doubleArray.getValuesCount() == expectedLength) {
			return doubleArray;
		} else if (doubleArray.getValuesCount() > expectedLength) {
			throw new IllegalArgumentException("DoubleArray is expected to have length " + expectedLength
					+ ", but has actualLength " + doubleArray.getValuesCount());
		} else {
			DoubleArray.Builder builder = doubleArray.toBuilder();
			for (int i = doubleArray.getValuesCount(); i < expectedLength; i++) {
				builder.addValues(0.0);
			}
			return builder.build();
		}
	}

	private static StringArray ensureLength(StringArray stringArray, int expectedLength) {
		if (stringArray.getValuesCount() == expectedLength) {
			return stringArray;
		} else if (stringArray.getValuesCount() > expectedLength) {
			throw new IllegalArgumentException("StringArray is expected to have length " + expectedLength
					+ ", but has actualLength " + stringArray.getValuesCount());
		} else {
			StringArray.Builder builder = stringArray.toBuilder();
			for (int i = stringArray.getValuesCount(); i < expectedLength; i++) {
				builder.addValues("");
			}
			return builder.build();
		}
	}

	private static ByteArray ensureLength(ByteArray byteArray, int expectedLength) {
		ByteString bytes = byteArray.getValues();

		if (bytes.size() == expectedLength) {
			return byteArray;
		} else if (bytes.size() > expectedLength) {
			throw new IllegalArgumentException("ByteArray is expected to have length " + expectedLength
					+ ", but has actualLength " + bytes.size());
		} else {
			byte[] newBytes = new byte[expectedLength - bytes.size()];
			return ByteArray.newBuilder().setValues(bytes.concat(ByteString.copyFrom(newBytes))).build();
		}
	}

	private static BooleanArray ensureLength(BooleanArray booleanArray, int expectedLength) {
		if (booleanArray.getValuesCount() == expectedLength) {
			return booleanArray;
		} else if (booleanArray.getValuesCount() > expectedLength) {
			throw new IllegalArgumentException("BooleanArray is expected to have length " + expectedLength
					+ ", but has actualLength " + booleanArray.getValuesCount());
		} else {
			BooleanArray.Builder builder = booleanArray.toBuilder();
			for (int i = booleanArray.getValuesCount(); i < expectedLength; i++) {
				builder.addValues(false);
			}
			return builder.build();
		}
	}

	private static LongArray ensureLength(LongArray longArray, int expectedLength) {
		if (longArray.getValuesCount() == expectedLength) {
			return longArray;
		} else if (longArray.getValuesCount() > expectedLength) {
			throw new IllegalArgumentException("LongArray is expected to have length " + expectedLength
					+ ", but has actualLength " + longArray.getValuesCount());
		} else {
			LongArray.Builder builder = longArray.toBuilder();
			for (int i = longArray.getValuesCount(); i < expectedLength; i++) {
				builder.addValues(0);
			}
			return builder.build();
		}
	}

	private static FloatArray ensureLength(FloatArray floatArray, int expectedLength) {
		if (floatArray.getValuesCount() == expectedLength) {
			return floatArray;
		} else if (floatArray.getValuesCount() > expectedLength) {
			throw new IllegalArgumentException("FloatArray is expected to have length " + expectedLength
					+ ", but has actualLength " + floatArray.getValuesCount());
		} else {
			FloatArray.Builder builder = floatArray.toBuilder();
			for (int i = floatArray.getValuesCount(); i < expectedLength; i++) {
				builder.addValues(0f);
			}
			return builder.build();
		}
	}

	public static int getLength(WriteRequest wr) {
		ScalarType scalarType = wr.getRawScalarType();

		if (ScalarType.FLOAT.equals(scalarType)) {
			return ((float[]) wr.getValues()).length;
		} else if (ScalarType.FLOAT_COMPLEX.equals(scalarType)) {
			return ((float[]) wr.getValues()).length / 2;
		} else if (ScalarType.INTEGER.equals(scalarType)) {
			return ((int[]) wr.getValues()).length;
		} else if (ScalarType.BOOLEAN.equals(scalarType)) {
			return ((boolean[]) wr.getValues()).length;
		} else if (ScalarType.BYTE.equals(scalarType)) {
			return ((byte[]) wr.getValues()).length;
		} else if (ScalarType.DATE.equals(scalarType)) {
			return ((String[]) wr.getValues()).length;
		} else if (ScalarType.DOUBLE.equals(scalarType)) {
			return ((double[]) wr.getValues()).length;
		} else if (ScalarType.DOUBLE_COMPLEX.equals(scalarType)) {
			return ((double[]) wr.getValues()).length / 2;
		} else if (ScalarType.FILE_LINK.equals(scalarType)) {
			return ((String[]) wr.getValues()).length / 3;
		} else if (ScalarType.LONG.equals(scalarType)) {
			return ((long[]) wr.getValues()).length;
		} else if (ScalarType.SHORT.equals(scalarType)) {
			return ((short[]) wr.getValues()).length;
		} else if (ScalarType.STRING.equals(scalarType)) {
			return ((String[]) wr.getValues()).length;
		} else {
			throw new DataAccessException("Not supported ScalarType " + scalarType);
		}
	}

//	public static ChannelValuesList toMeasuredValuesList(DataMatrix dmMeaQuantity, DataMatrix dmLocalcolumn) {
//		Column idColumn = null;
//		Column nameColumn = null;
//		Column valuesColumn = null;
//		Column flagsColumn = null;
//
//		for (Column c : dmMeaQuantity.getColumnsList()) {
//			if ("Id".equals(c.getName())) {
//				idColumn = c;
//			} else if ("Name".equals(c.getName())) {
//				nameColumn = c;
//			}
//		}
//		for (Column c : dmLocalcolumn.getColumnsList()) {
//			if ("Values".equals(c.getName())) {
//				valuesColumn = c;
//			} else if ("Flags".equals(c.getName())) {
//				flagsColumn = c;
//			}
//		}
//
//		List<ChannelValues> list = new ArrayList<>();
//		for (int i = 0; i < valuesColumn.getStringArray().getValuesCount(); i++) {
//
//			String channelId = idColumn.getLonglongArray().getValues(i) + "";
//			ChannelValues v = new ChannelValues(channelId, nameColumn.getStringArray().getValues(i),
//					toScalarType(valuesColumn.getDataType()), toValues(valuesColumn.getUnknownArrays().getValues(i)),
//					Ints.toArray(flagsColumn.getLongArrays().getValues(i).getValuesList()));
//			list.add(v);
//		}
//
//		return new ChannelValuesList(list);
//	}
}
