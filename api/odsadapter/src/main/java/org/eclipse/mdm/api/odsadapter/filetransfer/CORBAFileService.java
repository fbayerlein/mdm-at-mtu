/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.filetransfer;

import java.io.IOException;
import java.io.InputStream;

import org.asam.ods.ElemId;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.odsadapter.ODSCorbaContext;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityType;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.eclipse.mdm.api.odsadapter.transaction.ODSTransaction;
import org.eclipse.mdm.api.odsadapter.utils.ODSConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CORBA file service implementation of the {@link FileService} interface.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public class CORBAFileService extends BaseFileService {

	// ======================================================================
	// Class variables
	// ======================================================================

	private static final Logger LOGGER = LoggerFactory.getLogger(CORBAFileService.class);

	// ======================================================================
	// Instance variables
	// ======================================================================

	private final CORBAFileServer fileServer;
	private final ODSModelManager modelManager;

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param context  Used for {@link Entity} to {@link ElemId} conversion.
	 * @param transfer The transfer type for up- and downloads.
	 */
	public CORBAFileService(ODSCorbaContext context, Transfer transfer) {
		this.modelManager = context.getODSModelManager();

		fileServer = new CORBAFileServer(context, transfer);
	}

	// ======================================================================
	// Protected methods
	// ======================================================================

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	public InputStream openRemoteStream(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		return fileServer.openStream(fileLink, toElemID(entity), (ODSTransaction) transaction);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Entity entity, FileLink fileLink, Transaction transaction) {
		if (!fileLink.isRemote()) {
			// nothing to do
			return;
		}

		try {
			fileServer.delete(fileLink, toElemID(entity), (ODSTransaction) transaction);
			LOGGER.debug("File '{}' sucessfully deleted.", fileLink.getRemotePath());
		} catch (IOException e) {
			LOGGER.warn("Failed to delete remote file.", e);
		}
	}

	@Override
	public void loadSize(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		if (fileLink.getSize() > -1) {
			// file size is already known
			return;
		} else if (fileLink.isRemote()) {
			fileLink.setFileSize(fileServer.loadSize(fileLink, toElemID(entity), (ODSTransaction) transaction));
		} else {
			throw new IllegalArgumentException("File link is neither in local nor remote state: " + fileLink);
		}
	}

	@Override
	protected void upload(Entity entity, FileLink fileLink, InputStream inputStream, Transaction transaction)
			throws IOException {
		fileServer.uploadStream(inputStream, fileLink, toElemID(entity), (ODSTransaction) transaction);

	}

	/**
	 * Creates an ODS entity identity {@link ElemId} object for given
	 * {@link Entity}.
	 *
	 * @param entity The {@code Entity}.
	 * @return The created {@code ElemId} is returned.
	 */
	protected ElemId toElemID(Entity entity) {
		return new ElemId(ODSConverter.toODSLong(((ODSEntityType) modelManager.getEntityType(entity)).getODSID()),
				ODSConverter.toODSID(entity.getID()));
	}

}
