/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.utils;

import java.net.URI;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;

import org.eclipse.mdm.api.base.query.ConnectionLostException;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.util.JsonFormat;

import ods.Ods.ErrorInfo;

public class OdsHttpRequests {

	static final Logger LOGGER = LoggerFactory.getLogger(OdsHttpRequests.class);

	public static Entity<Message> ods(Message entity) {
		return OdsProtobufMessageBodyProvider.protobuf(entity);
	}

	public static final MediaType ACCEPT = OdsProtobufMessageBodyProvider.MEDIA_TYPE;

	public static URI getLocation(WebTarget session, String path, Message entity) throws OdsException {

		Response response = session.path(path).request(ACCEPT).post(ods(entity));
		try {
			if (response.getStatus() == Status.CREATED.getStatusCode()) {
				URI uri = response.getLocation();

				LOGGER.trace("POST {} with body:\n{}\nreturned location: {}", session.path(path).getUri(),
						OdsHttpRequests.toString(entity), uri);
				if (uri == null) {
					throw new OdsException("POST " + session.path(path).getUri() + " with body: '"
							+ OdsHttpRequests.toString(entity) + "' returned no Location Header.");
				}
				return uri;
			} else if (response.getStatusInfo().getFamily() == Family.CLIENT_ERROR
					|| response.getStatusInfo().getFamily() == Family.SERVER_ERROR) {
				ErrorInfo error = response.readEntity(ErrorInfo.class);

				throw new OdsException(
						"Cannot create file-access for " + OdsHttpRequests.toString(entity) + ": " + error.getReason());
			} else {
				throw new OdsException("Cannot create file-access for " + OdsHttpRequests.toString(entity) + ".");
			}
		} finally {
			response.close();
		}
	}

	public static void request(WebTarget session, String path, Message entity) throws OdsException {
		try {
			session.path(path).request(ACCEPT).post(ods(entity), Void.class);
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace("POST {} with body:\n{}\nreturned:\n{}", session.path(path).getUri(),
						OdsHttpRequests.toString(entity));
			}
		} catch (WebApplicationException e) {
			LOGGER.trace("POST {} with body:\n{}\nreturned:\n{}", session.path(path).getUri(),
					OdsHttpRequests.toString(entity), ODSHttpConverter.readErrorInfo(e));

			OdsException ex = ODSHttpConverter.toOdsException(e);
			if (ex.getMessage().contains("Cannot find session for connection identifier")) {
				throw new ConnectionLostException(ex.getMessage(), ex);
			} else {
				throw ex;
			}
		}
	}

	public static <T extends Message> T request(WebTarget session, String path, Message entity, Class<T> responseType)
			throws OdsException {
		try {
			T response = session.path(path).request(ACCEPT).post(ods(entity), responseType);
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace("POST {} with body:\n{}\nreturned:\n{}", session.path(path).getUri(),
						OdsHttpRequests.toString(entity), OdsHttpRequests.toString(response));
			}
			return response;
		} catch (WebApplicationException e) {
			LOGGER.trace("POST {} with body:\n{}\nreturned:\n{}", session.path(path).getUri(),
					OdsHttpRequests.toString(entity), ODSHttpConverter.readErrorInfo(e));

			OdsException ex = ODSHttpConverter.toOdsException(e);
			if (ex.getMessage().contains("Cannot find session for connection identifier")) {
				throw new ConnectionLostException(ex.getMessage(), ex);
			} else {
				throw ex;
			}
		}
	}

	public static String toString(MessageOrBuilder entity) {
		if (entity == null) {
			return "";
		}
		try {

			return JsonFormat.printer().print(entity);
		} catch (InvalidProtocolBufferException e) {
			return entity.toString();
		}
	}

}
