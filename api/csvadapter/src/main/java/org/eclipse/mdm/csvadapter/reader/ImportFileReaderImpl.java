/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.mdm.csvadapter.CsvConfiguration;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import com.google.common.collect.Maps;

public class ImportFileReaderImpl implements ImportFileReader {
	private static final Logger LOG = LoggerFactory.getLogger(ImportFileReaderImpl.class);

	private static final String UTF8_BOM = "\uFEFF";

	private static final String FLAGS_PATTERN = "(.*)_FLAGS";

	private final Map<String, String> metaData = new HashMap<>();

	private final Map<String, List<String>> measuredData = new HashMap<>();
	private final Map<String, List<String>> flags = new HashMap<>();

	private Map<String, String> unitMapping = new HashMap<>();
	private Map<String, String> quantityMapping = new HashMap<>();

	private final File file;
	private final Map<String, String> connectionParameters;

	public ImportFileReaderImpl(File file, Map<String, String> connectionParameters) throws CSVAdapterException {
		this.file = file;
		this.connectionParameters = connectionParameters;
		File quantityMappingFile = CsvConfiguration.getQuantityMappingFile(connectionParameters);
		loadQuantityMapping(quantityMappingFile);
		readFile(this.file);
	}

	private void loadQuantityMapping(File quantityMappingFile) throws CSVAdapterException {
		Properties prop = new Properties();

		if (quantityMappingFile != null) {
			try (InputStream is = new FileInputStream(quantityMappingFile);
					InputStreamReader isr = new InputStreamReader(is, Charset.forName("UTF-8"))) {
				prop.load(isr);
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
				throw new CSVAdapterException(e.getLocalizedMessage(), e);
			}
		}

		LOG.debug("CSV QuantityMapping: {}", prop);

		quantityMapping.putAll(Maps.fromProperties(prop));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.csvadapter.reader.ImportFileReader#getMetaData()
	 */
	@Override
	public Map<String, String> getMetaData() {
		return metaData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.csvadapter.reader.ImportFileReader#getMeasuredData()
	 */
	@Override
	public Map<String, List<String>> getMeasuredData() {
		return measuredData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.csvadapter.reader.ImportFileReader#getFlags()
	 */
	@Override
	public Map<String, List<String>> getFlags() {
		return flags;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.csvadapter.reader.ImportFileReader#getUnitMapping()
	 */
	@Override
	public Map<String, String> getUnitMapping() {
		return unitMapping;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.csvadapter.reader.ImportFileReader#getQuantityMapping()
	 */
	@Override
	public Map<String, String> getQuantityMapping() {
		return quantityMapping;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.csvadapter.reader.ImportFileReader#getFile()
	 */
	@Override
	public File getFile() {
		return file;
	}

	private void readFile(File file) throws CSVAdapterException {
		List<String> measuredDataLines = seperateFile(file);

		if (measuredDataLines != null && !measuredDataLines.isEmpty()) {
			readCSVFile(writeTempCSVFile(measuredDataLines));
		}
	}

	private void readCSVFile(File csvFile) throws CSVAdapterException {
		CsvPreference csvPreference = CsvConfiguration.getCsvPreference(connectionParameters);

		try (ICsvMapReader mapReader = new CsvMapReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"),
				csvPreference)) {
			String[] headers = mapReader.getHeader(true);

			unitMapping = mapReader.read(headers);
			if (CsvConfiguration.getQuantitiesInHeader(connectionParameters)) {
				quantityMapping = mapReader.read(headers);
			}

			Map<String, String> csvRow = mapReader.read(headers);

			while (csvRow != null) {
				addData(csvRow);
				csvRow = mapReader.read(headers);
			}

		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new CSVAdapterException(e.getLocalizedMessage(), e);
		}

	}

	private void addData(Map<String, String> csvRow) {
		Iterator<String> meaQuanNameIterator = csvRow.keySet().iterator();

		while (meaQuanNameIterator.hasNext()) {
			String colName = meaQuanNameIterator.next();

			String meaQuantityNameOfFlag = getMeaQuantityName(colName);
			if (meaQuantityNameOfFlag == null) {
				List<String> data = measuredData.computeIfAbsent(colName, k -> new ArrayList<>());
				data.add(csvRow.get(colName));
			} else {
				List<String> data = flags.computeIfAbsent(meaQuantityNameOfFlag, k -> new ArrayList<>());
				data.add(csvRow.get(colName));
			}

		}

	}

	/**
	 * Check if the column is a flags colum, if yes return the name of the
	 * corresponding meaquantity, if no return null
	 * 
	 * @param flagsColumnName
	 * @return
	 */
	private String getMeaQuantityName(String flagsColumnName) {
		String returnVal = null;

		Pattern p = Pattern.compile(FLAGS_PATTERN);

		Matcher m = p.matcher(flagsColumnName);
		if (m.matches()) {
			returnVal = m.group(1);
		}

		return returnVal;
	}

	private List<String> seperateFile(File file) throws CSVAdapterException {

		List<String> measuredDataLines = new ArrayList<>();

		try (Stream<String> stream = Files.lines(file.toPath(), StandardCharsets.UTF_8)) {
			List<String> lines = stream.collect(Collectors.toList());
			int row = 0;
			boolean isMetaDataRow = true;

			for (String line : lines) {
				if (row == 0 && !line.trim().equals("[METADATA]") && !line.trim().equals(UTF8_BOM + "[METADATA]")) {
					throw new CSVAdapterException("Invalid File. First row has to be '[METADATA]'");
				} else if (row > 0 && isMetaDataRow && line.equalsIgnoreCase("[MEASUREDDATA]")) {
					isMetaDataRow = false;
				} else if (row > 0 && isMetaDataRow && !line.isEmpty()) {
					String[] keyValue = line.split("=");

					if (keyValue.length == 2) {
						metaData.put(keyValue[0].trim(), keyValue[1].trim());
					}
				} else if (!isMetaDataRow) {
					measuredDataLines.add(line);
				}
				row++;
			}

		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new CSVAdapterException(e.getLocalizedMessage(), e);
		}

		return measuredDataLines;
	}

	private File writeTempCSVFile(List<String> measuredDataLines) throws CSVAdapterException {

		try {
			File tmpFile = File.createTempFile("csvadapter_" + System.currentTimeMillis(), ".csv");
			tmpFile.deleteOnExit();

			try (Writer writer = new FileWriter(tmpFile)) {
				int i = 0;
				for (String line : measuredDataLines) {
					if (i != 0) {
						writer.write("\n");
					}
					writer.write(line);
					i++;
				}
			}

			return tmpFile;
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new CSVAdapterException(e.getLocalizedMessage(), e);
		}

	}
}
