/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityFactory;
import org.eclipse.mdm.api.odsadapter.query.ODSHttpModelManager;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.filetransfer.CSVFileService;
import org.eclipse.mdm.csvadapter.reader.ImportFileReader;

/**
 * Implements the {@link ApplicationContext} interface to return
 * {@link EntityFactory} and {@link EntityManager}
 * 
 * @author akn
 *
 */
public class CSVApplicationContext implements ApplicationContext {

	private final Map<String, String> parameters;
	protected final CSVModelLoader csvModelLoader;
	protected CSVEntityManager csvEntityManager;
	private final Optional<ModelManager> modelManager;

	private final String sourceName;
	private final Path workingDir;

	/**
	 * 
	 * @param <T>
	 * @param importFileReader
	 * @param tplTestStep
	 * @param quantities
	 * @param units
	 * @param quantityMappingFile
	 * @param parameters
	 * @param modelManager
	 * @param extSystemAttrList
	 * @throws CSVAdapterException
	 */
	public CSVApplicationContext(ImportFileReader importFileReader, TemplateTestStep tplTestStep,
			List<Quantity> quantities, List<Unit> units, Map<String, String> parameters, String fileName,
			Optional<ModelManager> modelManager, List<ExtSystemAttribute> extSystemAttrList)
			throws CSVAdapterException {

		this.sourceName = fileName;
		this.modelManager = modelManager;
		this.parameters = parameters;

		if (importFileReader == null) {
			this.workingDir = Paths.get(fileName);
			this.csvModelLoader = new CSVModelLoader(this, this.workingDir, modelManager.get());
		} else {
			this.workingDir = importFileReader.getFile().toPath().getParent();
			this.csvModelLoader = new CSVModelLoader(this, importFileReader, tplTestStep, quantities, units,
					modelManager.get(), extSystemAttrList);
		}

		this.csvEntityManager = new CSVEntityManager(csvModelLoader, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#getSourceName()
	 */
	@Override
	public String getSourceName() {
		return sourceName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#getParameters()
	 */
	public Map<String, String> getParameters() {
		return parameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#getAdapterType()
	 */
	public String getAdapterType() {
		return "CSVAdapter";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#close()
	 */
	public void close() throws ConnectionException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#getEntityManager()
	 */
	public Optional<EntityManager> getEntityManager() {
		return Optional.of(csvEntityManager);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#getModelManager()
	 */
	public Optional<ModelManager> getModelManager() {
		return modelManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#getFileService()
	 */
	@Override
	public Optional<FileService> getFileService(FileServiceType type) {
		switch (type) {
		// No distinction between AOFILE and EXTREF necessary:
		case AOFILE:
		case EXTREF:
			return Optional.of(new CSVFileService(workingDir));
		default:
			return Optional.empty();
		}
	}

	@Override
	public Optional<EntityFactory> getEntityFactory() {
		try {
			if (modelManager.get() instanceof ODSHttpModelManager) {
				return Optional
						.of(new ODSEntityFactory((ODSHttpModelManager) modelManager.get(), () -> Optional.empty()));
			} else {
				return Optional
						.of(new ODSEntityFactory((ODSCorbaModelManager) modelManager.get(), () -> Optional.empty()));
			}
		} catch (DataAccessException e) {
			throw new IllegalStateException("Unable to load instance of the logged in user.");
		}
	}
}
