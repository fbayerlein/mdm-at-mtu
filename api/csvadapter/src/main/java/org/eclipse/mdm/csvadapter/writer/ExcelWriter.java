/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.DateFormatConverter;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.csvadapter.CSVEntityManager;
import org.eclipse.mdm.csvadapter.ExcelApplicationContext;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

/**
 * Class for writing measurements to a Excel files.
 * 
 * @author jz
 */
public class ExcelWriter extends Writer {
	private static final Logger LOG = LoggerFactory.getLogger(ExcelWriter.class);

	private static final int ROW_ACCESS_WINDOW_SIZE = 100;

	private final ExcelApplicationContext context;
	private final CSVEntityManager entityManager;

	private final DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	private final Locale locale = Locale.getDefault();

	/**
	 * @param context
	 */
	public ExcelWriter(ExcelApplicationContext context) {
		this.context = context;
		this.entityManager = (CSVEntityManager) context.getEntityManager().get();
	}

	@Override
	protected DateTimeFormatter getFormatter() {
		return DateTimeFormatter.ISO_DATE_TIME;
	}

	/**
	 * Writes the data into an Excel file
	 * 
	 * @param testStep
	 * 
	 * @throws DataAccessException
	 */
	public Path writeExcel(TestStep testStep) throws DataAccessException {

		Path parentPath = new File(context.getParameters().get("excelFile")).getParentFile().toPath();
		String filename = sanitzeFilename(testStep.getName());
		Path excelFile = avoidNameClashes(parentPath, filename, ".xlsx");

		LOG.debug("Writing TestStep '{}' to Excel file '{}'", testStep.getName(), excelFile);
		Stopwatch exportTimer = Stopwatch.createStarted();

		SXSSFWorkbook wb = new SXSSFWorkbook(ROW_ACCESS_WINDOW_SIZE);
		try (FileOutputStream fos = new FileOutputStream(excelFile.toFile())) {

			CellStyle dateCellStyle = wb.createCellStyle();
			dateCellStyle.setDataFormat(wb.createDataFormat().getFormat(DateFormatConverter.convert(locale, df)));

			for (Measurement mea : entityManager.loadChildren(testStep, Measurement.class)) {
				List<Sheet> sheets = new ArrayList<>();
				sheets.add(wb.createSheet(sanitzeFilename(mea.getName())));

				int rowNo = writeHeader(sheets.get(0), mea, dateCellStyle);
				rowNo++;

				LOG.trace("Finished writing header data for TestStep '{}' in {}", testStep.getName(), exportTimer);

				for (ChannelGroup channelGroup : entityManager.loadChildren(mea, ChannelGroup.class)) {

					List<WriteRequest> writeRequests = sortChannels(
							entityManager.getWriteRequests().get(channelGroup.getID()).values());

					List<List<WriteRequest>> writeRequestPartitionedBySheet = Lists.partition(writeRequests,
							sheets.get(0).getWorkbook().getSpreadsheetVersion().getMaxColumns());

					int newRowNo = rowNo;
					for (int sheetNo = 0; sheetNo < writeRequestPartitionedBySheet.size(); sheetNo++) {
						if (sheets.size() == sheetNo) {
							// No sheet yet, create a new one
							Sheet sheet = wb.createSheet(mea.getName() + "_" + (sheetNo + 1));
							writeHeader(sheet, mea, dateCellStyle);
							sheets.add(sheet);
							LOG.trace("Finished writing header data for TestStep '{}' (Sheet {})in {}",
									testStep.getName(), sheetNo, exportTimer);
						}
						Sheet sheet = sheets.get(sheetNo);
						newRowNo = writeChannelGroup(sheet, channelGroup, writeRequestPartitionedBySheet.get(sheetNo),
								rowNo, dateCellStyle);

						LOG.trace("Finished writing {} channels to sheet '{}' after {}",
								writeRequestPartitionedBySheet.get(sheetNo).size(), sheets.get(sheetNo).getSheetName(),
								exportTimer);

					}
					rowNo = newRowNo;

					LOG.trace("Finished writing channel group with id '{}' after {}", channelGroup.getID(),
							exportTimer);
				}
			}

			wb.write(fos);
			wb.close();
		} catch (IOException e) {
			LOG.error("writeExcel(context, entitiesByClassType, valuesForChannel)", e);
		} finally {
			if (!wb.dispose()) {
				LOG.warn("Temporary files created during excel export could not be deleted!");
			}
		}
		LOG.debug("Finished writing TestStep '{}' to Excel file '{}' in {}", testStep.getName(), excelFile,
				exportTimer);

		return excelFile;
	}

	private int writeHeader(Sheet sheet, Measurement measurement, CellStyle dateCellStyle) {
		int rowNo = 0;

		TestStep testStep = entityManager.loadParent(measurement, TestStep.class).get();
		Test test = entityManager.loadParent(testStep, Test.class).get();
		Pool pool = entityManager.loadParent(test, Pool.class).get();
		Project project = entityManager.loadParent(pool, Project.class).get();

		Row currentRow;

		currentRow = sheet.createRow(rowNo++);
		currentRow.createCell(0).setCellValue(Constants.IMPORTFILE_ATTRIBUTE_PROJECTNAME);
		currentRow.createCell(1).setCellValue(project.getName());

		currentRow = sheet.createRow(rowNo++);
		currentRow.createCell(0).setCellValue(Constants.IMPORTFILE_ATTRIBUTE_STRUCTURELEVELNAME);
		currentRow.createCell(1).setCellValue(pool.getName());

		currentRow = sheet.createRow(rowNo++);
		currentRow.createCell(0).setCellValue(Constants.IMPORTFILE_ATTRIBUTE_TESTNAME);
		currentRow.createCell(1).setCellValue(test.getName());

		currentRow = sheet.createRow(rowNo++);
		currentRow.createCell(0).setCellValue(Constants.IMPORTFILE_ATTRIBUTE_TESTSTEPNAME);
		currentRow.createCell(1).setCellValue(testStep.getName());

		currentRow = sheet.createRow(rowNo++);
		currentRow.createCell(0).setCellValue(Constants.IMPORTFILE_ATTRIBUTE_MEASUREMENTNAME);
		currentRow.createCell(1).setCellValue(measurement.getName());

		for (Map.Entry<ContextType, ContextRoot> e : entityManager.loadContexts(measurement).entrySet()) {
			ContextType contextType = e.getKey();
			ContextRoot contextRoot = e.getValue();
			if (contextRoot == null) {
				continue;
			}

			for (ContextComponent cc : contextRoot.getContextComponents()) {
				for (Entry<String, Value> v : cc.getValues().entrySet()) {
					if (isIgnoredContextAttribute(v.getKey())) {
						continue;
					}
					currentRow = sheet.createRow(rowNo++);
					currentRow.createCell(0).setCellValue(contextType + "." + cc.getName() + "." + v.getKey());
					if (v.getValue().extract() instanceof Instant) {
						Cell cell = currentRow.createCell(1);
						cell.setCellValue(
								LocalDateTime.ofInstant(v.getValue().extract(ValueType.DATE), ZoneOffset.UTC));
						cell.setCellStyle(dateCellStyle);
					} else {
						currentRow.createCell(1).setCellValue(serialize(v.getValue().extract()));
					}

				}
			}
		}
		return rowNo;
	}

	private int writeChannelGroup(Sheet sheet, ChannelGroup channelGroup, List<WriteRequest> writeRequests, int rowNo,
			CellStyle dateCellStyle) {

		Row channelGroupNameRow = sheet.createRow(rowNo++);
		channelGroupNameRow.createCell(0).setCellValue("CHANNELGROUP.Name");
		channelGroupNameRow.createCell(1).setCellValue(channelGroup.getName());

		rowNo = writeChannelNameRow(sheet, writeRequests, rowNo);

		rowNo = writeUnitNameRow(sheet, writeRequests, rowNo);

		int numberOfValues = channelGroup.getNumberOfValues();
		for (int valueIndex = 0; valueIndex < numberOfValues; valueIndex++) {
			rowNo = writeValueRow(sheet, writeRequests, rowNo, valueIndex, dateCellStyle);
			if (valueIndex % 100_000 == 0) {
				LOG.trace("Written {} rows for channel group with id '{}'", valueIndex, channelGroup.getID());
			}
		}

		return rowNo + 1;
	}

	private int writeChannelNameRow(Sheet sheet, List<WriteRequest> writeRequests, int startRow) {
		Row channelNameRow = sheet.createRow(startRow);
		for (int colNo = 0; colNo < writeRequests.size(); colNo++) {
			channelNameRow.createCell(colNo).setCellValue(writeRequests.get(colNo).getChannel().getName());
		}
		return startRow + 1;
	}

	private int writeUnitNameRow(Sheet sheet, List<WriteRequest> writeRequests, int startRow) {
		Row unitNameRow = sheet.createRow(startRow);
		for (int colNo = 0; colNo < writeRequests.size(); colNo++) {
			unitNameRow.createCell(colNo).setCellValue(writeRequests.get(colNo).getChannel().getUnit().getName());
		}
		return startRow + 1;
	}

	private int writeValueRow(Sheet sheet, List<WriteRequest> writeRequests, int startRow, int valueIndex,
			CellStyle dateCellStyle) {

		Row valueRow = sheet.createRow(startRow);

		for (int colNo = 0; colNo < writeRequests.size(); colNo++) {
			WriteRequest writeRequest = writeRequests.get(colNo);

			if (writeRequest.getValues() instanceof float[]) {
				valueRow.createCell(colNo).setCellValue(((float[]) writeRequest.getValues())[valueIndex]);
			} else if (writeRequest.getValues() instanceof int[]) {
				valueRow.createCell(colNo).setCellValue(((int[]) writeRequest.getValues())[valueIndex]);
			} else if (writeRequest.getValues() instanceof long[]) {
				valueRow.createCell(colNo).setCellValue(((long[]) writeRequest.getValues())[valueIndex]);
			} else if (writeRequest.getValues() instanceof double[]) {
				valueRow.createCell(colNo).setCellValue(((double[]) writeRequest.getValues())[valueIndex]);
			} else if (writeRequest.getValues() instanceof short[]) {
				valueRow.createCell(colNo).setCellValue(((short[]) writeRequest.getValues())[valueIndex]);
			} else if (writeRequest.getValues() instanceof byte[]) {
				valueRow.createCell(colNo).setCellValue(((byte[]) writeRequest.getValues())[valueIndex]);
			} else if (writeRequest.getValues() instanceof boolean[]) {
				valueRow.createCell(colNo).setCellValue(((boolean[]) writeRequest.getValues())[valueIndex]);
			} else if (writeRequest.getValues() instanceof Instant[]) {
				Cell cell = valueRow.createCell(colNo);
				cell.setCellValue(
						LocalDateTime.ofInstant(((Instant[]) writeRequest.getValues())[valueIndex], ZoneOffset.UTC));
				cell.setCellStyle(dateCellStyle);
			} else if (writeRequest.getValues() instanceof String[]) {
				valueRow.createCell(colNo).setCellValue(((String[]) writeRequest.getValues())[valueIndex]);
			} else {
				throw new CSVAdapterException("ScalarType " + writeRequest.getRawScalarType() + " (Value: "
						+ writeRequest.getValues() + ") not supported!");
			}
		}
		return startRow + 1;
	}
}
