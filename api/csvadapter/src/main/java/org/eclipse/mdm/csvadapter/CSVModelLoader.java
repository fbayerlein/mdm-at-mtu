/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter;

import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.mdm.api.base.adapter.EntityStore;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextDescribable;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.FilesAttachable;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.model.VersionState;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.api.dflt.model.MDMAttribute;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.reader.ImportFileReader;
import org.eclipse.mdm.csvadapter.util.CSVAdapterUtil;
import org.eclipse.mdm.csvadapter.util.Constants;
import org.eclipse.mdm.csvadapter.util.MDMModelHelper;

import com.google.common.base.Strings;

/**
 * 
 * @author akn
 *
 */
public class CSVModelLoader {

	private static final String NOUNIT = "-";

	private static final String CHANNEL_MEASUREMENTPOINTS = "MeasurementPoints";

	private Map<Class<?>, HashMap<String, Entity>> modelCache = new HashMap<>();
	private Map<Entity, Entity> parentCache = new HashMap<>();
	private Map<Entity, Map<Class<?>, List<Entity>>> childCache = new HashMap<>();

	private final DateTimeFormatter formatter;
	private final CSVEntityFactory entityFactory;
	private final Map<String, String> quantityMapping;
	private final boolean checkPhysicalDimension;
	private final TemplateTestStep tplTestStep;
	private final Map<String, Quantity> quantityCache = new HashMap<>();
	private final Map<String, Unit> unitCache = new HashMap<>();
	private final MDMModelHelper mdmModelHelper;
	private final Map<String, String> metaData;
	private Map<String, List<String>> measuredData;
	private final Map<String, List<String>> flags;
	private final Map<String, String> unitMapping;
	private final String fileName;
	private final Map<String, Map<ContextType, ContextRoot>> contextRootCache = new HashMap<>();

	private final Map<String, ExtSystemAttribute> extSystemAttrCache = new HashMap<>();

	private final Map<ContextType, HashMap<String, HashMap<String, String>>> contextValues = new HashMap<>();

	private final Path workingDir;

	private final Map<String, Map<String, WriteRequest>> writeRequests = new HashMap<>();

	/**
	 * 
	 * @param context
	 * @param importFileReader
	 * @param tplTestStep
	 * @param quantities
	 * @param units
	 * @param modelManager
	 * @param extSystemAttrList
	 * @throws CSVAdapterException
	 */
	public CSVModelLoader(CSVApplicationContext context, ImportFileReader importFileReader,
			TemplateTestStep tplTestStep, List<Quantity> quantities, List<Unit> units, ModelManager modelManager,
			List<ExtSystemAttribute> extSystemAttrList) throws CSVAdapterException {
		this.formatter = CsvConfiguration.getDateTimeFormat(context.getParameters());
		this.workingDir = importFileReader.getFile().toPath().getParent();
		this.metaData = importFileReader.getMetaData();
		this.unitMapping = importFileReader.getUnitMapping();
		this.measuredData = importFileReader.getMeasuredData();
		this.checkPhysicalDimension = CsvConfiguration.getCheckPhyiscalDimension(context.getParameters());

		boolean addMeasurementPoints = CsvConfiguration.getAddMeasurementPoints(context.getParameters());

		if (metaData.get(Constants.IMPORTFILE_ATTRIBUTE_XAXIS) == null && addMeasurementPoints
				&& this.measuredData.get(CHANNEL_MEASUREMENTPOINTS) == null && measuredData != null
				&& !measuredData.isEmpty()) {
			this.measuredData = addMeasuredDataOfGeneratedXAxis(this.measuredData);
		}

		this.flags = importFileReader.getFlags();
		this.fileName = importFileReader.getFile().getName();
		this.tplTestStep = tplTestStep;

		if (extSystemAttrList != null) {
			initializeExtSystemCache(extSystemAttrList);
		}

		this.entityFactory = new CSVEntityFactory(modelManager);

		initializeQuantityMap(quantities);
		initializeUnitMap(units);

		this.mdmModelHelper = new MDMModelHelper(tplTestStep);
		this.quantityMapping = importFileReader.getQuantityMapping();
		initializeContextValues();
		loadModel();
	}

	/**
	 * @param context
	 * @param workingDir
	 * @param modelManager
	 */
	public CSVModelLoader(CSVApplicationContext context, Path workingDir, ModelManager modelManager) {
		this.formatter = CsvConfiguration.getDateTimeFormat(context.getParameters());
		this.workingDir = workingDir;
		this.metaData = new HashMap<>();
		this.unitMapping = new HashMap<>();
		this.measuredData = new HashMap<>();
		this.checkPhysicalDimension = CsvConfiguration.getCheckPhyiscalDimension(context.getParameters());

		if (metaData.get(Constants.IMPORTFILE_ATTRIBUTE_XAXIS) == null
				&& this.measuredData.get(CHANNEL_MEASUREMENTPOINTS) == null && measuredData != null
				&& !measuredData.isEmpty()) {
			this.measuredData = addMeasuredDataOfGeneratedXAxis(this.measuredData);
		}

		this.flags = null;
		this.fileName = null;
		this.tplTestStep = null;

		this.entityFactory = new CSVEntityFactory(modelManager);

		this.mdmModelHelper = new MDMModelHelper(tplTestStep);
		quantityMapping = new HashMap<>();
		initializeContextValues();
	}

	private void initializeExtSystemCache(List<ExtSystemAttribute> extSystemAttrList) {
		for (ExtSystemAttribute extAttr : extSystemAttrList) {
			extSystemAttrCache.put(extAttr.getName(), extAttr);
		}
	}

	/**
	 * adding the channel measurement points to the measured datas of the file
	 * 
	 * @param measuredData
	 * @return
	 */
	private Map<String, List<String>> addMeasuredDataOfGeneratedXAxis(Map<String, List<String>> measuredData) {
		Map<String, List<String>> returnVal = measuredData;

		int size = measuredData.values().iterator().next().size();

		List<String> measuredPointsValue = new ArrayList<>();

		for (int i = 1; i <= size; i++) {
			measuredPointsValue.add(String.valueOf(i));
		}

		returnVal.put(CHANNEL_MEASUREMENTPOINTS, measuredPointsValue);
		unitMapping.put(CHANNEL_MEASUREMENTPOINTS, NOUNIT);

		return returnVal;

	}

	private void initializeUnitMap(List<Unit> unitsList) {
		for (Unit unit : unitsList) {
			unitCache.put(unit.getName(), unit);
		}
	}

	private void initializeQuantityMap(List<Quantity> quantitiesList) {
		for (Quantity quantity : quantitiesList) {
			String name = quantity.getName();
			if (isNewerValidQuantity(quantity, quantityCache.get(name))) {
				quantityCache.put(name, quantity);
			}
		}
	}

	private boolean isNewerValidQuantity(Quantity quantityNew, Quantity quantityExisting) {
		boolean isNewerValidQuantity = false;

		VersionState validFlag = quantityNew.getValue("ValidFlag").extract();

		if (VersionState.VALID.equals(validFlag)) {
			int versionExisting = -1;

			if (quantityExisting != null) {
				versionExisting = getQuantityVersion(quantityExisting);
			}

			if (getQuantityVersion(quantityNew) > versionExisting) {
				isNewerValidQuantity = true;
			}
		}

		return isNewerValidQuantity;
	}

	private int getQuantityVersion(Quantity quantityExisting) {
		String versionStr = quantityExisting.getValue("Version").extract();
		return Integer.valueOf(versionStr);
	}

	private void initializeContextValues() {

		Iterator<String> keyIterator = metaData.keySet().iterator();

		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			List<String> keySeperated = Stream.of(key.split("\\.")).map(elem -> new String(elem))
					.collect(Collectors.toList());

			if (extSystemAttrCache.get(key) != null) {
				for (MDMAttribute mdmAttribute : extSystemAttrCache.get(key).getAttributes()) {
					addContextValue(ContextType.valueOf(mdmAttribute.getComponentType().toUpperCase()),
							mdmAttribute.getComponentName(), mdmAttribute.getAttributeName(), metaData.get(key));
				}
			} else if (keySeperated.size() == 3) {
				ContextType contextType = ContextType.valueOf(keySeperated.get(0));
				String compName = keySeperated.get(1);
				String attrName = keySeperated.get(2);
				String attrValue = metaData.get(key);

				addContextValue(contextType, compName, attrName, attrValue);
			}
		}
	}

	private void addContextValue(ContextType contextType, String compName, String attrName, String attrValue) {
		if (contextType != null && !Strings.isNullOrEmpty(attrValue)) {
			HashMap<String, HashMap<String, String>> contextValuesOfContext = contextValues.computeIfAbsent(contextType,
					k -> new HashMap<>());
			HashMap<String, String> attrValuePairs = contextValuesOfContext.computeIfAbsent(compName,
					k -> new HashMap<>());
			attrValuePairs.computeIfAbsent(attrName, k -> attrValue);
		}
	}

	private void loadModel() throws CSVAdapterException {

		Project project = entityFactory
				.createProject(getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_PROJECTNAME, true));
		project.setMimeType(new MimeType(Constants.MIMETYPE_PROJECT));
		setAttributeValuesFromMetaData("PROJECT", project);
		putEntityToCache(project, null);

		Pool pool = entityFactory.createPool(getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_STRUCTURELEVELNAME, true),
				project);
		pool.setMimeType(new MimeType(Constants.MIMETYPE_STRUCTURE_LEVEL));
		setAttributeValuesFromMetaData("STRUCTURELEVEL", pool);
		putEntityToCache(pool, project);
		putChildsToCache(project, Collections.singletonList(pool), Pool.class);

		Test test = entityFactory.createTest(getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_TESTNAME, true), pool);
		test.setMimeType(new MimeType(Constants.MIMETYPE_TEST));
		test.getValue(Constants.TESTATTR_TEMPLATE).set(metaData.get(Constants.IMPORTFILE_ATTRIBUTE_TESTTEMPLATENAME));
		addFileLinks(test,
				CSVAdapterUtil.getFileReferences(getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_TESTMDMLINKS, false)));
		setAttributeValuesFromMetaData("TEST", test);

		putEntityToCache(test, pool);
		putChildsToCache(pool, Collections.singletonList(test), Test.class);

		TestStep testStep = entityFactory
				.createTestStep(getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_TESTSTEPNAME, true), test);
		testStep.setMimeType(new MimeType(Constants.MIMETYPE_TEST_STEP));
		testStep.getValue(Constants.TESTATTR_TEMPLATE)
				.set(metaData.get(Constants.IMPORTFILE_ATTRIBUTE_TESTSTEPTEMPLATENAME));
		addFileLinks(testStep, CSVAdapterUtil
				.getFileReferences(getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_TESTSTEPMDMLINKS, false)));
		setAttributeValuesFromMetaData("TESTSTEP", testStep);

		putEntityToCache(testStep, test);
		putChildsToCache(test, Collections.singletonList(testStep), TestStep.class);

		Measurement meaResult = loadMeasurement(testStep);
		putChildsToCache(testStep, Collections.singletonList(meaResult), Measurement.class);

		if (this.measuredData != null && !this.measuredData.isEmpty()) {
			ChannelGroup channelGroup = loadChannelGroup(meaResult);
			putChildsToCache(meaResult, Collections.singletonList(channelGroup), ChannelGroup.class);

			List<Entity> channels = loadChannels(meaResult);
			putChildsToCache(meaResult, channels, Channel.class);
			putChildsToCache(channelGroup, channels, Channel.class);
		}

	}

	private void addFileLinks(FilesAttachable<?> entity, List<String> fileReferences) throws CSVAdapterException {

		for (String fileReference : fileReferences) {
			entity.addFileLink(CSVAdapterUtil.getFileLink(workingDir, fileReference));
		}

	}

	private List<Entity> loadChannels(Measurement meaResult) throws CSVAdapterException {
		List<Entity> returnList = new ArrayList<>();

		Iterator<String> channelNameIterator = measuredData.keySet().iterator();

		while (channelNameIterator.hasNext()) {
			String channelName = channelNameIterator.next();
			Quantity quantity = getMappedQuantity(channelName);
			Unit unit = getMappedUnit(channelName, quantity);

			if (checkPhysicalDimension && !quantity.getDefaultUnit().getPhysicalDimension().getName()
					.equals(unit.getPhysicalDimension().getName())) {
				throw new CSVAdapterException(String.format(
						"Unit '%s' of Channel '%s' does not have the same PysicalDimension '%s' as the default Unit '%s' of the targeted Quantity '%s'.",
						unit.getName(), channelName, unit.getPhysicalDimension().getName(),
						quantity.getDefaultUnit().getName(), quantity.getName()));
			}

			Channel channel = entityFactory.createChannel(channelName, meaResult, quantity, unit);
			channel.setMimeType(new MimeType(Constants.MIMETYPE_MEASUREMENT_QUANTITY));
			returnList.add(channel);
			putEntityToCache(channel, meaResult);
		}

		return returnList;
	}

	private ChannelGroup loadChannelGroup(Measurement meaResult) {
		int size = measuredData.values().iterator().next().size();

		String channelGroupName = getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_CHANNELGROUPNAME, false);
		if (Strings.isNullOrEmpty(channelGroupName)) {
			// Fallback to Measurement name
			channelGroupName = meaResult.getName();
		}
		ChannelGroup channelGroup = entityFactory.createChannelGroup(channelGroupName, size, meaResult);
		channelGroup.setMimeType(new MimeType(Constants.MIMETYPE_SUBMATRIX));
		putEntityToCache(channelGroup, meaResult);
		return channelGroup;
	}

	private Unit getMappedUnit(String channelName, Quantity quantity) throws CSVAdapterException {
		String unitName = unitMapping.get(channelName);

		Unit unit = null;
		if (Strings.isNullOrEmpty(unitName)) {
			unit = quantity.getDefaultUnit();
		} else {
			unit = unitCache.get(unitName);
		}

		if (unit == null) {
			throw new CSVAdapterException(String.format("Unit with name '%s' does not exist!", unitName));
		}

		return unit;
	}

	private Quantity getMappedQuantity(String channelName) throws CSVAdapterException {
		String quantityName = quantityMapping.get(channelName);

		if (Strings.isNullOrEmpty(quantityName)) {
			quantityName = channelName;
		}

		Quantity quantity = quantityCache.get(quantityName);

		if (quantity == null) {
			throw new CSVAdapterException(String.format("Quantity with name '%s' does not exist!", quantityName));
		}

		return quantity;
	}

	private Measurement loadMeasurement(TestStep testStep) throws CSVAdapterException {

		ContextRoot contextRootUUT = createContextRoot(ContextType.UNITUNDERTEST);
		ContextRoot contextRootTS = createContextRoot(ContextType.TESTSEQUENCE);
		ContextRoot contextRootTE = createContextRoot(ContextType.TESTEQUIPMENT);

		List<ContextRoot> contextRoots = Stream.of(contextRootUUT, contextRootTS, contextRootTE)
				.filter(Objects::nonNull).collect(Collectors.toList());

		String meaResultName = getMetaDataValue(Constants.IMPORTFILE_ATTRIBUTE_MEASUREMENTNAME, false);

		if (Strings.isNullOrEmpty(meaResultName)) {
			meaResultName = fileName;
		}

		Measurement meaResult = entityFactory.createMeasurement(meaResultName, testStep, contextRoots);
		meaResult.setMimeType(new MimeType(Constants.MIMETYPE_MEASUREMENT));
		setAttributeValuesFromMetaData("MEASUREMENT", meaResult);
		putEntityToCache(meaResult, testStep);

		for (ContextRoot cr : contextRoots) {
			Map<ContextType, ContextRoot> map = contextRootCache.computeIfAbsent(cr.getID(), m -> new HashMap<>());
			map.put(cr.getContextType(), cr);
			setContextComponentsMimeTypes(cr);
		}

		return meaResult;
	}

	private void setAttributeValuesFromMetaData(String metaDataKeyPrefix, BaseEntity baseEntity) {
		for (String keyName : metaData.keySet().stream().filter(s -> s.startsWith(metaDataKeyPrefix + "."))
				.collect(Collectors.toList())) {
			int idxAttrName = keyName.indexOf('.') + 1;
			String attrName = (idxAttrName < keyName.length() ? keyName.substring(idxAttrName).trim() : "");
			if (attrName.length() > 0 && !"Name".equals(attrName) && !"MimeType".equals(attrName)) {
				Value val = baseEntity.getValue(attrName, true);
				if (val != null) {
					Object obj = CSVAdapterUtil.getValueFromString(workingDir, val.getValueType(),
							getMetaDataValue(keyName, false), formatter, null /* TODO */);
					val.set(obj);
				}
			}
		}
	}

	private void setContextComponentsMimeTypes(ContextRoot contextRoot) throws CSVAdapterException {
		for (ContextComponent comp : contextRoot.getContextComponents()) {
			comp.setMimeType(mdmModelHelper.getContextCompMimeType(comp));
		}
	}

	private ContextRoot createContextRoot(ContextType contextType) throws CSVAdapterException {
		ContextRoot contextRoot = null;

		Optional<TemplateRoot> templateRoot = tplTestStep.getTemplateRoot(contextType);
		if (templateRoot.isPresent()) {

			HashMap<String, HashMap<String, String>> contextValuesOfContext = contextValues.get(contextType);

			if (contextValuesOfContext != null) {

				Iterator<String> compNameIterator = contextValuesOfContext.keySet().iterator();
				Map<String, List<Value>> compValueMap = new HashMap<>();

				while (compNameIterator.hasNext()) {
					String compName = compNameIterator.next();

					List<Value> values = compValueMap.computeIfAbsent(compName, k -> new ArrayList<>());

					HashMap<String, String> attrValuePairs = contextValuesOfContext.get(compName);

					Iterator<String> attrNameIterator = attrValuePairs.keySet().iterator();

					while (attrNameIterator.hasNext()) {
						String attrName = attrNameIterator.next();
						ValueType<?> valueType = mdmModelHelper.getValueType(contextType, compName, attrName);
						if (valueType != null) {

							if (valueType.isEnumerationType()) {
								Enumeration<?> enumObj = mdmModelHelper.getEnumObj(contextType, compName, attrName);
								Object value = CSVAdapterUtil.getValueFromString(workingDir, valueType,
										attrValuePairs.get(attrName), formatter, enumObj);
								Value v = valueType.create(enumObj, attrName);
								v.set(value);
								values.add(v);
							} else {
								Object value = CSVAdapterUtil.getValueFromString(workingDir, valueType,
										attrValuePairs.get(attrName), formatter, null);
								values.add(valueType.create(attrName, value));
							}
						}

					}
				}
				contextRoot = entityFactory.createContextRoot(templateRoot.get(), compValueMap);
			}

		}
		return contextRoot;
	}

	public void putChildsToCache(Entity entity, List<Entity> childs, Class<?> entityClass) {
		if (!childs.isEmpty()) {
			Map<Class<?>, List<Entity>> childMap = childCache.computeIfAbsent(entity, k -> new HashMap<>());
			childMap.computeIfAbsent(entityClass, k -> childs);
		}
	}

	public void putEntityToCache(Entity entity, Entity parent) {
		HashMap<String, Entity> instanceCache = modelCache.computeIfAbsent(entity.getClass(), k -> new HashMap<>());
		instanceCache.put(entity.getID(), entity);
		parentCache.put(entity, parent);

		Map<Class<?>, List<Entity>> childMap = childCache.computeIfAbsent(parent, k -> new HashMap<>());
		List<Entity> children = childMap.computeIfAbsent(entity.getClass(), k -> new ArrayList<>());
		if (!children.contains(entity)) {
			children.add(entity);
		}
	}

	public void putEntityToCache(Entity entity) {
		EntityStore store = CSVEntityFactory.getPermanentStore((BaseEntity) entity);
		if (entity instanceof Project) {
			putEntityToCache(entity, null);
		} else if (entity instanceof Pool) {
			putEntityToCache(entity, store.get(Project.class));
		} else if (entity instanceof Test) {
			putEntityToCache(entity, store.get(Pool.class));
		} else if (entity instanceof TestStep) {
			putEntityToCache(entity, store.get(Test.class));
		} else if (entity instanceof Measurement) {
			putEntityToCache(entity, store.get(TestStep.class));
		} else if (entity instanceof ChannelGroup) {
			putEntityToCache(entity, store.get(Measurement.class));
		} else if (entity instanceof Channel) {
			putEntityToCache(entity, store.get(ChannelGroup.class));
		} else if (entity instanceof ContextRoot) {
			putEntityToCache(entity, null);
			ContextRoot contextRoot = (ContextRoot) entity;
			Map<ContextType, ContextRoot> m = contextRootCache.computeIfAbsent(entity.getID(), id -> new HashMap<>());
			m.put(contextRoot.getContextType(), contextRoot);
		} else if (entity instanceof ContextComponent) {
			putEntityToCache(entity, store.get(ContextRoot.class));
		} else {
			System.out.println("No Parent for " + entity);
		}
	}

	private String getMetaDataValue(String attrName, boolean mandatory) throws CSVAdapterException {
		String value = metaData.get(attrName);

		if (mandatory && Strings.isNullOrEmpty(value)) {
			throw new CSVAdapterException(String.format("Mandatory MetaData attribute '%s' is missing!", attrName));
		}

		return value;
	}

	/**
	 * 
	 * @param entityClass
	 * @param instanceIDs
	 * @return
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity> List<T> getEntities(Class<?> entityClass, Collection<String> instanceIDs) {
		List<T> returnList = new ArrayList<>();
		HashMap<String, Entity> entityMap = modelCache.get(entityClass);

		if (entityMap != null) {
			returnList = (List<T>) entityMap.values().stream().filter(o -> instanceIDs.contains(o.getID()))
					.collect(Collectors.toList());
		}

		return returnList;
	}

	public <T extends Entity> List<T> getEntities(Class<?> entityClass, String pattern) {
		return getEntitiesByName(entityClass, Arrays.asList(pattern));
	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> List<T> getEntitiesByName(Class<?> entityClass, Collection<String> patterns) {
		List<T> returnList = new ArrayList<>();
		HashMap<String, Entity> entityMap = modelCache.get(entityClass);

		if (entityMap != null) {
			returnList = (List<T>) entityMap.values().stream().filter(o -> {
				for (String pattern : patterns) {
					String aPattern = pattern;
					if ("*".equals(pattern)) {
						aPattern = ".*";
					}
					if (Pattern.matches(aPattern, o.getName())) {
						return true;
					}
				}
				return false;
			}).collect(Collectors.toList());
		}

		return returnList;
	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> Optional<T> getParent(Entity child) {
		Optional<Entity> returnVal = Optional.empty();

		if (parentCache.get(child) != null) {
			returnVal = Optional.of(parentCache.get(child));
		}

		return (Optional<T>) returnVal;
	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> List<T> getChildren(Entity entity, Class<T> entityClass, String pattern) {
		List<T> returnList = new ArrayList<>();
		Map<Class<?>, List<Entity>> map = childCache.get(entity);

		if (map != null) {
			List<Entity> entityList = map.get(entityClass);

			if (entityList != null) {
				returnList = (List<T>) entityList.stream().filter(o -> {
					String aPattern = null;
					if ("*".equals(pattern)) {
						aPattern = ".*";
					}
					return Pattern.matches(aPattern, o.getName());
				}).collect(Collectors.toList());
			}
		}

		return returnList;
	}

	public List<MeasuredValues> getMeasuredValues(ChannelGroup channelGroup, Map<Channel, Unit> channels)
			throws CSVAdapterException {
		List<MeasuredValues> returnList = new ArrayList<>();
		Iterator<Channel> channelIterator = channels.keySet().iterator();

		while (channelIterator.hasNext()) {
			returnList.add(getMeasuredValues(channelIterator.next()));
		}

		for (Channel channel : getChildren(channelGroup, Channel.class, "*")) {
			returnList.add(getMeasuredValues(channel));
		}

		return returnList;
	}

	private MeasuredValues getMeasuredValues(Channel channel) throws CSVAdapterException {
		MeasuredValues returnVal = null;

		String channelName = channel.getName();

		List<String> csvMeasuredValues = measuredData.get(channelName);

		if (csvMeasuredValues != null) {
			ScalarType scalarType = channel.getScalarType();
			Object values = getMeasuredValues(csvMeasuredValues, scalarType);
			AxisType axistype = AxisType.Y_AXIS;
			boolean independent = false;

			String xChannelName = metaData.getOrDefault(Constants.IMPORTFILE_ATTRIBUTE_XAXIS,
					CHANNEL_MEASUREMENTPOINTS);

			if (xChannelName.equals(channelName)) {
				axistype = AxisType.X_AXIS;
				independent = true;
			}

			returnVal = scalarType.createMeasuredValues(channelName, channel.getUnit().getName(),
					SequenceRepresentation.EXPLICIT, new double[0], independent, axistype, values,
					getFlags(channelName, csvMeasuredValues.size()));
		}

		return returnVal;
	}

	private short[] getFlags(String channelName, int size) {

		short[] returnVal = null;

		List<String> flagsFromFile = flags.get(channelName);

		if (flagsFromFile == null || flagsFromFile.isEmpty()) {
			returnVal = getDefaultFlags(size);
		} else {
			returnVal = getFlagsFromFile(flagsFromFile, size);
		}

		return returnVal;
	}

	private short[] getFlagsFromFile(List<String> flagsFromFile, int size) {
		short[] returnVal = new short[size];
		for (int i = 0; i < size; i++) {
			short flag = 0;

			if (i < flagsFromFile.size()) {
				String stringVal = flagsFromFile.get(i);
				try {
					flag = Short.valueOf(stringVal);
				} catch (NumberFormatException exc) {
					flag = (short) (Boolean.valueOf(stringVal) ? 15 : 0);
				}
			}

			returnVal[i] = flag;
		}
		return returnVal;
	}

	private short[] getDefaultFlags(int size) {
		short[] returnVal = new short[size];
		for (int i = 0; i < size; i++) {
			returnVal[i] = (short) 15;
		}
		return returnVal;
	}

	private Object getMeasuredValues(List<String> csvMeasuredValues, ScalarType scalarType) throws CSVAdapterException {
		Object returnVal = null;

		if (scalarType.isBoolean()) {
			returnVal = mdmModelHelper.createBoolenValues(csvMeasuredValues);
		} else if (scalarType.isByte()) {
			returnVal = mdmModelHelper.createByteValues(csvMeasuredValues);
		} else if (scalarType.isDate()) {
			returnVal = mdmModelHelper.createDateValues(csvMeasuredValues, formatter);
		} else if (scalarType.isDouble()) {
			returnVal = mdmModelHelper.createDoubleValues(csvMeasuredValues);
		} else if (scalarType.isFloat()) {
			returnVal = mdmModelHelper.createFloatValues(csvMeasuredValues);
		} else if (scalarType.isInteger()) {
			returnVal = mdmModelHelper.createIntegerValues(csvMeasuredValues);
		} else if (scalarType.isLong()) {
			returnVal = mdmModelHelper.createLongValues(csvMeasuredValues);
		} else if (scalarType.isShort()) {
			returnVal = mdmModelHelper.createShortValues(csvMeasuredValues);
		} else if (scalarType.isString()) {
			returnVal = mdmModelHelper.createStringValues(csvMeasuredValues);
		} else {
			throw new CSVAdapterException("Unsupported DataType.");
		}

		return returnVal;
	}

	/**
	 * @return the contextRootCache
	 */
	public Map<ContextType, ContextRoot> getContextRootCache(ContextDescribable contextDescribable) {
		Map<ContextType, ContextRoot> result = new HashMap<>();

		EntityStore store = CSVEntityFactory.getMutableStore((BaseEntity) contextDescribable);
		for (ContextType type : ContextType.values()) {
			ContextRoot contextRoot = store.get(ContextRoot.class, type);
			if (contextRoot != null) {
				result.put(type, contextRoot);
			}
		}

		return result;
	}

	public Optional<TemplateTestStep> getTemplateTestStep() {		
		return Optional.ofNullable(tplTestStep);
	}

	public void addWriteRequests(Collection<WriteRequest> writeRequests) {
		for (WriteRequest wr : writeRequests) {
			Map<String, WriteRequest> channelGroupWriteRequests = this.writeRequests.computeIfAbsent(
					wr.getChannelGroup().getID(), (channelGroupId) -> new HashMap<String, WriteRequest>());
			channelGroupWriteRequests.put(wr.getChannel().getID(), wr);
		}
	}

	public Map<String, Map<String, WriteRequest>> getWriteRequests() {
		return this.writeRequests;
	}
}
