package org.eclipse.mdm.csvadapter;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.ExtSystem;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.prefs.CsvPreference;

import com.google.common.base.Strings;

public class CsvConfiguration {

	private static final Logger LOG = LoggerFactory.getLogger(CsvConfiguration.class);

	/**
	 * ExternalSystem name to use for the import
	 */
	private static final String PROP_EXTSYSTEMNAME = "extSystemName";

	/**
	 * Path the file containing the quantity mapping
	 */
	private static final String PROP_QUANTITYMAPPING = "quantitymapping";

	/**
	 * If true, the quantities of the channels are in the header of the measured
	 * data, after Channel name and Unit name.
	 */
	private static final String PARAM_QUANTITIES_IN_HEADER = "quantities_in_header";

	/**
	 * If true, a channel named MeasurementPoints is added, if no xAxis is defined.
	 */
	private static final String PARAM_ADD_MEASUREMENT_POINTS = "add_measurement_points";

	/**
	 * Valid values for parameter are valid date patterns according to
	 * https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/time/format/DateTimeFormatter.html#patterns
	 */
	private final static String PARAM_DATETIME_FORMAT = "datetime_format";

	/**
	 * Valid values for parameter are: semicolon,tab,comma
	 */
	private final static String PARAM_DELIMITER = "delimiter";

	/**
	 * If true, the importer check that the physical dimension of the unit of the
	 * imported channel matches the physical dimension of the default unit of the
	 * quantity.
	 */
	private final static String PARAM_CHECK_PHYSICAL_DIMENSION = "check_physical_dimension";

	private final static String PARAM_QUOTE = "quote";

	/**
	 * Valid values for parameter are: crnl,nl
	 */
	private final static String PARAM_END_OF_LINE = "end_of_line";

	public static boolean getQuantitiesInHeader(Map<String, String> connectionParameters) {

		String d = connectionParameters.getOrDefault(PARAM_QUANTITIES_IN_HEADER, "false");

		if (Strings.isNullOrEmpty(d)) {
			return false;
		} else {
			return Boolean.parseBoolean(d);
		}
	}

	public static boolean getAddMeasurementPoints(Map<String, String> connectionParameters) {

		String d = connectionParameters.getOrDefault(PARAM_ADD_MEASUREMENT_POINTS, "true");

		if (Strings.isNullOrEmpty(d)) {
			return true;
		} else {
			return Boolean.parseBoolean(d);
		}
	}

	public static File getQuantityMappingFile(Map<String, String> connectionParameters) {

		String quantityMappingPath = getParameterValue(connectionParameters, PROP_QUANTITYMAPPING, false);

		if (!Strings.isNullOrEmpty(quantityMappingPath)) {
			File quantityMappingFile = new File(quantityMappingPath);
			LOG.debug("Configured quantity mapping: {}, exists: {}", quantityMappingFile.getAbsolutePath(),
					quantityMappingFile.exists());
			return quantityMappingFile;
		} else {
			LOG.debug("No quantity mapping configured (property: {})" + PROP_QUANTITYMAPPING);
			return null;
		}
	}

	public static List<ExtSystemAttribute> getExtSystemAttributes(EntityManager entityManager,
			Map<String, String> connectionParameters) {

		String extSystemName = getParameterValue(connectionParameters, PROP_EXTSYSTEMNAME, false);

		if (!Strings.isNullOrEmpty(extSystemName)) {
			List<ExtSystem> extSystemList = entityManager.loadAll(ExtSystem.class, extSystemName);

			if (extSystemList == null || extSystemList.isEmpty()) {
				LOG.warn("External system with name '{}' not found! Try to import file without mapping!",
						extSystemName);
				return Collections.emptyList();
			} else if (extSystemList.size() > 1) {
				throw new CSVAdapterException(
						String.format("More than one external system with name '%s' found!", extSystemName));
			} else {
				return entityManager.loadChildren(extSystemList.get(0), ExtSystemAttribute.class);
			}
		}
		return Collections.emptyList();
	}

	private static String getParameterValue(Map<String, String> connectionParameters, String paraName,
			boolean mandatory) throws CSVAdapterException {
		String returnVal = connectionParameters.get(paraName);

		if (mandatory && Strings.isNullOrEmpty(returnVal)) {
			throw new CSVAdapterException(String.format("Mandatory parameter '%s' not set", paraName));
		}

		return returnVal;
	}

	public static DateTimeFormatter getDateTimeFormat(Map<String, String> connectionParameters) {
		String d = connectionParameters.getOrDefault(PARAM_DATETIME_FORMAT, "yyyy-MM-dd'T'HH:mm:ss");

		try {
			if (d == null || d.isEmpty()) {
				return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
			} else {
				return DateTimeFormatter.ofPattern(d);
			}
		} catch (IllegalArgumentException e) {
			throw new CSVAdapterException("Parameter '" + PARAM_DATETIME_FORMAT + "' with value '"
					+ "' is not a valid date pattern. Refer to https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/time/format/DateTimeFormatter.html#patterns .",
					e);
		}
	}

	public static int getDelimiterChar(Map<String, String> connectionParameters) {
		String d = connectionParameters.getOrDefault(PARAM_DELIMITER, "semicolon");

		if (d == null || d.isEmpty() || "semicolon".equalsIgnoreCase(d)) {
			return ';';
		} else if ("tab".equalsIgnoreCase(d)) {
			return '\t';
		} else if ("comma".equalsIgnoreCase(d)) {
			return ',';
		} else {
			throw new CSVAdapterException(
					"Parameters '" + PARAM_DELIMITER + "' must be a single character, but got " + d);
		}
	}

	public static char getQuoteChar(Map<String, String> connectionParameters) {
		String d = connectionParameters.getOrDefault(PARAM_QUOTE, "\"");

		if (d == null || d.isEmpty() || "\"".equalsIgnoreCase(d)) {
			return '"';
		} else if (d.length() == 1) {
			return d.charAt(0);
		} else {
			throw new CSVAdapterException("Parameters '" + PARAM_QUOTE + "' must be a single character, but got " + d);
		}
	}

	public static String getEndOfLineSymbols(Map<String, String> connectionParameters) {
		String d = connectionParameters.getOrDefault(PARAM_END_OF_LINE, "nl");

		if (d == null || d.isEmpty() || "nl".equalsIgnoreCase(d)) {
			return "\n";
		} else if ("crnl".equalsIgnoreCase(d)) {
			return "crnl";
		} else {
			throw new CSVAdapterException(
					"Parameters '" + PARAM_END_OF_LINE + "' must be one of 'nl' or 'crnl', but got " + d);
		}
	}

	public static boolean getCheckPhyiscalDimension(Map<String, String> connectionParameters) {

		String d = connectionParameters.getOrDefault(PARAM_CHECK_PHYSICAL_DIMENSION, "true");

		if (Strings.isNullOrEmpty(d)) {
			return false;
		} else {
			return Boolean.parseBoolean(d);
		}
	}

	public static CsvPreference getCsvPreference(Map<String, String> connectionParameters) {
		char quoteChar = CsvConfiguration.getQuoteChar(connectionParameters);
		int delimiterChar = CsvConfiguration.getDelimiterChar(connectionParameters);
		String eolSymbols = CsvConfiguration.getEndOfLineSymbols(connectionParameters);

		return new CsvPreference.Builder(quoteChar, delimiterChar, eolSymbols).build();
	}

}
