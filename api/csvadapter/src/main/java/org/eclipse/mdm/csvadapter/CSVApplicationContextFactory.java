/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.ApplicationContextFactory;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.reader.ImportFileReader;
import org.eclipse.mdm.csvadapter.reader.ImportFileReaderImpl;
import org.eclipse.mdm.csvadapter.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * Creating the ApplicationContext for handling CSV files
 * 
 * @author akn
 *
 */
public class CSVApplicationContextFactory implements ApplicationContextFactory {

	private static final Logger LOG = LoggerFactory.getLogger(CSVApplicationContextFactory.class);

	/**
	 * 
	 */
	public CSVApplicationContextFactory() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseApplicationContextFactory#connect(java.util.Map)
	 */
	public ApplicationContext connect(String sourceName, Map<String, String> connectionParameters)
			throws ConnectionException {
		throw new ConnectionException("Not supported!");
	}

	public ApplicationContext connect(String sourceName, Map<String, String> connectionParameters,
			ApplicationContext targetContext) throws ConnectionException {

		try {
			if ("file".equals(getParameterValue(connectionParameters, "write_mode", false))) {
				return this.initContext(connectionParameters, targetContext);
			}

			EntityManager entityManager = targetContext.getEntityManager()
					.orElseThrow(() -> new ConnectionException("TargetEntity manager not present!"));
			File csvFile = new File(getParameterValue(connectionParameters, "csvFile", true));

			ImportFileReader importFileReader = new ImportFileReaderImpl(csvFile, connectionParameters);

			String tplTestStepName = getParameterValue(importFileReader.getMetaData(),
					Constants.IMPORTFILE_ATTRIBUTE_TESTSTEPTEMPLATENAME, true);
			List<TemplateTestStep> tplTestSteps = entityManager.loadAll(TemplateTestStep.class, tplTestStepName);

			if (tplTestSteps == null || tplTestSteps.isEmpty()) {
				throw new CSVAdapterException(
						String.format("Mandatory TestStepTemplate '%s' not found!", tplTestStepName));
			}
			TemplateTestStep tplTestStep = tplTestSteps.get(0);
			List<Quantity> quantities = entityManager.loadAll(Quantity.class);
			List<Unit> units = entityManager.loadAll(Unit.class);

			List<ExtSystemAttribute> extSystemAttrList = CsvConfiguration.getExtSystemAttributes(entityManager,
					connectionParameters);

			Optional<ModelManager> modelManager = null;
			if (targetContext.getModelManager().isPresent()) {
				modelManager = targetContext.getModelManager();
			} else {
				throw new ConnectionException("Target model manager is not available!");
			}

			return new CSVApplicationContext(importFileReader, tplTestStep, quantities, units, connectionParameters,
					connectionParameters.get("csvFile"), modelManager, extSystemAttrList);
		} catch (CSVAdapterException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new ConnectionException(e.getLocalizedMessage(), e);
		}
	}

	private ApplicationContext initContext(Map<String, String> connectionParameters, ApplicationContext targetContext)
			throws ConnectionException {
		try {
			EntityManager entityManager = targetContext.getEntityManager()
					.orElseThrow(() -> new ConnectionException("TargetEntity manager not present!"));

			List<ExtSystemAttribute> extSystemAttrList = CsvConfiguration.getExtSystemAttributes(entityManager,
					connectionParameters);

			List<Quantity> quantities = entityManager.loadAll(Quantity.class);
			List<Unit> units = entityManager.loadAll(Unit.class);

			Optional<ModelManager> modelManager = null;
			if (targetContext.getModelManager().isPresent()) {
				modelManager = targetContext.getModelManager();
			} else {
				throw new ConnectionException("Target model manager is not available!");
			}

			return new CSVApplicationContext(null, null, quantities, units, connectionParameters,
					connectionParameters.get("csvFile"), modelManager, extSystemAttrList);
		} catch (CSVAdapterException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new ConnectionException(e.getLocalizedMessage(), e);
		}
	}

	private String getParameterValue(Map<String, String> connectionParameters, String paraName, boolean mandatory)
			throws CSVAdapterException {
		String returnVal = connectionParameters.get(paraName);

		if (mandatory && Strings.isNullOrEmpty(returnVal)) {
			throw new CSVAdapterException(String.format("Mandatory parameter '%s' not set", paraName));
		}

		return returnVal;
	}

}
