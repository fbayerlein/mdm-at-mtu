/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.writer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.csvadapter.CSVApplicationContext;
import org.eclipse.mdm.csvadapter.CSVEntityManager;
import org.eclipse.mdm.csvadapter.CsvConfiguration;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.util.CSVAdapterUtil;
import org.eclipse.mdm.csvadapter.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvListWriter;
import org.supercsv.prefs.CsvPreference;

/**
 * Class for writing measurements to a CSV files.
 * 
 * @author jz
 */
public class CSVWriter extends Writer {
	private static final Logger LOG = LoggerFactory.getLogger(CSVWriter.class);

	private CSVApplicationContext context;
	private CSVEntityManager entityManager;

	private CsvPreference csvPreference = CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE;

	private final DateTimeFormatter dateTimeFormatter;

	/**
	 * @param context
	 */
	public CSVWriter(CSVApplicationContext context) {
		this.context = context;
		this.entityManager = (CSVEntityManager) context.getEntityManager().get();
		this.dateTimeFormatter = CsvConfiguration.getDateTimeFormat(context.getParameters());

		csvPreference = CsvConfiguration.getCsvPreference(context.getParameters());
	}

	@Override
	protected DateTimeFormatter getFormatter() {
		return dateTimeFormatter;
	}

	/**
	 * Writes the data into an CSV file
	 * 
	 * @param testStep
	 * @throws DataAccessException
	 */
	public Path writeCSV(ChannelGroup channelGroup) throws DataAccessException {
		Path parentPath = new File(context.getParameters().get("csvFile")).getParentFile().toPath();

		Measurement measurement = entityManager.loadParent(channelGroup, Measurement.class).get();

		String folder = sanitzeFilename(
				entityManager.loadParent(measurement, TestStep.class).map(TestStep::getName).orElse("TestStep"));
		String filename = sanitzeFilename(measurement.getName() + "_" + channelGroup.getName());

		Path csvFile = avoidNameClashes(parentPath.resolve(folder), filename, ".csv");

		try {
			Files.createDirectories(csvFile.getParent());

			try (CsvListWriter writer = new CsvListWriter(new FileWriter(csvFile.toFile()), csvPreference)) {
				writeHeader(writer, measurement, channelGroup);

				writeChannelGroup(writer, channelGroup);
			}
		} catch (IOException ex) {
			LOG.warn("Could not write CSV file {}.", csvFile, ex);
			throw new CSVAdapterException("Could not write CSV file " + csvFile, ex);
		}
		LOG.info("CSV file saved to {}.", csvFile);
		return csvFile;
	}

	private void writeChannelGroup(CsvListWriter writer, ChannelGroup channelGroup) throws IOException {
		List<WriteRequest> writeRequests = sortChannels(
				entityManager.getWriteRequests().get(channelGroup.getID()).values());

		List<String> header = new ArrayList<>();
		List<String> units = new ArrayList<>();
		List<String> quantities = new ArrayList<>();
		List<List<String>> exportColumns = new ArrayList<>();

		for (WriteRequest writeRequest : writeRequests) {
			if (channelGroup.equals(writeRequest.getChannelGroup())) {
				header.add(writeRequest.getChannel().getName());

				units.add(getUnitName(writeRequest));
				Quantity quantity = writeRequest.getChannel().getQuantity();
				if (quantity == null) {
					quantities.add("");
				} else {
					quantities.add(quantity.getName());
				}
				exportColumns.add(getValues(writeRequest.getValues()));
			}
		}
		writer.write(header);
		writer.write(units);
		if (CsvConfiguration.getQuantitiesInHeader(context.getParameters())) {
			writer.write(quantities);
		}

		for (int row = 0; row < channelGroup.getNumberOfValues(); row++) {
			List<String> c = new ArrayList<>();
			for (int column = 0; column < exportColumns.size(); column++) {
				c.add(exportColumns.get(column).get(row));
			}
			writer.write(c);
		}
	}

	private String getUnitName(WriteRequest writeRequest) {
		String unit = "";

		if (writeRequest.getChannel().getUnit() != null) {
			unit = writeRequest.getChannel().getUnit().getName();
		}
		return unit;
	}

	private void writeHeader(CsvListWriter writer, Measurement measurement, ChannelGroup channelGroup)
			throws IOException {

		TestStep testStep = entityManager.loadParent(measurement, TestStep.class).get();
		Test test = entityManager.loadParent(testStep, Test.class).get();
		Pool pool = entityManager.loadParent(test, Pool.class).get();
		Project project = entityManager.loadParent(pool, Project.class).get();

		writer.write("[METADATA]");
		writer.write(Constants.IMPORTFILE_ATTRIBUTE_PROJECTNAME + "=" + project.getName());
		writer.write(Constants.IMPORTFILE_ATTRIBUTE_STRUCTURELEVELNAME + "=" + pool.getName());
		writer.write(Constants.IMPORTFILE_ATTRIBUTE_TESTNAME + "=" + test.getName());
		writer.write(Constants.IMPORTFILE_ATTRIBUTE_TESTTEMPLATENAME + "="
				+ getValueOrEmptyIfNull(test.getValue(Constants.TESTATTR_TEMPLATE, true)));
		writer.write(Constants.IMPORTFILE_ATTRIBUTE_TESTSTEPNAME + "=" + testStep.getName());
		writer.write(Constants.IMPORTFILE_ATTRIBUTE_TESTSTEPTEMPLATENAME + "="
				+ getValueOrEmptyIfNull(testStep.getValue(Constants.TESTATTR_TEMPLATE, true)));

		for (Map.Entry<ContextType, ContextRoot> e : entityManager.loadContexts(measurement).entrySet()) {
			ContextType contextType = e.getKey();
			ContextRoot contextRoot = e.getValue();
			if (contextRoot == null) {
				continue;
			}

			for (ContextComponent cc : contextRoot.getContextComponents()) {
				for (Entry<String, Value> v : cc.getValues().entrySet()) {
					if (isIgnoredContextAttribute(v.getKey())) {
						continue;
					}
					if (v.getValue().isValid()) {
						writer.write(contextType + "." + cc.getName() + "." + v.getKey() + "="
								+ serialize(v.getValue().extract()));
					} else {
						writer.write(contextType + "." + cc.getName() + "." + v.getKey() + "=");
					}
				}
			}
		}

		writer.write(
				Constants.IMPORTFILE_ATTRIBUTE_MEASUREMENTNAME + Constants.KEY_VALUE_SEPARATOR + measurement.getName());
//		writer.write("" + measurement.getFileLinks());
		writer.write(Constants.IMPORTFILE_ATTRIBUTE_CHANNELGROUPNAME + Constants.KEY_VALUE_SEPARATOR
				+ channelGroup.getName());
		writer.write("");
		writer.write("[MEASUREDDATA]");
	}

	private String getValueOrEmptyIfNull(Value value) {
		String returnVal = "";

		if (value != null) {
			returnVal = value.extract();
		}

		return returnVal;
	}

	protected List<String> getValues(Object values) {
		List<String> strValues = new ArrayList<>();
		if (values instanceof float[]) {
			for (float value : (float[]) values) {
				strValues.add(Float.toString(value));
			}
		} else if (values instanceof int[]) {
			for (int value : (int[]) values) {
				strValues.add(Integer.toString(value));
			}
		} else if (values instanceof long[]) {
			for (long value : (long[]) values) {
				strValues.add(Long.toString(value));
			}
		} else if (values instanceof double[]) {
			for (double value : (double[]) values) {
				strValues.add(Double.toString(value));
			}
		} else if (values instanceof short[]) {
			for (short value : (short[]) values) {
				strValues.add(Short.toString(value));
			}
		} else if (values instanceof byte[]) {
			for (byte value : (byte[]) values) {
				strValues.add(Byte.toString(value));
			}
		} else if (values instanceof boolean[]) {
			for (boolean value : (boolean[]) values) {
				strValues.add(value ? "true" : "false");
			}
		} else if (values instanceof Instant[]) {
			for (Instant value : (Instant[]) values) {
				strValues.add(CSVAdapterUtil.toCSVDate(value, dateTimeFormatter));
			}
		} else if (values instanceof String[]) {
			strValues = Arrays.asList((String[]) values);
		} else {
			throw new CSVAdapterException("Value " + values + " not supported!");
		}
		return strValues;
	}
}
