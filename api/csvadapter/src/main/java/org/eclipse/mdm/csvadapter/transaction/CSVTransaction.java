/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.transaction;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.UpdateRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.csvadapter.CSVApplicationContext;
import org.eclipse.mdm.csvadapter.CSVEntityFactory;
import org.eclipse.mdm.csvadapter.CSVModelLoader;
import org.eclipse.mdm.csvadapter.ExcelApplicationContext;
import org.eclipse.mdm.csvadapter.writer.CSVWriter;
import org.eclipse.mdm.csvadapter.writer.ExcelWriter;

/**
 * Class for collecting values
 * 
 * @author jz
 */
public class CSVTransaction implements Transaction {

	private final CSVApplicationContext context;
	private final CSVModelLoader csvModelLoader;

	/**
	 * @param context
	 * @param csvModelLoader
	 */
	public CSVTransaction(CSVApplicationContext context, CSVModelLoader csvModelLoader) {
		this.context = context;
		this.csvModelLoader = csvModelLoader;
	}

	@Override
	public <T extends Entity> void create(Collection<T> entities) throws DataAccessException {
		if (entities != null && !entities.isEmpty()) {
			for (T entity : entities) {
				CSVEntityFactory.getCSVCore((BaseEntity) entity).setID(UUID.randomUUID().toString());
				csvModelLoader.putEntityToCache(entity);
			}
		}
	}

	@Override
	public <T extends Entity> void update(Collection<T> entities) throws DataAccessException {
		if (entities != null && !entities.isEmpty()) {
			for (T entity : entities) {
				csvModelLoader.putEntityToCache(entity);
			}
		}
	}

	@Override
	public <T extends Deletable> void delete(Collection<T> entities) throws DataAccessException {
		throw new DataAccessException("Not implemented method: delete(Collection<T> entities)");
	}

	@Override
	public void writeMeasuredValues(Collection<WriteRequest> writeRequests) throws DataAccessException {
		if (writeRequests.isEmpty()) {
			return;
		}

		csvModelLoader.addWriteRequests(writeRequests);
	}

	@Override
	public void updateMeasuredValues(Collection<UpdateRequest> updateRequests) throws DataAccessException {
		throw new DataAccessException(
				"Not implemented method: updateMeasuredValues(Collection<UpdateRequest> updateRequests)");
	}

	@Override
	public void appendMeasuredValues(Collection<WriteRequest> writeRequests) throws DataAccessException {
		throw new DataAccessException(
				"Not implemented method: appendMeasuredValues(Collection<WriteRequest> writeRequests)");
	}

	@Override
	public void commit() throws DataAccessException {
		List<Path> files = new ArrayList<>();
		if (context instanceof ExcelApplicationContext) {
			ExcelWriter writer = new ExcelWriter((ExcelApplicationContext) context);

			for (TestStep testStep : context.getEntityManager().get().loadAll(TestStep.class)) {
				files.add(writer.writeExcel(testStep));
			}
		} else if (context instanceof CSVApplicationContext) {
			CSVWriter writer = new CSVWriter((CSVApplicationContext) context);

			for (ChannelGroup channelGroup : context.getEntityManager().get().loadAll(ChannelGroup.class)) {
				files.add(writer.writeCSV(channelGroup));
			}
		}
	}

	@Override
	public void abort() {
		// nothing to do
	}

	@Override
	public FileService getFileService(FileServiceType fileServiceType) {
		return context.getFileService(fileServiceType).get();
	}
}
