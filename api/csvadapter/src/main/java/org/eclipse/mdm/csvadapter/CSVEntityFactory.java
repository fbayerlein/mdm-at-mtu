/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.adapter.ChildrenStore;
import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.adapter.EntityStore;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.csvadapter.entites.CSVCore;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.util.Constants;

/**
 * @author akn
 *
 */
public class CSVEntityFactory extends EntityFactory {

	private static final Map<Class<?>, String> classTypeMap = new HashMap<>();
	static {
		classTypeMap.put(Project.class, "Project");
		classTypeMap.put(Pool.class, "Pool");
		classTypeMap.put(Test.class, "Test");
		classTypeMap.put(TestStep.class, "TestStep");
		classTypeMap.put(Measurement.class, "Measurement");
	}

	private final ModelManager modelManager;

	private int currentId = 0;

	public CSVEntityFactory(ModelManager modelManager) {
		this.modelManager = modelManager;
	}

	@Override
	protected void validateEnum(Enumeration<?> enumerationObj) {

	}

	@Override
	protected Optional<User> getLoggedInUser() {
		return Optional.empty();
	}

	@Override
	protected <T extends Entity> Core createCore(Class<T> entityClass) {
		return getDefaultCore(entityClass);
	}

	private <T extends Entity> Core getDefaultCore(Class<T> entityClass) {

		Core core = new CSVCore(modelManager.getEntityType(entityClass), entityClass);
		core.setID(incrementId());
		return core;
	}

	private String incrementId() {
		return String.valueOf(++currentId);
	}

	@Override
	protected <T extends Entity> Core createCore(Class<T> entityClass, ContextType contextType) {
		Core core = new CSVCore(modelManager.getEntityType(entityClass, contextType), entityClass);
		core.setID(incrementId());
		return core;
	}

	@Override
	protected <T extends Entity> Core createCore(String name, Class<T> entityClass) {
		Core core = new CSVCore(modelManager.getEntityType(name), entityClass);
		core.setID(incrementId());
		return core;
	}

	/**
	 * 
	 * @param meaResult
	 * @param templateRoot
	 * @param compValueMap
	 * @return
	 * @throws CSVAdapterException
	 */
	public ContextRoot createContextRoot(TemplateRoot templateRoot, Map<String, List<Value>> compValueMap)
			throws CSVAdapterException {
		ContextRoot contextRoot = super.createContextRoot(templateRoot);

		Iterator<String> compNameIterator = compValueMap.keySet().iterator();

		while (compNameIterator.hasNext()) {
			String compName = compNameIterator.next();

			Optional<ContextComponent> contextComponent = contextRoot.getContextComponent(compName);

			if (contextComponent.isPresent()) {
				for (Value value : compValueMap.get(compName)) {
					try {
						contextComponent.get().getValues().get(value.getName()).set(value.extract());
					} catch (Exception e) {
						contextComponent.get().getValues().get(value.getName()).set(value.extract());
					}
				}
			}
		}

		if (ContextType.UNITUNDERTEST.equals(contextRoot.getContextType())) {
			contextRoot.setMimeType(new MimeType(Constants.MIMETYPE_UNITUNDERTEST));
		} else if (ContextType.TESTEQUIPMENT.equals(contextRoot.getContextType())) {
			contextRoot.setMimeType(new MimeType(Constants.MIMETYPE_TESTEQUIPMENT));
		} else if (ContextType.TESTSEQUENCE.equals(contextRoot.getContextType())) {
			contextRoot.setMimeType(new MimeType(Constants.MIMETYPE_TESTSEQUENCE));
		}

		return contextRoot;
	}

	/**
	 * {@inheritDoc}
	 */
	public static final Core getCSVCore(BaseEntity entity) {
		return EntityFactory.getCore(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public static final EntityStore getMutableStore(BaseEntity entity) {
		return EntityFactory.getMutableStore(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public static final EntityStore getPermanentStore(BaseEntity entity) {
		return EntityFactory.getPermanentStore(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public static final ChildrenStore getChildrenStore(BaseEntity entity) {
		return EntityFactory.getChildrenStore(entity);
	}
}
