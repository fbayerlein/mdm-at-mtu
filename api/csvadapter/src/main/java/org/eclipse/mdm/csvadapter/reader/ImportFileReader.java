/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.reader;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author akn
 *
 */
public interface ImportFileReader {

	/**
	 * 
	 * @return the metadata as from the file
	 */
	Map<String, String> getMetaData();

	/**
	 * 
	 * @return the measured data from the file
	 */
	Map<String, List<String>> getMeasuredData();

	/**
	 * 
	 * @return the measured data from the file
	 */
	Map<String, List<String>> getFlags();

	/**
	 * 
	 * @return mapping of measurementquantity to unit of the file
	 */
	Map<String, String> getUnitMapping();

	/**
	 * 
	 * @return mapping of measurementquantity to quantity of the file
	 */
	Map<String, String> getQuantityMapping();

	/**
	 * 
	 * @return the file
	 */
	File getFile();

}
