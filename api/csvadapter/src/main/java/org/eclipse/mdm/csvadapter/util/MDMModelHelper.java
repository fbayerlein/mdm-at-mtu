/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.util;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;

import com.google.common.base.Strings;

/**
 * @author akn
 *
 */
public class MDMModelHelper {

	private final TemplateTestStep tplTestStep;

	/**
	 * 
	 */
	public MDMModelHelper(TemplateTestStep tplTestStep) {
		this.tplTestStep = tplTestStep;
	}

	public ValueType<?> getValueType(ContextType contextType, String compName, String attrName) {
		ValueType<?> valueType = null;

		Optional<TemplateRoot> templateRoot = tplTestStep.getTemplateRoot(contextType);
		if (templateRoot.isPresent()) {

			Optional<TemplateComponent> templateComponent = templateRoot.get().getTemplateComponent(compName);
			if (templateComponent.isPresent()) {
				Optional<CatalogAttribute> catalogAttribute = templateComponent.get().getCatalogComponent()
						.getCatalogAttribute(attrName);

				if (catalogAttribute.isPresent()) {
					valueType = catalogAttribute.get().getValueType();
				}
			}
		}

		return valueType;
	}

	public Enumeration<?> getEnumObj(ContextType contextType, String compName, String attrName) {
		Enumeration<?> enumObj = null;

		Optional<TemplateRoot> templateRoot = tplTestStep.getTemplateRoot(contextType);
		if (templateRoot.isPresent()) {

			Optional<TemplateComponent> templateComponent = templateRoot.get().getTemplateComponent(compName);
			if (templateComponent.isPresent()) {
				Optional<CatalogAttribute> catalogAttribute = templateComponent.get().getCatalogComponent()
						.getCatalogAttribute(attrName);

				if (catalogAttribute.isPresent()) {
					enumObj = catalogAttribute.get().getEnumerationObject();
				}
			}
		}

		return enumObj;
	}

	public boolean[] createBoolenValues(List<String> stringValues) {
		boolean[] returnVal = new boolean[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = stringValues.get(i);

			if (!Strings.isNullOrEmpty(value)) {
				returnVal[i] = Boolean.valueOf(value);
			}
		}
		return returnVal;
	}

	public byte[] createByteValues(List<String> stringValues) throws CSVAdapterException {

		byte[] returnVal = new byte[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = stringValues.get(i);
			try {
				if (!Strings.isNullOrEmpty(value)) {
					returnVal[i] = Byte.valueOf(value);
				}
			} catch (NumberFormatException e) {
				throw new CSVAdapterException(String.format("Not possible to convert '%s' to byte", value));
			}
		}
		return returnVal;

	}

	public Instant[] createDateValues(List<String> stringValues, DateTimeFormatter formatter)
			throws CSVAdapterException {
		Instant[] returnVal = new Instant[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {

			String value = stringValues.get(i);

			if (!Strings.isNullOrEmpty(value)) {
				returnVal[i] = CSVAdapterUtil.fromCSVDate(value, formatter);
			}

		}
		return returnVal;
	}

	public double[] createDoubleValues(List<String> stringValues) throws CSVAdapterException {
		double[] returnVal = new double[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = stringValues.get(i);
			try {

				if (!Strings.isNullOrEmpty(value)) {
					returnVal[i] = Double.valueOf(value);
				}
			} catch (NumberFormatException e) {
				throw new CSVAdapterException(String.format("Not possible to convert '%s' to double", value));
			}
		}

		return returnVal;
	}

	public float[] createFloatValues(List<String> stringValues) throws CSVAdapterException {
		float[] returnVal = new float[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = stringValues.get(i);
			try {
				if (!Strings.isNullOrEmpty(value)) {
					returnVal[i] = Float.valueOf(value.replace(',', '.'));
				}
			} catch (NumberFormatException e) {
				throw new CSVAdapterException(String.format("Not possible to convert '%s' to float", value));
			}
		}

		return returnVal;
	}

	public int[] createIntegerValues(List<String> stringValues) throws CSVAdapterException {
		int[] returnVal = new int[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = stringValues.get(i);
			try {
				if (!Strings.isNullOrEmpty(value)) {
					returnVal[i] = Integer.valueOf(value);
				}
			} catch (NumberFormatException e) {
				throw new CSVAdapterException(String.format("Not possible to convert '%s' to int", value));
			}
		}

		return returnVal;
	}

	public long[] createLongValues(List<String> stringValues) throws CSVAdapterException {
		long[] returnVal = new long[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = stringValues.get(i);
			try {
				if (!Strings.isNullOrEmpty(value)) {
					returnVal[i] = Long.valueOf(value);
				}
			} catch (NumberFormatException e) {
				throw new CSVAdapterException(String.format("Not possible to convert '%s' to long", value));
			}
		}

		return returnVal;
	}

	public short[] createShortValues(List<String> stringValues) throws CSVAdapterException {
		short[] returnVal = new short[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = stringValues.get(i);
			try {
				if (!Strings.isNullOrEmpty(value)) {
					returnVal[i] = Short.valueOf(value);
				}
			} catch (NumberFormatException e) {
				throw new CSVAdapterException(String.format("Not possible to convert '%s' to short", value));
			}
		}

		return returnVal;
	}

	public String[] createStringValues(List<String> stringValues) throws CSVAdapterException {
		String[] returnVal = new String[stringValues.size()];

		for (int i = 0; i < stringValues.size(); i++) {
			String value = "";

			if (!Strings.isNullOrEmpty(stringValues.get(i))) {
				value = stringValues.get(i);
			}

			returnVal[i] = value;

		}

		return returnVal;
	}

	public MimeType getContextCompMimeType(ContextComponent comp) throws CSVAdapterException {
		ContextType contextType = comp.getContextRoot().getContextType();
		MimeType returnVal = null;
		Optional<TemplateRoot> templateRoot = tplTestStep.getTemplateRoot(contextType);

		if (templateRoot.isPresent()) {
			Optional<TemplateComponent> templateComponent = templateRoot.get().getTemplateComponent(comp.getName());

			if (templateComponent.isPresent()) {
				returnVal = new MimeType(buildContextComponentMimeType(contextType,
						templateComponent.get().getCatalogComponent().getName(), templateComponent.get().getName()));
			}
		}

		return returnVal;

	}

	private String buildContextComponentMimeType(ContextType contextType, String catName, String tplName)
			throws CSVAdapterException {
		StringBuilder sb = new StringBuilder();
		sb.append(getContextCompBasicMimeType(contextType));
		sb.append('.');
		sb.append(catName);
		sb.append('.');
		sb.append(tplName);
		return sb.toString();

	}

	private String getContextCompBasicMimeType(ContextType contextType) throws CSVAdapterException {
		String returnVal = null;

		if (ContextType.UNITUNDERTEST.equals(contextType)) {
			returnVal = Constants.MIMETYPE_UNIT_UNDER_TEST_PART;
		} else if (ContextType.TESTEQUIPMENT.equals(contextType)) {
			returnVal = Constants.MIMETYPE_TEST_EQUIPMENT_PART;
		} else if (ContextType.TESTSEQUENCE.equals(contextType)) {
			returnVal = Constants.MIMETYPE_TEST_SEQUENCE_PART;
		} else {
			throw new CSVAdapterException(String.format("invalid ContextType '%s'.", contextType));
		}

		return returnVal;

	}

}
