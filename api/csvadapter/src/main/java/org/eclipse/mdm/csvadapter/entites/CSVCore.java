/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.entites;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.mdm.api.base.adapter.DefaultCore;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;

import org.eclipse.mdm.csvadapter.util.Constants;

/**
 * @author akn
 *
 */
public class CSVCore extends DefaultCore {

	private Map<String, Value> values = new HashMap<>();

	public <T> CSVCore(EntityType entityType, Class<T> entityClass) {
		super(entityType);
		values = super.getValues();

		if (TestStep.class.equals(entityClass) | Test.class.equals(entityClass)) {
			values.put(Constants.TESTATTR_TEMPLATE, ValueType.STRING.create(null));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.adapter.DefaultCore#getValues()
	 */
	@Override
	public Map<String, Value> getValues() {
		return values;
	}

}
