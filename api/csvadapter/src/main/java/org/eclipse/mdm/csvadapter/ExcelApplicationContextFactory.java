/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.ApplicationContextFactory;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.ExtSystem;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * Creating the ApplicationContext for handling with Excel files
 * 
 * @author jz
 *
 */
public class ExcelApplicationContextFactory implements ApplicationContextFactory {

	private static final String PROP_EXTSYSTEMNAME = "extSystemName";

	private static final Logger LOG = LoggerFactory.getLogger(ExcelApplicationContextFactory.class);

	/**
	 * 
	 */
	public ExcelApplicationContextFactory() {

	}

	@Override
	public ApplicationContext connect(String sourceName, Map<String, String> connectionParameters)
			throws ConnectionException {
		throw new ConnectionException("Not supported!");
	}

	@Override
	public ApplicationContext connect(String sourceName, Map<String, String> connectionParameters,
			ApplicationContext targetContext) throws ConnectionException {
		try {
			if ("file".equals(getParameterValue(connectionParameters, "write_mode", false))) {
				EntityManager entityManager = targetContext.getEntityManager()
						.orElseThrow(() -> new ConnectionException("TargetEntity manager not present!"));

				String extSystemName = getParameterValue(connectionParameters, PROP_EXTSYSTEMNAME, false);

				List<ExtSystemAttribute> extSystemAttrList = new ArrayList<>();

				if (!Strings.isNullOrEmpty(extSystemName)) {
					List<ExtSystem> extSystemList = entityManager.loadAll(ExtSystem.class, extSystemName);

					if (extSystemList == null || extSystemList.isEmpty()) {
						LOG.warn("External system with name '{}' not found! Try to import file without mapping!",
								extSystemName);
					} else if (extSystemList.size() > 1) {
						throw new CSVAdapterException(
								String.format("More than one external system with name '%s' found!", extSystemName));
					} else {
						extSystemAttrList = entityManager.loadChildren(extSystemList.get(0), ExtSystemAttribute.class);
					}

				}

				List<Quantity> quantities = entityManager.loadAll(Quantity.class);
				List<Unit> units = entityManager.loadAll(Unit.class);

				Optional<ModelManager> modelManager = null;
				if (targetContext.getModelManager().isPresent()) {
					modelManager = targetContext.getModelManager();
				} else {
					throw new ConnectionException("Target model manager is not available!");
				}

				return new ExcelApplicationContext(null, null, quantities, units, connectionParameters,
						connectionParameters.get("excelFile"), modelManager, extSystemAttrList);
			} else {
				throw new ConnectionException("Import not supported!");
			}
		} catch (CSVAdapterException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new ConnectionException(e.getLocalizedMessage(), e);
		}
	}

	private String getParameterValue(Map<String, String> connectionParameters, String paraName, boolean mandatory)
			throws CSVAdapterException {
		String returnVal = connectionParameters.get(paraName);

		if (mandatory && Strings.isNullOrEmpty(returnVal)) {
			throw new CSVAdapterException(String.format("Mandatory parameter '%s' not set", paraName));
		}

		return returnVal;
	}
}
