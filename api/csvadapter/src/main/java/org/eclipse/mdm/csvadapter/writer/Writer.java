/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.writer;

import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.csvadapter.util.CSVAdapterUtil;

/**
 * Base class for writing measurements to files, providing data access.
 * 
 * @author jz
 */
public abstract class Writer {

	public final List<String> ignoredAttributes = Arrays.asList("Name", "MimeType");

	private final Comparator<WriteRequest> channelComparator = Comparator.comparing(WriteRequest::isIndependent)
			.reversed().thenComparingInt(r -> r.getAxisType().ordinal()).thenComparing(r -> r.getChannel().getName());

	public List<WriteRequest> sortChannels(Collection<WriteRequest> writeRequests) {
		List<WriteRequest> channels = new ArrayList<>();
		channels.addAll(writeRequests);
		channels.sort(channelComparator);
		return channels;
	}

	protected String sanitzeFilename(String filename) {
		return filename.replaceAll("[^a-zA-Z0-9.-]", "_");
	}

	abstract protected DateTimeFormatter getFormatter();

	/**
	 * @param parentPath parent path of the file to create
	 * @param filename   filename of the file without extension
	 * @param extension  extension of the file to create including <code>.</code>,
	 *                   e.g. ".csv"
	 * @return a path of the form parentPath/filename_{index}.csv
	 */
	protected Path avoidNameClashes(Path parentPath, String filename, String extension) {
		Path path = parentPath.resolve(filename + extension);
		int counter = 1;
		while (path.toFile().exists()) {
			path = parentPath.resolve(filename + "_" + counter + extension);
			counter++;
		}

		return path;
	}

	protected boolean isIgnoredContextAttribute(String attributeName) {
		return ignoredAttributes.contains(attributeName);
	}

	public String serialize(Object object) {
		return CSVAdapterUtil.serialize(object, getFormatter());
	}

}
