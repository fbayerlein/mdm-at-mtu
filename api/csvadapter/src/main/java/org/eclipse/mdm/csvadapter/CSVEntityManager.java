/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.ContextDescribable;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.StatusAttachable;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Status;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.transaction.CSVTransaction;

/**
 * @author akn
 *
 */
public class CSVEntityManager implements EntityManager {

	private final CSVModelLoader csvModelLoader;
	private final CSVApplicationContext context;

	/**
	 * @param csvModelLoader
	 * @param modelManager
	 * 
	 */
	public CSVEntityManager(CSVModelLoader csvModelLoader, CSVApplicationContext context) {
		this.csvModelLoader = csvModelLoader;
		this.context = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseEntityManager#loadEnvironment()
	 */
	@Override
	public Environment loadEnvironment() throws DataAccessException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseEntityManager#load(java.lang.Class,
	 * java.util.Collection)
	 */
	@Override
	public <T extends Entity> List<T> load(Class<T> entityClass, Collection<String> instanceIDs)
			throws DataAccessException {

		return load(entityClass, instanceIDs, true);
	}
	
	

	@Override
	public <T extends Entity> List<T> load(Class<T> entityClass, Collection<String> instanceIDs,
			boolean loadOptionalRelations) throws DataAccessException {
		return csvModelLoader.getEntities(entityClass, instanceIDs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseEntityManager#loadAll(java.lang.Class,
	 * java.lang.String)
	 */
	@Override
	public <T extends Entity> List<T> loadAll(Class<T> entityClass, String pattern) throws DataAccessException {
		return csvModelLoader != null ? csvModelLoader.getEntities(entityClass, pattern) : new ArrayList<>();
	}

	@Override
	public <T extends Entity> List<T> loadAll(Class<T> entityClass, Collection<String> names)
			throws DataAccessException {
		return csvModelLoader.getEntitiesByName(entityClass, names);
	}

	@Override
	public <T extends Entity> List<T> loadWithoutChildren(Class<T> entityClass, Collection<String> instanceIDs)
			throws DataAccessException {
		throw new DataAccessException("loadBasic not implemented!");
	}

	@Override
	public <T extends Entity> List<T> loadWithoutChildren(Class<T> entityClass, ContextType contextType,
			Collection<String> instanceIDs) throws DataAccessException {
		throw new DataAccessException("loadBasic not implemented!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseEntityManager#loadParent(org.eclipse.mdm.api.
	 * base.model.Entity, java.lang.Class)
	 */
	@Override
	public <T extends Entity> Optional<T> loadParent(Entity child, Class<T> entityClass) throws DataAccessException {
		return csvModelLoader.getParent(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseEntityManager#loadChildren(org.eclipse.mdm.api.
	 * base.model.Entity, java.lang.Class, java.lang.String)
	 */
	@Override
	public <T extends Entity> List<T> loadChildren(Entity parent, Class<T> entityClass, String pattern)
			throws DataAccessException {
		return csvModelLoader.getChildren(parent, entityClass, pattern);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseEntityManager#loadContextTypes(org.eclipse.mdm.
	 * api.base.model.ContextDescribable)
	 */
	@Override
	public List<ContextType> loadContextTypes(ContextDescribable contextDescribable) throws DataAccessException {
		return new ArrayList<>(loadContexts(contextDescribable).keySet());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseEntityManager#loadContexts(org.eclipse.mdm.api.
	 * base.model.ContextDescribable, org.eclipse.mdm.api.base.model.ContextType[])
	 */
	@Override
	public Map<ContextType, ContextRoot> loadContexts(ContextDescribable contextDescribable,
			ContextType... contextTypes) throws DataAccessException {
		if (Measurement.class.equals(contextDescribable.getClass())) {
			return csvModelLoader.getContextRootCache(contextDescribable);
		} else {
			return Collections.emptyMap();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseEntityManager#loadRelatedEntities(org.eclipse.
	 * mdm.api.base.model.Entity, java.lang.String, java.lang.Class)
	 */
	@Override
	public <T extends Entity> List<T> loadRelatedEntities(Entity entity, String relationName, Class<T> relatedClass) {
		// Crude workaround for DescriptiveFile relations (ignore these as we don't have
		// them even if they exist in the
		// transfer source/target application model, which unfortunately also serves as
		// our application model):
		if (relatedClass != null && DescriptiveFile.class.isAssignableFrom(relatedClass)) {
			return Collections.emptyList();
		}

		throw new DataAccessException(
				"Not implemented method: loadRelatedEntities(Entity entity, String relationName, Class<T> relatedClass)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseEntityManager#readMeasuredValues(org.eclipse.mdm
	 * .api.base.massdata.ReadRequest)
	 */
	@Override
	public List<MeasuredValues> readMeasuredValues(ReadRequest readRequest) throws DataAccessException {
		try {
			return csvModelLoader.getMeasuredValues(readRequest.getChannelGroup(), readRequest.getChannels());
		} catch (CSVAdapterException e) {
			throw new DataAccessException(e.getLocalizedMessage(), e);
		}
	}

	public Map<String, Map<String, WriteRequest>> getWriteRequests() {
		return csvModelLoader.getWriteRequests();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseEntityManager#startTransaction()
	 */
	@Override
	public Transaction startTransaction() throws DataAccessException {
		return new CSVTransaction(context, csvModelLoader);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.BaseEntityManager#getLinks(java.util.Collection)
	 */
	@Override
	public Map<Entity, String> getLinks(Collection<Entity> entities) {
		throw new DataAccessException("Not implemented method: getLinks(Collection<Entity> entities)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.dflt.EntityManager#load(java.lang.Class,
	 * org.eclipse.mdm.api.base.model.ContextType, java.util.Collection)
	 */
	@Override
	public <T extends Entity> List<T> load(Class<T> entityClass, ContextType contextType,
			Collection<String> instanceIDs) throws DataAccessException {
		throw new DataAccessException(
				"Not implemented method: load(Class<T> entityClass, ContextType contextType, Collection<String> instanceIDs)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.dflt.EntityManager#loadAll(java.lang.Class,
	 * org.eclipse.mdm.api.base.model.ContextType, java.lang.String)
	 */
	@Override
	public <T extends Entity> List<T> loadAll(Class<T> entityClass, ContextType contextType, String pattern)
			throws DataAccessException {
		throw new DataAccessException(
				"Not implemented method: loadAll(Class<T> entityClass, ContextType contextType, String pattern)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.dflt.EntityManager#loadAll(java.lang.Class,
	 * org.eclipse.mdm.api.dflt.model.Status, java.lang.String)
	 */
	@Override
	public <T extends StatusAttachable> List<T> loadAll(Class<T> entityClass, Status status, String pattern) {
		throw new DataAccessException(
				"Not implemented method: loadAll(Class<T> entityClass, Status status, String pattern)");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int countRelatedEntities(Entity entity, String relationName) {
		throw new DataAccessException(
				"Not implemented method: countRelatedEntities(Entity entity, String relationName)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.dflt.EntityManager#loadTemplate(org.eclipse.mdm.api.base.
	 * model.Test)
	 */
	@Override
	public Optional<TemplateTest> loadTemplate(Test test) {
		return Optional.empty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.dflt.EntityManager#loadTemplate(org.eclipse.mdm.api.base.
	 * model.TestStep)
	 */
	@Override
	public Optional<TemplateTestStep> loadTemplate(TestStep testStep) {
		return csvModelLoader.getTemplateTestStep();
	}

	@Override
	public <T extends Entity> List<T> loadAll(Class<T> entityClass, Filter filter)
			throws DataAccessException, UnsupportedOperationException {
		throw new UnsupportedOperationException("Queries are not supported.");
	}

	@Override
	public <T extends Entity> List<T> loadAll(Class<T> entityClass, ContextType contextType, String compName,
			Collection<String> ids) throws DataAccessException {
		throw new DataAccessException(
				"Not implemented method: loadAll(Class<T> entityClass, ContextType contextType, String compName, Collection<String> ids)");
	}
}
