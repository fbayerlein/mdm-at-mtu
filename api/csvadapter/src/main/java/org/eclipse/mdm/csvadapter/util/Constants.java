/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.util;

public class Constants {
	public static final String IMPORTFILE_ATTRIBUTE_TESTSTEPTEMPLATENAME = "MDMTemplateTestStepName";
	public static final String IMPORTFILE_ATTRIBUTE_TESTTEMPLATENAME = "MDMTemplateTestName";
	public static final String IMPORTFILE_ATTRIBUTE_PROJECTNAME = "PROJECT.Name";
	public static final String IMPORTFILE_ATTRIBUTE_STRUCTURELEVELNAME = "STRUCTURELEVEL.Name";
	public static final String IMPORTFILE_ATTRIBUTE_TESTNAME = "TEST.Name";
	public static final String IMPORTFILE_ATTRIBUTE_TESTMDMLINKS = "TEST.MDMLinks";
	public static final String IMPORTFILE_ATTRIBUTE_TESTSTEPNAME = "TESTSTEP.Name";
	public static final String IMPORTFILE_ATTRIBUTE_TESTSTEPMDMLINKS = "TESTSTEP.MDMLinks";
	public static final String IMPORTFILE_ATTRIBUTE_MEASUREMENTNAME = "MEASUREMENT.Name";
	public static final String IMPORTFILE_ATTRIBUTE_MEASUREMENTMDMLINKS = "MEASUREMENT.MDMLinks";
	public static final String IMPORTFILE_ATTRIBUTE_CHANNELGROUPNAME = "CHANNELGROUP.Name";
	public static final String IMPORTFILE_CONTEXT_UNITUNDERTEST = "UNITUNDERTEST";
	public static final String IMPORTFILE_CONTEXT_TESTEQUIPMENT = "TESTEQUIPMENT";
	public static final String IMPORTFILE_CONTEXT_TESTSEQUENCE = "TESTSEQUENCE";
	public static final String IMPORTFILE_ATTRIBUTE_XAXIS = "xAxis";

	public static final String TESTATTR_TEMPLATE = "template";

	public static final String MIMETYPE_PROJECT = "application/x-asam.aotest";
	public static final String MIMETYPE_STRUCTURE_LEVEL = "application/x-asam.aosubtest.structurelevel";
	public static final String MIMETYPE_TEST = "application/x-asam.aosubtest.test";
	public static final String MIMETYPE_TEST_STEP = "application/x-asam.aosubtest.teststep";
	public static final String MIMETYPE_MEASUREMENT = "application/x-asam.aomeasurement";
	public static final String MIMETYPE_MEASUREMENT_QUANTITY = "application/x-asam.aomeasurementquantity";
	public static final String MIMETYPE_UNITUNDERTEST = "application/x-asam.aounitundertest.unitundertest";
	public static final String MIMETYPE_TESTSEQUENCE = "application/x-asam.aotestsequence.testsequence";
	public static final String MIMETYPE_TESTEQUIPMENT = "application/x-asam.aotestequipment.testequipment";
	public static final String MIMETYPE_SUBMATRIX = "application/x-asam.aosubmatrix";
	public static final String MIMETYPE_UNIT_UNDER_TEST_PART = "application/x-asam.aounitundertestpart";
	public static final String MIMETYPE_TEST_SEQUENCE_PART = "application/x-asam.aotestsequencepart";
	public static final String MIMETYPE_TEST_EQUIPMENT_PART = "application/x-asam.aotestequipmentpart";

	public static final String FILE_LIST_SEPARATOR = "\\|";

	public static final String KEY_VALUE_SEPARATOR = "=";

	private Constants() {
	}
}
