/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;

import com.google.common.base.Strings;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Shorts;

/**
 * @author akn
 *
 */
public class CSVAdapterUtil {

	/**
	 * 
	 */
	private CSVAdapterUtil() {

	}

	public static Object getValueFromString(Path csvFileParent, ValueType<?> valueType, String stringVal,
			DateTimeFormatter formatter, Enumeration<?> enumObj) throws CSVAdapterException {
		Object returnVal = null;

		if (stringVal != null && valueType.isBoolean()) {
			returnVal = Boolean.valueOf(stringVal);
		} else if (stringVal != null && valueType.isBooleanSequence()) {
			returnVal = Booleans.toArray(splitValue(stringVal).map(Boolean::valueOf).collect(Collectors.toList()));
		} else if (stringVal != null && valueType.isByte()) {
			returnVal = Byte.valueOf(stringVal);
		} else if (stringVal != null && valueType.isByteSequence()) {
			returnVal = Bytes.toArray(splitValue(stringVal).map(Byte::valueOf).collect(Collectors.toList()));
		} else if (stringVal != null && valueType.isDate()) {
			returnVal = fromCSVDate(stringVal, formatter);
		} else if (stringVal != null && valueType.isDateSequence()) {
			returnVal = splitValue(stringVal).map(d -> fromCSVDate(d, formatter)).toArray(Instant[]::new);
		} else if (stringVal != null && valueType.isDouble()) {
			returnVal = Double.valueOf(stringVal);
		} else if (stringVal != null && valueType.isDoubleSequence()) {
			returnVal = splitValue(stringVal).mapToDouble(Double::valueOf).toArray();
		} else if (stringVal != null && valueType.isFloat()) {
			returnVal = Float.valueOf(stringVal);
		} else if (stringVal != null && valueType.isFloatSequence()) {
			returnVal = Floats.toArray(splitValue(stringVal).map(Float::valueOf).collect(Collectors.toList()));
		} else if (stringVal != null && valueType.isInteger()) {
			returnVal = Integer.valueOf(stringVal);
		} else if (stringVal != null && valueType.isIntegerSequence()) {
			returnVal = splitValue(stringVal).mapToInt(Integer::valueOf).toArray();
		} else if (stringVal != null && valueType.isLong()) {
			returnVal = Long.valueOf(stringVal);
		} else if (stringVal != null && valueType.isLongSequence()) {
			returnVal = splitValue(stringVal).mapToLong(Long::valueOf).toArray();
		} else if (stringVal != null && valueType.isShort()) {
			returnVal = Short.valueOf(stringVal);
		} else if (stringVal != null && valueType.isShortSequence()) {
			returnVal = Shorts.toArray(splitValue(stringVal).map(Short::valueOf).collect(Collectors.toList()));
		} else if (stringVal != null && valueType.isString()) {
			returnVal = stringVal;
		} else if (stringVal != null && valueType.isStringSequence()) {
			returnVal = splitValue(stringVal).toArray(String[]::new);
		} else if (stringVal != null && valueType.isEnumeration()) {
			returnVal = enumObj.valueOf(stringVal);
		} else if (stringVal != null && valueType.isEnumerationSequence()) {
			returnVal = splitValue(stringVal).map(v -> enumObj.valueOf(v)).toArray(EnumerationValue[]::new);
		} else if (stringVal != null && valueType.isFileLink()) {
			List<String> fileReferences = getFileReferences(stringVal);

			if (fileReferences.size() > 1) {
				throw new CSVAdapterException("Single file attribute has more than one linked file");
			} else if (fileReferences.size() == 1) {
				returnVal = getFileLink(csvFileParent, fileReferences.get(0));
			}
		} else if (stringVal != null && valueType.isFileLinkSequence()) {
			List<String> fileReferences = getFileReferences(stringVal);

			List<FileLink> fileLinks = new ArrayList<>();

			for (String fileReference : fileReferences) {
				fileLinks.add(getFileLink(csvFileParent, fileReference));
			}

			returnVal = fileLinks.toArray(new FileLink[fileLinks.size()]);
		} else {
			throw new CSVAdapterException(
					"Unsupported DataType. Cannot convert value '" + stringVal + "' for DataType: " + valueType);
		}

		return returnVal;
	}

	public static Stream<String> splitValue(String stringVal) {
		return Stream.of(stringVal.split("(?<!\\\\),")).map(v -> v.replace("\\,", ",").replace("\\\\", "\\"));
	}

	public static String mergeValues(String[] values) {
		return Stream.of(values).map(v -> v.replace("\\", "\\\\").replace(",", "\\,")).collect(Collectors.joining(","));
	}

	public static FileLink getFileLink(Path parentPath, String fileReference) throws CSVAdapterException {
		try {
			// split at [ preceded by an event number of backslashes
			String[] squareBracketSplit = fileReference.split("(?<!\\\\)(?:(\\\\\\\\)*)\\[");

			String reference = squareBracketSplit[0].replace("\\\\", "\\").replace("\\[", "[").replace("\\]", "]")
					.trim();
			Path filePath = parentPath.resolve(reference);

			MimeType mimeType;
			String mime = Files.probeContentType(filePath);
			mimeType = mime == null ? new MimeType("application/octet-stream") : new MimeType(mime);

			String description = filePath.getFileName().toString();

			if (squareBracketSplit.length > 1) {
				String[] commaSplit = squareBracketSplit[1].replaceAll("\\]$", "").split("(?<!\\\\),");

				if (commaSplit.length > 0) {
					mimeType = new MimeType(commaSplit[0].replace("\\\\", "\\").replace("\\[", "[").replace("\\]", "]")
							.replace("\\,", ",").trim());
				}

				if (commaSplit.length > 1) {
					description = commaSplit[1].replace("\\\\", "\\").replace("\\[", "[").replace("\\]", "]")
							.replace("\\,", ",").trim();
				}
			}
			return FileLink.newRemote(reference, mimeType, description, -1L, null, FileServiceType.EXTREF);

		} catch (IOException e) {
			throw new CSVAdapterException(e.getLocalizedMessage(), e);
		}
	}

	public static List<String> getFileReferences(String value) {
		final List<String> fileReferences = new ArrayList<>();

		if (!Strings.isNullOrEmpty(value)) {
			Arrays.asList(value.split("(?<!\\\\)" + Constants.FILE_LIST_SEPARATOR))
					.forEach(s -> fileReferences.add(s.replace("\\|", "|").trim()));
		}

		return fileReferences;
	}

	/**
	 * Converts given {@link String} to {@link Instant}.
	 *
	 * @param input
	 * @param formatter
	 * @return The converted {@code String} is returned.
	 * @throws CSVAdapterException
	 */
	public static Instant fromCSVDate(String input, DateTimeFormatter formatter) throws CSVAdapterException {
		if (input == null || input.isEmpty()) {
			return null;
		}

		try {
			return LocalDateTime.parse(input, formatter).toInstant(ZoneOffset.UTC);
		} catch (DateTimeParseException e) {
			throw new CSVAdapterException("Invalid CSV date: Given value '" + input
					+ "' does not conform to the configured pattern '" + formatter.toString() + "'.", e);
		}
	}

	/**
	 * Converts given {@link Instant} to {@link String}.
	 *
	 * @param input
	 * @param formatter
	 * @return The converted LocalDateTime is returned.
	 */
	public static String toCSVDate(Instant input, DateTimeFormatter formatter) {
		if (input == null) {
			return null;
		}

		return formatter.format(input);
	}

	public static String serialize(Object object, DateTimeFormatter formatter) {
		if (object instanceof String[]) {
			return Stream.of((String[]) object).collect(Collectors.joining(","));
		} else if (object instanceof EnumerationValue) {
			return ((EnumerationValue) object).name();
		} else if (object instanceof EnumerationValue[]) {
			return Stream.of((EnumerationValue[]) object).map(ev -> ev.name()).collect(Collectors.joining(","));
		} else if (object instanceof boolean[]) {
			boolean[] booleanArray = (boolean[]) object;
			return IntStream.range(0, booleanArray.length).mapToObj(i -> Boolean.toString(booleanArray[i]))
					.collect(Collectors.joining(","));
		} else if (object instanceof byte[]) {
			byte[] byteArray = (byte[]) object;
			return IntStream.range(0, byteArray.length).mapToObj(i -> Byte.toString(byteArray[i]))
					.collect(Collectors.joining(","));
		} else if (object instanceof short[]) {
			short[] shortArray = (short[]) object;
			return IntStream.range(0, shortArray.length).mapToObj(i -> Short.toString(shortArray[i]))
					.collect(Collectors.joining(","));
		} else if (object instanceof int[]) {
			return IntStream.of((int[]) object).mapToObj(Integer::toString).collect(Collectors.joining(","));
		} else if (object instanceof long[]) {
			return LongStream.of((long[]) object).mapToObj(Long::toString).collect(Collectors.joining(","));
		} else if (object instanceof float[]) {
			float[] floatArray = (float[]) object;
			return IntStream.range(0, floatArray.length).mapToObj(i -> Float.toString(floatArray[i]))
					.collect(Collectors.joining(","));
		} else if (object instanceof double[]) {
			return DoubleStream.of((double[]) object).mapToObj(Double::toString).collect(Collectors.joining(","));
		} else if (object instanceof Instant) {
			return CSVAdapterUtil.toCSVDate((Instant) object, formatter);
		} else if (object instanceof Instant[]) {
			return Stream.of((Instant[]) object).map(s -> toCSVDate(s, formatter)).collect(Collectors.joining(","));
		} else if (object instanceof FileLink) {
			return serializeFileLink((FileLink) object);
		} else if (object instanceof FileLink[]) {
			return Stream.of((FileLink[]) object).map(CSVAdapterUtil::serializeFileLink)
					.collect(Collectors.joining(","));
		} else {
			return Objects.toString(object);
		}
	}

	public static String serializeFileLink(FileLink fileLink) {
		return fileLink.getFileName() + "[" + fileLink.getMimeType() + ", " + fileLink.getDescription() + "]";
	}

}
