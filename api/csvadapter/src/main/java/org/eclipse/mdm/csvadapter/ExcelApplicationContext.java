/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.csvadapter.exception.CSVAdapterException;
import org.eclipse.mdm.csvadapter.reader.ImportFileReader;

/**
 * Implements the {@link ApplicationContext} interface to return
 * {@link EntityFactory} and {@link EntityManager}
 * 
 * @author jz
 *
 */
public class ExcelApplicationContext extends CSVApplicationContext {

	/**
	 * Constructor
	 * 
	 * @param importFileReader
	 * @param tplTestStep
	 * @param quantities
	 * @param units
	 * @param parameters
	 * @param fileName
	 * @param modelManager
	 * @param extSystemAttrList
	 * @throws CSVAdapterException
	 */
	public ExcelApplicationContext(ImportFileReader importFileReader, TemplateTestStep tplTestStep,
			List<Quantity> quantities, List<Unit> units, Map<String, String> parameters, String fileName,
			Optional<ModelManager> modelManager, List<ExtSystemAttribute> extSystemAttrList)
			throws CSVAdapterException {
		super(importFileReader, tplTestStep, quantities, units, parameters, fileName, modelManager, extSystemAttrList);
		csvEntityManager = new CSVEntityManager(csvModelLoader, this);
	}

}
