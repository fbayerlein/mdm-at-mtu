/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.writer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.csvadapter.CSVEntityManager;
import org.eclipse.mdm.csvadapter.ExcelApplicationContext;
import org.junit.Ignore;

import com.google.common.collect.ImmutableMap;

@Ignore
public class ExcelWriterTest {

	int numberOfRows = 10_000;
	int numberOfCols = 100;

	@org.junit.Test
	public void test() throws IOException {

		Project project = mock(Project.class);
		when(project.getName()).thenReturn("MyProject");

		Pool pool = mock(Pool.class);
		when(pool.getName()).thenReturn("MyPool");

		Test test = mock(Test.class);
		when(test.getName()).thenReturn("MyTest");

		TestStep testStep = mock(TestStep.class);
		when(testStep.getName()).thenReturn("MyTestStep");

		Measurement measurement = mock(Measurement.class);
		when(measurement.getName()).thenReturn("MyMeasurement");

		ChannelGroup channelgroup = mock(ChannelGroup.class);
		when(channelgroup.getName()).thenReturn("MyChannelGroup");
		when(channelgroup.getID()).thenReturn("1");
		when(channelgroup.getNumberOfValues()).thenReturn(numberOfRows);

		Unit unit = mock(Unit.class);
		when(unit.getName()).thenReturn("km/h");

		Channel channel = mock(Channel.class);
		when(channel.getName()).thenReturn("MyChannel");
		when(channel.getID()).thenReturn("1");
		when(channel.getUnit()).thenReturn(unit);

		CSVEntityManager em = mock(CSVEntityManager.class);
		when(em.loadParent(pool, Project.class)).thenReturn(Optional.of(project));
		when(em.loadParent(test, Pool.class)).thenReturn(Optional.of(pool));
		when(em.loadParent(testStep, Test.class)).thenReturn(Optional.of(test));
		when(em.loadParent(measurement, TestStep.class)).thenReturn(Optional.of(testStep));

		when(em.loadChildren(testStep, Measurement.class)).thenReturn(Arrays.asList(measurement));
		when(em.loadChildren(measurement, ChannelGroup.class)).thenReturn(Arrays.asList(channelgroup));

		WriteRequest wr = WriteRequest.create(channelgroup, channel, AxisType.Y_AXIS).explicit()
				.floatValues(new float[numberOfRows]).build();

		Map<String, Map<String, WriteRequest>> channelGroups = new HashMap<>();
		Map<String, WriteRequest> channels = new HashMap<>();
		for (int c = 0; c < numberOfCols; c++) {
			channels.put("channel" + c, wr);
		}

		channelGroups.put("1", channels);

		when(em.getWriteRequests()).thenReturn(channelGroups);

		Path tmp = Files.createTempDirectory("excelExport");

		ExcelApplicationContext context = mock(ExcelApplicationContext.class);
		when(context.getEntityManager()).thenReturn(Optional.of(em));
		when(context.getParameters()).thenReturn(ImmutableMap.of("excelFile", tmp.toAbsolutePath().toString()));

		ExcelWriter w = new ExcelWriter(context);

		Path p = w.writeExcel(testStep);
		System.out.println(p);
//		try {
//			Desktop.getDesktop().open(p.toFile());
//			System.in.read();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		Files.deleteIfExists(tmp);
	}
}
