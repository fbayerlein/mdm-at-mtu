package org.eclipse.mdm.csvadapter.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Paths;

import org.eclipse.mdm.api.base.model.MimeType;
import org.junit.Test;

public class CSVAdapterUtilTest {

	@Test
	public void testSplitValue() {
		assertThat(CSVAdapterUtil.splitValue("ts\\\\String1,ts\\,String2")).contains("ts\\String1", "ts,String2");
	}

	@Test
	public void testMergeValue() {
		assertThat(CSVAdapterUtil.mergeValues(new String[] { "ts\\String1", "ts,String2" }))
				.isEqualTo("ts\\\\String1,ts\\,String2");
	}

	@Test
	public void testGetFileLink() {
		assertThat(CSVAdapterUtil.getFileLink(Paths.get(""), "appendix.pdf"))
				.hasFieldOrPropertyWithValue("fileName", "appendix.pdf")
				.hasFieldOrPropertyWithValue("mimeType", new MimeType("application/pdf"))
				.hasFieldOrPropertyWithValue("description", "appendix.pdf");

		assertThat(CSVAdapterUtil.getFileLink(Paths.get(""), "appendix.pdf[application/pdf, Appended PDF file]"))
				.hasFieldOrPropertyWithValue("fileName", "appendix.pdf")
				.hasFieldOrPropertyWithValue("mimeType", new MimeType("application/pdf"))
				.hasFieldOrPropertyWithValue("description", "Appended PDF file");

		assertThat(CSVAdapterUtil.getFileLink(Paths.get(""),
				"appendix\\[0\\].pdf[application/pdf, Desc \\[0\\,1\\] with pipe | and \\\\"))
						.hasFieldOrPropertyWithValue("fileName", "appendix[0].pdf")
						.hasFieldOrPropertyWithValue("mimeType", new MimeType("application/pdf"))
						.hasFieldOrPropertyWithValue("description", "Desc [0,1] with pipe | and \\");

	}

	@Test
	public void testGetFileReferences() {
		assertThat(CSVAdapterUtil.getFileReferences("")).isEmpty();

		assertThat(CSVAdapterUtil.getFileReferences("appendix.pdf")).contains("appendix.pdf");

		assertThat(CSVAdapterUtil.getFileReferences("appendix.pdf|test.txt")).contains("appendix.pdf", "test.txt");

		assertThat(CSVAdapterUtil.getFileReferences("appendix.pdf[application/pdf, Appended PDF file]"))
				.contains("appendix.pdf[application/pdf, Appended PDF file]");

		assertThat(CSVAdapterUtil
				.getFileReferences("appendix\\[0\\].pdf[application/pdf, Desc \\[0\\,1\\] with pipe \\| and \\\\"))
						.contains("appendix\\[0\\].pdf[application/pdf, Desc \\[0\\,1\\] with pipe | and \\\\");

	}
}
