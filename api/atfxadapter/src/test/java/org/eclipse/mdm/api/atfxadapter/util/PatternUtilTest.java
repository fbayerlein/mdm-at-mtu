/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.api.atfxadapter.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class PatternUtilTest {

	@Test
	public void testContainsUnescapedWildcard() {
		assertThat(PatternUtil.containsUnescapedWildcard("abcd")).isFalse();
		assertThat(PatternUtil.containsUnescapedWildcard("\\*")).isFalse();
		assertThat(PatternUtil.containsUnescapedWildcard("\\?")).isFalse();
		assertThat(PatternUtil.containsUnescapedWildcard("*")).isTrue();
		assertThat(PatternUtil.containsUnescapedWildcard("?")).isTrue();
		assertThat(PatternUtil.containsUnescapedWildcard("ab*cd?ef")).isTrue();
		assertThat(PatternUtil.containsUnescapedWildcard("ab\\\\*cd")).isTrue();
		assertThat(PatternUtil.containsUnescapedWildcard("ab*\\cd")).isTrue();
		assertThat(PatternUtil.containsUnescapedWildcard("ab\\*cd\\?ef")).isFalse();
		assertThat(PatternUtil.containsUnescapedWildcard("ab\\*cd?ef")).isTrue();
		assertThat(PatternUtil.containsUnescapedWildcard("ab\\?cd*ef")).isTrue();
		assertThat(PatternUtil.containsUnescapedWildcard("ab?cd\\*ef")).isTrue();
	}
}
