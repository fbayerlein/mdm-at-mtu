/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.api.atfxadapter;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.assertj.core.groups.Tuple;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.common.collect.ImmutableMap;

public class AtfxAdapterTest {

	public static final String ATFX_FILE = "src/test/resources/Right_Acc.atfx";

	public static ApplicationContext context;

	@BeforeClass
	public static void init() throws ConnectionException {
		Map<String, String> map = ImmutableMap.of("atfxfile", ATFX_FILE, "freetext.active", "false");

		context = new ATFXContextFactory().connect("ATFX", map);
	}

	@AfterClass
	public static void teardown() throws ConnectionException {
		context.close();
	}

	@org.junit.Test
	public void testLoadLoggedOnUser() {
		EntityManager em = context.getEntityManager().get();

		assertThat(em.loadLoggedOnUser()).isNotNull();
	}

	@org.junit.Test
	public void testLoadEnvironment() {
		EntityManager em = context.getEntityManager().get();

		assertThat(em.loadEnvironment().getName()).isEqualTo("MDM");
	}

	@org.junit.Test
	public void testLoadAllProjects() {

		EntityManager em = context.getEntityManager().get();

		assertThat(em.loadAll(Project.class)).extracting(Project::getName).contains("PMV Model P");
	}

	@org.junit.Test
	public void testLoadAllTests() {

		EntityManager em = context.getEntityManager().get();

		assertThat(em.loadAll(Test.class)).extracting(Test::getName).contains("PBN_UNECE_R51_20140314170907");
	}

	@org.junit.Test
	public void testLoadAllTestSteps() {

		EntityManager em = context.getEntityManager().get();

		assertThat(em.loadAll(TestStep.class)).extracting(TestStep::getName).contains("PBN_UNECE_R51_Right_Acc_70");
	}

	@org.junit.Test
	public void testLoadTestStepWithExtRef() {
		
		EntityManager em = context.getEntityManager().get();

		assertThat(em.load(TestStep.class, "1").getFileLinks())
				.extracting(FileLink::getFileName, FileLink::getDescription, FileLink::getMimeType)
				.contains(Tuple.tuple("extref.txt", "a simple text file", new MimeType("text/plain")),
						Tuple.tuple("openmdm.png", "openMDM Logo", new MimeType("image/png")));
	}

	@org.junit.Test
	public void testLoadAllMeasurements() {

		EntityManager em = context.getEntityManager().get();
		assertThat(em.loadAll(Measurement.class)).extracting(Measurement::getName).contains("Channel");
	}

	@org.junit.Test
	public void testLoadParent() {

		EntityManager em = context.getEntityManager().get();
		Measurement meaResult = em.loadAll(Measurement.class).get(0);

		assertThat(em.loadParent(meaResult, TestStep.class).get())
				.extracting(TestStep::getName, as(InstanceOfAssertFactories.STRING))
				.contains("PBN_UNECE_R51_Right_Acc_70");
	}

	@org.junit.Test
	public void testLoadProjectChildren() {
		EntityManager em = context.getEntityManager().get();
		List<Project> projects = em.loadAll(Project.class);

		assertThat(projects).extracting(Project::getName).containsExactly("PMV Model P");

		List<Pool> pools = em.loadChildren(projects.get(0), Project.CHILD_TYPE_POOL);
		assertThat(pools).extracting(Pool::getName).contains("PBN Measurements");
	}

	@org.junit.Test
	public void testLoadChildren() {

		EntityManager em = context.getEntityManager().get();
		Test test = em.loadAll(Test.class).get(0);

		assertThat(em.loadChildren(test, TestStep.class).get(0))
				.extracting(TestStep::getName, as(InstanceOfAssertFactories.STRING))
				.contains("PBN_UNECE_R51_Right_Acc_70");
	}

	@org.junit.Test
	public void testLoadChildrenForChannelGroup() {

		EntityManager em = context.getEntityManager().get();
		ChannelGroup channelGroup = em.loadAll(ChannelGroup.class).get(0);

		assertThat(em.loadChildren(channelGroup, Channel.class)).extracting(Channel::getName).contains("X-Axis",
				"CHANNEL01", "CHANNEL02", "CHANNEL03", "CHANNEL04", "CHANNEL05", "CHANNEL06", "CHANNEL07", "CHANNEL08",
				"CHANNEL09", "CHANNEL10");
	}

	@org.junit.Test
	public void testLoadChildrenForChannelGroupWithPattern() {

		EntityManager em = context.getEntityManager().get();
		ChannelGroup channelGroup = em.loadAll(ChannelGroup.class).get(0);

		Assertions.assertThatThrownBy(() -> em.loadChildren(channelGroup, Channel.class, "*Axis"))
				.isInstanceOf(DataAccessException.class).hasMessageContaining("not supported");

		// assertThat(em.loadChildren(channelGroup, Channel.class,
		// "*Axis")).extracting(Channel::getName)
		// .contains("X-Axis", "CHANNEL01", "CHANNEL02", "CHANNEL03", "CHANNEL04",
		// "CHANNEL05", "CHANNEL06", "CHANNEL07", "CHANNEL08", "CHANNEL09",
		// "CHANNEL10");
	}

	@org.junit.Test
	public void testLoadParentForChannel() {

		EntityManager em = context.getEntityManager().get();
		Channel channel = em.loadAll(Channel.class).get(0);

		assertThat(em.loadParent(channel, ChannelGroup.class).get())
				.extracting(ChannelGroup::getName, as(InstanceOfAssertFactories.STRING)).contains("Channel");
	}

	@org.junit.Test
	public void testLoadAllChannelGroups() {

		EntityManager em = context.getEntityManager().get();

		assertThat(em.loadAll(ChannelGroup.class)).extracting(ChannelGroup::getName).contains("Channel");
	}

	@org.junit.Test
	public void testLoadAllChannels() {

		EntityManager em = context.getEntityManager().get();

		assertThat(em.loadAll(Channel.class)).extracting(Channel::getName).contains("X-Axis", "CHANNEL01", "CHANNEL02",
				"CHANNEL03", "CHANNEL04", "CHANNEL05", "CHANNEL06", "CHANNEL07", "CHANNEL08", "CHANNEL09", "CHANNEL10");
	}

	@org.junit.Test
	public void testLoadAllContext() {

		EntityManager em = context.getEntityManager().get();

		List<Measurement> measurements = em.loadAll(Measurement.class);
		assertThat(measurements).hasSize(1);

		Map<ContextType, ContextRoot> context = em.loadContexts(measurements.get(0));
		assertThat(context.get(ContextType.UNITUNDERTEST).getContextComponents()).extracting(cc -> cc.getName())
				.contains("FL_tyre");

		assertThat(context.get(ContextType.UNITUNDERTEST).getContextComponents().get(0).getValues()).hasSize(7)
				.containsEntry("Name", ValueType.STRING.create("Name", "FL_tyre"))
				.containsEntry("axle", ValueType.SHORT.create("axle", (short) 1))
				.containsEntry("side", ValueType.STRING.create("side", "Left"))
				.containsEntry("manufacturer", ValueType.STRING.create("manufacturer", "Tunnelsand"));

	}

	@org.junit.Test
	public void testReadMeasuredValues() {
		EntityManager em = context.getEntityManager().get();

		List<ChannelGroup> listCg = em.loadAll(ChannelGroup.class);
		assertThat(listCg).hasSize(1);

		List<MeasuredValues> values = em
				.readMeasuredValues(ReadRequest.create(listCg.get(0)).allChannels().allValues());
		assertThat(values).extracting(mv -> mv.getLength()).containsOnly(3001);
	}
}
