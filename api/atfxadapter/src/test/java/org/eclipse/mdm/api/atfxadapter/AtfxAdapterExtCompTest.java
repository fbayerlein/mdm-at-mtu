/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.api.atfxadapter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.massdata.ExternalComponentData;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.ReadRequest.ValuesMode;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.common.collect.ImmutableMap;

public class AtfxAdapterExtCompTest {

	public static final String ATFX_FILE = "../../nucleus/apicopy/src/test/resources/importtasktest_external.atfx";

	public static ApplicationContext context;

	@BeforeClass
	public static void init() throws ConnectionException {
		Map<String, String> map = ImmutableMap.of("atfxfile", ATFX_FILE, "freetext.active", "false");

		context = new ATFXContextFactory().connect("ATFX", map);
	}

	@AfterClass
	public static void teardown() throws ConnectionException {
		context.close();
	}

	@org.junit.Test
	public void testExtComps() {
		EntityManager em = context.getEntityManager().get();
		ChannelGroup group = em.loadAll(ChannelGroup.class).get(0);

		ReadRequest r = ReadRequest.create(group).valuesMode(ValuesMode.STORAGE).allChannels().allValues();

		// ValuesMode.STORAGE should be internally converted to
		// ValuesMode.STORAGE_PRESERVE_EXTCOMPS
		// by the ATFX EntityManager and thus ext comps should show up in the resulting
		// MeasuredValues:
		List<MeasuredValues> listMeasuredValues = em.readMeasuredValues(r);

		assertThat(listMeasuredValues).hasSize(11);
		listMeasuredValues.forEach(mv -> assertThat(mv.hasExternalComponents()).isTrue());
		listMeasuredValues.forEach(
				mv -> assertThat(mv.getExternalComponentData()).hasAtLeastOneElementOfType(ExternalComponentData.class));
	}
}
