/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.api.atfxadapter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.asam.ods.AoException;
import org.asam.ods.AoSession;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.ApplicationContextFactory;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.ExtSystem;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.omg.CORBA.ORB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import de.rechner.openatfx.AoServiceFactory;

public class ATFXContextFactory implements ApplicationContextFactory {

	private static final Logger LOG = LoggerFactory.getLogger(ATFXContextFactory.class);

	private static final ORB orb = ORB.init(new String[] {}, System.getProperties());

	private static final String PROP_EXTSYSTEMNAME = "extSystemName";

	@Override
	public ApplicationContext connect(String sourceName, Map<String, String> parameters) throws ConnectionException {
		return initApplicationContext(sourceName, parameters, null);
	}

	public ApplicationContext connect(String sourceName, Map<String, String> parameters,
			ApplicationContext targetContext) throws ConnectionException {

		EntityManager entityManager = targetContext.getEntityManager()
				.orElseThrow(() -> new ConnectionException("TargetEntity manager not present!"));

		String extSystemName = parameters.get(PROP_EXTSYSTEMNAME);

		List<ExtSystemAttribute> extSystemAttrList = new ArrayList<>();

		if (!Strings.isNullOrEmpty(extSystemName)) {
			List<ExtSystem> extSystemList = entityManager.loadAll(ExtSystem.class, extSystemName);

			if (extSystemList == null || extSystemList.isEmpty()) {
				LOG.warn("External system with name '{}' nor found! Try to import file without mapping!",
						extSystemName);
			} else if (extSystemList.size() > 1) {
				throw new ConnectionException(
						String.format("More than one external system with name '%s' found!", extSystemName));
			} else {
				extSystemAttrList = entityManager.loadChildren(extSystemList.get(0), ExtSystemAttribute.class);
			}
		}

		return initApplicationContext(sourceName, parameters, extSystemAttrList);
	}

	private ApplicationContext initApplicationContext(String sourceName, Map<String, String> parameters,
			List<ExtSystemAttribute> extSystemAttrList) throws ConnectionException {
		Path atfxFile = Paths.get(parameters.get("atfxfile"));

		try {
			AoSession aoSession;
			if (!Files.exists(atfxFile) || Files.size(atfxFile) == 0L) {
				try {
					LOG.debug(
							"File {} does not exist or is empty. Initializing with an empty openMDM application model atfx file.",
							atfxFile);
					Files.copy(ATFXContextFactory.class.getResourceAsStream("/emptyAtfx_AoFile.xml"), atfxFile);
				} catch (IOException e) {
					throw new ConnectionException(
							"Cannot create base ATFX file: " + atfxFile.toFile().getAbsolutePath());
				}
			}

			aoSession = AoServiceFactory.getInstance().newAoSession(orb, atfxFile.toFile());
			return new ATFXContext(sourceName, orb, aoSession, parameters, extSystemAttrList, atfxFile.toFile());
		} catch (AoException e) {
			throw new ConnectionException("Error " + e.reason, e);
		} catch (IOException | OdsException e) {
			throw new ConnectionException("Error reading ATFX file " + atfxFile.toFile().getAbsolutePath(), e);
		}
	}
}
