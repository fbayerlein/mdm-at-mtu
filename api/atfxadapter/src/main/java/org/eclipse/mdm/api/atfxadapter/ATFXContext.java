/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.api.atfxadapter;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.asam.ods.AoException;
import org.asam.ods.AoSession;
import org.eclipse.mdm.api.atfxadapter.filetransfer.ATFXFileService;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.query.ODSCorbaModelManager;
import org.eclipse.mdm.api.odsadapter.query.ODSEntityFactory;
import org.eclipse.mdm.api.odsadapter.query.ODSQueryService;
import org.eclipse.mdm.api.odsadapter.query.OdsException;
import org.omg.CORBA.ORB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ATFXContext encapsulates a session to the ASAM ODS CORBA API of openATFX
 *
 */
public class ATFXContext implements ODSContext {

	private static final Logger LOGGER = LoggerFactory.getLogger(ATFXContext.class);

	private Map<String, String> parameters;

	private ODSCorbaModelManager modelManager;
	private ATFXEntityManager entityManager;

	private File atfxFile;

	public static final String INCLUDE_CATALOG = "includeCatalog";
	public static final String INCLUDE_CATALOG_INSTANCES = "includeCatalogInstances";



	/**
	 * Creates a new ATFX application context.
	 * 
	 * @param orb               the CORBA ORB used to connect to the ODS API
	 * @param aoSession
	 * @param parameters
	 * @param atfxFile
	 * @param extSystemAttrList
	 * @throws AoException
	 */
	public ATFXContext(String sourceName, ORB orb, AoSession aoSession, Map<String, String> parameters,
			List<ExtSystemAttribute> extSystemAttrList, File atfxFile) throws OdsException, AoException {
		this.parameters = parameters;

		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			aoSession.setContextString(entry.getKey(), entry.getValue());
		}

		boolean includeCatalog = Boolean.valueOf(parameters.getOrDefault(INCLUDE_CATALOG, "false"));

		this.modelManager = new ODSCorbaModelManager(sourceName, orb, aoSession,
				new ATFXEntityConfigRepositoryLoader(includeCatalog));
		this.entityManager = new ATFXEntityManager(this, extSystemAttrList);
		this.atfxFile = atfxFile;

		LOGGER.debug("ATFXContext initialized.");
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSourceName() {
		return modelManager.getSourceName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<EntityManager> getEntityManager() {
		return Optional.of(entityManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ModelManager> getModelManager() {
		return Optional.of(modelManager);
	}

	@Override
	public Optional<EntityFactory> getEntityFactory() {
		try {
			return Optional.of(new ODSEntityFactory(modelManager, entityManager::loadLoggedOnUser));
		} catch (DataAccessException e) {
			throw new IllegalStateException("Unable to load instance of the logged in user.");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<QueryService> getQueryService() {
		return Optional.of(new ODSQueryService(modelManager));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getParameters() {
		return parameters;
	}

	/**
	 * @returns the string "atfx"
	 */
	@Override
	public String getAdapterType() {
		return "atfx";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSessionId() {
		return atfxFile.getAbsolutePath().toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		try {
			modelManager.close();
		} catch (OdsException e) {
			LOGGER.warn("Unable to close sesssion due to: " + e.getMessage(), e);
		}
	}

	/**
	 * Returns the {@link ATFXModelManager} used for this context.
	 * 
	 * @return the {@link ATFXModelManager}
	 */
	@Override
	public ODSCorbaModelManager getODSModelManager() {
		return modelManager;
	}

	/**
	 * Returns the {@link AoSession} used for this context.
	 * 
	 * @return {@link AoSession} used for this context.
	 */
	public AoSession getAoSession() {
		return modelManager.getAoSession();
	}

	/**
	 * Returns the ORB used for this context
	 * 
	 * @return ORB used for this context
	 */
	public ORB getORB() {
		return modelManager.getORB();
	}

	/**
	 * Returns a new {@link ATFXContext} with a new ODS co-session.
	 *
	 * @return The created {@code ODSContext} is returned.
	 * @throws OdsException Thrown on errors.
	 */
	@Override
	public ATFXContext newContext() throws OdsException {
		throw new OdsException("Method 'createCoSession' not implemented in openATFX");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.api.base.BaseApplicationContext#getFileService()
	 */
	@Override
	public Optional<FileService> getFileService(FileServiceType type) {
		switch (type) {
		// Only action needed is file "upload" to an ATFX file, which in all cases means
		// writing path information for an already downloaded file to an ATFX file,
		// hence no distinction between AOFILE and EXTREF necessary:
		case AOFILE:
		case EXTREF:
			return Optional.of(new ATFXFileService(this.atfxFile.getParentFile()));
		default:
			return Optional.empty();
		}
	}

	@Override
	public boolean isValid() {
		return false;
	}
}
