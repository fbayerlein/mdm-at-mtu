/*******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.mdm.api.atfxadapter.filetransfer;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.odsadapter.filetransfer.TracedInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author akn
 *
 */
public class ATFXFileService implements FileService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ATFXFileService.class);

	private static final int THREAD_POOL_SIZE = 5;

	private final Path parentDirectory;

	public ATFXFileService(File parentDirectory) {
		this.parentDirectory = parentDirectory.toPath();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#downloadSequential(org.eclipse.mdm.
	 * api.base.model.Entity, java.nio.file.Path, java.util.Collection,
	 * org.eclipse.mdm.api.base.file.FileService.ProgressListener)
	 */
	@Override
	public void downloadSequential(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		Map<String, List<FileLink>> groups = fileLinks.stream().filter(FileLink::isRemote)
				.collect(Collectors.groupingBy(FileLink::getRemotePath));

		long totalSize = calculateDownloadSize(groups);
		final AtomicLong transferred = new AtomicLong();
		LocalTime start = LocalTime.now();
		UUID id = UUID.randomUUID();
		LOGGER.debug("Sequential download of {} file(s) with id '{}' started.", groups.size(), id);
		for (List<FileLink> group : groups.values()) {
			FileLink fileLink = group.get(0);

			download(entity, target, fileLink, transaction, (b, p) -> {
				double tranferredBytes = transferred.addAndGet(b);
				if (progressListener != null) {
					progressListener.progress(b, (float) (tranferredBytes / totalSize));
				}
			});

			for (FileLink other : group.subList(1, group.size())) {
				other.setLocalPath(fileLink.getLocalPath());
			}
		}
		LOGGER.debug("Sequential download with id '{}' finished in {}.", id, Duration.between(start, LocalTime.now()));

	}

	private long calculateDownloadSize(Map<String, List<FileLink>> groups) {
		List<FileLink> links = groups.values().stream().map(l -> l.get(0)).collect(Collectors.toList());
		long totalSize = 0;
		for (FileLink fileLink : links) {
			File f = new File(fileLink.getRemotePath());
			// overflow may occur in case of total size exceeds 9223 PB!
			totalSize = Math.addExact(totalSize, f.length());
		}

		return totalSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#downloadParallel(org.eclipse.mdm.
	 * api.base.model.Entity, java.nio.file.Path, java.util.Collection,
	 * org.eclipse.mdm.api.base.file.FileService.ProgressListener)
	 */
	@Override
	public void downloadParallel(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#download(org.eclipse.mdm.api.base.
	 * model.Entity, java.nio.file.Path, org.eclipse.mdm.api.base.model.FileLink,
	 * org.eclipse.mdm.api.base.file.FileService.ProgressListener)
	 */
	@Override
	public void download(Entity entity, Path target, FileLink fileLink, Transaction transaction, ProgressListener progressListener)
			throws IOException {
		if (Files.exists(target)) {
			if (!Files.isDirectory(target)) {
				throw new IllegalArgumentException("Target path is not a directory.");
			}
		} else {
			Files.createDirectory(target);
		}

		try (InputStream inputStream = openStream(entity, fileLink, transaction)) {
			Path absolutePath = target.resolve(fileLink.getFileName()).toAbsolutePath();
			String remotePath = fileLink.getRemotePath();
			LOGGER.debug("Starting download of file '{}' to '{}'.", remotePath, absolutePath);
			LocalTime start = LocalTime.now();
			Files.copy(inputStream, absolutePath);
			LOGGER.debug("File '{}' successfully downloaded in {} to '{}'.", remotePath,
					Duration.between(start, LocalTime.now()), absolutePath);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#openStream(org.eclipse.mdm.api.base
	 * .model.Entity, org.eclipse.mdm.api.base.model.FileLink,
	 * org.eclipse.mdm.api.base.file.FileService.ProgressListener)
	 */
	@Override
	public InputStream openStream(Entity entity, FileLink fileLink, Transaction transaction, ProgressListener progressListener)
			throws IOException {

		return new FileInputStream(parentDirectory.resolve(fileLink.getRemotePath()).toFile());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#loadSize(org.eclipse.mdm.api.base.
	 * model.Entity, org.eclipse.mdm.api.base.model.FileLink)
	 */
	@Override
	public void loadSize(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#uploadSequential(org.eclipse.mdm.
	 * api.base.model.Entity, java.util.Collection,
	 * org.eclipse.mdm.api.base.file.FileService.ProgressListener)
	 */
	@Override
	public void uploadSequential(Entity entity, Collection<FileLink> fileLinks, Transaction transaction, ProgressListener progressListener)
			throws IOException {
		Map<Object, List<FileLink>> groups = fileLinks.stream().filter(FileLink::isLocal)
				.collect(Collectors.groupingBy(FileLink::getLocalPathOrStream));

		long totalSize = groups.values().stream().map(l -> l.get(0)).mapToLong(FileLink::getSize).sum();
		final AtomicLong transferred = new AtomicLong();
		LocalTime start = LocalTime.now();
		UUID id = UUID.randomUUID();
		LOGGER.debug("Sequential upload of {} file(s) with id '{}' started.", groups.size(), id);
		for (List<FileLink> group : groups.values()) {
			FileLink fileLink = group.get(0);

			upload(fileLink, (b, p) -> {
				double tranferredBytes = transferred.addAndGet(b);
				if (progressListener != null) {
					progressListener.progress(b, (float) (tranferredBytes / totalSize));
				}
			});

			for (FileLink other : group.subList(1, group.size())) {
				other.setRemotePath(fileLink.getRemotePath());
			}
		}
		LOGGER.debug("Sequential upload with id '{}' finished in {}.", id, Duration.between(start, LocalTime.now()));

	}

	/**
	 * Uploads given {@link FileLink}. The upload progress may be traced with a
	 * progress listener.
	 *
	 * @param fileLink         The {@code FileLink} to upload.
	 * @param progressListener The progress listener.
	 * @throws IOException Thrown if unable to upload file.
	 */
	private void upload(FileLink fileLink, ProgressListener progressListener) throws IOException {
		if (fileLink.isRemote()) {
			// nothing to do
			return;
		} else if (!fileLink.isLocal()) {
			throw new IllegalArgumentException("File link does not have a local path.");
		}

		try (InputStream sourceStream = getTracedStream(fileLink, progressListener)) {
			LOGGER.debug("Starting upload of file '{}'.", fileLink.getFileName());
			LocalTime start = LocalTime.now();
			
			String filename = fileLink.getFileName();
			Path remotePath = parentDirectory.resolve(filename);
			
			if (Files.exists(remotePath)) {
				// Use createTempFile to create a unique filename
//				String name = com.google.common.io.Files.getNameWithoutExtension(filename);
//				String extension = com.google.common.io.Files.getFileExtension(filename);
				
				String name = filename;
				String extension = "";
				int dotpos = filename.lastIndexOf('.');
				if (dotpos >= 0) {
					name = filename.substring(0, dotpos);
					extension = filename.substring(dotpos + 1);
				}

				Path path =  Files.createTempFile(parentDirectory, name, (dotpos >= 0 ? "." + extension : ""));

				fileLink.setRemotePath(path.getFileName().toString());
				Files.copy(sourceStream, remotePath, StandardCopyOption.REPLACE_EXISTING);
			} else {
				fileLink.setRemotePath(filename);
				Files.copy(sourceStream, remotePath);
			}

			LOGGER.debug("File '{}' successfully uploaded in {} to '{}'.", fileLink.getFileName(),
					Duration.between(start, LocalTime.now()), fileLink.getRemotePath());
		}
	}

	/**
	 * Retrieves an {@link InputStream} from a {@link FileLink} and wraps it into a
	 * {@link TracedInputStream} if a {@link ProgressListener} is available.
	 * 
	 * @param fileLink
	 * @param progressListener
	 * @return
	 * @throws FileNotFoundException
	 */
	private InputStream getTracedStream(FileLink fileLink, ProgressListener progressListener)
			throws FileNotFoundException {
		if (progressListener == null) {
			return fileLink.getInputStream();
		} else {
			return new TracedInputStream(fileLink.getInputStream(), progressListener, fileLink.getSize());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#uploadParallel(org.eclipse.mdm.api.
	 * base.model.Entity, java.util.Collection,
	 * org.eclipse.mdm.api.base.file.FileService.ProgressListener)
	 */
	@Override
	public void uploadParallel(Entity entity, Collection<FileLink> fileLinks, Transaction transaction, ProgressListener progressListener)
			throws IOException {
		Map<Object, List<FileLink>> groups = fileLinks.stream().filter(FileLink::isLocal)
				.collect(Collectors.groupingBy(FileLink::getLocalPathOrStream));

		long totalSize = groups.values().stream().map(l -> l.get(0)).mapToLong(FileLink::getSize).sum();
		final AtomicLong transferred = new AtomicLong();
		List<Callable<Void>> downloadTasks = new ArrayList<>();
		for (List<FileLink> group : groups.values()) {
			downloadTasks.add(() -> {
				FileLink fileLink = group.get(0);

				upload(fileLink, (b, p) -> {
					double tranferredBytes = transferred.addAndGet(b);
					if (progressListener != null) {
						progressListener.progress(b, (float) (tranferredBytes / totalSize));
					}
				});

				for (FileLink other : group.subList(1, group.size())) {
					other.setRemotePath(fileLink.getRemotePath());
				}

				return null;
			});
		}

		ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		LocalTime start = LocalTime.now();
		UUID id = UUID.randomUUID();
		LOGGER.debug("Parallel upload of {} file(s) with id '{}' started.", groups.size(), id);
		try {
			List<Throwable> errors = executorService.invokeAll(downloadTasks).stream().map(future -> {
				try {
					future.get();
					return null;
				} catch (ExecutionException | InterruptedException e) {
					LOGGER.error("Upload of failed due to: " + e.getMessage(), e);
					return e;
				}
			}).filter(Objects::nonNull).collect(Collectors.toList());

			if (!errors.isEmpty()) {
				throw new IOException("Upload faild for '" + errors.size() + "' files.");
			}
			LOGGER.debug("Parallel upload with id '{}' finished in {}.", id, Duration.between(start, LocalTime.now()));
		} catch (InterruptedException e) {
			throw new IOException("Unable to upload files due to: " + e.getMessage(), e);
		} finally {
			executorService.shutdown();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#delete(org.eclipse.mdm.api.base.
	 * model.Entity, java.util.Collection)
	 */
	@Override
	public void delete(Entity entity, Collection<FileLink> fileLinks, Transaction transaction) {
		fileLinks.stream().filter(FileLink::isRemote)
				.collect(groupingBy(FileLink::getRemotePath, reducing((fl1, fl2) -> fl1))).values().stream()
				.filter(Optional::isPresent).map(Optional::get).forEach(fl -> delete(entity, fl, transaction));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.api.base.file.FileService#delete(org.eclipse.mdm.api.base.
	 * model.Entity, org.eclipse.mdm.api.base.model.FileLink)
	 */
	@Override
	public void delete(Entity entity, FileLink fileLink, Transaction transaction) {
		if (!fileLink.isRemote()) {
			// nothing to do
			return;
		}

		try {
			Files.delete(Paths.get(fileLink.getRemotePath()));
			LOGGER.debug("File '{}' sucessfully deleted.", fileLink.getRemotePath());
		} catch (IOException e) {
			LOGGER.warn("Failed to delete remote file.", e);
		}
	}
}
