/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.api.atfxadapter.util;

public class PatternUtil {

	/**
	 * 
	 * @param pattern
	 * @return true if the pattern contains an unescaped wildcard character
	 */
	public static boolean containsUnescapedWildcard(String pattern) {
		boolean foundBackslash = false;

		for (char c : pattern.toCharArray()) {
			if ((c == '?' || c == '*') && !foundBackslash) {
				return true;
			}

			if (c == '\\') {
				foundBackslash = !foundBackslash;
			} else {
				foundBackslash = false;
			}
		}
		return false;
	}
}
