/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.query;

/**
 * Thrown to indicate an adapter has lost its connection to the data source.
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 */
public class ConnectionLostException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2947083638693286306L;

	/**
	 * @param message
	 * @param cause
	 */
	public ConnectionLostException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ConnectionLostException(String message) {
		super(message);
	}

}
