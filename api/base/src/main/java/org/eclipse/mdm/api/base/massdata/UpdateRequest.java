/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;

/**
 * Builds measured values update request configurations.
 *
 * @since 5.2.0
 * @author Joachim Zeyn, Peak Solution GmbH
 */
public final class UpdateRequest {

	// ======================================================================
	// Instance variables
	// ======================================================================
	private final String instanceID;
	private final ChannelGroup channelGroup;
	private final Channel channel;

	private AxisType axisType;
	private boolean independent;
	private short[] flags;
	private boolean allValid;
	private Short globalFlag;

	private String mimeType = "application/x-asam.aolocalcolumn";

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param instanceID The Id for this request.
	 */
	public UpdateRequest(ChannelGroup channelGroup, Channel channel, String instanceID) {
		this.channelGroup = channelGroup;
		this.channel = channel;
		this.instanceID = instanceID;
	}

	public ChannelGroup getChannelGroup() {
		return channelGroup;
	}

	public Channel getChannel() {
		return channel;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public AxisType getAxisType() {
		return axisType;
	}

	public void setAxisType(AxisType axisType) {
		this.axisType = axisType;
	}

	public boolean isIndependent() {
		return independent;
	}

	public void setIndependent(boolean independent) {
		this.independent = independent;
	}

	public short[] getFlags() {
		return flags;
	}

	public void setFlags(short[] flags) {
		this.flags = flags;
		if (this.flags == null || this.flags.length == 0) {
			this.allValid = true;
		} else {
			this.allValid = false;
		}
	}

	public boolean areAllValid() {
		return allValid;
	}

	public Short getGlobalFlag() {
		return globalFlag;
	}

	public void setGlobalFlag(Short globalFlag) {
		this.globalFlag = globalFlag;
	}

	public String getInstanceID() {
		return instanceID;
	}

}
