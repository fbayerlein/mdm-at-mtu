/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.dflt.EntityManager;

/**
 * Implementation of the local column entity type.
 * <p>
 * Introduced for use as the result type in {@link QueryRequest}s where the
 * result set should be aggregated by LocalColumn IDs.
 * <p>
 * Avoid using this Entity in situations where all attribute values of an entity
 * instance are retrieved from the data source (for example the load methods of
 * {@link EntityManager}) as this would include the (potentially large) data
 * sets for the Values and Flags values.
 *
 * @author Martin Fleischer, Peak Solution GmbH
 */
public class LocalColumn extends BaseEntity implements Deletable {

	/**
	 * The 'IndependentFlag' attribute name.
	 */
	public static final String ATTR_INDEPENDENT_FLAG = "IndependentFlag";

	/**
	 * The 'SequenceRepresentation' attribute name.
	 */
	public static final String ATTR_SEQUENCE_REPRESENTATION = "SequenceRepresentation";

	/**
	 * The 'GlobalFlag' attribute name.
	 */
	public static final String ATTR_GLOBAL_FLAG = "GlobalFlag";

	/**
	 * The 'Values' attribute name.
	 */
	public static final String ATTR_VALUES = "Values";

	/**
	 * The 'Flags' attribute name.
	 */
	public static final String ATTR_FLAGS = "Flags";

	/**
	 * The 'GenerationParameters' attribute name.
	 */
	public static final String ATTR_GENERATION_PARAMETERS = "GenerationParameters";

	/**
	 * The 'RawDatatype' attribute name.
	 */
	public static final String ATTR_RAW_DATATYPE = "RawDatatype";

	/**
	 * The 'axistype' attribute name.
	 */
	public static final String ATTR_AXISTYPE = "axistype";

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	LocalColumn(Core core) {
		super(core);
	}
}
