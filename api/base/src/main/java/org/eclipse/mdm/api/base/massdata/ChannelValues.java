/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import java.util.Arrays;

import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;

public class ChannelValues {

	public String channelId;
	public String name;
	public String channelGroupId;
	public Value values;
	public short[] flags = new short[0];
	public double[] genParameters = new double[0];

	public ChannelValues(String channelId, String name, String channelGroupId, Value values) {
		this(channelId, name, channelGroupId, values, null, null);
	}

	public ChannelValues(String channelId, String name, String channelGroupId, Value values, Value flags,
			Value genParameters) {
		this.channelId = channelId;
		this.channelGroupId = channelGroupId;
		this.name = name;
		this.values = values;
		if (flags != null) {
			this.flags = flags.extract(ValueType.SHORT_SEQUENCE);
		}
		if (genParameters != null) {
			this.genParameters = genParameters.extract(ValueType.DOUBLE_SEQUENCE);
		}
	}

	public String getChannelId() {
		return channelId;
	}

	public String getName() {
		return name;
	}

	public String getChannelGroupId() {
		return channelGroupId;
	}

	public Value getValues() {
		return values;
	}

	public short[] getFlags() {
		return flags;
	}

	public double[] getGenParameters() {
		return genParameters;
	}

	public boolean isValid(int index) {
		if (flags != null && index < flags.length) {
			return (flags[index] & 0x1) == 0x1;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return new StringBuilder().append(ChannelValues.class).append("[channelId=").append(channelId).append(", name=")
				.append(name).append(", values=").append(values).append(", flags=").append(Arrays.toString(flags))
				.append("]").append(", genParameters=").append(Arrays.toString(genParameters)).append("]").toString();
	}

}
