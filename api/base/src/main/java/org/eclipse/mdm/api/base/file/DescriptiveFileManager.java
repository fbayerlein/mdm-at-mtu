/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.file;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.DescriptiveFilesAttachable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.dflt.EntityManager;

/**
 * Class providing auxiliary methods for managing {@link DescriptiveFile}s.
 * 
 * @since 5.3.0
 * @author Martin Fleischer, Peak Solution GmbH
 *
 */
public class DescriptiveFileManager {
	private final ModelManager modelManager;

	public DescriptiveFileManager(ModelManager modelManager) {
		this.modelManager = modelManager;
	}

	public List<String> getDescriptiveFileRelationNames(DescriptiveFilesAttachable descriptiveFilesAttachable) {
		if (modelManager.listEntityTypes().stream().filter(et -> DescriptiveFile.TYPE_NAME.equals(et.getName()))
				.findFirst().isPresent()) {
			String descriptiveFileTypeName = modelManager.getEntityType(DescriptiveFile.class).getName();

			return modelManager.getEntityType(descriptiveFilesAttachable.getTypeName()).getRelations().stream()
					.filter(r -> (r.getTarget().getName().equals(descriptiveFileTypeName) && r.isNtoM()))
					.map(r -> r.getName()).collect(Collectors.toList());
		} else {
			return Collections.emptyList();
		}
	}

	public Relation getDescriptiveFileRelation(DescriptiveFilesAttachable descriptiveFilesAttachable,
			String relationName) {
		EntityType descriptiveFileType = modelManager.getEntityType(DescriptiveFile.class);

		return modelManager.getEntityType(descriptiveFilesAttachable.getTypeName()).getRelations().stream()
				.filter(r -> r.getName().equals(relationName) && r.isNtoM()
						&& r.getTarget().getName().equals(descriptiveFileType.getName()))
				.findFirst()
				.orElseThrow(() -> new IllegalStateException("Descriptive file relation with name '" + relationName
						+ "' does not exist at application element " + descriptiveFilesAttachable.getTypeName()));
	}

	public boolean hasAdditionalRelatedInstances(EntityManager entityManager, DescriptiveFile descriptiveFile,
			Relation relation) {
		return modelManager.getEntityType(DescriptiveFile.class).getRelations().stream()
				.filter(r -> (r.isNtoM() && (entityManager.countRelatedEntities((Entity) descriptiveFile,
						r.getName()) > (r.getName().equals(relation.getInverseName()) ? 1 : 0))))
				.findFirst().isPresent();

	}

}
