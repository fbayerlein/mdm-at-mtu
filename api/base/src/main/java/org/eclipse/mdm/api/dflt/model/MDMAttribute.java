/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Describable;

/**
 * Implementation of an external system entity type. An external system
 * attribute contains several mdm attributes.
 *
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
public class MDMAttribute extends BaseEntity implements Describable, Deletable {

	public static final String ATTR_COMPONENT_TYPE = "CompType";
	public static final String ATTR_COMPONENT_NAME = "CompName";
	public static final String ATTR_ATTRIBUTE_NAME = "AttrName";

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	protected MDMAttribute(Core core) {
		super(core);
	}

	/**
	 * Returns the component type for this attribute
	 *
	 * @return Returns the component type
	 */
	public String getComponentType() {
		return getValue(ATTR_COMPONENT_TYPE).extract();
	}

	/**
	 * Sets the component type for this attribute
	 *
	 * @param compType The component type
	 */
	public void setComponentType(String compType) {
		getValue(ATTR_COMPONENT_TYPE).set(compType);
	}

	/**
	 * Returns the component name for this attribute
	 *
	 * @return Returns the component name
	 */
	public String getComponentName() {
		return getValue(ATTR_COMPONENT_NAME).extract();
	}

	/**
	 * Sets the component name for this attribute
	 *
	 * @param compName The component name
	 */
	public void setComponentName(String compName) {
		getValue(ATTR_COMPONENT_NAME).set(compName);
	}

	/**
	 * Returns the attribute name for this attribute
	 *
	 * @return Returns the attribute name
	 */
	public String getAttributeName() {
		return getValue(ATTR_ATTRIBUTE_NAME).extract();
	}

	/**
	 * Sets the attribute name for this attribute
	 *
	 * @param attrName The attribute name
	 */
	public void setAttributeName(String attrName) {
		getValue(ATTR_ATTRIBUTE_NAME).set(attrName);
	}

}
