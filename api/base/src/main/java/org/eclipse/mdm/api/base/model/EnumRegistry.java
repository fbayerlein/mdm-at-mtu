/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This singleton class registers globally available enumerations.
 *
 */
public final class EnumRegistry {

	// singleton instance
	private static final EnumRegistry instance = new EnumRegistry();

	public static final String SCALAR_TYPE = "ScalarType";
	public static final String INTERPOLATION = "Interpolation";
	public static final String SEQUENCE_REPRESENTATION = "SequenceRepresentation";
	public static final String TYPE_SPECIFICATION = "TypeSpecification";
	public static final String VALUE_TYPE = "ValueType";
	public static final String VERSION_STATE = "VersionState";
	public static final String AXIS_TYPE = "AxisType";

	private Map<String, Map<String, Enumeration<? extends EnumerationValue>>> enumerations = new HashMap<>();

	/**
	 * Constructor. Not called directly. Use getInstance() instead.
	 */
	private EnumRegistry() {

	}

	private Map<String, Enumeration<? extends EnumerationValue>> getDefaults(String sourceName) {
		Map<String, Enumeration<? extends EnumerationValue>> defaults = new HashMap<>();
		defaults.put(INTERPOLATION, new Enumeration<Interpolation>(sourceName, Interpolation.class, INTERPOLATION));
		defaults.put(SCALAR_TYPE, new Enumeration<ScalarType>(sourceName, ScalarType.class, SCALAR_TYPE));
		defaults.put(SEQUENCE_REPRESENTATION, new Enumeration<SequenceRepresentation>(sourceName,
				SequenceRepresentation.class, SEQUENCE_REPRESENTATION));
		defaults.put(TYPE_SPECIFICATION,
				new Enumeration<TypeSpecification>(sourceName, TypeSpecification.class, TYPE_SPECIFICATION));
		defaults.put(VALUE_TYPE, new Enumeration<ValueType>(sourceName, ValueType.class, VALUE_TYPE));
		defaults.put(VERSION_STATE, new Enumeration<VersionState>(sourceName, VersionState.class, VERSION_STATE));
		defaults.put(AXIS_TYPE, new Enumeration<AxisType>(sourceName, AxisType.class, AXIS_TYPE));
		return defaults;
	}

	/**
	 * adds a new enumeration to the registry
	 * 
	 * @param enumeration the dynamic enumeration object
	 */
	public void add(Enumeration<? extends EnumerationValue> enumeration) {
		Map<String, Enumeration<? extends EnumerationValue>> map = enumerations
				.computeIfAbsent(enumeration.getSourceName(), key -> getDefaults(key));
		map.put(enumeration.getName(), enumeration);
	}

	/**
	 * @param name the name of the registered enumeration
	 * @return the dynamic enumeration object
	 */
	public Enumeration<?> get(String sourceName, String name) {
		return enumerations.computeIfAbsent(sourceName, key -> getDefaults(key)).get(name);
	}

	/**
	 * 
	 * @return a collection of all registered enumerations
	 */
	public Collection<Enumeration<?>> list(String sourceName) {
		return enumerations.computeIfAbsent(sourceName, key -> getDefaults(key)).values();
	}

	/**
	 * @return the instance of this singleton
	 */
	public static EnumRegistry getInstance() {
		return instance;
	}
}
