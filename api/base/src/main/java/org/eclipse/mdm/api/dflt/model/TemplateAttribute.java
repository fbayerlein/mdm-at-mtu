/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.eclipse.mdm.api.base.BaseEntityManager;
import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.DescriptiveFilesAttachable;
import org.eclipse.mdm.api.base.model.DoubleComplex;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.FilesAttachable;
import org.eclipse.mdm.api.base.model.FloatComplex;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;

/**
 * Implementation of the template attribute entity type. A template attribute
 * adds meta data to a {@link CatalogAttribute} it is associated with. It always
 * belongs to a {@link TemplateComponent} or a {@link TemplateSensor}. Its name
 * is the same as the name of the associated {@code CatalogAttribute} and is not
 * allowed to be modified at all.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 * @see TemplateComponent
 * @see TemplateSensor
 */
public class TemplateAttribute extends BaseEntity
		implements Deletable, FilesAttachable<DescriptiveFile>, DescriptiveFilesAttachable {

	// ======================================================================
	// Class variables
	// ======================================================================

	/**
	 * This {@code Comparator} compares {@link TemplateAttribute}s by the sort index
	 * of their corresponding {@link CatalogAttribute} in ascending order.
	 */
	public static final Comparator<TemplateAttribute> COMPARATOR = Comparator
			.comparing(ta -> ta.getCatalogAttribute().getSortIndex());

	/**
	 * The 'DefaultValue' attribute name.
	 */
	public static final String ATTR_DEFAULT_VALUE = "DefaultValue";

	/**
	 * The 'DefaultValue' attribute name.
	 */
	public static final String ATTR_DEFAULT_VALUES = "DefaultValues";

	/**
	 * The 'DefaultFiles' attribute name.
	 */
	public static final String ATTR_DEFAULT_FILES = "DefaultFiles";

	/**
	 * The 'ValueReadOnly' attribute name.
	 */
	public static final String ATTR_VALUE_READONLY = "ValueReadonly";

	/**
	 * The 'Optional' attribute name.
	 */
	public static final String ATTR_OPTIONAL = "Obligatory";

	private static final Map<ValueType<?>, Function<String, Object>> VALUETYPE_FUNCTION_MAP = new HashMap<>();

	static {
		VALUETYPE_FUNCTION_MAP.put(ValueType.STRING, v -> v);
		VALUETYPE_FUNCTION_MAP.put(ValueType.DATE, v -> {
			LocalDateTime parse = LocalDateTime.parse(v, Value.LOCAL_DATE_TIME_FORMATTER);
			return parse.toInstant(ZoneOffset.UTC);
		});
		VALUETYPE_FUNCTION_MAP.put(ValueType.BOOLEAN, Boolean::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.BYTE, Byte::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.SHORT, Short::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.INTEGER, Integer::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.LONG, Long::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.FLOAT, Float::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.DOUBLE, Double::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.FLOAT_COMPLEX, FloatComplex::valueOf);
		VALUETYPE_FUNCTION_MAP.put(ValueType.DOUBLE_COMPLEX, DoubleComplex::valueOf);
	}

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	TemplateAttribute(Core core) {
		super(core);
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Returns the default {@link Value} of this template attribute.
	 *
	 * @return The default {@code Value} is returned.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Value getDefaultValue() {
		ValueType valueType = getCatalogAttribute().getValueType();
		if (valueType.isSequence()) {
			Value defaultValue = getValue(ATTR_DEFAULT_VALUES, true);
			if (defaultValue == null) {
				if (valueType.isEnumerationSequence()) {
					return valueType.create(getCatalogAttribute().getEnumerationObject(), getName());
				} else {
					return valueType.create(ATTR_DEFAULT_VALUES, null);
				}
			}
			boolean isValid = defaultValue.isValid();
			String[] value = defaultValue.extract();
			return valueType.create(getName(), isValid ? parse(Arrays.asList(value), valueType) : null);
		} else {

			Value defaultValue = getValue(ATTR_DEFAULT_VALUE);
			boolean isValid = defaultValue.isValid();
			String value = defaultValue.extract();
			if (valueType.isEnumeration()) {
				Value enumValue = valueType.create(getCatalogAttribute().getEnumerationObject(), getName());

				if (value != null && !value.trim().equals("")) {
					enumValue.set(getCatalogAttribute().getEnumerationObject().valueOf(Integer.valueOf(value)));
				}

				return enumValue;
			} else {
				return valueType.create(getName(), isValid ? parse(Collections.singletonList(value), valueType) : null);
			}
		}
	}

	/**
	 * Sets a new default value for this template attribute. Given input will be
	 * stored in its {@link String} representation.
	 *
	 * @param input The new default value.
	 */
	public void setDefaultValue(Object input) {
		ValueType<?> valueType = getCatalogAttribute().getValueType();

		if (input == null) {
			getValue(ATTR_DEFAULT_VALUE).set(null);
			return;
		}

		// if this passes -> input is valid
		Value value = valueType.create("notRelevant", input);

		List<String> stringValues = new ArrayList<>();
		if (valueType.isFileLinkType()) {
			FileLink[] values = valueType.isSequence() ? value.extract() : new FileLink[] { value.extract() };

			for (FileLink fl : values) {
				stringValues.add(getStringValueForFileLink(fl));
			}

		} else if (valueType.isDateType()) {
			Instant[] values = valueType.isSequence() ? value.extract() : new Instant[] { value.extract() };

			for (Instant ldt : values) {
				stringValues.add(Value.LOCAL_DATE_TIME_FORMATTER.format(ldt));
			}

		} else {
			List<Object> vals = value.extract();
			vals.forEach(o -> stringValues.add(o.toString()));
		}

		if (valueType.isSequence()) {
			getValue(ATTR_DEFAULT_VALUES).set(stringValues);
		} else if (stringValues.isEmpty()) {
			getValue(ATTR_DEFAULT_VALUE).set(null);
		} else {
			getValue(ATTR_DEFAULT_VALUE).set(stringValues.get(0));
		}

	}

	/**
	 * Returns the value read only flag of this template attribute.
	 *
	 * @return Returns {@code true} if it is not allowed to modify {@link Value}s
	 *         derived from this template attribute.
	 */
	public Boolean isValueReadOnly() {
		return getValue(ATTR_VALUE_READONLY).extract();
	}

	/**
	 * Sets a new value read only flag for this template attribute.
	 *
	 * @param valueReadOnly The new value read only flag.
	 */
	public void setValueReadOnly(Boolean valueReadOnly) {
		getValue(ATTR_VALUE_READONLY).set(valueReadOnly);
	}

	/**
	 * Returns the optional flag of this template attribute.
	 *
	 * @return Returns {@code true} if it is allowed to omit a {@link Value} derived
	 *         from this template attribute.
	 */
	public Boolean isOptional() {
		boolean mandatory = getValue(ATTR_OPTIONAL).extract();
		return mandatory ? Boolean.FALSE : Boolean.TRUE;
	}

	/**
	 * Sets a new optional flag for this template attribute.
	 *
	 * @param optional The new optional flag.
	 */
	public void setOptional(Boolean optional) {
		getValue(ATTR_OPTIONAL).set(optional ? Boolean.FALSE : Boolean.TRUE);
	}

	/**
	 * Returns the {@link CatalogAttribute} this template attribute is associated
	 * with.
	 *
	 * @return The associated {@code CatalogAttribute} is returned.
	 */
	public CatalogAttribute getCatalogAttribute() {
		return getCore().getMutableStore().get(CatalogAttribute.class);
	}

	/**
	 * Returns the {@link TemplateRoot} this template attribute belongs to.
	 *
	 * @return The {@code TemplateRoot} is returned.
	 */
	public TemplateRoot getTemplateRoot() {
		Optional<TemplateComponent> templateComponent = getTemplateComponent();
		Optional<TemplateSensor> templateSensor = getTemplateSensor();
		if (templateComponent.isPresent()) {
			return templateComponent.get().getTemplateRoot();
		} else if (templateSensor.isPresent()) {
			return templateSensor.get().getTemplateRoot();
		} else {
			throw new IllegalStateException("Parent entity is unknown.");
		}
	}

	/**
	 * Returns the parent {@link TemplateComponent} of this template attribute.
	 *
	 * @return {@code Optional} is empty if a {@link TemplateSensor} is parent of
	 *         this template attribute.
	 * @see #getTemplateSensor()
	 */
	public Optional<TemplateComponent> getTemplateComponent() {
		return Optional.ofNullable(getCore().getPermanentStore().get(TemplateComponent.class));
	}

	/**
	 * Returns the parent {@link TemplateSensor} of this template attribute.
	 *
	 * @return {@code Optional} is empty if a {@link TemplateComponent} is parent of
	 *         this template attribute.
	 * @see #getTemplateComponent()
	 */
	public Optional<TemplateSensor> getTemplateSensor() {
		return Optional.ofNullable(getCore().getPermanentStore().get(TemplateSensor.class));
	}

	// ======================================================================
	// Private methods
	// ======================================================================

	@SuppressWarnings({ "rawtypes" })
	private Object parse(List<String> valuesString, ValueType<?> valueType) {

		Object returnVal = null;

		if (valueType.isFileLinkType()) {
			List<FileLink> values = new ArrayList<>();
			valuesString.forEach(s -> values.add(FileLinkParser.parse(s)));
			FileLink[] resultArray = values.toArray(new FileLink[values.size()]);

			if (valueType.isSequence()) {
				returnVal = resultArray;
			} else if (resultArray.length == 1) {
				returnVal = resultArray[0];
			} else {
				throw new IllegalStateException("DefaultValue of a single valuetype can only persist 1 velue!");
			}

		} else if (valueType.isEnumeration()) {
			Enumeration enumObject = getCatalogAttribute().getEnumerationObject();

			List<EnumerationValue> enumValues = new ArrayList<>();
			valuesString.forEach(s -> enumValues.add(enumObject.valueOf(s)));

			if (valueType.isSequence()) {
				returnVal = valueType.create(getName(), "", true, enumValues, enumObject.getName());
			} else if (enumValues.size() == 1) {
				returnVal = valueType.create(getName(), "", true, enumValues.get(0), enumObject.getName());
			} else {
				throw new IllegalStateException("DefaultValue of a single valuetype can only persist 1 velue!");
			}
		} else {
			List<Object> values = new ArrayList<>();
			Function<String, Object> converter = getParser(valueType.toSingleType());
			valuesString.forEach(s -> values.add(converter.apply(s)));
			Object[] resultArray = values.toArray(new Object[values.size()]);

			if (valueType.isSequence()) {
				Object array = Array.newInstance(valueType.getValueClass().getComponentType(), values.size());

				if (valueType.getValueClass().getComponentType().isPrimitive()) {
					IntStream.range(0, values.size()).forEach(i -> Array.set(array, i, values.get(i)));
				} else {
					values.toArray((Object[]) array);
				}

				returnVal = array;
			} else if (resultArray.length == 1) {
				returnVal = resultArray[0];
			} else {
				throw new IllegalStateException("DefaultValue of a single valuetype can only persist 1 velue!");
			}
		}

		return returnVal;

	}

	/**
	 * Returns the {@code String} conversion function for given {@link ValueType}.
	 *
	 * @param valueType Used as identifier.
	 * @return The {@code String} conversion {@code Function} is returned.
	 * @throws IllegalArgumentException Thrown if a corresponding {@code String} is
	 *                                  not supported.
	 */
	private static Function<String, Object> getParser(ValueType<?> valueType) {
		Function<String, Object> converter = VALUETYPE_FUNCTION_MAP.get(valueType);
		if (converter == null) {
			throw new IllegalArgumentException("String conversion for value type '" + valueType + "' not supported.");
		}
		return converter;
	}

	// ======================================================================
	// Inner classes
	// ======================================================================

	/**
	 * Utility class restore a {@link FileLink} from given {@code String}.
	 */
	private static final class FileLinkParser {

		// ======================================================================
		// Class variables
		// ======================================================================

		// markers
		private static final String NO_DESC_MARKER = "NO_DESC#";
		private static final String LOCAL_MARKER = "LOCALPATH#";

		// pattern group names
		private static final String DESCRIPTION = "description";
		private static final String MIMETYPE = "mimetype";
		private static final String PATH = "path";

		// pattern
		private static final Pattern FILE_LINK_PATTERN = Pattern
				.compile("(?<" + DESCRIPTION + ">.*?)\\[(?<" + MIMETYPE + ">.*?),(?<" + PATH + ">.*?)\\]");

		// ======================================================================
		// Public methods
		// ======================================================================

		/**
		 * Parses given {@code String} and returns it as {@link FileLink}.
		 *
		 * @param value The {@code String} value which will be parsed.
		 * @return The corresponding {@code FileLink} is returned.
		 */
		public static FileLink parse(String value) {
			Matcher matcher = FILE_LINK_PATTERN.matcher(value);
			if (!matcher.find()) {
				throw new IllegalStateException("Unable to restore file link.");
			}
			String description = matcher.group(DESCRIPTION);
			String path = matcher.group(PATH);
			FileLink fileLink;
			if (path.startsWith(LOCAL_MARKER)) {
				try {
					fileLink = FileLink.newLocal(Paths.get(path.replaceFirst(LOCAL_MARKER, "")), null,
							FileServiceType.EXTREF);
				} catch (IOException e) {
					throw new IllegalStateException("Unable to restore local file link.", e);
				}
			} else {
				fileLink = FileLink.newRemote(path, new MimeType(matcher.group(MIMETYPE)), description, -1L, null,
						FileServiceType.EXTREF);
			}

			fileLink.setDescription(NO_DESC_MARKER.equals(description) ? null : description);
			return fileLink;
		}

	}

	/**
	 * Get string value for FileLinkType
	 *
	 * @param value    The {@code Value} value.
	 * @param sequence The {@code boolean} sequence.
	 * @return Return {@code String} string value for FileLinkType
	 */
	private String getStringValueForFileLink(FileLink fl) {

		StringBuilder sb = new StringBuilder();
		if (fl.getDescription().isEmpty()) {
			sb.append(FileLinkParser.NO_DESC_MARKER);
		} else {
			sb.append(fl.getDescription());
		}
		sb.append('[').append(fl.getMimeType()).append(',');
		if (fl.isRemote()) {
			sb.append(fl.getRemotePath());
		} else if (fl.isLocalPath()) {
			sb.append(FileLinkParser.LOCAL_MARKER).append(fl.getLocalPath());
		} else {
			throw new IllegalStateException("File link is neither in local nor remote state: " + fl);
		}

		sb.append(']');

		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see: org.eclipse.mdm.api.base.model.FilesAttachable#getFileLinks()
	 */
	@Override
	public FileLink[] getFileLinks() {

		FileLink[] retVal = new FileLink[0];

		ValueType<?> vt = getCatalogAttribute().getValueType();

		if (vt.isFileLinkType()) {

			String stringValue = getValue(ATTR_DEFAULT_VALUE).extract();

			if (stringValue != null && !stringValue.isEmpty()) {

				Object rawValue = getDefaultValue().extract();

				if (rawValue != null) {

					if (vt.isSequence()) {
						retVal = ((FileLink[]) getDefaultValue().extract()).clone();
					} else {
						retVal = new FileLink[] { (FileLink) getDefaultValue().extract() };
					}
				}
			}
		}

		return retVal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see: org.eclipse.mdm.api.base.model.FilesAttachable#setFileLinks(FileLink[])
	 */
	@Override
	public void setFileLinks(FileLink[] fileLinks) {

		ValueType<?> vt = getCatalogAttribute().getValueType();

		if (vt.isFileLinkType()) {

			if (vt.isSequence()) {
				setDefaultValue((fileLinks == null || fileLinks.length == 0) ? null : fileLinks);
			} else {
				setDefaultValue((fileLinks == null || fileLinks.length == 0) ? null : fileLinks[0]);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see: org.eclipse.mdm.api.base.model.FilesAttachable#addFileLink(FileLink)
	 */
	@Override
	public void addFileLink(FileLink fileLink) {

		if (getCatalogAttribute().getValueType().isFileLinkType()) {

			FilesAttachable.super.addFileLink(fileLink);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see: org.eclipse.mdm.api.base.model.FilesAttachable#removeFileLink(FileLink)
	 */
	@Override
	public void removeFileLink(FileLink fileLink) {

		if (getCatalogAttribute().getValueType().isFileLinkType()) {

			FilesAttachable.super.removeFileLink(fileLink);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DescriptiveFile> getMDMFiles(BaseEntityManager manager) {

		return manager.loadRelatedEntities(this, ATTR_DEFAULT_FILES, DescriptiveFile.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeMDMFile(DescriptiveFile descriptiveFile) {
		// "TplUnitUnderTestAttr", "TplTestSequenceAttr", "TplTestEquipmentAttr",
		// "TplSensorAttr"
		getCore().getNtoMStore().remove(TemplateAttribute.ATTR_DEFAULT_FILES, descriptiveFile);
		BaseEntity.getCore(descriptiveFile).getNtoMStore().remove(getCore().getTypeName(), this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addDescriptiveFile(Relation relation, DescriptiveFile descriptiveFile) {
		getCore().getNtoMStore().add(relation.getName(), descriptiveFile);
		BaseEntity.getCore(descriptiveFile).getNtoMStore().add(relation.getInverseName(), this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DescriptiveFile> getDescriptiveFiles(Relation relation, BaseEntityManager manager) {
		return getCore().getNtoMStore().get(relation.getName());
//		return manager.loadRelatedEntities(this, relation.getName(), DescriptiveFile.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeDescriptiveFile(Relation relation, DescriptiveFile descriptiveFile) {
		getCore().getNtoMStore().remove(relation.getName(), descriptiveFile);
		BaseEntity.getCore(descriptiveFile).getNtoMStore().remove(relation.getInverseName(), this);
	}
}
