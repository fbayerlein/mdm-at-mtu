/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.model.Deletable;

/**
 * Holds related entities of any kind and keeps track of modifications.
 */
public final class RelationStore {

	private final Map<String, List<? extends Deletable>> added = new HashMap<>(0);
	private final Map<String, List<? extends Deletable>> removed = new HashMap<>(0);

	/**
	 * Returns current set of related children mapped by their type.
	 *
	 * @return Returned {@code Map} is unmodifiable.
	 */
	public Map<String, List<? extends Deletable>> getAdded() {
		return Collections.unmodifiableMap(added);
	}

	/**
	 * Returns current set of removed related children mapped by their type.
	 *
	 * @return Returned {@code Map} is unmodifiable.
	 */
	public Map<String, List<? extends Deletable>> getRemoved() {
		return Collections.unmodifiableMap(removed);
	}

	/**
	 * Returns related child entities of given type.
	 *
	 * @param <T>         Desired entity type.
	 * @param entityClass Used as identifier.
	 * @return Returned {@code List} is unmodifiable.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Deletable> List<T> get(String relation) {
		return Collections.unmodifiableList((List<T>) added.computeIfAbsent(relation, k -> new ArrayList<>()));
	}

	/**
	 * Sorts the child entities with given {@code Comparator}.
	 *
	 * @param <T>         Desired entity type.
	 * @param entityClass Used as identifier.
	 * @param comparator  Used for sorting.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Deletable> void sort(String relation, Comparator<? super T> comparator) {
		List<T> children = (List<T>) added.get(relation);
		if (children != null) {
			children.sort(comparator);
		}
	}

	/**
	 * Adds given child entity.
	 *
	 * @param child The new child.
	 */
	@SuppressWarnings("unchecked")
	public void add(String relation, Deletable relatedEntity) {
		removed.getOrDefault(relation, new ArrayList<>())
				.removeIf(e -> e == relatedEntity || e.getID().equals(relatedEntity.getID()));
		((List<Deletable>) added.computeIfAbsent(relation, k -> new ArrayList<>())).add(relatedEntity);
	}

	/**
	 * Removes given child entity.
	 *
	 * @param relatedEntity The child which will be removed.
	 */
	@SuppressWarnings("unchecked")
	public void remove(String relation, Deletable relatedEntity) {
		added.getOrDefault(relation, new ArrayList<>())
				.removeIf(e -> e == relatedEntity || e.getID().equals(relatedEntity.getID()));
		((List<Deletable>) removed.computeIfAbsent(relation, k -> new ArrayList<>())).add(relatedEntity);
	}

	/**
	 * Clean up list of removed entities.
	 */
	void apply() {
		removed.clear();
	}

}