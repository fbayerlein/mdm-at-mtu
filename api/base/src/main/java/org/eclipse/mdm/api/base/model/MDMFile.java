/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.query.DataAccessException;

public abstract class MDMFile extends BaseEntity implements Deletable, Describable {

	public static final String ATTR_SIZE = "Size";
	public static final String ATTR_LOCATION = "Location";
	public static final String ATTR_ORIG_FILE_NAME = "OriginalFileName";
	public static final String ATTR_ORIG_FILE_DATE = "OriginalFileDate";
	public static final String ATTR_FILE_MIMETYPE = "FileMimeType";
	public static final String ATTR_FILE_CHECKSUM = "FileChecksum";
	public static final String ATTR_FILE_CHECKSUM_TYPE = "FileChecksumType";

	public static final String HASH_ALGORITHM_SHA256 = "SHA-256";

	// ======================================================================
	// Class variables
	// ======================================================================

	private FileLink fileLink;

	// ======================================================================
	// Constructors
	// ======================================================================

	protected MDMFile(Core core) {
		super(core);

		setMimeType(new MimeType("application/x-asam.aofile"));
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Returns the {@link FileLink} set by {@link MDMFile#setFileLink(FileLink)} or
	 * creates a new remote {@link FileLink} if none was set.
	 * 
	 * @return
	 */
	public FileLink getFileLink() {
		if (fileLink == null) {
			return FileLink.newRemote(getLocation(), new MimeType(getFileMimeType()), getDescription(), getSize(), this,
					FileServiceType.AOFILE);
		} else {
			return fileLink;
		}
	}

	/**
	 * Sets a {@link FileLink} for this {@link MDMFile}. The remoteObject, name,
	 * OriginalFileName, Description and FileMimeType of the given {@link FileLink}
	 * is updated <code>this</code>.
	 * 
	 * @param fileLink
	 */
	public void setFileLink(FileLink fileLink) {
		if (fileLink.getFileName() == null || fileLink.getFileName().isEmpty()) {
			setName(getTypeName());
		} else {
			setName(fileLink.getFileName());
		}
		setOriginalFileName(fileLink.getFileName());
		setDescription(fileLink.getDescription());
		setFileMimeType(fileLink.getMimeType() == null ? "" : fileLink.getMimeType().toString());

		this.fileLink = fileLink;
		this.fileLink.setRemoteObject(this);
	}

	/**
	 * Returns the Size of this entity.
	 *
	 * @return The Size is returned.
	 */
	public long getSize() {
		return getValue(ATTR_SIZE).extract();
	}

	/**
	 * Sets new Size for this entity.
	 *
	 * @param size The new Size.
	 */
	public void setSize(long size) {
		getValue(ATTR_SIZE).set(size);
	}

	/**
	 * Returns the Location of this entity.
	 *
	 * @return The Location is returned.
	 */
	public String getLocation() {
		return getValue(ATTR_LOCATION).extract();
	}

	/**
	 * Sets new Location for this entity.
	 *
	 * @param size The new Location.
	 */
	public void setLocation(String location) {
		getValue(ATTR_LOCATION).set(location);
	}

	/**
	 * Returns the FileMimeType of this entity.
	 *
	 * @return The FileMimeType is returned.
	 */
	public String getFileMimeType() {
		return getValue(ATTR_FILE_MIMETYPE).extract();
	}

	/**
	 * Sets new OriginalFileName for this entity.
	 *
	 * @param originalFileName The new OriginalFileName.
	 */
	public void setFileMimeType(String fileMimeType) {
		getValue(ATTR_FILE_MIMETYPE).set(fileMimeType);
	}

	/**
	 * Returns the OriginalFileName of this entity.
	 *
	 * @return The OriginalFileName is returned.
	 */
	public String getOriginalFileName() {
		return getValue(ATTR_ORIG_FILE_NAME).extract();
	}

	/**
	 * Sets new OriginalFileName for this entity.
	 *
	 * @param originalFileName The new OriginalFileName.
	 */
	public void setOriginalFileName(String originalFileName) {
		getValue(ATTR_ORIG_FILE_NAME).set(originalFileName);
	}

	/**
	 * Returns the OriginalFileName of this entity.
	 *
	 * @return The OriginalFileName is returned.
	 */
	public Instant getOriginalFileDate() {
		return getValue(ATTR_ORIG_FILE_DATE).extract();
	}

	/**
	 * Sets new OriginalFileName for this entity.
	 *
	 * @param originalFileName The new OriginalFileName.
	 */
	public void setOriginalFileDate(Instant originalFileDate) {
		getValue(ATTR_ORIG_FILE_DATE).set(originalFileDate);
	}

	/**
	 * Returns the FileChecksum of this entity.
	 *
	 * @return The FileChecksum is returned.
	 */
	public String getFileChecksum() {
		return getValue(ATTR_FILE_CHECKSUM).extract();
	}

	/**
	 * Sets new FileChecksum for this entity.
	 *
	 * @param fileChecksum The new FileChecksum.
	 */
	public void setFileChecksum(String fileChecksum) {
		getValue(ATTR_FILE_CHECKSUM).set(fileChecksum);
	}

	/**
	 * Returns the FileChecksumType of this entity.
	 *
	 * @return The FileChecksumType is returned.
	 */
	public String getFileChecksumType() {
		return getValue(ATTR_FILE_CHECKSUM_TYPE).extract();
	}

	/**
	 * Sets new FileChecksumType for this entity.
	 *
	 * @param fileChecksumType The new FileChecksumType.
	 */
	public void setFileChecksumType(String fileChecksumType) {
		getValue(ATTR_FILE_CHECKSUM_TYPE).set(fileChecksumType);
	}

	public static String getHash(File file, String algorithm) {
		try (FileInputStream is = new FileInputStream(file)) {
			MessageDigest digest = MessageDigest.getInstance(algorithm);

			byte[] bytesBuffer = new byte[1024];
			int bytesRead = -1;

			while ((bytesRead = is.read(bytesBuffer)) != -1) {
				digest.update(bytesBuffer, 0, bytesRead);
			}

			byte[] arr = digest.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < arr.length; i++) {
				sb.append(Integer.toString((arr[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException | IOException exc) {
			throw new DataAccessException("Could not generate hash from file!", exc);
		}
	}

}
