/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.mdm.api.base.BaseEntityManager;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.StatusAttachable;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.dflt.model.Status;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.dflt.model.Versionable;

/**
 * Extends the {@link BaseEntityManager} interface with additional load methods
 * dedicated to the default application models.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public interface EntityManager extends BaseEntityManager {

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Loads the entity identified by given entity class, {@link ContextType} and
	 * its instance ID.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param contextType The {@link ContextType}.
	 * @param instanceID  The instance ID.
	 * @return The entity with given instance ID is returned.
	 * @throws DataAccessException Thrown if unable to retrieve the entity.
	 */
	default <T extends Entity> T load(Class<T> entityClass, ContextType contextType, String instanceID)
			throws DataAccessException {
		List<T> entities = load(entityClass, contextType, Collections.singletonList(instanceID));
		if (entities.size() != 1) {
			throw new DataAccessException("Failed to load entity by instance ID.");
		}
		return entities.get(0);

	}

	/**
	 * Loads the entities identified by given entity class, {@link ContextType} and
	 * their instance IDs.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param contextType The {@link ContextType}.
	 * @param instanceIDs The instance IDs.
	 * @return The entities with given instance IDs are returned.
	 * @throws DataAccessException Thrown if unable to retrieve the entities.
	 */
	<T extends Entity> List<T> load(Class<T> entityClass, ContextType contextType, Collection<String> instanceIDs)
			throws DataAccessException;

	/**
	 * Loads the entities identified by given entity class and their instance IDs
	 * without loading their children.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param instanceIDs The instance IDs.
	 * @return The entities with given instance IDs are returned.
	 * @throws DataAccessException Thrown if unable to retrieve the entities.
	 */
	<T extends Entity> List<T> loadWithoutChildren(Class<T> entityClass, Collection<String> instanceIDs)
			throws DataAccessException;

	/**
	 * Loads the entities identified by given entity class, {@link ContextType} and
	 * their instance IDs without loading their children.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param contextType The {@link ContextType}.
	 * @param instanceIDs The instance IDs.
	 * @return The entities with given instance IDs are returned.
	 * @throws DataAccessException Thrown if unable to retrieve the entities.
	 */
	<T extends Entity> List<T> loadWithoutChildren(Class<T> entityClass, ContextType contextType,
			Collection<String> instanceIDs) throws DataAccessException;

	/**
	 * Loads the entity identified by given entity class and instance ID without
	 * loading its children.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param instanceID  The instance ID.
	 * @return The entity with given instance ID is returned.
	 * @throws DataAccessException Thrown if unable to retrieve the entity.
	 */
	default <T extends Entity> T loadWithoutChildren(Class<T> entityClass, String instanceID)
			throws DataAccessException {
		List<T> entities = loadWithoutChildren(entityClass, Collections.singletonList(instanceID));
		if (entities.size() != 1) {
			throw new DataAccessException("Failed to load entity by instance ID.");
		}
		return entities.get(0);
	}

	/**
	 * Loads the entity identified by given entity class, {@link ContextType} and
	 * instance ID without loading its children.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param contextType The {@link ContextType}.
	 * @param instanceID  The instance ID.
	 * @return The entity with given instance ID is returned.
	 * @throws DataAccessException Thrown if unable to retrieve the entity.
	 */
	default <T extends Entity> T loadWithoutChildren(Class<T> entityClass, ContextType contextType, String instanceID)
			throws DataAccessException {
		List<T> entities = loadWithoutChildren(entityClass, contextType, Collections.singletonList(instanceID));
		if (entities.size() != 1) {
			throw new DataAccessException("Failed to load entity by instance ID.");
		}
		return entities.get(0);
	}

	/**
	 * Loads the entities identified by given class and {@link ContextType} that
	 * match the criteria specified in the filter.
	 * 
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param filter      The {@link Filter} to use for selecting the entities.
	 * @return The entities matching the filter criteria are returned.
	 * @throws DataAccessException Thrown if unable to retrieve the entities.
	 */
	<T extends Entity> List<T> loadAll(Class<T> entityClass, Filter filter) throws DataAccessException;

	/**
	 * Loads all available entities of given type.
	 *
	 * <pre>
	 * {
	 * 	&#64;code
	 * 	List<CatalogComponent> catalogComponents = entityManager.loadAll(CatalogComponent.class, UNITUNDERTEST);
	 * }
	 * </pre>
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entities.
	 * @param contextType The {@link ContextType}.
	 * @return Entities are returned in a {@code List}.
	 * @throws DataAccessException Thrown if unable to retrieve the entities.
	 * @see #loadAll(Class, ContextType, String)
	 */
	default <T extends Entity> List<T> loadAll(Class<T> entityClass, ContextType contextType)
			throws DataAccessException {
		return loadAll(entityClass, contextType, "*");
	}

	/**
	 * Loads all available entities of given type whose name fulfills the given
	 * pattern.
	 *
	 * <pre>
	 * {
	 * 	&#64;code
	 * 	// retrieve all template roots whose name starts with 'Example'
	 * 	List<TemplateRoot> templateRoots = entityManager.loadAll(TemplateRoot.class, UNITUNDERTEST, "Example*");
	 * }
	 * </pre>
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entities.
	 * @param contextType The {@link ContextType}.
	 * @param pattern     Is always case sensitive and may contain wildcard
	 *                    characters as follows: "?" for one matching character and
	 *                    "*" for a sequence of matching characters.
	 * @return Matched entities are returned in a {@code List}.
	 * @throws DataAccessException Thrown if unable to retrieve the entities.
	 * @see #loadAll(Class)
	 */
	<T extends Entity> List<T> loadAll(Class<T> entityClass, ContextType contextType, String pattern)
			throws DataAccessException;

	/**
	 * 
	 * @param entityClass Type of the returned entities.
	 * @param contextType The {@link ContextType}.
	 * @param compName    name of the context component
	 * @param ids         ids of the context component instances
	 * @return Matched entities are returned in a {@code List}.
	 * @throws DataAccessException Thrown if unable to retrieve the entities.
	 * @see #loadAll(Class)
	 */
	<T extends Entity> List<T> loadAll(Class<T> entityClass, ContextType contextType, String compName,
			Collection<String> ids) throws DataAccessException;

	/**
	 * Loads the latest valid {@link Versionable} entity of given type and name.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param name        The exact name of the requested entity.
	 * @return Optional is empty if no such entity was found
	 * @throws DataAccessException Thrown if unable to retrieve the entity.
	 */
	default <T extends Versionable> Optional<T> loadLatestValid(Class<T> entityClass, String name)
			throws DataAccessException {
		return loadAll(entityClass, name).stream().filter(v -> v.nameEquals(name)).filter(Versionable::isValid)
				.max(Versionable.COMPARATOR);
	}

	/**
	 * Loads the latest valid {@link Versionable} entity of given type,
	 * {@link ContextType} and name.
	 *
	 * @param <T>         The desired type.
	 * @param entityClass Type of the returned entity.
	 * @param contextType The {@code ContextType}.
	 * @param name        The exact name of the requested entity.
	 * @return Optional is empty if no such entity was found
	 * @throws DataAccessException Thrown if unable to retrieve the entity.
	 */
	default <T extends Versionable> Optional<T> loadLatestValid(Class<T> entityClass, ContextType contextType,
			String name) throws DataAccessException {
		return loadAll(entityClass, contextType, name).stream().filter(v -> v.nameEquals(name))
				.filter(Versionable::isValid).max(Versionable.COMPARATOR);
	}

	<T extends StatusAttachable> List<T> loadAll(Class<T> entityClass, Status status, String pattern);

	/**
	 * Loads the referenced {@link TemplateTest} for a {@link Test}.
	 * 
	 * @param test {@link Test} referencing the desired template
	 * @return {@link Optional} with the loaded {@link TemplateTest}
	 */
	Optional<TemplateTest> loadTemplate(Test test);

	/**
	 * Loads the referenced {@link TemplateTestStep} for a {@link TestStep}
	 * 
	 * @param testStep {@link TestStep} referencing the desired template
	 * @return {@link Optional} with the loaded {@link TemplateTestStep}
	 */
	Optional<TemplateTestStep> loadTemplate(TestStep testStep);
}
