/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import java.util.List;

import org.eclipse.mdm.api.base.BaseEntityManager;
import org.eclipse.mdm.api.base.adapter.Relation;

/**
 * This interface extends the {@link Entity} interface and provides methods for
 * managing the DescriptiveFiles attached to an entity.
 *
 * @since 1.0.0
 * @author Martin Fleischer, Peak Solution GmbH
 */
public interface DescriptiveFilesAttachable extends Entity {

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Attaches a {@link DescriptiveFile} to this entity.
	 * 
	 * @param relation        The relation with which the {@link DescriptiveFile}
	 *                        should be attached to this entity.
	 * @param descriptiveFile The {@link DescriptiveFile} to attach to this entity.
	 */
	void addDescriptiveFile(Relation relation, DescriptiveFile descriptiveFile);

	/**
	 * Gets the {@link DescriptiveFile}s attached to this entity.
	 * 
	 * @param relation The relation with which the {@link DescriptiveFile} is
	 *                 attached to this entity.
	 * @param manager  The {@link BaseEntityManager} to use.
	 * @return The {@link DescriptiveFile}s attached to this entity.
	 */
	List<DescriptiveFile> getDescriptiveFiles(Relation relation, BaseEntityManager manager);

	/**
	 * Removes a {@link DescriptiveFile} attached to this entity.
	 * 
	 * @param relation        The relation with which the {@link DescriptiveFile} is
	 *                        attached to this entity.
	 * @param descriptiveFile The {@link DescriptiveFile} to remove from this
	 *                        entity.
	 */
	void removeDescriptiveFile(Relation relation, DescriptiveFile descriptiveFile);

}
