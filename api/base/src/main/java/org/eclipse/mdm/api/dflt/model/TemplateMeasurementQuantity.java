/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Sortable;

/**
 * Entity TemplateMeasurementQuantity
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
public class TemplateMeasurementQuantity extends BaseEntity implements Deletable, Sortable {

	protected TemplateMeasurementQuantity(Core core) {
		super(core);
	}

	public void addItem(TemplateChannelGroup templateChannelGroup) {
		getCore().getNtoMStore().add("TplSubMatrix", templateChannelGroup);
	}

	public void removeItem(TemplateChannelGroup templateChannelGroup) {
		getCore().getNtoMStore().remove("TplSubMatrix", templateChannelGroup);
	}
}
