/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import java.util.Arrays;
import java.util.List;

import org.eclipse.mdm.api.base.query.ComparisonOperator;

public interface ValuesFilter {

	public static class ValuesCondition implements ValuesFilter {
		private String name;
		private ComparisonOperator operator;
		private List<String> values;

		public ValuesCondition(String name, ComparisonOperator operator, String value) {
			this(name, operator, Arrays.asList(value));
		}

		public ValuesCondition(String name, ComparisonOperator operator, List<String> values) {
			this.name = name;
			this.operator = operator;
			this.values = values;
		}

		public String getName() {
			return name;
		}

		public ComparisonOperator getOperator() {
			return operator;
		}

		public List<String> getValues() {
			return values;
		}

		@Override
		public String toString() {
			return "Leaf{ name=" + name + ", operator=" + operator + ", values=" + values + " }";
		}
	}

	public static class ValuesConjunction implements ValuesFilter {
		private String conjunction;
		private ValuesFilter left;
		private ValuesFilter right;

		private ValuesConjunction(String conjunction, ValuesFilter left, ValuesFilter right) {
			this.conjunction = conjunction;
			this.left = left;
			this.right = right;
		}

		public static ValuesConjunction and(ValuesFilter left, ValuesFilter right) {
			return new ValuesConjunction("and", left, right);
		}

		public static ValuesConjunction or(ValuesFilter left, ValuesFilter right) {
			return new ValuesConjunction("or", left, right);
		}

		public static ValuesConjunction not(ValuesFilter arg) {
			return new ValuesConjunction("not", arg, null);
		}

		public ValuesFilter getLeft() {
			return left;
		}

		public ValuesFilter getRight() {
			return right;
		}

		public String getConjunction() {
			return conjunction;
		}

		@Override
		public String toString() {
			return "CNode{ conjunction=" + conjunction + ", left=" + left + ", right=" + right + " }";
		}
	}
}
