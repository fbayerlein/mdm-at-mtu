/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import java.util.List;

import org.eclipse.mdm.api.base.BaseEntityManager;
import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.adapter.Relation;

/**
 * Class for the commonalities of all context objects (ContextComponent and
 * ContextSensor).
 *
 * @since 5.3.0
 * @author Martin Fleischer, Peak Solution GmbH
 */
public abstract class ContextBase extends BaseEntity implements DescriptiveFilesAttachable, Deletable {

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	ContextBase(Core core) {
		super(core);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addDescriptiveFile(Relation relation, DescriptiveFile descriptiveFile) {
		getCore().getNtoMStore().add(relation.getName(), descriptiveFile);
		descriptiveFile.getCore().getNtoMStore().add(relation.getInverseName(), this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DescriptiveFile> getDescriptiveFiles(Relation relation, BaseEntityManager manager) {
		return manager.loadRelatedEntities(this, relation.getName(), DescriptiveFile.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeDescriptiveFile(Relation relation, DescriptiveFile descriptiveFile) {
		getCore().getNtoMStore().remove(relation.getName(), descriptiveFile);
		descriptiveFile.getCore().getNtoMStore().remove(relation.getInverseName(), this);
	}

}
