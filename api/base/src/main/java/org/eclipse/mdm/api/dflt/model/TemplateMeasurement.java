/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Describable;
import org.eclipse.mdm.api.base.model.Sortable;

/**
 * Entity TemplateMeasurement
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
public class TemplateMeasurement extends BaseEntity implements Deletable, Describable, Sortable, Versionable {

	public static final String ATTR_DEFAULT_MIME_TYPE = "DefaultMimeType";
	public static final String ATTR_DEFAULT_NAME = "DefaultName";

	protected TemplateMeasurement(Core core) {
		super(core);
	}

	public String getDefaultMimeType() {
		return getValue(ATTR_DEFAULT_MIME_TYPE).extract();
	}

	public void setDefaultMimeType(String defaultMimeType) {
		getValue(ATTR_DEFAULT_MIME_TYPE).set(defaultMimeType);
	}

	public String getDefaultName() {
		return getValue(ATTR_DEFAULT_NAME).extract();
	}

	public void setDefaultName(String defaultName) {
		getValue(ATTR_DEFAULT_NAME).set(defaultName);
	}
}
