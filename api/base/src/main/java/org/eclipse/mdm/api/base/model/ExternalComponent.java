/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.api.base.model;

import java.util.List;

import org.eclipse.mdm.api.base.BaseEntityManager;
import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.query.DataAccessException;

/**
 * {@link Entity} representing the ExternalComponent application element in the
 * data storage.
 * 
 * @author Matthias Koller, Peak Solution GmbH
 * @author Martin Fleischer, Peak Solution GmbH
 *
 */
public class ExternalComponent extends BaseEntity implements Deletable {

	public static final String TYPE_NAME = "ExternalComponent";

	public static final String ATTR_ORDINALNUMBER = "OrdinalNumber";
	public static final String ATTR_TYPESPECIFICATION = "TypeSpecification";
	public static final String ATTR_LENGTH = "Length";
	public static final String ATTR_STARTOFFSET = "StartOffset";
	public static final String ATTR_BLOCKSIZE = "Blocksize";
	public static final String ATTR_VALUESPERBLOCK = "ValuesPerBlock";
	public static final String ATTR_VALUEOFFSET = "ValueOffset";
	public static final String ATTR_FILENAMEURL = "FilenameURL";
	public static final String ATTR_FLAGSFILENAMEURL = "FlagsFilenameURL";
	public static final String ATTR_FLAGSSTARTOFFSET = "FlagsStartOffset";
	public static final String ATTR_BITCOUNT = "BitCount";
	public static final String ATTR_BITOFFSET = "BitOffset";

	public static final String REL_VALUESFILE = "ValuesFile";
	public static final String REL_FLAGSFILE = "FlagsFile";

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	public ExternalComponent(Core core) {
		super(core);
	}

	/**
	 * Sets the {@link TypeSpecification} for this instance.
	 * 
	 * @param typeSpecification The {@link TypeSpecification} to set.
	 */
	public void setTypeSpecification(TypeSpecification typeSpecification) {
		getValue(ATTR_TYPESPECIFICATION).set(typeSpecification);
	}

	/**
	 * Sets the length for this instance.
	 * 
	 * @param length The length to set.
	 */
	public void setLength(int length) {
		getValue(ATTR_LENGTH).set(length);
	}

	/**
	 * Sets the start offset for this instance.
	 * 
	 * @param startOffset The start offset to set.
	 */
	public void setStartOffset(Long startOffset) {
		if (startOffset != null && getValue(ATTR_STARTOFFSET).getValueType().equals(ValueType.INTEGER)) {
			try {
				getValue(ATTR_STARTOFFSET).set(Math.toIntExact(startOffset));
			} catch (ArithmeticException exc) {
				throw new DataAccessException(
						String.format("Cannot convert StartOffset value %d to integer!", startOffset));
			}
		} else {
			getValue(ATTR_STARTOFFSET).set(startOffset);
		}
	}

	/**
	 * Sets the block size for this instance.
	 * 
	 * @param blocksize The block size to set.
	 */
	public void setBlocksize(Integer blocksize) {
		getValue(ATTR_BLOCKSIZE).set(blocksize);
	}

	/**
	 * Sets the values per block for this instance.
	 * 
	 * @param valuesPerBlock The values per block to set.
	 */
	public void setValuesPerBlock(Integer valuesPerBlock) {
		getValue(ATTR_VALUESPERBLOCK).set(valuesPerBlock);
	}

	/**
	 * Sets the value offset for this instance.
	 * 
	 * @param valueOffset The value offset to set.
	 */
	public void setValueOffset(Integer valueOffset) {
		getValue(ATTR_VALUEOFFSET).set(valueOffset);
	}

	/**
	 * Sets the values {@link FileLink} for this instance.
	 * 
	 * @param fileLink The values {@link FileLink} to set.
	 */
	public void setFileLink(FileLink fileLink) {
		getValue(ATTR_FILENAMEURL).set(fileLink);
	}

	/**
	 * Sets the flags {@link FileLink} for this instance.
	 * 
	 * @param flagsFileLink The flags {@link FileLink} to set.
	 */
	public void setFlagsFileLink(FileLink flagsFileLink) {
		getValue(ATTR_FLAGSFILENAMEURL).set(flagsFileLink);
	}

	/**
	 * Sets the flags start offset for this instance.
	 * 
	 * @param flagsStartOffset The flags start offset to set.
	 */
	public void setFlagsStartOffset(Long flagsStartOffset) {
		if (flagsStartOffset != null && getValue(ATTR_FLAGSSTARTOFFSET).getValueType().equals(ValueType.INTEGER)) {
			try {
				getValue(ATTR_FLAGSSTARTOFFSET).set(Math.toIntExact(flagsStartOffset));
			} catch (ArithmeticException exc) {
				throw new DataAccessException(
						String.format("Cannot convert FlagsStartOffset value %d to integer!", flagsStartOffset));
			}
		} else {
			getValue(ATTR_FLAGSSTARTOFFSET).set(flagsStartOffset);
		}
	}

	/**
	 * Sets the ordinal number for this instance.
	 * 
	 * @param ordinalNumber The ordinal number to set.
	 */
	public void setOrdinalNumber(int ordinalNumber) {
		getValue(ATTR_ORDINALNUMBER).set(ordinalNumber);
	}

	/**
	 * Sets the bit count for this instance.
	 * 
	 * @param bitCount The bit count to set.
	 */
	public void setBitCount(Short bitCount) {
		getValue(ATTR_BITCOUNT).set(bitCount);
	}

	/**
	 * Sets the bit offset for this instance.
	 * 
	 * @param bitOffset The bit offset to set.
	 */
	public void setBitOffset(Short bitOffset) {
		getValue(ATTR_BITOFFSET).set(bitOffset);
	}

	/**
	 * Attaches an {@link ExtCompFile} to this entity.
	 * 
	 * @param relation    The relation with which the {@link ExtCompFile} should be
	 *                    attached to this entity.
	 * @param extCompFile The {@link ExtCompFile} to attach to this entity.
	 */
	public void addExtCompFile(Relation relation, ExtCompFile extCompFile) {
		getCore().getMutableStore().set(relation.getName(), extCompFile);
	}

	/**
	 * Gets the {@link ExtCompFile} attached to this entity by the relation with
	 * relationName.
	 * 
	 * @param relationName Name of the relation by which the {@link ExtCompFile} is
	 *                     attached to this entity
	 * @param manager      the {@link BaseEntityManager} to use
	 * 
	 * @return {@link ExtCompFile} attached to this entity
	 */
	public ExtCompFile getExtCompFile(String relationName, BaseEntityManager manager) {
		List<ExtCompFile> listExtCompFiles = manager.loadRelatedEntities(this, relationName, ExtCompFile.class);

		if (listExtCompFiles.isEmpty()) {
			return null;
		} else {
			return listExtCompFiles.get(0);
		}
	}
}
