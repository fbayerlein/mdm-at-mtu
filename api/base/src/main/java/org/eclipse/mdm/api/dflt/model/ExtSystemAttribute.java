/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import java.util.List;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Describable;

/**
 * Implementation of an external system attribute entity type. An external
 * system attribute contains several mdm attributes.
 *
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
public class ExtSystemAttribute extends BaseEntity implements Describable, Deletable {

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	protected ExtSystemAttribute(Core core) {
		super(core);
	}

	public List<MDMAttribute> getAttributes() {
		return getMDMAttributes();
	}

	/**
	 * Returns all available {@link MDMAttribute}s related to this external system
	 * attribute.
	 *
	 * @return The returned {@code List} is unmodifiable.
	 */
	public List<MDMAttribute> getMDMAttributes() {
		return getCore().getChildrenStore().get(MDMAttribute.class);
	}

}
