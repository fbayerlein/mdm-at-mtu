/**
 * 
 */
package org.eclipse.mdm.api.dflt.model;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Describable;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Tagable;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;

/**
 * @author akn
 *
 */
public class MDMTag extends BaseEntity implements Describable, Deletable {

	/**
	 * @param core
	 */
	public MDMTag(Core core) {
		super(core);
	}

	public void tagItem(Tagable tagableItem) {
		if (tagableItem instanceof Test) {
			getCore().getNtoMStore().add("Test", (Test) tagableItem);
		} else if (tagableItem instanceof TestStep) {
			getCore().getNtoMStore().add("TestStep", (TestStep) tagableItem);
		} else if (tagableItem instanceof Measurement) {
			getCore().getNtoMStore().add("MeaResult", (Measurement) tagableItem);
		}

	}

	public void removeFromTag(Tagable tagableItem) {
		if (tagableItem instanceof Test) {
			getCore().getNtoMStore().remove("Test", (Test) tagableItem);
		} else if (tagableItem instanceof TestStep) {
			getCore().getNtoMStore().remove("TestStep", (TestStep) tagableItem);
		} else if (tagableItem instanceof Measurement) {
			getCore().getNtoMStore().remove("MeaResult", (Measurement) tagableItem);
		}
	}

}
