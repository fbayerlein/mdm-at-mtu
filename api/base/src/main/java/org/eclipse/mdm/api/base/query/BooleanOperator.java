/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.query;

/**
 * The operator enumeration.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 * @see Condition
 * @see Filter
 */
public enum BooleanOperator {

	// ======================================================================
	// Enumerations
	// ======================================================================

	/**
	 * Logical conjunction.
	 */
	AND,

	/**
	 * Logical disjunction.
	 */
	OR,

	/**
	 * Logical Negation.
	 */
	NOT;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		switch (this) {
		case AND:
			return "and";
		case OR:
			return "or";
		case NOT:
			return "not";
		default:
			return this.toString();
		}
	}
}
