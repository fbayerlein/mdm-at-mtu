/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import java.util.List;

public class ChannelValuesList {

	private List<ChannelValues> values;

	public ChannelValuesList(List<ChannelValues> values) {
		this.values = values;
	}

	public List<ChannelValues> getValues() {
		return values;
	}

	public String getNextPageToken() {
		// TODO Auto-generated method stub
		return null;
	}

}
