/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.adapter.Core;

/**
 * 
 * @author jst
 *
 */
public class MDMLocalization extends BaseEntity implements Deletable {

	private static final String NO_TRANSLATION = "_no_translation";

	public MDMLocalization(Core core) {
		super(core);
	}

	public List<String> getTranslations() {
		String[] stringSeq = getValue("AliasNames").extract();
		return Arrays.stream(stringSeq).map(s -> s.equalsIgnoreCase(NO_TRANSLATION) ? null : s)
				.collect(Collectors.toList());
//		return Arrays.stream(stringSeq).collect(Collectors.joining(";"));
	}
}
