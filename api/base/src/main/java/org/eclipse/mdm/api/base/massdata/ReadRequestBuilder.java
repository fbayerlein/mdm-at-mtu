/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import java.util.Arrays;
import java.util.List;

import org.eclipse.mdm.api.base.massdata.ReadRequest.ValuesMode;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.Unit;

/**
 * Builds measured values read request configurations.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 */
public final class ReadRequestBuilder {

	// ======================================================================
	// Instance variables
	// ======================================================================

	private final ReadRequest readRequest;

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param readRequest The {@link ReadRequest} that will be configured.
	 */
	ReadRequestBuilder(ReadRequest readRequest) {
		this.readRequest = readRequest;
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Configures the {@link ReadRequest} to retrieve measured values of all related
	 * {@link Channel}s.
	 *
	 * @return This builder is returned.
	 */
	public ReadRequestBuilder allChannels() {
		readRequest.setLoadAllChannels();
		return this;
	}

	/**
	 * Adds given {@link Channel}s to the underlying {@link ReadRequest}.
	 *
	 * <p>
	 * <b>Note:</b> {@code Channel}s added with this method will be ignored, once
	 * {@link #allChannels()} was called.
	 *
	 * @param channels The {@code Channel}s whose measured values will be loaded.
	 * @return This builder is returned.
	 */
	public ReadRequestBuilder channels(List<Channel> channels) {
		channels.forEach(readRequest::addChannel);
		return this;
	}

	/**
	 * Adds given {@link Channel}s to the underlying {@link ReadRequest}.
	 *
	 * <p>
	 * <b>Note:</b> {@code Channel}s added with this method will be ignored, once
	 * {@link #allChannels()} was called.
	 *
	 * @param channels The {@code Channel}s whose measured values will be loaded.
	 * @return This builder is returned.
	 */
	public ReadRequestBuilder channels(Channel... channels) {
		Arrays.stream(channels).forEach(readRequest::addChannel);
		return this;
	}

	/**
	 * Adds given {@link Channel} to the underlying {@link ReadRequest}.
	 *
	 * <p>
	 * <b>Note:</b> {@code Channel} added with this method will be ignored, once
	 * {@link #allChannels()} was called.
	 *
	 * @param channel The {@code Channel} whose measured values will be loaded.
	 * @param unit    The unit of the loaded measured values.
	 * @return This builder is returned.
	 */
	public ReadRequestBuilder channel(Channel channel, Unit unit) {
		readRequest.addChannel(channel, unit);
		return this;
	}

	/**
	 * Configures the valuesMode.
	 * 
	 * @param valuesMode {@link ValuesMode} to use for the read request.
	 * @return This builder is returned.
	 */
	public ReadRequestBuilder valuesMode(ReadRequest.ValuesMode valuesMode) {
		readRequest.setValuesMode(valuesMode);
		return this;
	}

	/**
	 * Configures the {@link ReadRequest} to load all measured values of each
	 * configured {@link Channel} and returns the configured {@code
	 * ReadRequest}.
	 *
	 * <p>
	 * <b>Note:</b> Before calling this the {@code Channel}s whose measured values
	 * have to be loaded must be configured.
	 *
	 * @return This builder is returned.
	 */
	public ReadRequest allValues() {
		readRequest.setStartIndex(0);
		readRequest.setRequestSize(0);
		return readRequest;
	}

	/**
	 * Configures the {@link ReadRequest} to load the specified number of values for
	 * each {@link Channel}. this is a terminal operation returning the built
	 * {@link ReadRequest}.
	 *
	 * <p>
	 * <b>Note:</b> If the request size is zero, then all available measured values
	 * will be loaded for each configured {@link Channel}.
	 *
	 * @param requestSize The request size.
	 * @return the {@link ReadRequest}
	 * @throws IllegalArgumentException Thrown if the request size is smaller than
	 *                                  0.
	 */
	public ReadRequest values(int requestSize) {
		readRequest.setStartIndex(0);
		readRequest.setRequestSize(requestSize);
		return readRequest;
	}

	/**
	 * Configures the {@link ReadRequest} to load the specified number of values
	 * from a specified start index for each {@link Channel}. this is a terminal
	 * operation returning the built {@link ReadRequest}.
	 *
	 * <p>
	 * <b>Note:</b> If the request size is zero, then all available measured values
	 * will be loaded for each configured {@link Channel}.
	 *
	 * @param startIndex  The start index.
	 * @param requestSize The request size.
	 * @return the {@link ReadRequest}
	 * @throws IllegalArgumentException Thrown if the request size is smaller than 0
	 *                                  or if the start index is smaller than 0.
	 */
	public ReadRequest values(int startIndex, int requestSize) {
		readRequest.setStartIndex(startIndex);
		readRequest.setRequestSize(requestSize);
		return readRequest;
	}

	/**
	 * Returns an {@link ReadRequestIterable} to iterate over all available
	 * {@link ReadRequest}s.
	 *
	 * @return The {@code ReadRequestIterable} is returned.
	 */
	public ReadRequestIterable createIterable() {
		return new ReadRequestIterable(readRequest);
	}

}
