/********************************************************************************
 * Copyright (c) 2025 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.api.dflt.model;

public class DataSourceHealth {
	
	private long uptimeSeconds;
	
	private DataSourceSessionInfo sessions;
	

	public DataSourceHealth() {
		// TODO Auto-generated constructor stub
	}

	
	
	/**
	 * @return the uptimeSeconds
	 */
	public long getUptimeSeconds() {
		return uptimeSeconds;
	}



	/**
	 * @param uptimeSeconds the uptimeSeconds to set
	 */
	public void setUptimeSeconds(long uptimeSeconds) {
		this.uptimeSeconds = uptimeSeconds;
	}



	/**
	 * @return the sessions
	 */
	public DataSourceSessionInfo getSessions() {
		return sessions;
	}



	/**
	 * @param sessions the sessions to set
	 */
	public void setSessions(DataSourceSessionInfo sessions) {
		this.sessions = sessions;
	}

	
}
