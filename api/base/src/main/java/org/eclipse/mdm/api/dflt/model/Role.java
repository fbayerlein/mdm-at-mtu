/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Describable;
import org.eclipse.mdm.api.base.model.User;

/**
 * Implementation of the role entity type. Each role can contain multiple
 * {@link User} and each {@link User} can be assigned multiple {@link Role}s.
 */
public class Role extends BaseEntity implements Deletable, Describable {

	/**
	 * The 'SuperuserFlag' attribute name.
	 */
	public static final String ATTR_SUPERUSER_FLAG = "SuperuserFlag";

	/**
	 * The 'groups2users' relation name.
	 */
	public static final String REL_GROUPS2USERS = "groups2users";

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	Role(Core core) {
		super(core);
		getValue(ATTR_SUPERUSER_FLAG).set((short) 0);
	}

	/**
	 * @param user User to add
	 */
	public void addUser(User user) {
		getCore().getNtoMStore().add(REL_GROUPS2USERS, user);
	}

	/**
	 * @param user {@link User} to remove
	 */
	public void removeUser(User user) {
		getCore().getNtoMStore().remove(REL_GROUPS2USERS, user);
	}
}
