/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.api.base;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Arrays;

import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.ExternalComponentData;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.TypeSpecification;
import org.junit.Test;

public class WriteRequestBuilderTest {

	private final ChannelGroup channelGroup = mock(ChannelGroup.class);
	private final Channel channel = mock(Channel.class);
	private final AxisType axisType = mock(AxisType.class);

	private final ExternalComponentData externalComponent1 = new ExternalComponentData()
			.setTypeSpecification(TypeSpecification.BIT_INTEGER).setStartOffset(100L).setBlocksize(1)
			.setValuesPerBlock(100).setLength(100)
			.setFileLink(FileLink.newRemote("extComp1.btf", new MimeType(""), "", -1L, null, FileServiceType.EXTREF))
			.setFlagsFileLink(
					FileLink.newRemote("extCompFlags.btf", new MimeType(""), "", -1L, null, FileServiceType.EXTREF))
			.setFlagsStartOffset(0L).setBitCount((short) 24).setBitOffset((short) 0);

	private final ExternalComponentData externalComponent2 = new ExternalComponentData()
			.setTypeSpecification(TypeSpecification.BIT_INTEGER).setStartOffset(10L).setBlocksize(1)
			.setValuesPerBlock(50).setLength(200)
			.setFileLink(FileLink.newRemote("extComp2.btf", new MimeType(""), "", -1L, null, FileServiceType.EXTREF))
			.setFlagsFileLink(
					FileLink.newRemote("extCompFlags.btf", new MimeType(""), "", -1L, null, FileServiceType.EXTREF))
			.setFlagsStartOffset(200L).setBitCount((short) 24).setBitOffset((short) 0);

	@Test
	public void testOneExternalComponent() {
		WriteRequest r1 = WriteRequest.create(channelGroup, channel, axisType).explicitExternal()
				.externalComponent(ScalarType.INTEGER, externalComponent1).independent().build();

		assertThat(r1).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channel", channel).hasFieldOrPropertyWithValue("axisType", axisType);
		assertThat(r1.getExternalComponents()).containsExactly(externalComponent1);
	}

	@Test
	public void testMultipleExternalComponents() {
		WriteRequest r1 = WriteRequest.create(channelGroup, channel, axisType).explicitExternal()
				.externalComponents(ScalarType.INTEGER, Arrays.asList(externalComponent1, externalComponent2))
				.independent().build();

		assertThat(r1).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channel", channel).hasFieldOrPropertyWithValue("axisType", axisType);
		assertThat(r1.getExternalComponents()).containsExactly(externalComponent1, externalComponent2);
	}

	@Test
	public void testMultipleExternalComponentsCopyExtComp() {

		WriteRequest r1 = WriteRequest.create(channelGroup, channel, axisType).explicitExternal()
				.externalComponents(ScalarType.INTEGER, Arrays.asList(new ExternalComponentData[] {
						(new ExternalComponentData()).setTypeSpecification(externalComponent1.getTypeSpecification())
								.setStartOffset(externalComponent1.getStartOffset())
								.setBlocksize(externalComponent1.getBlocksize())
								.setValuesPerBlock(externalComponent1.getValuesPerBlock())
								.setLength(externalComponent1.getLength()).setFileLink(externalComponent1.getFileLink())
								.setFlagsFileLink(externalComponent1.getFlagsFileLink())
								.setFlagsStartOffset(externalComponent1.getFlagsStartOffset())
								.setBitCount(externalComponent1.getBitCount())
								.setBitOffset(externalComponent1.getBitOffset()),
						(new ExternalComponentData()).setTypeSpecification(externalComponent2.getTypeSpecification())
								.setStartOffset(externalComponent2.getStartOffset())
								.setBlocksize(externalComponent2.getBlocksize())
								.setValuesPerBlock(externalComponent2.getValuesPerBlock())
								.setLength(externalComponent2.getLength()).setFileLink(externalComponent2.getFileLink())
								.setFlagsFileLink(externalComponent2.getFlagsFileLink())
								.setFlagsStartOffset(externalComponent2.getFlagsStartOffset())
								.setBitCount(externalComponent2.getBitCount())
								.setBitOffset(externalComponent2.getBitOffset()) }))
				.independent().build();

		assertThat(r1).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channel", channel).hasFieldOrPropertyWithValue("axisType", axisType);
		assertThat(r1.getExternalComponents()).containsExactly(externalComponent1, externalComponent2);
	}
}
