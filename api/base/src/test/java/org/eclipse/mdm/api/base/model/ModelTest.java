/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.assertj.core.data.Offset;
import org.eclipse.mdm.api.base.CoreImpl;
import org.eclipse.mdm.api.base.EntityFactoryImpl;
import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.massdata.UnitBuilder;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequestBuilder;
import org.eclipse.mdm.api.base.model.MeasuredValues.ValueIterator;
import org.eclipse.mdm.api.base.search.ContextState;

/**
 * 
 * some unit test to test functionality of org.eclipse.mdm.api.base.model. At
 * this point, please don't expect anything near complete test coverage.
 * 
 * @author Florian Schmitt
 *
 */
public class ModelTest {

	// compare doubles up to this accuracy
	private static final Offset<Double> DOUBLE_OFFSET = Offset.offset(0.00005);
	private static final Offset<Float> FLOAT_OFFSET = Offset.offset(0.00005f);

	/**
	 * basic test for reading the value of a parameter.The intialization via maps is
	 * needed to simulate the data store.
	 */
	@org.junit.Test
	public void parameterValue() {
		Map<String, Value> map = new HashMap<>();
		map.put("DataType", new Value(ValueType.STRING, "DataType", null, true, ScalarType.FLOAT, ScalarType.class,
				ScalarType.FLOAT, EnumRegistry.SCALAR_TYPE));
		map.put("Value", ValueType.STRING.create("Value", null, true, "5.7"));
		map.put("Name", ValueType.STRING.create("Name", null, true, "paramname"));
		Core core = new CoreImpl(map);
		Parameter tp = new Parameter(core);
		Value vv = tp.getVirtualValue();

		assertThat(vv.extract(ValueType.FLOAT)).isCloseTo(5.7f, FLOAT_OFFSET);
	}

	/**
	 * basic test for reading measured float values
	 */
	@org.junit.Test
	public void measuredValueFlags() {
		int[] vals = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
		boolean[] booleanFlags = { false, true, false, true, false, true, false, true, false, true, false, true, false,
				true, false, true, false, true };
		short[] shortFlags = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
		MeasuredValues mv = new MeasuredValues(ScalarType.INTEGER, "Value1", "lightyears",
				SequenceRepresentation.EXPLICIT, new double[0], false, AxisType.Y_AXIS, vals, shortFlags);
		ValueIterator<Integer> valueIterator = mv.iterator();
		int i = 0;
		while (valueIterator.hasNext()) {
			assertThat(valueIterator.isValid()).isEqualTo(booleanFlags[i]);
			assertThat(valueIterator.getFlag()).isEqualTo(shortFlags[i]);
			assertThat(valueIterator.next()).isEqualTo(vals[i]);
			i++;
		}
		assertEquals(i, vals.length);
	}

	/**
	 * basic test for reading measured float values
	 */
	@org.junit.Test
	public void measuredValueFloat() {
		Float[] vals = { 1.0f, 2.0f, 3.7f, 2.1f };
		boolean[] booleanFlags = { true, false, false, true };
		short[] shortFlags = { 15, 2, 0, 15 };
		MeasuredValues mv = new MeasuredValues(ScalarType.FLOAT, "Value1", "lightyears",
				SequenceRepresentation.EXPLICIT, new double[0], false, AxisType.Y_AXIS, vals, shortFlags);
		ValueIterator<Float> valueIterator = mv.iterator();
		int i = 0;
		while (valueIterator.hasNext()) {
			assertThat(valueIterator.isValid()).isEqualTo(booleanFlags[i]);
			assertThat(valueIterator.getFlag()).isEqualTo(shortFlags[i]);
			assertThat(valueIterator.next()).isCloseTo(vals[i], FLOAT_OFFSET);
			i++;
		}
		assertEquals(i, vals.length);
	}

	/**
	 * basic test for reading measured double values
	 */
	@org.junit.Test
	public void measuredValueDouble() {
		Double[] vals = { 1.0d, 2.0d, 3.7d, 2.1d };
		boolean[] booleanFlags = { true, true, false, false };
		short[] shortFlags = { 15, 7, 0, 14 };
		MeasuredValues mv = new MeasuredValues(ScalarType.DOUBLE, "Value1", "lightyears",
				SequenceRepresentation.EXPLICIT, new double[0], false, AxisType.Y_AXIS, vals, shortFlags);
		ValueIterator<Double> valueIterator = mv.iterator();
		int i = 0;
		while (valueIterator.hasNext()) {
			assertThat(valueIterator.isValid()).isEqualTo(booleanFlags[i]);
			assertThat(valueIterator.getFlag()).isEqualTo(shortFlags[i]);
			assertThat(valueIterator.next()).isCloseTo(vals[i], DOUBLE_OFFSET);
			i++;
		}
		assertEquals(i, vals.length);
	}

	/**
	 * basic test for reading measured string values
	 */
	@org.junit.Test
	public void measuredValueString() {
		String[] vals = { "str1", "str2", "str3", "str4" };
		boolean[] booleanFlags = { true, true, false, true };
		short[] shortFlags = { 15, 15, 0, 15 };
		MeasuredValues mv = new MeasuredValues(ScalarType.STRING, "Value1", "lightyears",
				SequenceRepresentation.EXPLICIT, new double[0], false, AxisType.Y_AXIS, vals, shortFlags);
		ValueIterator<String> valueIterator = mv.iterator();
		int i = 0;
		while (valueIterator.hasNext()) {
			assertThat(valueIterator.isValid()).isEqualTo(booleanFlags[i]);
			assertThat(valueIterator.getFlag()).isEqualTo(shortFlags[i]);
			assertThat(valueIterator.next()).isEqualTo(vals[i]);
			i++;
		}
		assertEquals(i, vals.length);
	}

	/**
	 * basic test for reading attributes of the channel.The intialization via maps
	 * is needed to simulate the data store.
	 */
	@org.junit.Test
	public void getChannelAttrs() {
		Map<String, Value> map = new HashMap<String, Value>();
		double min_src = 5.7d;
		double max_src = 10.23d;
		map.put("Minimum", ValueType.DOUBLE.create("Minimum", "m/s", true, new Double(min_src)));
		map.put("Maximum", ValueType.DOUBLE.create("Maximum", "m/s", true, new Double(max_src)));
		Core core = new CoreImpl(map);
		Channel ch = new Channel(core);

		assertThat(ch.getMinimum()).isCloseTo(min_src, DOUBLE_OFFSET);
		assertThat(ch.getMaximum()).isCloseTo(max_src, DOUBLE_OFFSET);
	}

	/**
	 * basic test to read the default scalar type of a quantity. The intialization
	 * via maps is needed to simulate the data store.
	 */
	@org.junit.Test
	public void getQuantity() {
		Map<String, Value> map = new HashMap<String, Value>();
		map.put("DefDataType", new Value(ValueType.ENUMERATION, "name", "unit", true, ScalarType.FLOAT,
				ScalarType.class, ScalarType.FLOAT, EnumRegistry.SCALAR_TYPE));
		Core core = new CoreImpl(map);
		Quantity quantity = new Quantity(core);
		ScalarType defaultScalarType = quantity.getDefaultScalarType();
		assertTrue(defaultScalarType.isFloat());
	}

	/**
	 * basic test for building a write request
	 */
	@org.junit.Test
	public void writeRequest() {
		AxisType axisType = AxisType.X_AXIS;
		Core core = new CoreImpl(new HashMap<>());
		ChannelGroup channelGroup = new ChannelGroup(core);
		Channel channel = new Channel(core);
		WriteRequestBuilder wrb = WriteRequest.create(channelGroup, channel, axisType);
		UnitBuilder ub = wrb.implicitConstant(ScalarType.DOUBLE, 0.7d);
		WriteRequest wr = ub.build();
		SequenceRepresentation sequenceRepresentation = wr.getSequenceRepresentation();
		assertTrue(sequenceRepresentation.isConstant());
		assertEquals(AxisType.X_AXIS, wr.getAxisType());
	}

	/**
	 * basic test for getting a measurement channel and all necessary related
	 * objects (measurment, and so on) via factory methods. The intialization via
	 * maps is needed to simulate the data store.
	 */
	@org.junit.Test
	public void entityFactory() {
		Map<String, Value> map = new HashMap<String, Value>();
		map.put("Name", ValueType.STRING.create("Name", null, true, "pdnameabc"));
		map.put("Length", ValueType.INTEGER.create("Length", null, true, 13));
		map.put("Mass", ValueType.INTEGER.create("Length", null, true, 0));
		map.put("Time", ValueType.INTEGER.create("Length", null, true, 0));
		map.put("Temperature", ValueType.INTEGER.create("Length", null, true, 0));
		map.put("Current", ValueType.INTEGER.create("Length", null, true, 0));
		map.put("MolarAmount", ValueType.INTEGER.create("Length", null, true, 0));
		map.put("LuminousIntensity", ValueType.INTEGER.create("Length", null, true, 0));
		// TODO (Florian Schmitt): check if angle in lower case is correct
		map.put("angle", ValueType.INTEGER.create("Length", null, true, 0));
		Core core = new CoreImpl(map);
		EntityFactoryImpl ef = new EntityFactoryImpl(core);
		PhysicalDimension physicalDimension = ef.createPhysicalDimension("physdim");

		map = new HashMap<String, Value>();
		map.put("Offset", ValueType.DOUBLE.create("Length", null, true, 0d));
		map.put("Factor", ValueType.DOUBLE.create("Length", null, true, 0d));
		map.put("Name", ValueType.STRING.create("Name", null, true, "unitname"));
		core = new CoreImpl(map);
		ef = new EntityFactoryImpl(core);
		Unit unit = ef.createUnit("unit", physicalDimension);

		map = new HashMap<String, Value>();
		map.put("Name", ValueType.STRING.create("Name", null, true, "quantname"));
		map.put("Version", ValueType.STRING.create("Version", null, true, "4711"));
		map.put("DateCreated", ValueType.DATE.create("DateCreated", null, true, null));
		map.put("DefaultRank", ValueType.INTEGER.create("DefaultRank", null, true, 5));
		map.put("DefDimension", ValueType.INTEGER_SEQUENCE.create("DefDimension", null, true, new int[] { 5 }));
		map.put("DefTypeSize", ValueType.INTEGER.create("DefTypeSize", null, true, 8));
		map.put("DefMQName", ValueType.STRING.create("DefMQName", null, true, "mqname"));
		map.put("DefDataType", new Value(ValueType.ENUMERATION, "DefDataType", "", true, ScalarType.DOUBLE,
				ScalarType.class, ScalarType.DOUBLE, EnumRegistry.SCALAR_TYPE));
		map.put("ValidFlag", new Value(ValueType.ENUMERATION, "ValidFlag", "", false, VersionState.ARCHIVED,
				VersionState.class, null, EnumRegistry.VERSION_STATE));
		map.put("Description", ValueType.STRING.create("Description", null, true, null));
		core = new CoreImpl(map);
		ef = new EntityFactoryImpl(core);
		Quantity quantity = ef.createQuantity("quantity", unit);
		// Note that default values are set in createQuantity and thus should
		// differ from above.
		assertEquals(ScalarType.FLOAT, quantity.getDefaultScalarType());
		assertEquals((Integer) 1, quantity.getDefaultRank());
		assertEquals("quantity", quantity.getName());
		assertEquals(unit, quantity.getDefaultUnit());

		map = new HashMap<String, Value>();
		map.put("Name", ValueType.STRING.create("Name", null, true, null));
		map.put("DateCreated", ValueType.DATE.create("DateCreated", null, true, null));
		core = new CoreImpl(map);
		ef = new EntityFactoryImpl(core);
		Test test = ef.createTest("mytest");

		map = new HashMap<String, Value>();
		map.put("Name", ValueType.STRING.create("Name", null, true, null));
		map.put("DateCreated", ValueType.DATE.create("DateCreated", null, true, null));
		map.put("Optional", ValueType.BOOLEAN.create("Optional", null, true, null));
		map.put("Sortindex", ValueType.INTEGER.create("Sortindex", null, true, null));
		core = new CoreImpl(map);
		ef = new EntityFactoryImpl(core);
		TestStep testStep = ef.createTestStep("teststep", test);

		map = new HashMap<String, Value>();
		map.put("Name", ValueType.STRING.create("Name", null, true, null));
		map.put("DateCreated", ValueType.DATE.create("DateCreated", null, true, null));
		core = new CoreImpl(map);
		ef = new EntityFactoryImpl(core);
		Measurement measurement = ef.createMeasurement("measurement", testStep, Collections.emptyList());

		map = new HashMap<String, Value>();
		map.put("Name", ValueType.STRING.create("Name", null, true, null));
		map.put("Description", ValueType.STRING.create("Description", null, true, null));
		map.put("Interpolation", new Value(ValueType.ENUMERATION, "Interpolation", "", true, null, Interpolation.class,
				null, EnumRegistry.SCALAR_TYPE));
		map.put("DataType", new Value(ValueType.ENUMERATION, "DataType", "", true, null, ScalarType.class, null,
				EnumRegistry.SCALAR_TYPE));

		map.put("TypeSize", ValueType.INTEGER.create("TypeSize", null, true, null));
		map.put("Rank", ValueType.INTEGER.create("Rank", null, true, null));
		core = new CoreImpl(map);
		ef = new EntityFactoryImpl(core);
		Channel channel = ef.createChannel("channel", measurement, quantity);
		assertEquals(Interpolation.NONE, channel.getInterpolation());
	}

	/**
	 * basic test of some ValuType methods.
	 */
	@org.junit.Test
	public void valueType() {
		EnumRegistry.getInstance();
		assertEquals(ValueType.SHORT_SEQUENCE.toSingleType(), ValueType.SHORT);
		assertEquals(ValueType.DATE.toSequenceType(), ValueType.DATE_SEQUENCE);
		assertEquals(ValueType.ENUMERATION_SEQUENCE.toSingleType(), ValueType.ENUMERATION);
		assertEquals(ValueType.ENUMERATION.toSingleType(), ValueType.ENUMERATION);
		assertEquals(ValueType.ENUMERATION, ValueType.ENUMERATION.valueOf(ValueType.ENUMERATION.toSingleType().name()));
		assertEquals(Float.class, ValueType.FLOAT.getValueClass());
		assertTrue(ValueType.DOUBLE.isAnyFloatType());
		assertTrue(ValueType.DOUBLE.isDouble());
		assertFalse(ValueType.DOUBLE.isFloat());
		assertTrue(ValueType.FLOAT.isAnyFloatType());
		assertFalse(ValueType.INTEGER.isAnyFloatType());
		assertTrue(ValueType.INTEGER.isInteger());
		assertFalse(ValueType.FLOAT.isInteger());
		assertTrue(ValueType.FLOAT.isFloat());
	}

	/**
	 * basic tests of some ScalarType methods
	 */
	@org.junit.Test
	public void scalarType() {
		assertEquals(ValueType.BYTE_SEQUENCE, ScalarType.BYTE.toValueType());
		assertTrue(ScalarType.LONG.isLong());
		assertFalse(ScalarType.DOUBLE_COMPLEX.isLong());
	}

	/**
	 * basic test of some SequenceReprestentaion methods
	 */
	@org.junit.Test
	public void sequenceRepresentation() {
		assertNotEquals(SequenceRepresentation.EXPLICIT, SequenceRepresentation.IMPLICIT_CONSTANT);
		assertTrue(SequenceRepresentation.EXPLICIT.isExplicit());
		assertTrue(SequenceRepresentation.EXPLICIT_EXTERNAL.isExplicit());
		assertTrue(SequenceRepresentation.EXPLICIT_EXTERNAL.isExternal());
		assertFalse(SequenceRepresentation.EXPLICIT_EXTERNAL.isImplicit());
		assertEquals(SequenceRepresentation.EXPLICIT.ordinal(), (Integer) 0);
		assertEquals((Integer) 5, SequenceRepresentation.RAW_POLYNOMIAL.ordinal());
		assertEquals((Integer) 7, SequenceRepresentation.EXPLICIT_EXTERNAL.ordinal());
		assertEquals((Integer) 9, SequenceRepresentation.RAW_POLYNOMIAL_EXTERNAL.ordinal());
		assertEquals((Integer) 10, SequenceRepresentation.RAW_LINEAR_CALIBRATED.ordinal());
		assertEquals((Integer) 11, SequenceRepresentation.RAW_LINEAR_CALIBRATED_EXTERNAL.ordinal());
	}

	/**
	 * basic tests of TypeSpecification
	 */
	@org.junit.Test
	public void typeSpecification() {
		assertNotEquals(TypeSpecification.BIT_INTEGER, TypeSpecification.BIT_FLOAT_BEO);
		assertEquals(TypeSpecification.BOOLEAN, TypeSpecification.BOOLEAN);
	}

	/**
	 * basic tests of AxisTzpe methods
	 */
	@org.junit.Test
	public void axisType() {
		assertTrue(AxisType.X_AXIS.isXAxis());
		assertTrue(AxisType.Y_AXIS.isYAxis());
		assertTrue(AxisType.XY_AXIS.isXYAxis());
		assertFalse(AxisType.X_AXIS.isYAxis());
		assertFalse(AxisType.Y_AXIS.isXYAxis());
		assertFalse(AxisType.XY_AXIS.isXAxis());
	}

	/**
	 * basic tests of VersionState enumeration
	 */
	@org.junit.Test
	public void versionState() {
		assertFalse(VersionState.ARCHIVED.isEditable());
		assertTrue(VersionState.ARCHIVED.isArchived());
		assertFalse(VersionState.ARCHIVED.isValid());
		assertTrue(VersionState.EDITABLE.isEditable());
		assertFalse(VersionState.EDITABLE.isArchived());
		assertFalse(VersionState.EDITABLE.isValid());
		assertFalse(VersionState.VALID.isEditable());
		assertFalse(VersionState.VALID.isArchived());
		assertTrue(VersionState.VALID.isValid());
	}

	/**
	 * basic tests of interpolation enumeration
	 */
	@org.junit.Test
	public void interpolation() {
		assertTrue(Interpolation.LINEAR.isLinear());
		assertFalse(Interpolation.NONE.isSpecific());
		assertEquals(Interpolation.NONE.ordinal(), (Integer) 0);
		assertEquals(Interpolation.LINEAR.ordinal(), (Integer) 1);
		assertEquals(Interpolation.SPECIFIC.ordinal(), (Integer) 2);
	}

	@org.junit.Test
	public void enumRegistry() {
		EnumRegistry er = EnumRegistry.getInstance();
		assertTrue(er.get("TestSourceName", "Interpolation") != null);
	}

	@org.junit.Test
	public void measuredValue() {
		Value measured = new Value(ValueType.STRING, "Foo", null, true, "abc", String.class, "abc", null);

		assertEquals("abc", measured.extract());
		assertTrue(measured.isValid());

		assertEquals("abc", measured.extract(ContextState.MEASURED));
		assertTrue(measured.isValid(ContextState.MEASURED));

		assertNull(measured.extract(ContextState.ORDERED));
		assertFalse(measured.isValid(ContextState.ORDERED));

		measured.set("xyz");
		assertEquals("xyz", measured.extract());
		assertEquals("xyz", measured.extract(ContextState.MEASURED));
	}

	@org.junit.Test
	public void orderedValue() {
		Value ordered = new Value(ValueType.STRING, "Foo", null, false, true, null, "abc", String.class, null, null);

		assertEquals("abc", ordered.extract(ContextState.ORDERED));
		assertTrue(ordered.isValid(ContextState.ORDERED));

		assertNull(ordered.extract(ContextState.MEASURED));
		assertFalse(ordered.isValid(ContextState.MEASURED));

		ordered.set(ContextState.ORDERED, "xyz");
		assertNull(ordered.extract());
		assertEquals("xyz", ordered.extract(ContextState.ORDERED));
	}

	@org.junit.Test
	public void swapValue() {
		Value value = new Value(ValueType.STRING, "Foo", null, true, true, "abc", "xyz", String.class, null, null);

		assertEquals("abc", value.extract(ContextState.MEASURED));
		assertTrue(value.isValid(ContextState.MEASURED));

		assertEquals("xyz", value.extract(ContextState.ORDERED));
		assertTrue(value.isValid(ContextState.ORDERED));

		value.swapContext();

		assertEquals("abc", value.extract(ContextState.ORDERED));
		assertTrue(value.isValid(ContextState.ORDERED));

		assertEquals("xyz", value.extract(ContextState.MEASURED));
		assertTrue(value.isValid(ContextState.MEASURED));
	}

	@org.junit.Test
	public void mergeMeasuredValues() {
		Value measuredFirst = new Value(ValueType.STRING, "Foo", null, true, "abc", String.class, null, null);

		Value measuredSecond = new Value(ValueType.STRING, "Foo", null, true, "abc", String.class, null, null);

		Value merged = measuredFirst.merge(measuredSecond);
		assertEquals("abc", merged.extract());
		assertTrue(merged.isValid());
	}

	@org.junit.Test(expected = IllegalArgumentException.class)
	public void mergeInvalidNameMeasuredValues() {
		Value measuredFirst = new Value(ValueType.STRING, "Foo", null, true, "abc", String.class, null, null);

		Value measuredSecond = new Value(ValueType.STRING, "Bar", null, true, "abc", String.class, null, null);

		measuredFirst.merge(measuredSecond);
	}

	@org.junit.Test(expected = IllegalArgumentException.class)
	public void mergeInvalidUnitMeasuredValues() {
		Value measuredFirst = new Value(ValueType.STRING, "Foo", null, true, "abc", String.class, null, null);

		Value measuredSecond = new Value(ValueType.STRING, "Foo", "Unit", true, "abc", String.class, null, null);

		measuredFirst.merge(measuredSecond);
	}

	@org.junit.Test(expected = IllegalArgumentException.class)
	public void mergeInvalidTypeMeasuredValues() {
		Value measuredFirst = new Value(ValueType.STRING, "Foo", null, true, "abc", String.class, null, null);

		Value measuredSecond = new Value(ValueType.FLOAT, "Foo", null, true, "abc", String.class, null, null);

		measuredFirst.merge(measuredSecond);
	}

	@org.junit.Test
	public void mergeValidTargetMeasuredValues() {
		Value measuredFirst = new Value(ValueType.STRING, "Foo", null, true, "abc", String.class, null, null);

		Value measuredSecond = new Value(ValueType.STRING, "Foo", null, false, true, null, "xyz", String.class, null,
				null);

		assertEquals("abc", measuredFirst.extract());
		assertTrue(measuredFirst.isValid());
		assertNull(measuredFirst.extract(ContextState.ORDERED));
		assertFalse(measuredFirst.isValid(ContextState.ORDERED));

		assertNull(measuredSecond.extract());
		assertFalse(measuredSecond.isValid());
		assertEquals("xyz", measuredSecond.extract(ContextState.ORDERED));
		assertTrue(measuredSecond.isValid(ContextState.ORDERED));

		Value merged = measuredFirst.merge(measuredSecond);
		assertEquals("abc", merged.extract());
		assertTrue(merged.isValid());
		assertEquals("xyz", merged.extract(ContextState.ORDERED));
		assertTrue(merged.isValid(ContextState.ORDERED));
	}

}
