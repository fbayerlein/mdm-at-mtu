**Document History**

[%autowidth]
[options="header"]
|=======================
|Author |Date      |Version |Affects SW Version | Description
|Peak Solution GmbH  | 21.04.2021   |0.52  |5.2.0Mx | 
Initial version +    
|=======================

    This document is published under the Eclipse Public License 2.0: 
    https://www.eclipse.org/legal/epl-2.0/
    Copyright(c) 2020-2021, Peak Solution GmbH.


// == TODOs: List of open points

// * add Tags for test, teststep, measurement (reference predefined tags)

== Introduction

=== General
The openMDM Application is an open source software platform for measured data management systems. 
In compliance with the ASAM ODS standard, this
platform enables an ecosystem currently consisting
of automotive companies to build in-house application as well as application vendors building commercial and open source solutions tools and systems.

The openMDM Application can serve many ASAM ODS compliant servers in
parallel for data ingest, data exploration, test management, user and access management and more.
Security mechanisms at data and functional level
protect the stored information against unauthorized access.

In summary, it implements an industry standard
platform for tools and products for ODS compliant
products and tool chains.

image:openmdm.png[]

The development of the platform and concepts is driven by the link:https://wiki.eclipse.org/Open-Measured-Data-Management-WG[openMDM Eclipse Working Group]. 
The source code, documentaton and concepts are part of the open source link:https://projects.eclipse.org/projects/automotive.mdmbl[Eclipse mdmbl project] published under the business friendly Eclipse Public License 2.0 (link:https://www.eclipse.org/legal/epl-2.0/[EPL-2.0]).

=== Features

The openMDM Application can be used to manage test and measurement data for a broad variety of domains. The openMDM web client offers a web based graphical user interface, that offers the following features: 

- <<navigator, navigate through data structures>> 
- <<search, search and retrieve of test and measurement data>>
- <<detail_viewer, inspect data (_Detail Viewer_)>>
- <<shopping_basket, collect data for further processing (_Shopping Basket_)>>
- <<visualize_data, visualize data (_Chart Viewer_)>>
- <<export_files, upload / download files, update, preview or delete existing files>>



With extended administration rights, a user can:

- configure preferences 
- configure external systems

Refer to <<admin_function, this chapter>>.

NOTE: This document includes screenshots of the link:https://demo.openmdm.org/[openMDM Demo Application] which is provided by the Eclipse openMDM Working Group. Please be aware that different domain specific openMDM applications have different data models and so different meta and measurement data.

NOTE: The test and measurement data are usually imported by server processes and stored in an standardized ASAM ODS data source. The way of storing the data is not part of this user manual.
