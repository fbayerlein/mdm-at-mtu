[[known_problems]]
== Known (setup) problems and solutions

=== Glassfish

==== Glassfish - WEB9102: Login failed: Lookup failed

If you cannot login to the webclient and the Glassfish log shows the following error:

 WEB9102: Web Login Failed: com.sun.enterprise.security.auth.login.common.LoginException: Login failed: Lookup failed for 'java:global/org.eclipse.mdm.nucleus/ConnectorService!org.eclipse.mdm.connector.boundary.ConnectorService' in SerialContext[myEnv={java.naming.factory.initial=com.sun.enterprise.naming.impl.SerialInitContextFactory, java.naming.factory.state=com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl, java.naming.factory.url.pkgs=com.sun.enterprise.naming}|#]

TIP: Check that the war file was deployed with the Application Name “org.eclipse.mdm.nucleus”

==== Glassfish 4 only  - java.lang.ClassNotFoundException

If you run into

    "java.lang.ClassNotFoundException: javax.xml.parsers.ParserConfigurationException not found by org.eclipse.persistence.moxy"

this is a bug described in +
https://bugs.eclipse.org/bugs/show_bug.cgi?id=463169 +
and +
https://java.net/jira/browse/GLASSFISH-21440.

The **solution** is to replace +
`GLASSFISH_HOME/glassfish/modules/org.eclipse.persistence.moxy.jar` +
with this: +
http://central.maven.org/maven2/org/eclipse/persistence/org.eclipse.persistence.moxy/2.6.1/org.eclipse.persistence.moxy-2.6.1.jar

[[glassfish4_problem]]
==== Glassfish 4 only - org.osgi.framework.BundleException

If you run into

    “org.osgi.framework.BundleException: Unresolved constraint in bundle com.fasterxml.jackson.module.jackson-module-jaxb-annotations”

This is a compatibility problem with the installed jackson libraries. The **solution** is to replace +
`GLASSFISH_HOME/glassfish/modules/jackson-*.jar` +
with these files:

* http://central.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.8.1/jackson-annotations-2.8.1.jar
* http://central.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.8.1/jackson-core-2.8.1.jar
* http://central.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.8.1/jackson-databind-2.8.1.jar
* http://central.maven.org/maven2/com/fasterxml/jackson/jaxrs/jackson-jaxrs-base/2.8.1/jackson-jaxrs-base-2.8.1.jar
* http://central.maven.org/maven2/com/fasterxml/jackson/jaxrs/jackson-jaxrs-json-provider/2.8.1/jackson-jaxrs-json-provider-2.8.1.jar

Rename the jars to the replaced ones (remove the versions).

==== Glassfish 4 only - java.lang.NoSuchMethodError

If you are using the REST-API to access the MDM entities (CREATE und UPDATE) and encounter the following error:

    java.lang.NoSuchMethodError: com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer.<init>

replace the jackson libraries as mentioned <<glassfish4_problem, here>>.
