= Installation and Setup Guide for the openMDM(R) Application
Eclipse mdmbl project
:version:, {docdate}: {build-revision}

:toc:

include::010_documentHistory.adoc[]

include::020_releases.adoc[]

include::030_introduction.adoc[]

include::040_prerequisites.adoc[]

include::050_getAndBuildCode.adoc[]

include::060_preInstallations.adoc[]

include::070_installApplication.adoc[]

include::080_startApplication.adoc[]

include::090_knownProblems.adoc[]

include::100_ExtendedRunConfig.adoc[]

include::110_DevelopmentRules.adoc[]

include::120_WebFrontend.adoc[]

include::130_SAMLIntegration.adoc[]

include::140_OIDCIntegration.adoc[]

include::150_UserRoleSync.adoc[]

include::170_oss_governance.adoc[]

