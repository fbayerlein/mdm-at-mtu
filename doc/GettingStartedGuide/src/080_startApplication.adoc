== Start application

Start application:

* start ORB ($JAVA_HOME/bin/orbd -ORBInitialPort 2809)  
(skip this if you are using Peak ODS Server with no NAMESERVICE specified in the server.properties)
* start the database for the ODS Server (if necessary)
* start the ODS server
* start Elasticsearch
* start Glassfish (including database for UPS) 
* start Glassfish
** `asadmin start-database`
** `asadmin start-domain`

THe browser URL is http://localhost:8080/org.eclipse.mdm.nucleus.  +

You should see the openMDM LoginPage. Lock in with for user/ password from the userXX table in  zhe database, e.g. sa/sa.

=== Troubleshooting
Look into the Logfiles:

Glassfish:
$glassfish_root/domains/domain1/logs/server.log
Derby DB for User Preference Service:
$glassfish_root/databases/derby.log
The Logfiles of your ODS compatible datasource




