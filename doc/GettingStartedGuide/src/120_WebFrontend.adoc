== Web Frontend Configuration

[[nodeprovider-web]]
=== System Preferences

==== Node Provider Configuration

To create a Node Provider in the Web UI, an administrative user must login into the application. After login, it is necessary to switch to the administration page and add a system preference to the configuration.
The configuration must be located in the system scope and the key must be prefixed with `nodeprovider.`.

image::images/NodeproviderPreference.png[]

Example Node provider key: +

* nodeprovider.generic_measured


The internal default node provider can be deactivated by setting the following key to 'false':

* nodeprovider.config.default_is_active

A new default node provider can be set by providing the full key of that node provider as value for the following key:

* nodeprovider.config.use_as_default

Following the above example the value would be:

image::images/DefaultNodeProvider.png[]


==== Search Preferences

For the search default values a System Preference can be configured.

===== Result types

To configure the "Result type" default value:

* **Key:** search.default_values
* **Value:** a valid JSON string e.g.:
----
{"resultType": "Measurement"}
----
* Possible **resultType** values are:
    - Test
    - TestStep
    - Measurement
    - ChannelGroup
    - Channel

Following the above example the value would be:

image::images/SearchDefaultPreferences.png[]

===== Case Sensitivity =====

Search and filtering can be made case sensitive by adding the following system preference to true.

* **Key:** search.case_sensitive
* **Value:** true

===== JSON

To activate the button "JSON" which will provide the search request as JSON string use the following configuration:

* **Key:** search.button.request-as-json
* **Value:** true


==== Quick Viewer Preferences

In the Quick Viewer the amount of loaded channels will be restricted to **10** channels, if a channel group or measurement was selected.
In case this default setting does not fit your needs, you can change it with:

* **Key:** chart-viewer.channels.max-display
* **Value:** we recommend any cardinal number greater than 0 and smaller than 51, e.g.: **5**

Following the above example the value would be:

image::images/QuickViewerDefaultPreferences.png[]

=== I18N Configuration
I18N support for web frontend, languages available: English, German.

For adding a new language see: +
org.eclipse.mdm/nucleus/webclient/src/main/webapp/README_I18N.md

Files with translations: +
org.eclipse.mdm/nucleus/webclient/src/main/webapp/src/assets/i18n/*.json    

=== Icons Configuration
All icons for the web frontend are taken from the FAMFAMFAM Silk Icons library, version 1.3   (http://www.famfamfam.com/lab/icons/silk/). This library is licensed under the Creative Commons Attribution 3.0 License (https://creativecommons.org/licenses/by/3.0/). This library was approved by the Eclipse Foundation, see CQ 17759.


The mapping is defined in:
org.eclipse.mdm/nucleus/webclient/src/main/webapp/src/styles.css

The mapping from the ao elements to FAMFAMFAM icons:

[%autowidth]
[options="header"]
|=======================
|ASAM ODS 5.3.0 Base Element Definitions |Icon|FamFamFam Silk Icons, V1.3

|AoFile
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/page_white_stack.png[]
|page_white_stack

|AoEnvironment
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/database.png[]
|database

|folder
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/folder.png[]
|folder

|AoMeasurementQuantity
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/chart_curve_go.png[]
|chart_curve_go

|AoMeasurement
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/chart_curve.png[]
|chart_curve

|AoParameter
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/shape_square.png[]
|shape_square

|AoParameterSet
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/shape_move_forwards.png[]
|shape_move_forwards

|project
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/house.png[]
|house

|structure level
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/paste_plain.png[]
|paste_plain

|AoSubmatrix
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/calendar.png[]
|calendar

|AoTestEquipmentPart
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/monitor.png[]
|monitor

|AoTestEquipment
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/computer.png[]
|computer

|AoTest
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/brick_add.png[]
|brick_add

|AoTestSequencePart
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/shape_move_front.png[]
|shape_move_front

|AoTestSequence
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/page_white_stack.png[]
|page_white_stack

|test step
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/brick.png[]
|brick

|AoUnitUnderTestPart
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/cog.png[]
|cog

|AoUnitUnderTest
|image:../../../nucleus/webclient/src/main/webapp/src/assets/famfamfam_icons/cog_go.png[]
|cog_go
|=======================

