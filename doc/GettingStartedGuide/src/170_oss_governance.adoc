== OSS Governance

Eclipse mdmbl is an open source project hosted by the Eclipse Foundation licensed under the http://www.eclipse.org/legal/epl-2.0[EPL-2.0]. The legal obligations of the content must be observed in all forms of which the content is available.

The source of truth is always the https://www.eclipse.org/projects/handbook[Eclipse Foundation Project Handbook].

=== Legal Documentation

The following files must be part of your repository root folder:

- LICENSE
- NOTICE.md
- DEPENDENCIES
- SECURITY.md
- CONTRIBUTING.md
- CODE_OF_CONDUCT.md

See https://www.eclipse.org/projects/tools/documentation.php?id=automotive.mdmbl[EF - Legal Documentation Generator]

==== LICENSE FILE

Project license: EPL-2.0

- SPDX-License-Identifier: EPL-2.0
- http://www.eclipse.org/legal/epl-2.0[License Text]

See the https://www.eclipse.org/projects/handbook/#legaldoc-license[Handbook#legaldoc-license].

For documentation the Creative Commons Attribution 4.0 International (CC BY 4.0) is recommended.

==== NOTICE FILE

- Add the link to your repository
- Add the link(s) to your SBOM, e.g. the DEPENDENCY file (one or more)
- Add information for third party content checks, if not covered by the Dash Tool (e.g. IP checks for icons, fonts, ...)

==== DEPENDENCY FILE

Note: Third-party dependencies need to be checked regularly to reflect your code changes. The DEPENDENCY file must be updated accordingly. This is recommended for every contribution (e.g. PR) whenever possible.

Use the [Eclipse Dash License Tool](https://www.eclipse.org/projects/handbook/#ip-license-tool)

If different technologies / package managers (e.g. npm and maven) are used you are free to have several dependency files. Use the naming convention DEPENDENCY_XYZ, e.g. DEPENDENCY_FRONTEND and DEPENDENCY_BACKEND.

==== SECURITY FILE

- The security file should at least contain information, where/how to report a vulnerability issue
- Add this https://www.eclipse.org/security/[template]

See the https://www.eclipse.org/projects/handbook/#vulnerability[Handbook#vulnerability].

==== CONTRIBUTOR GUIDE

See the https://www.eclipse.org/projects/handbook/#legaldoc-contributor[Handbook#legaldoc-contributor]

==== CODE OF CONDUCT

See the https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php[CODE OF CONDUCT]
and here in https://raw.githubusercontent.com/eclipse/.github/master/CODE_OF_CONDUCT.md[md format].


=== Copyright and Licence header

See the https://www.eclipse.org/projects/handbook/#ip-copyright-headers[Handbook#ip-copyright-headers]

Example (Java):

    /********************************************************************************
    * Copyright (c) 20xx,20yy Contributors to the Eclipse Foundation
    *
    * See the NOTICE file(s) distributed with this work for additional
    * information regarding copyright ownership.
    *
    * This program and the accompanying materials are made available under the
    * terms of the Eclipse Public License v. 2.0 which is available at
    * http://www.eclipse.org/legal/epl-2.0.
    *
    * SPDX-License-Identifier: EPL-2.0
    *
    ********************************************************************************/

Note: +
Update the year in the copyright header at the start of each new year!

Example: +
Copyright (c) 202x, **<new year>** Contributors to the Eclipse Foundation


=== IP checks for project content

Project content is roughly said, all content you created, e.g. code, scripts, documentation and configuration files.
See the exact explanation for **project content** in the https://www.eclipse.org/projects/handbook/#ip-project-content[Handbook#ip-project-content].

Contributions by Eclipse contributors (signed https://www.eclipse.org/projects/handbook/#contributing-eca[ECA]) must be received via an Eclipse Foundation channel (e.g. Gerrit, GitHub pull request, attachment on an issue).

A mdmbl committer **can** accept the contribution without further investigation if all of the following conditions are met:

- Was developed from scratch; written 100% by Eclipse contributors (no copied code, e.g. from StackOverflow or generated AI code e.g. from GitHub Copilot or ChatGPT)
- Was submitted under the terms of the project license (e.g. legal doc, copyright & license header)
- Contains no cryptography; and
- It contains fewer than 1,000 lines of code, configuration files, and other forms of source code.

If not, a project committer **must** engage with the IP Team to request an https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/new[IP review for Code Contributions], choose the "vet-project" template.

[[ip-third-party]]
=== IP checks for 3rd party content

The term third-party content refers to any content that is leveraged by the Eclipse project that is not produced and/or maintained as an asset of the project. This includes content from other Eclipse projects. See the complete explanation of https://www.eclipse.org/projects/handbook/#ip-third-party[third-party content].

All third-party content has to be checked and approved by the Eclipse Foundation IP Team. There are two ways:

- Creating an IP issue manually (e.g. fonts, images, ...)
- Using the Eclipse Dash License Tool to create IP issues in an automated way (libraries)

All third party content used has to be documented in the NOTICE file or in the DEPENDENCY file.


**Note:** Only project committers can open IP issues, manually or via the Dash Tool!

==== Checking libraries using the Eclipse Dash License Tool

You can request the status of your used libraries via the https://github.com/eclipse/dash-licenses/blob/master/README.md[Dash Licence Tool], see also the https://www.eclipse.org/projects/handbook/#ip-license-tool[handbook].

**Steps:**

* Download the latest version: README => Get It => Download Link
* For every repository / technology:
  - Create the list of transitive dependencies, see the README
  - Run the Dash Tool with the parameters "-project automotive.mdmbl" and "-summary DEPENDENCIES" (or use the maven plugin)
* Check for libraries with status "rejected" in the DEPENDENCIES file, if present, the dependency cannot be used and has to be removed.
* Check for libraries with status "restricted", if so request checks by automatically creating issues via the Dash Tool:
  - include the -review option;
  - pass the API token via the -token option; and
  - pass the project id via the -project option.
* Track the https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/?search=automotive.mdmbl&sort=created_date&state=opened&first_page_size=20[issues created by the Dash Tool]
* Provide support if an issue is labeled with "Help wanted"
* Add the summary as DEPENDENCY file to the according repository (root level)

**Important Notes:**

- Get your API Token (see README of the Dash Tool), note that only committers can get an API Token
- Do **NOT** share your API Token!
- DO **NOT** integrate into automatic builds the "Automatic IP Team Review Requests" (creation of ip tickets: " -review option") via your **personal** API Token
- To integrate in your automatic builds: request a dash-bot via the EF Helpdesk.

==== Check the mdmbl backend with the Dash Tool

Generate / update the DEPENDENCIES_backend file.

* Change into your local 'org.eclipse.mdm' repository, for each module get the dependency list via the gradle tooling and concatenate the outputs:

    ./gradlew nucleus:dependencies | grep -Poh "(?<=\s)[\w.-]+:[\w.-]+:[^:\s]+" | grep -v "^org.eclipse" | sort | uniq > depend_backend_all.txt
    ./gradlew api:csvadapter:dependencies | grep -Poh "(?<=\s)[\w.-]+:[\w.-]+:[^:\s]+" | grep -v "^org.eclipse" | sort | uniq >> depend_backend_all.txt
    ./gradlew api:atfxadapter:dependencies | grep -Poh "(?<=\s)[\w.-]+:[\w.-]+:[^:\s]+" | grep -v "^org.eclipse" | sort | uniq >> depend_backend_all.txt
    ./gradlew api:base:dependencies | grep -Poh "(?<=\s)[\w.-]+:[\w.-]+:[^:\s]+" | grep -v "^org.eclipse" | sort | uniq >> depend_backend_all.txt
    ./gradlew api:odsadapter:dependencies | grep -Poh "(?<=\s)[\w.-]+:[\w.-]+:[^:\s]+" | grep -v "^org.eclipse" | sort | uniq >> depend_backend_all.txt

* Sort the output and remove duplicates:

    sort depend_backend_all.txt | uniq > depend_backend.txt

* Run the Dash Tool:

    java -jar dash.jar depend_backend.txt -project automotive.mdmbl -summary DEPENDENCIES_backend.csv

* Or run the Dash Tool for automated ticket creation :

    java -jar dash.jar depend_backend.txt -review -token <your_token> -project automotive.mdmbl

* Check the summary file / tickets as described above, update the file in your repo.

==== Check the mdmbl frontend with the Dash Tool

Generate / update the DEPENDENCIES_frontend file.

    java -jar dash.jar package-lock.json -project automotive.mdmbl -summary DEPENDENCIES_frontend.csv

    java -jar dash.jar package-lock.json -review -token <your_token> -project automotive.mdmbl

==== Checking other content (fonts, images, ...)

Other 3rd party content, e.g. fonts, icons, images has also to be approved by the Eclipse Foundation IP Team. https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/new[IP review], choose the "vet-third-party" template.

If the content is approved add the information to the NOTICE file. See also the NOTICE file for examples.

=== Legal information for distributions

The distribution form of software artifacts (often in a compiled form) generated from a project’s source code repositories must also include legal information.
The source of truth is always the https://www.eclipse.org/projects/handbook/

License, notice and (if existing) DEPENDENCIES files, must be included in the root of every distribution artifact (e.g. JAR file). In the most general case, these files will appear in the root of distribution unit, but the exact location varies by technology type.

For content delivered as Java archive (JAR) files, for example, the legal files should be placed in the META-INF directory.

When the distribution is an individual file (e.g. JavaScript), the file must contain a header with copyright and license information, and the best effort must be undertaken to associate the file with the notices (e.g. a link to the source repository in the header).

Project teams should consult with their Project Management Committee (PMC) for input and advice regarding alternate names and locations for legal documentation.

(Text from the https://www.eclipse.org/projects/handbook/#legaldoc-distribution[Eclipse Foundation Project Handbook])

===  Legal notice for end user content

Add an About Dialog (or similar) with the following information:

- Copyright statements
- License
- Trademarks owned by the Eclipse Foundation on behalf of the project
- The source code repositories
- Used 3rd party libraries
- When applicable — a cryptography notice

The information can be provided in a static way or you can link directly to the source repository as they already contain your legal information.

**Important:** The links to your repository have to reference the tagged version (*) / commit id the component delivery has been created / build from!

Example:

    [Your product name](URL to the YOUR repository)
    * License: EPL-2.0
    * Licence Path: <URL to the License in your repository (*)>
    * NOTICE: <URL to the NOTICE file in your repository (*)>
    * Source URL: <URL to the your repository (*)>
    * Commit ID: <the commit id the component delivery has been created / build from>
