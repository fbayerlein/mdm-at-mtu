/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.io.File;

/**
 * @author akn
 *
 */
public class ImporterJobObject extends JobObject {

	private File directory;
	private File stateFile;
	private File importFile;
	private boolean isFixStateFile;

	/**
	 * @param directory
	 * @param stateFile
	 */
	public ImporterJobObject(File directory, File stateFile, File importFile, boolean isFixStateFile) {
		super();
		this.directory = directory;
		this.stateFile = stateFile;
		this.importFile = importFile;
		this.isFixStateFile = isFixStateFile;
		setIdentifier(stateFile.getAbsolutePath());
	}

	/**
	 * @return the directory
	 */
	public File getDirectory() {
		return directory;
	}

	/**
	 * @param directory the directory to set
	 */
	public void setDirectory(File directory) {
		this.directory = directory;
	}

	/**
	 * @return the stateFile
	 */
	public File getStateFile() {
		return stateFile;
	}

	/**
	 * @param stateFile the stateFile to set
	 */
	public void setStateFile(File stateFile) {
		this.stateFile = stateFile;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public boolean isFixStateFile() {
		return isFixStateFile;
	}

	public void setFixStateFile(boolean isFixStateFile) {
		this.isFixStateFile = isFixStateFile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JobObject [directory=" + directory + ", stateFile=" + stateFile + "]";
	}

}
