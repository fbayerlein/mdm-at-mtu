/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

/**
 * @author akn
 *
 */
public class StateCount {

	private int all;

	private int scheduled;

	private int running;

	private int done;

	private int failed;

	private int unknown;

	/**
	 * @param all
	 * @param scheduled
	 * @param running
	 * @param done
	 * @param failed
	 * @param unknown
	 */
	public StateCount(int all, int scheduled, int running, int done, int failed, int unknown) {
		super();
		this.all = all;
		this.scheduled = scheduled;
		this.running = running;
		this.done = done;
		this.failed = failed;
		this.unknown = unknown;
	}

	/**
	 * 
	 */
	public StateCount() {
		super();
	}

	/**
	 * @return the all
	 */
	public int getAll() {
		return all;
	}

	/**
	 * @param all the all to set
	 */
	public void setAll(int all) {
		this.all = all;
	}

	/**
	 * @return the scheduled
	 */
	public int getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled(int scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return the running
	 */
	public int getRunning() {
		return running;
	}

	/**
	 * @param running the running to set
	 */
	public void setRunning(int running) {
		this.running = running;
	}

	/**
	 * @return the done
	 */
	public int getDone() {
		return done;
	}

	/**
	 * @param done the done to set
	 */
	public void setDone(int done) {
		this.done = done;
	}

	/**
	 * @return the failed
	 */
	public int getFailed() {
		return failed;
	}

	/**
	 * @param failed the failed to set
	 */
	public void setFailed(int failed) {
		this.failed = failed;
	}

	/**
	 * @return the unknown
	 */
	public int getUnknown() {
		return unknown;
	}

	/**
	 * @param unknown the unknown to set
	 */
	public void setUnknown(int unknown) {
		this.unknown = unknown;
	}

}
