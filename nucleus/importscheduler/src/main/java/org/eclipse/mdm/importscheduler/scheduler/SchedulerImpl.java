/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.eclipse.mdm.importscheduler.scheduler.util.StateFileHelper;
import org.eclipse.mdm.importscheduler.state.RunningImportHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * Implementation of a {@link Scheduler}
 * 
 * @author akn
 *
 */
public class SchedulerImpl implements Scheduler {

	private static final Logger LOG = LoggerFactory.getLogger(SchedulerImpl.class);

	private ConcurrentHashMap<File, JobInfo> jobMap = new ConcurrentHashMap<>();
	private final AtomicLong nextJobId;

	private RunnableJob runnable;

	private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

	private int maxParallelImports;

	private long runningImportTimeoutInMillis;

	private RunningImportHandler runningImportHandler;

	private ScheduledFuture<?> scheduledFuture;

	private String targetService;

	private String emailRicipient;

	/**
	 * @param pollingIntervalSeconds     the polling interval in seconds
	 * @param maxParallelImports
	 * @param runningImportTimeoutInMins the running import timeout in minutes
	 * @param targetService              TODO
	 * @param emailRicipient             TODO
	 * @throws SchedulerException
	 * 
	 */
	public SchedulerImpl(File rootDir, String stateFileName, long pollingIntervalSeconds, int maxParallelImports,
			AtomicLong nextJobId, RunningImportHandler runningImportHandler, int runningImportTimeoutInMins,
			String targetService, String emailRicipient, RunnableJob runnable) {
		this(rootDir, stateFileName, pollingIntervalSeconds, TimeUnit.SECONDS, maxParallelImports, nextJobId,
				runningImportHandler, runningImportTimeoutInMins, targetService, emailRicipient, runnable);
	}

	/**
	 * Used for Unit Tests.
	 * 
	 * @param rootDir
	 * @param stateFileName
	 * @param pollingInterval
	 * @param timeUnit
	 * @param maxParallelImports
	 * @param runningImportHandler
	 * @param runningImportTimeoutInMins
	 * @param targetService              TODO
	 * @param emailRicipient             TODO
	 * @param runnable
	 */
	SchedulerImpl(File rootDir, String stateFileName, long pollingInterval, TimeUnit timeUnit, int maxParallelImports,
			AtomicLong nextJobId, RunningImportHandler runningImportHandler, int runningImportTimeoutInMins,
			String targetService, String emailRicipient, RunnableJob runnable) {
		this.maxParallelImports = maxParallelImports;
		this.runnable = runnable;
		this.runningImportHandler = runningImportHandler;
		this.runningImportTimeoutInMillis = runningImportTimeoutInMins * 60 * 1000;
		this.targetService = targetService;
		this.emailRicipient = emailRicipient;
		this.nextJobId = nextJobId;

		scheduledFuture = executorService.scheduleAtFixedRate(new DirectoryPollingService(rootDir, this, stateFileName),
				0, pollingInterval, timeUnit);
	}

	public void stop() throws InterruptedException {
		scheduledFuture.cancel(false);
		executorService.awaitTermination(1, TimeUnit.SECONDS);
	}

	/**
	 * Used for Unit Tests.
	 * 
	 * @throws InterruptedException
	 */
	void stopNow() throws InterruptedException {
		scheduledFuture.cancel(false);
		executorService.shutdownNow();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler# getScheduledJobs()
	 */
	@Override
	public List<JobInfo> getScheduledJobs() throws SchedulerException {
		return getJobInfoByState(JobState.SCHEDULED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#getFailedJobs( )
	 */
	@Override
	public List<JobInfo> getFailedJobs() throws SchedulerException {
		return getJobInfoByState(JobState.FAILED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#getDoneJobs()
	 */
	@Override
	public List<JobInfo> getDoneJobs() throws SchedulerException {
		return getJobInfoByState(JobState.DONE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#getRunningJobs ()
	 */
	@Override
	public List<JobInfo> getRunningJobs() throws SchedulerException {
		return getJobInfoByState(JobState.RUNNING);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#deleteJob()
	 */
	@Override
	public void deleteJob(long jobId) throws SchedulerException {
		for (JobInfo jobInfo : jobMap.values()) {
			if (jobId == jobInfo.getJobId()) {
				StateFileHelper.removeJob(jobInfo, emailRicipient);
				jobMap.remove(JobObjectHelper.getDirectory(jobInfo.getJobObject()));
				LOG.debug("Job with id {} deleted!", jobInfo);
				break;
			}
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#rescheduleJob()
	 */
	@Override
	public void rescheduleJob(Long jobId) throws SchedulerException {
		for (JobInfo jobInfo : jobMap.values()) {
			if (jobId == jobInfo.getJobId()) {
				File importDirectory = JobObjectHelper.getStateFile(jobInfo.getJobObject()).getParentFile();
				JobInfo tmpJobInfo = StateFileHelper.getJobInfoFromFile(jobId, importDirectory,
						JobObjectHelper.getStateFile(jobInfo.getJobObject()), emailRicipient);
				if (tmpJobInfo != null) {
					// persist on filesystem
					tmpJobInfo.setJobState(JobState.SCHEDULED);
					StateFileHelper.writeJobState(tmpJobInfo, emailRicipient);
					jobMap.remove(JobObjectHelper.getDirectory(jobInfo.getJobObject()));
					LOG.debug("Job with id {} rescheduled!", jobInfo);
				} else {
					LOG.debug("FAILED rescheduling of Job with id {}!", jobInfo);
				}
				break;
			}
		}

	}

	/**
	 * 
	 * @param stateFile
	 * @throws SchedulerException
	 */
	@Override
	public void createOrUpdateJob(File stateFile) throws SchedulerException {

		File importDirectory = stateFile.getParentFile();

		JobInfo jobInfo = jobMap.get(importDirectory);

		if (jobInfo == null) {
			if (stateFile.exists()) {
				nextJobId.incrementAndGet();
				jobInfo = StateFileHelper.getJobInfoFromFile(nextJobId.get(), importDirectory, stateFile,
						emailRicipient);
				jobMap.put(importDirectory, jobInfo);
				LOG.trace("Job added: {} Job count: {}", jobInfo, jobMap.size());
			} else {
				LOG.trace("Job for stateFile {} not added. StateFile does not exist anymore.", stateFile);
			}
		} else {
			synchronized (jobInfo) {
				if (stateFile.exists()) {
					JobInfo tmpJobInfo = StateFileHelper.getJobInfoFromFile(-1, importDirectory, stateFile,
							emailRicipient);

					if (jobInfo.getJobState() != tmpJobInfo.getJobState()) {
						jobInfo.setInfo(tmpJobInfo.getInfo());
						jobInfo.setJobState(tmpJobInfo.getJobState());
						jobInfo.setJobObject(tmpJobInfo.getJobObject());
						jobInfo.setFileDate(tmpJobInfo.getFileDate());
						jobInfo.setProperties(tmpJobInfo.getProperties());
						jobInfo.resetThrowable();
						LOG.trace("Job updated: {} Job count: {}", jobInfo, jobMap.size());
					}
				} else {
					jobMap.remove(importDirectory);
					LOG.trace("Job removed: {}. StateFile {} does not exist. Job count: {}", jobInfo, stateFile,
							jobMap.size());
				}

			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#deleteJob(java
	 * .io.File)
	 */
	@Override
	public void deleteJob(File directory) throws SchedulerException {

		if (jobMap.get(directory) != null) {
			jobMap.remove(directory);
			LOG.debug("Job deleted: {}, Job count: {}", directory.getAbsolutePath(), jobMap.size());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#startJobs()
	 */
	@Override
	public void startJobs() throws SchedulerException {

		for (JobInfo jobInfo : getNextIdleJobs()) {
			LOG.debug("Job {} started!", jobInfo);
			ExecutionThread executionThread = new ExecutionThread(jobInfo, runnable, runningImportHandler,
					emailRicipient);
			executionThread.start();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.importscheduler.scheduler.Scheduler#clearNonExistingJobs(
	 * java.util.List)
	 */
	@Override
	public void clearNonExistingJobs() throws SchedulerException {
		Iterator<JobInfo> iter = jobMap.values().iterator();
		while (iter.hasNext()) {
			JobInfo jobInfo = iter.next();

			ImporterJobObject jobObject = (ImporterJobObject) jobInfo.getJobObject();

			if ((!jobObject.isFixStateFile() && !jobObject.getImportFile().exists())
					|| (jobObject.isFixStateFile() && !jobObject.getStateFile().exists())) {
				jobMap.remove(jobObject.getDirectory());
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#getAllJobs()
	 */
	@Override
	public List<JobInfo> getAllJobs() throws SchedulerException {

		List<JobInfo> returnVal = new ArrayList<>();
		for (JobInfo jobInfo : jobMap.values()) {
			returnVal.add(jobInfo);
		}
		// sort by path ascending
		Collections.sort(returnVal, (JobInfo j1, JobInfo j2) -> JobObjectHelper.getStateFile(j1.getJobObject())
				.toString().compareTo(JobObjectHelper.getStateFile(j2.getJobObject()).toString()));
		// sort by date descending
		Collections.sort(returnVal, (JobInfo j1, JobInfo j2) -> j2.getFileDate().compareTo(j1.getFileDate()));
		return returnVal;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#getJobCount()
	 */
	@Override
	public int getJobCount(String jobState, String stateFile, Date dateFrom, Date dateTo) throws SchedulerException {
		List<JobInfo> list = filterJobList(getAllJobs(), jobState, stateFile, dateFrom, dateTo);
		return list.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#getPagedJobs()
	 */
	@Override
	public List<JobInfo> getPagedJobs(int from, int to, String jobState, String stateFile, Date dateFrom, Date dateTo)
			throws SchedulerException {
		List<JobInfo> returnVal = new ArrayList<>();
		List<JobInfo> list = filterJobList(getAllJobs(), jobState, stateFile, dateFrom, dateTo);

		for (int i = from; i <= to && i < list.size(); i++) {
			returnVal.add(list.get(i));
		}
		return returnVal;
	}

	private List<JobInfo> filterJobList(List<JobInfo> list, String jobState, String stateFile, Date dateFrom,
			Date dateTo) {
		List<JobInfo> returnVal = list;
		if (jobState != null && !"null".equals(jobState) && !"undefined".equals(jobState)) {
			returnVal.removeIf(ji -> !ji.getJobState().getValue().equals(jobState));
		}

		if (!Strings.isNullOrEmpty(stateFile)) {
			returnVal.removeIf(
					ji -> !JobObjectHelper.getStateFile(ji.getJobObject()).getAbsolutePath().contains(stateFile));
		}

		if (dateFrom != null) {
			returnVal.removeIf(ji -> ji.getFileDate().before(dateFrom));
		}

		if (dateTo != null) {
			returnVal.removeIf(ji -> ji.getFileDate().after(dateTo));
		}

		return returnVal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.scheduler.Scheduler#getJobById()
	 */
	@Override
	public JobInfo getJobById(int id) throws SchedulerException {

		JobInfo returnVal = null;
		for (JobInfo jobInfo : jobMap.values()) {
			if (jobInfo.getJobId() == Integer.valueOf(id).longValue()) {
				returnVal = jobInfo;
				break;
			}
		}
		return returnVal;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.importscheduler.scheduler.Scheduler#getJobInfoByPath(java.
	 * lang.String)
	 */
	@Override
	public JobInfo getJobInfoByDirectory(String directory) throws SchedulerException {

		JobInfo returnVal = null;
		for (JobInfo jobInfo : jobMap.values()) {
			if (JobObjectHelper.getDirectory(jobInfo.getJobObject()).getAbsolutePath() == directory) {
				returnVal = jobInfo;
				break;
			}
		}
		return returnVal;
	}

	/**
	 * 
	 * @param jobState
	 * @return a List of {@link JobInfo}s in the queried state
	 */
	private List<JobInfo> getJobInfoByState(JobState jobState) {

		List<JobInfo> returnVal = new ArrayList<>();
		for (JobInfo jobInfo : jobMap.values()) {
			if (jobInfo.getJobState().equals(jobState)) {
				returnVal.add(jobInfo);
			}
		}
		return returnVal;

	}

	/**
	 * 
	 * @return all idled jobs (state = SCHEDULED)
	 */
	private List<JobInfo> getNextIdleJobs() {
		Date timeOutTimeStamp = new Date(System.currentTimeMillis() - runningImportTimeoutInMillis);
		runningImportHandler.removeRunningImportsOlderThen(timeOutTimeStamp, targetService);

		List<JobInfo> idleJobs = new ArrayList<>();
		for (JobInfo jobInfo : this.jobMap.values()) {
			if (jobInfo.getJobState().equals(JobState.SCHEDULED) && runningImportHandler.insertRunningImport(
					JobObjectHelper.getDirectory(jobInfo.getJobObject()).getAbsolutePath(), targetService,
					maxParallelImports)) {
				jobInfo.setJobState(JobState.RUNNING);
				idleJobs.add(jobInfo);
			}
		}

		return idleJobs;
	}
}
