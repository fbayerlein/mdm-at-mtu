/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * DAO of one job
 * 
 * @author akn
 *
 */
public class JobInfo {

	private long jobId;
	private boolean isFixStateFile;
	private JobState jobState;
	private String info;
	private Object jobObject;
	private Date fileDate;
	private Map<String, String> properties = new HashMap<String, String>();
	private Optional<Throwable> throwable = Optional.empty();

	private PropertyChangeSupport changes = new PropertyChangeSupport(this);

	/**
	 * 
	 * @param jobId     id of the job
	 * @param jobState  the {@link JobState}
	 * @param info      info/error message of the job
	 * @param directory the corresponding directory of the job
	 * @param stateFile the corresponding state file of the job
	 * @param fileDate  the file date of the state file
	 */
	public JobInfo(long jobId, JobState jobState, String info, Object jobObject, Date fileDate) {
		this(jobId, jobState, info, jobObject, fileDate, null);
	}

	/**
	 * 
	 * @param jobId      id of the job
	 * @param jobState   the {@link JobState}
	 * @param info       info/error message of the job
	 * @param directory  the corresponding directory of the job
	 * @param stateFile  the corresponding state file of the job
	 * @param fileDate   the file date of the state file
	 * @param properties properties of the job
	 * @param importFile
	 */
	public JobInfo(long jobId, JobState jobState, String info, Object jobObject, Date fileDate,
			Map<String, String> properties) {
		super();
		this.jobId = jobId;
		this.jobState = jobState;
		this.info = info;
		this.jobObject = jobObject;
		this.fileDate = fileDate;
		this.properties = properties;
	}

	/**
	 * @return the jobState
	 */
	public JobState getJobState() {
		return jobState;
	}

	/**
	 * @param jobState the jobState to set
	 */
	public void setJobState(JobState jobState) {
		JobState oldState = this.jobState;
		this.jobState = jobState;
		changes.firePropertyChange("jobState", oldState, jobState);
	}

	/**
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * @return the jobId
	 */
	public long getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public Date getFileDate() {
		return fileDate;
	}

	public void setFileDate(Date fileDate) {
		this.fileDate = fileDate;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = Optional.of(throwable);
	}

	public void resetThrowable() {
		this.throwable = Optional.empty();
	}

	public Optional<Throwable> getThrowable() {
		return throwable;
	}

	/**
	 * @return the properties
	 */
	public Map<String, String> getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	/**
	 * <<<<<<< HEAD
	 * 
	 * @return the jobObject
	 */
	public Object getJobObject() {
		return jobObject;
	}

	/**
	 * @param jobObject the jobObject to set
	 */
	public void setJobObject(Object jobObject) {
		this.jobObject = jobObject;
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		changes.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		changes.removePropertyChangeListener(l);
	}

	@Override
	public String toString() {
		return "JobInfo [jobId=" + jobId + ", isFixStateFile=" + isFixStateFile + ", jobState=" + jobState + ", info="
				+ info + ", jobObject=" + jobObject + ", fileDate=" + fileDate + ", properties=" + properties
				+ ", throwable=" + throwable + ", changes=" + changes + "]";
	}

}
