/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

/**
 * 
 * @author akn
 *
 */
public enum JobState {
	SCHEDULED("SCHEDULED"), RUNNING("RUNNING"), DONE("DONE"), FAILED("FAILED"), UNKNOWN("UNKNOWN");

	private final String value;

	/**
	 * @param value
	 */
	private JobState(String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 
	 * @param value state as string value
	 * @return the {@link JobState} of the given value, UNKNOWN if the state is
	 *         unknown
	 * @throws SchedulerException
	 */
	public static JobState getJobState(String value) throws SchedulerException {
		String trimedValue = value.trim();

		if ("SCHEDULED".equalsIgnoreCase(trimedValue)) {
			return JobState.SCHEDULED;
		} else if ("RUNNING".equalsIgnoreCase(trimedValue)) {
			return JobState.RUNNING;
		} else if ("DONE".equalsIgnoreCase(trimedValue)) {
			return JobState.DONE;
		} else if ("FAILED".equalsIgnoreCase(trimedValue)) {
			return JobState.FAILED;
		} else {
			return JobState.UNKNOWN;
		}
	}
}
