/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.eclipse.mdm.importscheduler.ImportScheduler;

/**
 * Intermediate class to interact with the import scheduler singleton
 * 
 * @author jkl
 *
 */
@Stateless
public class SchedulerService {

	@EJB
	private ImportScheduler importScheduler;

	public boolean isActive() {
		try {
			return importScheduler.getScheduler() != null && !importScheduler.getScheduler().isEmpty();
		} catch (SchedulerException e) {
			return false;
		}
	}

	public List<JobInfo> getAllJobs() throws SchedulerException {
		List<JobInfo> returnList = new ArrayList<>();

		for (Scheduler s : importScheduler.getScheduler()) {
			returnList.addAll(s.getAllJobs());
		}

		return returnList;
	}

	public int getJobCount(String jobState, String stateFile, Date dateFrom, Date dateTo) throws SchedulerException {
		int count = 0;

		for (Scheduler s : importScheduler.getScheduler()) {
			count += s.getJobCount(jobState, stateFile, dateFrom, dateTo);
		}

		return count;
	}

	public List<JobInfo> getPagedJobs(int from, int to, String jobState, String stateFile, Date dateFrom, Date dateTo)
			throws SchedulerException {

		List<JobInfo> returnList = new ArrayList<>();

		for (Scheduler s : importScheduler.getScheduler()) {
			returnList.addAll(s.getPagedJobs(from, to, jobState, stateFile, dateFrom, dateTo));
		}

		return returnList;
	}

	public JobInfo getJobById(int id) throws SchedulerException {
		for (Scheduler s : importScheduler.getScheduler()) {
			JobInfo jobById = s.getJobById(id);
			if (jobById != null) {
				return jobById;
			}
		}

		return null;
	}

	public void deleteJob(Long jobId) throws SchedulerException {
		for (Scheduler s : importScheduler.getScheduler()) {
			s.deleteJob(jobId);
		}

	}

	public void rescheduleJob(Long jobId) throws SchedulerException {
		for (Scheduler s : importScheduler.getScheduler()) {
			s.rescheduleJob(jobId);
		}
	}

	public StateCount getStateCount(String stateFile, Date dateFrom, Date dateTo) throws SchedulerException {

		List<JobInfo> allFiltersJobs = new ArrayList<>();

		for (Scheduler s : importScheduler.getScheduler()) {
			allFiltersJobs.addAll(s.getPagedJobs(0, Integer.MAX_VALUE, null, stateFile, dateFrom, dateTo));
		}

		int all = allFiltersJobs.size();
		int scheduled = 0;
		int running = 0;
		int done = 0;
		int failed = 0;
		int unknown = 0;

		for (JobInfo ji : allFiltersJobs) {
			switch (ji.getJobState()) {
			case SCHEDULED:
				scheduled++;
				break;
			case DONE:
				done++;
				break;
			case FAILED:
				failed++;
				break;
			case RUNNING:
				running++;
				break;
			default:
				unknown++;
				break;
			}
		}

		return new StateCount(all, scheduled, running, done, failed, unknown);
	}

	public void rescheduleJobOfLastDays(Long dayCount) throws SchedulerException {
		long timeInMillisReschedule = System.currentTimeMillis() - (dayCount * 24 * 60 * 60 * 1000);
		for (JobInfo job : getAllJobs()) {
			if (JobState.FAILED.equals(job.getJobState()) && job.getFileDate() != null
					&& job.getFileDate().getTime() >= timeInMillisReschedule) {
				rescheduleJob(job.getJobId());
			}
		}
	}

}
