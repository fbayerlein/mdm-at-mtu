/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler.entities;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.mdm.importscheduler.scheduler.JobInfo;

/**
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SchedulerEntityResponse {

	private List<JobInfo> jobs;

	/**
	 * Plain constructor
	 */
	public SchedulerEntityResponse() {

	}

	/**
	 * Constructor with a list of jobs
	 * 
	 * @param jobs
	 */
	public SchedulerEntityResponse(List<JobInfo> jobs) {
		this.jobs = jobs;
	}

	/**
	 * Getter for job infos
	 * 
	 * @return
	 */
	public List<JobInfo> getJobs() {
		return jobs;
	}
}