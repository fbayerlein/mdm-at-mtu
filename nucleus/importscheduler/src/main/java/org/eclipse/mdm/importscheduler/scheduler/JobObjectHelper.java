/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.io.File;

/**
 * @author akn
 *
 */
public class JobObjectHelper {

	public static File getDirectory(Object o) {
		ImporterJobObject jobObject = getJobObject(o);
		return jobObject.getDirectory();
	}

	public static File getStateFile(Object o) {
		ImporterJobObject jobObject = getJobObject(o);
		return jobObject.getStateFile();
	}

	private static ImporterJobObject getJobObject(Object o) {
		if (o instanceof ImporterJobObject) {
			return (ImporterJobObject) o;
		}
		throw new RuntimeException("Object cannot be cast to JobObject");
	}
}
