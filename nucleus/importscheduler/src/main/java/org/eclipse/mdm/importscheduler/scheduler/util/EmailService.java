/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler.util;

import java.util.Date;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * Email service class which will send emails via the JNDI interface from the
 * application server. It requires only two configuration parameters: One is the
 * JNDI name, the other is the email recipient.
 */
public class EmailService {

	private static final Logger LOG = LoggerFactory.getLogger(EmailService.class);

	private static final String PROPERTY_EMAIL_JNDI = "org.eclipse.mdm.importscheduler.email.jndi";

	private Session session;

	private static EmailService self;

	/**
	 * Constructor for the EJB
	 */
	public EmailService() {
		try {
			InitialContext ctx = new InitialContext();
			session = (Session) ctx.lookup(System.getProperty(PROPERTY_EMAIL_JNDI));
		} catch (Exception e) {
			LOG.error("Failed to lookup mail JNDI with name " + System.getProperty(PROPERTY_EMAIL_JNDI));
		}
	}

	/**
	 * Get a static instance of the email service
	 *
	 * @return Email sevice object
	 */
	public static EmailService getInstance() {
		if (self == null || self.session == null) {
			EmailService.self = new EmailService();
		}
		return self;
	}

	/**
	 * Send an email to the defined recipient
	 *
	 * @param title     the header of the email
	 * @param detail    the detailed mail body
	 * @param recipient TODO
	 */
	public void sendMail(String title, String detail, String recipient) {
		if (session == null || Strings.isNullOrEmpty(recipient)) {
			LOG.error("Mail service not configured!");
			return;
		}
		MimeMessage message = new MimeMessage(session);
		try {
			LOG.trace("Sending email to {}: {}\n{}", recipient, title, detail);
			message.setFrom(new InternetAddress(session.getProperty("mail.from")));
			InternetAddress[] address = { new InternetAddress(recipient) };
			message.setRecipients(Message.RecipientType.TO, address);
			message.setSubject(title);
			message.setSentDate(new Date());
			message.setContent(detail, "text/html; charset=utf-8");
			Transport.send(message);
		} catch (MessagingException ex) {
			LOG.error("Failed to send email.", ex);
		}
	}

}
