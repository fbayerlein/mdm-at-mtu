/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.mdm.importscheduler.ImportScheduler;
import org.eclipse.mdm.importscheduler.scheduler.ImporterJobObject;
import org.eclipse.mdm.importscheduler.scheduler.JobInfo;
import org.eclipse.mdm.importscheduler.scheduler.JobObjectHelper;
import org.eclipse.mdm.importscheduler.scheduler.JobState;
import org.eclipse.mdm.importscheduler.scheduler.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

/**
 * @author akn
 *
 */
public class StateFileHelper {

	private static final String SUFFIX_FAILED = ".failed";

	private static final String SUFFIX_DONE = ".done";

	private static final String SUFFIX_IMPORT = ".import";

	private static final String SUFFIX_RUNNING = ".running";

	private static final Logger LOG = LoggerFactory.getLogger(StateFileHelper.class);

	private static Pattern PROPERTY_PATTERN = Pattern.compile("(.*)=(.*)");

	private static final List<String> suffixList = new ArrayList<>(
			Arrays.asList(SUFFIX_DONE, SUFFIX_IMPORT, SUFFIX_FAILED));

	/**
	 * 
	 */
	private StateFileHelper() {
	}

	/**
	 * 
	 * @param id        of the job
	 * @param directory of the job
	 * @param stateFile file with the job state and furthe informations
	 * @return the {@link JobInfo}
	 * @throws SchedulerException
	 */
	public static JobInfo getJobInfoFromFile(long id, File directory, File stateFile, String recipient)
			throws SchedulerException {
		try (Stream<String> stream = Files.lines(stateFile.toPath())) {

			List<String> lines = stream.collect(Collectors.toList());

			JobState jobState = getJobStateFromFileName(stateFile.getName());

			if (jobState == null) {
				if (lines.isEmpty()) {
					jobState = JobState.UNKNOWN;
				} else {
					jobState = JobState.getJobState(lines.get(0));
				}
			}

			String info = "";
			Map<String, String> properties = new HashMap<>();
			int currLine = 0;

			String tmpInfo = "";

			for (String line : lines) {
				Matcher matcher = PROPERTY_PATTERN.matcher(line);

				if (matcher.matches()) {
					properties.put(matcher.group(1).trim(), matcher.group(2).trim());
				} else if (currLine > 0) {
					tmpInfo += line;
				}
				currLine++;
			}

			if (!JobState.SCHEDULED.equals(jobState)) {
				info = tmpInfo;
			}

			// fetch the file last modified date
			FileTime fileDate = Files.getLastModifiedTime(stateFile.toPath());
			Calendar fileCal = Calendar.getInstance();
			fileCal.setTimeInMillis(fileDate.toMillis());

			boolean isFixStateFile = stateFile != null && ImportScheduler.STATE_FILE.equals(stateFile.getName());

			File importFile = null;
			if (!isFixStateFile) {
				importFile = getImportFileFromStateFile(stateFile);
			}

			ImporterJobObject jobObject = new ImporterJobObject(directory, stateFile, importFile, isFixStateFile);

			JobInfo jobInfo = new JobInfo(id, jobState, info, jobObject, fileCal.getTime(), properties);

			return jobInfo;

		} catch (IOException e) {
			ExceptionReporter.sendMail("Get job info from file", Optional.of(e), directory, null, recipient);
			LOG.error(e.getLocalizedMessage(), e);
			throw new SchedulerException(e.getLocalizedMessage(), e);
		}

	}

	private static File getImportFileFromStateFile(File stateFile) {
		File importFile = null;
		String name = stateFile.getName();

		for (String suffix : suffixList) {

			int i = name.indexOf(suffix);

			if (i > 0) {
				Path p = Paths.get(stateFile.getParent(), name.substring(0, i));
				importFile = p.toFile();
				break;
			}
		}

		return importFile;
	}

	private static JobState getJobStateFromFileName(String name) {
		JobState jobState = null;

		if (name.endsWith(SUFFIX_IMPORT)) {
			jobState = JobState.SCHEDULED;
		} else if (name.endsWith(SUFFIX_DONE)) {
			jobState = JobState.DONE;
		} else if (name.endsWith(SUFFIX_FAILED)) {
			jobState = JobState.FAILED;
		} else if (name.endsWith(SUFFIX_RUNNING)) {
			jobState = JobState.RUNNING;
		}
		return jobState;
	}

	/**
	 * Writing the state file
	 * 
	 * @param jobInfo
	 * @throws SchedulerException
	 */
	public static void writeJobState(JobInfo jobInfo, String recipient) throws SchedulerException {
		jobInfo.setFileDate(new Date());

		if (jobInfo.getJobObject() != null && jobInfo.getJobObject() instanceof ImporterJobObject) {
			ImporterJobObject jobObject = (ImporterJobObject) jobInfo.getJobObject();
			Path path = Paths.get(jobObject.getDirectory().getAbsolutePath());
			if (Files.exists(path)) {
				synchronized (jobInfo) {
					writeScheduleFile(JobObjectHelper.getStateFile((jobInfo.getJobObject())),
							jobInfo.getJobState().getValue(), jobInfo.getProperties(), jobInfo.getInfo(),
							jobInfo.getThrowable(), recipient);
					LOG.debug("Job file written: {}", jobInfo);
				}
				JobState jobState = jobInfo.getJobState();

				if (!jobObject.isFixStateFile()) {
					if (jobObject.getStateFile() != null && jobObject.getStateFile().exists()) {
						jobObject.getStateFile().delete();
					}
				}

				if (jobObject.isFixStateFile() || (!jobObject.isFixStateFile() && !JobState.RUNNING.equals(jobState))) {
					File stateFile = jobObject.getStateFile();

					if (!jobObject.isFixStateFile()) {
						stateFile = createStateFileForImportFile(jobObject.getImportFile(), jobState);
					}
					synchronized (jobInfo) {
						writeScheduleFile(stateFile, jobState.getValue(), jobInfo.getProperties(), jobInfo.getInfo(),
								jobInfo.getThrowable(), recipient);
						LOG.debug("Job file written: {}", jobInfo);
					}
				}

			} else {
				LOG.debug("Job cannot be written as the path does not exist: {}", jobInfo);
			}
		}

	}

	private static File createStateFileForImportFile(File importFile, JobState jobState) throws SchedulerException {
		String suffix = getFileSuffixByJobState(jobState);
		String filePath = importFile.getAbsolutePath() + suffix;

		File file = new File(filePath);

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				throw new SchedulerException(e.getLocalizedMessage(), e);
			}
		}
		return file;
	}

	private static String getFileSuffixByJobState(JobState jobState) {
		String suffix = null;

		if (JobState.DONE.equals(jobState)) {
			suffix = SUFFIX_DONE;
		} else if (JobState.FAILED.equals(jobState)) {
			suffix = SUFFIX_FAILED;
		} else if (JobState.RUNNING.equals(jobState)) {
			suffix = SUFFIX_RUNNING;
		} else if (JobState.SCHEDULED.equals(jobState)) {
			suffix = SUFFIX_IMPORT;
		}
		return suffix;
	}

	public static void writeScheduleFile(File stateFile, String stateValue, Map<String, String> properties,
			String jobInfo, Optional<Throwable> throwable, String recipient) throws SchedulerException {
		try (Writer writer = new FileWriter(stateFile)) {
			List<String> lines = new ArrayList<>();
			lines.add(stateValue);
			if (!JobState.DONE.getValue().equals(stateValue) && properties != null) {
				lines.addAll(properties.entrySet().stream().filter(me -> "append".equalsIgnoreCase(me.getKey()))
						.map(me -> String.format("%s=%s", me.getKey(), me.getValue())).collect(Collectors.toList()));
			}

			lines.add(jobInfo);
			lines.add(throwable.map(Throwables::getStackTraceAsString).orElse(""));

			writer.write(lines.stream().collect(Collectors.joining("\n")));
			if (throwable.isPresent()) {
				ExceptionReporter.sendMail("Write job state", throwable, stateFile.getParentFile(), null, recipient);
			}
		} catch (IOException e) {
			ExceptionReporter.sendMail("Write job state error handler", Optional.of(e), stateFile.getParentFile(), null,
					recipient);
			LOG.error(e.getLocalizedMessage(), e);
			throw new SchedulerException(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Remove the complete job
	 * 
	 * @param jobInfo
	 */
	public static void removeJob(JobInfo jobInfo, String recipient) {
		synchronized (jobInfo) {
			try {
				deleteDirectory(JobObjectHelper.getDirectory(jobInfo.getJobObject()).getAbsolutePath());
			} catch (IOException e) {
				ExceptionReporter.sendMail("Remove job", Optional.of(e),
						JobObjectHelper.getDirectory(jobInfo.getJobObject()), null, recipient);
				LOG.error(e.getLocalizedMessage(), e);
				// Just log the error to properly remove the job
			}
			LOG.debug("Job deleted: {}", jobInfo);
		}
	}

	private static void deleteDirectory(String dirPath) throws IOException {
		Path path = Paths.get(dirPath);
		if (Files.exists(path)) {
			List<Path> files = Files.list(path).collect(Collectors.toList());
			// Delete all files first
			for (Path file : files) {
				if (file.toFile().isDirectory()) {
					deleteDirectory(file.toString());
				} else {
					Files.delete(file.toAbsolutePath());
				}
			}
			// Can only delete empty directory
			Files.delete(path.toAbsolutePath());
		}
	}

	public static boolean hasStateFileSuffix(String fileName) {
		boolean hasStateFileSuffix = false;
		for (String suffix : suffixList) {
			if (fileName.endsWith(suffix)) {
				hasStateFileSuffix = true;
				break;
			}
		}

		return hasStateFileSuffix;
	}

	public static boolean isPossibleVariableStateFile(File stateFile, File importFile) {
		List<String> posFilePaths = new ArrayList<>();
		posFilePaths.add(importFile.getAbsolutePath() + SUFFIX_DONE);
		posFilePaths.add(importFile.getAbsolutePath() + SUFFIX_FAILED);
		posFilePaths.add(importFile.getAbsolutePath() + SUFFIX_IMPORT);
		posFilePaths.add(importFile.getAbsolutePath() + SUFFIX_DONE);

		return posFilePaths.contains(stateFile.getAbsolutePath());
	}

}
