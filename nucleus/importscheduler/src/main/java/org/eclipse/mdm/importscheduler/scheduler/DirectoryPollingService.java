/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.eclipse.mdm.importscheduler.scheduler.util.StateFileHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Poller as timer task
 * 
 * at execution he collects all state files and trigger the scheduler
 * 
 * @author akn
 *
 */
public class DirectoryPollingService implements Runnable {

	private static final Logger LOG = LoggerFactory.getLogger(DirectoryPollingService.class);

	private final File rootDir;
	private final Scheduler scheduler;
	private final String stateFileName;

	public DirectoryPollingService(File rootDir, Scheduler scheduler, String stateFileName) {
		this.rootDir = rootDir;
		this.scheduler = scheduler;
		this.stateFileName = stateFileName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		try {
			List<File> stateFiles = getAllStateFilesRecursively();
			LOG.debug("Pollingservice found {} state files!", stateFiles.size());
			for (File stateFile : stateFiles) {
				scheduler.createOrUpdateJob(stateFile);
			}
			scheduler.clearNonExistingJobs();
			scheduler.startJobs();
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}

	}

	private List<File> getAllStateFilesRecursively() {
		IOFileFilter filter = new IOFileFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return true;
			}

			@Override
			public boolean accept(File file) {
				return file.getName().equalsIgnoreCase(stateFileName)
						|| StateFileHelper.hasStateFileSuffix(file.getName()) || file.isDirectory();
			}
		};

		return new ArrayList<>(FileUtils.listFiles(rootDir, filter, filter));
	}

}
