/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;

/**
 * Freemarker template singleton which loads
 */
public class FreemarkerTemplate {

	private static final Logger LOG = LoggerFactory.getLogger(FreemarkerTemplate.class);

	public static final String TPL_IMPORT_EXCEPTIONS = "import-exceptions.ftlh";

	private Configuration cfg;

	private static FreemarkerTemplate self;

	/**
	 * Get a static instance of the email service
	 *
	 * @return Email sevice object
	 */
	public static FreemarkerTemplate getInstance() {
		if (self == null || self.cfg == null) {
			FreemarkerTemplate.self = new FreemarkerTemplate();
			FreemarkerTemplate.self.init();
		}
		return self;
	}

	/**
	 * Initialize the freemarker functionality
	 */
	public void init() {
		// Create your Configuration instance, and specify if up to what FreeMarker
		// version (here 2.3.29) do you want to apply the fixes that are not 100%
		// backward-compatible. See the Configuration JavaDoc for details.
		cfg = new Configuration(Configuration.VERSION_2_3_29);

		// Specify the source where the template files come from. Here I set a
		// plain directory for it, but non-file-system sources are possible too:
		cfg.setClassForTemplateLoading(this.getClass(), "/templates");

		// From here we will set the settings recommended for new projects. These
		// aren't the defaults for backward compatibilty.

		// Set the preferred charset template files are stored in. UTF-8 is
		// a good choice in most applications:
		cfg.setDefaultEncoding("UTF-8");

		// Sets how errors will appear.
		// During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is
		// better.
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

		// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
		cfg.setLogTemplateExceptions(false);

		// Wrap unchecked exceptions thrown during template processing into
		// TemplateException-s:
		cfg.setWrapUncheckedExceptions(true);

		// Do not fall back to higher scopes when reading a null loop variable:
		cfg.setFallbackOnNullLoopVariable(false);
	}

	/**
	 * Method to load the template
	 *
	 * @param name The filename of the template to load
	 * @return The Template or null
	 */
	private Template loadTemplate(String name) {
		try {
			return cfg.getTemplate(name);
		} catch (IOException e) {
			LOG.error("Template not found", e);
		}
		return null;
	}

	/**
	 * Process the provided template with the mappings
	 *
	 * @param template The template to process
	 * @param root     The mappings to use with the template
	 * @return The processed template or an empty string
	 */
	private String processTemplate(Template template, Map<String, Object> root) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		if (template != null) {
			Writer out = new OutputStreamWriter(bos);
			try {
				template.process(root, out);
			} catch (TemplateException | IOException e) {
				LOG.error("Template process error", e);
			}
		}
		return new String(bos.toByteArray());
	}

	/**
	 * Transform the template with the provided data model
	 *
	 * @param templateName The template filename
	 * @param dataModel    The mappings to use with the template
	 * @return The processed template or an empty string
	 */
	public String useTemplate(String templateName, Map<String, Object> dataModel) {
		Template tpl = loadTemplate(templateName);
		return processTemplate(tpl, dataModel);
	}

}
