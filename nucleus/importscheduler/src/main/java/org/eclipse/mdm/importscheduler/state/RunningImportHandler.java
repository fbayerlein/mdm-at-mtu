/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.state;

import java.util.Date;
import java.util.List;

/**
 * @author akn
 *
 */
public interface RunningImportHandler {

	/**
	 * 
	 * @param directory  of the state file
	 * @param maxImports the maximum of possible import processes
	 * @return true if a further import is possible, false otherwise
	 */
	boolean insertRunningImport(String directory, int maxImports);

	/**
	 * 
	 * @param directory     of the state file
	 * @param targetService the target service name
	 * @param maxImports    the maximum of possible import processes
	 * @return true if a further import is possible, false otherwise
	 */
	boolean insertRunningImport(String directory, String targetService, int maxImports);

	/**
	 * 
	 * @return all active imports
	 */
	List<RunningImport> getRunningImports();

	/**
	 * removes a importprovess
	 * 
	 * @param directory of the importprocess
	 */
	void removeRunningImportOfDirectory(String directory);

	/**
	 * 
	 * @param hostName
	 * @return
	 */
	List<RunningImport> getRunningImportsByHostName(String hostName);

	/**
	 * 
	 * @param hostName
	 * @return
	 */
	List<RunningImport> getRunningImportsByHostName(String hostName, String targetService);

	/**
	 * 
	 * @param timestamp
	 * @return
	 */
	List<RunningImport> getRunningImportsOlderThan(Date timestamp);

	/**
	 * 
	 * @param timestamp
	 * @return
	 */
	List<RunningImport> getRunningImportsOlderThan(Date timestamp, String targetService);

	/**
	 * 
	 * @param hostName
	 */
	void removeRunningImportsOfHostName(String hostName);

	/**
	 * 
	 * @param hostName
	 */
	void removeRunningImportsOfHostName(String hostName, String targetService);

	/**
	 * 
	 * @param hostName
	 */
	void removeRunningImportsOlderThen(Date timestamp);

	/**
	 * 
	 * @param hostName
	 */
	void removeRunningImportsOlderThen(Date timestamp, String targetService);

}
