/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author akn
 *
 */
public interface Scheduler {

	/**
	 * 
	 * @return all jobs in the state SCHEDULED
	 * @throws SchedulerException
	 */
	List<JobInfo> getScheduledJobs() throws SchedulerException;

	/**
	 * 
	 * @return all jobs in the state FAILED
	 * @throws SchedulerException
	 */
	List<JobInfo> getFailedJobs() throws SchedulerException;

	/**
	 * 
	 * @return all jobs in the state DONE
	 * @throws SchedulerException
	 */
	List<JobInfo> getDoneJobs() throws SchedulerException;

	/**
	 * 
	 * @return all jobs in the state RUNNING
	 * @throws SchedulerException
	 */
	List<JobInfo> getRunningJobs() throws SchedulerException;

	/**
	 * deleting a job
	 * 
	 * @param jobId id of the job
	 * @throws SchedulerException
	 */
	void deleteJob(long jobId) throws SchedulerException;

	/**
	 * Deleting the job which belongs to the directory
	 * 
	 * @param directory
	 * @throws SchedulerException
	 */
	void deleteJob(File directory) throws SchedulerException;

	/**
	 * Reschedule a job from failed to scheduled
	 * 
	 * @param jobId
	 * @throws SchedulerException
	 */
	void rescheduleJob(Long jobId) throws SchedulerException;

	/**
	 * Creating one job with the informations of the file. If the job already exists
	 * he made a update of it
	 * 
	 * @param stateFile file includes the state of the job
	 * @throws SchedulerException
	 */
	void createOrUpdateJob(File stateFile) throws SchedulerException;

	/**
	 * Starting all idled jobs
	 * 
	 * @throws SchedulerException
	 */
	void startJobs() throws SchedulerException;

	/**
	 * Deleting a job if the job object not exists anymore
	 * 
	 * @param stateFiles list of state files
	 */
	void clearNonExistingJobs() throws SchedulerException;

	/**
	 * Return all jobs
	 * 
	 * @return a complete List of {@link JobInfo}s
	 * @throws SchedulerException
	 */
	List<JobInfo> getAllJobs() throws SchedulerException;

	/**
	 * Return the number of all jobs
	 * 
	 * @param jobState  the job state
	 * @param dateTo
	 * @param dateFrom
	 * @param stateFile
	 * @return a number of {@link JobInfo}s
	 * @throws SchedulerException
	 */
	int getJobCount(String jobState, String stateFile, Date dateFrom, Date dateTo) throws SchedulerException;

	/**
	 * Return a specific set of jobs
	 * 
	 * @param from      The lower limiter
	 * @param to        The upper limiter
	 * @param jobState  the job state
	 * @param dateTo
	 * @param dateFrom
	 * @param stateFile
	 * @return a limited List of {@link JobInfo}s
	 * @throws SchedulerException
	 */
	List<JobInfo> getPagedJobs(int from, int to, String jobState, String stateFile, Date dateFrom, Date dateTo)
			throws SchedulerException;

	/**
	 * Return a specific job
	 * 
	 * @param id The id of the job
	 * @return a specific {@link JobInfo}
	 * @throws SchedulerException
	 */
	JobInfo getJobById(int id) throws SchedulerException;

	/**
	 * 
	 * @param path
	 * @return
	 * @throws SchedulerException
	 */
	JobInfo getJobInfoByDirectory(String path) throws SchedulerException;
}
