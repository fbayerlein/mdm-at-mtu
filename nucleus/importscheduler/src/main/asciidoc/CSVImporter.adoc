= CSV Importer

== Konfiguration

=== System Properties

[width="100%",options="header"]
|====================
| Name | Beschreibung | Pflicht
|  org.eclipse.mdm.importscheduler.rootDir| Verzeichnis in dem die zu importierenden Datensätze abgelegt werden müssen  | Ja  
| org.eclipse.mdm.importscheduler.targetService | Service in dem der Datensatz importiert wird | Ja
| org.eclipse.mdm.csvadapter.quantityMappingFile | Pfad zu der Quantity Mapping-Datei | Nein
|====================


=== Quantity Mapping-Datei
Bei dieser Datei handelt es sich um eine Textdatei, in der man Kanalnamen aus der CSV zu Messgrößen in MDM mappen kann. Das Mapping erfolgt als Key-Value-Pairs:
[source,]
----
<KanalnameCSV>=<MessgrößeMDM>
----

Beispiel:
[source,]
----
MD.MOT=MD
MD.EG.SA=MD
DATE=Datum
CO.EG=EG
CO2.EG=EG
----

Ist ein Kanalname aus der CSV enthalten wird die gemappte Messgröße für den Import verwendet. Ist kein Eintrag vorhanden, wird für den Import eine Messgröße mit dem selben Namen verwendet. 

=== Externe Systeme
Für die Zuordnung eines CSV Attributs können externe Systeme genutzt werden. Dies ist flexibel über die Ablage der Datensätze auf dem Dateisystem möglich: <RootVerzeichnis>\<NameDesExternenSystems\<Messdateiordner>. Der erste Unterordner des Import-Roots enstpricht dem Namen des externen Systems. Ist diese Ebene nicht vorhanden oder das externe System nicht in der openMDM-Datenbank vorhanden, erfolgt der Import ohne Mapping.

Als Name des externen Systemattributes muß einfach der Attributname aus der CSV Datei eingetragen werden.

=== Messgrössen und Einheiten-Mapping

Über  MDM-Systemparameters können Mappings der Messgrössen- und Einheitennamen konfiguriert werden. Die Namen sind der Systemparameters sind wie folgt:

[width="100%", cols="2,8",options="header" ]
|====================
| Mapping | Systemparameter 
| Messgrößen | org.eclipse.mdm.importscheduler.<identifier>.quantitymapping
| Einheiten | org.eclipse.mdm.importscheduler.<identifier>.unitmapping
|====================

Als identifier dient hier, wie als Name des externen Systems, der erste Unterordner des Importroots. Der Wert entspricht dem Mapping und muß als ';' - separierte Schlüsselwertpaare abgelegt werden:

[source,]
----
<NameQuelle1>=<WertZiel1>;<NameQuelle2>=<NameZiel>...
----

Ist zu einem Quell-Name kein Mapping konfiguriert, erfolgt der Import eins zu eins.


== Dateiformat
=== Dateiendung
Die Datei muß die Dateiendung ".csv" besitzen. 

=== Metadaten
In der ersten Zeile der Datei, muß die Kennzeichnung "[METADATA]" stehen. Dies kennzeichnet den Start des Metadaten-Abschnittes. In den folgenen Zeilen folgen die Metadaten in Form von Schlüsselwertpaaren:
[source,]
----
<openMDMSchlüssel>=<Wert>
----

Über folgende Attribute erfolgt die Zuordnung in die openMDM-Struktur, bzw. die Zuordnung zu den Templates:


[width="100%",options="header"]
|====================
| openMDMSchlüssel |  Beschreibung | Pflicht 
| MDMTemplateTestStepName | Name des Messschritttemplates welches für den Import benutzt wird | Ja
| PROJECT.Name |  Name des Projektes unter dem die Messung angelegt wird  | Ja
| STRUCTURELEVEL.Name | Name der Strukturebene unter dem die Messung angelegt wird  | Ja
| TEST.Name |  Name des Versuches unter dem die Messung angelegt wird  | Ja
| TESTSTEP.Name |  Name des Messschrittes unter dem die Messung angelegt wird  | Ja
| MEASUREMENT.Name | Name der Messung | Nein (Defaultwert: Name der Importdatei)
|====================

Des weiteren können Werte zu den Metadaten aus dem Template zugeordnet werden. Dies ist optional. Zuersts wird versucht, falls ein externes System konfiguriert ist, ein Mapping über das externe Systeme zu finden. Ist keines vorhanden, wird versucht den Wert direkt zuzuordnen. Der openMDMSchlüssel muss sich dann wie folgt zusammen setzen:

[source,]
----
<KontextTyp>.<KomponentenName>.<AttributName>
----

Der KontextTyp muß einen folgender Werte enthalten: UNITUNDERTEST, TESTSEQUENCE und TESTEQUIPMENT

==== Dateianhänge
Es können Dateien an Versuche, Messschritte, Messergebnisse und an Kontextdaten angehängt werden, dies geschieht über folgende CSV Attribute:

[width="100%",options="header"]
|====================
| openMDM Ebene |  CSV Attribut
| Versuch | TEST.MDMLinks
| Messschritt | TESTSTEP.MDMLinks
| Messergebnis | MEASUREMENT.MDMLinks
| Kontextattribut | <KontextTyp>.<KomponentenName>.<AttributName>
|====================

Der Wert muß den absoluten oder den relativen Pfad, ausgehend von der CSV-Datei, zur anzuhängenden Datei enthalten. Ist das Zielattribut vom Datentyp 'DS_EXTERNALREFERENCE' können auch mehrere Dateien angehängt werden. Dafür müssen die Dateipfade mit '|' getrennt werden. 


=== Messdaten
Der Messdatenabschnitt beginnt mit der Zeile "[MEASUREDDATA]". Dies kennzeichnet zugleich das Ende des Metadaten-Abschnittes. Auf die Zeile mit dem Schlüssel "[MEASUREDDATA]" folgt die Zeile mit den Kanalnamen, danach folgt eine Zeile mit der zugehörigen Einheit. Alle weiteren Reihen entsprechen den Messwerten. 

Es besteht die Möglichkeit das Gültigkeitsflag eines Messwertes zu verarbeiten, dafür muß in der Zeile mit der Kanalname zu dem gewünschten Kanal eine weiter Spalte vorhanden sein mit dem Namen <Kanalname>_FLAGS. Als Werte für gültig kann true oder 15, und für ungültig false oder 0, benutzt werden. Ist keine Flags spalte für ein Kanal vorhanden ist dieser initial gültig. 

Als Trennzeichen der Messwerte, muß ein Tab verwendet werden.

=== Leerzeilen
Die erste Zeile darf keine Leerzeile sein, alle anderen werden vom Importer ignoriert.

=== Anforderungen an die Werte

Werte die einem DT_DATE entsprechen müssen in folgendem Format gespeichert werden:


[source,]
----
Format: JJJJ-MM-DDThh:mm:ss

Beispiel: 2008-02-01T09:00:22
----


=== x-Achse
Der Name der x-Achse, kann mit dem Meta-Daten-Attribut 'xAxis', konfiguriert werden. Wird über den Parameter der Name geändert, muß die Datei einen Kanal mit diesem Namen enthalten. 

Ist der Parameter nicht konfiguriert, wird der Standardname 'MeasurementPoints'verwendet. Ist in diesem Fall kein Kanal mit diesem Namen vorhanden, wird dieser, mit den Werten 1 bis Länge der Messwerte, autogeneriert. 



== Importprozess

Um einen Importprozess anzustoßen, muss ein Unterverzeichnis mit einer CSV-Datei und einer Status-Datei(state.file) in dem konfigurierten Verzeichnis abgelegt werden. Der Inhalt der Status-Datei muß "SCHEDULED" sein. Nach dem Import wird der Status DONE oder FAILED, inkl. Fehlermeldung, in die Statusdatei zurückgeschrieben.

Die Namen der Ebenen Projekt, Structurelevel, Test und TestStep werden aus den Metadaten übernommen. Der Name der Messung enspricht dem Namen der Importdatei.


== Voraussetzungen für einen erfolgreichen Import

Folgende Voraussetzungen müßen gegeben sein, andernfalls wird der Import abgebrochen und der Fehler in die Statusdatei geschrieben:

* Unterverzeichnis darf nur eine CSV-Datei enthalten
* CSV muß dem beschriebenen Format entsprechen
* Zu den Kanälen aus der CSV-Datei muß eine passende Messgröße vorhanden sein. 
* Die Einheit aus der CSV-Datei muß mit der Einheit aus der zugeordneten MDM-Messgröße übereinstimmen
* Die Werte der Metadaten und Kanälen müssen in den Datentypes des Metadaten-Attributes, bzw. in den Datentyp der Messgröße, konvertierbar sein.


