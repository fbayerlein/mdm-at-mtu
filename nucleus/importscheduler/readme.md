Import Scheduler
================

This component polls a configured directory in a defined time interval for import files. If it finds a file with the name `state.file` it will analyze the content of it. In this file the current status of an import will be documented. If the state is `SCHEDULED` the importer will import the accompanying ATFX or CSV file. After execution of the import the final state will be written to the file (either `DONE` or `FAILED`). 



# Configuration

The component is configured with system properties. All system properties have the prefix "org.eclipse.mdm.importscheduler.":


| Parameter             | Description                                               | Default       | Example                                                               |
|-----------------------|-----------------------------------------------------------|---------------|---------------------------------------------------------------------- |
| targetService         | The service where the data will be imported               | MDM           | `org.eclipse.mdm.importscheduler.targetService=MDM`                       |
| rootDir               | Directory were the importer is searching for import files | Java Temp Dir | `org.eclipse.mdm.importscheduler.rootDir=/tmp/importDir/`                 |
| pollingIntervalSeconds| Polling interval of the scheduler                         | 10            | `org.eclipse.mdm.importscheduler.pollingIntervalSeconds=5`                |
| email.jndi            | JNDI name of the email connector                          |               | `org.eclipse.mdm.importscheduler.email.jndi=produktaudit-import`          |
| email.recipient       | The recipient address of the error emails                 |               | `org.eclipse.mdm.importscheduler.email.recipient=produktaudit-import@domain.tld` |
| schedulerWebURL       | The URL to the scheduler website which is embedded in the email |         | `org.eclipse.mdm.importscheduler.schedulerWebURL=http://domain.local:8080/` |

If targetService is not specified, it is determined from the name of the first subdirectory within the import file path, starting from rootDir.
The external system is determined from the name of the second subdirectory within the import file path, starting from rootDir.