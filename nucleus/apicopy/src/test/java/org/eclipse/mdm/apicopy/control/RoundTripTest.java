/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apicopy.control;

import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_NAMESERVICE;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_PASSWORD;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_SERVICENAME;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_USER;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.asam.ods.AoException;
import org.asam.ods.AoSession;
import org.asam.ods.ApplicationAttribute;
import org.asam.ods.DataType;
import org.asam.ods.InstanceElement;
import org.asam.ods.NameValueUnit;
import org.asam.ods.TS_Union;
import org.asam.ods.TS_Value;
import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.odsadapter.ODSContextFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.omg.CORBA.ORB;

import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;

import de.rechner.openatfx.AoServiceFactory;

@Ignore
//FIXME 09.03.2021: this test needs a running ODS Server, that is not suitable for continous build in Jenkins.
//Comment this in for local tests only.
public class RoundTripTest {
	public static final String ATFX_FILE = "src/main/resources/emptyAtfx.xml";

	public static ApplicationContext contextODSServer;

	@ClassRule
	public static TemporaryFolder tmpFolder = new TemporaryFolder();

	private static final String NAME_SERVICE = "corbaloc::1.2@%s:%s/NameService";

	private static final String USER = "sa";
	private static final String PASSWORD = "sa";

	@BeforeClass
	public static void init() throws ConnectionException, IOException {

		// Source context
		String nameServiceHost = System.getProperty("host");
		String nameServicePort = System.getProperty("port");
		String serviceName = System.getProperty("service");

		if (nameServiceHost == null || nameServiceHost.isEmpty()) {
			throw new IllegalArgumentException("name service host is unknown: define system property 'host'");
		}

		nameServicePort = nameServicePort == null || nameServicePort.isEmpty() ? String.valueOf(2809) : nameServicePort;
		if (nameServicePort == null || nameServicePort.isEmpty()) {
			throw new IllegalArgumentException("name service port is unknown: define system property 'port'");
		}

		if (serviceName == null || serviceName.isEmpty()) {
			throw new IllegalArgumentException("service name is unknown: define system property 'service'");
		}

		Map<String, String> connectionParameters = new HashMap<>();
		connectionParameters.put(PARAM_NAMESERVICE, String.format(NAME_SERVICE, nameServiceHost, nameServicePort));
		connectionParameters.put(PARAM_SERVICENAME, serviceName + ".ASAM-ODS");
		connectionParameters.put(PARAM_USER, USER);
		connectionParameters.put(PARAM_PASSWORD, PASSWORD);

		contextODSServer = new ODSContextFactory().connect(serviceName, connectionParameters);
	}

	@AfterClass
	public static void teardown() throws ConnectionException {
		contextODSServer.close();
	}

	@Test
	public void testRoundTrip() throws ConnectionException, AoException, IOException {
		File exportedAtfx = exportAtfx();

		File readyToImportAtfx = addTemplateAttributes(exportedAtfx, "Roundtrip1", "PBN_UNECE_R51",
				"PBN_UNECE_R51_RUN");

		importAtfx(readyToImportAtfx);
	}

	private File exportAtfx() throws ConnectionException, IOException {

		// target context
		File f = tmpFolder.newFile("exported.atfx");
		Files.copy(new File(ATFX_FILE), f);

		Map<String, String> map = ImmutableMap.<String, String>builder().put("atfxfile", f.getAbsolutePath())
				.put("freetext.active", "false").put("includeCatalog", "true").put("WRITE_EXTERNALCOMPONENTS", "true")
				.put("write_mode", "file").build();

		try (ApplicationContext contextDst = new ATFXContextFactory().connect("ATFX", map)) {
			ExportTask task = new ExportTask(contextODSServer, contextDst);
			task.copyCatalog();
		}

		try (ApplicationContext contextDst = new ATFXContextFactory().connect("ATFX", map)) {
			ExportTask task = new ExportTask(contextODSServer, contextDst);
			task.copy(Arrays.asList(
					contextODSServer.getEntityManager().get().load(org.eclipse.mdm.api.base.model.Test.class, "2017")));
		}
		return f;
	}

	/**
	 * open ATFx file, add template attributes and set the appropriate template
	 * names.
	 * 
	 * @param exportedAtfx
	 * @param newTestName
	 * @param testTplName
	 * @param testStepTplName
	 * @return
	 * @throws AoException
	 */
	private File addTemplateAttributes(File exportedAtfx, String newTestName, String testTplName,
			String testStepTplName) throws AoException {

		ORB orb = ORB.init(new String[] {}, System.getProperties());
		AoSession session = AoServiceFactory.getInstance().newAoSession(orb, exportedAtfx);

		session.startTransaction();
		ApplicationAttribute testTemplate = session.getApplicationStructure().getElementByName("Test")
				.createAttribute();
		testTemplate.setName("template");
		testTemplate.setDataType(DataType.DT_STRING);

		ApplicationAttribute testStepTemplate = session.getApplicationStructure().getElementByName("TestStep")
				.createAttribute();
		testStepTemplate.setName("template");
		testStepTemplate.setDataType(DataType.DT_STRING);

		session.commitTransaction();
		session.startTransaction();
		InstanceElement[] insts = session.getApplicationStructure().getElementByName("Test").getInstances("*")
				.nextN(1000);
		for (InstanceElement inst : insts) {
			TS_Union u = new TS_Union();
			u.stringVal(testTplName);
			inst.setValue(new NameValueUnit("template", new TS_Value(u, (short) 15), ""));

			inst.setName(newTestName);
		}

		insts = session.getApplicationStructure().getElementByName("TestStep").getInstances("*").nextN(1000);
		for (InstanceElement inst : insts) {
			TS_Union u = new TS_Union();
			u.stringVal(testStepTplName);
			inst.setValue(new NameValueUnit("template", new TS_Value(u, (short) 15), ""));
		}

		session.commitTransaction();
		session.close();

		return exportedAtfx;
	}

	private void importAtfx(File readyToImportAtfx) throws ConnectionException {
		Map<String, String> mapContextImportSrc = ImmutableMap.<String, String>builder()
				.put("atfxfile", readyToImportAtfx.getAbsolutePath()).put("freetext.active", "false")
				.put("includeCatalog", "true").put("WRITE_EXTERNALCOMPONENTS", "true").put("write_mode", "file")
				.build();

		try (ApplicationContext contextImportSrc = new ATFXContextFactory().connect("ATFX", mapContextImportSrc)) {

			ImportTask importTask = new ImportTask(contextImportSrc, contextODSServer, new DefaultTemplateManager());

			importTask.copy(Arrays.asList(contextImportSrc.getEntityManager().get()
					.loadAll(org.eclipse.mdm.api.base.model.Test.class).get(0)));
		}
	}
}
