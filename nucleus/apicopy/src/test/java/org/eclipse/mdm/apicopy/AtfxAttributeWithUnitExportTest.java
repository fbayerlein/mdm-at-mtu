/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apicopy;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.eclipse.mdm.api.atfxadapter.ATFXContext;
import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.apicopy.control.ExportTask;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.io.TempDir;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.google.common.collect.ImmutableMap;

@Testcontainers(disabledWithoutDocker = true)
public class AtfxAttributeWithUnitExportTest {
	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext contextSrc;
	public static TestStep testStep;
	public static Quantity quantity;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		contextSrc = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = contextSrc.getEntityFactory().get();
		EntityManager em = contextSrc.getEntityManager().get();

		Unit uVelocity = em.loadAll(Unit.class, "m/s").get(0);

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		ef.createCatalogAttribute("v_max", ValueType.INTEGER, ccVehicle, uVelocity);

		TemplateRoot trUut = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uut");
		ef.createTemplateComponent("vehicle", trUut, ccVehicle, true);

		TemplateTestStep templateTestStep = ef.createTemplateTestStep("MyTemplateTestStep");
		templateTestStep.setTemplateRoot(trUut);

		TemplateTest templateTest = ef.createTemplateTest("TplTest1");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle));
			t.create(Arrays.asList(trUut));
			t.create(Arrays.asList(templateTestStep));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);

		Test test = ef.createTest("MyTest", pool, templateTest, false);
		TestStep testStep = ef.createTestStep("MyTestStep", test, templateTestStep);

		ContextRoot uut = templateTestStep.getTemplateRoot(ContextType.UNITUNDERTEST)
				.map(templateRoot -> ef.createContextRoot(templateRoot)).get();

		ContextComponent vehicle = uut.getContextComponent("vehicle").get();
		vehicle.getValue("v_max").set(210);

		Measurement measurement = ef.createMeasurement("MyMeasurement", testStep, uut);

		Quantity quantity = ef.createQuantity("MyQuantity");

		Channel channel1 = ef.createChannel("Channel1", measurement, quantity);
		Channel channel2 = ef.createChannel("Channel2", measurement, quantity);
		Channel channel3 = ef.createChannel("Channel3", measurement, quantity);

		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		List<WriteRequest> writeRequests = Arrays.asList(
				WriteRequest.create(channelGroup, channel1, AxisType.X_AXIS).explicit()
						.longValues(new long[] { 42L, 128L, -1 }).build(),
				WriteRequest.create(channelGroup, channel2, AxisType.Y_AXIS).explicit()
						.longValues(new long[] { 42L, 128L, -1 }).build(),
				WriteRequest.create(channelGroup, channel3, AxisType.Y_AXIS).explicit()
						.longValues(new long[] { 42L, 128L, -1 }).build());

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project));
			t.writeMeasuredValues(writeRequests);
			t.commit();
		}
		em.loadAll(TestStep.class, "*");
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (contextSrc != null) {
			contextSrc.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testAtfxExport(@TempDir Path tempDir) throws Exception {

		Path atfxFile = tempDir.resolve("export.atfx");

		Map<String, String> map = ImmutableMap.<String, String>builder().put("atfxfile", atfxFile.toString())
				.put("freetext.active", "false").put("write_mode", "file").put(ATFXContext.INCLUDE_CATALOG, "true")
				.build();

		ApplicationContext contextDst = new ATFXContextFactory().connect("ATFX", map, contextSrc);
		try {
			ExportTask task = new ExportTask(contextSrc, contextDst);
			task.copyCatalog();
			contextDst.close();

			contextDst = new ATFXContextFactory().connect("ATFX", map, contextSrc);
			task = new ExportTask(contextSrc, contextDst);
			task.copy(contextSrc.getEntityManager().get().loadAll(Project.class, "*"));

			assertThat(atfxFile).exists();

			String atfxContents = new String(Files.readAllBytes(atfxFile));
			System.out.println(atfxContents);

			EntityManager emAtfx = contextDst.getEntityManager().get();

			assertThat(emAtfx.loadAll(Unit.class)).extracting(Unit::getName).contains("m/s");

			assertThat(emAtfx.loadAll(Measurement.class).get(0).loadContexts(emAtfx).get(ContextType.UNITUNDERTEST))
					.extracting(cr -> cr.getContextComponent("vehicle")).extracting(Optional::get)
					.extracting(cc -> cc.getValue("v_max")).extracting(Value::extract).isEqualTo(210);
		} finally {
			contextDst.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testAtfxExportWithCatalogInstances(@TempDir Path tempDir) throws Exception {

		Path atfxFile = tempDir.resolve("export.atfx");

		Map<String, String> map = ImmutableMap.<String, String>builder().put("atfxfile", atfxFile.toString())
				.put("freetext.active", "false").put("write_mode", "file").put(ATFXContext.INCLUDE_CATALOG, "true")
				.put(ATFXContext.INCLUDE_CATALOG_INSTANCES, "true").build();

		ApplicationContext contextDst = new ATFXContextFactory().connect("ATFX", map, contextSrc);
		try {
			ExportTask task = new ExportTask(contextSrc, contextDst);
			task.copyCatalog();
			contextDst.close();

			contextDst = new ATFXContextFactory().connect("ATFX", map, contextSrc);
			task = new ExportTask(contextSrc, contextDst);
			task.copy(contextSrc.getEntityManager().get().loadAll(Project.class, "*"));

			assertThat(atfxFile).exists();

			String atfxContents = new String(Files.readAllBytes(atfxFile));
			System.out.println(atfxContents);

			assertThat(atfxContents).contains("<CatUnitUnderTestComp>", "<CatUnitUnderTestAttr>");
			assertThat(atfxContents).contains("<name>CatUnitUnderTestComp</name>", "<name>CatUnitUnderTestAttr</name>");
		} finally {
			contextDst.close();
		}
	}

	@org.junit.jupiter.api.Test
	public void testAtfxExportWithoutCatalogInstances(@TempDir Path tempDir) throws Exception {

		Path atfxFile = tempDir.resolve("export.atfx");

		Map<String, String> map = ImmutableMap.<String, String>builder().put("atfxfile", atfxFile.toString())
				.put("freetext.active", "false").put("write_mode", "file").put(ATFXContext.INCLUDE_CATALOG, "true")
				.build();

		ApplicationContext contextDst = new ATFXContextFactory().connect("ATFX", map, contextSrc);
		try {
			ExportTask task = new ExportTask(contextSrc, contextDst);
			task.copyCatalog();
			contextDst.close();

			Map<String, String> map2 = new HashMap<>(map);
			map2.put(ATFXContext.INCLUDE_CATALOG, "false");

			contextDst = new ATFXContextFactory().connect("ATFX", map2, contextSrc);
			task = new ExportTask(contextSrc, contextDst);
			task.copy(contextSrc.getEntityManager().get().loadAll(Project.class, "*"));

			assertThat(atfxFile).exists();

			String atfxContents = new String(Files.readAllBytes(atfxFile));
			System.out.println(atfxContents);

			assertThat(atfxContents).doesNotContain("<CatUnitUnderTestComp>", "<CatUnitUnderTestAttr>");
			assertThat(atfxContents).doesNotContain("<name>CatUnitUnderTestComp</name>",
					"<name>CatUnitUnderTestAttr</name>");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contextDst.close();
		}
	}
}
