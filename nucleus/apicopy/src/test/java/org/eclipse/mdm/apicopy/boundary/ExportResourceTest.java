/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.boundary;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.businessobjects.utils.JsonMessageBodyProvider;
import org.eclipse.mdm.businessobjects.utils.MDMExceptionMapper;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.eclipse.mdm.shoppingbasket.entity.BasketItem;
import org.eclipse.mdm.shoppingbasket.entity.ShoppingBasket;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ExportResourceTest extends JerseyTest {

	@Mock
	private ExportService mockedExportService;

	private static final String EXPORT_RESOURCE = "export";

	@Override
	public Application configure() {
		MockitoAnnotations.initMocks(this);

		return new ResourceConfig().register(new ExportResource(mockedExportService)).register(MultiPartFeature.class)
				.register(MDMExceptionMapper.class).register(new AbstractBinder() {
					@Override
					protected void configure() {
						bind(mockedExportService).to(ExportService.class);
					}
				});
	}

	@Override
	protected void configureClient(ClientConfig config) {
		config.register(JacksonFeature.class);
		config.register(JsonMessageBodyProvider.class);
		config.register(ProtobufMessageBodyProvider.class);
		super.configureClient(config);
	}

	@Test
	public void testExport() throws IOException {
		Path exportFolder = Files.createTempDirectory("exportAtfx");
		Path exportAtfx = exportFolder.resolve("export.atfx");
		Files.write(exportAtfx, "test".getBytes());

		when(mockedExportService.export(any(), any(), any())).thenReturn(exportFolder);

		BasketItem item = new BasketItem();
		item.setSource("ENV");
		item.setRestURI(URI.create("environments/ENV/tests/10"));
		ShoppingBasket basket = new ShoppingBasket();
		basket.setItems(Arrays.asList(item));

		Response r = getClient().target(getBaseUri()).path(EXPORT_RESOURCE).path("atfx").request()
				.post(Entity.entity(basket, MediaType.APPLICATION_XML_TYPE));

		try {
			assertThat(exportAtfx.toFile()).doesNotExist();
			assertThat(r.readEntity(String.class)).isEqualTo("test");
			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			assertThat(r.getMediaType().toString()).isEqualTo(MediaType.APPLICATION_XML);
		} finally {
			r.close();
		}
	}

	@Test
	public void testExportMultipleFiles() throws IOException, InterruptedException {
		Path exportFolder = Files.createTempDirectory("exportAtfx");
		Path exportAtfx = exportFolder.resolve("export.atfx");
		Files.write(exportAtfx, "test".getBytes());
		Path exportBin = exportFolder.resolve("export.bin");
		Files.write(exportBin, "test".getBytes());

		when(mockedExportService.export(any(), eq(ExportType.ATFX), any())).thenReturn(exportFolder);

		BasketItem item = new BasketItem();
		item.setSource("ENV");
		item.setRestURI(URI.create("environments/ENV/tests/10"));
		ShoppingBasket basket = new ShoppingBasket();
		basket.setItems(Arrays.asList(item));

		Response r = getClient().target(getBaseUri()).path(EXPORT_RESOURCE).path("atfx").request()
				.post(Entity.entity(basket, MediaType.APPLICATION_XML_TYPE));

		try {
			assertThat(r.readEntity(String.class)).isNotEmpty();

			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			assertThat(r.getMediaType().toString()).isEqualTo("application/zip");
		} finally {
			r.close();
		}
		Thread.sleep(500L);
		assertThat(exportAtfx.toFile()).doesNotExist();
		assertThat(exportBin.toFile()).doesNotExist();
	}

	@Test
	public void testExportFile() throws IOException {
		Path exportFolder = Files.createTempDirectory("exportAtfx");
		Path exportAtfx = exportFolder.resolve("export.atfx");
		Files.write(exportAtfx, "test".getBytes());

		when(mockedExportService.export(any(), eq(ExportType.ATFX), any())).thenReturn(exportFolder);

		BasketItem item = new BasketItem();
		item.setSource("ENV");
		item.setRestURI(URI.create("environments/ENV/tests/10"));
		ShoppingBasket basket = new ShoppingBasket();
		basket.setItems(Arrays.asList(item));

		Response r = getClient().target(getBaseUri()).path(EXPORT_RESOURCE).path("atfx").request()
				.post(Entity.entity(basket, MediaType.APPLICATION_XML_TYPE));

		try {
			assertThat(r.readEntity(String.class)).isEqualTo("test");

			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			assertThat(r.getMediaType().toString()).isEqualTo(MediaType.APPLICATION_XML);
			assertThat(exportAtfx.toFile()).doesNotExist();
		} finally {
			r.close();
		}
	}

//	@Test
//	public void testExportMixedSources() throws IOException {
//		when(mockedExportService.exportAtfx(any())).thenReturn(Paths.get("exportAtfx"));
//
//		BasketItem item1 = new BasketItem();
//		item1.setSource("ENV");
//		BasketItem item2 = new BasketItem();
//		item2.setSource("ENV2");
//		ShoppingBasket basket = new ShoppingBasket();
//		basket.setItems(Arrays.asList(item1, item2));
//
//		Response r = getClient().target(getBaseUri()).path(EXPORT_RESOURCE)
//				.request()
//				.post(Entity.entity(basket, MediaType.APPLICATION_XML_TYPE));
//
//		try {
//			assertThat(r.getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
//			assertThat(r.readEntity(CopyStatusResponse.class))
//				.hasFieldOrPropertyWithValue("state", "failed")
//				.hasFieldOrPropertyWithValue("message", "Cannot export from multiple sources.")
//				.hasFieldOrProperty("stacktrace");
//		} finally {
//			r.close();
//		}
//	}
}
