/*******************************************************************************
 * Copyright (c) 2019, 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.apicopy.control;

import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_NAMESERVICE;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_PASSWORD;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_SERVICENAME;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_USER;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSContextFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

@Ignore
//FIXME 22.11.2019: this test needs a running ODS Server, that is not suitable for continous build in Jenkins.
//Comment this in for local tests only.
public class ImportAoFilesTaskTest {
	public static final String ATFX_FILE = "src/test/resources/importaofiletasktest.atfx";

	public static ApplicationContext contextDst;
	public static ApplicationContext contextSrc;

	private static final String NAME_SERVICE = "corbaloc::1.2@%s:%s/NameService";

	private static final String USER = "sa";
	private static final String PASSWORD = "sa";

	// Set to true to automatically remove TestSteps with name "ImportTaskTest"
	// before the import
	boolean deleteExistingTestSteps = true;

	@BeforeClass
	public static void init() throws ConnectionException, IOException {

		// Source context
		String nameServiceHost = "localhost"; // System.getProperty("host");
		String nameServicePort = "2809"; // System.getProperty("port");
		String serviceName = "MDMNVH"; // System.getProperty("service");

		if (nameServiceHost == null || nameServiceHost.isEmpty()) {
			throw new IllegalArgumentException("name service host is unknown: define system property 'host'");
		}

		nameServicePort = nameServicePort == null || nameServicePort.isEmpty() ? String.valueOf(2809) : nameServicePort;
		if (nameServicePort == null || nameServicePort.isEmpty()) {
			throw new IllegalArgumentException("name service port is unknown: define system property 'port'");
		}

		if (serviceName == null || serviceName.isEmpty()) {
			throw new IllegalArgumentException("service name is unknown: define system property 'service'");
		}

		Map<String, String> connectionParameters = new HashMap<>();
		connectionParameters.put(PARAM_NAMESERVICE, String.format(NAME_SERVICE, nameServiceHost, nameServicePort));
		connectionParameters.put(PARAM_SERVICENAME, serviceName + ".ASAM-ODS");
		connectionParameters.put(PARAM_USER, USER);
		connectionParameters.put(PARAM_PASSWORD, PASSWORD);

		contextDst = new ODSContextFactory().connect(serviceName, connectionParameters);

		// source context
		File f = new File(ATFX_FILE);

		Map<String, String> map = ImmutableMap.of("atfxfile", f.getAbsolutePath(), "freetext.active", "false");

		contextSrc = new ATFXContextFactory().connect("ATFX", map);
	}

	@AfterClass
	public static void teardown() throws ConnectionException {
		contextDst.close();
		contextSrc.close();
	}

	@Test
	public void testImport() throws ConnectionException {
		List<TestStep> testSteps = contextDst.getEntityManager().get().loadAll(TestStep.class, "ImportTaskTest");

		if (deleteExistingTestSteps) {
			Transaction t = contextDst.getEntityManager().get().startTransaction();
			t.delete(testSteps);
			t.commit();
		} else {
			assumeTrue("Teststep 'ImportTaskTest' already exists!", testSteps.isEmpty());
		}

		ImportTask task = new ImportTask(contextSrc, contextDst, new TemplateManager() {

			@Override
			public void setSourceContext(ApplicationContext source) throws ApiCopyException {
			}

			@Override
			public String getTemplateTestStepName(TestStep testStepSrc) throws ApiCopyException {
				return "maf_teststep";
			}

			@Override
			public String getTemplateTestName(org.eclipse.mdm.api.base.model.Test testSrc) throws ApiCopyException {
				return "maf_test";
			}
		});

		task.copy(Arrays.asList(contextSrc.getEntityManager().get().load(Project.class, "1")));
	}
}
