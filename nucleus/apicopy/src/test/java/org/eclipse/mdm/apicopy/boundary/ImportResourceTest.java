/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.boundary;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.apicopy.control.ApiCopyException;
import org.eclipse.mdm.businessobjects.entity.MDMErrorResponse;
import org.eclipse.mdm.businessobjects.utils.JsonMessageBodyProvider;
import org.eclipse.mdm.businessobjects.utils.MDMExceptionMapper;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ImportResourceTest extends JerseyTest {

	@Mock
	private ImportService mockedImportService;

	private static final String IMPORT_RESOURCE = "environments/ENV/import";

	private final Path resources = Paths.get("src/test/resources/");
	private final Path atfxFile = resources.resolve("example.atfx");
	private final Path binFile = resources.resolve("example_1.bin");

	@Override
	public Application configure() {
		MockitoAnnotations.initMocks(this);

		return new ResourceConfig().register(new ImportResource(mockedImportService)).register(MultiPartFeature.class)
				.register(JacksonFeature.class).register(MDMExceptionMapper.class).register(new AbstractBinder() {
					@Override
					protected void configure() {
						bind(mockedImportService).to(ImportService.class);
					}
				});
	}

	@Override
	protected void configureClient(ClientConfig config) {
		config.register(JacksonFeature.class);
		config.register(JsonMessageBodyProvider.class);
		config.register(ProtobufMessageBodyProvider.class);
		config.register(MultiPartFeature.class);
		config.register(MDMExceptionMapper.class);
		super.configureClient(config);
	}

	@Test
	public void testImportFile() throws IOException {
		@SuppressWarnings("unchecked")
		ArgumentCaptor<Map<String, String>> mapCaptor = ArgumentCaptor.forClass(Map.class);
		when(mockedImportService.importFile(any(), any(), mapCaptor.capture(), eq("atfx")))
				.thenReturn(Response.ok().build());

		Response r = getClient().target(getBaseUri()).path(IMPORT_RESOURCE).path("atfx")
				.queryParam("someConfigKey", "configValue").request()
				.post(Entity.entity(new FileInputStream(atfxFile.toFile()), MediaType.APPLICATION_XML_TYPE));
		try {
			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			verify(mockedImportService, times(1)).importFile(any(), eq("ENV"), any(), eq("atfx"));
			assertThat(mapCaptor.getValue()).containsEntry("someConfigKey", "configValue");
		} finally {
			r.close();
		}
	}

	@Test
	public void testImportMultiPart() throws IOException {
		when(mockedImportService.importFile(any(), any(), any(), eq("atfx"))).thenReturn(Response.ok().build());

		try (final FormDataMultiPart multiPartEntity = new FormDataMultiPart()) {
			multiPartEntity.field(atfxFile.getFileName().toString(), new FileInputStream(atfxFile.toFile()),
					MediaType.APPLICATION_XML_TYPE).field(binFile.getFileName().toString(),
							new FileInputStream(binFile.toFile()), MediaType.APPLICATION_OCTET_STREAM_TYPE);

			Response r = getClient().target(getBaseUri()).path(IMPORT_RESOURCE).path("atfx").request()
					.post(Entity.entity(multiPartEntity, multiPartEntity.getMediaType()));
			try {
				assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
				verify(mockedImportService, times(1)).importFile(any(), eq("ENV"), any(), eq("atfx"));
			} finally {
				r.close();
			}
		}
	}

	@Test
	public void testImportZip() throws IOException {
		when(mockedImportService.importFile(any(), any(), any(), eq("atfx"))).thenReturn(Response.ok().build());

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		ZipOutputStream zipOut = new ZipOutputStream(out);

		zipOut.putNextEntry(new ZipEntry(atfxFile.getFileName().toString()));
		writeFileToStream(atfxFile, zipOut);

		zipOut.putNextEntry(new ZipEntry(binFile.getFileName().toString()));
		writeFileToStream(binFile, zipOut);
		zipOut.close();

		Response r = getClient().target(getBaseUri()).path(IMPORT_RESOURCE).path("atfx").request()
				.post(Entity.entity(out.toByteArray(), "application/zip"));
		try {
			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			verify(mockedImportService, times(1)).importFile(any(), eq("ENV"), any(), eq("atfx"));
		} finally {
			r.close();
		}
	}

	@Test
	@Ignore
	public void testFailedImport() throws IOException {
		when(mockedImportService.importFile(any(), any(), any(), eq("atfx")))
				.thenThrow(new ApiCopyException("JUnit test fail"));

		try (final FormDataMultiPart multiPartEntity = new FormDataMultiPart()) {
			multiPartEntity.field(atfxFile.getFileName().toString(), new FileInputStream(atfxFile.toFile()),
					MediaType.APPLICATION_XML_TYPE);
			Response r = getClient().target(getBaseUri()).path(IMPORT_RESOURCE).path("atfx").request()
					.post(Entity.entity(multiPartEntity, multiPartEntity.getMediaType()));
			try {
				assertThat(r.getStatus()).isEqualTo(Status.INTERNAL_SERVER_ERROR.getStatusCode());
				MDMErrorResponse error = r.readEntity(MDMErrorResponse.class);
				assertThat(error).hasFieldOrPropertyWithValue("message", "JUnit test fail");
			} finally {
				r.close();
			}
		}
	}

	@Test
	@Ignore
	public void testExport() {
		when(mockedImportService.importFile(any(), any(), any(), eq("atfx"))).thenReturn(Response.ok().build());

		Response r = getClient().target(getBaseUri()).path("environments/mdm/export").path("atfx").request().get();
		try {
			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			assertThat(r.getMediaType().toString()).isEqualTo(MediaType.APPLICATION_XML);
		} finally {
			r.close();
		}
	}

	@Test
	public void testQueryParametersFile() throws IOException {
		@SuppressWarnings("unchecked")
		ArgumentCaptor<Map<String, String>> mapCaptor = ArgumentCaptor.forClass(Map.class);
		when(mockedImportService.importFile(any(), any(), mapCaptor.capture(), eq("atfx")))
				.thenReturn(Response.ok().build());

		Response r = getClient().target(getBaseUri()).path(IMPORT_RESOURCE).path("atfx")
				.queryParam("someConfigKey", "configValue1").queryParam("someConfigKey", "configValue2")
				.queryParam("another", "value1").request()
				.post(Entity.entity(new FileInputStream(atfxFile.toFile()), MediaType.APPLICATION_XML_TYPE));
		try {
			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			verify(mockedImportService, times(1)).importFile(any(), eq("ENV"), any(), eq("atfx"));
			assertThat(mapCaptor.getValue()).containsEntry("someConfigKey", "configValue1").containsEntry("another",
					"value1");
		} finally {
			r.close();
		}
	}

	private void writeFileToStream(Path atfxFile, OutputStream out) throws FileNotFoundException, IOException {
		try (FileInputStream in = new FileInputStream(atfxFile.toFile())) {
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
		}
	}

}
