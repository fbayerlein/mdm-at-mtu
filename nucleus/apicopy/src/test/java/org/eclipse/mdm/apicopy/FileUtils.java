/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apicopy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Stream;

public class FileUtils {
	/**
	 * Writes inputstream to file
	 * 
	 * @param file
	 * @param inputstream
	 * @throws IOException
	 */
	public static void writeToFile(Path file, InputStream inputstream) throws IOException {
		try (OutputStream out = new FileOutputStream(file.toFile())) {
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputstream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
		}
	}

	/**
	 * Deletes a directory.
	 * 
	 * @param pathToBeDeleted directory to delete
	 * @throws IOException
	 */
	public static void deleteDirectory(java.nio.file.Path pathToBeDeleted) throws IOException {
		try (Stream<Path> paths = Files.walk(pathToBeDeleted)) {
			paths.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
		}
	}
}
