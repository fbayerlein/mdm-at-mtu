/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apicopy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.PhysicalDimension;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.apicopy.control.ImportTask;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.google.common.collect.ImmutableMap;

@Testcontainers(disabledWithoutDocker = true)
public class DuplicateUnitNamesImportTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	public static ApplicationContext contextDst;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		contextDst = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = contextDst.getEntityFactory().get();
		EntityManager em = contextDst.getEntityManager().get();

		assertThat(em.loadAll(PhysicalDimension.class, "mass")).hasSize(1);
		assertThat(em.loadAll(Unit.class, "g")).hasSize(1).extracting(Unit::getPhysicalDimension).first()
				.hasFieldOrPropertyWithValue("name", "mass").hasFieldOrPropertyWithValue("angle", 0)
				.hasFieldOrPropertyWithValue("current", 0).hasFieldOrPropertyWithValue("length", 0)
				.hasFieldOrPropertyWithValue("luminousIntensity", 0).hasFieldOrPropertyWithValue("mass", 1)
				.hasFieldOrPropertyWithValue("molarAmount", 0).hasFieldOrPropertyWithValue("temperature", 0)
				.hasFieldOrPropertyWithValue("time", 0);

		PhysicalDimension accel = em.loadAll(PhysicalDimension.class, "acceleration").get(0);

		Unit grav = ef.createUnit("g", accel);
		Quantity qWeight = ef.createQuantity("Weight");
		Quantity qAccel = ef.createQuantity("Accel");

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(grav, qWeight, qAccel));
			t.commit();
		}

	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (contextDst != null) {
			contextDst.close();
		}
	}

	/**
	 * Import an atfx with 2 Units having the same name 'g', but different
	 * PhysicalDimensions 'mass' and 'acceleration'.
	 * 
	 * This importer should map them correctly to the appropriate Units in the
	 * target based on their names and PhysicalDimensions.
	 * 
	 * @throws ConnectionException
	 */
	@org.junit.jupiter.api.Test
	public void testImportDuplicateUnitNames() throws ConnectionException {
		File f = new File("src/test/resources/duplicate_unit_names.atfx");
		Map<String, String> map = ImmutableMap.of("atfxfile", f.getAbsolutePath(), "freetext.active", "false");
		ApplicationContext contextSrc = new ATFXContextFactory().connect("ATFX", map);

		try {
			ImportTask task = new ImportTask(contextSrc, contextDst, null);
			task.setProperties(ImmutableMap.of("requireTemplate", "false"));

			task.copy(contextSrc.getEntityManager().get().loadAll(Project.class, "*"));
		} finally {
			contextSrc.close();
		}
	}

	/**
	 * Import an atfx with 2 Units having the same name 'g' and no
	 * PhysicalDimensions.
	 * 
	 * Without PhysicalDimensions in this case, the importer has no means to figure
	 * out the correct mapping and should throw an exception.
	 * 
	 * @throws ConnectionException
	 */
	@org.junit.jupiter.api.Test
	public void testImportDuplicateUnitNamesNoPhysDims() throws ConnectionException {
		File f = new File("src/test/resources/duplicate_unit_names_no_phys_dims.atfx");
		Map<String, String> map = ImmutableMap.of("atfxfile", f.getAbsolutePath(), "freetext.active", "false");
		ApplicationContext contextSrc = new ATFXContextFactory().connect("ATFX", map);

		try {
			ImportTask task = new ImportTask(contextSrc, contextDst, null);
			task.setProperties(ImmutableMap.of("requireTemplate", "false"));

			assertThatThrownBy(() -> task.copy(contextSrc.getEntityManager().get().loadAll(Project.class, "*")))
					.hasMessage(
							"Could not copy data: The source unit g could not be mapped uniquely to an unit in the target datasource!");
		} finally {
			contextSrc.close();
		}
	}
}
