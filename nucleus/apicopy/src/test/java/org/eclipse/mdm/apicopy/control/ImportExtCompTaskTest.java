/*******************************************************************************
 * Copyright (c) 2019, 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.apicopy.control;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_NAMESERVICE;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_PASSWORD;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_SERVICENAME;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_USER;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSContextFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runners.MethodSorters;

import com.google.common.collect.ImmutableMap;

@Ignore
//FIXME 22.11.2019: this test needs a running ODS Server, that is not suitable for continous build in Jenkins.
//Comment this in for local tests only.
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ImportExtCompTaskTest {

	// Set to true to automatically remove TestSteps with name "ImportTaskTest"
	// before the import
	boolean deleteExistingTestSteps = false;

	public static final String ATFX_FILE = "src/test/resources/importtasktest_external.atfx";
	public static final String BTF_FILE = "src/test/resources/importtasktest_external.btf";

	public static ApplicationContext contextDst;
	public static ApplicationContext contextSrc;

	private static final String NAME_SERVICE = "corbaloc::1.2@%s:%s/NameService";

	private static final String USER = "sa";
	private static final String PASSWORD = "sa";

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@BeforeClass
	public static void init() throws ConnectionException, IOException {

		// Source context
		String nameServiceHost = System.getProperty("host");
		String nameServicePort = System.getProperty("port");
		String serviceName = System.getProperty("service");

		if (nameServiceHost == null || nameServiceHost.isEmpty()) {
			throw new IllegalArgumentException("name service host is unknown: define system property 'host'");
		}

		nameServicePort = nameServicePort == null || nameServicePort.isEmpty() ? String.valueOf(2809) : nameServicePort;
		if (nameServicePort == null || nameServicePort.isEmpty()) {
			throw new IllegalArgumentException("name service port is unknown: define system property 'port'");
		}

		if (serviceName == null || serviceName.isEmpty()) {
			throw new IllegalArgumentException("service name is unknown: define system property 'service'");
		}

		Map<String, String> connectionParameters = new HashMap<>();
		connectionParameters.put(PARAM_NAMESERVICE, String.format(NAME_SERVICE, nameServiceHost, nameServicePort));
		connectionParameters.put(PARAM_SERVICENAME, serviceName + ".ASAM-ODS");
		connectionParameters.put(PARAM_USER, USER);
		connectionParameters.put(PARAM_PASSWORD, PASSWORD);

		contextDst = new ODSContextFactory().connect(serviceName, connectionParameters);

		// source context
		File f = new File(ATFX_FILE);

		Map<String, String> map = ImmutableMap.of("atfxfile", f.getAbsolutePath(), "freetext.active", "false");

		contextSrc = new ATFXContextFactory().connect("ATFX", map);

	}

	@AfterClass
	public static void teardown() throws ConnectionException {
		contextDst.close();
		contextSrc.close();
	}

	@Test
	public void test1Import() throws ConnectionException {
		List<TestStep> testSteps = contextDst.getEntityManager().get().loadAll(TestStep.class, "ImportTaskTest");

		if (deleteExistingTestSteps) {
			Transaction t = contextDst.getEntityManager().get().startTransaction();
			t.delete(testSteps);
			t.commit();
		} else {
			assumeTrue("Teststep 'ImportTaskTest' already exists!", testSteps.isEmpty());
		}

		ImportTask task = new ImportTask(contextSrc, contextDst, new TemplateManager() {

			@Override
			public void setSourceContext(ApplicationContext source) throws ApiCopyException {
			}

			@Override
			public String getTemplateTestStepName(TestStep testStepSrc) throws ApiCopyException {
				return "PBN_UNECE_R51_RUN";
			}

			@Override
			public String getTemplateTestName(org.eclipse.mdm.api.base.model.Test testSrc) throws ApiCopyException {
				return "PBN_UNECE_R51";
			}
		});

		task.copy(Arrays.asList(contextSrc.getEntityManager().get().load(Project.class, "1")));
	}

	@Test
	public void test2Export() throws ConnectionException, IOException {
		File f = folder.newFile("export.atfx");

		writeToFile(f, ExportTask.class.getResourceAsStream("/emptyAtfx.xml"));
		Map<String, String> map = ImmutableMap.of("atfxfile", f.getAbsolutePath(), "freetext.active", "false",
				"includeCatalog", "true");

		ApplicationContext contextExp = new ATFXContextFactory().connect("ATFX", map);

		/*
		 * Copy the catalog (which involves creating the ContextComponts).
		 */
		ExportTask task = new ExportTask(contextDst, contextExp);
		task.copyCatalog();
		contextExp.close();

		/*
		 * After creating the ContextComponents, the application model has to be
		 * reloaded. This is normally done in a co-session, but openATFX does not
		 * support co-sessions. Thus we have to create a new session by creating a new
		 * application context.
		 */
		contextExp = new ATFXContextFactory().connect("ATFX", map);
		task = new ExportTask(contextDst, contextExp);
		task.copy(contextDst.getEntityManager().get().loadAll(TestStep.class, "ImportTaskTest"));
		contextExp.close();

		assertThat(f).isFile();
		assertThat(folder.getRoot().toPath().resolve("importtasktest_external.btf")).isRegularFile()
				.hasSameContentAs(Paths.get(BTF_FILE));
	}

	private static void writeToFile(File file, InputStream entityAs) throws IOException {
		try (OutputStream out = new FileOutputStream(file)) {
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = entityAs.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
		}
	}
}
