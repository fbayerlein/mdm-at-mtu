/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_FILE_TYPE;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.eclipse.mdm.apicopy.control.ApiCopyException;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Provides endpoints for importing ATFX files to a specific datasource.
 *
 */
@Tag(name = "Import")
@javax.ws.rs.Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/import/{" + REQUESTPARAM_FILE_TYPE + "}")
public class ImportResource {
	private static final Logger LOG = LoggerFactory.getLogger(ImportResource.class);

	@EJB
	private ImportService importService;

	/**
	 * Default public constructor for JAX-RS
	 */
	public ImportResource() {
	}

	/**
	 * Used to provide service for unit tests
	 * 
	 * @param importService
	 */
	protected ImportResource(ImportService importService) {
		this.importService = importService;
	}

	/**
	 * Imports an ATFX file. The ATFX file is provided in the body of the POST
	 * request. Note that, this method is not suitable for importing a ATFX file
	 * with accompanying binary files. In this case, use
	 * {@link ImportResource#importMultiPart(FormDataMultiPart, String)} or
	 * {@link ImportResource#importZip(InputStream, String)}
	 *
	 * @param atfxInputStream  the content of the ATFX file
	 * @param targetSourceName name of the target datasource
	 * @param uriInfo          {@link UriInfo}
	 * @return
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_XML, "text/csv" })
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Imports a single ATFx without any additional files", description = "Imports a single ATFx without any additional files. Properties for the importer can be provided as query parameters.", responses = {
			@ApiResponse(responseCode = "200", description = "Import successful"),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response importXml(
			@Parameter(name = "ATFx file", description = "atfx file to import") InputStream atfxInputStream,
			@Parameter(name = "sourceName", description = "Target source name for the import") @PathParam(REQUESTPARAM_SOURCENAME) String targetSourceName,
			@PathParam(REQUESTPARAM_FILE_TYPE) String fileType, @Context UriInfo uriInfo) {
		return importService.importFile(tmpDir -> extractDataFromInputStream(tmpDir, atfxInputStream, fileType),
				targetSourceName, convertToMap(uriInfo.getQueryParameters()), fileType);
	}

	/**
	 * Imports an ATFX file. The ATFX file and its binary files are provided as
	 * multipart form data. The field name represents the original file name and the
	 * field value the contents of the file. The ATFX file is detected by its file
	 * extension 'atfx' or the media type 'application/xml'. Only one ATFX file can
	 * be set at a time.
	 *
	 * @param multiPart        form data as described above
	 * @param targetSourceName name of the target datasource
	 * @param uriInfo          {@link UriInfo}
	 * @return
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Imports an ATFx and referenced files provided as multipart form data", description = "Imports an ATFx and referenced files provided as multipart form data. Properties for the importer can be provided as query parameters.", responses = {
			@ApiResponse(responseCode = "200", description = "Import successful"),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response importMultiPart(
			@Parameter(name = "Import files", description = "Import files provided as multipart form data", hidden = true) FormDataMultiPart multiPart,
			@Parameter(name = "sourceName", description = "Target source name for the import") @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_FILE_TYPE) String fileType, @Context UriInfo uriInfo) {
		return importService.importFile(tmpDir -> extractDataFromMultiPart(tmpDir, multiPart, fileType), sourceName,
				convertToMap(uriInfo.getQueryParameters()), fileType);
	}

	/**
	 * Imports an ATFX file. The ATFX file and its binary files are provided as a
	 * zip file. The ATFX file is detected by its file extension 'atfx'. Only one
	 * ATFX file can be included in the zip file.
	 *
	 * @param zipFile          the ATFX file and its binary files as zip compressed
	 *                         {@link InputStream}
	 * @param targetSourceName name of the target datasource
	 * @param uriInfo          {@link UriInfo}
	 * @return
	 */
	@POST
	@Consumes({ "application/zip", "application/x-zip-compressed" })
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Imports an ATFx and referenced files packaged as zip file", description = "Imports an ATFx and referenced files packaged as zip file. Properties for the importer can be provided as query parameters.", responses = {
			@ApiResponse(responseCode = "200", description = "Import successful"),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response importZip(
			@Parameter(name = "ZIP file", description = "ZIP file provided in the message body") InputStream zipFile,
			@Parameter(name = "sourceName", description = "Target source name for the import") @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_FILE_TYPE) String fileType, @Context UriInfo uriInfo) {
		return importService.importFile(tmpDir -> extractDataFromZip(tmpDir, zipFile, fileType), sourceName,
				convertToMap(uriInfo.getQueryParameters()), fileType);
	}

	/**
	 * Writes the provided {@link InputStream} into the folder <code>tmpDir</code>
	 * and returns the path to this file.
	 * 
	 * @param tmpDir          temporary folder
	 * @param fileInputStream stream to be written into a file in tmpDir
	 * @return Path referencing the written file.
	 */
	private Path extractDataFromInputStream(Path tmpDir, InputStream fileInputStream, String fileType) {
		try {
			Path inputFile = tmpDir.resolve("tmp." + fileType);

			writeToFile(inputFile, fileInputStream);

			LOG.debug("Received {} file {}.", fileType, inputFile);
			return inputFile;
		} catch (IOException e) {
			throw new ApiCopyException("Could not extract data from request body.", e);
		}
	}

	/**
	 * Writes the files provided as multi part form data into the folder
	 * <code>tmpDir</code> and returns the path to the ATFX file.
	 * 
	 * @param tmpDir    temporary folder
	 * @param multiPart multi part form data with files
	 * @return Path referencing the extracted ATFX file.
	 * @throws ApiCopyException if there is no or more than one ATFX file is found
	 */
	private Path extractDataFromMultiPart(Path tmpDir, FormDataMultiPart multiPart, String fileType) {
		try {
			if (multiPart.getBodyParts().isEmpty()) {
				throw new ApiCopyException(
						"MultiPart message does not contain any parts. Atleast an ATFX file is expected.");
			}

			Path inputFile = null;
			List<Path> binFiles = new ArrayList<>();

			for (Entry<String, List<FormDataBodyPart>> entry : multiPart.getFields().entrySet()) {
				if (entry.getValue().size() != 1) {
					throw new ApiCopyException("Expecting exactly one part per field.");
				}

				String fileName = entry.getKey();
				Path filePath = tmpDir.resolve(fileName);
				FormDataBodyPart data = entry.getValue().get(0);

				writeToFile(filePath, data.getEntityAs(InputStream.class));

				if (fileName.toLowerCase().endsWith(ImportType.findByFileType(fileType).getSuffix())
						|| data.getMediaType().equals(MediaType.APPLICATION_XML_TYPE)) {
					if (inputFile == null) {
						inputFile = filePath;
					} else {
						LOG.info("Multiple files of type " + fileType + " found. " + filePath.getFileName()
								+ " will not be handled as main file, but as attachment.");
					}

				} else {
					binFiles.add(filePath);
				}
			}
			if (inputFile == null) {
				throw new ApiCopyException("Expecting at least one part with Content-Type '" + MediaType.APPLICATION_XML
						+ "', but zero parts were found.");
			}

			LOG.debug("Received {} file {} and binary files {}.", fileType, inputFile, binFiles);

			return inputFile;
		} catch (IOException e) {
			throw new ApiCopyException("Could not extract data from multipart data.", e);
		}
	}

	/**
	 * Extracts the zip archive provided by {@link InputStream} into the folder
	 * <code>tmpDir</code> and returns the path to the ATFX file.
	 * 
	 * @param tmpDir temporary folder
	 * @param in     zip file to be extracted into a file in tmpDir
	 * @return Path referencing the extracted ATFX file.
	 * @throws ApiCopyException if there is no or more than one ATFX file is found
	 */
	private Path extractDataFromZip(Path tmpDir, InputStream in, String fileType) {
		try {
			Path inputFile = null;
			List<Path> binFiles = new ArrayList<>();

			ZipInputStream zipIn = new ZipInputStream(in);
			ZipEntry entry;
			while ((entry = zipIn.getNextEntry()) != null) {
				String fileName = entry.getName();
				Path file = tmpDir.resolve(fileName);
				writeToFile(file, zipIn);

				if (fileName.endsWith(ImportType.findByFileType(fileType).getSuffix())) {
					if (inputFile == null) {
						inputFile = file;
					} else {
						throw new ApiCopyException(
								"Expecting exactly one file with extension 'atfx', but files were found.");
					}
				} else {
					binFiles.add(file);
				}
			}
			if (inputFile == null) {
				throw new ApiCopyException(
						"Expecting exactly one file with extension 'atfx', but zero parts were found.");
			}

			LOG.debug("Received ATFX file {} and binary files {}.", inputFile, binFiles);
			return inputFile;
		} catch (IOException e) {
			throw new ApiCopyException("Could not extract data from zip file.", e);
		}
	}

	/**
	 * Writes inputstream to file
	 * 
	 * @param file
	 * @param inputstream
	 * @throws IOException
	 */
	private void writeToFile(Path file, InputStream inputstream) throws IOException {
		try (OutputStream out = new FileOutputStream(file.toFile())) {
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputstream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
		}
	}

	/**
	 * Converts {@link MultivaluedMap} to a normal {@link Map} by picking only the
	 * first value.
	 * 
	 * @param queryParameters {@link MultivaluedMap}
	 * @return {@link Map}
	 */
	private Map<String, String> convertToMap(MultivaluedMap<String, String> queryParameters) {
		Map<String, String> map = new HashMap<>();

		for (String key : queryParameters.keySet()) {
			map.put(key, queryParameters.getFirst(key));
		}

		return map;
	}
}
