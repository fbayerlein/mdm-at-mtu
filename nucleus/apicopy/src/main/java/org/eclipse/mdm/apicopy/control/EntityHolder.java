/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.control;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;

import com.google.common.base.Strings;

public class EntityHolder {
	private final Entity entity;
	private final EntityManager entityManager;
	private String name;
	private String path;
	private String key;

	public EntityHolder(Entity entity, EntityManager entityManager) {
		this.entity = entity;
		this.entityManager = entityManager;
	}

	public String getName() {
		if (null == name) {
			name = entity.getName();
		}

		return name;
	}

	public Entity getEntity() {
		return entity;
	}

	public String getKey() {
		if (null == key) {
			key = String.format("%s:%s", entity.getTypeName(), entity.getID());
		}

		return key;
	}

	public String getPath() {
		if (null == path) {
			EntityHolder parent = this;
			List<String> listNames = new LinkedList<>();
			while (null != parent) {
				listNames.add(parent.getName());
				parent = parent.getParent();
			}
			Collections.reverse(listNames);

			path = listNames.stream().collect(Collectors.joining(" / "));
		}

		return path;
	}

	public EntityHolder getParent() {
		if (entity instanceof Environment) {
			return null;
		} else if (entity instanceof Project) {
			return null;
		} else if (entity instanceof Pool) {
			return new EntityHolder(entityManager.loadParent(entity, Project.class).get(), entityManager);
		} else if (entity instanceof Test) {
			return new EntityHolder(entityManager.loadParent(entity, Pool.class).get(), entityManager);
		} else if (entity instanceof TestStep) {
			return new EntityHolder(entityManager.loadParent(entity, Test.class).get(), entityManager);
		} else if (entity instanceof Measurement) {
			return new EntityHolder(entityManager.loadParent(entity, TestStep.class).get(), entityManager);
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (Strings.nullToEmpty(getKey()).hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final EntityHolder other = (EntityHolder) obj;
		String k = getKey();
		if (null == k) {
			if (other.getKey() != null) {
				return false;
			}
		} else if (!k.equals(other.getKey())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format("%s.%s.%s", getEntity().getSourceName(), getEntity().getTypeName(), getEntity().getID());
	}
}
