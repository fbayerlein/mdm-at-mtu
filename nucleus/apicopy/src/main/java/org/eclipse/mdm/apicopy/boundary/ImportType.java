/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apicopy.boundary;

import org.eclipse.mdm.apicopy.control.ApiCopyException;

public enum ImportType {
	ATFX("atfx", "atfxfile", "org.eclipse.mdm.api.atfxadapter.ATFXContextFactory", ".atfx"),
	CSV("csv", "csvFile", "org.eclipse.mdm.csvadapter.CSVApplicationContextFactory", ".csv");

	private final String fileType;
	private final String key;
	private final String contextFactoryClassname;
	private final String suffix;

	private ImportType(String fileType, String key, String contextFactoryClassname, String suffix) {
		this.fileType = fileType;
		this.key = key;
		this.contextFactoryClassname = contextFactoryClassname;
		this.suffix = suffix;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the contextFactoryClassname
	 */
	public String getContextFactoryClassname() {
		return contextFactoryClassname;
	}

	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * @param pKuerzel
	 * @return the enum for the specified fileType
	 */
	public static ImportType findByFileType(final String fileType){
		if (fileType != null) {
			for (final ImportType element : ImportType.values()) {
				if (element.fileType.equals(fileType)) {
					return element;
				}
			}
		}
		throw new ApiCopyException("File type " + fileType + " is nt supported");
	}
}
