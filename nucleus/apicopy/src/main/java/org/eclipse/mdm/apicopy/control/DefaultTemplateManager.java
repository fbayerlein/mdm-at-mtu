/*******************************************************************************
 * Copyright (c) 2019, 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/**
 * 
 */
package org.eclipse.mdm.apicopy.control;

import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;

/**
 * Default implementation of the templatemanager. It read the template names
 * from the attribute 'template' of the source test and test step
 *
 */
public class DefaultTemplateManager implements TemplateManager {

	private static final String TESTATTR_TEMPLATE = "template";

	@Override
	public void setSourceContext(ApplicationContext source) throws ApiCopyException {
	}

	@Override
	public String getTemplateTestName(Test test) throws ApiCopyException {
		String returnVal = null;

		try {
			returnVal = test.getValue(TESTATTR_TEMPLATE).extract(ValueType.STRING);
		} catch (Exception e) {
			// if attribute not exists return null
		}

		return returnVal;
	}

	@Override
	public String getTemplateTestStepName(TestStep testStep) throws ApiCopyException {
		String returnVal = null;

		try {
			returnVal = testStep.getValue(TESTATTR_TEMPLATE).extract(ValueType.STRING);
		} catch (Exception e) {
			// if attribute not exists return null
		}

		return returnVal;
	}

}
