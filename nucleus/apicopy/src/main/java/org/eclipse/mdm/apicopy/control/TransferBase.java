/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apicopy.control;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.file.DescriptiveFileManager;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.AnyTypeValuesBuilder;
import org.eclipse.mdm.api.base.massdata.ComplexNumericalValuesBuilder;
import org.eclipse.mdm.api.base.massdata.ExternalComponentData;
import org.eclipse.mdm.api.base.massdata.NumericalValuesBuilder;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequestBuilder;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.DescriptiveFilesAttachable;
import org.eclipse.mdm.api.base.model.DoubleComplex;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.ExtCompFile;
import org.eclipse.mdm.api.base.model.ExternalComponent;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.FilesAttachable;
import org.eclipse.mdm.api.base.model.FloatComplex;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.MeasuredValues.ValueIterator;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.PhysicalDimension;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.filetransfer.HttpExtRefFileService;
import org.eclipse.mdm.businessobjects.utils.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;

public abstract class TransferBase {
	private static final Logger LOG = LoggerFactory.getLogger(TransferBase.class);

	List<Class<? extends Entity>> supportedRootEntities = Arrays.asList(Project.class, Pool.class, Test.class,
			TestStep.class, Measurement.class, ChannelGroup.class, Channel.class);

	ApplicationContext contextSrc;
	EntityManager entityManagerSrc;
	ModelManager modelManagerSrc;
	Transaction transactionSrc;

	ApplicationContext contextDst;
	EntityManager entityManagerDst;
	EntityFactory entityFactoryDst;
	ModelManager modelManagerDst;
//	Map<FileServiceType, FileService> mapFileServicesDst = new HashMap<>();
	Map<FileServiceType, FileService> mapFileServicesSrc = new HashMap<>();

	Map<EntityHolder, EntityHolder> mapSrcDstEntities = new HashMap<>();
	Multimap<Entity, FileLink> mapFileLinksReplaced = ArrayListMultimap.create();
	Multimap<Entity, FileLink> mapFileLinksUploaded = ArrayListMultimap.create();

	List<File> listFilesDownloadedToTempDir = new ArrayList<>();
	Map<String, FileLink> mapFileLinksExtComp = new HashMap<>();

	public TransferBase(ApplicationContext src, ApplicationContext dst) {
		contextSrc = src;
		entityManagerSrc = contextSrc.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		modelManagerSrc = contextSrc.getModelManager()
				.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));
		contextDst = dst;
		entityManagerDst = contextDst.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		entityFactoryDst = contextDst.getEntityFactory()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityFactory.class));
		modelManagerDst = contextDst.getModelManager()
				.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));

		FileServiceType[] types = new FileServiceType[] { FileServiceType.EXTREF, FileServiceType.AOFILE };

		for (FileServiceType type : types) {
//			if (contextDst.getFileService(type).isPresent()) {
//				mapFileServicesDst.put(type, contextDst.getFileService(type).get());
//			}

			if (contextSrc.getFileService(type).isPresent()) {
				mapFileServicesSrc.put(type, contextSrc.getFileService(type).get());
			}
		}

	}

	ListMultimap<Class<? extends Entity>, Entity> loadParents(List<? extends Entity> entities) {
		List<Entity> list = entities.stream().filter(e -> !isSupported(e)).collect(Collectors.toList());
		if (!list.isEmpty()) {
			throw new ApiCopyException("Entity " + list + " not supported!");
		}
		LinkedListMultimap<Class<? extends Entity>, Entity> byType = LinkedListMultimap.create();
		entities.forEach(e -> byType.put(e.getClass(), e));

		byType.get(Channel.class)
				.forEach(e -> byType.put(ChannelGroup.class, entityManagerSrc.loadParent(e, ChannelGroup.class).get()));
		byType.get(ChannelGroup.class)
				.forEach(e -> byType.put(Measurement.class, entityManagerSrc.loadParent(e, Measurement.class).get()));
		byType.get(Measurement.class)
				.forEach(e -> byType.put(TestStep.class, entityManagerSrc.loadParent(e, TestStep.class).get()));
		byType.get(TestStep.class)
				.forEach(e -> byType.put(Test.class, entityManagerSrc.loadParent(e, Test.class).get()));
		byType.get(Test.class).forEach(e -> byType.put(Pool.class, entityManagerSrc.loadParent(e, Pool.class).get()));
		byType.get(Pool.class)
				.forEach(e -> byType.put(Project.class, entityManagerSrc.loadParent(e, Project.class).get()));

		entities.forEach(e -> byType.remove(e.getClass(), e));
		return byType;
	}

	boolean isSupported(Entity e) {
		return supportedRootEntities.contains(e.getClass());
	}

	<T extends Entity> Optional<T> fetchOne(SearchService searchService, Class<T> entityClass, Filter filter) {
		List<T> results = searchService.fetch(entityClass, filter);

		if (results.isEmpty()) {
			return Optional.empty();
		} else if (results.size() == 1) {
			return Optional.of(results.get(0));
		} else {
			throw new IllegalStateException(String.format("Expected at most one instance of %s, but found %s!",
					entityClass.getName(), results.size()));
		}
	}

	<T extends Entity> Optional<T> fetchOne(EntityManager entityManager, Class<T> entityClass, String name) {
		final List<T> results = entityManager.loadAll(entityClass, escapeWildcards(name));

		if (results.isEmpty()) {
			return Optional.empty();
		} else if (results.size() == 1) {
			return Optional.of(results.get(0));
		} else {
			throw new IllegalStateException(String.format("Expected at most one instance of %s, but found %s!",
					entityClass.getName(), results.size()));
		}
	}

	/**
	 * Escapes wildcard characters *, ? and \ in the given name and returns the
	 * pattern.
	 * 
	 * @param name string to be escaped
	 * @return escaped name
	 */
	protected String escapeWildcards(String name) {
		return name.replace("\\", "\\\\").replace("*", "\\*").replace("?", "\\?");
	}

	<T extends Entity> Optional<T> fetchChild(EntityManager entityManager, Entity parent, Class<T> childEntityClass,
			String name) {
		List<T> results = entityManagerDst.loadChildren(parent, childEntityClass, escapeWildcards(name));
		if (results.isEmpty()) {
			return Optional.empty();
		} else if (results.size() == 1) {
			return Optional.of(results.get(0));
		} else {
			throw new IllegalStateException(String.format("Expected at most one instance of %s, but found %s!",
					childEntityClass.getName(), results.size()));
		}
	}

	boolean isNewEntity(Entity entity) {
		return (Strings.isNullOrEmpty(entity.getID()) || entity.getID().equals("0") || entity.getID().startsWith("-"));
	}

	void persist(Transaction transaction, Entity entity) {
		if (isNewEntity(entity)) {
			transaction.create(Lists.newArrayList(entity));
		} else {
			transaction.update(Lists.newArrayList(entity));
		}
	}

	void persist(Transaction transaction, List<? extends Entity> entities) {
		List<Entity> newEntities = new ArrayList<>();
		List<Entity> updateEntities = new ArrayList<>();
		for (Entity entity : entities) {
			if (isNewEntity(entity)) {
				newEntities.add(entity);
			} else {
				updateEntities.add(entity);
			}
		}
		transaction.create(newEntities);
		transaction.update(updateEntities);
	}

	void closeTransactionSrc() {
		if (transactionSrc != null) {
			transactionSrc.abort();
		}
	}

	List<Channel> copyChannels(List<Channel> channelsSrc, Measurement measurementParentDst,
			List<Channel> existingChannels, Map<String, Optional<Quantity>> quantitiesByName,
			Multimap<String, Unit> unitsByName, Transaction transaction) {
		List<Channel> returnChannels = new ArrayList<>();
		List<Channel> toPersist = new ArrayList<>();

		Map<EntityHolder, Channel> map = new HashMap<>();
		for (Channel channelSrc : channelsSrc) {
			EntityHolder ehSrc = new EntityHolder(channelSrc, entityManagerSrc);

			EntityHolder ehDst = mapSrcDstEntities.get(ehSrc);

			if (null == ehDst) {
				LOG.trace("Copying Channel '{}'", channelSrc.getName());

				Quantity quantity = null;

				if (channelSrc.getQuantity() != null) {
					quantity = quantitiesByName.getOrDefault(channelSrc.getQuantity().getName(), Optional.empty())
							.orElseThrow(() -> new ApiCopyException(
									String.format("Cannot find Quantity with name '%s' in destination!",
											channelSrc.getQuantity().getName())));
				}

				Unit unit = null;

				if (channelSrc.getUnit() != null) {
					try {
						unit = findUnit(unitsByName, channelSrc.getUnit());
//						Collection<Unit> units = unitsByName.get(channelSrc.getUnit().getName());
//
//						if (units.isEmpty()) {
//							throw new ApiCopyException(
//									"Unit " + channelSrc.getUnit().getName() + " not found in target datasource!");
//						} else if (units.size() == 1) {
//							unit = units.iterator().next();
//						} else if (units.size() > 1) {
//							unit = tryToFindUnitByPhysDim(units, channelSrc.getUnit());
//						}
					} catch (Exception e) {
						throw new ApiCopyException("The source unit " + channelSrc.getUnit().getName()
								+ " could not be mapped uniquely to an unit in the target datasource!");
					}
				}

				final Quantity quantityDest = quantity;
				final Unit unitDst = unit;

				final ScalarType scalarType = channelSrc.getScalarType();
				final String description = channelSrc.getDescription();
				final Integer rank = channelSrc.getRank();
				final Integer typeSize = channelSrc.getTypeSize();

				Channel channelDst = existingChannels.stream().filter(c -> c.getName().equals(channelSrc.getName()))
						.findAny().orElseGet(() -> entityFactoryDst.createChannel(channelSrc.getName(),
								measurementParentDst, quantityDest, unitDst, scalarType, description, rank, typeSize));

				copyValues(channelSrc, channelDst, Arrays.asList("Id"), false); // initially ignored in export:
																				// "Name"

				map.put(ehSrc, channelDst);
				toPersist.add(channelDst);

				ehDst = new EntityHolder(channelDst, entityManagerDst);
				mapSrcDstEntities.put(ehSrc, ehDst);
			}

			returnChannels.add((Channel) ehDst.getEntity());
		}
		if (!toPersist.isEmpty()) {
			persist(transaction, toPersist);
		}

		return returnChannels;
	}

	Unit findUnit(Multimap<String, Unit> unitsByName, Unit sourceUnit) {
		Collection<Unit> units = unitsByName.get(sourceUnit.getName());

		if (units.isEmpty()) {
			throw new ApiCopyException("Unit " + sourceUnit.getName() + " not found in target datasource!");
		} else if (units.size() == 1) {
			return units.iterator().next();
		} else {
			return tryToFindUnitByPhysDim(units, sourceUnit);
		}
	}

	/**
	 * Try to find a unit from available units that is compatible with sourceUnit,
	 * based on PhysicalDimensions.
	 * 
	 * @param availableUnits
	 * @param sourceUnit
	 * @return an Unit from availableUnits which is compatible with sourceUnit
	 */
	private Unit tryToFindUnitByPhysDim(Collection<Unit> availableUnits, Unit sourceUnit) {
		PhysicalDimension physDim = sourceUnit.getPhysicalDimension();

		if (physDim == null) {
			throw new ApiCopyException("Unit " + sourceUnit.getName() + " in source has no physical dimension!");
		}

		for (Unit u : availableUnits) {
			if (isEquivalentPhysicalDimension(u.getPhysicalDimension(), sourceUnit.getPhysicalDimension())) {
				LOG.debug("Name conflict for unit " + sourceUnit.getName()
						+ " was resolved by comparing PhysicalDimensions");
				return u;
			}
		}
		LOG.warn("Source unit " + sourceUnit.getName() + " can be mapped to multiple target units " + availableUnits);

		throw new ApiCopyException("For unit " + sourceUnit.getName()
				+ ", no unit with equivalent physical dimension was found in target!");
	}

	private boolean isEquivalentPhysicalDimension(PhysicalDimension physDim1, PhysicalDimension physDim2) {
		return Objects.equal(physDim1.getAngle(), physDim2.getAngle())
				&& Objects.equal(physDim1.getCurrent(), physDim2.getCurrent())
				&& Objects.equal(physDim1.getLength(), physDim2.getLength())
				&& Objects.equal(physDim1.getLuminousIntensity(), physDim2.getLuminousIntensity())
				&& Objects.equal(physDim1.getMass(), physDim2.getMass())
				&& Objects.equal(physDim1.getMolarAmount(), physDim2.getMolarAmount())
				&& Objects.equal(physDim1.getTemperature(), physDim2.getTemperature())
				&& Objects.equal(physDim1.getTime(), physDim2.getTime());
	}

	void copyValues(Entity srcEntity, Entity dstEntity, List<String> ignoredAttributes) {
		copyValues(srcEntity, dstEntity, ignoredAttributes, false);
	}

	void copyValues(Entity srcEntity, Entity dstEntity, List<String> ignoredAttributes, boolean onlyValid) {
		Set<String> valueNamesDst = dstEntity.getValues().keySet();
		for (Map.Entry<String, Value> me : srcEntity.getValues().entrySet()) {
			String key = me.getKey();
			if (!ignoredAttributes.contains(key) && valueNamesDst.contains(key)) {
				Value valueSrc = me.getValue();
				ValueType<?> valueTypeSrc = valueSrc.getValueType();

				if (ValueType.FILE_RELATION.equals(valueTypeSrc) || ValueType.FILE_LINK.equals(valueTypeSrc)
						|| ValueType.FILE_LINK_SEQUENCE.equals(valueTypeSrc) || (onlyValid && !valueSrc.isValid())) {
					continue;
				}

				Value value = dstEntity.getValue(me.getKey());

				try {
					value.set(valueSrc.extract());
				} catch (IllegalArgumentException e) {
					throw new ApiCopyException(
							"Cannot set value for " + dstEntity.getName() + "." + me.getKey() + ": " + e.getMessage(),
							e);
				}
				value.setValid(valueSrc.isValid());
			}
		}
	}

	<T extends MDMFile> void copyFiles(FilesAttachable<T> filesAttachableSrc, FilesAttachable<T> filesAttachableDst,
			Transaction transaction) {
		List<Object> listFileObjectsToCopy = new ArrayList<>();

		for (T mdmFileSrc : filesAttachableSrc.getMDMFiles(entityManagerSrc)) {
			listFileObjectsToCopy.add(mdmFileSrc);
		}

		listFileObjectsToCopy.addAll(getFileLinks(filesAttachableSrc, FilesAttachable.ATTR_FILE_LINKS));

		try {
			if (modelManagerDst.getEntityType(filesAttachableDst).getChildRelations().stream()
					.filter(r -> FilesAttachable.REL_FILE_CHILDREN.equals(r.getName())).findFirst().isPresent()) {
				for (Object obj : listFileObjectsToCopy) {
					T mdmFileDst = entityFactoryDst.createMDMFile(filesAttachableDst);

					copyToMDMFile(filesAttachableSrc, obj, mdmFileDst, getOrCreateTransactionSrc(), transaction);
				}
			} else if (modelManagerDst.getEntityType(filesAttachableDst).getAttributes().stream()
					.filter(a -> FilesAttachable.ATTR_FILE_LINKS.equals(a.getName())).findFirst().isPresent()) {
				copyToExtRef(filesAttachableSrc, filesAttachableDst, FilesAttachable.ATTR_FILE_LINKS,
						listFileObjectsToCopy, transaction);

				persist(transaction, filesAttachableDst);
			}
		} catch (IOException exc) {
			throw new ApiCopyException(
					"Error copying file for " + filesAttachableDst.getName() + ": " + exc.getMessage(), exc);
		}
	}

	void copyFiles(DescriptiveFilesAttachable descriptiveFilesAttachableSrc,
			DescriptiveFilesAttachable descriptiveFilesAttachableDst, Transaction transaction) {
		Set<String> setRelationOrAttrNames = new HashSet<>();

		DescriptiveFileManager descriptiveFilesManagerSrc = new DescriptiveFileManager(modelManagerSrc);
		DescriptiveFileManager descriptiveFilesManagerDst = new DescriptiveFileManager(modelManagerDst);

		setRelationOrAttrNames
				.addAll(descriptiveFilesManagerDst.getDescriptiveFileRelationNames(descriptiveFilesAttachableDst));
		setRelationOrAttrNames.addAll(modelManagerDst.getEntityType(descriptiveFilesAttachableDst).getAttributes()
				.stream().filter(a -> a.getValueType().isFileLinkType()).map(a -> a.getName())
				.collect(Collectors.toList()));

		for (String relationOrAttrName : setRelationOrAttrNames) {
			List<Object> listFileObjectsToCopy = new ArrayList<>();

			if (descriptiveFilesManagerSrc.getDescriptiveFileRelationNames(descriptiveFilesAttachableSrc)
					.contains(relationOrAttrName)) {
				Relation relation = descriptiveFilesManagerSrc.getDescriptiveFileRelation(descriptiveFilesAttachableSrc,
						relationOrAttrName);

				for (DescriptiveFile descriptiveFileSrc : descriptiveFilesAttachableSrc.getDescriptiveFiles(relation,
						entityManagerSrc)) {
					listFileObjectsToCopy.add(descriptiveFileSrc);
				}
			}

			listFileObjectsToCopy.addAll(getFileLinks(descriptiveFilesAttachableSrc, relationOrAttrName));

			try {
				if (descriptiveFilesManagerDst.getDescriptiveFileRelationNames(descriptiveFilesAttachableDst)
						.contains(relationOrAttrName)) {
					EntityType etDescriptiveFileDst = modelManagerDst.getEntityType(DescriptiveFile.class);
					Attribute attrFileChecksumType = etDescriptiveFileDst.getAttribute(MDMFile.ATTR_FILE_CHECKSUM_TYPE);
					Attribute attrFileChecksum = etDescriptiveFileDst.getAttribute(MDMFile.ATTR_FILE_CHECKSUM);
					Relation relation = descriptiveFilesManagerDst
							.getDescriptiveFileRelation(descriptiveFilesAttachableDst, relationOrAttrName);

					objectloop: for (Object obj : listFileObjectsToCopy) {
						DescriptiveFile descriptiveFileDst = null;
						EntityHolder ehSrc = null;
						if (obj instanceof MDMFile) {
							MDMFile mdmFile = (MDMFile) obj;
							ehSrc = new EntityHolder(mdmFile, entityManagerSrc);
							EntityHolder ehDst = mapSrcDstEntities.get(ehSrc);
							if (ehDst == null) {
								String checksumType = mdmFile.getFileChecksumType();
								if (!Strings.isNullOrEmpty(checksumType)) {
									String checksum = mdmFile.getFileChecksum();
									try {
										List<DescriptiveFile> listDescriptiveFiles = entityManagerDst.loadAll(
												DescriptiveFile.class,
												Filter.and()
														.addAll(ComparisonOperator.CASE_INSENSITIVE_EQUAL
																.create(attrFileChecksumType, checksumType),
																ComparisonOperator.CASE_INSENSITIVE_EQUAL
																		.create(attrFileChecksum, checksum)));

										if (listDescriptiveFiles.size() == 1) {
											descriptiveFileDst = listDescriptiveFiles.get(0);
										}
									} catch (UnsupportedOperationException exc) {
									}
								}
							} else {
								descriptiveFileDst = (DescriptiveFile) ehDst.getEntity();
							}

							if (descriptiveFileDst != null) {
								descriptiveFilesAttachableDst.addDescriptiveFile(relation, descriptiveFileDst);
								// persist(transaction, descriptiveFileDst);

								continue objectloop;
							}
						}

						descriptiveFileDst = entityFactoryDst.createDescriptiveFile(descriptiveFilesAttachableDst,
								relation);
						copyToMDMFile(descriptiveFilesAttachableSrc, obj, descriptiveFileDst,
								getOrCreateTransactionSrc(), transaction);
						if (ehSrc != null) {
							mapSrcDstEntities.put(ehSrc, new EntityHolder(descriptiveFileDst, entityManagerDst));
						}
					}
				} else if (modelManagerDst.getEntityType(descriptiveFilesAttachableDst).getAttributes().stream()
						.filter(a -> (relationOrAttrName.equals(a.getName()) && a.getValueType().isFileLinkType()))
						.findFirst().isPresent()) {
					copyToExtRef(descriptiveFilesAttachableSrc, descriptiveFilesAttachableDst, relationOrAttrName,
							listFileObjectsToCopy, transaction);
				}
			} catch (IOException exc) {
				throw new ApiCopyException("Error copying file for " + descriptiveFilesAttachableDst.getName() + "."
						+ relationOrAttrName + ": " + exc.getMessage(), exc);
			}
		}
	}

	private List<FileLink> getFileLinks(Entity descriptiveFilesAttachableSrc, String relationOrAttrName) {

		if (!(mapFileServicesSrc.get(FileServiceType.EXTREF) instanceof HttpExtRefFileService)) {
			Value valueSrc = descriptiveFilesAttachableSrc.getValue(relationOrAttrName, true);
			if (valueSrc != null && valueSrc.isValid()) {
				ValueType<?> vt = valueSrc.getValueType();
				if (ValueType.FILE_LINK.equals(vt)) {
					return Arrays.asList(valueSrc.extract(ValueType.FILE_LINK));
				} else if (ValueType.FILE_LINK_SEQUENCE.equals(vt)) {
					return Arrays.asList(valueSrc.extract(ValueType.FILE_LINK_SEQUENCE));
				}
			}
		}
		return Collections.emptyList();
	}

	void copyToMDMFile(Entity entitySrc, Object obj, MDMFile mdmFileDst, Transaction transactionSrc,
			Transaction transactionDst) throws IOException {
		MDMFile mdmFileSrc = (obj instanceof MDMFile ? (MDMFile) obj : null);
		FileLink fileLinkSrc = (mdmFileSrc == null ? (FileLink) obj : mdmFileSrc.getFileLink());

		if (mdmFileSrc != null) {
			copyValues(mdmFileSrc, mdmFileDst, Arrays.asList("Id", "Location"), false);
		} else {
			mdmFileDst.setName(fileLinkSrc.getFileName());
			mdmFileDst.setOriginalFileName(fileLinkSrc.getFileName());
			mdmFileDst.setDescription(fileLinkSrc.getDescription());
			mdmFileDst.setFileMimeType(fileLinkSrc.getMimeType().toString());
		}

		FileLink fileLink = downloadSourceFile(entitySrc, fileLinkSrc, transactionSrc);

		mdmFileDst.setFileLink(fileLink);

		// mdmFileDst must exist so upload can find the instance:
		persist(transactionDst, mdmFileDst);

		transactionDst.getFileService(FileServiceType.AOFILE).uploadSequential(mdmFileDst,
				Collections.singletonList(fileLink), transactionDst, null);
		mdmFileDst.setLocation(fileLink.getRemotePath());
		if (mdmFileSrc == null) {
			File localFile = fileLink.getLocalPath().toFile();
			mdmFileDst.setSize(localFile.length());
			mdmFileDst.setFileChecksumType(MDMFile.HASH_ALGORITHM_SHA256);
			mdmFileDst.setFileChecksum(MDMFile.getHash(localFile, MDMFile.HASH_ALGORITHM_SHA256));
		}

		persist(transactionDst, mdmFileDst);
	}

	void copyToExtRef(Entity entitySrc, Entity entityDst, String attrName, List<Object> listFileObjectsToCopy,
			Transaction transaction) throws IOException {

		if (transaction.getFileService(FileServiceType.EXTREF) instanceof HttpExtRefFileService) {
			LOG.warn("Ignoring ExternalReferences {} at {}.{}", listFileObjectsToCopy, entitySrc.getName(), attrName);
			return;
		}

		Value value = entityDst.getValue(attrName);

		collectReplacedFileLinks(entityDst, value, isReplaceFileLinks());

		List<FileLink> listFileLinks = new ArrayList<>();

		for (Object obj : listFileObjectsToCopy) {
			MDMFile mdmFileSrc = (obj instanceof MDMFile ? (MDMFile) obj : null);
			FileLink fileLinkSrc = (mdmFileSrc == null ? (FileLink) obj : mdmFileSrc.getFileLink());

			listFileLinks.add(downloadSourceFile(entitySrc, fileLinkSrc, getOrCreateTransactionSrc()));
		}

		transaction.getFileService(FileServiceType.EXTREF).uploadSequential(entityDst, listFileLinks, transaction,
				null);
		mapFileLinksUploaded.putAll(entityDst, listFileLinks);

		ValueType<?> vt = value.getValueType();

		if (ValueType.FILE_LINK_SEQUENCE.equals(vt) && !isReplaceFileLinks()) {
			listFileLinks.addAll(Arrays.asList(value.extract(ValueType.FILE_LINK_SEQUENCE)));
		}

		try {
			if (ValueType.FILE_LINK.equals(vt) && listFileLinks.size() > 0) {
				value.set(listFileLinks.get(0));
			} else if (ValueType.FILE_LINK_SEQUENCE.equals(vt)) {
				value.set(listFileLinks.toArray(new FileLink[listFileLinks.size()]));
				value.setValid(listFileLinks.size() > 0);
			}
		} catch (IllegalArgumentException e) {
			throw new ApiCopyException(
					"Cannot set value for " + entityDst.getName() + "." + attrName + ": " + e.getMessage(), e);
		}
	}

	private void collectReplacedFileLinks(Entity dstEntity, Value value, boolean replaceFileLinks) {
		if (ValueType.FILE_LINK.equals(value.getValueType()) && value.isValid()) {
			FileLink fileToDelete = value.extract();
			mapFileLinksReplaced.put(dstEntity, fileToDelete);
		} else if (ValueType.FILE_LINK_SEQUENCE.equals(value.getValueType()) && replaceFileLinks) {
			FileLink[] fileLinksToDelete = value.extract();
			mapFileLinksReplaced.putAll(dstEntity, Arrays.asList(fileLinksToDelete));
		}
	}

	private FileLink downloadSourceFile(Entity srcEntity, FileLink fileLinkSrc, Transaction transaction)
			throws IOException {
		Path targetPath = Files.createTempDir().toPath();
		Path filePathAbsolute = targetPath.resolve(fileLinkSrc.getFileName());
		mapFileServicesSrc.get(fileLinkSrc.getFileServiceType()).download(srcEntity, targetPath, fileLinkSrc,
				transaction);
		listFilesDownloadedToTempDir.add(filePathAbsolute.toFile());
		return FileLink.newLocal(filePathAbsolute, null, fileLinkSrc.getFileServiceType());
	}

	private void copyExtCompFiles(ExternalComponentData extCompData, boolean isValuesFile, Entity entitySrc,
			List<FileLink> listFileLinksToUpload, Transaction transactionSrc, Transaction transactionDst)
			throws IOException {
		String extCompFileId = (isValuesFile ? extCompData.getValuesExtCompFileId()
				: extCompData.getFlagsExtCompFileId());
		ExtCompFile extCompFileSrc = (!Strings.isNullOrEmpty(extCompFileId)
				? entityManagerSrc.load(ExtCompFile.class, extCompFileId)
				: null);

		Object objToCopy = null;

		if (extCompFileSrc == null) {
			if (isValuesFile) {
				objToCopy = extCompData.getFileLink();
			} else {
				objToCopy = extCompData.getFlagsFileLink();
			}
		} else {
			objToCopy = extCompFileSrc;
		}

		if (objToCopy != null) {
			String fileLinkIdentifier = Serializer.getFileLinkIdentifier(
					objToCopy instanceof MDMFile ? ((MDMFile) objToCopy).getFileLink() : (FileLink) objToCopy);
			FileLink fileLink = mapFileLinksExtComp.get(fileLinkIdentifier);

			if (modelManagerDst.listEntityTypes().stream().filter(et -> ExtCompFile.TYPE_NAME.equals(et.getName()))
					.findFirst().isPresent()
					&& modelManagerDst.getEntityType(ExternalComponent.class).getRelations().stream().filter(
							r -> (isValuesFile ? ExternalComponent.REL_VALUESFILE : ExternalComponent.REL_FLAGSFILE)
									.equals(r.getName()))
							.findFirst().isPresent()) {
				ExtCompFile extCompFileDst = null;
				if (fileLink == null) {
					extCompFileDst = entityFactoryDst.createExtCompFile();
					copyToMDMFile(entitySrc, objToCopy, extCompFileDst, transactionSrc, transactionDst);
					mapFileLinksExtComp.put(fileLinkIdentifier, extCompFileDst.getFileLink());
				} else {
					if (fileLink.getRemoteObject() instanceof ExtCompFile) {
						extCompFileDst = (ExtCompFile) fileLink.getRemoteObject();
					} else {
						throw new ApiCopyException("Internal error: Illegal entry in file link map!");
					}
				}

				if (isValuesFile) {
					extCompData.setValuesExtCompFile(extCompFileDst);
					extCompData.setValuesExtCompFileId(extCompFileDst.getID());
					extCompData.setFileLink(null);
				} else {
					extCompData.setFlagsExtCompFile(extCompFileDst);
					extCompData.setFlagsExtCompFileId(extCompFileDst.getID());
					extCompData.setFlagsFileLink(null);
				}
			} else {
				if (fileLink == null) {
					FileLink fileLinkSrc = objToCopy instanceof MDMFile ? ((MDMFile) objToCopy).getFileLink()
							: (FileLink) objToCopy;

					fileLink = downloadSourceFile(entitySrc, fileLinkSrc, transactionSrc);
					mapFileLinksExtComp.put(fileLinkIdentifier, fileLink);
					listFileLinksToUpload.add(fileLink);
				}

				if (isValuesFile) {
					extCompData.setValuesExtCompFileId(null);
					extCompData.setValuesExtCompFile(null);
					extCompData.setFileLink(fileLink);
				} else {
					extCompData.setFlagsExtCompFileId(null);
					extCompData.setFlagsExtCompFile(null);
					extCompData.setFlagsFileLink(fileLink);
				}
			}
		}
	}

	WriteRequest createWriteRequest(EntityManager entityManagerSrc, ChannelGroup channelGroupDst, Channel channelDst,
			MeasuredValues measuredValues, Unit sourceUnit, Channel channelSrc, Transaction transaction) {
		WriteRequestBuilder wrb = WriteRequest.create(channelGroupDst, channelDst, measuredValues.getAxisType())
				.setMimeType(measuredValues.getMimeType());
		NumericalValuesBuilder builder = null;
		SequenceRepresentation seqRep = measuredValues.getSequenceRepresentation();
		ScalarType scalarType = measuredValues.getScalarType();
		boolean independent = measuredValues.isIndependent();
		double[] generationParameters = measuredValues.getGenerationParameters();

		if (seqRep.isExternal() && measuredValues.hasExternalComponents()) {
			try {
				List<FileLink> listFileLinksToUpload = new ArrayList<>();
				for (ExternalComponentData extCompData : measuredValues.getExternalComponentData()) {
					copyExtCompFiles(extCompData, true, channelSrc, listFileLinksToUpload, getOrCreateTransactionSrc(),
							transaction);
					copyExtCompFiles(extCompData, false, channelSrc, listFileLinksToUpload, getOrCreateTransactionSrc(),
							transaction);
				}

				if (mapFileServicesSrc.get(FileServiceType.EXTREF) != null
						&& transaction.getFileService(FileServiceType.EXTREF) != null
						&& !listFileLinksToUpload.isEmpty()) {
					transaction.getFileService(FileServiceType.EXTREF).uploadSequential(channelDst,
							listFileLinksToUpload, transaction, null);
					mapFileLinksUploaded.putAll(channelDst, listFileLinksToUpload);
				}
			} catch (IOException e) {
				throw new ApiCopyException(e.getLocalizedMessage(), e);
			}
		}

		if (SequenceRepresentation.EXPLICIT.equals(seqRep)) {
			builder = wrb.explicit();
		} else if (SequenceRepresentation.EXPLICIT_EXTERNAL.equals(seqRep)) {
			return wrb.explicitExternal().externalComponents(scalarType, measuredValues.getExternalComponentData())
					.independent(independent).build();
		} else if (SequenceRepresentation.IMPLICIT_CONSTANT.equals(seqRep)) {
			checkGenerationParameters(generationParameters, 1);

			return wrb.implicitConstant(scalarType, generationParameters[0]).build();
		} else if (SequenceRepresentation.IMPLICIT_LINEAR.equals(seqRep)) {
			checkGenerationParameters(generationParameters, 2);

			return wrb.implicitLinear(scalarType, generationParameters[0], generationParameters[1])
					.independent(independent).build();
		} else if (SequenceRepresentation.IMPLICIT_SAW.equals(seqRep)) {
			checkGenerationParameters(generationParameters, 3);

			return wrb
					.implicitSaw(scalarType, generationParameters[0], generationParameters[1], generationParameters[2])
					.build();
		} else if (SequenceRepresentation.RAW_LINEAR.equals(seqRep)) {
			checkGenerationParameters(generationParameters, 2);

			builder = wrb.rawLinear(generationParameters[0], generationParameters[1]);

		} else if (SequenceRepresentation.RAW_LINEAR_EXTERNAL.equals(seqRep)) {
			checkGenerationParameters(generationParameters, 2);

			return wrb.rawLinearExternal(generationParameters[0], generationParameters[1])
					.externalComponents(scalarType, measuredValues.getExternalComponentData()).independent(independent)
					.build();
		} else if (SequenceRepresentation.RAW_LINEAR_CALIBRATED.equals(seqRep)) {
			checkGenerationParameters(generationParameters, 3);

			builder = wrb.rawLinearCalibrated(generationParameters[0], generationParameters[1],
					generationParameters[2]);
		} else if (SequenceRepresentation.RAW_LINEAR_CALIBRATED_EXTERNAL.equals(seqRep)) {
			checkGenerationParameters(generationParameters, 3);

			return wrb
					.rawLinearCalibratedExternal(generationParameters[0], generationParameters[1],
							generationParameters[2])
					.externalComponents(scalarType, measuredValues.getExternalComponentData()).independent(independent)
					.build();
		} else if (SequenceRepresentation.RAW_POLYNOMIAL.equals(seqRep)) {
			builder = wrb.rawPolynomial(generationParameters);
		} else if (SequenceRepresentation.RAW_POLYNOMIAL_EXTERNAL.equals(seqRep)) {
			return wrb.rawPolynomialExternal(generationParameters)
					.externalComponents(scalarType, measuredValues.getExternalComponentData()).independent(independent)
					.build();
		}

		if (scalarType.isString()) {
			String[] values = new String[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<String> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, AnyTypeValuesBuilder.class).stringValues(values, flags).build();
		} else if (scalarType.isDate()) {
			Instant[] values = new Instant[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Instant> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, AnyTypeValuesBuilder.class).dateValues(values, flags).independent(independent)
					.build();
		} else if (scalarType.isBoolean()) {
			boolean[] values = new boolean[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Boolean> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, AnyTypeValuesBuilder.class).booleanValues(values, flags).build();
		} else if (scalarType.isByte()) {
			byte[] values = new byte[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Byte> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return builder.byteValues(values, flags).independent(independent).build();
		} else if (scalarType.isShort()) {
			short[] values = new short[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Short> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return builder.shortValues(values, flags).sourceUnit(sourceUnit).independent(independent).build();
		} else if (scalarType.isInteger()) {
			int[] values = new int[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Integer> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return builder.integerValues(values, flags).sourceUnit(sourceUnit).independent(independent).build();
		} else if (scalarType.isLong()) {
			long[] values = new long[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Long> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return builder.longValues(values, flags).sourceUnit(sourceUnit).independent(independent).build();
		} else if (scalarType.isFloat()) {
			float[] values = new float[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Float> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return builder.floatValues(values, flags).sourceUnit(sourceUnit).independent(independent).build();
		} else if (scalarType.isDouble()) {
			double[] values = new double[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Double> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return builder.doubleValues(values, flags).sourceUnit(sourceUnit).independent(independent).build();
		} else if (scalarType.isByteStream()) {
			byte[][] values = new byte[measuredValues.getLength()][];
			short[] flags = new short[values.length];
			ValueIterator<byte[]> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, AnyTypeValuesBuilder.class).byteStreamValues(values, flags).build();
		} else if (scalarType.isFloatComplex()) {
			FloatComplex[] values = new FloatComplex[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<FloatComplex> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, ComplexNumericalValuesBuilder.class).floatComplexValues(values, flags)
					.sourceUnit(sourceUnit).build();
		} else if (scalarType.isDoubleComplex()) {
			DoubleComplex[] values = new DoubleComplex[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<DoubleComplex> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, ComplexNumericalValuesBuilder.class).doubleComplexValues(values, flags)
					.sourceUnit(sourceUnit).build();
		} else if (scalarType.isFileLink()) {
			FileLink[] values = new FileLink[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<FileLink> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, AnyTypeValuesBuilder.class).fileLinkValues(values, flags).build();
		} else if (scalarType.isBlob()) {
			Object[] values = new Object[measuredValues.getLength()];
			short[] flags = new short[values.length];
			ValueIterator<Object> iter = measuredValues.iterator();
			int count = 0;
			while (iter.hasNext()) {
				flags[count] = iter.getFlag();
				values[count++] = iter.next();
			}

			return castBuilder(builder, AnyTypeValuesBuilder.class).blobValue(values, flags).build();
		} else {
			throw new IllegalStateException(
					String.format("Unsupported ScalarType %s in MeasuredValues!", scalarType.name()));
		}
	}

	void checkGenerationParameters(double[] generationParameters, int expectedLength) {
		if (null == generationParameters || generationParameters.length < expectedLength) {
			throw new IllegalStateException(String.format("Number of generation parameters is %d, expected %d!",
					(null == generationParameters ? 0 : generationParameters.length), expectedLength));
		}
	}

	<T extends NumericalValuesBuilder> T castBuilder(NumericalValuesBuilder builder, Class<T> cls) {
		if (!builder.getClass().isAssignableFrom(cls)) {
			throw new IllegalStateException(String.format(
					"Error creating the write values builder, expected class is %s, actual class is %s (likely column data type and sequence representation mismatch)!",
					cls.getName(), (null == builder ? "???" : builder.getClass().getName())));
		}

		return cls.cast(builder);
	}

	/**
	 * Delete all replaced file links
	 */
	void deleteFilesOfReplacedFileLinks(Transaction transaction) {
		mapFileLinksReplaced.asMap().forEach((entity, fileLinkList) -> {
			transaction.getFileService(FileServiceType.EXTREF).delete(entity, fileLinkList.stream()
					.filter(fl -> fl.getFileServiceType() == FileServiceType.EXTREF).collect(Collectors.toList()),
					transaction);
		});
	}

	void clearReplacedFileLinkCache() {
		mapFileLinksReplaced.clear();
	}

	/**
	 * Delete all uploaded files, is necessary if the Import will be canceled
	 */
	void deleteFilesOfUploadedFileLinks(Transaction transaction) {
		mapFileLinksUploaded.asMap().forEach((entity, fileLinkList) -> {
			transaction.getFileService(FileServiceType.EXTREF).delete(entity, fileLinkList.stream()
					.filter(fl -> fl.getFileServiceType() == FileServiceType.EXTREF).collect(Collectors.toList()),
					transaction);
		});
	}

	void deleteLocalTempFiles() {
		listFilesDownloadedToTempDir.forEach(file -> deleteFileAndEmptyParentFolder(file));
	}

	void clearUploadedFileLinkCache() {
		mapFileLinksUploaded.clear();
	}

	void deleteFileAndEmptyParentFolder(File file) {
		file.delete();
		Path dir = Paths.get(file.getAbsolutePath()).getParent();
		if (dir != null && dir.toFile().list().length == 0) {
			dir.toFile().delete();
		}
	}

	List<Channel> loadIndependent(ApplicationContext context, ChannelGroup channelGroup) {
		ModelManager mm = context.getModelManager()
				.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));
		QueryService queryService = context.getQueryService()
				.orElseThrow(() -> new ServiceNotProvidedException(QueryService.class));
		EntityManager em = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));

		EntityType etChannel = mm.getEntityType(Channel.class);

		Attribute channelId = etChannel.getIDAttribute();
		Attribute channelGroupId = mm.getEntityType(ChannelGroup.class).getIDAttribute();
		Attribute ind = mm.getEntityType("LocalColumn").getAttribute("IndependentFlag");

		List<Result> results = queryService.createQuery().select(channelId)
				.fetch(Filter.and().add(ComparisonOperator.EQUAL.create(ind, (short) 1))
						.add(ComparisonOperator.EQUAL.create(channelGroupId, channelGroup.getID())));

		List<String> ids = results.stream().map(r -> r.getRecord(etChannel).getID()).collect(Collectors.toList());

		return em.load(Channel.class, ids);
	}

	public boolean isReplaceFileLinks() {
		return false;
	}

	private Transaction getOrCreateTransactionSrc() {
		if (transactionSrc == null) {
			LOG.debug("Source transaction started");
			transactionSrc = entityManagerSrc.startTransaction();
		}
		return transactionSrc;
	}
}
