/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_FILE_TYPE;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import org.eclipse.mdm.shoppingbasket.entity.ShoppingBasket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Export")
@javax.ws.rs.Path("/export/{" + REQUESTPARAM_FILE_TYPE + "}")
public class ExportResource {
	private static final Logger LOG = LoggerFactory.getLogger(ExportResource.class);

	@EJB
	private ExportService exportService;

	/**
	 * Default public constructor for JAX-RS
	 */
	public ExportResource() {
	}

	/**
	 * Used to provide service for unit tests
	 * 
	 * @param exportService
	 */
	protected ExportResource(ExportService exportService) {
		this.exportService = exportService;
	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces({ MediaType.APPLICATION_XML, "application/zip", MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response export(@PathParam(REQUESTPARAM_FILE_TYPE) String fileType, @Context UriInfo uriInfo,
			ShoppingBasket basket) throws IOException {
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		final ExportType exportType = ExportType.findByFileType(fileType);
		final Path exportFile = exportService.export(basket, exportType, queryParams);

		final List<Path> paths;
		try (Stream<Path> files = Files.walk(exportFile)) {
			paths = files.filter(p -> p.toFile().isFile()).collect(Collectors.toList());
		}

		if (paths.size() == 1) {
			StreamingOutput output = new StreamingOutput() {
				@Override
				public void write(OutputStream out) throws IOException {
					try (final InputStream responseStream = Files.newInputStream(paths.get(0))) {
						int length;
						byte[] buffer = new byte[1024];
						while ((length = responseStream.read(buffer)) != -1) {
							out.write(buffer, 0, length);
						}
						out.flush();
					} finally {
						if (exportFile != null) {
							try {
								deleteDirectory(exportFile);
							} catch (IOException e) {
								LOG.warn("Could not delete temporary directory "
										+ exportFile.getParent().toFile().getAbsolutePath(), e);
							}
						}
					}
				}
			};

			return Response.ok(output, exportType.getMediaType())
					.header("content-disposition", this.getContentDisposition(exportType.getSuffix())).build();
		} else {
			StreamingOutput out = new StreamingOutput() {
				@Override
				public void write(OutputStream output) throws IOException, WebApplicationException {
					try (ZipOutputStream out = new ZipOutputStream(output)) {
						for (Path p : paths) {
							out.putNextEntry(new ZipEntry(exportFile.relativize(p).toString()));
							try (InputStream in = Files.newInputStream(p)) {
								int read = 0;
								byte[] bytes = new byte[1024];

								while ((read = in.read(bytes)) != -1) {
									out.write(bytes, 0, read);
								}
							}
						}
					} finally {
						if (exportFile != null) {
							try {
								deleteDirectory(exportFile);
							} catch (IOException e) {
								LOG.warn(
										"Could not delete temporary directory " + exportFile.toFile().getAbsolutePath(),
										e);
							}
						}
					}
				}
			};
			return Response.ok(out, "application/zip").header("content-disposition", this.getContentDisposition(".zip"))
					.build();
		}
	}

	/**
	 * Deletes a directory.
	 * 
	 * @param pathToBeDeleted directory to delete
	 * @throws IOException
	 */
	private void deleteDirectory(java.nio.file.Path pathToBeDeleted) throws IOException {
		try (Stream<Path> stream = Files.walk(pathToBeDeleted)) {
			stream.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
		}
	}

	private String getContentDisposition(String fileExt) {
		StringBuilder sb = new StringBuilder("attachment; filename = MDMExport_");
		sb.append(String.format("%1$tF_%1$tH-%1$tM-%1$tS", new Date()));
		sb.append(fileExt);
		return sb.toString();
	}
}
