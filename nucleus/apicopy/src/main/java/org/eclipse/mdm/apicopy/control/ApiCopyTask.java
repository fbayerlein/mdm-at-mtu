/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.control;

import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.model.Entity;

public interface ApiCopyTask {

	void copy(List<? extends Entity> entities);

	/**
	 * Setting a optional mapping of source unit names to target unit names
	 * 
	 * @param unitMapping key = source unit name, value = target unit name
	 */
	void setUnitMapping(Map<String, String> unitMapping);

	/**
	 * Setting a optional mapping of source quantity names to target quantity names
	 * 
	 * @param quantityMapping key = source quantity name, value = target quantity
	 *                        name
	 */
	void setQuantityMapping(Map<String, String> quantityMapping);

	/**
	 * setting a map with properties to configure the transfer task
	 * 
	 * @param properties
	 */
	void setProperties(Map<String, String> properties);
}
