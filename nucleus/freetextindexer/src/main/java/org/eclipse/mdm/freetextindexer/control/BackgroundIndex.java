/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.freetextindexer.control;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.freetextindexer.boundary.ElasticsearchBoundary;
import org.eclipse.mdm.freetextindexer.boundary.MdmApiBoundary;
import org.eclipse.mdm.freetextindexer.entities.MDMEntityResponse;
import org.eclipse.mdm.property.GlobalProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Background index worker which will synchronize MDM entities with Elastic
 * Search
 *
 * -> Configuration parameter:
 * freetextindexer.background.<entityclass>.<freetext>=<day_upper>,<day_lower>,<only_every_n_days>
 * -> Batchsize parameter:
 * freetextindexer.background.batchsize=<amount_of_entities_in_one_batch>
 *
 * Open implementation point: priorisize different time frames with a fourth
 * parameter <priority>
 */
@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
@Startup
@Singleton
@DependsOn({ "MdmApiBoundary" })
public class BackgroundIndex {

	private static final Logger LOGGER = LoggerFactory.getLogger(BackgroundIndex.class);

	@EJB
	ElasticsearchBoundary esBoundary;

	@EJB
	MdmApiBoundary apiBoundary;

	@EJB
	MDMDatabaseHandler mdmDBHandler;

	@Inject
	UpdateIndex updateIndex;

	@Inject
	@GlobalProperty(value = "freetext.active")
	private String active = "false";

	boolean processing = false;

	@PostConstruct
	public void postConstruct() {
		processing = false;
	}

	/**
	 * Main entry method of the scheduler which will trigger the synchronization
	 */
	@Schedule(hour = "*", minute = "*/3", persistent = false)
	@Lock(LockType.WRITE)
	public void triggerBackgroundWork() {

		if (!isActive()) {
			LOGGER.trace("Background indexing disabled, because freetext.active=false.");
			return;
		}

		if (!processing) {
			processing = true;

			try {
				LOGGER.info("Starting BackgroundIndex.triggerBackgroundWork()");
				Map<String, String> lastRuntimes = mdmDBHandler.getLastRuntimes("freetextindexer.background.");
				String batchSize = mdmDBHandler.getPreferences("freetextindexer.background.batchsize").values().stream()
						.findAny().orElse(null);

				Map<String, String> preferences = mdmDBHandler
						.getPreferences("freetextindexer.background.measurement.");
				Set<String> keySet = preferences.keySet();
				for (String key : keySet) {
					processEntitiesByTimeframe(key, Measurement.class, preferences.get(key), lastRuntimes.get(key),
							batchSize);
				}

				preferences = mdmDBHandler.getPreferences("freetextindexer.background.teststep.");
				keySet = preferences.keySet();
				for (String key : keySet) {
					processEntitiesByTimeframe(key, TestStep.class, preferences.get(key), lastRuntimes.get(key),
							batchSize);
				}

				preferences = mdmDBHandler.getPreferences("freetextindexer.background.test.");
				keySet = preferences.keySet();
				for (String key : keySet) {
					processEntitiesByTimeframe(key, Test.class, preferences.get(key), lastRuntimes.get(key), batchSize);
				}

			} catch (Throwable t) {
				LOGGER.error("BackgroundIndex exception caught: " + t.getClass().toString() + " with message: "
						+ t.getLocalizedMessage());
			}
			LOGGER.info("Completing BackgroundIndex.triggerBackgroundWork()");
			processing = false;
		}

	}

	/**
	 * Process entities that are within a certain time frame
	 * 
	 * @param key         The current configuration key
	 * @param clazz       The entity class to process e.g. test, teststep,
	 *                    measurement
	 * @param timeFrame   comma separated values of day_from and day_until and
	 *                    only_every_n_days
	 * @param lastRuntime comma separated values of last_runtime and optional
	 *                    last_interval_time where as last_interval_time is the time
	 *                    where the next batch should continue within the
	 *                    last_runtime interval to split high amounts of entities
	 *                    into multiple execution batches
	 * @param batchSize   The batch size which defines how many entities should be
	 *                    processed in one run
	 */
	private void processEntitiesByTimeframe(String key, Class<? extends Entity> clazz, String timeFrame,
			String lastRuntime, String batchSize) {
		String[] timeFrames = timeFrame.split(",");
		String[] lastRuntimes = lastRuntime != null ? lastRuntime.split(",") : new String[0];
		boolean hasUnfinishedBatch = lastRuntimes.length == 2 && lastRuntimes[1].length() > 0;

		Calendar executionTime = Calendar.getInstance();
		// check if we are within the execution timeframe or past it
		if (!hasUnfinishedBatch && lastRuntimes.length > 0 && lastRuntimes[0].length() > 0 && timeFrames.length == 3) {
			executionTime.setTimeInMillis(Long.parseLong(lastRuntimes[0]));
			executionTime.add(Calendar.DAY_OF_YEAR, Integer.parseInt(timeFrames[2]));
			if (Calendar.getInstance().getTimeInMillis() < executionTime.getTimeInMillis()) {
				// execution runtime too soon, wait another cycle
				return;
			}
		}

		Calendar calendarUpper = Calendar.getInstance();
		calendarUpper.add(Calendar.DAY_OF_YEAR, Integer.parseInt(timeFrames[0]));

		// determine the lower calendar date
		Date dateLower = null;
		if (timeFrames.length >= 2) {
			Calendar calendarLower = Calendar.getInstance();
			calendarLower.add(Calendar.DAY_OF_YEAR, Integer.parseInt(timeFrames[1]));
			dateLower = calendarLower.getTime();
		}

		if (hasUnfinishedBatch) {
			// delimit upper calendar to this date
			try {
				calendarUpper.setTimeInMillis(Long.parseLong(lastRuntimes[1]));
			} catch (NumberFormatException ignored) {
				// don't delimit if it failed, next run will fix this
			}
		}

		List<MDMEntityResponse> entities = apiBoundary.loadEntitiesOfTypeWithinTimeFrame(clazz, calendarUpper.getTime(),
				dateLower);
		LOGGER.debug("Found BackgroundIndex.processEntitiesByTimeframe() Class: " + clazz.getName() + ", Results: "
				+ entities.size() + ", Key: " + key);
		int maxEntities = Integer.MAX_VALUE;
		if (batchSize != null) {
			maxEntities = Integer.parseInt(batchSize);
		}

		String lastDate = "";
		// limit entites on batch size
		if (maxEntities < entities.size()) {
			int lower = entities.size() - maxEntities;
			if (lower < 0) {
				lower = 0;
			}
			int sizeBefore = entities.size();
			entities = entities.subList(lower, entities.size());
			if (entities.size() != sizeBefore) {
				// only if we reduced the list we need to continue at this date descending
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
				Calendar calendar = Calendar.getInstance();
				try {
					calendar.setTime(sdf.parse(entities.get(0).data.attributes.get("DateCreated")));
					lastDate = Long.toString(calendar.getTimeInMillis());
				} catch (ParseException error) {
					LOGGER.error("BackgroundIndex DateParseException" + error);
				}
			}
			LOGGER.debug("Limiting BackgroundIndex.processEntitiesByTimeframe() to Results: " + entities.size());
		}

		// update the runtime for this process key
		mdmDBHandler.setLastRuntime(key, Calendar.getInstance().getTimeInMillis() + "," + lastDate);

		// synchronize into elastic search
		entities.stream().forEach(e -> {
			LOGGER.debug("BackgroundIndex: Entity: " + e.toString());
			updateIndex.change(e);
		});
	}

	private boolean isActive() {
		return Boolean.parseBoolean(active);
	}

}
