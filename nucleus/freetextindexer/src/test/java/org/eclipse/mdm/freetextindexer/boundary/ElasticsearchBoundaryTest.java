/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.freetextindexer.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpHost;
import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.freetextindexer.entities.MDMEntityResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import com.google.common.collect.ImmutableMap;

@Ignore
//FIXME 05.02.2020: this test needs a docker to run elasticsearch and is not suitable for continous build in Jenkins.
//Comment this in for local tests only, if docker is available
public class ElasticsearchBoundaryTest {

	private static String atfxFilePath = "../org.eclipse.mdm.api.atfxadapter/src/test/resources/";
	private static String atfxFile = "Right_acc.atfx";

	private static ApplicationContext context;
	private static String sourceName;
	private static RestHighLevelClient client;

	private ElasticsearchBoundary es;

	@ClassRule
	public static ElasticsearchContainer elasticSearch = new ElasticsearchContainer(
			"docker.elastic.co/elasticsearch/elasticsearch-oss:7.0.0").withEnv("discovery.type", "single-node");

	@BeforeClass
	public static void setup() throws ConnectionException {
		context = new ATFXContextFactory().connect("ATFX", ImmutableMap.of("atfxfile", atfxFilePath + atfxFile));
		Environment env = context.getEntityManager().get().loadAll(Environment.class).get(0);
		sourceName = env.getSourceName().toLowerCase();

		client = new RestHighLevelClient(RestClient.builder(HttpHost.create(elasticSearch.getHttpHostAddress())));
	}

	@Before
	public void init() throws DataAccessException, ConnectionException {
		es = new ElasticsearchBoundary();
		es.esAddress = "http://" + elasticSearch.getHttpHostAddress();
		es.active = "true";
		es.init();
	}

	@Test
	public void indexSuccessfullyCreated_CaseDoesNotMatter() throws InterruptedException, ExecutionException {
		es.createIndex("BlA");
		assertThat(es.hasIndex("bla")).isTrue();
	}

	@Test
	public void indexSuccessfullyCreated_OtherIndizesNot() throws InterruptedException, ExecutionException {
		es.createIndex("someIndex");
		assertThat(es.hasIndex("asdf")).isFalse();
	}

	@Test
	public void deletedDoc_isGone() throws InterruptedException, DataAccessException, IOException {
		Measurement ts = context.getEntityManager().get().load(Measurement.class, "1");

		MDMEntityResponse document = MDMEntityResponse.build(Measurement.class, ts, context.getEntityManager().get());
		es.index(document);
		Thread.sleep(1000L);
		es.delete(sourceName, "Measurement", "1");
		Thread.sleep(1000L);
		assertThat(search(sourceName, "Model P").getTotalHits().value).isEqualTo(0);
	}

	@Test
	public void docIsIndexed_isFound() throws DataAccessException, InterruptedException, IOException {
		Measurement ts = context.getEntityManager().get().load(Measurement.class, "1");

		MDMEntityResponse document = MDMEntityResponse.build(Measurement.class, ts, context.getEntityManager().get());
		es.index(document);
		Thread.sleep(1000L);
		assertThat(search(sourceName, "Model P").getTotalHits().value).isEqualTo(1);
	}

	@Test(expected = IllegalStateException.class)
	public void indexCreatedTwice_ThrowsError() {
		es.createIndex("someRandomIndex");
		es.createIndex("someRandomIndex");
	}

	@Test
	public void indexDeactivated_NoIndexingDone() {
		es.active = "false";

		es.createIndex("someSource");
		assertThat(es.hasIndex("someSource")).isFalse();
	}

	private SearchHits search(String index, String query) throws IOException {
		String[] includeFields = new String[] { "source", "type", "id" };
		String[] excludeFields = new String[0];

		SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
		sourceBuilder.query(QueryBuilders.simpleQueryStringQuery(query).lenient(true)).fetchSource(includeFields,
				excludeFields);

		SearchRequest searchRequest = new SearchRequest();
		searchRequest.indices(index);
		searchRequest.source(sourceBuilder);

		SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
		return response.getHits();
	}
}
