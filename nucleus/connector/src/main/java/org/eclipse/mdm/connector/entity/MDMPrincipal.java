package org.eclipse.mdm.connector.entity;

import java.security.Principal;

/**
 * This interface representes an MDM principal with basic account information
 * like given name, surname and email address
 * 
 * @author sch
 *
 */
public interface MDMPrincipal extends Principal {

	/**
	 * Returns the given name of this principal.
	 *
	 * @return the given name of this principal.
	 */
	public String getGivenName();

	/**
	 * Returns the surname of this principal.
	 *
	 * @return the surname of this principal.
	 */
	public String getSurname();

	/**
	 * Returns the email address of this principal.
	 *
	 * @return the email address of this principal.
	 */
	public String getEmail();
	
	/**
	 * Returns the telephone number of this principal.
	 *
	 * @return the telephone number of this principal.
	 */
	public String getTelephone();
	
	/**
	 * Returns the department of this principal.
	 *
	 * @return the department of this principal.
	 */
	public String getDepartment();
}
