/**
 * 
 */
package org.eclipse.mdm.connector.boundary;

import java.security.Principal;
import java.util.Collections;
import java.util.Map;

import javax.ejb.Singleton;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.property.GlobalProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author akn
 *
 */
@Singleton
public class GlobalConnectorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalConnectorService.class);

	@Inject
	@GlobalProperty
	private Map<String, String> globalProperties = Collections.emptyMap();

	private ConnectorService connectorService;

	public void initalize() {
		Principal principal = new Principal() {

			@Override
			public String getName() {
				return null;
			}
		};

		connectorService = new ConnectorService(principal, globalProperties);
		connectorService.connect();
	}

	/**
	 * @return the connectorService
	 */
	public ConnectorService getConnectorService() {
		if (connectorService == null) {
			initalize();
		}

		return connectorService;
	}

	public ApplicationContext getContextByName(String sourceName) {
		ApplicationContext appContext = null;

		ConnectorService conService = getConnectorService();

		appContext = conService.getContextByName(sourceName);

		if (!isSessionValid(appContext)) {
			try {
				appContext.close();
			} catch (Exception e) {
				LOGGER.error("Closing invalid context failed!", e);
			}
			appContext = conService.reconnect(sourceName).get();
		}

		return appContext;
	}

	private boolean isSessionValid(ApplicationContext appContext) {
		boolean isSessionValid = true;

		try {
			appContext.getEntityManager().get().load(Project.class, "1");
		} catch (DataAccessException e) {
			// do nothing, because if project with id 1 does not exist, the
			// DataAccessException will be thrown
		} catch (Throwable t) {
			isSessionValid = false;
			LOGGER.trace("Session closed: " + t.getLocalizedMessage());
		}
		return isSessionValid;
	}

}
