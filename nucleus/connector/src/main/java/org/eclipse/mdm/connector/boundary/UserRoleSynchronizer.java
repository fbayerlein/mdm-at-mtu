/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.connector.boundary;

import java.io.File;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.core.SecurityContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Role;
import org.eclipse.mdm.connector.entity.MDMPrincipal;
import org.eclipse.mdm.connector.entity.RoleMetadata;
import org.eclipse.mdm.connector.rolesyncconfig.ObjectFactory;
import org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig;
import org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context;
import org.eclipse.mdm.logging.AuditLoggerFactory;
import org.eclipse.mdm.logging.EcsEventCategory;
import org.eclipse.mdm.logging.EcsEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;

/**
 * @author akn
 *
 */
@Singleton
public class UserRoleSynchronizer {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRoleSynchronizer.class);
	private static final AuditLoggerFactory loggerFactory = new AuditLoggerFactory("user-sync", EcsEventCategory.Api,
			EcsEventCategory.Authentication, EcsEventType.User);

	private static final ObjectMapper MAPPER = new ObjectMapper();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final String DO_NOT_SYNCHRONIZE = "do_not_synchronize";


	@EJB
	private GlobalConnectorService globalConnectorService;

	private RoleSyncConfig roleSyncConfig;

	public UserRoleSynchronizer() {
	}

	@PostConstruct
	public void init() {
		try {

			String roleSyncConfigFile = System.getProperty("org.eclipse.mdm.connector.rolesyncconfig");

			roleSyncConfig = new ObjectFactory().createRoleSyncConfig();

			if (!Strings.isNullOrEmpty(roleSyncConfigFile)) {
				File roleSyncConfigfile = new File(roleSyncConfigFile);
				if (!roleSyncConfigfile.exists()) {
					throw new ConnectorServiceException(
							String.format("Configured rolesync config file '%s' does not exist!", roleSyncConfigFile));
				}

				JAXBContext jaxbContext = JAXBContext.newInstance(RoleSyncConfig.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

				roleSyncConfig = (RoleSyncConfig) jaxbUnmarshaller.unmarshal(roleSyncConfigfile);
			} else {
				LOGGER.warn("No RoleSync config file configured!");
			}

		} catch (JAXBException e) {
			LOGGER.error(e.getLocalizedMessage(), e);
			throw new ConnectorServiceException(e.getLocalizedMessage(), e);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage(), e);
			throw new ConnectorServiceException(e.getLocalizedMessage(), e);
		}

	}

	/**
	 * 
	 * @param userName
	 */
	public void syncUserAndRoles(String sourceName, SecurityContext securityContext) {
		Principal principal = securityContext.getUserPrincipal();
		String userName = principal.getName();
		loggerFactory.start().log((m) -> LOGGER.debug(m,
				String.format("Synchronize User and Roles. sourceName ='%1s, userName = '%2s'", sourceName, userName)));

		if (!Strings.isNullOrEmpty(userName)) {
			ApplicationContext context = globalConnectorService.getContextByName(sourceName);
			syncUserAndRoles(context, principal, securityContext);
		}
	}

	private void syncUserAndRoles(ApplicationContext context, Principal principal, SecurityContext securityContext) {
		EntityManager em = context.getEntityManager().get();
		EntityFactory ef = context.getEntityFactory().get();

		List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> possibleRolesOfContext = getPossibleRoles(
				context.getSourceName());

		User user = syncUser(principal, possibleRolesOfContext, em, ef, securityContext);

		boolean unassignRoles = true;
		for (Context source : this.roleSyncConfig.getContext()) {
			if (context.getSourceName().equals(source.getSourceName())) {
				unassignRoles = Boolean.TRUE.equals(source.isUnassignRoles());
				break;
			}
		}

		if (user != null) {
			syncRoles(user, possibleRolesOfContext, em, ef, securityContext, unassignRoles);
		}

	}

	private List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> getPossibleRoles(
			String sourceName) {

		List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> returnVal = new ArrayList<>();

		for (Context context : roleSyncConfig.getContext()) {
			if (context.getSourceName().equals(sourceName)) {
				returnVal = context.getRole();
				break;
			}
		}

		return returnVal;
	}

	private User syncUser(Principal principal,
			List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> possibleRolesOfContext,
			EntityManager em, EntityFactory ef, SecurityContext securityContext) {

		String userName = principal.getName();
		loggerFactory.start().log((m) -> LOGGER.debug(m, String.format("Sync user '%1s'", userName)));
		User user = getUserByNameIgnoreCase(userName, em);

		if (user == null && isUserInOneOfRoles(userName, possibleRolesOfContext, securityContext)) {
			loggerFactory.start().log((m) -> LOGGER.trace(m, "User is not available. It will be created."));
			boolean sucess = false;
			Transaction t = null;

			try {
				t = em.startTransaction();

				user = ef.createUser(userName, "autogenerated", "autogenerated");
				user.setDescription(MAPPER.writeValueAsString(new RoleMetadata(sdf.format(new Date()))));
				if (principal instanceof MDMPrincipal) {
					user.setGivenName(((MDMPrincipal) principal).getGivenName());
					user.setSurname(((MDMPrincipal) principal).getSurname());
					user.setMail(((MDMPrincipal) principal).getEmail());
					user.setPhone(((MDMPrincipal) principal).getTelephone());
					user.setDepartment(((MDMPrincipal) principal).getDepartment());
				}
				t.create(Collections.singletonList(user));

				loggerFactory.success().log((m) -> LOGGER.debug(m, "User successfully created"));
				sucess = true;
			} catch (Exception e) {
				loggerFactory.error().log((m) -> LOGGER.error(m, e.getLocalizedMessage(), e));
				throw new ConnectorServiceException(e.getLocalizedMessage(), e);
			} finally {
				if (t != null && sucess) {
					t.commit();
				} else if (t != null && !sucess) {
					t.abort();
				}
			}

		} else if (user != null && mustNotSynch(user)) {
			user = null;
			loggerFactory.success()
					.log((m) -> LOGGER.trace(m, "User is already available, but the roles must not be synchronized"));
		} else if (user != null && !mustNotSynch(user)) {
			loggerFactory.success()
					.log((m) -> LOGGER.trace(m, "User is already available. Roles will be synchronized"));
		} else {
			loggerFactory.success()
					.log((m) -> LOGGER.trace(m, "User is not available, but has no configured role of the context"));
		}

		return user;
	}

	private boolean mustNotSynch(User user) {
		String description = "";

		if (user.getDescription() != null) {
			description = user.getDescription();
		}

		return description.contains(DO_NOT_SYNCHRONIZE);
	}

	private User getUserByNameIgnoreCase(String userName, EntityManager em) {

		User returnVal = null;

		for (User user : em.loadAll(User.class)) {
			if (user.getName().equalsIgnoreCase(userName)) {
				returnVal = user;
				break;
			}
		}

		return returnVal;
	}

	private boolean isUserInOneOfRoles(String userName,
			List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> possibleRolesOfContext,
			SecurityContext securityContext) {

		for (org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role role : possibleRolesOfContext) {
			if (securityContext.isUserInRole(role.getName())) {
				return true;
			}
		}

		return false;
	}

	private void syncRoles(User user,
			List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> possibleRolesOfContext,
			EntityManager em, EntityFactory ef, SecurityContext securityContext, Boolean unassignRoles) {
		loggerFactory.start().log((m) -> LOGGER.trace(m, "Synchronizing roles"));
		boolean sucess = false;
		Transaction t = null;
		try {
			t = em.startTransaction();
			Map<String, Role> rolesOfUser = getRolesOfUser(user, em);
			if (Boolean.TRUE.equals(unassignRoles)) {
				handleRemovedRoles(user, rolesOfUser, possibleRolesOfContext, em, ef, t, securityContext);
			}
			handleNewRoles(user, rolesOfUser, possibleRolesOfContext, em, ef, t, securityContext);
			sucess = true;
			loggerFactory.success().log((m) -> LOGGER.trace(m, "Synchronized roles"));
		} catch (Exception e) {
			loggerFactory.error().log((m) -> LOGGER.trace(m, e.getLocalizedMessage(), e));
			throw new ConnectorServiceException(e.getLocalizedMessage(), e);
		} finally {
			if (t != null && sucess) {
				t.commit();
			} else if (t != null && !sucess) {
				t.abort();
			}
		}

	}

	private Map<String, Role> getRolesOfUser(User user, EntityManager em) {
		Map<String, Role> returnVal = new HashMap<>();

		em.loadRelatedEntities(user, "groups2users", Role.class).forEach(r -> returnVal.put(r.getID(), r));

		return returnVal;
	}

	/**
	 * Removes an user from a role, if it is not assigned at right now
	 * 
	 * @param user                   {@link User}to check
	 * @param rolesOfUser            List of {@link User}, which are assigned in MDM
	 *                               with the user
	 * @param possibleRolesOfContext
	 * @param em                     {@link EntityManager}
	 * @param ef                     {@link EntityFactory}
	 * @param transaction            an open transaction for updating the
	 *                               {@link Role} list
	 * @param securityContext        the {@link SecurityContext}
	 */
	private void handleRemovedRoles(User user, Map<String, Role> rolesOfUser,
			List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> possibleRolesOfContext,
			EntityManager em, EntityFactory ef, Transaction transaction, SecurityContext securityContext) {

		if (!rolesOfUser.isEmpty()) {
			List<Role> updatedRoles = new ArrayList<>();
			for (Role role : rolesOfUser.values()) {

				String roleNameIP = getRoleNameOfIdentityProvider(possibleRolesOfContext, role.getName());
				if (!securityContext.isUserInRole(roleNameIP)) {
					LOGGER.trace(String.format("Remove role '%1s' from user %2s'", role.getName(), user.getName()));
					user.removeRole(role);
					updatedRoles.add(role);
				}
			}

			if (!updatedRoles.isEmpty()) {
				transaction.update(Collections.singletonList(user));
			}

		}
	}

	private String getRoleNameOfIdentityProvider(
			List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> possibleRolesOfContext,
			String mdmRoleName) {

		String returnValue = mdmRoleName;

		for (org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role ipRole : possibleRolesOfContext) {
			String posMDMName = ipRole.getTargetRole();

			if (posMDMName == null) {
				posMDMName = ipRole.getName();
			}

			if (posMDMName.equals(mdmRoleName)) {
				returnValue = ipRole.getName();
				break;
			}
		}

		return returnValue;
	}

	/**
	 * Creating new roles,. Assign user the mdm role, if not already assigned
	 * 
	 * @param user            {@link User}to check
	 * @param rolesOfUser     List of {@link User}, which are assigned in MDM with
	 *                        the user
	 * @param possibleRoles   List of possible
	 *                        {@link org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role}s
	 * @param em              {@link EntityManager}
	 * @param ef              {@link EntityFactory}
	 * @param transaction     an open transaction for updating/creating the
	 *                        {@link Role} list
	 * @param securityContext the {@link SecurityContext}
	 */
	private void handleNewRoles(User user, Map<String, Role> rolesOfUser,
			List<org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role> possibleRoles, EntityManager em,
			EntityFactory ef, Transaction t, SecurityContext securityContext) {
		for (org.eclipse.mdm.connector.rolesyncconfig.RoleSyncConfig.Context.Role posRole : possibleRoles) {
			String roleName = posRole.getName();

			if (securityContext.isUserInRole(roleName)) {

				String targetRole = posRole.getTargetRole();

				if (Strings.isNullOrEmpty(targetRole)) {
					targetRole = roleName;
				}

				List<Role> roles = em.loadAll(Role.class, targetRole);

				Role role = null;

				if (roles.isEmpty()) {
					LOGGER.trace(String.format("Create role '%1s' and add user '%2s'", targetRole, user.getName()));
					role = ef.createRole(targetRole);
					role.setDescription("autogenerated");
					role.addUser(user);
					t.create(Collections.singletonList(role));

				} else {
					role = roles.get(0);

					if (rolesOfUser.get(role.getID()) == null) {
						LOGGER.trace(String.format("Add role '%1s' to user '%2s'", targetRole, user.getName()));
						user.addRole(role);
						t.update(Collections.singletonList(user));
					} else {
						LOGGER.trace(String.format("User '%1s' already has role '%2s'", user.getName(), targetRole));
					}

				}

			}
		}

	}

}
