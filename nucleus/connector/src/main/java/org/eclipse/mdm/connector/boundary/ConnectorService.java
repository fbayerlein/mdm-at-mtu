/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.connector.boundary;

import java.io.Serializable;
import java.security.Principal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.security.auth.spi.LoginModule;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.text.StringSubstitutor;
import org.apache.commons.text.lookup.StringLookup;
import org.apache.commons.text.lookup.StringLookupFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.notification.NotificationFilter;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.ApplicationContextFactory;
import org.eclipse.mdm.connector.control.ServiceConfigurationActivity;
import org.eclipse.mdm.connector.entity.ServiceConfiguration;
import org.eclipse.mdm.logging.AuditLoggerFactory;
import org.eclipse.mdm.logging.EcsEventCategory;
import org.eclipse.mdm.property.GlobalProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

/**
 * ConnectorServcie Bean implementation to create and close connections
 *
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 * @author Canoo Engineering (removal of hardcoded ODS dependencies)
 *
 */
@SessionScoped
public class ConnectorService implements Serializable {

	private static final long serialVersionUID = -5891142709182298531L;

	private static final Logger LOG = LoggerFactory.getLogger(ConnectorService.class);
	
	private static final String CONNECTION_PARAM_FOR_USER = "for_user";
	private static final String USER_ROLE_SYNC_PROPERTY = "userrolesync.active";

	@Inject
	Principal principal;

	@Context
	private SecurityContext securityContext;

	@Inject
	ServiceConfigurationActivity serviceConfigurationActivity;

	@Inject
	@GlobalProperty
	private Map<String, String> globalProperties = Collections.emptyMap();

	private static class ContextInfo {
		private ApplicationContext context;
		private ServiceConfiguration serviceConfiguration;

		public ContextInfo(ApplicationContext context, ServiceConfiguration serviceConfiguration) {
			if (context == null) {
				throw new IllegalArgumentException("Context cannot be null.");
			}
			if (serviceConfiguration == null) {
				throw new IllegalArgumentException("ServiceConfiguration cannot be null.");
			}
			this.context = context;
			this.serviceConfiguration = serviceConfiguration;
		}

		public ApplicationContext getContext() {
			return context;
		}

		public ServiceConfiguration getServiceConfiguration() {
			return serviceConfiguration;
		}
	}

	private Map<String, ContextInfo> contexts = new ConcurrentHashMap<>();

	private transient StringLookup stringLookup = StringLookupFactory.INSTANCE.interpolatorStringLookup();
	private transient StringSubstitutor substitutor = new StringSubstitutor(stringLookup);

	@EJB
	private UserRoleSynchronizer userRoleSynchronizer;

	public ConnectorService() {
		// empty constructor for CDI
	}

	/**
	 * Creates a connector service for usage outside the session scope of CDI.
	 * 
	 * @param principal        Principal the connector service uses.
	 * @param globalProperties global properties supplied the opened application
	 *                         contexts.
	 */
	public ConnectorService(Principal principal, Map<String, String> globalProperties) {
		super();

		this.principal = principal;
		this.serviceConfigurationActivity = new ServiceConfigurationActivity();
		this.globalProperties = globalProperties;
	}

	/**
	 * Read the service configurations after bean construction
	 */
	@PostConstruct
	public void init() {
		LOG.info("Initialized connector service for user with name '" + principal.getName() + "'.");
	}

	/**
	 * disconnect from all connected data sources This method is call from a
	 * {@link LoginModule} at logout
	 *
	 * This method is call from a {@link LoginModule}
	 *
	 */
	@PreDestroy
	public void disconnect() {
		AuditLoggerFactory loggerFactory = new AuditLoggerFactory("logout", EcsEventCategory.Api,
				EcsEventCategory.Authentication);
		loggerFactory.start().log((m) -> LOG.info(m, "Disconecting user with name '" + principal.getName()));
		for (ApplicationContext context : getContexts()) {
			disconnectContext(context);
		}
		loggerFactory.success()
				.log((m) -> LOG.info("User with name '" + principal.getName() + "' has been disconnected!"));
	}

	/**
	 * @return list with names of configured source names.
	 */
	public List<String> getSourceNames() {
		return serviceConfigurationActivity.readServiceConfigurations().stream().map(ServiceConfiguration::getName)
				.collect(Collectors.toList());
	}

	/**
	 * Disconnects all currently connected source and connects to the given list of
	 * sources.
	 * 
	 * @param sourceNames list of source names to connect to.
	 */
	public void setActiveSourceNames(List<String> sourceNames) {
		this.disconnect();
		serviceConfigurationActivity.readServiceConfigurations().stream()
				.filter(val -> sourceNames.contains(val.getName())).forEach(this::connectContexts);
	}

	/**
	 * returns all available {@link ApplicationContext}s
	 *
	 * @return list of available {@link ApplicationContext}s
	 */
	public List<ApplicationContext> getContexts() {
		return ImmutableList
				.copyOf(contexts.values().stream().map(ContextInfo::getContext).toArray(ApplicationContext[]::new));
	}

	/**
	 * returns an {@link ApplicationContext} identified by the given name
	 *
	 * @param name source name (e.g. MDM {@link Environment} name)
	 * @return the matching {@link ApplicationContext}
	 */
	public ApplicationContext getContextByName(String name) {
		try {
			ContextInfo contextInfo = contexts.get(name);

			if (contextInfo == null) {
				// try to open context on the fly
				ServiceConfiguration s = serviceConfigurationActivity.readServiceConfigurations().stream()
						.filter(val -> name.contains(val.getName())).findFirst()
						.orElseThrow(() -> new ConnectorServiceException(
								"no data source with service name '" + name + "' configured!"));

				return this.connectContexts(s).orElseThrow(
						() -> new ConnectorServiceException("Could not connect to service name '" + name + "'."));
			}

			return contextInfo.getContext();
		} catch (DataAccessException e) {
			throw new ConnectorServiceException(e.getMessage(), e);
		}
	}

	/**
	 * Connects all configured application contexts
	 */
	public void connect() {
		setActiveSourceNames(getSourceNames());
	}

	/**
	 * Tries to close the given context and reconnect.
	 * 
	 * @param context ApplicationContext to reconnect
	 * @return the reconnected context
	 */
	public Optional<ApplicationContext> reconnect(ApplicationContext context) {
		Map<String, NotificationFilter> notificationRegistrations = new HashMap<>();
		try {
			if (context.getNotificationService().isPresent()) {
				notificationRegistrations = context.getNotificationService().get().getRegistrations();
			}
		} catch (Exception e) {
			LOG.debug("Unable to get notification registrations!", e);
		}

		try {
			context.close();
		} catch (Exception e) {
			LOG.warn("Unable to close ApplicationContext!", e);
		}

		removeContextFromMap(context);

		ContextInfo contextInfo = contexts.get(context.getSourceName());
		if (contextInfo == null) {
			LOG.warn("Could not reconnect ApplicationContext, because service configuration is null!");
			return Optional.empty();
		} else {
			return connectContexts(contextInfo.getServiceConfiguration(), notificationRegistrations);
		}
	}

	/**
	 * Tries to close the given context and reconnect.
	 * 
	 * @param context ApplicationContext to reconnect
	 * @return the reconnected context
	 */
	public Optional<ApplicationContext> reconnect(String name) {
		LOG.info("Reconnecting ApplicationContext '{}'", name);
		ContextInfo contextInfo = contexts.get(name);

		Map<String, NotificationFilter> notificationRegistrations = new HashMap<>();

		if (contextInfo == null) {
			LOG.debug("ContextInfo null. No need to invoke close.");
		} else {
			ApplicationContext context = contextInfo.getContext();
			LOG.debug("Closing context: {}", context);

			try {
				if (context.getNotificationService().isPresent()) {
					notificationRegistrations = context.getNotificationService().get().getRegistrations();
				}
			} catch (Exception e) {
				LOG.debug("Unable to get notification registrations!", e);
			}

			try {
				context.close();
				LOG.debug("ApplicationContext closed: {}", context);
			} catch (Exception e) {
				LOG.debug("Unable to close ApplicationContext!", e);
			}
		}

		if (contexts.remove(name) != null) {
			LOG.debug("Context '{}' removed from contexts map", name);
		}

		if (contextInfo == null) {
			LOG.warn("Could not reconnect ApplicationContext, because service configuration is null!");
			return Optional.empty();
		} else {
			return connectContexts(contextInfo.getServiceConfiguration(), notificationRegistrations);
		}
	}

	/**
	 * Connects an {@link ApplicationContext} with the given
	 * {@link ServiceConfiguration}
	 * 
	 * @param source {@link ServiceConfiguration} used to open the
	 *               {@link ApplicationContext}
	 * @return Optional containing the ApplicationContext or empty Optional if
	 *         ApplicationContext could not be opened.
	 */
	private Optional<ApplicationContext> connectContexts(ServiceConfiguration source) {
		return connectContexts(source, Collections.emptyMap());
	}

	/**
	 * Connects an {@link ApplicationContext} with the given
	 * {@link ServiceConfiguration}
	 * 
	 * @param source                    {@link ServiceConfiguration} used to open
	 *                                  the {@link ApplicationContext}
	 * @param notificationRegistrations
	 * @return Optional containing the ApplicationContext or empty Optional if
	 *         ApplicationContext could not be opened.
	 */
	private Optional<ApplicationContext> connectContexts(ServiceConfiguration source,
			Map<String, NotificationFilter> notificationRegistrations) {
		AuditLoggerFactory loggerFactory = new AuditLoggerFactory("login", EcsEventCategory.Api,
				EcsEventCategory.Authentication);
		try {

			loggerFactory.start().log((m) -> LOG.info(m, "Logging in."));

			/**
			 * Can be null, e.g. at the shared superuser context. In this case no user sync
			 */
			if (userRoleSynchronizer != null
					&& Boolean.valueOf(globalProperties.getOrDefault(USER_ROLE_SYNC_PROPERTY, "false"))) {
				userRoleSynchronizer.syncUserAndRoles(source.getName(), securityContext);
			}

			Class<? extends ApplicationContextFactory> contextFactoryClass = Thread.currentThread()
					.getContextClassLoader().loadClass(source.getContextFactoryClass())
					.asSubclass(ApplicationContextFactory.class);
			ApplicationContextFactory contextFactory = contextFactoryClass.newInstance();

			Map<String, String> connectionParameters = new HashMap<>();
			connectionParameters.putAll(globalProperties);
			connectionParameters.putAll(processLookups(source.getConnectionParameters()));
			connectionParameters.put(CONNECTION_PARAM_FOR_USER, principal.getName());

			ApplicationContext context = contextFactory.connect(source.getName(), connectionParameters);

			contexts.put(context.getSourceName(), new ContextInfo(context, source));

			loggerFactory.success().log((m) -> LOG.info("Logged in."));
			return Optional.of(context);

		} catch (ConnectionException e) {

			String msg = "unable to logon user with name '" + principal.getName() + "' at data source '"
					+ source.getContextFactoryClass() + "' (reason: " + e.getMessage() + ")";
			loggerFactory.error().log((m) -> LOG.error(msg, e));
		} catch (Exception e) {
			String msg = "failed to initialize entity manager using factory '" + source.getContextFactoryClass()
			+ "' (reason: " + e + ")";
			loggerFactory.error().log((m) -> LOG.error(msg, e));
		}
		return Optional.empty();
	}

	/**
	 * Processes lookups on the values of the map
	 * 
	 * @param map
	 * @return map with applied lookups
	 */
	protected Map<String, String> processLookups(Map<String, String> map) {
		return map.entrySet().stream().collect(Collectors.toMap(Entry::getKey, e -> substitutor.replace(e.getValue())));
	}

	/**
	 * Disconnects the given context.
	 * 
	 * @param context context to disconnect
	 */
	private void disconnectContext(ApplicationContext context) {
		try {
			if (context != null) {
				context.close();
			}
		} catch (ConnectionException e) {
			new AuditLoggerFactory("logout", EcsEventCategory.Api,
					EcsEventCategory.Authentication).error().log((m) -> LOG.error(m,
					"Unable to logout user from MDM datasource (reason: " + e.getMessage() + ")", e));
			throw new ConnectorServiceException(e.getMessage(), e);
		} finally {
			removeContextFromMap(context);
		}
	}

	/**
	 * Removed the given context
	 * 
	 * @param context {@link ApplicationContext} to remove
	 */
	private void removeContextFromMap(ApplicationContext context) {
		Iterator<Map.Entry<String, ContextInfo>> it = contexts.entrySet().iterator();
		while (it.hasNext()) {
			if (it.next().getValue().getContext() == context) {
				it.remove();
			}
		}
	}
}
