/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.connector.boundary;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.security.Principal;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import javax.ejb.SessionContext;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.ApplicationContextFactory;
import org.eclipse.mdm.connector.control.ServiceConfigurationActivity;
import org.eclipse.mdm.connector.entity.ServiceConfiguration;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableMap;

/**
 * JUNIT Test for {@link ConnectorService}
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 * @author Canoo Engineering (more tests)
 *
 */
class ConnectorServiceTest {

	private final Principal testUser = new SimplePrincipal("testUser");
	private static final String TEST_SOURCE_NAME = "testSource";
	private final String differentSourceName = "differentSource";

	@Test
	void testGetEntityManagers_happyFlow() throws Exception {
		ConnectorService connectorService = createConnectorService(testUser);
		connectorService.connect();

		assertThat(connectorService.getContexts()).hasSize(1);
	}

	@Test
	void testGetEntityManagerByName_differentSourceName() throws Exception {
		ConnectorService connectorService = createConnectorService(testUser);
		connectorService.connect();
		assertThatThrownBy(() -> connectorService.getContextByName(differentSourceName))
				.isInstanceOf(ConnectorServiceException.class);
	}

	@Test
	void testGetEntityManagerByName_happyFlow() throws Exception {
		ConnectorService connectorService = createConnectorService(testUser);
		connectorService.connect();
		assertThat(connectorService.getContextByName(TEST_SOURCE_NAME)).isNotNull();
	}

	@Test
	void testDisconnect() throws Exception {
		ConnectorService connectorService = createConnectorService(testUser);
		assertThatCode(() -> connectorService.disconnect()).doesNotThrowAnyException();
	}

	private static final class SimplePrincipal implements Principal {
		private final String name;

		SimplePrincipal(String name) {
			this.name = Objects.requireNonNull(name);
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public boolean equals(Object obj) {
			return (obj instanceof SimplePrincipal && ((SimplePrincipal) obj).name.equals(name));
		}

		@Override
		public int hashCode() {
			return name.hashCode();
		}

		@Override
		public String toString() {
			return name;
		}

	}

	private static ConnectorService createConnectorService(Principal user) throws Exception {

		SessionContext sessionContextMock = mock(SessionContext.class);
		when(sessionContextMock.getCallerPrincipal()).thenReturn(user);

		ServiceConfiguration serviceConfiguration = new ServiceConfiguration(TEST_SOURCE_NAME,
				TestContextFactory.class.getName(), Collections.emptyMap());

		ConnectorService connectorService = new ConnectorService();

		connectorService.principal = user;

		ServiceConfigurationActivity serviceConfigurationActivity = mock(ServiceConfigurationActivity.class);
		when(serviceConfigurationActivity.readServiceConfigurations())
				.thenReturn(Collections.singletonList(serviceConfiguration));
		connectorService.serviceConfigurationActivity = serviceConfigurationActivity;

		return connectorService;
	}

	public static final class TestContextFactory implements ApplicationContextFactory {

		@Override
		public ApplicationContext connect(String sourceName, Map<String, String> connectionParameters)
				throws ConnectionException {
			return createContext(sourceName);
		}

		@Override
		public ApplicationContext connect(String sourceName, Map<String, String> connectionParameters,
				ApplicationContext contextDst) throws ConnectionException {
			return createContext(sourceName);
		}
	}

	private static ApplicationContext createContext(String sourceName) {
		ApplicationContext ctx = mock(ApplicationContext.class);
		when(ctx.getSourceName()).thenReturn(sourceName);

		return ctx;
	}

	@Test
	void testProcessLookupsSys() throws Exception {
		ConnectorService connectorService = createConnectorService(testUser);

		System.setProperty("MY_PASSWORD", "s3cr3t");
		assertThat(connectorService.processLookups(ImmutableMap.of("password", "${sys:MY_PASSWORD}")))
				.containsEntry("password", "s3cr3t");
		System.setProperty("MY_PASSWORD", "");
	}

	@Test
	void testProcessLookupsProperties() throws Exception {
		ConnectorService connectorService = createConnectorService(testUser);

		assertThat(connectorService.processLookups(
				ImmutableMap.of("password", "${properties:src/test/resources/lookup.properties::MyPassword}")))
						.containsEntry("password", "s4cr4t");
	}
}
