/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.logging;

import org.slf4j.MDC;

/**
 * Enriches the Mapped Diagnostic Context (MDC) with fields matching the Elastic Common Schema (ECS).
 * Auto cleans MDC after usage.
 * 
 * @author Johannes Stamm, Peak Solution GmbH
 * 
 */
public class EcsMdcContext implements AutoCloseable {
	
	public EcsMdcContext(AuditLogger context) {
		MDC.put(EcsFields.EVENT_ACTION, context.getAction());
		MDC.put(EcsFields.EVENT_KIND, context.getKind());
		MDC.put(EcsFields.EVENT_CATEGORY, context.getCategory());
		MDC.put(EcsFields.EVENT_TYPE, context.getType());
		MDC.put(EcsFields.EVENT_OUTCOME, context.getOutcome());
	}

	@Override
	public void close() {
		MDC.remove(EcsFields.EVENT_ACTION);
		MDC.remove(EcsFields.EVENT_CATEGORY);
		MDC.remove(EcsFields.EVENT_KIND);
		MDC.remove(EcsFields.EVENT_TYPE);
		MDC.remove(EcsFields.EVENT_OUTCOME);
	}
}
