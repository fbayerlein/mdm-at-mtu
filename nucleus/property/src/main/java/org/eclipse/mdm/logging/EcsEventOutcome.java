/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.logging;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 
 * @author Johannes Stamm, Peak Solution GmbH
 * 
 * @see https://www.elastic.co/guide/en/ecs/current/ecs-event.html
 */
public enum EcsEventOutcome implements EcsEventCategorization {
	
	Failure("failure"),
	Success("success"),
	Unknown("unknown");

	private final String value;
	
	private EcsEventOutcome(final String value) {
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return value;
	}
}
