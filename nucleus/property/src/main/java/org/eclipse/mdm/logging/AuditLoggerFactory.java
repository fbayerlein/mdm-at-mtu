package org.eclipse.mdm.logging;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AuditLoggerFactory {

	private String action;
	private EcsEventKind kind = EcsEventKind.Event;
	private Set<EcsEventCategory> categories;
	private Set<EcsEventType> types;
	private EcsEventOutcome outcome = EcsEventOutcome.Unknown;

	public AuditLoggerFactory(String action, EcsEventCategorization... categorizations) {
		this.action = action;
		if (categorizations != null) {
			Arrays.stream(categorizations).forEach(this::addCategorization);
		}
	}

	public AuditLogger create() {
		return new AuditLogger(action, kind, categories, types, outcome);
	}

	public AuditLogger start() {
		AuditLogger config = new AuditLogger(action, kind, categories, types, outcome);
		config.addType(EcsEventType.Start);
		return config;
	}

	public AuditLogger end() {
		return end(outcome);
	}

	public AuditLogger success() {
		return end(EcsEventOutcome.Success);
	}

	public AuditLogger end(EcsEventOutcome outcome) {
		AuditLogger config = new AuditLogger(action, kind, categories, types, outcome);
		config.addType(EcsEventType.End);
		return config;
	}

	public AuditLogger error() {
		AuditLogger config = new AuditLogger(action, kind, categories, types, outcome);
		config.addType(EcsEventType.End);
		config.addType(EcsEventType.Error);
		return config;
	}

	private void addCategorization(EcsEventCategorization categorization) {
		if (categorization instanceof EcsEventKind) {
			setKind((EcsEventKind) categorization);
		} else if (categorization instanceof EcsEventCategory) {
			addCategory((EcsEventCategory) categorization);
		} else if (categorization instanceof EcsEventType) {
			addType((EcsEventType) categorization);
		} else if (categorization instanceof EcsEventOutcome) {
			setOutcome((EcsEventOutcome) categorization);
		}
	}

	/**
	 * @param category the category to add
	 */
	private void addCategory(EcsEventCategory... category) {
		if (this.categories == null) {
			this.categories = new HashSet<>();
		}
		this.categories.addAll(Arrays.asList(category));
	}

	/**
	 * @param type the type to add
	 */
	private void addType(EcsEventType... type) {
		if (this.types == null) {
			this.types = new HashSet<>();
		}
		this.types.addAll(Arrays.asList(type));
	}

	/**
	 * @param outcome the outcome to set
	 */
	private void setOutcome(EcsEventOutcome outcome) {
		this.outcome = outcome;
	}

	/**
	 * @param kind the kind to set
	 */
	private void setKind(EcsEventKind kind) {
		this.kind = kind;
	}
}
