/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.oidc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.common.base.Splitter;

public class AccessTokenRESTAuthenticatorTest {

	@Test
	void checkAudience() {
		AccessTokenRESTAuthenticator a = new AccessTokenRESTAuthenticator(null, "", "", false,
				Arrays.asList("openMDM"));

		assertThat(a.checkAudience(Arrays.asList("openMDM"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("api", "openMDM"))).isTrue();

		assertThatThrownBy(() -> a.checkAudience(null))
				.hasMessage("Expected one of [openMDM] as audience, but no audiences are provided.");

		assertThatThrownBy(() -> a.checkAudience(Collections.emptyList()))
				.hasMessage("Expected one of [openMDM] as audience, but no audiences are provided.");

		assertThatThrownBy(() -> a.checkAudience(Arrays.asList("api")))
				.hasMessage("Expected one of [openMDM] as audience, but only got [api].");

		// Audience comparison is case sensitive:
		// https://www.rfc-editor.org/rfc/rfc7519#section-4.1.3
		assertThatThrownBy(() -> a.checkAudience(Arrays.asList("openmdm")))
				.hasMessage("Expected one of [openMDM] as audience, but only got [openmdm].");
	}

	@Test
	void checkMultipleAudiences() {
		AccessTokenRESTAuthenticator a = new AccessTokenRESTAuthenticator(null, "", "", false,
				Arrays.asList("openMDM", "xyz"));

		assertThat(a.checkAudience(Arrays.asList("openMDM"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz", "openMDM"))).isTrue();

		assertThatThrownBy(() -> a.checkAudience(null))
				.hasMessage("Expected one of [openMDM, xyz] as audience, but no audiences are provided.");

		assertThatThrownBy(() -> a.checkAudience(Collections.emptyList()))
				.hasMessage("Expected one of [openMDM, xyz] as audience, but no audiences are provided.");

		assertThatThrownBy(() -> a.checkAudience(Arrays.asList("api")))
				.hasMessage("Expected one of [openMDM, xyz] as audience, but only got [api].");

		// Audience comparison is case sensitive:
		// https://www.rfc-editor.org/rfc/rfc7519#section-4.1.3
		assertThatThrownBy(() -> a.checkAudience(Arrays.asList("openmdm")))
				.hasMessage("Expected one of [openMDM, xyz] as audience, but only got [openmdm].");
	}

	@Test
	void checkNoExpectedAudiences() {

		AccessTokenRESTAuthenticator a = new AccessTokenRESTAuthenticator(null, "", "", false, Collections.emptyList());

		assertThat(a.checkAudience(Arrays.asList("openMDM"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz", "openMDM"))).isTrue();
		assertThat(a.checkAudience(null)).isTrue();
		assertThat(a.checkAudience(Collections.emptyList())).isTrue();

		a = new AccessTokenRESTAuthenticator(null, "", "", false, null);

		assertThat(a.checkAudience(Arrays.asList("openMDM"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz", "openMDM"))).isTrue();
		assertThat(a.checkAudience(null)).isTrue();
		assertThat(a.checkAudience(Collections.emptyList())).isTrue();

		List<String> audiences = Splitter.on(",").trimResults().omitEmptyStrings().splitToList("");
		a = new AccessTokenRESTAuthenticator(null, "", "", false, audiences);

		assertThat(a.checkAudience(Arrays.asList("openMDM"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz"))).isTrue();
		assertThat(a.checkAudience(Arrays.asList("xyz", "openMDM"))).isTrue();
		assertThat(a.checkAudience(null)).isTrue();
		assertThat(a.checkAudience(Collections.emptyList())).isTrue();
	}
}
