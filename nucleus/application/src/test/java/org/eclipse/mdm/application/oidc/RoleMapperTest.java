/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.oidc;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableSet;

public class RoleMapperTest {

	@Test
	public void testProvidedExampleMapping() throws IOException {
		RoleMapper mapper = new RoleMapper();
		mapper.initFromStream(RoleMapperTest.class.getResourceAsStream("/role-mappings-test.properties"));

		assertThat(mapper.map("hans", ImmutableSet.of("Administrator"))).containsOnly("Admin");
		assertThat(mapper.map("hans", ImmutableSet.of("User"))).containsOnly("DescriptiveDataAuthor");
		assertThat(mapper.map("hans", ImmutableSet.of("Guest"))).containsOnly("Guest");
	}

	@Test
	public void testMapPrincipal() throws IOException {
		String mapping = "hans=Admin";
		RoleMapper mapper = new RoleMapper();
		mapper.initFromStream(new ByteArrayInputStream(mapping.getBytes()));

		assertThat(mapper.map("hans", ImmutableSet.of("xy"))).containsOnly("Admin", "xy");
	}

	@Test
	public void testMapMultipleRoles() throws IOException {
		String mapping = "RoleA=Mapped1,Mapped2,Mapped3\nRoleC=Mapped4\n";
		RoleMapper mapper = new RoleMapper();
		mapper.initFromStream(new ByteArrayInputStream(mapping.getBytes()));

		assertThat(mapper.map("hans", ImmutableSet.of("RoleA", "RoleB", "RoleC"))).containsOnly("Mapped1", "Mapped2",
				"Mapped3", "RoleB", "Mapped4");
	}

	@Test
	public void testSeperatorInKey() throws IOException {
		String mapping = "CN\\=openMDM_Admin,OU\\=Groups,DC\\=eclipse,DC\\=org=Admin";
		RoleMapper mapper = new RoleMapper();
		mapper.initFromStream(new ByteArrayInputStream(mapping.getBytes()));

		assertThat(mapper.map("hans", ImmutableSet.of("CN=openMDM_Admin,OU=Groups,DC=eclipse,DC=org")))
				.containsOnly("Admin");
	}

	@Test
	public void testSeperatorInValue() throws IOException {
		String mapping = "Administrator=Admin\\=tor";
		RoleMapper mapper = new RoleMapper();
		mapper.initFromStream(new ByteArrayInputStream(mapping.getBytes()));

		assertThat(mapper.map("hans", ImmutableSet.of("Administrator"))).containsOnly("Admin=tor");
	}
}
