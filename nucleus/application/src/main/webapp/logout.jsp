<!--********************************************************************************
 * Copyright (c) 2015-2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************-->

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Please select Login</title>


<link rel='stylesheet' href="${pageContext.request.contextPath}/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login.css">

</head>

<body>
	<div class="bform">
		<div class="bform2">
			<div class="container">

				<h2 class="form-signin-heading">Relogin</h2>

				<p>
					<a href="${pageContext.request.contextPath}/nosso">Username/Password</a>.
					<a href="${pageContext.request.contextPath}/index.html">SingleSignOn</a>.
				</p>

			</div>
		</div>
	</div>

</body>
</html>
