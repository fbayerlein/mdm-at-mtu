package org.eclipse.mdm.application.oidc;

public class InvalidSessionIdException extends RuntimeException {

	private static final long serialVersionUID = 3149100409123802432L;

	public InvalidSessionIdException(String message) {
		super(message);
	}


}
