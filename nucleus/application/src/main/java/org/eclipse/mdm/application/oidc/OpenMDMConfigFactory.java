/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.oidc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.FilterConfig;

import org.pac4j.core.authorization.authorizer.RequireAnyRoleAuthorizer;
import org.pac4j.core.client.Client;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.pac4j.core.config.ConfigFactory;
import org.pac4j.core.context.JEEContextFactory;
import org.pac4j.core.context.session.JEESessionStore;
import org.pac4j.core.credentials.TokenCredentials;
import org.pac4j.core.credentials.UsernamePasswordCredentials;
import org.pac4j.core.credentials.authenticator.LocalCachingAuthenticator;
import org.pac4j.core.engine.DefaultCallbackLogic;
import org.pac4j.core.engine.DefaultLogoutLogic;
import org.pac4j.core.engine.DefaultSecurityLogic;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.core.http.adapter.JEEHttpActionAdapter;
import org.pac4j.core.http.callback.NoParameterCallbackUrlResolver;
import org.pac4j.core.matching.matcher.PathMatcher;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.http.client.direct.DirectBasicAuthClient;
import org.pac4j.http.client.direct.DirectBearerAuthClient;
import org.pac4j.oidc.client.OidcClient;
import org.pac4j.oidc.config.OidcConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;

/**
 * MDM-Implementation of pac4j-{@link ConfigFactory}. Creates OIDC configuration
 * and OIDC clients for user-to-machine and machine-to-machine authentication
 * 
 * @author Silvio Christ, Peak Solution GmbH
 *
 */
public class OpenMDMConfigFactory implements ConfigFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(OpenMDMConfigFactory.class);

	private static final String OIDC_CLIENT = "oidcClient";

	private static final String M2M_CLIENT = "m2mClient";

	private static final String API_TOKEN_CLIENT = "apiTokenClient";

	private static final String SYSPARAM_CLIENTS = System.getProperty("org.eclipse.mdm.oidc.clients",
			OIDC_CLIENT + "," + M2M_CLIENT);

	private static final String SYSPARAM_ROLE_MAPPINGS = System.getProperty("org.eclipse.mdm.oidc.role-mappings");

	private static final String SYSPARAM_DISCOVERY_URI = System.getProperty("org.eclipse.mdm.oidc.discoveryUri");

	private static final String SYSPARAM_SCOPES = System.getProperty("org.eclipse.mdm.oidc.scopes");

	private static final String SYSPARAM_CLIENT_ID = System.getProperty("org.eclipse.mdm.oidc.clientId");

	private static final String SYSPARAM_CLIENT_SECRET = System.getProperty("org.eclipse.mdm.oidc.clientSecret");

	private static final String SYSPARAM_ROLES_CLAIM = System.getProperty("org.eclipse.mdm.oidc.rolesClaim", "groups");

	public static final String SYSPARAM_EXPECTED_AUDIENCE = System.getProperty("org.eclipse.mdm.oidc.expectedAudience",
			"");

	public static final String SYSPARAM_M2M_TOKENINFO_URL = System.getProperty("org.eclipse.mdm.oidc.m2m.tokeninfoUrl");

	public static final String SYSPARAM_M2M_DISCOVERY_URI = System.getProperty("org.eclipse.mdm.oidc.m2m.discoveryUri",
			SYSPARAM_DISCOVERY_URI);

	public static final String SYSPARAM_M2M_CLIENT_ID = System.getProperty("org.eclipse.mdm.oidc.m2m.clientId",
			SYSPARAM_CLIENT_ID);

	public static final String SYSPARAM_M2M_CLIENT_SECRET = System.getProperty("org.eclipse.mdm.oidc.m2m.clientSecret",
			SYSPARAM_CLIENT_SECRET);

	public static final String SYSPARAM_M2M_USE_TOKEN_INFO_ENDPOINT = System
			.getProperty("org.eclipse.mdm.oidc.m2m.useTokenInfoEndpoint");

	private static final String ROLE_MAPPINGS_PROPERTIES = "/WEB-INF/role-mappings.properties";

	public static final String LOGOUT_SITE = "logout_oidc.jsp";

	public static final String CALLBACK_PREFIX = "oidc/callback";

	private static String contextRoot;

	public static Set<String> ROLES = ImmutableSet.of("Admin", "DescriptiveDataAuthor", "Guest");

	private RoleMapper roleMapper;

	@Override
	public Config build(final Object... parameters) {

		if (parameters.length < 1 || !(parameters[0] instanceof FilterConfig)) {
			throw new IllegalArgumentException("Expecting FilterConfig as first parameter!");
		}

		contextRoot = System.getProperty("org.eclipse.mdm.oidc.contextRoot");

		if (contextRoot == null || contextRoot.isEmpty()) {
			throw new IllegalArgumentException("System property for oidcContextRoot is missing.");
		}
		if (!contextRoot.endsWith("/")) {
			contextRoot = contextRoot + "/";
		}

		List<String> clientNames = Splitter.on(",").trimResults().splitToList(SYSPARAM_CLIENTS.toLowerCase());
		if (clientNames.isEmpty()) {
			throw new IllegalArgumentException("System property org.eclipse.mdm.oidc.clients is empty!");
		}
		if (!OIDC_CLIENT.toLowerCase().equalsIgnoreCase(clientNames.get(0))) {
			throw new IllegalArgumentException(
					"System property org.eclipse.mdm.oidc.clients must contain " + OIDC_CLIENT + " as first entry!");
		}

		@SuppressWarnings("rawtypes")
		List<Client> clientsList = new ArrayList<>();
		clientsList.add(buildOidcUserClient());

		if (clientNames.contains(M2M_CLIENT.toLowerCase())) {
			try {
				Client<TokenCredentials> m2mClient = buildM2MClient();
				clientsList.add(m2mClient);
			} catch (MalformedURLException | TechnicalException e) {
				LOGGER.error("DirectBearerAuthClient for machine-to-machine communication could not be built.", e);
			}
		}

		if (clientNames.contains(API_TOKEN_CLIENT.toLowerCase())) {
			try {
				Client<UsernamePasswordCredentials> apiTokenClient = buildApiTokenClient();
				clientsList.add(apiTokenClient);
			} catch (TechnicalException e) {
				LOGGER.error("DirectBasicAuthClient for api token authentication could not be built.", e);
			}
		}

		LOGGER.info("Enabling the following authentication clients: {}",
				clientsList.stream().map(Client::getName).collect(Collectors.joining(",")));

		Clients clients = new Clients(contextRoot + CALLBACK_PREFIX, clientsList);

		final Config config = new Config(clients);
		config.setSecurityLogic(DefaultSecurityLogic.INSTANCE);
		config.setSessionStore(JEESessionStore.INSTANCE);
		config.setHttpActionAdapter(JEEHttpActionAdapter.INSTANCE);
		config.setCallbackLogic(DefaultCallbackLogic.INSTANCE);
		config.setLogoutLogic(DefaultLogoutLogic.INSTANCE);
		config.setWebContextFactory(JEEContextFactory.INSTANCE);

		config.addAuthorizer("mdmAuthorizer", new RequireAnyRoleAuthorizer<UserProfile>(ROLES));

		// Exclude some paths to avoid Idp communication when not needed
		config.addMatcher("mdmExcludes",
				new PathMatcher().excludePaths("/" + LOGOUT_SITE, "/mdm/logout", "/favicon.ico", "/login.css"));

		roleMapper = buildRoleMapper((FilterConfig) parameters[0]);

		return config;
	}

	/**
	 * Builds up an DirectBearer Client with all neccessary OIDC configuration. This
	 * Client, respectively the Authenticator, works with a gives access token and
	 * validates this token against the introspect endpoint of the Identityy
	 * Provider
	 * 
	 * @return the {@link DirectBearerAuthClient}
	 * @throws MalformedURLException when no URL could be constructed from
	 *                               IntrospectionEndpointURI
	 */
	private DirectBearerAuthClient buildM2MClient() throws MalformedURLException {

		final OidcConfiguration oidcConfiguration = new OidcConfiguration();
		oidcConfiguration.setDiscoveryURI(SYSPARAM_M2M_DISCOVERY_URI);
		oidcConfiguration.setClientId(SYSPARAM_M2M_CLIENT_ID);
		oidcConfiguration.setSecret(SYSPARAM_M2M_CLIENT_SECRET);
		URI introspecEndpoint = oidcConfiguration.findProviderMetadata().getIntrospectionEndpointURI();

		DirectBearerAuthClient directBearerAuthClient = null;

		URL url = null;
		if (Boolean.valueOf(SYSPARAM_M2M_USE_TOKEN_INFO_ENDPOINT)) {
			url = new URL(SYSPARAM_M2M_TOKENINFO_URL);
		} else if (introspecEndpoint != null) {
			url = introspecEndpoint.toURL();
		}
		if (url == null) {
			throw new TechnicalException("Neither Introspec nor TokenInfo endpoint available to validate token. "
					+ "Please check your configuration.");
		}

		List<String> audiences = Splitter.on(",").trimResults().omitEmptyStrings()
				.splitToList(SYSPARAM_EXPECTED_AUDIENCE);

		directBearerAuthClient = new DirectBearerAuthClient(
				new AccessTokenRESTAuthenticator(url, SYSPARAM_M2M_CLIENT_ID, SYSPARAM_M2M_CLIENT_SECRET,
						Boolean.valueOf(SYSPARAM_M2M_USE_TOKEN_INFO_ENDPOINT), audiences));

		directBearerAuthClient.setName(M2M_CLIENT);
		return directBearerAuthClient;
	}

	private Client<UsernamePasswordCredentials> buildApiTokenClient() {
		LocalCachingAuthenticator<UsernamePasswordCredentials> authenticator = new LocalCachingAuthenticator<>(
				new ApiTokenAuthenticator(), 10000, 1, TimeUnit.MINUTES);

		DirectBasicAuthClient apiTokenClient = new DirectBasicAuthClient(authenticator);
		apiTokenClient.setName(API_TOKEN_CLIENT);
		return apiTokenClient;
	}

	/**
	 * Builds up an OIDC-User Client with all neccessary OIDC configuration
	 * 
	 * @return the {@link OidcClient}
	 */
	private OidcClient<OidcConfiguration> buildOidcUserClient() {
		final OidcConfiguration oidcConfiguration = new OidcConfiguration();
		oidcConfiguration.setDiscoveryURI(SYSPARAM_DISCOVERY_URI);
		oidcConfiguration.setClientId(SYSPARAM_CLIENT_ID);
		oidcConfiguration.setUseNonce(true);
		oidcConfiguration.setClientAuthenticationMethod(
				new ClientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC.getValue()));
		oidcConfiguration.setSecret(SYSPARAM_CLIENT_SECRET);
		oidcConfiguration.setScope(SYSPARAM_SCOPES);

		oidcConfiguration.setLogoutUrl(contextRoot + LOGOUT_SITE);

		final OidcClient<OidcConfiguration> oidcClient = new OidcClient<OidcConfiguration>(oidcConfiguration);
		oidcClient.setCallbackUrlResolver(new NoParameterCallbackUrlResolver());
		oidcClient.addAuthorizationGenerator((context, profile) -> {
			return mapRoles(profile);
		});
		oidcClient.setName(OIDC_CLIENT);
		return oidcClient;
	}

	/**
	 * Create the RoleMapper. Depending on the value of
	 * {@link #SYSPARAM_ROLE_MAPPINGS}, it either uses the provided file or load
	 * from a default file on the classpath.
	 * 
	 * @return {@link RoleMapper}
	 */
	private RoleMapper buildRoleMapper(FilterConfig filterConfig) {
		RoleMapper roleMapper = new RoleMapper();
		if (Strings.isNullOrEmpty(SYSPARAM_ROLE_MAPPINGS)) {
			InputStream in = filterConfig.getServletContext().getResourceAsStream(ROLE_MAPPINGS_PROPERTIES);

			if (in == null) {
				LOGGER.warn("Could not load resource '{}' from servlet context!", ROLE_MAPPINGS_PROPERTIES);
			} else {
				roleMapper.initFromStream(in);
			}
		} else {
			try {
				roleMapper.initFromStream(new FileInputStream(SYSPARAM_ROLE_MAPPINGS));
			} catch (IOException e) {
				LOGGER.warn("Could not initialize RoleMapper from classpath: " + e.getMessage(), e);
			}

		}
		return roleMapper;
	}

	/**
	 * Assign user roles from attribute defined by SYSPARAM_ROLES_CLAIM to profile
	 * 
	 * @param profile the {@link UserProfile} that is enriched with attributes from
	 *                tokens
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Optional<UserProfile> mapRoles(UserProfile profile) {

		List<String> roles = (List<String>) profile.getAttribute(SYSPARAM_ROLES_CLAIM);
		LOGGER.debug("id_token: " + profile.getAttribute("id_token"));
		LOGGER.debug("access_token: " + profile.getAttribute("access_token"));
		LOGGER.debug("Roles from token {}", roles);

		if (roles == null) {
			profile.addRoles(roleMapper.map(profile.getUsername(), Collections.emptySet()));
		} else {
			profile.addRoles(roleMapper.map(profile.getUsername(), new HashSet<>(roles)));
		}
		return Optional.of(profile);
	}

	public static String getContextRoot() {
		return contextRoot;
	}
}
