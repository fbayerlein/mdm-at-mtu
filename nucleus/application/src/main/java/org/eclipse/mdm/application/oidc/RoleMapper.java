/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.oidc;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;

/**
 * A RoleMapper mapping an principal with its original roles to a set of mapped
 * roles. Principal and original roles are tried to be mapped. If there is no
 * mapping defined the original roles are also returned as mapped roles.
 * 
 */
public class RoleMapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleMapper.class);

	private Properties mappings = new Properties();

	public void initFromStream(InputStream in) {
		try {
			mappings.load(in);
			LOGGER.debug("Loaded role mappings from provided inputstream");
		} catch (IOException e) {
			LOGGER.warn("Could not initialize RoleMapper  provided inputstream", e);
		}

	}

	public Set<String> map(final String principalName, final Set<String> roles) {
		if (mappings.isEmpty()) {
			return roles;
		}

		final Set<String> resolved = new HashSet<>();
		for (String role : roles) {
			if (mappings.containsKey(role)) {
				extractRolesIntoSet(role, resolved);
			} else {
				resolved.add(role);
			}
		}

		if (principalName != null && mappings.containsKey(principalName)) {
			extractRolesIntoSet(principalName, resolved);
		}

		LOGGER.debug("Mapped {} with roles {} to final roles: {}", principalName, roles, resolved);
		return resolved;
	}

	private void extractRolesIntoSet(final String key, final Set<String> roles) {
		String value = mappings.getProperty(key);
		if (!value.isEmpty()) {
			roles.addAll(Splitter.on(",").trimResults().splitToList(value));
		}
	}

}
