/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.oidc;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pac4j.core.context.HttpConstants;
import org.pac4j.core.context.JEEContext;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.TokenCredentials;
import org.pac4j.core.credentials.authenticator.Authenticator;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileHelper;
import org.pac4j.core.profile.definition.CommonProfileDefinition;
import org.pac4j.core.profile.definition.ProfileDefinitionAware;
import org.pac4j.core.profile.jwt.JwtClaims;
import org.pac4j.core.util.CommonHelper;
import org.pac4j.core.util.HttpUtils;
import org.pac4j.http.profile.RestProfile;
import org.pac4j.jwt.profile.JwtGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nimbusds.jose.util.JSONObjectUtils;
import com.nimbusds.jwt.JWTClaimsSet;

/**
 * Authenticates against a REST API. The access token is passed as bearer in
 * Http header. The JSON response will be validated to its claims and put into a
 * user profile.
 *
 * @author Silvio Christ, Peak Solution GmbH
 */
public class AccessTokenRESTAuthenticator extends ProfileDefinitionAware<RestProfile>
		implements Authenticator<TokenCredentials> {

	private static final Logger logger = LoggerFactory.getLogger(AccessTokenRESTAuthenticator.class);

	private URL url;

	private String username;

	private String password;

	private boolean useTokenInfoEndpoint = false;
	private List<String> acceptedAudiences = new ArrayList<>();

	public AccessTokenRESTAuthenticator() {
	}

	public AccessTokenRESTAuthenticator(final URL url, final String username, final String password,
			final boolean useTokenInfoEndpoint, List<String> acceptedAudiences) {
		this.url = url;
		this.username = username;
		this.password = password;
		this.useTokenInfoEndpoint = useTokenInfoEndpoint;
		if (acceptedAudiences != null) {
			this.acceptedAudiences.addAll(acceptedAudiences);
		}
	}

	@Override
	protected void internalInit() {
		CommonHelper.assertNotBlank("url", url.toString());
		defaultProfileDefinition(new CommonProfileDefinition<>(x -> new RestProfile()));
	}

	@Override
	public void validate(final TokenCredentials credentials, final WebContext context) {
		init();

		JEEContext jeeContext = (JEEContext) context;
		String requestedSessionId = jeeContext.getNativeRequest().getRequestedSessionId();

		if (requestedSessionId != null) {
			String currentSessionId = jeeContext.getNativeRequest().getSession().getId();
			if (!currentSessionId.equals(requestedSessionId)) {
				throw new InvalidSessionIdException(
						"Provided JSESSIONID is invalid or does not exist anymore. Use a valid JSESSIONID or remove JSESSIONID from request to create a new session.");
			}
		}

		final String token = credentials.getToken();

		if (CommonHelper.isBlank(token)) {
			logger.info("Empty token");
			return;
		}

		final String body;

		if (useTokenInfoEndpoint) {
			body = callTokenInfoEndpoint(token);
		} else {
			body = callIntrospectionEndpoint(token);
		}

		try {
			JWTClaimsSet claimSet = getClaimsSet(body);

			// if introspection endpoint is used, active must be true. Otherwise it will not
			// contain the client_id used later.
			if (!useTokenInfoEndpoint) {
				boolean active = claimSet.getBooleanClaim("active");

				if (!active) {
					throw new TechnicalException("The JWT must active.");
				}
			}

			createJwtProfile(credentials, claimSet);
		} catch (ParseException e) {
			throw new IllegalStateException(e);
		}
	}

	private JWTClaimsSet getClaimsSet(String body) throws ParseException {
		if (body == null) {
			throw new TechnicalException("TokenInfo/Introspection endpoint return null body.");
		}
		logger.debug("body: {}", body);
		return JWTClaimsSet.parse(JSONObjectUtils.parse(body));
	}

	@SuppressWarnings("unchecked")
	protected void createJwtProfile(final TokenCredentials credentials, final JWTClaimsSet claimSet)
			throws ParseException {

		String subject;

		if (useTokenInfoEndpoint) {
			subject = claimSet.getStringClaim("client_id");
		} else {
			subject = claimSet.getStringClaim("sub");
		}

		if (subject == null) {
			throw new TechnicalException("The JWT must contain a subject");
		}

		checkAudience(claimSet.getAudience());

		final Map<String, Object> attributes = new HashMap<>(claimSet.getClaims());
		attributes.remove(JwtClaims.SUBJECT);

		final List<String> roles = (List<String>) attributes.get(JwtGenerator.INTERNAL_ROLES);
		attributes.remove(JwtGenerator.INTERNAL_ROLES);
		final List<String> permissions = (List<String>) attributes.get(JwtGenerator.INTERNAL_PERMISSIONS);
		attributes.remove(JwtGenerator.INTERNAL_PERMISSIONS);
		final String linkedId = (String) attributes.get(JwtGenerator.INTERNAL_LINKEDID);
		attributes.remove(JwtGenerator.INTERNAL_LINKEDID);

		final CommonProfile profile = ProfileHelper.restoreOrBuildProfile(getProfileDefinition(), subject, attributes,
				null);

		if (roles != null) {
			profile.addRoles(roles);
		}
		// Authenticated technical user gets all roles
		profile.addRoles(OpenMDMConfigFactory.ROLES);

		if (permissions != null) {
			profile.addPermissions(permissions);
		}
		if (linkedId != null) {
			profile.setLinkedId(linkedId);
		}
		credentials.setUserProfile(profile);
	}

	/**
	 * Checks if the audiences provided in the Claim set match atleast on of the
	 * required audiences.
	 * 
	 * @param providedAudiences
	 */
	boolean checkAudience(List<String> providedAudiences) {
		if (acceptedAudiences == null || acceptedAudiences.isEmpty()) {
			logger.trace("No audiences required.");
			return true;
		}

		if (providedAudiences == null || providedAudiences.isEmpty()) {
			throw new TechnicalException(
					"Expected one of " + acceptedAudiences + " as audience, but no audiences are provided.");
		}

		for (String acceptedAudience : acceptedAudiences) {
			if (providedAudiences.contains(acceptedAudience)) {
				logger.trace("Audience matches: " + acceptedAudience);
				return true;
			}
		}

		throw new TechnicalException(
				"Expected one of " + acceptedAudiences + " as audience, but only got " + providedAudiences + ".");
	}

	/**
	 * Returns the body from the REST API, passing the username/pasword auth. To be
	 * overridden using another HTTP client if necessary.
	 *
	 * @param token
	 * @return the response body
	 */
	protected String callIntrospectionEndpoint(final String token) {

		final String basicAuth = Base64.getEncoder()
				.encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));
		final Map<String, String> headers = new HashMap<>();
		headers.put(HttpConstants.AUTHORIZATION_HEADER, HttpConstants.BASIC_HEADER_PREFIX + basicAuth);

		HttpURLConnection connection = null;
		try {
			connection = HttpUtils.openPostConnection(url, headers);
			try (OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream(), "UTF-8")) {
				osw.write("token=" + token);
			}
			;

			int code = connection.getResponseCode();
			if (code == 200) {
				logger.debug("Authentication success for username: {}", username);
				return HttpUtils.readBody(connection);
			} else if (code == 401 || code == 403) {
				logger.info("Authentication failure for username: {} -> {}", username,
						HttpUtils.buildHttpErrorMessage(connection));
				return null;
			} else {
				logger.warn("Unexpected error for username: {} -> {}", username,
						HttpUtils.buildHttpErrorMessage(connection));
				return null;
			}
		} catch (final IOException e) {
			throw new TechnicalException(e);
		} finally {
			HttpUtils.closeConnection(connection);
		}
	}

	/**
	 * Asks rest endpoint for token info. To be overridden using another HTTP client
	 * if necessary.
	 * 
	 * @param the access token
	 * @return the response body with detailed information from token info endpoint.
	 */
	protected String callTokenInfoEndpoint(final String token) {

		final Map<String, String> headers = new HashMap<>();
		headers.put(HttpConstants.AUTHORIZATION_HEADER, HttpConstants.BEARER_HEADER_PREFIX + token);

		HttpURLConnection connection = null;
		try {
			connection = openGetConnection(url, headers);

			int code = connection.getResponseCode();
			if (code == 200) {
				logger.debug("Authentication success for m2m");
				return HttpUtils.readBody(connection);
			} else if (code == 401 || code == 403) {
				logger.info("Authentication failure for m2m: {}", HttpUtils.buildHttpErrorMessage(connection));
				return null;
			} else {
				logger.warn("Unexpected error for m2m: {}", HttpUtils.buildHttpErrorMessage(connection));
				return null;
			}
		} catch (final IOException e) {
			throw new TechnicalException(e);
		} finally {
			HttpUtils.closeConnection(connection);
		}
	}

	protected static HttpURLConnection openGetConnection(final URL url, final Map<String, String> headers)
			throws IOException {
		final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(false);
		connection.setRequestMethod(HttpConstants.HTTP_METHOD.GET.name());
		connection.setConnectTimeout(500);
		connection.setReadTimeout(5000);
		if (headers != null) {
			for (final Map.Entry<String, String> entry : headers.entrySet()) {
				connection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
		return connection;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(final URL url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return CommonHelper.toNiceString(this.getClass(), "url", url);
	}
}
