/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.regex.Pattern;

import com.sun.enterprise.security.auth.login.PasswordLoginModule;
import com.sun.enterprise.security.auth.login.common.LoginException;
import com.sun.enterprise.security.auth.realm.Realm;
import com.sun.enterprise.security.auth.realm.jdbc.JDBCRealm;

@SuppressWarnings("deprecation")
public class TokenJDBCLoginModule extends PasswordLoginModule {

	void initRealm() throws LoginException {
		String realmName = null;
		try {
			realmName = (String) _options.get("realmName");
			_currentRealm = Realm.getInstance(realmName);

		} catch (Exception e) {
			String msg = sm.getString("pwdlm.norealm", realmName);
			_logger.log(Level.SEVERE, msg);
			throw new LoginException(msg);
		}

	}

	/**
	 * Perform JDBC authentication. Delegates to JDBCRealm. Extracts the jdbc
	 * username from the token given in the password.
	 * 
	 * @see com.sun.enterprise.security.ee.auth.login.JDBCLoginModule
	 * @throws LoginException If login fails (JAAS login() behavior).
	 */
	protected void authenticate() throws LoginException {
		initRealm();
		if (!(_currentRealm instanceof JDBCRealm)) {
			throw new LoginException(sm.getString("jdbclm.badrealm"));
		}

		final JDBCRealm jdbcRealm = (JDBCRealm) _currentRealm;

		// A JDBC user must have a name not null and non-empty.
		if ((_username == null) || (_username.length() == 0)) {
			throw new LoginException(sm.getString("jdbclm.nulluser"));
		}
		String password = new String(getPasswordChar());

		String[] groups = jdbcRealm.authenticate(_username + "." + extractUsernameFromToken(password),
				getPasswordChar());

		if (groups == null) { // JAAS behavior
			throw new LoginException(sm.getString("jdbclm.loginfail", _username));
		}

		if (_logger.isLoggable(Level.FINEST)) {
			_logger.finest("JDBC login succeeded for: " + _username + " groups:" + Arrays.toString(groups));
		}

		commitAuthentication(_username, getPasswordChar(), _currentRealm, groups);
	}

	/**
	 * Extracts the token name from an API token. For example, if the token is
	 * 'openmdm.mytoken.a3d4e3b13aabc', this method returns 'mytoken'.
	 * 
	 * @param token an openMDM API token
	 * @return extracts the token name from an API token
	 */
	String extractUsernameFromToken(String token) {
		Pattern separator = Pattern.compile(Pattern.quote("."));

		String[] parts = separator.split(token);

		if (parts.length < 3 || !"openmdm".equals(parts[0])) {
			throw new LoginException("Invalid token!");
		}

		return parts[1];
	}
}
