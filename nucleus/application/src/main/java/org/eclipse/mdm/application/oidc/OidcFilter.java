/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.oidc;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.eclipse.mdm.logging.AuditLoggerFactory;
import org.eclipse.mdm.logging.EcsEventCategory;
import org.pac4j.core.client.Client;
import org.pac4j.core.config.Config;
import org.pac4j.core.config.ConfigBuilder;
import org.pac4j.core.context.JEEContext;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.core.http.adapter.HttpActionAdapter;
import org.pac4j.core.util.Pac4jConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Silvio Christ, Peak Solution GmbH
 *
 */
public class OidcFilter implements Filter {

	private static final Logger LOG = LoggerFactory.getLogger(OidcFilter.class);
	private static final AuditLoggerFactory loggerFactory = new AuditLoggerFactory("oidc-login", EcsEventCategory.Api,
			EcsEventCategory.Authentication);

	protected Config config;

	// normal
	private String authorizers = "mdmAuthorizer";
	private String matchers = "mdmExcludes";
	private Boolean multiProfile;

	// callback
	private Boolean saveInSession;
	private Boolean renewSession;
	private String defaultClient;
	private String defaultUrl;

	// logout
	private String logoutUrlPattern;
	private Boolean localLogout;
	private Boolean destroySession;
	private Boolean centralLogout;

	public void init(final FilterConfig filterConfig) throws ServletException {

		final String configFactoryParam = filterConfig.getInitParameter(Pac4jConstants.CONFIG_FACTORY);
		if (configFactoryParam == null) {
			throw new RuntimeException("ConfigFactory not found!");
		} else {
			config = ConfigBuilder.build(configFactoryParam, filterConfig);
		}
		StringBuilder stringBuilder = new StringBuilder();
		Set<String> matchersList = config.getMatchers().keySet();
		for (String matcher : matchersList) {
			stringBuilder.append(matcher);
			if (matchersList.size() > 1) {
				stringBuilder.append(",");
			}
		}
		this.matchers = stringBuilder.toString();
	}

	@SuppressWarnings("unchecked")
	public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain filterChain)
			throws IOException, ServletException {

		final HttpServletRequest request = (HttpServletRequest) req;
		final HttpServletResponse response = (HttpServletResponse) resp;

		if (request.getUserPrincipal() == null) {
			loggerFactory.start().log((m) -> LOG.info(m, "User principal is null. Logging in."));
		}

		final JEEContext context = new JEEContext(request, response, config.getSessionStore());

		// No caching at relogin to force Idp redirect
		if (request.getParameter("relogin") != null && !request.getParameter("relogin").isEmpty()) {
			context.setResponseHeader("Cache-Control", "no-cache, no-store");
			context.setResponseHeader("Pragma", "no-cache");
		}

		String contextRoot = OpenMDMConfigFactory.getContextRoot();

		@SuppressWarnings("rawtypes")
		final HttpActionAdapter actionAdapter = config.getHttpActionAdapter();

		try {
			if (request.getRequestURI().contains(OpenMDMConfigFactory.CALLBACK_PREFIX)) {
				// Callback
				config.getCallbackLogic().perform(context, config, actionAdapter, defaultUrl, this.saveInSession,
						this.multiProfile, this.renewSession, this.defaultClient);
				loggerFactory.success().log((m) -> LOG.info(m, "Logg in callback"));
			} else if (request.getRequestURI().contains("/oidc/logout")) {
				// Logout
				final WebContext webContext = config.getWebContextFactory().newContext(request, response,
						config.getSessionStore());
				config.getLogoutLogic().perform(webContext, config, actionAdapter,
						contextRoot + OpenMDMConfigFactory.LOGOUT_SITE, this.logoutUrlPattern, this.localLogout,
						this.destroySession, this.centralLogout);
			} else {
				config.getSecurityLogic().perform(context, config, (ctx, profiles, parameters) -> {
					// if no profiles are loaded, pac4j is not concerned with this request
					filterChain.doFilter(
							profiles.isEmpty() ? request : new Pac4JHttpServletRequestWrapper(request, profiles),
							response);
					return null;
				}, actionAdapter, this.config.getClients().getClients().stream().map(Client::getName)
						.collect(Collectors.joining(",")), authorizers, matchers, multiProfile);
			}
		} catch (InvalidSessionIdException e) {
			LOG.debug("Invalid OIDC session id: {}", e.getMessage(), e);
			response.sendError(HttpStatus.SC_UNAUTHORIZED, e.getMessage());
		} catch (TechnicalException e) {
			LOG.warn("Error during OIDC authentication: {}", e.getMessage(), e);
			response.sendError(HttpStatus.SC_UNAUTHORIZED, e.getMessage());
		}
	}

	public void destroy() {
	}
}
