package org.eclipse.mdm.application;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.keycloak.adapters.saml.SamlPrincipal;

/**
 * Wraps an {@link Principal} with given user attributes and returns an
 * {@link MDMSamlPrincipal}
 * 
 * @author sch
 *
 */
public class MDMSamlHTTPServletRequestWrapper extends HttpServletRequestWrapper {

	private MDMSamlPrincipal mdmSamlPrincipal = null;

	private String givenNameAttribute = "";
	private String surnameAttribute = "";
	private String emailAttribute = "";
	private String telephoneAttribute = "";
	private String departmentAttribute = "";

	public MDMSamlHTTPServletRequestWrapper(HttpServletRequest request, String givenNameAttribute,
			String surnameAttribute, String emailAttribute, String telephoneAttribute, String departmentAttribute) {
		super(request);
		this.givenNameAttribute = givenNameAttribute;
		this.surnameAttribute = surnameAttribute;
		this.emailAttribute = emailAttribute;
		this.telephoneAttribute = telephoneAttribute;
		this.departmentAttribute = departmentAttribute;
	}

	@Override
	public Principal getUserPrincipal() {
		if (this.mdmSamlPrincipal == null) {
			this.mdmSamlPrincipal = new MDMSamlPrincipal((SamlPrincipal) super.getUserPrincipal(),
					this.givenNameAttribute, this.surnameAttribute, this.emailAttribute, this.telephoneAttribute,
					this.departmentAttribute);
		}
		return this.mdmSamlPrincipal;
	}
}
