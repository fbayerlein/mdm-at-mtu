/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * NoSSOServlet
 * 
 * This servlet routes calls to "/nosso" to MDM basic context.
 * 
 * @author Silvio Christ, (Peak Solution GmbH)
 *
 */
@WebServlet(name = "NoSSOServlet", urlPatterns = { "/nosso" })
public class NoSSOServlet extends HttpServlet {

	private static final long serialVersionUID = 8078555871708173929L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.sendRedirect(req.getContextPath().replace("nosso", "index.html"));
	}

}
