/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.logging;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import org.eclipse.mdm.logging.EcsEventType;

public class AuditLogging {

	private static List<String> ignoredPatterns = Arrays.asList("/localizations", "/searchattributes", "/mdm/logout");

	boolean isRelevant(ContainerRequestContext requestContext) {
		String path = requestContext.getUriInfo().getPath();
		return !ignoredPatterns.stream().anyMatch(path::endsWith);
	}

	EcsEventType evaluateHttpVerbType(ContainerRequestContext requestContext) {
		switch (requestContext.getMethod()) {
		case "PATCH":
		case "PUT":
			return EcsEventType.Change;
		case "POST":
			return EcsEventType.Creation;
		case "DELETE":
			return EcsEventType.Deletion;
		case "GET":
			return EcsEventType.Access;
		default:
			return EcsEventType.Info;
		}
	}

}
