/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.logout;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.mdm.logging.AuditLoggerFactory;
import org.eclipse.mdm.logging.EcsEventCategory;
import org.eclipse.mdm.property.GlobalPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MDMLogoutServlet
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@WebServlet(name = "MDMLogoutServlet", urlPatterns = { "/mdm/logout" })
public class MDMLogoutServlet extends HttpServlet {

	private static final long serialVersionUID = -2243639870075761399L;
	private static final Logger LOG = LoggerFactory.getLogger(MDMLogoutServlet.class);

	private static final AuditLoggerFactory loggerFactory = new AuditLoggerFactory("logout", EcsEventCategory.Api, EcsEventCategory.Authentication);

	@Inject
	private GlobalPropertyService propertyService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		loggerFactory.start().log((m) -> LOG.info(m, "Logging out."));
		req.logout();
		req.getSession().invalidate();

		String r = propertyService.getGlobalPropertyMap(null).getOrDefault("application.logoutRedirect",
				req.getContextPath());

		loggerFactory.success().log((m) -> LOG.info(m, "Logged out, redirecting to {}", r));
		resp.sendRedirect(r);
	}

}
