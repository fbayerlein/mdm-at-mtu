/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.logging;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.mdm.logging.EcsFields;
import org.slf4j.MDC;

@WebFilter(urlPatterns = { "/*" }, filterName = "MdmMdcInsertingServletFilter")
public class MdmMdcInsertingServletFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		MDC.put(EcsFields.TRACE_ID, "provider?");
		MDC.put(EcsFields.CLIENT_IP, req.getRemoteHost());
		MDC.put(EcsFields.RELATED_IP, "[\"" + req.getRemoteHost() + "\"]");
		Principal principal = req.getUserPrincipal();
		if (principal != null) {
			MDC.put(EcsFields.USER_NAME, principal.getName());
			MDC.put(EcsFields.RELATED_USER, "[\"" + principal.getName() + "\"]");
		}
		try {
			chain.doFilter(req, response);
		} finally {
			MDC.remove(EcsFields.TRACE_ID);
			MDC.remove(EcsFields.CLIENT_IP);
			MDC.remove(EcsFields.USER_NAME);
		}
	}
}
