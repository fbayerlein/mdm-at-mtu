/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apitoken.controller;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Set;

import org.apache.commons.codec.binary.Hex;
import org.eclipse.mdm.apitoken.entity.UserApiToken;

public class TokenGenerator {

	private static final String APITOKEN_PREFIX = "openmdm";
	private static final String ALGORITHM = "SHA-256";
	private static final SecureRandom RAND = new SecureRandom();

	public static GeneratedToken createToken(String tokenName, String username, Set<String> groups) {

		byte[] rnd = new byte[64];
		RAND.nextBytes(rnd);

		String token = APITOKEN_PREFIX + "." + tokenName + "."
				+ Base64.getEncoder().encodeToString(rnd).replaceAll("(\\+|/|=)", "");

		return new GeneratedToken(token, new UserApiToken(tokenName, hashToken(token), username, groups));
	}

	public static String hashToken(String token) {
		try {
			MessageDigest digest = MessageDigest.getInstance(ALGORITHM);

			byte[] hashedToken = digest.digest(token.getBytes(Charset.forName("UTF-8")));

			return new String(Hex.encodeHex(hashedToken));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Could not generated token.", e);
		}
	}

	public static class GeneratedToken {
		private String token;
		private UserApiToken userApiToken;

		public GeneratedToken(String token, UserApiToken userApiToken) {
			super();
			this.token = token;
			this.userApiToken = userApiToken;
		}

		public String getToken() {
			return token;
		}

		public UserApiToken getUserApiToken() {
			return userApiToken;
		}
	}
}
