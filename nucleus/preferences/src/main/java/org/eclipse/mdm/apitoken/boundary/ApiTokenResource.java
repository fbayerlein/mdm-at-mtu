/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apitoken.boundary;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.mdm.apitoken.controller.ApiTokenService;
import org.eclipse.mdm.apitoken.controller.TokenGenerator.GeneratedToken;
import org.eclipse.mdm.apitoken.entity.UserApiToken;
import org.eclipse.mdm.apitoken.entity.UserApiTokenDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "API Token")
@Path("/apitoken")
@Produces(MediaType.APPLICATION_JSON)
public class ApiTokenResource {

	@EJB
	private ApiTokenService apiTokenService;

	@GET
	@Path("/{tokenId}")
	@Operation(summary = "Loads an existing API token by ID.", description = "Loads an existing api token by ID.", responses = {
			@ApiResponse(description = "The API token", content = @Content(schema = @Schema(implementation = UserApiTokenDTO.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	public UserApiTokenDTO getUserApiTokens(
			@Parameter(description = "ID of the API token to load", required = true) @PathParam("tokenId") long tokenId) {
		return convert(this.apiTokenService.getTokenById(tokenId));
	}

	@GET
	@Operation(summary = "Loads all existing API tokens for the current user.", description = "Loads all existing API tokens for the current user.", responses = {
			@ApiResponse(description = "List of API tokens", content = @Content(schema = @Schema(implementation = UserApiTokenDTO.class))) })
	public List<UserApiTokenDTO> getUserApiTokens() {
		return this.apiTokenService.getTokens().stream().map(this::convert).collect(Collectors.toList());
	}

	@POST
	@Operation(summary = "Creates a new API token for the current user.", description = "Creates a new API token for the current user.", responses = {
			@ApiResponse(description = "The create API token with tokenUser and token string.") })
	public UserApiTokenDTO createUserApiToken(UserApiTokenDTO token) {
		return convert(this.apiTokenService.createToken(token.getName()));
	}

	@DELETE
	@Path("/{tokenId}")
	@Operation(summary = "Deletes an exsiting API token for the current user.", description = "Deletes an exsiting API token for the current user.", responses = {
			@ApiResponse(description = "The deleted API token") })
	public UserApiTokenDTO deleteUserApiToken(
			@Parameter(description = "ID of the API token to load", required = true) @PathParam("tokenId") long tokenId) {
		return convert(this.apiTokenService.removeToken(tokenId));
	}

	private UserApiTokenDTO convert(UserApiToken token) {
		return new UserApiTokenDTO(token.getId(), token.getName(), token.getCreated());
	}

	private UserApiTokenDTO convert(GeneratedToken token) {
		return new UserApiTokenDTO(token.getUserApiToken().getId(), token.getUserApiToken().getName(),
				token.getUserApiToken().getCreated(), token.getToken());
	}
}
