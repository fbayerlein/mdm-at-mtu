/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.stateshare.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.google.common.base.Ascii;
import com.google.common.base.MoreObjects;

/**
 * @author mko
 *
 */
@Entity
@Table(name = "STATE")
@Cacheable(false)
public class StateShare {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STATE_ID", unique = true)
	private long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "STATE")
	@Lob
	private String state;

	@Column(name = "CREATED_AT")
	private Date created;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "LAST_ACCESS_AT")
	private Date lastAccess;

	@Column(name = "ACCESS_COUNT")
	private long accessCount;

	@SuppressWarnings("unused")
	private StateShare() {

	}

	public StateShare(String name, String state, String createdBy) {
		super();
		this.name = name;
		this.state = state;
		this.createdBy = createdBy;
		this.created = new Date();
		this.lastAccess = new Date();
		this.accessCount = 0L;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return the lastAccess
	 */
	public Date getLastAccess() {
		return lastAccess;
	}

	/**
	 * @return the accessCount
	 */
	public long getAccessCount() {
		return accessCount;
	}

	public void setLastAccess(Date lastAccess) {
		this.lastAccess = lastAccess;
	}

	public void setAccessCount(long accessCount) {
		this.accessCount = accessCount;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		final StateShare other = (StateShare) obj;
		return Objects.equals(this.id, other.id) && Objects.equals(this.name, other.name)
				&& Objects.equals(this.state, other.state) && Objects.equals(this.lastAccess, other.lastAccess)
				&& Objects.equals(this.created, other.created) && Objects.equals(this.createdBy, other.createdBy)
				&& Objects.equals(this.accessCount, other.accessCount);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, state, lastAccess, created, createdBy, accessCount);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(StateShare.class).add("id", id).add("name", name)
				.add("state", Ascii.truncate(state, 100, "...")).add("lastAccess", lastAccess).add("created", created)
				.add("createdBy", createdBy).add("accessCount", accessCount).toString();
	}

}
