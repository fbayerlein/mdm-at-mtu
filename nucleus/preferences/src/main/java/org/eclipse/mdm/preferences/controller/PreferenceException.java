/*******************************************************************************
 * Copyright (c) 2019, 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available a
 * thttp://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.preferences.controller;

public class PreferenceException extends RuntimeException {

	private static final long serialVersionUID = -4350425959578840884L;

	public PreferenceException(String message) {
		super(message);
	}

	public PreferenceException(String message, Throwable t) {
		super(message, t);
	}
}
