/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apitoken.entity;

import java.util.Date;

public class UserApiTokenDTO {
	private long id;
	private String name;
	private Date created;

	// only filled when token is created
	private String token;

	@SuppressWarnings("unused")
	private UserApiTokenDTO() {
		// constructor for deserialization
	}

	public UserApiTokenDTO(long id, String name, Date created) {
		super();
		this.id = id;
		this.name = name;
		this.created = created;
	}

	public UserApiTokenDTO(long id, String name, Date created, String token) {
		this(id, name, created);
		this.token = token;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return token the token to set
	 */
	public String getToken() {
		return token;
	}
}
