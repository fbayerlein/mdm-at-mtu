/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.stateshare.controller;

import java.time.Instant;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.mdm.preferences.controller.PreferenceException;
import org.eclipse.mdm.stateshare.entity.StateShare;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

@Stateless
public class StateService {

	private static final Logger LOG = LoggerFactory.getLogger(StateService.class);

	@PersistenceContext(unitName = "openMDM")
	private EntityManager em;

	@Context
	public SecurityContext securityContext;

	public StateShare getStateById(long stateId) {
		StateShare state = em.find(StateShare.class, stateId);

		try {
			state.setAccessCount(state.getAccessCount() + 1);
			state.setLastAccess(new Date());
			em.persist(state);
		} catch (Exception e) {
			LOG.warn("Could not update state with ID {}.", stateId);
		}

		return state;
	}

	public StateShare createState(String name, String state) {
		if (Strings.isNullOrEmpty(name)) {
			name = Instant.now().toString();
		}
		StateShare stateShare = new StateShare(name, state, getUsername());
		em.persist(stateShare);
		return stateShare;
	}

	public StateShare removeState(long stateId) {
		StateShare state = em.find(StateShare.class, stateId);

		if (state.getCreatedBy().equalsIgnoreCase(getUsername())) {
			em.remove(state);
			em.flush();
			return state;
		} else {
			throw new PreferenceException("Username does not match. Cannot delete state with id " + stateId + "!");
		}
	}

	private String getUsername() {
		return securityContext.getUserPrincipal().getName();
	}
}
