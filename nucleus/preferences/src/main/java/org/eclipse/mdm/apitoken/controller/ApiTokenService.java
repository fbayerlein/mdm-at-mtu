/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apitoken.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.security.DeclareRoles;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.mdm.apitoken.controller.TokenGenerator.GeneratedToken;
import org.eclipse.mdm.apitoken.entity.UserApiToken;
import org.eclipse.mdm.preferences.controller.PreferenceException;

@Stateless
@DeclareRoles(value = { "Admin", "DescriptiveDataAuthor", "Guest" })
public class ApiTokenService {
	private List<String> availableGroups = Arrays.asList("Admin", "DescriptiveDataAuthor", "Guest");

	@PersistenceContext(unitName = "openMDM")
	private EntityManager em;

	@Context
	public SecurityContext securityContext;

	public UserApiToken getTokenById(long tokenId) {
		return em.find(UserApiToken.class, tokenId);
	}

	public List<UserApiToken> getTokens() {

		return em
				.createQuery("select t from UserApiToken t where LOWER(t.username) = LOWER(:username)",
						UserApiToken.class)
				.setParameter("username", getUsername()).getResultList().stream().collect(Collectors.toList());
	}

	public GeneratedToken createToken(String tokenName) {

		GeneratedToken token = TokenGenerator.createToken(tokenName, getUsername(), getGroups());
		em.persist(token.getUserApiToken());
		return token;
	}

	public UserApiToken removeToken(long id) {
		UserApiToken token = em.find(UserApiToken.class, id);

		if (token.getUsername().equalsIgnoreCase(getUsername())) {
			em.remove(token);
			em.flush();
			return token;
		} else {
			throw new PreferenceException("Username does not match. Cannot delete api token with id " + id + "!");
		}
	}

	private String getUsername() {
		return securityContext.getUserPrincipal().getName();
	}

	private Set<String> getGroups() {
		Set<String> groups = new HashSet<>();

		for (String group : availableGroups) {
			if (securityContext.isUserInRole(group)) {
				groups.add(group);
			}
		}
		return groups;
	}
}
