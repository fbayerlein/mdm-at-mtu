/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.state;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author akn
 *
 */
@Entity
@Table(name = "RUNNING_IMPORTS")
@XmlAccessorType(XmlAccessType.FIELD)
@Cacheable(false)
public class RunningImport {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RUNNINGIMPORT_ID")
	private int runningImportId;

	@Column(name = "DIRECTORY")
	private String directory;

	@Column(name = "HOSTNAME")
	private String hostName;

	@Column(name = "TIMESTAMP")
	private Date timestamp;

	@Column(name = "TARGET_SERVICE")
	private String targetService;

	/**
	 * 
	 */
	public RunningImport() {
		super();
	}

	/**
	 * @return the runningImportId
	 */
	public int getRunningImportId() {
		return runningImportId;
	}

	/**
	 * @param runningImportId the runningImportId to set
	 */
	public void setRunningImportId(int runningImportId) {
		this.runningImportId = runningImportId;
	}

	/**
	 * @return the directory
	 */
	public String getDirectory() {
		return directory;
	}

	/**
	 * @param directory the directory to set
	 */
	public void setDirectory(String directory) {
		this.directory = directory;
	}

	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * @param hostName the hostName to set
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getTargetService() {
		return targetService;
	}

	public void setTargetService(String targetService) {
		this.targetService = targetService;
	}

	@Override
	public String toString() {
		return "RunningImport [runningImportId=" + runningImportId + ", directory=" + directory + ", hostName="
				+ hostName + ", timestamp=" + timestamp + ", targetService=" + targetService + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(directory, hostName, runningImportId, targetService, timestamp);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RunningImport other = (RunningImport) obj;
		return Objects.equals(directory, other.directory) && Objects.equals(hostName, other.hostName)
				&& runningImportId == other.runningImportId && Objects.equals(targetService, other.targetService)
				&& Objects.equals(timestamp, other.timestamp);
	}

}
