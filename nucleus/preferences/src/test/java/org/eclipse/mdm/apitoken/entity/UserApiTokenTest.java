/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apitoken.entity;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

public class UserApiTokenTest {

	private EntityManagerFactory factory;

	@Before
	public void init() {
		factory = Persistence.createEntityManagerFactory("openMDMTest", ImmutableMap
				.of(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_XML, "META-INF/persistence-test.xml"));

		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.createQuery("delete from UserApiToken").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	@After
	public void destroy() {
		factory.close();
	}

	@Test
	public void testPersist() {

		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		UserApiToken t = new UserApiToken("myTokenName", "xyz", "myUsername",
				Sets.newHashSet("Admin", "DescriptiveDataAuthor"));
		em.persist(t);
		em.getTransaction().commit();
	}

	@Test
	public void testLoad() {
		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		UserApiToken t = new UserApiToken("myTokenName", "xyz", "myUsername",
				Sets.newHashSet("Admin", "DescriptiveDataAuthor"));
		em.persist(t);
		em.getTransaction().commit();

		assertThat(em.find(UserApiToken.class, t.getId())).isEqualToIgnoringGivenFields(
				new UserApiToken("myTokenName", "xyz", "myUsername", Sets.newHashSet("Admin", "DescriptiveDataAuthor")),
				"id", "created");

		em.close();
	}

	@Test
	public void testDelete() {
		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		UserApiToken t = new UserApiToken("myTokenName", "xyz", "myUsername",
				Sets.newHashSet("Admin", "DescriptiveDataAuthor"));
		em.persist(t);
		em.getTransaction().commit();

		UserApiToken loaded = em.find(UserApiToken.class, t.getId());

		assertThat(loaded).isEqualToIgnoringGivenFields(
				new UserApiToken("myTokenName", "xyz", "myUsername", Sets.newHashSet("Admin", "DescriptiveDataAuthor")),
				"id", "created");

		em.getTransaction().begin();
		em.remove(loaded);
		em.getTransaction().commit();

		assertThat(em.find(UserApiToken.class, t.getId())).isNull();

		assertThat(em.createQuery("select t from UserApiToken t", UserApiToken.class).getResultList()).hasSize(0);

		em.close();
	}

	@Test
	public void testQuery() {
		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		UserApiToken t = new UserApiToken("myTokenName", "xyz", "myUsername",
				Sets.newHashSet("Admin", "DescriptiveDataAuthor"));
		em.persist(t);
		em.getTransaction().commit();

		TypedQuery<UserApiToken> q = em.createQuery("select t from UserApiToken t", UserApiToken.class);
		assertThat(q.getResultList()).hasSize(1).usingElementComparatorIgnoringFields("id", "created")
				.contains(new UserApiToken("myTokenName", "xyz", "myUsername",
						Sets.newHashSet("Admin", "DescriptiveDataAuthor")));

		em.close();
	}
}
