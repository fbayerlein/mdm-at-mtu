/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.state;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.eclipse.mdm.stateshare.entity.StateShare;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

public class StateShareTest {

	private EntityManagerFactory factory;

	@Before
	public void init() {
		factory = Persistence.createEntityManagerFactory("openMDMTest", ImmutableMap
				.of(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_XML, "META-INF/persistence-test.xml"));

		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.createQuery("delete from StateShare").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	@After
	public void destroy() {
		factory.close();
	}

	@Test
	public void testPersist() {
		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		StateShare t = new StateShare(LocalDateTime.now().toString(), "{\"myState\": true}", "myUsername");
		em.persist(t);
		em.getTransaction().commit();
	}

	@Test
	public void testLoad() {
		EntityManager em = factory.createEntityManager();

		String name = LocalDateTime.now().toString();

		em.getTransaction().begin();
		StateShare t = new StateShare(name, "xyz", "myUsername");
		em.persist(t);
		em.getTransaction().commit();

		assertThat(em.find(StateShare.class, t.getId()))
				.isEqualToIgnoringGivenFields(new StateShare(name, "xyz", "myUsername"), "id", "created", "lastAccess");

		em.close();
	}

	@Test
	public void testDelete() {
		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		StateShare t = new StateShare("myName", "xyz", "myUsername");
		em.persist(t);
		em.getTransaction().commit();

		StateShare loaded = em.find(StateShare.class, t.getId());

		assertThat(loaded).isEqualToIgnoringGivenFields(new StateShare("myName", "xyz", "myUsername"), "id", "created",
				"lastAccess");

		em.getTransaction().begin();
		em.remove(loaded);
		em.getTransaction().commit();

		assertThat(em.find(StateShare.class, t.getId())).isNull();

		assertThat(em.createQuery("select t from StateShare t", StateShare.class).getResultList()).hasSize(0);

		em.close();
	}

	@Test
	public void testQuery() {
		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		StateShare t = new StateShare("myName", "xyz", "myUsername");
		em.persist(t);
		em.getTransaction().commit();

		TypedQuery<StateShare> q = em.createQuery("select t from StateShare t", StateShare.class);
		assertThat(q.getResultList()).hasSize(1).usingElementComparatorIgnoringFields("id", "created", "lastAccess")
				.contains(new StateShare("myName", "xyz", "myUsername"));

		em.close();
	}
}
