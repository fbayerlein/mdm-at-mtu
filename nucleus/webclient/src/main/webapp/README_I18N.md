# General

The [ngx-translate library](https://github.com/ngx-translate/core) is used for internationalization. 
This library basically provides functionality for replacing keys (as placeholders for the text to be translated)
in HTML or TypeScript files with values (the actual translations) defined as key-value pairs in separate JSON files (one for each language to be supported). The JSON files (in UTF8 character encoding) should be placed in the _src/assets/i18n_ directory. It is suggested the 2-letter ISO language codes be used for the file names, the file name extension "json" is mandatory.

Key/value replacement is performed at runtime of the web application, and it is possible to switch between languages at runtime (see below).

For details, please refer to the ngx-translate documentation at the link above.


## Recommendations

The keys for the texts to be translated should follow the pattern 

`<directory>.<filename>.<descriptive key>`

where _directory_ is the directory within the _app_ folder containing the respective file, _filename_ the root file name (omitting the file name extension and  qualifiers such as "component" or "module"), and _descriptive key_ a name for the key preferably so chosen as to give some indication about its usage or meaning. Key names should be in English, with dashes (-) replacing spaces between words. For clarity, consider using prefixes such as "title" for dialog titles, "btn" for button texts, "lbl" for labels, "tooltip" for tooltip texts, "err" for error messages, etc.  

Examples: 

* `search.mdm-search.btn-apply-changes`
* `details.sensor.err-cannot-load-descriptive-data`

For the sake of uniformity, it is recommended to use the _translate_ pipe in HTML files:

```html
<h4 class="modal-title">{{ 'tableview.editview.title-view-editor' | translate }}</h4>
```

Using the _TranslateService.instant()_ method, a translation for a key can be obtained in the language currently selected at the time of the method call. A typical application scenario for this is a message etc. shown in response to an event triggered by user action (e.g., a button click):

```typescript
saveBasket(e: Event) {
    if (e) {
      e.stopPropagation();
    }
    if (this.baskets.find(f => f.name === this.basketName) != undefined) {
      this.childSaveModal.hide();
      this.overwriteDialogComponent.showOverwriteModal(
        this.translateService.instant('basket.mdm-basket.item-save-shopping-basket')).subscribe(
          needSave => this.saveBasket2(needSave),
          error => {
            this.saveBasket2(false);
            this.notificationService.notifyError(this.translateService.instant('basket.mdm-basket.err-save-shopping-basket'), error);
        });
    } else {
      this.saveBasket2(true);
    }
  }
```

A function _streamTranslate_ is defined in mdm-core.module.ts which accepts a translation key and returns an Observable:

```typescript
export function streamTranslate(translateService: TranslateService, keys: string | Array<string>, params?: any): Observable<any> {
  return translateService.onLangChange.pipe( startWith({}),
      switchMap(() => params ? translateService.get(keys, params) : translateService.get(keys)) );
}
```
By subscribing to that observable, a change of language in the application can be detected and the current translation for the key passed can be obtained. This is especially useful for class variables containing text to be localized, which is then guaranteed to always be in the language currently selected. Example (see below for an explanation of the marker function _TRANSLATE_):

```typescript
import { TranslateService } from '@ngx-translate/core';
import { streamTranslate, TRANSLATE } from '../core/mdm-core.module';

export class MDMNavigatorComponent implements OnInit {

  loadingNode = <TreeNode>{
    label: 'Loading subordinate items...',
    leaf: true,
    icon: 'fa fa-spinner fa-pulse fa-fw'
  };
  
  [...]

  contextMenuItems: MenuItem[] = [
    { label: 'Refresh', icon: 'fa fa-refresh', command: (event) => this.refresh() },
    { label: 'Add to shopping basket', icon: 'fa fa-shopping-cart', command: (event) => this.addSelectionToBasket() }
  ];

  constructor(private translateService: TranslateService) {
  }

  ngOnInit() {
    streamTranslate(this.translateService, TRANSLATE('navigator.mdm-navigator.loading-subordinate-items')).subscribe(
            (msg: string) => this.loadingNode.label = msg);
    streamTranslate(this.translateService, TRANSLATE('navigator.mdm-navigator.add-to-shopping-basket')).subscribe(
            (msg: string) => this.contextMenuItems[0].label = msg);
  }
  
  [...]
}
```
  
Please also note that a suitable default value should be provided for the text if there is the possibility of it being needed before the translation library has been fully initialized. These default texts should be in English.



## Language selection in the web application

The user can select the language for the UI from a list presented in the web application's main menu bar.
The entries for this list are defined in the _languages_ array of the _AppComponent_ class. For each language, a _SelectItem_ must be added to this array (label: the name of the language _in that language itself_, value: the name of the JSON file with the translations, excluding the "json" extension):

```typescript
export class AppComponent implements OnInit {
  [...]
 
  languages = <SelectItem[]> [
    { label: 'English', value: 'en' },
    { label: 'Deutsch', value: 'de' },
    { label: 'Français', value: 'fr' },
  ];
  
  [...]
}
```


# Extracting translation keys from source files

Using the [ngx-translate-extract](https://github.com/biesbjerg/ngx-translate-extract) library, translation keys can be automatically extracted from the source files and collected in a JSON file for use with ngx-translate. Documentation for ngx-translate-extract is available at the link above. 

A script (_extract-translations_) which extracts the keys to two files (_en.json_ and _de.json_ for English and German, respectively) has been defined in _package.json_ and can be invoked on the command line by _npm run extract-translations_. 

```typescript
"extract-translations": "ngx-translate-extract --input ./src --output ./src/assets/i18n/en.json ./src/assets/i18n/de.json --clean --sort --format namespaced-json --marker TRANSLATE"

```

In this script, a marker function (_TRANSLATE_, defined in mdm-core.module.ts) is specified for marking translation keys outside the methods provided by ngx-translate. Whenever a translation key is used in an ngx-translate method (such as _instant()_) or in conjunction with the _translate_ pipe or directive, ngx-translate-extract automatically extracts that key to the JSON file. Keys located elsewhere in a typescript file must, if they are to be included in the JSON file, be marked using a marker function, which should not do anything but return the string passed into it:

```typescript
export function TRANSLATE(str: string) {
  return str;
}
```

At each extraction run, new keys found in the sources are added to the JSON file(s), keys no longer used in the source files removed from the JSON file(s), and all other keys in the JSON file(s) left untouched, together with any already existing translations for these. 

The script can, of course, be modified as needed. Please note that some of the ways to specify the target JSON files mentioned in the ngx-translate-extract documentation appear to be non-functional (e.g., the _{en,de}.json_ notation for creating two files, _en.json_ and _de.json_).









