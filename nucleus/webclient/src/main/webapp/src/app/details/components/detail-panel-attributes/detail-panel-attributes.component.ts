/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input } from "@angular/core";
import { Attribute, Node } from "@navigator/node";

@Component({
  selector: "mdm-detail-panel-attributes",
  templateUrl: "detail-panel-attributes.component.html",
})
export class DetailPanelAttributesComponent {
  @Input() node: Node;
  @Input() displayAttributes: Attribute[];
}
