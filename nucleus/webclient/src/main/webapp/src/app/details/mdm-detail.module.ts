/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { CalendarModule } from "primeng/calendar";
import { ChartModule } from "primeng/chart";
import { CheckboxModule } from "primeng/checkbox";
import { ChipModule } from "primeng/chip";
import { DialogModule } from "primeng/dialog";
import { InputSwitchModule } from "primeng/inputswitch";
import { InputTextModule } from "primeng/inputtext";
import { ListboxModule } from "primeng/listbox";
import { MultiSelectModule } from "primeng/multiselect";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { PanelModule } from "primeng/panel";
import { Table, TableModule } from "primeng/table";
import { TagModule } from "primeng/tag";
import { ToggleButtonModule } from "primeng/togglebutton";
import { ToolbarModule } from "primeng/toolbar";
import { TooltipModule } from "primeng/tooltip";
import { TreeTableModule } from "primeng/treetable";
import { MDMCoreModule } from "../core/mdm-core.module";
import { FileExplorerModule } from "../file-explorer/file-explorer.module";
import { AttributeEditorComponent } from "./components/attribute-editor/attribute-editor.component";
import { AttributeViewerComponent } from "./components/attribute-viewer/attribute-viewer.component";
import { DetailPanelAttributesComponent } from "./components/detail-panel-attributes/detail-panel-attributes.component";
import { DetailPanelComponent } from "./components/detail-panel/detail-panel.component";
import { ExtendCellButtonComponent } from "./components/extend-cell-button/extend-cell-button.component";
import { MDMDescriptiveDataComponent } from "./components/mdm-detail-descriptive-data/mdm-detail-descriptive-data.component";
import { MDMDetailViewComponent } from "./components/mdm-detail-view/mdm-detail-view.component";
import { MDMDetailComponent } from "./components/mdm-detail/mdm-detail.component";
import { ParameterSetComponent } from "./components/parameterset/main/parameterset.component";
import { ParameterSetPanelComponent } from "./components/parameterset/panel/parameterset-panel.component";
import { QuickPlotComponent } from "./components/quick-plot/quick-plot.component";
import { SensorComponent } from "./components/sensor/sensor.component";
import { MDMDetailRoutingModule } from "./mdm-detail-routing.module";
import { AttributeValidatorService } from "./services/attribute-validator.service";
import { ContextService } from "./services/context.service";
import { DetailViewService } from "./services/detail-view.service";
import { ParameterSetService } from "./services/parameterset.service";
import { SystemParameterService } from "./services/systemparameter.service";

@NgModule({
  imports: [
    MDMDetailRoutingModule,
    MDMCoreModule,
    PanelModule,
    TreeTableModule,
    ListboxModule,
    TooltipModule,
    FileExplorerModule,
    OverlayPanelModule,
    TableModule,
    InputTextModule,
    DialogModule,
    ToolbarModule,
    InputSwitchModule,
    CalendarModule,
    ChartModule,
    MultiSelectModule,
    CheckboxModule,
    TagModule,
    ChipModule,
    TranslateModule,
    FormsModule,
    ToggleButtonModule,
  ],
  declarations: [
    MDMDetailComponent,
    ParameterSetComponent,
    ParameterSetPanelComponent,
    MDMDetailViewComponent,
    MDMDescriptiveDataComponent,
    SensorComponent,
    DetailPanelComponent,
    DetailPanelAttributesComponent,
    AttributeEditorComponent,
    AttributeViewerComponent,
    QuickPlotComponent,
    ExtendCellButtonComponent,
  ],
  exports: [MDMDetailComponent, QuickPlotComponent, AttributeEditorComponent, AttributeViewerComponent],
  providers: [DetailViewService, ContextService, DatePipe, ParameterSetService, SystemParameterService, Table, AttributeValidatorService],
})
export class MDMDetailModule {}
