/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { BasketService } from "@basket/basket.service";
import { MDMItem } from "@core/mdm-item";
import { PreferenceService, StatusPreferences } from "@core/preference.service";
import { NavigatorService } from "@navigator/navigator.service";
import { Attribute, getNodeClass, Node } from "@navigator/node";
import { map, Observable } from "rxjs";
import { MDMTagService } from "src/app/mdmtag/mdmtag.service";
import { StatusService } from "src/app/status/status.service";
import { DetailViewService } from "../../services/detail-view.service";

@Component({
  selector: "mdm-detail-panel",
  templateUrl: "./detail-panel.component.html",
  styleUrls: ["./detail-panel.component.css"],
})
export class DetailPanelComponent implements OnInit, OnChanges {
  // node to display
  @Input() node: Node;
  // handles pannel expansion state
  @Input() collapsed: boolean;
  // handles shopping button disabled state
  @Input() shoppable = false;
  // handles shopping button visible state
  @Input() showButton = false;

  // attributes of display node
  displayAttributes: Attribute[] = [];
  status$: Observable<string>;
  mdmTags: Node[];
  statusPreferences: StatusPreferences;

  constructor(
    private detailViewService: DetailViewService,
    private basketService: BasketService,
    private navigatorService: NavigatorService,
    private preferenceService: PreferenceService,
    private mdmTagService: MDMTagService,
    private statusService: StatusService,
  ) {}

  ngOnInit(): void {
    this.mdmTagService.tagUpdated.subscribe(() => {
      this.refreshAttributes(this.node);
    });

    this.loadStatusPreferences();
  }

  // change detection for display node
  ngOnChanges(changes: SimpleChanges): void {
    if (changes["node"] && this.node != undefined) {
      this.refreshAttributes(this.node);
    }
  }

  private loadStatusPreferences(): void {
    this.preferenceService
      .getPreference("details.status")
      .pipe(
        map((data) => {
          const s: StatusPreferences = {};

          data.map((property) => {
            if (property.key === "details.status.format") {
              s.format = JSON.parse(property.value);
            }
          });

          return s;
        }),
      )
      .subscribe((prefs: StatusPreferences) => {
        this.statusPreferences = prefs;
      });
  }

  public getStatusStyle(status: string) {
    let backgroundColor = "#64748B";
    if (this.statusPreferences && this.statusPreferences.format && status in this.statusPreferences.format) {
      const bgColor = this.statusPreferences.format[status];
      if (/^#[0-9A-F]{6}$/i.test(bgColor)) {
        backgroundColor = bgColor;
      }
    }

    const color = backgroundColor.charAt(0) === "#" ? backgroundColor.substring(1, 7) : backgroundColor;
    const r = parseInt(color.substring(0, 2), 16); // hexToR
    const g = parseInt(color.substring(2, 4), 16); // hexToG
    const b = parseInt(color.substring(4, 6), 16); // hexToB
    const foregroundColor = r * 0.299 + g * 0.587 + b * 0.114 > 186 ? "#000000" : "#ffffff";

    return { background: backgroundColor, color: foregroundColor };
  }

  // refresh display attributes
  public refreshAttributes(node: Node) {
    this.mdmTags = [];
    if (node != undefined) {
      this.displayAttributes = JSON.parse(JSON.stringify(this.detailViewService.getAttributesToDisplay(this.node)));
      this.status$ = this.statusService.loadStatus(node);

      this.detailViewService.loadMDMTags(node).subscribe((nodes) => {
        this.mdmTags = nodes;
      });
    }
  }

  // button listener for shopping button
  public add2Basket() {
    const item = this.getMDMItem();
    if (item !== undefined) {
      this.basketService.add(item);
    }
  }

  // provides style class for panel header (+ icon)
  public getClass() {
    const style = this.showButton ? "margin5 " : "";
    return style + getNodeClass(this.node);
  }

  private getMDMItem(): MDMItem {
    return this.node === undefined ? undefined : new MDMItem(this.node.sourceName, this.node.type, this.node.id);
  }

  // button listener for open in tree
  public openInTree() {
    const item = this.getMDMItem();
    if (item !== undefined) {
      this.navigatorService.fireOnOpenInTree([item]);
    }
  }
}
