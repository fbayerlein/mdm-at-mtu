/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { streamTranslate, TRANSLATE } from "@core/mdm-core.module";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { Attribute, Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { EMPTY } from "rxjs";
import { MDMTagService } from "src/app/mdmtag/mdmtag.service";
import { NodeService } from "../../navigator/node.service";

@Injectable()
export class DetailViewService {
  private _contextUrl: string;

  ignoreAttributesPrefs: Preference[] = [];

  msgFaultyPreferenceForAttributesToIgnore: string;

  constructor(
    private nodeService: NodeService,
    private preferenceService: PreferenceService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private mdmTagService: MDMTagService,
  ) {
    this.preferenceService.getPreference("ignoredAttributes").subscribe(
      (prefs) => (this.ignoreAttributesPrefs = this.ignoreAttributesPrefs.concat(prefs)),
      (error) =>
        this.notificationService.notifyWarn(
          this.translateService.instant("details.detail-view.err-cannot-load-preference-for-attributes-to-ignore"),
          error,
        ),
    );

    streamTranslate(this.translateService, TRANSLATE("details.detail-view.err-faulty-preference-for-attributes-to-ignore")).subscribe(
      (msg: string) => (this.msgFaultyPreferenceForAttributesToIgnore = msg),
    );
  }

  getAttributesToDisplay(node: Node) {
    const filterList = this.getFilters(node.sourceName)
      .map((p) => {
        const splitted = p.split(".");
        return { type: splitted[0], attribute: splitted[1] };
      })
      .filter((p) => p.type === node.type || p.type === "*")
      .map((p) => p.attribute);

    const id = new Attribute();
    id.name = "Id";
    id.dataType = "DT_STRING";
    id.value = node.id;

    return this.getFilteredAttributes(
      [...node.attributes, id].sort((a, b) => a.name.localeCompare(b.name)),
      filterList,
    );
  }

  getFilters(source: string): string[] {
    if (this.ignoreAttributesPrefs.length > 0) {
      return this.ignoreAttributesPrefs
        .filter((p) => p.scope !== Scope.SOURCE || p.source === source)
        .sort(Preference.sortByScope)
        .map((p) => this.parsePreference(p))
        .reduce((acc, value) => acc.concat(value), []);
    } else {
      return [];
    }
  }

  loadMDMTags(node: Node) {
    if (node.type.toLowerCase() === "test" || node.type.toLowerCase() === "teststep" || node.type.toLowerCase() === "measurement") {
      return this.mdmTagService.getMDMTagOfTagable(node.sourceName, node);
    }
    return EMPTY;
  }

  private parsePreference(pref: Preference) {
    try {
      return <string[]>JSON.parse(pref.value);
    } catch (e) {
      this.notificationService.notifyError(this.msgFaultyPreferenceForAttributesToIgnore, e);
      return [];
    }
  }

  private processFilter(prefList: string[], type: string) {
    return prefList.filter((p) => p.split(".")[0] === type || p.split(".")[0] === "*").map((p) => p.split(".")[1]);
  }

  private getFilteredAttributes(attributes: Attribute[], filter: string[]) {
    if (filter.indexOf("*") !== -1) {
      return [];
    } else {
      return attributes.filter((attr) => filter.indexOf(attr.name) === -1);
    }
  }
}
