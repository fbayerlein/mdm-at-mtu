/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { PreferenceService } from "@core/preference.service";
import { getAttribute } from "@core/utils/node-utils";
import { ContextAttributeIdentifier } from "@details/model/details.model";
import { ContextType, getContextTypeAbbr } from "@details/model/types/context-type";
import { map, shareReplay, take } from "rxjs";

@Injectable()
export class AttributeValidatorService {
  private readonly PREF_ATTR_REX_EXP = "context_attribute.validators";

  private attributeValidatorPref = this.preferenceService.getPreference(this.PREF_ATTR_REX_EXP).pipe(
    shareReplay(1),
    map((prefs) => prefs[0]),
  );

  constructor(private preferenceService: PreferenceService) {}

  /**
   * Gets the validation pattern of an context attribute
   *
   * @param contextAttribute
   * @returns empty string if no pattern for the given attribute is specified in the preference
   */
  public getAttributeValidatorPattern(contextAttribute: ContextAttributeIdentifier) {
    return this.attributeValidatorPref.pipe(
      take(1),
      map((pref) => {
        if (!pref) {
          return;
        }
        let parsed: string;
        let compName = contextAttribute.contextDescribable
          ? contextAttribute.contextDescribable.name
          : contextAttribute.contextComponent?.name;
        if (!compName) {
          compName = getAttribute(contextAttribute.contextComponent, "MimeType")?.value.toString().split(".")[3];
        }
        if (!compName) {
          return;
        }
        const attrToLookFor =
          getContextTypeAbbr(contextAttribute.contextType as ContextType) + "." + compName + "." + contextAttribute.attribute.name;
        try {
          parsed = JSON.parse(pref.value)[attrToLookFor];
        } catch {
          parsed = "";
        }
        if (!parsed) {
          parsed = "";
        }
        return parsed;
      }),
    );
  }
}
