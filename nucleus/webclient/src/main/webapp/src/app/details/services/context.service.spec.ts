/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { PropertyService } from "@core/property.service";
import { Attribute } from "@navigator/node";
import { ContextService } from "./context.service";

describe("ContextService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PropertyService, ContextService],
    });
  });

  describe("mergeAttributes()", () => {
    it("should merge value of one attribute", async(
      inject([ContextService], (contextService) => {
        const attribute1: Attribute = {
          name: "size",
          value: "95R16",
          unit: "",
          dataType: "STRING",
        };
        const attributes = [attribute1];
        const contextIndex = 2;
        const resultAttributes: Attribute[] = [];

        expect(contextService.mergeAttributes(attributes, contextIndex, resultAttributes)).toEqual([
          {
            name: "size",
            value: [undefined, undefined, "95R16"],
            unit: "",
            dataType: "STRING",
          },
        ]);
      }),
    ));

    it("should merge values of multiple attributes", async(
      inject([ContextService], (contextService) => {
        const attribute1: Attribute = {
          name: "size",
          value: "95R16",
          unit: "",
          dataType: "STRING",
        };
        const attribute2: Attribute = {
          name: "side",
          value: "Left",
          unit: "",
          dataType: "STRING",
        };

        const attributes = [attribute1, attribute2];
        const contextIndex = 0;
        const resultAttributes: Attribute[] = [];

        expect(contextService.mergeAttributes(attributes, contextIndex, resultAttributes)).toEqual([
          {
            name: "size",
            value: ["95R16"],
            unit: "",
            dataType: "STRING",
          },
          {
            name: "side",
            value: ["Left"],
            unit: "",
            dataType: "STRING",
          },
        ]);
      }),
    ));
  });

  describe("mergeContextRoots()", () => {
    it("should merge attributes values of all context components", async(
      inject([ContextService], (contextService) => {
        const data = {
          ordered: {
            UNITUNDERTEST: [
              {
                name: "FL_tyre",
                id: "38",
                type: "ContextComponent",
                sourceType: "tyre",
                sourceName: "MDM",
                attributes: [
                  {
                    name: "size",
                    value: "95R16",
                    unit: "",
                    dataType: "STRING",
                  },
                ],
              },
            ],
          },
          measured: {
            UNITUNDERTEST: [
              {
                name: "FL_tyre",
                id: "39",
                type: "ContextComponent",
                sourceType: "tyre",
                sourceName: "MDM",
                attributes: [
                  {
                    name: "size",
                    value: "95R17",
                    unit: "",
                    dataType: "STRING",
                  },
                ],
              },
              {
                name: "engine",
                id: "12",
                type: "ContextComponent",
                sourceType: "engine",
                sourceName: "MDM",
                attributes: [
                  {
                    name: "cylinders",
                    value: "2",
                    unit: "",
                    dataType: "STRING",
                  },
                ],
              },
            ],
          },
        };

        const mergedData = contextService.mergeContextRoots([data.ordered, data.measured]);

        // Workarround since jasmine check for type. Service returns Components, mocked object is of type Obejct.
        /*expect(jasmine.objectContaining(Object.assign({}, mergedData))).toEqual({
        'UNITUNDERTEST': [{
          'name' : 'FL_tyre',
          'id' : '38',
          'type' : 'ContextComponent',
          'sourceType' : 'tyre',
          'sourceName' : 'MDM',
          'attributes' : [{
            'name' : 'size',
            'value' : ['95R16', '95R17'],
            'unit' : '',
            'dataType' : 'STRING'
          }]
        }, {
          'name' : 'engine',
          'id' : '12',
          'type' : 'ContextComponent',
          'sourceType' : 'engine',
          'sourceName' : 'MDM',
          'attributes' : [{
            'name' : 'cylinders',
            'value' : [undefined, '2'],
            'unit' : '',
            'dataType' : 'STRING'
          }]
        }]
      });*/
      }),
    ));
  });
});
