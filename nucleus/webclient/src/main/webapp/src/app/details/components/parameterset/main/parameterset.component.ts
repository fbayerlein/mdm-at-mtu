/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { TRANSLATE } from "@core/mdm-core.module";
import { MDMItem } from "@core/mdm-item";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { ParameterSet } from "@details/model/details.model";
import { DataType, LinkData, Parameter } from "@details/model/parameterset/parameter";
import { ParameterSetService } from "@details/services/parameterset.service";
import { NavigatorService } from "@navigator/navigator.service";
import { Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { Condition, Operator, SearchFilter } from "src/app/search/filter.service";

@Component({
  selector: "mdm-parametersets",
  templateUrl: "./parameterset.component.html",
  styleUrls: ["./parameterset.component.css"],
})
export class ParameterSetComponent implements OnInit {
  private readonly StatusNoDescriptiveData = TRANSLATE("details.parameterset.status-no-parametersets");
  private readonly StatusNoNodes = TRANSLATE("details.sensor.status-no-nodes-available");
  private readonly StatusVirtualNodes = TRANSLATE("details.sensor.status-virtual-node-selected");

  public status: string;
  public nodeStatus: string;
  public parametersets: ParameterSet[];
  editMode: true;
  addParamTitle: string;
  dialogAddParam = false;
  dialogCanRemove = false;
  currentNode: Node | MDMItem;

  // Temp data for dialog
  tmpParamAdd: ParameterSet = new ParameterSet();

  constructor(
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private navigatorService: NavigatorService,
    private parametersetService: ParameterSetService,
  ) {
    this.addParamTitle = translateService.instant("details.parameterset.dialog-add-param");
  }

  ngOnInit() {
    //this.refreshData(this.navigatorService.getSelectedNode());

    // node changed from the navigation tree
    this.navigatorService.onSelectionChanged().subscribe(
      (node) => {
        this.handleNavigatorSelectionChanged(node);
      },
      (error) => this.notificationService.notifyError(this.translateService.instant("details.parameterset.err-cannot-load-data"), error),
    );
  }

  private handleNavigatorSelectionChanged(object: Node | MDMItem) {
    this.nodeStatus = null;
    if (object instanceof Node) {
      this.currentNode = object;
      this.refreshData(object);
    } else if (object instanceof MDMItem) {
      this.nodeStatus = this.StatusVirtualNodes;
    } else {
      this.nodeStatus = this.StatusNoNodes;
    }
  }

  refreshRequested() {
    this.refreshData(this.currentNode);
  }

  addParameterset() {
    this.addParamTitle = this.translateService.instant("details.parameterset.dialog-add-param");
    this.dialogAddParam = true;
    this.dialogCanRemove = false;
  }

  saveDialogAdd() {
    this.parametersetService.createParameterSet(this.currentNode, this.tmpParamAdd).subscribe(
      () => {
        this.refreshData(this.currentNode);
      },
      (error) => {
        this.notificationService.notifyError(this.translateService.instant("details.parameterset.cannot-add-parameterset"), error.message);
      },
    );

    this.cancelDialogAdd();
  }

  hideDialogAdd() {
    if (this.dialogAddParam) {
      this.cancelDialogAdd();
    }
  }

  cancelDialogAdd() {
    this.dialogAddParam = false;
    this.dialogCanRemove = false;
  }

  refreshData(node: Node | MDMItem) {
    this.parametersets = [];
    if (node != null && (node.type === "Measurement" || node.type === "Channel")) {
      this.parametersets = null;
      this.status = this.StatusNoDescriptiveData;
      this.parametersetService.getParameterSets(node).subscribe(
        (data) => {
          if (data != null && data.length > 0) {
            this.parametersets = [];
            this.status = "";
            for (const n of data) {
              const version = this.getAttributeValue("Version", n).toString();
              const label = n.name + " (Version: " + version + ")";
              this.parametersets.push({
                id: n.id,
                sourceName: n.sourceName,
                name: n.name,
                version: version,
                label: label,
              });
            }

            this.getParameters(this.parametersets);
          }
        },
        (error) => this.notificationService.notifyError(this.translateService.instant("details.parameterset.err-cannot-load-data"), error),
      );
    } else {
      this.parametersets = null;
      this.status = this.StatusNoDescriptiveData;
    }
  }

  getRefLinks(value: string): LinkData[] {
    const references: string[] = value.replace("[MDMREF]", "").split(";");

    const links: { link: string; text: string }[] = [];

    references.forEach((reference) => {
      const splittedReference = reference.split("/");

      const channelIds = [];
      const channelGroups = [];
      const environments = [];

      environments.push(splittedReference[0]);
      const channelIdPart = splittedReference[2];
      if (channelIdPart && channelIdPart !== "*") {
        if (channelIdPart.includes(",")) {
          channelIds.push(...channelIdPart.split(","));
        } else {
          channelIds.push(channelIdPart);
        }
      } else {
        channelGroups.push(splittedReference[1]);
      }

      let filter: SearchFilter = undefined;
      const conditions = [];
      let resultType = "ChannelGroup";
      if (channelIds.length > 0) {
        resultType = "Channel";
        conditions.push(new Condition("Channel", "Id", Operator.IN, channelIds));
      }

      if (channelGroups.length > 0) {
        conditions.push(new Condition("ChannelGroup", "Id", Operator.IN, channelGroups));
      }

      filter = new SearchFilter("", environments, resultType, "", conditions);

      const link = `${window.origin}${
        window.location.href.includes("/org.eclipse.mdm.nucleus") ? "/org.eclipse.mdm.nucleus" : ""
      }/navigator/search?filter=${encodeURIComponent(JSON.stringify(filter))}`;

      links.push({ link, text: reference });
    });

    return links;
  }

  getParameters(parametersets: ParameterSet[]) {
    for (const parameterset of parametersets) {
      this.parametersetService.getParameters(parameterset.sourceName, parameterset.id).subscribe(
        (data) => {
          parameterset.parameters = [];
          for (const n of data) {
            const name = this.getAttributeValue("Name", n).toString();
            const value = this.getAttributeValue("Value", n).toString();
            const dataType = this.getAttributeValue("DataType", n).toString();
            const convertedDt = DataType[dataType.toLowerCase()];
            var dt;
            let links: LinkData[];
            if (value.startsWith("[MDMREF]")) {
              links = this.getRefLinks(value);
            }

            if (convertedDt === DataType.date) {
              const pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;
              dt = new Date(value.replace(pattern, "$1-$2-$3T$4:$5:$6"));
            }

            let unitId = null;
            for (const rel of n.relations) {
              if (rel.entityType === "Unit") {
                if (rel.ids != null && rel.ids.length > 0) {
                  unitId = rel.ids[0];
                }
                break;
              }
            }

            if (unitId !== null) {
              this.parametersetService.getUnit(n.sourceName, unitId).subscribe(
                (unitRes) => {
                  let unit = "";
                  if (unitRes.length > 0) {
                    unit = unitRes[0].name;
                  }
                  parameterset.parameters.push(
                    new Parameter({
                      id: n.id,
                      sourceName: n.sourceName,
                      name: name,
                      value: value,
                      dateValue: dt,
                      unit: unit,
                      unitId: unitId,
                      dataType: convertedDt,
                      linkValue: links,
                    }),
                  );
                },
                (error) =>
                  this.notificationService.notifyError(this.translateService.instant("details.parameterset.err-cannot-load-data"), error),
              );
            } else {
              parameterset.parameters.push(
                new Parameter({
                  id: n.id,
                  sourceName: n.sourceName,
                  name: name,
                  value: value,
                  dateValue: dt,
                  unit: "",
                  unitId: unitId,
                  dataType: convertedDt,
                  linkValue: links,
                }),
              );
            }
          }
        },
        (error) => this.notificationService.notifyError(this.translateService.instant("details.parameterset.err-cannot-load-data"), error),
      );
    }
  }

  getAttributeValue(attrName: string, node: Node) {
    if (node.attributes !== null) {
      for (const attr of node.attributes) {
        if (attr.name === attrName) {
          return attr.value;
        }
      }
    }

    return "";
  }
}
