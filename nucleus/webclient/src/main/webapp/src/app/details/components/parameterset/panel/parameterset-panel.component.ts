/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Router } from "@angular/router";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { PropertyService } from "@core/property.service";
import { Parameter, ParameterSet } from "@details/model/details.model";
import { DataType } from "@details/model/parameterset/parameter";
import { ParameterSetService } from "@details/services/parameterset.service";
import { TranslateService } from "@ngx-translate/core";
import { SelectItem } from "primeng/api";

@Component({
  selector: "mdm-parametersetpanel",
  templateUrl: "./parameterset-panel.component.html",
  styleUrls: ["./parameterset-panel.component.css"],
})
export class ParameterSetPanelComponent implements OnInit {
  // handles pannel expansion state
  @Input() collapsed: boolean;
  @Input() parameterSet: ParameterSet;

  // Inform for reload
  @Output() parameterChanged = new EventEmitter<void>();

  // Temp data for dialog
  tmpParamAdd: Parameter = new Parameter();
  tmpUnits: SelectItem[] = [];
  tmpDataTypes: SelectItem[] = [];
  tmpParameterSetEdit: ParameterSet;

  dataType = DataType;
  editMode: boolean;
  canEdit: boolean;
  oldParameters = [];
  columns: any[];
  dialogAddParam = false;
  formNameInvalidate: boolean;
  formTypeInvalidate: boolean;
  parameterTypeSet: boolean;
  dialogParameterSetDelete = false;
  dialogParameterSetEdit = false;
  dialogParameterDelete = false;
  tmpParamToDelete: Parameter;

  constructor(
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private parametersetService: ParameterSetService,
    private datePipe: DatePipe,
    private router: Router,
    private _prop: PropertyService,
  ) {}

  ngOnInit() {
    this.columns = [{ field: "name" }, { field: "value" }, { field: "unit" }];

    this.editMode = false;
    this.canEdit = true;
    this.formNameInvalidate = false;
    this.formTypeInvalidate = false;
    this.parameterTypeSet = false;
    this.tmpParameterSetEdit = this.parameterSet;
  }

  onParameterUnitUpdated(event, item: Parameter) {
    const unitObj = this.tmpUnits.find((u) => u.value == event.value);
    item.unit = unitObj !== undefined ? unitObj.label : "";
  }

  removeParameter(parameter: Parameter) {
    this.dialogParameterDelete = true;
    this.tmpParamToDelete = parameter;
    this.cancelDialogAdd();
  }

  cancelRemoveParameter() {
    this.tmpParamToDelete = null;
    this.dialogParameterDelete = false;
  }

  confirmRemoveParameter() {
    this.parametersetService.removeParameter(this.tmpParamToDelete).subscribe(
      () => {
        this.parameterChanged.emit();
      },
      (error) => {
        this.notificationService.notifyError(
          this.translateService.instant("details.parameterset-panel.cannot-add-parameter"),
          error.message,
        );
      },
    );
    this.cancelRemoveParameter();
  }

  removeParameterSet() {
    this.dialogParameterSetDelete = true;
    this.cancelDialogAdd();
  }

  cancelEditParameterSet() {
    this.dialogParameterSetEdit = false;
  }

  confirmRemoveParameterSet() {
    this.parametersetService.removeParameterSet(this.parameterSet).subscribe(
      () => {
        this.parameterChanged.emit();
      },
      (error) => {
        this.notificationService.notifyError(
          this.translateService.instant("details.parameterset-panel.cannot-add-parameter"),
          error.message,
        );
      },
    );
    this.cancelRemoveParameterSet();
  }

  editDialogParameterSet() {
    this.dialogParameterSetEdit = true;
    this.cancelDialogAdd();
  }

  cancelRemoveParameterSet() {
    this.dialogParameterSetDelete = false;
  }

  confirmEditParameterSet() {
    this.parametersetService.updateParameterSet(this.parameterSet).subscribe(
      () => {
        this.parameterChanged.emit();
      },
      (error) => {
        this.notificationService.notifyError(
          this.translateService.instant("details.parameterset-panel.cannot-add-parameter"),
          error.message,
        );
      },
    );
    this.dialogParameterSetDelete = false;
  }

  validateForm() {
    if (this.tmpParamAdd !== undefined) {
      this.formNameInvalidate = !(this.tmpParamAdd.name !== undefined && this.tmpParamAdd.name.length > 0);
      this.formTypeInvalidate = !(this.tmpParamAdd.dataType !== undefined && this.tmpParamAdd.dataType !== null);
      return;
    }
    this.formNameInvalidate = true;
    this.formTypeInvalidate = true;
  }

  onParameterTypeChanged() {
    if (this.tmpParamAdd !== undefined) {
      this.parameterTypeSet = this.tmpParamAdd.dataType !== undefined && this.tmpParamAdd.dataType !== null;
    }
    this.validateForm();
  }

  canSetUnit(): boolean {
    if (this.tmpParamAdd !== undefined) {
      return DataType[this.tmpParamAdd.dataType] === DataType.integer || DataType[this.tmpParamAdd.dataType] === DataType.double;
    }
    return false;
  }

  canSetUnitForParam(param: Parameter): boolean {
    return param.dataType === DataType.integer || param.dataType === DataType.double;
  }

  isValueAvailable(string): boolean {
    if (this.tmpParamAdd !== undefined) {
      return DataType[string] === DataType[this.tmpParamAdd.dataType];
    }
    return false;
  }

  saveDialogAdd() {
    this.validateForm();
    if (this.formNameInvalidate || this.formTypeInvalidate) {
      this.notificationService.notifyError("Validation error", "Name and data type are required");
      return;
    }
    this.parametersetService.createParameterForParameterSet(this.parameterSet, this.tmpParamAdd).subscribe(
      () => {
        this.parameterChanged.emit();
      },
      (error) => {
        this.notificationService.notifyError(
          this.translateService.instant("details.parameterset-panel.cannot-add-parameter"),
          error.message,
        );
      },
    );

    this.cancelDialogAdd();
  }

  addParameter() {
    this.parametersetService.getUnits(this.parameterSet.sourceName).subscribe(
      (unitsRes) => {
        this.tmpParamAdd = new Parameter();
        this.tmpUnits = unitsRes.map((u) => {
          return { label: u.name, value: u.id };
        });
        this.tmpDataTypes = Object.keys(DataType).map((key) => {
          return { label: DataType[key], value: key };
        });
        this.dialogAddParam = true;
        this.formNameInvalidate = true;
        this.formTypeInvalidate = true;
        this.parameterTypeSet = false;
      },
      (error) => this.notificationService.notifyError(this.translateService.instant("details.parameterset.err-cannot-load-data"), error),
    );
  }

  hideDialogAdd() {
    if (this.dialogAddParam) {
      this.cancelDialogAdd();
    }
  }

  cancelDialogAdd() {
    this.dialogAddParam = false;
  }

  editParameterSet() {
    this.editMode = true;
    this.parametersetService.getUnits(this.parameterSet.sourceName).subscribe(
      (unitsRes) => {
        this.tmpUnits = unitsRes.map((u) => {
          return { label: u.name, value: u.id };
        });
      },
      (error) => this.notificationService.notifyError(this.translateService.instant("details.parameterset.err-cannot-load-data"), error),
    );
    this.oldParameters = [];
    this.parameterSet.parameters.forEach((val) => this.oldParameters.push(Object.assign({}, val)));
  }

  saveParameterSet() {
    this.editMode = false;

    for (const parameter of this.parameterSet.parameters) {
      this.parametersetService.saveParameter(parameter).subscribe(
        (r) => "",
        (error) => {
          this.notificationService.notifyError(
            this.translateService.instant("details.parameterset-panel.err-cannot-save-parameter"),
            error,
          );
        },
      );
    }
  }

  onCancelEdit() {
    this.editMode = false;
    this.parameterSet.parameters = this.oldParameters;
  }

  formatOutput(parameter: Parameter, colField: string): string {
    if (colField === "value") {
      if (parameter.dataType === DataType.date) {
        return this.datePipe.transform(parameter.dateValue, "yyyy/MM/dd");
      }
      return parameter.value;
    }
    return parameter[colField];
  }
}
