import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "mdm-extend-cell-button",
  templateUrl: "./extend-cell-button.component.html",
  styleUrls: ["./extend-cell-button.component.css"],
})
export class ExtendCellButtonComponent implements OnInit {
  @Input() checked: boolean;
  @Output() checkedChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  changeChecked(event: any) {
    this.checkedChanged.emit(event.checked);
  }
}
