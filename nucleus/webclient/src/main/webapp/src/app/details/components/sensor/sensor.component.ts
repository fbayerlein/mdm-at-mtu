/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { TRANSLATE } from "@core/mdm-core.module";
import { MDMItem } from "@core/mdm-item";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { NavigatorService } from "@navigator/navigator.service";
import { Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { Sensor } from "../../model/details.model";
import { ContextService } from "../../services/context.service";

@Component({
  selector: "mdm-sensors",
  templateUrl: "sensor.component.html",
})
export class SensorComponent implements OnInit, OnDestroy {
  private readonly StatusLoading = TRANSLATE("details.sensor.status-loading");
  private readonly StatusNoNodes = TRANSLATE("details.sensor.status-no-nodes-available");
  private readonly StatusVirtualNodes = TRANSLATE("details.sensor.status-virtual-node-selected");
  private readonly StatusNoDescriptiveData = TRANSLATE("details.sensor.status-no-descriptive-data-available");

  public sensors: Sensor[];
  public status: string;

  private sub = new Subscription();

  constructor(
    private _contextService: ContextService,
    private navigatorService: NavigatorService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.status = this.StatusNoNodes;

    this.sub.add(
      this.navigatorService
        .onSelectionChanged()
        .pipe()
        .subscribe(
          (node) => this.handleNavigatorSelectionChanged(node),
          (error) => this.notificationService.notifyError(this.translateService.instant("details.sensor.err-cannot-load-data"), error),
        ),
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  private handleNavigatorSelectionChanged(object: Node | MDMItem) {
    this.sensors = [];
    if (object instanceof Node) {
      this.loadContext(object);
    } else if (object instanceof MDMItem) {
      this.status = this.StatusVirtualNodes;
    } else {
      this.status = this.StatusNoNodes;
    }
  }

  private loadContext(node: Node) {
    this.status = this.StatusLoading;
    if (node != undefined) {
      if (node.name != undefined && (node.type.toLowerCase() === "measurement" || node.type.toLowerCase() === "teststep")) {
        this._contextService.getSensors(node).subscribe(
          (sensors) => (this.sensors = <Sensor[]>sensors),
          (error) =>
            this.notificationService.notifyError(this.translateService.instant("details.sensor.err-cannot-load-descriptive-data"), error),
          () => (this.status = this.StatusNoDescriptiveData),
        );
      } else {
        this.status = this.StatusNoDescriptiveData;
      }
    } else {
      this.status = this.StatusNoNodes;
    }
  }

  public diff(attr1: string, attr2: string) {
    if (attr1 !== attr2) {
      return "danger";
    }
  }
}
