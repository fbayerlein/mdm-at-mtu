/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { functionalRightGuard } from "@core/authentication/authentication.service";
import { MDMDescriptiveDataComponent } from "./components/mdm-detail-descriptive-data/mdm-detail-descriptive-data.component";
import { MDMDetailViewComponent } from "./components/mdm-detail-view/mdm-detail-view.component";
import { MDMDetailComponent } from "./components/mdm-detail/mdm-detail.component";
import { ParameterSetComponent } from "./components/parameterset/main/parameterset.component";
import { SensorComponent } from "./components/sensor/sensor.component";

const detailRoutes: Routes = [
  {
    path: "",
    component: MDMDetailComponent,
    children: [
      {
        path: "parametersets",
        component: ParameterSetComponent,
        canActivate: [functionalRightGuard("mdm-modules.details.parameter-sets.show")],
      },
      { path: "general", component: MDMDetailViewComponent, canActivate: [functionalRightGuard("mdm-modules.details.general.show")] },
      { path: "sensors", component: SensorComponent, canActivate: [functionalRightGuard("mdm-modules.details.sensors.show")] },
      {
        path: ":context",
        component: MDMDescriptiveDataComponent,
        canActivate: [functionalRightGuard("mdm-modules.details.context.show")],
      },
      { path: "", redirectTo: "general", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(detailRoutes)],
  exports: [RouterModule],
})
export class MDMDetailRoutingModule {}
