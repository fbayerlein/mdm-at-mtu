/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

export enum DataType {
  string = "String",
  integer = "Integer",
  double = "Double",
  boolean = "Boolean",
  date = "Date",
}

export interface LinkData {
  link: string;
  text: string;
}

export class Parameter {
  id: string;
  sourceName: string;
  name: string;
  value: string;
  unit: string;
  unitId: number;
  dataType: DataType;
  stringValue: string;
  integerValue: number;
  doubleValue: number;
  booleanValue: boolean;
  dateValue: Date;
  linkValue: LinkData[];

  public constructor(init?: Partial<Parameter>) {
    Object.assign(this, init);
  }
}
