/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, OnInit, ViewChild } from "@angular/core";
import { AxisSelectItem } from "@details/model/details.model";
import { MdmTranslatePipe } from "@localization/mdm-translate.pipe";
import { TranslateService } from "@ngx-translate/core";
import { Chart } from "chart.js";
import zoomPlugin from "chartjs-plugin-zoom";
import { MenuItem } from "primeng/api";
import { UIChart } from "primeng/chart";
import { ContextMenu } from "primeng/contextmenu";
import { combineLatest, take, tap } from "rxjs";
import { Columns, Row } from "src/app/tableview/query.service";

Chart.register(zoomPlugin);

const defaultLegendClickHandler = Chart.defaults.plugins.legend.onClick;

@Component({
  selector: "mdm-quick-plot",
  templateUrl: "./quick-plot.component.html",
  styleUrls: ["./quick-plot.component.css"],
  providers: [MdmTranslatePipe],
})
export class QuickPlotComponent implements OnInit, OnChanges {
  @Input() items: Row[];
  public columns: AxisSelectItem[] = [];
  public xColumn: AxisSelectItem;
  public yColumns: AxisSelectItem[];
  public data: any;
  public options: any;
  public contextMenuItems: MenuItem[];

  @ViewChild("contextMenu") contextMenu: ContextMenu;
  @ViewChild("chart") chart: UIChart;

  constructor(private readonly mdmTranslate: MdmTranslatePipe, private translateService: TranslateService) {}

  ngOnInit(): void {
    this.createContextMenu();
    this.refreshPlot();
  }

  ngOnChanges(): void {
    this.refreshPlot();
  }

  clearYColumns(): void {
    this.yColumns = [];
    this.refreshPlot();
  }

  private createContextMenu() {
    this.contextMenuItems = [
      {
        label: this.translateService.instant("basket.mdm-basket.reset-scale"),
        icon: "pi pi-fw pi-undo",
        command: () => this.chart.chart.resetZoom(),
      },
    ];
  }

  onRightClick(event: MouseEvent): void {
    event.preventDefault();
    this.contextMenu.show(event);
  }

  refreshPlot(): void {
    this.data = this.getEmptyData();
    this.options = this.getEmptyOptions();

    this.setAvailableColumns();

    if (this.xColumn && this.yColumns && this.items && this.items.length > 0) {
      // Add labels
      this.setXAxisLabels();

      // Add x axis title
      this.setXAxisData();

      // Clear empty data
      this.data.datasets = [];
      // Add y data
      this.yColumns.forEach((yColumn, index) => {
        this.setYAxisData(yColumn, index);
      });
    }
  }

  setXAxisLabels(): void {
    this.items.forEach((item) => {
      const value = item.columns.find((col) => {
        return col.attribute === this.xColumn.attribute;
      }).value;

      this.data.labels.push(this.parseValueForTicks(value));
    });
  }

  setXAxisData(): void {
    combineLatest([
      this.mdmTranslate.transform(this.xColumn.type, undefined, this.xColumn.attribute),
      this.mdmTranslate.transform(this.xColumn.type),
    ])
      .pipe(
        take(1),
        tap((data) => {
          this.options.scales["x"] = {
            offset: true,
            title: {
              display: true,
              text: `${data[0]} (${data[1]})`,
            },
          };
        }),
      )
      .subscribe();
  }

  setYAxisData(yColumn, index: number): void {
    const color = this.stringToColour(yColumn.attribute);

    combineLatest([this.mdmTranslate.transform(yColumn.type, undefined, yColumn.attribute), this.mdmTranslate.transform(yColumn.type)])
      .pipe(
        take(1),
        tap((data) => {
          // Add axis to chart
          this.options.scales[`y${index}`] = {
            type: this.getAxisType(this.items[0], yColumn),
            labels: [],
            offset: true,
            time: {
              unit: "day",
              unitStepSize: 1,
              displayFormats: {
                day: "DD",
              },
            },
            display: true,
            position: "left",
            ticks: this.getTicksOptions(this.items[0], yColumn, color),
            title: {
              display: true,
              text: `${data[0]} (${data[1]})`,
            },
            grid: {
              borderColor: color,
            },
          };

          // Create dataset
          const dataset = {
            label: `${data[0]} (${data[1]})`,
            borderColor: color,
            yAxisID: `y${index}`,
            data: [],
          };

          this.items.forEach((item) => {
            dataset.data.push(
              item.columns.find((col) => {
                return col.attribute === yColumn.attribute;
              }).value,
            );
          });

          this.data.datasets.push(dataset);
        }),
      )
      .subscribe();
  }

  getTicksOptions(item, yColumn, color) {
    const ticksOptions = {
      color: color,
      source: "data",
    };

    const value = item.columns.find((col) => {
      return col.attribute === yColumn.attribute;
    }).value;

    if (this.isInt(value)) {
      ticksOptions["callback"] = function (value) {
        if (value % 1 === 0) {
          return value;
        }
      };
    } else if (this.isNumber(value)) {
      ticksOptions["callback"] = function (value) {
        return Number(value).toFixed(1);
      };
    }

    return ticksOptions;
  }

  getAxisType(item, yColumn): string {
    const value = item.columns.find((col) => {
      return col.attribute === yColumn.attribute;
    }).value;

    if (this.isDate(value)) {
      return "time";
    } else if (this.isNumber(value)) {
      return "linear";
    }

    return "category";
  }

  parseValueForTicks(value: string): string {
    let parsedValue = value;

    if (this.isDate(value)) {
      const options: Intl.DateTimeFormatOptions = {
        year: "numeric",
        month: "short",
        day: "numeric",
      };
      parsedValue = new Date(value).toLocaleDateString(undefined, options);
    } else if (this.isInt(value)) {
      parsedValue = parseInt(value, 10).toString();
    } else if (this.isNumber(value)) {
      parsedValue = parseFloat(value).toFixed(1);
    }

    return parsedValue;
  }

  isNumber(value) {
    return !Number.isNaN(Number(value));
  }

  isInt(value) {
    return (
      !isNaN(value) &&
      (function (x) {
        return (x | 0) === x;
      })(parseFloat(value))
    );
  }

  isDate(value) {
    return /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.?\d{0,3}Z/.test(value);
  }

  getEmptyData() {
    return {
      labels: [],
      datasets: [
        {
          label: "No data",
          data: [],
          borderColor: "#fff",
        },
      ],
    };
  }

  getEmptyOptions() {
    return {
      elements: {
        line: {
          tension: 0,
        },
      },
      plugins: {
        zoom: {
          pan: {
            enabled: true,
            mode: "xy",
            onDrag: true,
            treshold: 10,
            mouseButtonPan: "left",
          },
          zoom: {
            wheel: {
              enabled: true,
            },
            pinch: {
              enabled: true,
            },
            mode: "xy",
            scaleMode: "xy",
          },
        },
        legend: {
          onClick: function (e, legendItem, legend) {
            const index = legendItem.datasetIndex;
            const ci = this.chart;

            ci.data.datasets[index].hidden = !ci.data.datasets[index].hidden;

            ci.config._config.options.scales[`y${index}`].display = !ci.data.datasets[index].hidden;

            ci.update();
          },
        },
      },
      scales: {
        y: {
          display: false,
        },
      },
    };
  }

  setAvailableColumns() {
    this.columns = [];
    if (this.items && this.items.length > 0) {
      this.items[0].columns.forEach((column: Columns) => {
        this.columns.push({ attribute: column.attribute, type: column.type });
      });
    }
  }

  stringToColour(str) {
    let hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 6) - hash);
    }
    let colour = "#";
    for (var i = 0; i < 3; i++) {
      const value = (hash >> (i * 8)) & 0xff;
      colour += ("00" + value.toString(16)).slice(-2);
    }
    return colour;
  }
}
