/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MDMItem } from "@core/mdm-item";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { NavigatorService } from "@navigator/navigator.service";
import { Node } from "@navigator/node";
import { Subscription } from "rxjs";

@Component({
  selector: "mdm-detail",
  templateUrl: "mdm-detail.component.html",
  styleUrls: ["./mdm-detail.component.css"],
  providers: [],
})
export class MDMDetailComponent implements OnInit, OnDestroy, IStatefulComponent {
  private subscription = new Subscription();
  private visibleContext = false;
  private visibleChannel = false;
  private isMeasurement = false;
  private lastRoute: string;

  constructor(
    private router: Router,
    private navigatorService: NavigatorService,
    private stateService: StateService,
    private route: ActivatedRoute,
  ) {
    this.stateService.register(this, "mdm-detail");
  }

  getState(): unknown {
    return {
      lastroute: this.route.snapshot.firstChild.url[0].path,
    };
  }
  applyState(state: unknown) {
    this.lastRoute = state["lastroute"];
  }

  ngOnInit() {
    if (this.lastRoute) {
      this.router.navigate([this.lastRoute], { relativeTo: this.route });
    }
    this.subscription.add(this.navigatorService.onSelectionChanged().subscribe((node) => this.refreshVisibility(node)));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.stateService.unregister(this);
  }

  refreshVisibility(object: Node | MDMItem) {
    this.visibleContext = false;
    this.isMeasurement = false;
    this.visibleChannel = false;
    if (
      object != undefined &&
      object.type != undefined &&
      (object.type.toLowerCase() === "measurement" || object.type.toLowerCase() === "teststep" || object.type.toLowerCase() === "test")
    ) {
      this.visibleContext = true;
    } else if (object != undefined && object.type != undefined && object.type.toLowerCase() === "channel") {
      this.visibleChannel = true;
    }

    if (object != undefined && object.type != undefined && object.type.toLowerCase() === "measurement") {
      this.isMeasurement = true;
    }

    // check if we are in the general node and if the context tabs are visible
    if (!this.visibleContext && !this.visibleChannel && this.router.url !== "/navigator/details/general") {
      // redirect to correct router path
      this.router.navigate(["/navigator/details/general"]);
    }
  }

  isVisible(type: string) {
    if (type === "channel") {
      return this.visibleChannel;
    } else {
      return this.visibleContext;
    }
  }
}
