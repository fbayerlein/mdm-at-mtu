/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { PropertyService } from "../core/property.service";
import { FilereleaseService, Release } from "./filerelease.service";

@Component({
  selector: "mdm-filerelease",
  templateUrl: "mdm-filerelease.component.html",
  styles: [".box {border: 1px solid #ddd; border-radius: 4px;}"],
})
export class MDMFilereleaseComponent implements OnInit {
  incoming: Release[] = [];
  outgoing: Release[] = [];
  release: Release = new Release();
  event = "display";
  dataHost: string;

  lgModal = false;
  smModal = false;

  constructor(
    private service: FilereleaseService,
    private prop: PropertyService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {
    this.dataHost = prop.getDataHost();
  }

  ngOnInit() {
    this.getReleases();
  }

  getReleases() {
    this.service.readOutgoging().subscribe(
      (releases) => (this.outgoing = releases),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("filerelease.mdm-filerelease.err-cannot-read-outgoing-release"),
          error,
        ),
    );
    this.service.readIncomming().subscribe(
      (releases) => (this.incoming = releases),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("filerelease.mdm-filerelease.err-cannot-read-outgoing-release"),
          error,
        ),
    );
  }
  setData(release) {
    this.release = release;
  }
  getState(release: Release) {
    if (release.state === "RELEASE_ORDERED") {
      return "info";
    }
    if (release.state === "RELEASE_APPROVED") {
      return "warning";
    }
    if (release.state === "RELEASE_RELEASED") {
      return "success";
    }
    if (release.state === "RELEASE_EXPIRED") {
      return "danger";
    }
    if (release.state === "RELEASE_REJECTED") {
      return "danger";
    }
    if (release.state === "RELEASE_PROGRESSING_ERROR") {
      return "danger";
    }
    if (release.state === "RELEASE_PROGRESSING") {
      return "warning";
    }
    return "info";
  }
  rejectRelease(reason: string) {
    this.release.rejectMessage = reason;
    this.service.reject(this.release).subscribe(
      (release) => this.updateList(release),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("filerelease.mdm-filerelease.err-error-declining-release"),
          error,
        ),
    );
  }
  approveRelease() {
    this.service.approve(this.release).subscribe(
      (release) => this.updateList(release),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("filerelease.mdm-filerelease.err-error-approving-release"),
          error,
        ),
    );
    this.release.state = "RELEASE_PROGRESSING";
  }
  updateList(id) {
    let pos = this.outgoing
      .map(function (e) {
        return e.identifier;
      })
      .indexOf(id);
    if (pos !== -1) {
      this.outgoing.splice(pos, 1);
    }
    pos = this.incoming
      .map(function (e) {
        return e.identifier;
      })
      .indexOf(id);
    if (pos !== -1) {
      this.incoming.splice(pos, 1);
    }
  }
  isDeletable() {
    if (this.release.state !== "RELEASE_PROGRESSING") {
      return false;
    }
    return true;
  }
  deleteRelease() {
    const id = this.release.identifier;
    this.service.delete(this.release).subscribe(
      (data) => this.updateList(id),
      (err) => console.log(err),
    );
  }
  setEvent(event) {
    this.event = event;
  }
  isReleaseable() {
    if (this.release.state !== "RELEASE_ORDERED") {
      return true;
    }
    return false;
  }
  getFormat(format) {
    return this.service.formatMap[format];
  }
  getTransState(state) {
    return this.service.stateMap[state];
  }
  getDate(date) {
    if (date === 0) {
      return;
    }
    return this.service.formatDate(date);
  }
}
