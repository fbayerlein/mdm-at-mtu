/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { DateTime, Zone } from "luxon";
import { BehaviorSubject, ReplaySubject } from "rxjs";
import * as tzdata from "tzdata/tzdata.js";

export type DateFormatType = "absolute" | "relative" | "combined";
export type HourFormatType = "h11" | "h12" | "h23" | "h24" | "auto";

@Injectable({
  providedIn: "root",
})
export class TimezoneService {
  private timezoneChanged = new BehaviorSubject<string>(this.getSelectedTimezone());
  private dateTimeConfigChanged = new ReplaySubject<boolean>();

  constructor(private translateService: TranslateService) {
    this.translateService.onLangChange.subscribe(() => this.dateTimeConfigChanged.next(true));
  }

  getDateTimeConfigChanged() {
    return this.dateTimeConfigChanged.asObservable();
  }

  /**
   * Should be private and used only locally.
   *
   * Prefer getTimezone() for external requests.
   */
  getSelectedTimezone(): string | undefined {
    if (localStorage && localStorage["selectedTimezone"]) {
      return localStorage["selectedTimezone"];
    } else {
      return Intl.DateTimeFormat().resolvedOptions().timeZone;
    }
  }

  /**
   * Returns the currently selected timezone
   * @returns
   */
  public getTimeZone() {
    return this.timezoneChanged.asObservable();
  }

  setTimezone(timezone: string) {
    let previouslySelected = this.getPreviouslySelected();
    previouslySelected.unshift(timezone);
    previouslySelected = previouslySelected
      .filter(function (item, pos) {
        return previouslySelected.indexOf(item) == pos;
      })
      .slice(0, 3);
    if (localStorage) {
      localStorage["selectedTimezone"] = timezone;
      localStorage["previouslySelected"] = JSON.stringify(previouslySelected);
    }
    this.timezoneChanged.next(timezone);
    this.dateTimeConfigChanged.next(true);
  }

  getAvailableTimezones() {
    const luxonValidTimezones = [
      ...new Set<string>(Object.keys(tzdata.zones).filter((tz) => tz.includes("/") && DateTime.local().setZone(tz).isValid)),
    ].sort((a, b) => (a < b ? -1 : 1));

    return luxonValidTimezones;
  }

  getPreviouslySelected() {
    const previouslySelectedTimezones: string[] = [];
    if (localStorage) {
      if (localStorage["previouslySelected"]) {
        previouslySelectedTimezones.push(...JSON.parse(localStorage["previouslySelected"]));
      }
    }
    if (previouslySelectedTimezones.length == 0) {
      previouslySelectedTimezones.push(this.getSelectedTimezone());
    }
    return previouslySelectedTimezones;
  }

  getRestOfAvailableTimezones() {
    const available = this.getAvailableTimezones();
    const selected = this.getPreviouslySelected();
    return available.filter((n) => !selected.includes(n));
  }

  getDateFormatType(): DateFormatType {
    return localStorage["dateFormatType"] || "absolute";
  }

  setDateFormatType(dateFormatType: DateFormatType) {
    localStorage["dateFormatType"] = dateFormatType;
    this.dateTimeConfigChanged.next(true);
  }

  getHourFormatType(): HourFormatType {
    return localStorage["hourFormatType"] || "auto";
  }

  setHourFormatType(hourFormatType: HourFormatType) {
    localStorage["hourFormatType"] = hourFormatType;
    this.dateTimeConfigChanged.next(true);
  }

  getIsoFormat() {
    try {
      return JSON.parse(localStorage["isoFormat"]) as boolean;
    } catch (e) {
      return false;
    }
  }

  setIsoFormat(selectedIsoFormat: boolean) {
    localStorage["isoFormat"] = selectedIsoFormat;
    this.dateTimeConfigChanged.next(true);
  }

  /**
   *
   * @param date input DateTime
   * @param zone overrides timezone-service selected zone
   * @returns
   */
  public formatDateTime(date: DateTime, zone?: string | Zone, formatOptions?: Intl.DateTimeFormatOptions) {
    const dataFormatType = this.getDateFormatType();
    if (dataFormatType === "relative") {
      return this.localize(date, zone).toRelative();
    } else if (dataFormatType === "combined") {
      const at = this.translateService.currentLang === "de" ? " am " : " at ";
      return this.localize(date, zone).toRelative() + at + this.formatAbsoluteDateTime(date, zone, formatOptions);
    } else {
      return this.formatAbsoluteDateTime(date, zone, formatOptions);
    }
  }

  /**
   *
   * @param date input DateTime
   * @param zone overrides timezone-service selected zone
   * @returns
   */
  public formatAbsoluteDateTime(date: DateTime, zone?: string | Zone, formatOptions?: Intl.DateTimeFormatOptions) {
    if (this.getIsoFormat()) {
      return this.localize(date, zone).toISO({ suppressMilliseconds: true });
    } else {
      return this.toLocaleString(date, formatOptions ?? DateTime.DATETIME_SHORT_WITH_SECONDS, zone);
    }
  }

  /**
   *
   * @param date input DateTime
   * @param zone overrides timezone-service selected zone
   * @returns
   */
  public formatAbsoluteDate(date: DateTime, zone?: string | Zone) {
    if (this.getIsoFormat()) {
      return this.localize(date, zone).toISODate();
    } else {
      return this.toLocaleString(date, DateTime.DATE_SHORT, zone);
    }
  }

  /**
   *
   * @param date input DateTime
   * @param zone overrides timezone-service selected zone
   * @returns
   */
  public formatAbsoluteTime(date: DateTime, zone?: string | Zone) {
    if (this.getIsoFormat()) {
      return this.localize(date, zone).toISOTime({ suppressMilliseconds: true });
    } else {
      return this.toLocaleString(date, DateTime.TIME_24_SIMPLE, zone);
    }
  }

  /**
   *
   * @param date input DateTime
   * @param formatOptions
   * @param zone overrides timezone-service selected zone
   * @returns
   */
  private toLocaleString(date: DateTime, formatOptions: Intl.DateTimeFormatOptions, zone?: string | Zone) {
    const hourCycle = this.getHourFormatType();
    const format = Object.assign({}, formatOptions);
    if (hourCycle != "auto") {
      format.hourCycle = hourCycle;
    }
    return this.localize(date, zone).toLocaleString(format);
  }

  /**
   *
   * @param date input DateTime
   * @param zone overrides timezone-service selected zone
   * @returns
   */
  private localize(date: DateTime, zone?: string | Zone) {
    const z = zone ?? this.getSelectedTimezone();
    return date.setLocale(this.translateService.currentLang).setZone(z);
  }
}
