/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { TranslateService } from "@ngx-translate/core";
import { forkJoin as observableForkJoin, Observable, of as observableOf } from "rxjs";
import { catchError, map, mergeMap, publishReplay, refCount } from "rxjs/operators";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { Preference, PreferenceService, Scope } from "../core/preference.service";
import { PropertyService } from "../core/property.service";
import { LocalizationService } from "../localization/localization.service";
import { Filter, Query } from "../tableview/query.service";
import { View } from "../tableview/tableview.service";
import { TimezoneService } from "../timezone/timezone-service";
import { Condition, Operator, OperatorUtil, SearchFilter } from "./filter.service";

export class SearchLayout {
  map: { [sourceNames: string]: Condition[] } = {};

  public static createSearchLayout(envs: string[], attributesPerEnv: { [env: string]: SearchAttribute[] }, conditions: Condition[]) {
    const conditionsWithSortIndex = conditions.map((c, i) => {
      c.sortIndex = i;
      return c;
    });

    const result = new SearchLayout();
    const attribute2Envs = SearchLayout.mapAttribute2Environments(envs, attributesPerEnv);
    const globalEnv = "Global";
    Object.keys(attribute2Envs).forEach((attr) => {
      const c = conditionsWithSortIndex.find((cond) => cond.type + "." + cond.attribute === attr);
      if (c) {
        if (attribute2Envs[attr].length === envs.length) {
          result.add(globalEnv, c);
        } else {
          attribute2Envs[attr].forEach((e) => result.add(e, c));
        }
      }
    });
    return result;
  }

  public static groupByEnv(attrs: SearchAttribute[]) {
    const attributesPerEnv: { [environment: string]: SearchAttribute[] } = {};

    attrs.forEach((attr) => {
      attributesPerEnv[attr.source] = attributesPerEnv[attr.source] || [];
      attributesPerEnv[attr.source].push(attr);
    });
    return attributesPerEnv;
  }

  private static mapAttribute2Environments(envs: string[], attributesPerEnv: { [environment: string]: SearchAttribute[] }) {
    const attribute2Envs: { [attribute: string]: string[] } = {};

    Object.keys(attributesPerEnv)
      .filter((env) => envs.find((e) => e === env))
      .forEach((env) =>
        attributesPerEnv[env].forEach((sa) => {
          const attr = sa.boType + "." + sa.attrName;

          attribute2Envs[attr] = attribute2Envs[attr] || [];
          attribute2Envs[attr].push(env);
        }),
      );

    return attribute2Envs;
  }

  getSourceNames() {
    return Object.keys(this.map).sort((s1, s2) => {
      if (s1 === "Global") {
        return -1;
      } else if (s2 === "Global") {
        return 1;
      } else if (s1) {
        return s1.localeCompare(s2);
      } else {
        return -1;
      }
    });
  }

  getConditions(sourceName: string) {
    return (this.map[sourceName] || []).sort((c1, c2) => c1.sortIndex - c2.sortIndex);
  }

  getSourceName(condition: Condition) {
    if (condition) {
      let sourceName;

      Object.keys(this.map).forEach((env) => {
        if (this.map[env].find((c) => c.type === condition.type && c.attribute === condition.attribute)) {
          sourceName = env;
        }
      });
      return sourceName;
    }
  }

  set(sourceName: string, conditions: Condition[]) {
    this.map[sourceName] = conditions;
  }

  add(sourceName: string, condition: Condition) {
    this.map[sourceName] = this.map[sourceName] || [];
    this.map[sourceName].push(condition);
  }
}

export class SearchAttribute {
  source: string;
  boType: string;
  attrName: string;
  valueType: string;
  criteria: string;

  constructor(source: string, boType: string, attrName: string, valueType?: string, criteria?: string) {
    this.source = source;
    this.boType = boType;
    this.attrName = attrName;
    this.valueType = valueType || "string";
    this.criteria = criteria || "";
  }
}

export class SearchDefinition {
  key: string;
  value: string;
  type: string;
  label: string;
}

export class SearchDefaults {
  resultType: string;

  constructor(resultType?: string) {
    if (resultType !== undefined && resultTypes.includes(resultType)) {
      this.resultType = resultType;
    } else {
      this.resultType = resultTypes[0];
    }
  }
}

export const resultTypes: string[] = ["Test", "TestStep", "Measurement", "ChannelGroup", "Channel"];

@Injectable()
export class SearchService {
  private _searchUrl: string;
  private errorMessage: string;

  private defs: SearchAttribute[];

  ignoreAttributesPrefs: Preference[] = [];

  private cachedAttributes = new Map(); //Observable<any>;

  constructor(
    private http: HttpClient,
    private localService: LocalizationService,
    private _prop: PropertyService,
    private preferenceService: PreferenceService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private timezoneService: TimezoneService,
  ) {
    this.preferenceService.getPreference("ignoredAttributes").subscribe(
      (prefs) => (this.ignoreAttributesPrefs = this.ignoreAttributesPrefs.concat(prefs)),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("search.search.err-cannot-load-preference-for-attributes-to-ignore"),
          error,
        ),
    );
  }

  public getDefaults(): Observable<SearchDefaults> {
    return this.preferenceService.getPreferenceForScope(Scope.SYSTEM, "search.default_values").pipe(
      map((prefs) => {
        return prefs.length > 0 ? new SearchDefaults(JSON.parse(prefs[0].value)["resultType"]) : new SearchDefaults();
      }),
    );
  }

  loadSearchAttributes(type: string, env: string) {
    return this.http.get<any>(this._prop.getUrl("mdm/environments/" + env + "/" + type + "/searchattributes")).pipe(
      map((response) => <SearchAttribute[]>response.data),
      map((sas) =>
        sas.map((sa) => {
          sa.source = env;
          return sa;
        }),
      ),
      map((sas) => this.filterIgnoredAttributes(env, sas)),
      catchError((e) => addErrorDescription(e, "Could not request search attributes!")),
    );
  }

  getDefinitionsSimple() {
    return observableOf([
      <SearchDefinition>{
        key: "1",
        value: "tests",
        type: resultTypes[0],
        label: "Versuche",
      },
      <SearchDefinition>{
        key: "2",
        value: "teststeps",
        type: resultTypes[1],
        label: "Versuchsschritte",
      },
      <SearchDefinition>{
        key: "3",
        value: "measurements",
        type: resultTypes[2],
        label: "Messungen",
      },
      <SearchDefinition>{
        key: "4",
        value: "channelgroups",
        type: resultTypes[3],
        label: "Kanalgruppen",
      },
      <SearchDefinition>{
        key: "5",
        value: "channels",
        type: resultTypes[4],
        label: "Kanäle",
      },
    ]);
  }

  getSearchAttributesPerEnvs(envs: string[], type: string) {
    return observableForkJoin(
      envs.map((env) =>
        this.loadSearchAttributes(type, env).pipe(
          map((sas) =>
            sas.map((sa) => {
              sa.source = env;
              return sa;
            }),
          ),
        ),
      ),
    ).pipe(
      map((x) =>
        x.reduce(function (explored, toExplore) {
          return explored.concat(toExplore);
        }, []),
      ),
    );
  }

  loadSearchAttributesStructured(environments: string[]): Observable<{ [type: string]: { [env: string]: SearchAttribute[] } }> {
    // TODO
    if (!this.cachedAttributes.has(environments.toString())) {
      this.cachedAttributes.set(
        environments.toString(),
        this.getDefinitionsSimple().pipe(
          map((defs) => defs.map((d) => d.value)),
          mergeMap((defs) => this.loadSearchAttributesForAllDefs(defs, environments)),
          publishReplay(1),
          refCount(),
        ),
      );
    }
    return this.cachedAttributes.get(environments.toString());
  }

  loadSearchAttributesForAllDefs(types: string[], environments: string[]) {
    return observableForkJoin(types.map((type) => this.loadSearchAttributesForDef(type, environments))).pipe(
      map((type2AttributesPerEnv) =>
        type2AttributesPerEnv.reduce(function (acc, value) {
          acc[value.type] = value.attributesPerEnv;
          return acc;
        }, <{ [type: string]: { [env: string]: SearchAttribute[] } }>{}),
      ),
    );
  }

  loadSearchAttributesForDef(type: string, environments: string[]) {
    return observableForkJoin(
      environments.map((env) =>
        this.loadSearchAttributes(type, env).pipe(
          catchError((error) => {
            console.log("Could not load search attributes for type " + type + " in environment " + env);
            return observableOf(<SearchAttribute[]>[]);
          }),
          map((attrs) => {
            return { env: env, attributes: attrs };
          }),
        ),
      ),
    ).pipe(
      map((attrsPerEnv) =>
        attrsPerEnv.reduce(function (acc, value) {
          acc[value.env] = value.attributes;
          return acc;
        }, <{ [env: string]: SearchAttribute[] }>{}),
      ),
      map((attrsPerEnv) => {
        return { type: type, attributesPerEnv: attrsPerEnv };
      }),
    );
  }

  convertToQuery(
    searchFilter: SearchFilter,
    attr: { [type: string]: { [env: string]: SearchAttribute[] } },
    view: View,
    and = true,
    caseSensitive: boolean,
  ) {
    const q = new Query();

    q.resultType = searchFilter.resultType;
    if (attr && attr["tests"]) {
      const envsAttr = this.getEnvFromCachedAttributes();
      const envs: string[] = [];
      searchFilter.environments.forEach((env) => {
        envsAttr.forEach((val) => {
          env === val ? envs.push(env) : null;
        });
      });

      q.filters = this.convert(envs, searchFilter.conditions, attr["tests"], searchFilter.fulltextQuery, and, caseSensitive); // TODO
    }
    q.columns = view.columns.map((c) => c.type + "." + c.name);
    console.log("Query", q);

    return q;
  }

  /*
   * get the environments from the cached Attributes
   */
  getEnvFromCachedAttributes() {
    const envs: string[] = [];
    this.cachedAttributes.forEach((value: any, key: string) => {
      key.split(",").forEach((env) => {
        if (!envs.some((val) => env === val)) {
          envs.push(env);
        }
      });
    });
    return envs;
  }

  convert(
    envs: string[],
    conditions: Condition[],
    attr: { [env: string]: SearchAttribute[] },
    fullTextQuery: string,
    and: boolean,
    caseSensitive: boolean,
  ): Filter[] {
    return envs.map((e) => this.convertEnv(e, conditions, attr[e], fullTextQuery, and, caseSensitive)).filter((e) => e != undefined);
  }

  convertEnv(
    env: string,
    conditions: Condition[],
    attrs: SearchAttribute[],
    fullTextQuery: string,
    and: boolean,
    caseSensitive: boolean,
  ): Filter {
    
    const filterString = conditions
      .map((c) => {
        if (c.operator === Operator.BETWEEN) {
          if (c.value[0] != undefined && c.value[0].length > 0 && c.value[1] != undefined && c.value[1].length > 0) {
            return (
              c.type +
              "." +
              c.attribute +
              " " +
              OperatorUtil.toFilterString(c.operator) +
              " (" +
              this.quoteValue(c.value[0], this.getValueType(c, attrs)) +
              ", " +
              this.quoteValue(c.value[1], this.getValueType(c, attrs)) +
              ")"
            );
          } else {
            return "";
          }
        } else if (c.operator === Operator.IN) {
          if (c.value != undefined && c.value.length > 0) {
            return (
              c.type +
              "." +
              c.attribute +
              " " +
              OperatorUtil.toFilterString(c.operator) +
              " (" +
              c.value.map((cVal) => this.quoteValue(cVal, this.getValueType(c, attrs))).join(",") +
              ")"
            );
          } else {
            return "";
          }
        } else if (c.operator === Operator.EQUALS || c.operator === Operator.LIKE) {
          const valueType = this.getValueType(c, attrs);
          if (valueType.length > 0) {
            return c.value
              .map(
                (value) =>
                  c.type +
                  "." +
                  c.attribute +
                  " " +
                  this.adjustOperator(OperatorUtil.toFilterString(c.operator), valueType, c.operator === Operator.EQUALS ? true : false) +
                  " " +
                  this.quoteValue(value, valueType),
              )
              .join(" or ");
          } else {
            return "";
          }
        } else {
          const valueType = this.getValueType(c, attrs);
          if (valueType.length > 0) {
            return c.value
              .map(
                (value) =>
                  c.type +
                  "." +
                  c.attribute +
                  " " +
                  this.adjustOperator(OperatorUtil.toFilterString(c.operator), valueType, caseSensitive) +
                  " " +
                  this.quoteValue(value, valueType),
              )
              .join(c.operator === Operator.NOT_EQUALS ? " and " : " or ");
          } else {
            return "";
          }
        }
      })
      .filter((c) => c.length > 0)
      .map((c) => "( " + c + " )")
      .join(and ? " and " : " or ");

    if (filterString.length != 0 || fullTextQuery.length !== 0) {
      return new Filter(env, filterString, fullTextQuery);
    }
  }

  public convertToCondition(source: string, serial: string) {
    return this.http.get<any>(this._prop.getUrl("mdm/environments/" + source + "/conditions/serial/" + serial)).pipe(
      map((response) =>
        (response.data as any[]).map((cond) => Condition.deserialize(new Condition(undefined, undefined, undefined, undefined), cond)),
      ),
      catchError((e) => addErrorDescription(e, "Could not request serial!")),
    );
  }

  quoteValue(value: string, valueType: string) {
    if (
      valueType.toLowerCase() === "string" ||
      valueType.toLowerCase() === "string_sequence" ||
      valueType.toLowerCase() === "date" ||
      valueType.toLowerCase() === "date_sequence"
    ) {
      return '"' + this.adjustValue(value, valueType) + '"';
    } else if (
      typeof value === "string" &&
      (valueType.toLowerCase() === "enumeration" || valueType.toLowerCase() === "enumeration_sequence")
    ) {
      return '"' + this.adjustValue(value, valueType) + '"';
    } else {
      return this.adjustValue(value, valueType);
    }
  }

  getSourceTimezone(env: string) {
    this.http
      .get<any>(this._prop.getUrl("mdm/environments/" + env))
      .pipe(
        map((response) =>
          (response.data as any[])
            .filter((source) => {
              return source.sourceName == env;
            })
            .map((source) =>
              source.attributes.filter((attr) => {
                return attr.name == "Timezone";
              }),
            ),
        ),
        catchError((e) => addErrorDescription(e, "Could not request serial!")),
      )
      .subscribe((res) => {
        if (res.length > 0) {
          return res[0].value;
        }
      });

    return "UTC";
  }

  adjustValue(value: string, valueType: string) {
    if (valueType.toLowerCase() === "date") {
      const calcTimeZone = this.calcTime(this.timezoneService.getSelectedTimezone(), "UTC", new Date(value + "Z"));
      return calcTimeZone.toISOString().split(".")[0];
    } else {
      return value;
    }
  }

  private calcTime(fromTimezone: string, toTimezone: string, dateFromTimezone: Date): Date {
    const dateToGetOffset = dateFromTimezone;

    const fromTimeString = dateToGetOffset.toLocaleTimeString("en-UK", {
      timeZone: fromTimezone,
      hour12: false,
    });
    const toTimeString = dateToGetOffset.toLocaleTimeString("en-UK", {
      timeZone: toTimezone,
      hour12: false,
    });

    const fromTimeHours: number = parseInt(fromTimeString.substr(0, 2), 10);
    const toTimeHours: number = parseInt(toTimeString.substr(0, 2), 10);

    const offset: number =  toTimeHours - fromTimeHours;

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    const dateFromTimezoneUTC = Date.UTC(
      dateFromTimezone.getUTCFullYear(),
      dateFromTimezone.getUTCMonth(),
      dateFromTimezone.getUTCDate(),
      dateFromTimezone.getUTCHours(),
      dateFromTimezone.getUTCMinutes(),
      dateFromTimezone.getUTCSeconds(),
    );

    // create new Date object for different city
    // using supplied offset
    const dateUTC = new Date(dateFromTimezoneUTC + 3600000 * offset);

    // return time as a string
    return dateUTC;
  }

  getValueType(c: Condition, attrs: SearchAttribute[]) {
    if (attrs === undefined) {
      return "";
    }
    const suchAttr = attrs.find((a) => a.boType === c.type && a.attrName === c.attribute);
    return suchAttr === undefined ? "" : suchAttr.valueType;
  }

  adjustOperator(operator: string, valueType: string, caseSensitive: boolean) {
    if (!caseSensitive && valueType.toLowerCase() === "string") {
      return "ci_" + operator;
    } else {
      return operator;
    }
  }

  isAttributeIgnored(attributeName: string, sa: SearchAttribute) {
    const x = attributeName.split(".", 2);
    const fType = x[0];
    const fName = x[1];
    return (fType === sa.boType || fType === "*") && (fName === sa.attrName || fName === "*");
  }

  private filterIgnoredAttributes(environment: string, searchAttributes: SearchAttribute[]) {
    const filters = this.getFilters(environment);
    filters.forEach((f) => (searchAttributes = searchAttributes.filter((sa) => !this.isAttributeIgnored(f, sa))));
    return searchAttributes;
  }

  getFilters(source: string): string[] {
    return this.ignoreAttributesPrefs
      .filter((p) => p.scope !== Scope.SOURCE || p.source === source)
      .sort(Preference.sortByScope)
      .map((p) => this.parsePreference(p))
      .reduce((acc, value) => acc.concat(value), []);
  }

  private parsePreference(pref: Preference) {
    try {
      return <string[]>JSON.parse(pref.value);
    } catch (e) {
      this.notificationService.notifyError(
        this.translateService.instant("search.search.err-faulty-preference-for-attributes-to-ignore"),
        e,
      );
      return [];
    }
  }
}
