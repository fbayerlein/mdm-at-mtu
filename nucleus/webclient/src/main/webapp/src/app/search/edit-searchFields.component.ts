/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { PreferenceService } from "@core/preference.service";
import { MdmLocalizationService } from "@localization/mdm-localization.service";
import { MdmTranslatePipe } from "@localization/mdm-translate.pipe";
import { TranslateService } from "@ngx-translate/core";
import { TreeNode } from "primeng/api";
import { map, Subscription } from "rxjs";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { Node } from "../navigator/node";
import { NodeService } from "../navigator/node.service";
import { SearchattributeTreeComponent } from "../searchattribute-tree/searchattribute-tree.component";
import { Condition, Operator, SearchFilter } from "./filter.service";
import { SearchAttribute, SearchLayout } from "./search.service";

export class SearchField {
  group: string;
  attribute: string;

  constructor(group?: string, attribute?: string) {
    this.group = group || "";
    this.attribute = attribute || "";
  }

  equals(searchField: SearchField) {
    return searchField.group === this.group && searchField.attribute === this.attribute;
  }
}

@Component({
  selector: "mdm-edit-search-fields",
  templateUrl: "edit-searchFields.component.html",
})
export class EditSearchFieldsComponent implements OnChanges, OnInit, AfterViewInit, OnDestroy {
  public lgEditSearchFieldsModal = false;
  @ViewChild(SearchattributeTreeComponent) tree: SearchattributeTreeComponent;

  @Input() environments: Node[];
  @Input() searchAttributes: { [env: string]: SearchAttribute[] } = {};
  @Input() filters: SearchFilter[];

  allSuggestions: { label: string; group: string; attribute: SearchAttribute }[] = [];
  filteredSuggestions: { label: string; group: string; attribute: SearchAttribute }[] = [];
  selectedSuggestion: SearchAttribute;

  @Output()
  conditionsSubmitted = new EventEmitter<Condition[]>();

  @Output()
  newFilterSubmitted = new EventEmitter<SearchFilter>();

  layout: SearchLayout = new SearchLayout();
  conditions: Condition[] = [];
  selectedSearchAttribute: SearchAttribute;
  newFilterName = "";
  newSearchFieldsComponent = false;
  selectedFilterName: string;
  caseSensitivity = false;

  private allUniqueBoTypeTranslations: any[] = [];
  private allAttribueTranslations: any[] = [];
  private subscription = new Subscription();

  constructor(
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private localizationService: MdmLocalizationService,
    private preferenceService: PreferenceService,
  ) {}

  ngOnInit() {
    this.subscription.add(this.translateService.onLangChange.subscribe(() => this.onLanguageChanged()));
    this.preferenceService
      .getPreference("user-settings.search-attributes-case-sensitivity")
      .pipe(map((prefs) => prefs.map((p) => p.value)))
      .subscribe((pref) => {
        if (pref.length > 0) this.caseSensitivity = pref.some((p) => p === "true");
      });
  }

  ngAfterViewInit() {
    this.subscription.add(
      this.tree.onNodeSelect$.subscribe(
        (node) => this.nodeSelect(node),
        (error) =>
          this.notificationService.notifyError(
            this.translateService.instant("search.edit-searchfields.err-cannot-update-selected-node"),
            error,
          ),
      ),
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["searchAttributes"] || changes["environments"]) {
      this.onLanguageChanged();
    }
    if (changes["environments"]) {
      this.conditionUpdated();
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private onLanguageChanged() {
    this.allUniqueBoTypeTranslations = [];
  }

  asyncTranslateBoTypesAndAttributes() {
    if (this.allUniqueBoTypeTranslations.length === 0) {
      const allAttributes: SearchAttribute[] = Object.keys(this.searchAttributes)
        .map((env) => this.searchAttributes[env])
        .reduce((acc, value) => acc.concat(value), <SearchAttribute[]>[]);

      this.allAttribueTranslations = [];

      const allBoTypes: any[] = allAttributes.map((sa) => {
        return { key: sa.source + "." + sa.boType, boType: sa.boType, label: sa.boType };
      });
      allBoTypes.sort((bo1, bo2) => bo1["key"].localeCompare(bo2["key"]));
      const allUniqueBoTypes: any[] = [];
      allBoTypes.forEach((bo1) => {
        if (allUniqueBoTypes.findIndex((bo2) => bo1["key"].localeCompare(bo2["key"]) === 0) === -1) {
          allUniqueBoTypes.push(bo1);
        }
      });

      const translatePipe: MdmTranslatePipe = new MdmTranslatePipe(this.localizationService);

      allUniqueBoTypes.forEach((bt, i) => {
        translatePipe
          .transform(bt["boType"], bt["source"])
          .subscribe((result) => {
            // for some keys we have 3 or more different translations
            // and thus 3 or more iterations inside the subscription
            // e.g. 'Channel' = 'Channel'; 'Channel' = 'Measurement quantity'
            // take the last one like in attribute tree using Translate pipe
            // to get the same displayed results
            bt["label"] = result;
            if (this.allUniqueBoTypeTranslations.length < allUniqueBoTypes.length) {
              delete bt["boType"];
              this.allUniqueBoTypeTranslations.push(bt);
            }
            if (this.allUniqueBoTypeTranslations.length === allUniqueBoTypes.length && allUniqueBoTypes.length - 1 === i) {
              this.onTranslationDone();
            }
          })
          .unsubscribe();
      });

      allAttributes.forEach((attr, i) => {
        translatePipe
          .transform(attr.boType, attr.source, attr.attrName)
          .subscribe((result) => {
            if (this.allAttribueTranslations.length <= i) {
              const item = {
                key: attr.source + "." + attr.boType + "." + attr.attrName,
                label: result,
              };
              this.allAttribueTranslations.push(item);
              if (allAttributes.length - 1 === i) {
                this.onTranslationDone();
              }
            }
          })
          .unsubscribe();
      });
    }
  }

  /**
   * this is called, when all subscriptions finished updating the
   * asynchronous translation result for bo types or attributes
   */
  private onTranslationDone() {
    if (this.allUniqueBoTypeTranslations.length > 0 && this.allAttribueTranslations.length > 0) {
      const allAttributes: SearchAttribute[] = Object.keys(this.searchAttributes)
        .map((env) => this.searchAttributes[env])
        .reduce((acc, value) => acc.concat(value), <SearchAttribute[]>[]);

      // space will not work with type-ahead values
      const ar = allAttributes.map((sa) => {
        return { label: this.findBoTypeTranslation(sa) + "." + this.findAttributeTranslation(sa), group: sa.boType, attribute: sa };
      });

      this.allSuggestions = this.uniqueBy(ar, (p) => p.label);
    }
  }

  private findBoTypeTranslation(attr: SearchAttribute): string {
    const key = attr.source + "." + attr.boType;
    const retVal = this.allUniqueBoTypeTranslations.find((bt) => bt["key"].localeCompare(key) === 0);
    return retVal === undefined ? attr.boType : retVal["label"];
  }

  private findAttributeTranslation(attr: SearchAttribute): string {
    const key = attr.source + "." + attr.boType + "." + attr.attrName;
    const retVal = this.allAttribueTranslations.find((a) => a["key"].localeCompare(key) === 0);
    return retVal === undefined ? attr.attrName : retVal["label"];
  }

  show(conditions?: Condition[]) {
    this.newSearchFieldsComponent = false;
    this.asyncTranslateBoTypesAndAttributes();

    if (conditions) {
      this.conditions = conditions.map((c) => {
        const clone: Condition = new Condition(
          c.type,
          c.attribute,
          c.operator,
          c.value.slice(),
          c.valueType,
          c.isCurrentlyLinked,
          c.isCreatedViaLink,
        );
        clone.sortIndex = c.sortIndex;
        return clone;
      });
    } else {
      this.conditions = [];
    }
    this.conditionUpdated();
    this.lgEditSearchFieldsModal = true;
    return this.conditionsSubmitted;
  }

  conditionUpdated() {
    const envs = (this.environments || []).map((node) => node.sourceName);
    this.layout = SearchLayout.createSearchLayout(envs, this.searchAttributes, this.conditions);
  }

  nodeSelect(node: TreeNode) {
    if (node && node.type === "attribute") {
      const sa = <SearchAttribute>node.data;
      this.pushCondition(new Condition(sa.boType, sa.attrName, Operator.EQUALS, [], sa.valueType));
      this.conditionUpdated();
    }
  }

  removeCondition(condition: Condition) {
    const index = this.conditions.findIndex((c) => condition.type === c.type && condition.attribute === c.attribute);
    this.conditions.splice(index, 1);
    this.conditionUpdated();
  }

  addSearchFields() {
    this.lgEditSearchFieldsModal = false;
    this.conditionsSubmitted.emit(this.conditions);
  }

  public onSelect(event) {
    this.pushCondition(new Condition(event.attribute.boType, event.attribute.attrName, Operator.EQUALS, [], event.attribute.valueType));
    this.conditionUpdated();
    this.selectedSuggestion = undefined;
  }

  search(event) {
    if (this.caseSensitivity) this.filteredSuggestions = this.allSuggestions.filter((v) => v.label.includes(event.query));
    else this.filteredSuggestions = this.allSuggestions.filter((v) => v.label.toLowerCase().includes(event.query.toLowerCase()));
  }

  pushCondition(condition: Condition) {
    if (this.conditions.find((c) => condition.type === c.type && condition.attribute === c.attribute)) {
      this.notificationService.notifyInfo(this.translateService.instant("search.edit-searchfields.search-field-already-selected"), "Info");
    } else {
      this.conditions.push(condition);
      this.conditionUpdated();
    }
  }

  mapSourceNameToName(sourceName: string) {
    return NodeService.mapSourceNameToName(this.environments, sourceName);
  }

  isLast(col: Condition) {
    const sourceName = this.layout.getSourceName(col);
    if (sourceName) {
      const conditionsInSameSource = this.layout.getConditions(sourceName);
      return conditionsInSameSource.indexOf(col) === conditionsInSameSource.length - 1;
    }
  }

  isFirst(col: Condition) {
    const sourceName = this.layout.getSourceName(col);
    if (sourceName) {
      const conditionsInSameSource = this.layout.getConditions(sourceName);
      return conditionsInSameSource.indexOf(col) === 0;
    }
  }

  moveUp(condition: Condition) {
    if (!this.isFirst(condition)) {
      const sourceName = this.layout.getSourceName(condition);
      if (sourceName) {
        const conditionsInSameSource = this.layout.getConditions(sourceName);

        const oldIndex = conditionsInSameSource.indexOf(condition);
        const otherCondition = conditionsInSameSource[oldIndex - 1];
        this.swap(condition, otherCondition);
      }
    }
  }

  moveDown(condition: Condition) {
    if (!this.isLast(condition)) {
      const sourceName = this.layout.getSourceName(condition);
      if (sourceName) {
        const conditionsInSameSource = this.layout.getConditions(sourceName);

        const oldIndex = conditionsInSameSource.indexOf(condition);
        const otherCondition = conditionsInSameSource[oldIndex + 1];
        this.swap(condition, otherCondition);
      }
    }
  }

  private swap(condition1: Condition, condition2: Condition) {
    const index1 = this.conditions.findIndex((c) => c.type === condition1.type && c.attribute === condition1.attribute);
    const index2 = this.conditions.findIndex((c) => c.type === condition2.type && c.attribute === condition2.attribute);

    const tmp = this.conditions[index1];
    this.conditions[index1] = this.conditions[index2];
    this.conditions[index2] = tmp;
    this.conditionUpdated();
  }

  private uniqueBy<T>(a: T[], key: (T) => any) {
    const seen = {};
    return a.filter(function (item) {
      const k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
  }

  showForNew(): EventEmitter<SearchFilter> {
    this.newSearchFieldsComponent = true;
    this.asyncTranslateBoTypesAndAttributes();

    this.conditions = [];
    this.conditionUpdated();
    this.lgEditSearchFieldsModal = true;
    this.tree.expandRootNodeOnly();
    return this.newFilterSubmitted;
  }

  saveNewFilter(e: Event) {
    if (this.selectedFilterName && this.conditions.length > 0) {
      this.lgEditSearchFieldsModal = false;
      this.newFilterSubmitted.emit(new SearchFilter(this.selectedFilterName, [], "", "", this.conditions));
    }
  }

  onChange(e) {
    this.selectedFilterName = e.value;
  }
}
