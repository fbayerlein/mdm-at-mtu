/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { NavigatorService } from "@navigator/navigator.service";
import { TranslateService } from "@ngx-translate/core";
import { SelectItem } from "primeng/api";
import { AutoComplete } from "primeng/autocomplete";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { Node } from "../navigator/node";
import { QueryService } from "../tableview/query.service";
import { AutoCompleteEditInfo, Condition, Operator, OperatorUtil } from "./filter.service";

@Component({
  selector: "mdm-search-condition",
  templateUrl: "search-condition.component.html",
  styleUrls: ["search-condition.component.css"],
})
export class SearchConditionComponent implements OnChanges, OnInit {
  private readonly multiValueOperators = [Operator.LIKE, Operator.EQUALS, Operator.NOT_EQUALS, Operator.IN];

  @Input() env: string;
  @Input() condition: Condition;
  @Input() form: FormGroup;
  @Input() disabled: boolean;
  @Input() selectedEnvs: Node[];
  @Input() defaultSourceName: string;
  @Output() remove$ = new EventEmitter<Condition>();
  @Output() autoCompleteEdit = new EventEmitter<AutoCompleteEditInfo>();

  suggestions: string[];
  lastQuery: string;

  // date values for two input fields
  dateValue: Date;
  dateValueEnd: Date;

  // string values for two input fields
  stringValueStart = "";
  stringValueEnd = "";

  public isBinaryOperator = false;
  public isHandlingMultipleValues = false;

  public operators: SelectItem[];
  constructor(
    private queryService: QueryService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private datePipe: DatePipe,
    private navigatorService: NavigatorService,
  ) {}

  ngOnInit() {
    this.operators = OperatorUtil.values().map((o) => <SelectItem>{ label: OperatorUtil.toString(o), value: o });

    // set default operator if empty and evaluates operator properties like multiple input, binary eg.
    this.setOperator(this.condition.operator === undefined ? Operator.EQUALS : this.condition.operator);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["condition"]) {
      // set initial date to today, otherwise primeNg calender breaks on undefined input.
      if (this.condition.valueType === "date") {
        if (this.condition.value === undefined || this.condition.value[0] === undefined) {
          this.dateValue = new Date();
          this.dateValue.setHours(0, 0, 0, 0);
        } else {
          this.dateValue = new Date(this.condition.value[0]);
        }
        this.setDateValue(this.dateValue, 0);
        if (this.condition.value === undefined || this.condition.value[1] === undefined) {
          this.dateValueEnd = new Date();
          this.dateValueEnd.setHours(0, 0, 0, 0);
          this.dateValueEnd.setDate(this.dateValueEnd.getDate() + 1);
        } else {
          this.dateValueEnd = new Date(this.condition.value[1]);
        }
      } else if (this.condition.valueType != "string") {
        if (this.condition.value != undefined) {
          if (this.condition.value[0] != undefined) {
            this.stringValueStart = this.condition.value[0];
          }
          if (this.condition.value[1] != undefined) {
            this.stringValueEnd = this.condition.value[1];
          }
        }
      }
    }
  }

  onAutocompleteBlur(event: any, autocomplete: AutoComplete): void {
    const inputValue = event.target.value.trim();

    if (!inputValue) return;

    if (
      (this.lastQuery && !this.condition.value.includes(inputValue) && !this.suggestions.includes(this.lastQuery)) ||
      (!this.lastQuery && this.condition.value.length === 0) ||
      (this.lastQuery &&
        this.condition.value.includes(this.lastQuery) &&
        this.suggestions.includes(this.lastQuery) &&
        this.lastQuery !== inputValue)
    ) {
      this.condition.value.push(inputValue);
    }
    this.lastQuery = inputValue;
    event.target.value = "";
  }

  onEnter(event: any, autocomplete: AutoComplete): void {
    const inputValue = event.target.value.trim();

    if (inputValue && !this.condition.value.includes(inputValue)) {
      this.condition.value.push(inputValue);
    }
    this.lastQuery = inputValue;
    event.target.value = "";
  }

  onSelect(item: any): void {
    if (item && this.suggestions.includes(item) && !this.condition.value.includes(item)) {
      this.condition.value.push(item);
    }

    if (
      this.lastQuery &&
      !this.suggestions.includes(this.lastQuery) &&
      this.condition.value.includes(this.lastQuery) &&
      this.lastQuery !== item
    ) {
      const index = this.condition.value.indexOf(item);
      if (index !== -1) {
        this.condition.value.splice(index - 1, 1);
      }
    }
    this.lastQuery = item;
  }

  onClickAutoComplete(event: Event, autoComplete: AutoComplete) {
    const eventTarget = event.target as Element;

    if (eventTarget.classList.contains("ui-autocomplete-token-label")) {
      const clickedValue = eventTarget.textContent;

      if (Array.isArray(autoComplete.value)) {
        const editIndex = autoComplete.value.findIndex((s) => s === clickedValue);
        if (editIndex > -1) {
          this.autoCompleteEdit.emit(new AutoCompleteEditInfo(autoComplete, editIndex, clickedValue));
        }
      } else if (autoComplete.value !== undefined) {
        this.autoCompleteEdit.emit(new AutoCompleteEditInfo(autoComplete, -1, autoComplete.value.toString()));
      }
    }
  }

  updateSuggestions(e: any) {
    this.queryService
      .suggestValues(
        this.env === "Global" ? this.selectedEnvs.map((env) => env.sourceName) : [this.env],
        this.condition.type,
        this.condition.attribute,
        e.query,
      )
      .subscribe(
        (values) => (this.suggestions = Array.from(new Set<string>(values))),
        (error) =>
          this.notificationService.notifyError(
            this.translateService.instant("search.search-condition.err-cannot-initialize-autocompletion"),
            error,
          ),
      );
  }

  operatorChanged(event: any) {
    this.setOperator(event.value as Operator);
  }

  setOperator(operator: Operator) {
    this.condition.operator = operator;
    this.isBinaryOperator = this.condition.operator === Operator.BETWEEN;
    this.isHandlingMultipleValues = this.multiValueOperators.some((op) => op === this.condition.operator);
    this.adjustInput();
  }

  renameItem(oldValue: string, c: Condition) {
    const newValue = prompt("Enter new value: ", oldValue);
    this.condition.value = this.condition.value.map((v) => (v === oldValue && newValue != null ? newValue : v));
  }

  private adjustInput() {
    if (this.condition.valueType === "string") {
      if (!this.isHandlingMultipleValues) {
        this.stringValueStart = this.condition.value[0];
        this.stringValueEnd = this.condition.value[1];
      } else {
        this.condition.value = this.condition.value.filter((v) => v != undefined);
      }
    } else if (this.condition.valueType === "date") {
      this.setDateValue(this.isBinaryOperator ? this.dateValueEnd : undefined, 1);
    }
  }

  setValue() {
    if (this.stringValueEnd == null || this.stringValueEnd === "") {
      if (this.stringValueStart == null || this.stringValueStart === "") {
        this.condition.value = [];
      } else {
        this.condition.value = [this.stringValueStart];
      }
    } else {
      this.condition.value = [this.stringValueStart, this.stringValueEnd];
    }
  }

  setDateValue(value: Date | undefined, index: number) {
    if (this.condition.value === undefined) {
      this.condition.value = [];
    }
    if (value === undefined) {
      if (index > -1) {
        this.condition.value.splice(index, 1);
      }
    } else {
      this.condition.value[index] = this.datePipe.transform(value, "yyyy-MM-dd" + "T" + "HH:mm:ss");
    }
  }

  remove() {
    this.remove$.emit(this.condition);
  }
}
