/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { MDMDetailModule } from "@details/mdm-detail.module";
import { AccordionModule } from "primeng/accordion";
import { AutoCompleteModule } from "primeng/autocomplete";
import { DialogModule } from "primeng/dialog";
import { InputTextModule } from "primeng/inputtext";
import { PanelModule } from "primeng/panel";
import { ToggleButtonModule } from "primeng/togglebutton";
import { TooltipModule } from "primeng/tooltip";
import { MDMCoreModule } from "../core/mdm-core.module";
import { SearchattributeTreeModule } from "../searchattribute-tree/searchattribute-tree.module";
import { TableViewModule } from "../tableview/tableview.module";
import { EditSearchFieldsComponent } from "./edit-searchFields.component";
import { FilterService } from "./filter.service";
import { MDMSearchComponent } from "./mdm-search.component";
import { TypeIconPipe } from "./pipes/type-icon.pipe";
import { SearchConditionComponent } from "./search-condition.component";
import { SearchService } from "./search.service";

@NgModule({
  imports: [
    MDMCoreModule,
    TableViewModule,
    SearchattributeTreeModule,
    AutoCompleteModule,
    TooltipModule,
    PanelModule,
    ToggleButtonModule,
    InputTextModule,
    AccordionModule,
    DialogModule,
    MDMDetailModule,
  ],
  declarations: [MDMSearchComponent, SearchConditionComponent, EditSearchFieldsComponent, TypeIconPipe],
  exports: [MDMSearchComponent, EditSearchFieldsComponent],
  providers: [SearchService, FilterService, DatePipe, TypeIconPipe],
})
export class MDMSearchModule {}
