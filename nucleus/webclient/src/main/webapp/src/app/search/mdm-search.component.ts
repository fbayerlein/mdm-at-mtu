/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MDMItem } from "@core/mdm-item";
import { PreferenceConstants, PreferenceService, Scope } from "@core/preference.service";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { NavigatorService } from "@navigator/navigator.service";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem, SelectItem } from "primeng/api";
import { combineLatest, of, Subscription } from "rxjs";
import { defaultIfEmpty, map, mergeMap, tap } from "rxjs/operators";
import { BasketService } from "../basket/basket.service";
import { streamTranslate, TRANSLATE } from "../core/mdm-core.module";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { OverwriteDialogComponent } from "../core/overwrite-dialog.component";
import { Node } from "../navigator/node";
import { NodeService } from "../navigator/node.service";
import { QueryService, Row, SearchResult } from "../tableview/query.service";
import { TableviewComponent } from "../tableview/tableview.component";
import { View } from "../tableview/tableview.service";
import { EditSearchFieldsComponent } from "./edit-searchFields.component";
import { AutoCompleteEditInfo, Condition, FilterService, Operator, SearchFilter } from "./filter.service";
import { SearchAttribute, SearchDefaults, SearchDefinition, SearchLayout, SearchService } from "./search.service";

@Component({
  selector: "mdm-search",
  templateUrl: "mdm-search.component.html",
})
export class MDMSearchComponent implements OnInit, OnDestroy, IStatefulComponent {
  maxResults = 1000;

  isCaseSensitive = false;

  filters: SearchFilter[] = [];
  currentFilter: SearchFilter;
  defaultResultType: string;
  dialogSearchFilterDelete = false;
  dialogSearchFilterChanged = false;
  createNewFilter = false;
  tmpSelectedFilter: SearchFilter;
  tmpFilterChanged: SearchFilter;
  tmpFilterDelete: SearchFilter;
  filterName = "";
  public filterModel: SelectItem[] = [];
  public selectedFilter: string;
  labelNoFilter = "-";

  environments: Node[];
  selectedEnvironments: Node[] = [];
  nodeServiceSubscription: Subscription;

  definitions: SearchDefinition[] = [];
  selectedDefinition: string;

  results: SearchResult = new SearchResult();
  allSearchAttributes: {
    [type: string]: { [env: string]: SearchAttribute[] };
  } = {};
  allSearchAttributesForCurrentResultType: {
    [env: string]: SearchAttribute[];
  } = {};

  isAdvancedSearchClosed = false;
  isAdvancedSearchActive = true;
  isSearchResultsClosed = true;

  public lgQuickPlotModal = false;

  layout: SearchLayout = new SearchLayout();

  public dropdownModel: SelectItem[] = [];
  public selectedEnvs: string[] = [];

  searchFields: { group: string; attribute: string }[] = [];

  searchExecuted = false;

  selectedFilterToSave: SearchFilter;
  lazySelectedRow: SearchFilter;
  loading = false;

  public selectedItems: Row[];

  contextMenuItems: MenuItem[] = [
    {
      label: "Add to shopping basket",
      icon: "fa fa-shopping-cart",
      command: (event) => this.addSelectionToBasket(),
    },
  ];

  editingAutoCompleteSuggestion: AutoCompleteEditInfo;
  canEditAutoCompleteSuggestion = false;

  public isSearchLinkedToNavigator: false;
  private linkedToNavigatorSub: Subscription;

  private newSearchFieldsComponent = false;

  hasJsonButton = false;
  searchJson: string;
  childJsonDialog = false;

  selectedView: View;

  @ViewChild(TableviewComponent)
  tableViewComponent: TableviewComponent;

  public lgSaveModal = false;

  @ViewChild(EditSearchFieldsComponent)
  editSearchFieldsComponent: EditSearchFieldsComponent;

  @ViewChild(OverwriteDialogComponent)
  overwriteDialogComponent: OverwriteDialogComponent;

  constructor(
    private searchService: SearchService,
    private queryService: QueryService,
    private filterService: FilterService,
    private nodeService: NodeService,
    private notificationService: MDMNotificationService,
    private basketService: BasketService,
    private translateService: TranslateService,
    private navigatorService: NavigatorService,
    private preferenceService: PreferenceService,
    private stateService: StateService,
    private route: ActivatedRoute,
  ) {
    this.searchService.getDefaults().subscribe((d) => (this.defaultResultType = d.resultType));
    this.stateService.register(this, "search");
  }

  getState(): unknown {
    return {
      isSearchResultsClosed: this.isSearchResultsClosed,
      isAdvancedSearchClosed: this.isAdvancedSearchClosed,
      currentFilter: this.currentFilter,
      searchResult: this.results,
      isAdvancedSearchActive: this.isAdvancedSearchActive,
    };
  }

  takeOverDialogVisible = false;
  takeOverSearchFilter = {} as SearchFilter;
  takeOverNameExists = false;
  blockStateApply = false;

  applyState(state: unknown) {
    if (!this.blockStateApply) {
      this.results = (state["searchResult"] as SearchResult) || new SearchResult();
      this.selectFilter((state["currentFilter"] as SearchFilter) || this.getEmptyFilter());
      this.isAdvancedSearchActive = !("false" === state["isAdvancedSearchActive"]);
      this.isAdvancedSearchClosed = "false" === state["isAdvancedSearchClosed"];
      this.isSearchResultsClosed = "false" === state["isSearchResultsClosed"];

      if (state["takeover-search-filter"]) {
        this.takeOverSearchFilter = state["takeover-search-filter"] as SearchFilter;
        this.takeOverDialogVisible = true;
      }
    }
  }

  takeOver() {
    this.filterService.getFiltersOfCurrentUser().subscribe((filters) => {
      if (filters.findIndex((f) => f.name === this.takeOverSearchFilter.name) >= 0) {
        this.takeOverNameExists = true;
      } else {
        this.takeOverNameExists = false;
        this.filterService.saveFilter(this.takeOverSearchFilter).subscribe(() => {
          this.takeOverDialogVisible = false;
          this.loadFilters(this.takeOverSearchFilter);
        });
      }
    });
  }

  loadSearchFromUrl(): void {
    const filterParam = this.route.snapshot.queryParams["filter"];
    if (filterParam) {
      this.selectFilter(JSON.parse(filterParam));
      this.isAdvancedSearchActive = true;
      this.isAdvancedSearchClosed = false;
      this.isSearchResultsClosed = false;
      console.log(this.selectedView);
      this.selectedView ? this.onSearch() : (this.searchExecuted = true);
      this.blockStateApply = true;
    }
  }

  getStateForTakeOver(): unknown {
    return {
      search: {
        "takeover-search-filter": this.currentFilter,
      },
    };
  }

  quickPlotMetadataTrends(e: Event) {
    this.selectedItems = [];
    if (this.tableViewComponent.selectedViewRows) {
      this.selectedItems = this.results.rows.filter((row: Row) => {
        return (
          this.tableViewComponent.selectedViewRows.findIndex((selectedRow: Row) => {
            return row.id === selectedRow.id;
          }) > -1
        );
      });
    }

    if (this.selectedItems.length > 0) {
      this.lgQuickPlotModal = true;
    }

    e.stopPropagation();
  }

  ngOnInit() {
    streamTranslate(this.translateService, TRANSLATE("search.mdm-search.add-to-shopping-basket")).subscribe(
      (msg: string) => (this.contextMenuItems[0].label = msg),
    );

    this.initNodes();
    this.subscribeToDatasourceChanges();

    this.preferenceService.getPreferenceForScope(Scope.SYSTEM, PreferenceConstants.SEARCH_CASE_SENSITIVE).subscribe((preferences) => {
      if (preferences.length > 0) {
        this.isCaseSensitive = "true" === preferences[0].value;
      }
    });

    // event handlers
    /*
     this.viewComponent.view.subscribe(
       view => this.onViewChanged(view),
       error => this.notificationService.notifyError(this.translateService.instant('search.mdm-search.err-cannot-update-view'), error)
     );*/

    this.preferenceService.getPreferenceForScope(Scope.SYSTEM, "search.button.request-as-json").subscribe((preferences) => {
      if (preferences.length > 0) {
        this.hasJsonButton = preferences[0].value === "true";
      }
    });

    this.nodeService.parentNodeResponseEvent.subscribe((responseNodes) => this.addLInkedConditionForNodeWithParent(responseNodes));
  }

  ngOnDestroy() {
    if (this.linkedToNavigatorSub) {
      this.linkedToNavigatorSub.unsubscribe();
    }
    if (this.nodeServiceSubscription) {
      this.nodeServiceSubscription.unsubscribe;
    }
    this.stateService.unregister(this);
  }

  subscribeToDatasourceChanges() {
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe((n) => this.initNodes());
  }

  initNodes() {
    this.nodeService
      .getNodes()
      .pipe(
        mergeMap((envs) =>
          combineLatest([
            of(envs),
            this.searchService.loadSearchAttributesStructured(envs.map((env) => env.sourceName)).pipe(map((x) => <any>x)), // without the map to any, an error occurs despite the signitures match
            this.filterService.getFiltersOfCurrentUser().pipe(defaultIfEmpty([this.currentFilter])),
            this.searchService.getDefinitionsSimple(),
            this.searchService.getDefaults(),
          ]),
        ),
      )
      .subscribe(
        ([envs, attrs, filters, definitions, defaultResultType]) => {
          this.init(envs, attrs, filters, definitions, defaultResultType);
          this.loadSearchFromUrl();
        }, // TODO
        (error) =>
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-load-data-sources"), error),
      );
  }

  init(
    envs: Node[],
    attrs: { [type: string]: { [env: string]: SearchAttribute[] } },
    filters: SearchFilter[],
    definitions: SearchDefinition[],
    defaultResultType: SearchDefaults,
  ) {
    this.environments = envs;
    this.allSearchAttributes = attrs;
    this.definitions = definitions;

    this.dropdownModel = envs.map((env) => <SelectItem>{ value: env.sourceName, label: env.name });
    this.selectedEnvs = envs.map((env) => env.sourceName);
    this.defaultResultType = defaultResultType.resultType;

    if (this.currentFilter === undefined) {
      this.currentFilter = this.getEmptyFilter();
    }

    if (this.currentFilter !== undefined && this.currentFilter.resultType !== undefined) {
      this.selectedDefinition = this.getSearchDefinition(this.currentFilter.resultType).value;
    } else {
      this.selectedDefinition = this.getSearchDefinition("Test").value;
    }

    this.mapFilterModel(filters);

    if (this.defaultResultType !== undefined && this.currentFilter.name === this.filterService.NO_FILTER_NAME) {
      this.currentFilter.environments = this.selectedEnvs;
    }

    this.updateSearchAttributesForCurrentResultType();
    this.selectedEnvironmentsChanged();
  }

  mapFilterModel(filters: SearchFilter[]) {
    this.filters = filters;
    this.filterModel = this.filters.map(
      (filter) =>
        <SelectItem>{
          value: filter.name,
          label: filter.name,
        },
    );
  }

  onViewClick(e: Event) {
    e.stopPropagation();
  }

  onCheckBoxClick(event: any) {
    event.stopPropagation();
    this.isAdvancedSearchClosed = !event.target.checked;
    this.isAdvancedSearchActive = !this.isAdvancedSearchClosed;
  }

  onViewChanged(view: View) {
    this.selectedView = structuredClone(view);
    if (this.searchExecuted) {
      this.onSearch();
    }
  }

  selectedEnvironmentsChanged() {
    this.currentFilter.environments = this.selectedEnvs;
    if (this.environments) {
      const envs = this.environments.filter((env) => this.currentFilter.environments.find((envName) => envName === env.sourceName));

      if (envs.length === 0) {
        this.selectedEnvironments = this.environments;
      } else {
        this.selectedEnvironments = envs;
      }
    }
    this.calcCurrentSearch();
  }

  loadFilters(selectedFilter: SearchFilter) {
    this.filters = [];
    this.filterService
      .getFiltersOfCurrentUser()
      .pipe()
      .subscribe(
        (filters) => {
          this.mapFilterModel(filters);
          if (selectedFilter != undefined) {
            this.selectFilter(selectedFilter);
          }
        },
        (error) =>
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-load-search-filter"), error),
      );
  }

  selectFilterByName(defaultFilterName: string) {
    this.selectFilter(this.filters.find((f) => f.name === defaultFilterName));
  }

  removeSearchField(searchField: { group: string; attribute: string }) {
    const index = this.searchFields.findIndex((sf) => sf.group === searchField.group && sf.attribute === searchField.attribute);
    this.searchFields.splice(index, 1);
  }

  onResultTypeChanged(event: { value: string; originalEvent: Event }) {
    const searchDef = this.definitions.find((sd) => sd.value === event.value);
    if (searchDef) {
      this.currentFilter.resultType = searchDef.type;
      this.updateSearchAttributesForCurrentResultType();
    }
  }

  updateSearchAttributesForCurrentResultType() {
    if (this.allSearchAttributes.hasOwnProperty(this.getSelectedDefinition())) {
      this.allSearchAttributesForCurrentResultType = this.allSearchAttributes[this.getSelectedDefinition()];
    }
  }

  getSearchDefinition(type: string) {
    return this.definitions.find((def) => def.type === type);
  }

  getSelectedDefinition() {
    const def = this.getSearchDefinition(this.currentFilter.resultType);
    if (def) {
      return def.value;
    }
  }

  onSearch() {
    let query;
    this.loading = true;
    this.isSearchResultsClosed = false;

    if (this.selectedEnvs.length === 0) {
      this.notificationService.notifyInfo(
        this.translateService.instant("search.mdm-search.search-filter-sources"),
        this.translateService.instant("search.mdm-search.search-filter-sources-details"),
      );
    }
    if (this.isAdvancedSearchActive) {
      query = this.searchService.convertToQuery(
        this.currentFilter,
        this.allSearchAttributes,
        this.selectedView,
        true,
        this.isCaseSensitive,
      );
    } else {
      const filter = this.currentFilter; //classToClass
      filter.conditions = [];
      query = this.searchService.convertToQuery(filter, this.allSearchAttributes, this.selectedView, true, this.isCaseSensitive);
    }
    this.queryService
      .query(query)
      .pipe(tap((result) => this.generateWarningsIfMaxResultsReached(result)))
      .subscribe({
        next: (r) => {
          this.results = r;
          this.isSearchResultsClosed = false;
          this.searchExecuted = true;
          this.loading = false;
        },
        error: (e) => {
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-process-search-query"), e);
          this.loading = false;
        },
      });
  }

  generateWarningsIfMaxResultsReached(result: SearchResult) {
    const resultsPerSource = result.rows
      .map((r) => r.source)
      .reduce((prev, item) => {
        prev[item] ? (prev[item] += 1) : (prev[item] = 1);
        return prev;
      }, {});

    Object.keys(resultsPerSource)
      .filter((source) => resultsPerSource[source] > this.maxResults)
      .forEach((source) =>
        this.notificationService.notifyWarn(
          this.translateService.instant("search.mdm-search.errheading-too-many-search-results"),
          this.translateService.instant("search.mdm-search.err-too-many-search-results", { numresults: this.maxResults, source: source }),
        ),
      );
  }

  calcCurrentSearch() {
    const environments = this.currentFilter.environments;
    const conditions = this.currentFilter.conditions;
    this.layout = SearchLayout.createSearchLayout(environments, this.allSearchAttributesForCurrentResultType, conditions);
  }

  onFilterChange(e: any) {
    this.selectFilter(this.filters.find((filter) => e.value === filter.name));
  }

  selectFilter(filter: SearchFilter) {
    if (this.hasFilterChanged()) {
      this.tmpFilterChanged = this.currentFilter;
      this.tmpSelectedFilter = filter;
      this.dialogSearchFilterChanged = true;
    } else {
      this.currentFilter = this.getFilterClone(filter);
      this.selectedFilter = this.currentFilter.name;
      this.selectedEnvs = this.currentFilter.environments;
      this.updateResultType();
      this.updateSearchAttributesForCurrentResultType();
      this.selectedEnvironmentsChanged();
      this.calcCurrentSearch();
    }
  }

  private getFilterClone(filter: SearchFilter): SearchFilter {
    if (filter === undefined) {
      return undefined;
    }
    return new SearchFilter(
      filter.name,
      filter.environments.slice(),
      filter.resultType,
      filter.fulltextQuery,
      filter.conditions.map((c) => {
        const clone: Condition = new Condition(
          c.type,
          c.attribute,
          c.operator,
          c.value.slice(),
          c.valueType,
          c.isCurrentlyLinked,
          c.isCreatedViaLink,
        );
        clone.sortIndex = c.sortIndex;
        return clone;
      }),
    );
  }

  private getEmptyFilter(): SearchFilter {
    const filter = this.getFilterClone(this.filterService.EMPTY_FILTER);
    if (this.defaultResultType !== undefined) {
      filter.resultType = this.defaultResultType;
    }
    filter.environments = this.environments !== undefined ? this.environments.map((env) => env.sourceName) : this.selectedEnvs;
    return filter;
  }

  private updateResultType() {
    const def = this.getSearchDefinition(this.currentFilter.resultType);
    if (def !== undefined && this.selectedDefinition !== def.value) {
      this.selectedDefinition = def.value;
    }
  }

  resetConditions(e: Event) {
    e.stopPropagation();
    this.currentFilter.conditions.forEach((cond) => (cond.value = []));
    this.selectFilter(this.currentFilter);
  }

  removeAllSearchAttributes(e: Event) {
    e.stopPropagation();
    this.currentFilter.conditions.forEach((cond) => (cond.value = []));
    this.currentFilter.conditions.splice(0, this.currentFilter.conditions.length);
    this.selectFilter(this.currentFilter);
  }

  clearResultlist(e: Event) {
    e.stopPropagation();
    this.results = new SearchResult();
  }

  deleteFilter(e: Event) {
    e.stopPropagation();
    if (this.currentFilter.name === this.filterService.NO_FILTER_NAME || this.currentFilter.name === this.filterService.NEW_FILTER_NAME) {
      this.notificationService.notifyInfo(
        this.translateService.instant("search.mdm-search.errheading-cannot-delete-search-filter-none-selected"),
        this.translateService.instant("search.mdm-search.err-cannot-delete-search-filter-none-selected"),
      );
    } else {
      this.tmpFilterDelete = this.currentFilter;
      this.dialogSearchFilterDelete = true;
    }
  }

  cancelRemoveSearchFilter() {
    this.dialogSearchFilterDelete = false;
    this.tmpFilterDelete = undefined;
  }

  confirmedRemoveSearchFilter() {
    if (this.tmpFilterDelete !== undefined) {
      this.layout = new SearchLayout();
      this.filterService.deleteFilter(this.tmpFilterDelete.name).subscribe(
        () => {
          this.cancelRemoveSearchFilter();
          this.loadFilters(this.getEmptyFilter());
        },
        (error) =>
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-delete-search-filter"), error),
      );
      this.cancelRemoveSearchFilter();
    }
  }

  saveFilter(e: Event) {
    e.stopPropagation();
    this.saveFilter1();
  }

  private saveFilter1() {
    if (this.filters.find((f) => f.name === this.filterName) != undefined) {
      this.lgSaveModal = false;
      this.overwriteDialogComponent.showOverwriteModal(this.translateService.instant("search.mdm-search.a-filter")).subscribe(
        (needSave) => this.saveFilter2(needSave),
        (error) => {
          this.saveFilter2(false);
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-save-search-filter"), error);
        },
      );
    } else {
      this.saveFilter2(true);
    }
  }

  private saveFilter2(save: boolean) {
    if (save) {
      const filter = this.currentFilter;
      filter.name = this.filterName;
      this.filterService.saveFilter(filter).subscribe(
        () => {
          this.loadFilters(filter);
        },
        (error) =>
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-save-search-filter"), error),
      );
      this.lgSaveModal = false;
    } else {
      this.lgSaveModal = true;
    }
  }

  removeCondition(condition: Condition) {
    this.currentFilter.conditions = this.currentFilter.conditions.filter(
      (c) => !(c.type === condition.type && c.attribute === condition.attribute),
    );

    this.calcCurrentSearch();
  }

  selected2Basket(e: Event) {
    e.stopPropagation();
    this.basketService.addMany(this.tableViewComponent.selectedViewRows.map((selectedRow: Row) => Row.getItem(selectedRow)));
  }

  showSaveModal(e: Event) {
    e.stopPropagation();
    if (this.currentFilter.name === this.filterService.NO_FILTER_NAME || this.currentFilter.name === this.filterService.NEW_FILTER_NAME) {
      this.filterName = "";
    } else {
      this.filterName = this.currentFilter.name;
    }
    this.lgSaveModal = true;
  }

  public getFiltersForSave(): SearchFilter[] {
    // The NO_FILTER_NAME is not a valid Name for a Filter.
    return this.filters.filter((c) => !(c.name === this.filterService.NO_FILTER_NAME));
  }

  showSearchFieldsEditor(e: Event, conditions?: Condition[]) {
    e.stopPropagation();
    this.newSearchFieldsComponent = false;
    this.editSearchFieldsComponent.show(conditions).subscribe(
      (conds) => {
        this.updateFilterConditions(conds, conditions);
        this.calcCurrentSearch();
      },
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("search.mdm-search.err-cannot-display-search-field-editor"),
          error,
        ),
    );
  }

  private updateFilterConditions(newConds: Condition[], oriConds?: Condition[]) {
    if (!oriConds) {
      const filter = this.getEmptyFilter();
      filter.environments = this.currentFilter.environments;
      filter.conditions = newConds;
      this.selectFilter(filter);
    }
    this.currentFilter.conditions = newConds;
  }

  exitSearchFilterChanged() {
    this.dialogSearchFilterChanged = false;
    this.createNewFilter = false;
    this.selectedFilter = this.currentFilter.name;
  }

  saveChangedFilter() {
    const filter = this.currentFilter;
    this.filterService.saveFilter(filter).subscribe(
      () => {
        this.loadFilters(undefined);
        this.reloadFilter();
      },
      (error) =>
        this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-save-search-filter"), error),
    );
  }

  reloadFilter() {
    this.tmpFilterChanged = undefined;
    this.dialogSearchFilterChanged = false;
    if (this.createNewFilter) {
      this.currentFilter = this.getEmptyFilter();
      this.selectFilter(this.currentFilter);
      this.openSearchFieldsEditor();
    } else {
      this.currentFilter = this.filters.find((filter) => this.currentFilter.name === filter.name);
      this.selectFilter(this.tmpSelectedFilter);
    }
  }

  public getFiltersForNew(): SearchFilter[] {
    return this.newSearchFieldsComponent ? this.getFiltersForSave() : [];
  }

  private hasFilterChanged(): boolean {
    const previousFilter = this.filters.find((filter) => this.currentFilter.name === filter.name);
    return this.currentFilter !== undefined && previousFilter !== undefined && !SearchFilter.equals(this.currentFilter, previousFilter);
  }

  newSearchFieldsEditor(e: Event) {
    e.stopPropagation();
    if (this.hasFilterChanged()) {
      this.createNewFilter = true;
      this.tmpFilterChanged = this.currentFilter;
      this.dialogSearchFilterChanged = true;
    } else {
      this.openSearchFieldsEditor();
    }
  }

  public openSearchFieldsEditor() {
    this.newSearchFieldsComponent = true;

    this.editSearchFieldsComponent.showForNew().subscribe(
      (filter) => {
        this.filterName = filter.name;
        this.updateFilterConditions(filter.conditions);
        this.saveFilter1();
      },
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("search.mdm-search.err-cannot-display-search-field-editor"),
          error,
        ),
    );
  }

  addSelectionToBasket() {
    this.basketService.add(Row.getItem(this.tableViewComponent.menuSelectedRow));
  }

  mapSourceNameToName(sourceName: string) {
    return NodeService.mapSourceNameToName(this.environments, sourceName);
  }

  getSaveFilterBtnTitle() {
    return this.filterName ? TRANSLATE("search.mdm-search.tooltip-save-search-filter") : TRANSLATE("search.mdm-search.tooltip-no-name-set");
  }

  getAdvancedSearchCbxTitle() {
    return this.isAdvancedSearchActive
      ? TRANSLATE("search.mdm-search.tooltip-disable-advanced-search")
      : TRANSLATE("search.mdm-search.tooltip-enable-advanced-search");
  }

  onRowSelect(e: any) {
    this.selectedFilterToSave = e.data;
    this.filterName = e.data.name;
  }

  onRowUnselect() {
    this.selectedFilterToSave = undefined;
    this.filterName = "";
  }

  public openJsonSearch() {
    this.searchJson = "";

    let query;
    if (this.isAdvancedSearchActive) {
      query = this.searchService.convertToQuery(
        this.currentFilter,
        this.allSearchAttributes,
        this.selectedView,
        true,
        this.isCaseSensitive,
      );
    } else {
      const filter = this.currentFilter; // classToClass
      filter.conditions = [];
      query = this.searchService.convertToQuery(filter, this.allSearchAttributes, this.selectedView, true, this.isCaseSensitive);
    }

    this.searchJson = JSON.stringify(query);

    this.childJsonDialog = true;
  }

  public onToggleSearchLinkedToNavigator(event: { checked: boolean; originalEvent: MouseEvent }) {
    // prevent advanced-search-tab to toggle.
    event.originalEvent.stopPropagation();
    if (event.checked) {
      const sub = this.navigatorService
        .onSelectionChanged()
        .pipe(tap((obj) => this.handleSelectedTreeNodeChanged(obj)))
        .subscribe();
      this.linkedToNavigatorSub = sub;
    } else {
      this.linkedToNavigatorSub.unsubscribe();
      this.unlinkConditions();
    }
  }

  onEditAutoCompleteSuggestion(editInfo: AutoCompleteEditInfo) {
    this.editingAutoCompleteSuggestion = editInfo;
    this.canEditAutoCompleteSuggestion = this.editingAutoCompleteSuggestion !== undefined;
  }

  cancelDialogAutoCompleteSuggestion() {
    this.canEditAutoCompleteSuggestion = false;
    this.editingAutoCompleteSuggestion = undefined;
  }

  hideDialogAutoCompleteSuggestion() {
    if (this.editingAutoCompleteSuggestion !== undefined) {
      this.cancelDialogAutoCompleteSuggestion();
    }
  }

  setInputFocus(inputId: string) {
    const input: HTMLInputElement = document.getElementById(inputId) as HTMLInputElement;
    setTimeout(() => {
      input.focus();
    }, 250);
  }

  saveAutoCompleteSuggestion() {
    if (this.editingAutoCompleteSuggestion !== undefined) {
      if (this.editingAutoCompleteSuggestion.suggestionIndex > -1) {
        (this.editingAutoCompleteSuggestion.autoComplete.value as Array<any>)[this.editingAutoCompleteSuggestion.suggestionIndex] =
          this.editingAutoCompleteSuggestion.suggestionValue;
      } else {
        this.editingAutoCompleteSuggestion.autoComplete.value = this.editingAutoCompleteSuggestion.suggestionValue;
      }
      this.editingAutoCompleteSuggestion.autoComplete.updateInputField();
      this.cancelDialogAutoCompleteSuggestion();
    }
  }

  private handleSelectedTreeNodeChanged(object: Node | MDMItem) {
    // unlink other conditions (the ones that where not created via link but got linked)
    this.unlinkConditions();
    if (object != undefined) {
      // remove other conditions created via link
      this.currentFilter.conditions = this.currentFilter.conditions.filter((cond) => !cond.isCreatedViaLink);
      // add conditions and recalculate search
      this.addLinkedConditions(object)
        .pipe(tap(() => this.calcCurrentSearch()))
        .subscribe();
    }
  }

  /**
   * Links the related condition or create linked condition if none exists.
   * @param object is Node if selected TreeNode is real node, is MDMItem if selected TreeNode is virtual node.
   */
  private addLinkedConditions(object: Node | MDMItem) {
    let response = of([]);
    if (object.type !== "Environment") {
      if (object instanceof Node) {
        this.addLinkedConditionForNode(object);
      } else if (object.filter) {
        response = this.addLinkedConditionsforVirtualNode(object);
      }
    }
    return response;
  }

  /**
   * Converts Item filter into Conditions via REST-Service. The Conditions are set to the
   * linked condition or added as new if none exists
   * @param item the virtual Node
   */
  private addLinkedConditionsforVirtualNode(item: MDMItem) {
    this.assureSelectedEnvironment(item.source);
    return this.searchService.convertToCondition(item.source, item.serial).pipe(
      tap((conditions) =>
        conditions.forEach((condition) => {
          condition.isCurrentlyLinked = true;
          const linkedIndex = this.currentFilter.conditions.findIndex(
            (cond) => cond.type === condition.type && cond.attribute === condition.attribute,
          );
          if (linkedIndex > -1) {
            this.currentFilter.conditions.splice(linkedIndex, 1, condition);
          } else {
            condition.isCreatedViaLink = true;
            this.currentFilter.conditions.push(condition);
          }
        }),
      ),
    );
  }

  private addLInkedConditionForNodeWithParent(nodes: Node[]) {
    nodes.forEach((node) => {
      if (node) {
        const idAttribute = node.idAttribute !== undefined ? node.idAttribute : "Id";
        const linkedCondition = this.currentFilter.conditions.find((cond) => cond.type === node.type && cond.attribute === idAttribute);
        if (linkedCondition) {
          linkedCondition.value = [node.id];
          linkedCondition.isCurrentlyLinked = true;
        } else {
          let label = node.label;
          if (node[this.lowerCaseFirst(idAttribute)]) {
            label = node[this.lowerCaseFirst(idAttribute)];
          }
          this.currentFilter.conditions.push(new Condition(node.type, idAttribute, Operator.EQUALS, [label], "string", true, true));
        }
      }
    });
  }

  /**
   * Links the related condition or create linked condition if none exists.
   * @param node  the Node
   */
  private addLinkedConditionForNode(node: Node) {
    this.assureSelectedEnvironment(node.sourceName);

    if (node.idAttribute && node.idAttribute !== "Id") {
      this.nodeService.parentNodeRequestEvent.emit(node);
    } else {
      const idAttribute = node.idAttribute !== undefined ? node.idAttribute : "Id";
      const linkedCondition = this.currentFilter.conditions.find((cond) => cond.type === node.type && cond.attribute === idAttribute);
      if (linkedCondition) {
        linkedCondition.value = [node.id];
        linkedCondition.isCurrentlyLinked = true;
      } else {
        this.currentFilter.conditions.push(
          new Condition(node.type, idAttribute, Operator.EQUALS, [node[this.lowerCaseFirst(idAttribute)]], "string", true, true),
        );
      }
    }
  }

  private assureSelectedEnvironment(sourceName: string) {
    const index = this.selectedEnvs.findIndex((src) => src === sourceName);

    if (index < 0) {
      const matchingEnv: Node = this.environments.find((env) => env.sourceName === sourceName);

      if (matchingEnv !== undefined) {
        const idx = this.selectedEnvironments.findIndex((env) => env.sourceName === sourceName);
        if (idx < 0) {
          this.selectedEnvironments.push(matchingEnv);
        }
        this.selectedEnvs.push(sourceName);
        // touching the model, will cause the layout component to refresh
        // otherwise, selection is changed, but not shown in the UI
        this.dropdownModel = this.dropdownModel.slice();
      }
    }
  }

  private lowerCaseFirst(str: string) {
    return str.substr(0, 1).toLowerCase() + str.substr(1, str.length);
  }

  private unlinkConditions() {
    this.currentFilter.conditions.forEach((cond) => (cond.isCurrentlyLinked = false));
  }
}
