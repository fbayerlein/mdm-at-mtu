/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of as observableOf } from "rxjs";
import { FilterService, Operator, OperatorUtil, SearchFilter } from "./filter.service";

class TestPreferenceService {
  getPreferenceForScope(scope: string, key?: string): Observable<Preference[]> {
    return observableOf([
      {
        id: 1,
        key: "filter.nodes.test",
        scope: Scope.USER,
        source: null,
        user: "testUser",
        value: '{"conditions":[],"name":"TestFilter","environments":[],"resultType":"Test","fulltextQuery":""}',
      },
    ]);
  }
}

class TestTranslateService {}

describe("OperatorUtil", () => {
  describe("toString()", () => {
    it("should return associated string", () => {
      expect(OperatorUtil.toString(Operator.EQUALS)).toMatch("=");
      expect(OperatorUtil.toString(Operator.LESS_THAN)).toMatch("<");
      expect(OperatorUtil.toString(Operator.GREATER_THAN)).toMatch(">");
      expect(OperatorUtil.toString(Operator.LIKE)).toMatch("like");
    });
  });

  describe("toFilterString()", () => {
    it("should return associated filterstring", () => {
      expect(OperatorUtil.toFilterString(Operator.EQUALS)).toMatch("eq");
      expect(OperatorUtil.toFilterString(Operator.LESS_THAN)).toMatch("lt");
      expect(OperatorUtil.toFilterString(Operator.GREATER_THAN)).toMatch("gt");
      expect(OperatorUtil.toFilterString(Operator.LIKE)).toMatch("lk");
    });
  });
});

describe("FilterService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        FilterService,
        PropertyService,
        {
          provide: TranslateService,
          useClass: TestTranslateService,
        },
        {
          provide: PreferenceService,
          useClass: TestPreferenceService,
        },
      ],
    });
  });

  describe("getFiltersOfAllUsers()", () => {
    it("should return array of filters from preference", (done: DoneFn) => {
      const service = TestBed.inject(FilterService);
      const filters = JSON.parse(JSON.stringify([new SearchFilter("TestFilter", [], "Test", "", [])]));

      service.getFiltersOfAllUsers().subscribe((f) => {
        expect(f).toEqual(filters);
        done();
      });
    });
  });

  describe("filterToPreference()", () => {
    it("should return preference holding input filter", async(
      inject([FilterService], (service) => {
        const filter = new SearchFilter("TestFilter", [], "Test", "", []);
        const pref = service.filterToPreference(filter);

        expect(pref.scope).toEqual(Scope.USER);
        expect(pref.key).toMatch("filter.nodes.TestFilter");
        expect(pref.value).toEqual('{"conditions":[],"name":"TestFilter","environments":[],"resultType":"Test","fulltextQuery":""}');
      }),
    ));

    it("should return preference holding input filter", async(
      inject([FilterService], (service) => {
        const pref = service.filterToPreference(undefined);

        expect(pref.scope).toEqual(Scope.USER);
        expect(pref.key).toMatch("filter.nodes.");
        expect(pref.value).toEqual(undefined);
      }),
    ));
  });
});
