/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { TRANSLATE } from "@core/mdm-core.module";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { TranslateService } from "@ngx-translate/core";
import { AutoComplete } from "primeng/autocomplete";
import { map } from "rxjs/operators";

export enum Operator {
  EQUALS,
  NOT_EQUALS,
  LESS_THAN,
  GREATER_THAN,
  LIKE,
  GREATER_THAN_OR_EQUAL,
  LESS_THAN_OR_EQUAL,
  BETWEEN,
  IN,
}

export namespace OperatorUtil {
  export function values() {
    return Object.keys(Operator)
      .filter((k) => isNaN(+k))
      .map((key) => Operator[key] as Operator);
  }
  export function toString(operator: Operator) {
    switch (operator) {
      case Operator.EQUALS:
        return "=";
      case Operator.NOT_EQUALS:
        return "!=";
      case Operator.LESS_THAN:
        return "<";
      case Operator.GREATER_THAN:
        return ">";
      case Operator.LIKE:
        return "like";
      case Operator.GREATER_THAN_OR_EQUAL:
        return "≥";
      case Operator.LESS_THAN_OR_EQUAL:
        return "≤";
      case Operator.BETWEEN:
        return "between";
      case Operator.IN:
        return "in";
      default:
        return undefined;
    }
  }
  export function toFilterString(operator: Operator) {
    switch (operator) {
      case Operator.EQUALS:
        return "eq";
      case Operator.NOT_EQUALS:
        return "ne";
      case Operator.LESS_THAN:
        return "lt";
      case Operator.GREATER_THAN:
        return "gt";
      case Operator.LIKE:
        return "lk";
      case Operator.GREATER_THAN_OR_EQUAL:
        return "ge";
      case Operator.LESS_THAN_OR_EQUAL:
        return "le";
      case Operator.BETWEEN:
        return "bw";
      case Operator.IN:
        return "in";
      default:
        return undefined;
    }
  }
}

export class Condition {
  type: string;
  attribute: string;
  operator: Operator;
  value: string[];
  valueType: string;

  isCurrentlyLinked: boolean;
  isCreatedViaLink: boolean;
  sortIndex: number;

  constructor(
    type: string,
    attribute: string,
    operator: Operator,
    value: string[],
    valueType = "string",
    isCurrentlyLinked = false,
    isCreatedViaLink = false,
  ) {
    this.type = type;
    this.attribute = attribute;
    this.operator = operator;
    this.value = value;
    this.valueType = valueType.toLowerCase();
    this.isCreatedViaLink = isCreatedViaLink;
    this.isCurrentlyLinked = isCurrentlyLinked;
  }

  public static deserialize(condition: Condition, obj: any) {
    Object.assign(condition, obj);
    condition.operator = Operator[obj.operator as keyof typeof Operator];
    condition.valueType = obj.valueType.toLowerCase();
    // reset values
    condition.isCurrentlyLinked = false;
    condition.isCreatedViaLink = false;
    condition.sortIndex = 0;
    return condition;
  }
}

export class SearchFilter {
  name: string;
  environments: string[];
  resultType: string;
  fulltextQuery: string;
  conditions: Condition[] = [];

  constructor(name: string, environments: string[], resultType: string, fulltextQuery: string, conditions: Condition[]) {
    this.name = name;
    this.environments = environments;
    this.resultType = resultType;
    this.fulltextQuery = fulltextQuery;
    this.conditions = conditions;
  }

  public static equals(filter1: SearchFilter, filter2: SearchFilter) {
    return (
      filter1 !== undefined &&
      filter2 != undefined &&
      filter1.fulltextQuery.localeCompare(filter2.fulltextQuery) === 0 &&
      filter1.resultType.localeCompare(filter2.resultType) === 0 &&
      filter1.environments.length === filter2.environments.length &&
      SearchFilter.compareEnv(filter1.environments, filter2.environments) &&
      filter1.conditions.length === filter2.conditions.length &&
      SearchFilter.compareConditions(filter1.conditions, filter2.conditions)
    );
  }

  private static compareEnv(envs1: string[], envs2: string[]) {
    let result = true;
    envs2.forEach((env) => {
      if (envs1.find((envi) => envi === env) === undefined) {
        result = false;
      }
    });
    return result;
  }

  private static compareConditions(conditions1: Condition[], conditions2: Condition[]) {
    let result = true;
    conditions2.forEach((cond) => {
      const condition = conditions1.find((condi) => condi.type === cond.type && condi.attribute === cond.attribute);
      if (condition === undefined || !SearchFilter.equalsCondition(condition, cond)) {
        result = false;
      }
    });
    return result;
  }

  private static equalsCondition(cond1: Condition, cond2: Condition) {
    return (
      cond1.type === cond2.type &&
      cond1.attribute === cond2.attribute &&
      cond1.operator === cond2.operator &&
      cond1.value.length === cond2.value.length &&
      SearchFilter.compareValues(cond1.value, cond2.value)
    );
  }

  private static compareValues(values: string[], values2: string[]) {
    const result = true;
    values2.forEach((value) => {
      if (values.find((val) => val === value) === undefined) {
        return result;
      }
    });
    return result;
  }
}

export class AutoCompleteEditInfo {
  autoComplete: AutoComplete;
  suggestionIndex: number;
  suggestionValue: string;

  constructor(autoComplete: AutoComplete, suggestionIndex: number, suggestionValue: string) {
    this.autoComplete = autoComplete;
    this.suggestionIndex = suggestionIndex;
    this.suggestionValue = suggestionValue;
  }
}

@Injectable()
export class FilterService {
  public readonly NO_FILTER_NAME = TRANSLATE("search.filter.no-filter-selected");
  public readonly NEW_FILTER_NAME = TRANSLATE("search.filter.new-filter");
  public readonly EMPTY_FILTER: SearchFilter = new SearchFilter("", [], "Test", "", []);

  constructor(private preferenceService: PreferenceService, private translateService: TranslateService) {}

  getFiltersOfAllUsers() {
    return this.preferenceService
      .getPreferenceForScope(Scope.USER, "filter.nodes.")
      .pipe(map((preferences) => preferences.map((p) => JSON.parse(p.value) as SearchFilter)));
  }

  getFiltersOfCurrentUser() {
    return this.preferenceService
      .getPreference("filter.nodes.")
      .pipe(map((preferences) => preferences.map((p) => JSON.parse(p.value) as SearchFilter)));
  }

  saveFilter(filter: SearchFilter) {
    return this.preferenceService.savePreference(this.filterToPreference(filter));
  }

  private filterToPreference(filter: SearchFilter) {
    const pref = new Preference();
    pref.value = JSON.stringify(filter);
    pref.key = filter ? "filter.nodes." + filter.name : "filter.nodes.";
    pref.scope = Scope.USER;
    return pref;
  }

  deleteFilter(name: string) {
    return this.preferenceService.deletePreferenceByScopeAndKey(Scope.USER, "filter.nodes." + name);
  }
}
