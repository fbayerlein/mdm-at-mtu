/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Pipe, PipeTransform } from "@angular/core";
import { SearchDefinition } from "../search.service";

type nodeType = "Environment" | "Pool" | "Project" | "Test" | "TestStep" | "Measurement" | "ChannelGroup" | "Channel" | "Quantity" | "Unit";

@Pipe({
  name: "typeIcon",
})
export class TypeIconPipe implements PipeTransform {
  transform(value: nodeType | SearchDefinition): any {
    let type: string;
    if (value instanceof SearchDefinition) {
      type = value.type;
    } else {
      type = value;
    }
    let imagePath = "assets/famfamfam_icons/";

    switch (type as nodeType) {
      case "Environment":
        imagePath += "database.png";
        break;
      case "Project":
        imagePath += "house.png";
        break;
      case "Pool":
        imagePath += "paste_plain.png";
        break;
      case "Test":
        imagePath += "brick_add.png";
        break;
      case "TestStep":
        imagePath += "brick.png";
        break;
      case "Measurement":
        imagePath += "chart_curve.png";
        break;
      case "ChannelGroup":
        imagePath += "calendar.png";
        break;
      case "Channel":
      case "Unit":
      case "Quantity":
        imagePath += "chart_curve_go.png";
        break;
    }
    return imagePath;
  }
}
