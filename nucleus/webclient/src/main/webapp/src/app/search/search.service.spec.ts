/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule, HttpTestingController, RequestMatch } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { LocalizationService } from "@localization/localization.service";
import { NodeService } from "@navigator/node.service";
import { TranslateModule } from "@ngx-translate/core";
import { Observable, of as observableOf } from "rxjs";
import { QueryService } from "../tableview/query.service";
import { Condition, Operator } from "./filter.service";
import { SearchAttribute, SearchLayout, SearchService } from "./search.service";

class TestPreferenceService {
  getPreference(key?: string): Observable<Preference[]> {
    return observableOf([
      {
        id: 1,
        key: "ignoredAttributes",
        scope: Scope.USER,
        source: null,
        user: "testUser",
        value: '["*.Name", "TestStep.Id", "Measurement.*"]',
        // value: '[\"*.MimeType\", \"TestStep.Sortindex\"]'
      },
    ]);
  }
}

describe("SearchService", () => {
  let httpTestingController: HttpTestingController;

  const reqGetMatch: RequestMatch = { method: "GET" };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        LocalizationService,
        PropertyService,
        {
          provide: PreferenceService,
          useClass: TestPreferenceService,
        },
        SearchService,
        MDMNotificationService,
        NodeService,
        QueryService,
      ],
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe("groupByEnv()", () => {
    it("should group attributes by environment", () => {
      const attributes = [
        new SearchAttribute("env1", "Test", "Name"),
        new SearchAttribute("env1", "Vehicle", "Name"),
        new SearchAttribute("env2", "Test", "Name"),
        new SearchAttribute("env2", "Uut", "Name"),
      ];

      const conditionsPerEnv = SearchLayout.groupByEnv(attributes);

      expect(Object.getOwnPropertyNames(conditionsPerEnv)).toContain("env1", "env2");

      expect(conditionsPerEnv["env1"]).toContain(attributes[0], attributes[1]);

      expect(conditionsPerEnv["env2"]).toContain(attributes[2], attributes[3]);
    });
  });

  describe("createSearchLayout()", () => {
    it("should create a searchLayout object from conditions", () => {
      const cond1 = new Condition("Test", "Name", Operator.EQUALS, []);
      const cond2 = new Condition("Vehicle", "Name", Operator.EQUALS, []);

      const attributes = {
        env1: [new SearchAttribute("env1", "Test", "Name"), new SearchAttribute("env1", "Vehicle", "Name")],
        env2: [new SearchAttribute("env2", "Test", "Name"), new SearchAttribute("env2", "Uut", "Name")],
      };

      const searchLayout = SearchLayout.createSearchLayout(["env1", "env2"], attributes, [cond1, cond2]);

      expect(searchLayout.getSourceNames()).toEqual(["Global", "env1"]);

      expect(searchLayout.getConditions("Global")).toEqual([cond1]);

      expect(searchLayout.getConditions("env1")).toEqual([cond2]);
    });
  });

  describe("convert()", () => {
    it("should convert conditions to filter string", async(
      inject([SearchService], (service) => {
        const cond1 = new Condition("Test", "Name", Operator.LIKE, ["PBN*"]);
        const cond2 = new Condition("Vehicle", "Number", Operator.EQUALS, ["12"]);
        const cond3 = new Condition("Vehicle", "Created", Operator.EQUALS, ["2017-07-17T12:13:14"]);

        const attributes = [
          new SearchAttribute("env1", "Test", "Name"),
          new SearchAttribute("env1", "Vehicle", "Number", "LONG"),
          new SearchAttribute("env2", "Test", "Name"),
          new SearchAttribute("env2", "Uut", "Name"),
          new SearchAttribute("env2", "Vehicle", "Created", "DATE"),
        ];

        const filter = service.convertEnv("env1", [cond1, cond2, cond3], attributes, "test", true, false);

        expect(filter.sourceName).toEqual("env1");
        // eslint-disable-next-line max-len
        expect(filter.filter).toEqual(
          '( Test.Name ci_lk "PBN*" ) and ( Vehicle.Number eq 12 ) and ( Vehicle.Created eq "2017-07-17T10:13:14" )',
        );

        expect(filter.searchString).toEqual("test");
        const req = httpTestingController.match(reqGetMatch);
        req.forEach((r) =>
          r.flush({
            type: "Environment",
            data: [
              {
                name: "MDM-NVH",
                id: "1",
                type: "Environment",
                sourceType: "Environment",
                sourceName: "MTU_TEST",
                attributes: [
                  {
                    name: "Timezone",
                    value: "GMT",
                    unit: "",
                    dataType: "STRING",
                    enumName: null,
                  },
                ],
              },
            ],
          }),
        );
      }),
    ));
  });

  describe("loadSearchAttributes()", () => {
    it("should return filtered search attributes for env", async(
      inject([SearchService], (searchService: SearchService) => {
        const mockResponse = {
          data: [
            new SearchAttribute("TestEnv", "Test", "Name", "STRING", "*"),
            new SearchAttribute("TestEnv", "Test", "Id", "LONG", "*"),
            new SearchAttribute("TestEnv", "TestStep", "Name", "STRING", "*"),
            new SearchAttribute("TestEnv", "TestStep", "MimeType", "STRING", "*"),
            new SearchAttribute("TestEnv", "TestStep", "Id", "LONG", "*"),
            new SearchAttribute("TestEnv", "Measurement", "Name", "STRING", "*"),
            new SearchAttribute("TestEnv", "Measurement", "Id", "LONG", "*"),
          ],
        };
        const ans = [
          new SearchAttribute("TestEnv", "Test", "Id", "LONG", "*"),
          new SearchAttribute("TestEnv", "TestStep", "MimeType", "STRING", "*"),
        ];
        searchService.loadSearchAttributes("", "TestEnv").subscribe((sas) => {
          expect(sas).toEqual(ans);
        });

        const req = httpTestingController.match(reqGetMatch);
        req.forEach((r) => r.flush(mockResponse));
      }),
    ));
  });

  describe("getFilters()", () => {
    it("should retrun ignoredAttributes in a string array", async(
      inject([SearchService], (searchService) => {
        expect(searchService.getFilters(undefined)).toEqual(["*.Name", "TestStep.Id", "Measurement.*"]);
      }),
    ));
  });

  describe("filterIgnoredAttributes", () => {
    it("should return searchAttributes without the ignored ones", async(
      inject([SearchService], (searchService) => {
        const input = [
          new SearchAttribute("TestEnv", "Test", "Name", "STRING"),
          new SearchAttribute("TestEnv", "Test", "MimeType", "STRING"),
          new SearchAttribute("TestEnv", "Test", "Id", "LONG"),
          new SearchAttribute("TestEnv", "TestStep", "Name", "STRING"),
          new SearchAttribute("TestEnv", "TestStep", "MimeType", "STRING"),
          new SearchAttribute("TestEnv", "TestStep", "Id", "LONG"),
          new SearchAttribute("TestEnv", "Measurement", "Name", "STRING"),
          new SearchAttribute("TestEnv", "Measurement", "MimeType", "STRING"),
          new SearchAttribute("TestEnv", "Measurement", "Id", "LONG"),
        ];

        const ans = [
          new SearchAttribute("TestEnv", "Test", "MimeType", "STRING"),
          new SearchAttribute("TestEnv", "Test", "Id", "LONG"),
          new SearchAttribute("TestEnv", "TestStep", "MimeType", "STRING"),
        ];

        expect(searchService.filterIgnoredAttributes(undefined, input)).toEqual(ans);
      }),
    ));
  });

  describe("getSearchAttributes()", () => {
    it("should return filtered search attributes for env", async(
      inject([SearchService], (searchService) => {
        const mockResponse = (env) => {
          return {
            data: [
              new SearchAttribute(env, "Test", "Name", "STRING", "*"),
              new SearchAttribute(env, "Test", "Id", "LONG", "*"),
              new SearchAttribute(env, "TestStep", "Name", "STRING", "*"),
              new SearchAttribute(env, "TestStep", "MimeType", "STRING", "*"),
              new SearchAttribute(env, "TestStep", "Id", "LONG", "*"),
              new SearchAttribute(env, "Measurement", "Name", "STRING", "*"),
              new SearchAttribute(env, "Measurement", "Id", "LONG", "*"),
            ],
          };
        };

        const ans = [
          new SearchAttribute("TestEnv", "Test", "Id", "LONG", "*"),
          new SearchAttribute("TestEnv", "TestStep", "MimeType", "STRING", "*"),
          new SearchAttribute("TestEnv2", "Test", "Id", "LONG", "*"),
          new SearchAttribute("TestEnv2", "TestStep", "MimeType", "STRING", "*"),
        ];

        searchService.getSearchAttributesPerEnvs(["TestEnv", "TestEnv2"], "").subscribe((sas) => expect(sas).toEqual(ans));

        const req = httpTestingController.match(reqGetMatch);
        expect(req.length).toEqual(2);
        req[0].flush(mockResponse("TestEnv"));
        req[1].flush(mockResponse("TestEnv2"));
      }),
    ));
  });
});
