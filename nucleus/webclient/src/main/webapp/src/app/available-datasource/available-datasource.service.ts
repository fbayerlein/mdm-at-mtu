/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { catchError } from "rxjs/operators";
import { PropertyService } from "../core/property.service";

export class ServiceConfiguration {
  contextFactoryClass: string;
  connectionParameters: Map<string, string>;
}

@Injectable({
  providedIn: "root",
})
export class AvailableDatasourceService {
  readonly loginURL: string;
  readonly currentURL: string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.currentURL = _prop.getUrl("mdm/datasources/");
  }

  getAvailableDatasource() {
    return this.http
      .get<string[]>(this.currentURL)
      .pipe(catchError((e) => addErrorDescription(e, "Could not request available datasources!")));
  }

  setDatasource(activeDatasources: string[]) {
    return this.http
      .put<string[]>(this.currentURL, activeDatasources)
      .pipe(catchError((e) => addErrorDescription(e, "Could not activate datasources!")));
  }
}
