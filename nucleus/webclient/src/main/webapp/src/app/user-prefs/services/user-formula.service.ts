/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { forkJoin, Observable } from "rxjs";
import { concatMap, map, take } from "rxjs/operators";
import { AuthenticationService } from "../../core/authentication/authentication.service";
import { FormulaPreference, UserFormulaBaseService } from "./user-formula-base.service";

export {
  FilteredFormulaPreference,
  FormulaPreference,
  FormulaValidationResult,
  TYPE_VIRTUAL_CHANNEL,
  VirtualChannelUnitUI,
} from "./user-formula-base.service";

@Injectable()
export class UserFormulaService extends UserFormulaBaseService {
  constructor(private preferenceService: PreferenceService, private authService: AuthenticationService) {
    super();
  }

  /**
   * read user's formula preferences from USER scope
   * @returns response
   */
  public getUserFormulaPreferences(): Observable<FormulaPreference[]> {
    return forkJoin([
      this.authService.getCurrentUserName().pipe(take(1)),
      this.preferenceService.getPreferenceForScope(Scope.USER, this.FORMULA_PREF_KEY),
    ]).pipe(
      map(([username, preferences]) =>
        this.preferenceToFormulaPref(preferences.filter((pref) => pref.user === undefined || pref.user === "" || pref.user === username)),
      ),
    );
  }

  /**
   * save user's formula preferences in USER scope
   * @returns response
   */
  public saveFormulaPreferences(userFormulaPrefs: FormulaPreference[]) {
    return this.authService.getCurrentUserName().pipe(
      take(1),
      concatMap((username) => this.preferenceService.savePreference(this.formulaPreferenceToPreference(userFormulaPrefs, username))),
    );
  }

  private preferenceToFormulaPref(prefs: Preference[]): FormulaPreference[] {
    return prefs.map((p) => Object.assign(new FormulaPreference("", ""), JSON.parse(p.value)[0])).filter((ufp) => this.assureValid(ufp));
  }

  private formulaPreferenceToPreference(userFormulaPrefs: FormulaPreference[], username: string) {
    const pref = new Preference();
    pref.value = JSON.stringify(userFormulaPrefs);
    pref.key = this.FORMULA_PREF_KEY;
    pref.scope = Scope.USER;
    pref.user = username;
    return pref;
  }
}
