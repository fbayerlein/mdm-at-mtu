/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component } from "@angular/core";
import { streamTranslate, TRANSLATE } from "@core/mdm-core.module";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";

@Component({
  selector: "mdm-user-prefs-nav-card",
  templateUrl: "user-prefs-nav-card.component.html",
  styleUrls: ["user-prefs-nav-card.component.css"],
})
export class UserPrefsNavCardComponent {
  environments: Node[];

  initSubcription: Subscription;

  links = [];

  public constructor(
    private nodeService: NodeService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {
    this.initSubcription = this.nodeService.getRootNodes().subscribe(
      (envs) => this.setEnvironments(envs),
      (error) =>
        this.translateService
          .instant("user-prefs.unit-prefs.err-cannot-load-data-sources")
          .subscribe((msg) => this.notificationService.notifyError(msg, error)),
    );

    streamTranslate(this.translateService, [
      TRANSLATE("user-prefs.links.channel-filter"),
      TRANSLATE("user-prefs.links.user-formula"),
      TRANSLATE("user-prefs.links.unit-list"),
      TRANSLATE("user-prefs.links.api-token"),
      TRANSLATE("user-prefs.links.user-settings"),
    ]).subscribe((msg: string) => {
      this.links = [
        { label: msg["user-prefs.links.channel-filter"], routerLink: "channel-filter" },
        { label: msg["user-prefs.links.user-formula"], routerLink: "user-formula" },
        { label: msg["user-prefs.links.unit-list"], routerLink: "unit-mapping" },
        { label: msg["user-prefs.links.api-token"], routerLink: "api-token" },
        { label: msg["user-prefs.links.user-settings"], routerLink: "user-settings" },
      ];
    });
  }

  private createGlobalNode(): Node {
    const retVal: Node = new Node();

    retVal.id = "*";
    retVal.name = "Global";
    retVal.sourceName = retVal.id;
    retVal.type = "Environment";
    retVal.sourceType = retVal.type;

    return retVal;
  }

  private setEnvironments(envs: Node[]) {
    this.initSubcription.unsubscribe();
  }
}
