import { Component, OnInit } from "@angular/core";
import { UserSetting, UserSettingsService } from "../../services/user-settings.service";

@Component({
  selector: "mdm-user-settings",
  templateUrl: "./user-settings.component.html",
  styleUrls: ["./user-settings.component.css"],
})
export class UserSettingsComponent implements OnInit {
  public userSettings: UserSetting[] = [
    { preference: "user-settings.show-navigator-icons", value: true },
    { preference: "user-settings.navigator-chunk-size", value: 0 },
    { preference: "user-settings.search-attributes-case-sensitivity", value: false },
  ];
  currentlyEditedSetting: UserSetting;

  constructor(private userSettingsService: UserSettingsService) {}

  ngOnInit(): void {
    this.userSettingsService.getUserSettingsPreferences().subscribe((settings: UserSetting[]) => {
      this.userSettings = this.userSettings.map((setting) => {
        const savedSetting = settings.find((set) => set.preference === setting.preference);
        if (savedSetting) {
          setting.value = savedSetting.value;
        }

        return setting;
      });
    });
  }

  getSettingType(setting: UserSetting) {
    return typeof setting.value;
  }

  editInit(event: any) {
    this.currentlyEditedSetting = { ...event.data };
  }

  editCompleted(event: any) {
    if (event.data.value !== this.currentlyEditedSetting.value) {
      this.userSettingsService.saveSetting(event.data);
    }
  }
}
