/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Scope } from "@core/preference.service";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService, SelectItem, SelectItemGroup } from "primeng/api";
import { EditableColumn } from "primeng/table";
import { Subscription, take } from "rxjs";
import { PhysDimService, PhysDimTuple } from "../../../administration/units/physicaldimension.service";
import { PhysDimInfo, UnitInfo, UnitService } from "../../../administration/units/unit.service";
import { UnitFilter, UnitPreference, UserUnitPreferenceService } from "../../services/user-unit-mapping.service";

@Component({
  selector: "mdm-user-unit-mapping",
  templateUrl: "user-unit-mapping.component.html",
  styleUrls: ["./user-unit-mapping.component.css"],
  providers: [EditableColumn],
})
export class UserUnitMappingComponent implements OnInit, OnDestroy, IStatefulComponent {
  private readonly MSG_PREFIX: string = "user-prefs.unit-prefs";
  private readonly MSG_ERR_PREFIX: string = this.MSG_PREFIX + ".err-cannot-";
  private readonly MSG_POSTFIX: string = "-unit-prefs";

  private userUnitPrefList: SelectItem[] = [];
  private selectedUnitPrefVal: string;
  public selectedUnitPrefIdx = -1;
  currentUnitPref: UnitPreference;
  public groupedUserUnitPrefs: SelectItemGroup[] = [];
  public selectedUnitPrefDisplayName: string;

  public unitPreferences: UnitPreference[] = [];
  labelNewUnitPref = "";

  public isUserPrefUnitMappingOpen = true;

  tmpUnitPrefDelete: UnitPreference;
  isShowDialogUnitPrefDelete = false;
  tmpUnitPrefEdit: UnitPreference;
  isShowDialogUnitPrefEdit = false;
  isSaveMode = false;
  unitPrefAlreadyExists: string;

  loadSubcription: Subscription;

  environments: Node[];

  physDims: Map<string, PhysDimInfo[]> = new Map<string, PhysDimInfo[]>();

  private sourceSelectList: SelectItem[] = [];

  takeOverDialogVisible = false;
  takeOverUnitPreference = new UnitPreference(this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY);
  takeOverNameExists = false;

  constructor(
    private nodeService: NodeService,
    private userUnitPrefService: UserUnitPreferenceService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
    private physDimService: PhysDimService,
    private unitService: UnitService,
    private stateService: StateService,
  ) {
    this.currentUnitPref = this.getNewUnitPreference();
    this.labelNewUnitPref = this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY;
    this.stateService.register(this, "user-unit-mapping");
  }

  getState(): unknown {
    return {}; // we do not save anything in global state
  }

  applyState(state: unknown) {
    if (state["unitPreference"]) {
      this.takeOverUnitPreference = state["unitPreference"] as UnitPreference;
      this.takeOverUnitPreference.isDefault = false;
      this.takeOverDialogVisible = true;
    }
  }

  takeOver() {
    this.userUnitPrefService.getUserUnitPreferences().subscribe((prefs) => {
      if (prefs.findIndex((p) => p.displName === this.takeOverUnitPreference.displName) >= 0) {
        this.takeOverNameExists = true;
      } else {
        this.takeOverNameExists = false;
        prefs.push(this.takeOverUnitPreference);
        this.userUnitPrefService.saveUserUnitPreferences(prefs).subscribe(() => {
          this.takeOverDialogVisible = false;
          this.reloadUserPrefs();
        });
      }
    });
  }

  getStateForTakeOver(): unknown {
    return {
      "user-unit-mapping": {
        unitPreference: this.currentUnitPref,
      },
    };
  }

  ngOnDestroy() {
    this.stateService.unregister(this);
  }

  ngOnInit() {
    this.nodeService
      .getRootNodes()
      .pipe(take(1))
      .subscribe({
        next: (envs) => {
          const globalNode = {
            id: "*",
            name: "Global",
            sourceName: "*",
            type: "Environment",
            sourceType: "Environment",
          } as Node;

          this.environments = [globalNode].concat(envs);
          this.initPhysDimsAndUnits();
          this.mapUnitPrefOptions();
        },
        error: (error) =>
          this.translateService
            .instant("user-prefs.unit-prefs.err-cannot-load-data-sources")
            .subscribe((msg) => this.notificationService.notifyError(msg, error)),
      });
    this.currentUnitPref = this.getNewUnitPreference();

    this.reloadUserPrefs();
  }

  private reloadUserPrefs(source?: string, displName?: string) {
    this.unitPreferences = [];
    this.loadSubcription = this.userUnitPrefService
      .getUserUnitPreferences()
      .pipe()
      .subscribe(
        (userPrefs) => this.mapUnitPrefs(userPrefs, source, displName),
        (error) =>
          this.notificationService.notifyError(this.translateService.instant(this.MSG_ERR_PREFIX + "load" + this.MSG_POSTFIX), error),
      );
  }

  private initPhysDimsAndUnits() {
    if (this.environments !== undefined && this.environments.length !== 0) {
      this.environments
        .filter((env) => env.sourceName !== "*")
        .forEach((env) => this.physDimService.getPhysDimsForSource(env.sourceName).subscribe((pDims) => this.initPhysDims(env, pDims)));
    }
  }

  private initPhysDims(env: Node, physDims: PhysDimTuple[]) {
    this.unitService.getPhysDimInfosForSource(env.sourceName, physDims).subscribe((pds) => this.setPhysDims(env.sourceName, pds));
  }

  private setPhysDims(source: string, physDims: PhysDimInfo[]) {
    physDims = physDims.filter((physDim: PhysDimInfo) =>
      physDim.unitList.some((unit: UnitInfo) => unit.unitId !== "" && unit.unitName !== ""),
    );
    this.physDims.set(source, physDims);
    if (source !== "*" && physDims.length > 0) {
      if (this.physDims.has("*")) {
        this.physDims.set("*", this.physDims.get("*").concat(physDims));
      } else {
        this.physDims.set("*", physDims);
      }
    }
  }

  private mapUnitPrefOptions() {
    if (this.environments !== undefined) {
      this.userUnitPrefList = this.unitPreferences.map(
        (pref) =>
          <SelectItem>{
            value: this.getUnitPreferenceStringValue(pref),
            label: this.getUnitPreferenceDisplayName(pref),
          },
      );
      this.generateGroupedUserPreferences(this.userUnitPrefList);
    }
  }

  private mapUnitPrefs(unitPrefs: UnitPreference[], source?: string, displName?: string) {
    if (this.labelNewUnitPref === "" || this.labelNewUnitPref === this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY) {
      this.translateService.get(this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY).subscribe((msg: string) => this.setNewUnitPrefLabel(msg));
    }

    if (unitPrefs.length === 0) {
      this.unitPreferences = new Array<UnitPreference>();
      this.unitPreferences.push(this.getNewUnitPreference());
    } else {
      this.unitPreferences = this.userUnitPrefService.sortUnitPreferences(unitPrefs);
    }

    this.mapUnitPrefOptions();

    if (source !== undefined && displName !== undefined) {
      const idx = this.unitPreferences.findIndex((up) => up.source === source && up.displName === displName);
      this.selectUnitPreference(this.unitPreferences[idx]);
    } else {
      this.selectUnitPreference(this.unitPreferences[0]);
      this.loadSubcription.unsubscribe();
    }
  }

  private setNewUnitPrefLabel(label: string) {
    this.labelNewUnitPref = label;
    this.userUnitPrefList.forEach((pref) => {
      if (pref.value === this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY) {
        pref.label = label;
      }
    });
  }

  public onUnitPreferenceDefaultChange(checked: boolean) {
    if (this.selectedUnitPrefIdx >= 0) {
      if (checked && this.unitPreferences.length > 1) {
        this.unitPreferences.forEach((up, idx) => {
          up.isDefault = idx === this.selectedUnitPrefIdx;
        });
      } else if (!checked && this.selectedUnitPrefIdx < this.unitPreferences.length) {
        this.unitPreferences[this.selectedUnitPrefIdx].isDefault = false;
      }
    }
  }

  private getUnitPreferenceDisplayName(pref: UnitPreference): string {
    const sourceDisplNames: string[] = this.getSourceSelectList()
      .filter((sl) => sl.value === pref.source)
      .map((sl) => sl.label);
    return (
      (pref.displName === this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY ? this.labelNewUnitPref : pref.displName) +
      " (" +
      (sourceDisplNames.length === 0 ? pref.source : sourceDisplNames[0]) +
      ")"
    );
  }

  private getUnitPreferenceStringValue(pref: UnitPreference): string {
    return pref.displName + ":" + pref.source;
  }

  selectUnitPreference(pref: UnitPreference) {
    this.selectedUnitPrefIdx = this.unitPreferences.findIndex((up) => up.source === pref.source && up.displName === pref.displName);
    this.selectedUnitPrefVal = this.getUnitPreferenceStringValue(pref);
    this.selectedUnitPrefDisplayName = this.getUnitPreferenceDisplayName(pref);
    this.currentUnitPref = this.userUnitPrefService.getUnitPreferenceClone(pref);
    this.currentUnitPref["json"] = JSON.stringify(this.currentUnitPref);
  }

  private checkForChangeAndCallFunc(callback: Function) {
    let isChanged = false;
    if (
      this.currentUnitPref !== undefined &&
      (this.selectedUnitPrefVal !== this.labelNewUnitPref || this.currentUnitPref.unitFilters.length > 0)
    ) {
      isChanged = JSON.stringify(this.userUnitPrefService.getUnitPreferenceClone(this.currentUnitPref)) !== this.currentUnitPref["json"];
    }
    if (isChanged) {
      this.translateService.get(this.MSG_PREFIX + ".msg-confirm-unsaved-change").subscribe((msg) =>
        this.confirmationService.confirm({
          message: msg,
          key: "unitPrefConfirmation",
          header: this.translateService.instant(this.MSG_PREFIX + ".confirmation-title"),
          accept: callback,
        }),
      );
    } else {
      callback.call(this);
    }
  }

  public onNewUnitPref(e: any): void {
    e.stopPropagation();
    this.unitPrefAlreadyExists = undefined;
    this.checkForChangeAndCallFunc(() => this.confirmedNewUnitPref());
  }

  confirmedNewUnitPref(): void {
    this.unitPrefAlreadyExists = undefined;
    this.tmpUnitPrefEdit = new UnitPreference("");
    this.isShowDialogUnitPrefEdit = true;
    this.isSaveMode = false;
  }

  hideDialogUnitPref(): void {
    if (this.isShowDialogUnitPrefEdit) {
      this.cancelDialogUnitPref();
    }
  }

  cancelDialogUnitPref(): void {
    this.tmpUnitPrefEdit = undefined;
    this.unitPrefAlreadyExists = undefined;
    this.isShowDialogUnitPrefEdit = false;
    this.isSaveMode = false;
  }

  confirmDialogUnitPref(): void {
    this.selectUnitPreference(this.tmpUnitPrefEdit);
    if (
      this.unitPreferences.length === 1 &&
      (this.unitPreferences[0].displName === this.labelNewUnitPref ||
        this.unitPreferences[0].displName === this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY)
    ) {
      this.unitPreferences.splice(0, 1);
    }

    this.currentUnitPref["new"] = true;
    this.currentUnitPref["json"] = "";
    this.unitPreferences.push(this.currentUnitPref);
    this.userUnitPrefService.sortUnitPreferences(this.unitPreferences);
    this.selectedUnitPrefIdx = this.unitPreferences.findIndex((up) => up["new"] === true);
    delete this.currentUnitPref["new"];

    this.mapUnitPrefOptions();
    this.cancelDialogUnitPref();
  }

  private displNameCheckUnique(): void {
    let unitPref = undefined;

    if (this.tmpUnitPrefEdit !== undefined && this.tmpUnitPrefEdit.displName.trim().length > 0) {
      unitPref = this.unitPreferences.find(
        (up) =>
          up.displName === this.tmpUnitPrefEdit.displName &&
          up.source === this.tmpUnitPrefEdit.source &&
          (up.displName !== this.currentUnitPref.displName || up.source !== this.currentUnitPref.source),
      );
    }

    if (unitPref !== undefined || this.labelNewUnitPref === this.tmpUnitPrefEdit.displName) {
      this.unitPrefAlreadyExists = this.translateService.instant(this.MSG_PREFIX + ".err-already-exists" + this.MSG_POSTFIX, {
        displName: this.tmpUnitPrefEdit.displName,
      });
    } else {
      this.unitPrefAlreadyExists = undefined;
    }
  }

  public onShowUnitPrefSaveModal(): void {
    this.tmpUnitPrefEdit = this.userUnitPrefService.getUnitPreferenceClone(this.currentUnitPref);
    this.tmpUnitPrefEdit.displName =
      this.currentUnitPref.displName === this.userUnitPrefService.NEW_UNIT_PREF_NAME_KEY ? "" : this.currentUnitPref.displName;
    this.displNameCheckUnique();
    this.isShowDialogUnitPrefEdit = true;
    this.isSaveMode = true;
  }

  public onKeyUpDialogUnitPref(e: KeyboardEvent) {
    e.stopPropagation();
    if (this.tmpUnitPrefEdit !== undefined && this.tmpUnitPrefEdit.displName.trim().length > 0) {
      this.displNameCheckUnique();
      if (this.unitPrefAlreadyExists === undefined && e.code === "Enter") {
        if (this.isSaveMode) {
          this.saveDialogUnitPref();
        } else {
          this.confirmDialogUnitPref();
        }
      }
    }
  }

  saveDialogUnitPref(): void {
    if (this.isShowDialogUnitPrefEdit) {
      this.saveUnitPrefs("save", () => this.cancelDialogUnitPref(), this.tmpUnitPrefEdit);
    }
    this.cancelDialogUnitPref();
  }

  private saveUnitPrefs(msgKey: string, cancelCallback: Function, changedPref?: UnitPreference) {
    const source = changedPref === undefined ? undefined : changedPref.source;
    const displName = changedPref === undefined ? undefined : changedPref.displName;

    const replacement: UnitPreference[] = new Array<UnitPreference>();
    for (let i = 0; i < this.unitPreferences.length; i++) {
      if (i === this.selectedUnitPrefIdx) {
        if (changedPref !== undefined) {
          replacement.push(this.userUnitPrefService.getUnitPreferenceCloneForSave(changedPref));
        }
      } else {
        replacement.push(this.userUnitPrefService.getUnitPreferenceCloneForSave(this.unitPreferences[i]));
      }
    }
    this.userUnitPrefService.saveUserUnitPreferences(replacement).subscribe(
      () => {
        cancelCallback.call(this);
        if (changedPref === undefined) {
          this.reloadUserPrefs();
        } else {
          this.reloadUserPrefs(source, displName);
        }
      },
      (error) =>
        this.notificationService.notifyError(this.translateService.instant(this.MSG_ERR_PREFIX + msgKey + this.MSG_POSTFIX), error),
    );
  }

  public onRemoveUnitFilter(unitFilter: UnitFilter): void {
    // currently not used
  }

  getPhysDims(env: string): PhysDimInfo[] {
    let retVal: PhysDimInfo[] = undefined;
    if (this.physDims !== undefined && env !== undefined) {
      retVal = this.physDims.get(env);
    }
    return retVal === undefined ? [] : retVal;
  }

  getSourceSelectList(): SelectItem[] {
    if (this.environments !== undefined && this.environments.length > 0 && this.sourceSelectList.length === 0) {
      this.sourceSelectList = this.environments.map(
        (env) =>
          <SelectItem>{
            value: env.sourceName,
            label: env.name,
          },
      );
    }

    return this.sourceSelectList;
  }

  public onDeleteUnitPref(e: any) {
    e.stopPropagation();
    this.tmpUnitPrefDelete = this.currentUnitPref;
    this.isShowDialogUnitPrefDelete = true;
  }

  cancelRemoveUnitPref() {
    this.tmpUnitPrefDelete = undefined;
    this.isShowDialogUnitPrefDelete = false;
  }

  confirmedRemoveUnitPref() {
    if (this.isShowDialogUnitPrefDelete) {
      this.saveUnitPrefs("delete", () => this.cancelRemoveUnitPref());
    }
    this.cancelRemoveUnitPref();
  }

  private getNewUnitPreference(): UnitPreference {
    return this.userUnitPrefService.getUnitPreferenceClone(this.userUnitPrefService.EMPTY_UNIT_PREF);
  }

  private generateGroupedUserPreferences(userUnitPrefs: SelectItem[]): any {
    this.groupedUserUnitPrefs = [];
    this.groupedUserUnitPrefs.push({
      value: Scope.USER,
      items: userUnitPrefs,
      label: Scope.toLabel(Scope.USER),
    });
  }

  public clickUnitPreference($event: any) {
    const userUnitPref = $event.value;
    const idx = this.userUnitPrefList.findIndex((uup) => uup.value === userUnitPref);
    this.checkForChangeAndCallFunc(() => this.selectUnitPreference(this.unitPreferences[idx]));
  }
}
