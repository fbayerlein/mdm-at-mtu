/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ApiTokenComponent } from "./apitoken/apitoken.component";
import { UserChannelFilterComponent } from "./components/user-channel-filter/user-channel-filter.component";
import { UserFormulaComponent } from "./components/user-formula/user-formula.component";
import { UserPrefsNavCardComponent } from "./components/user-prefs-nav-card/user-prefs-nav-card.component";
import { UserSettingsComponent } from "./components/user-settings/user-settings.component";
import { UserUnitMappingComponent } from "./components/user-unit-mapping/user-unit-mapping.component";

const userPrefRoutes: Routes = [
  {
    path: "",
    component: UserPrefsNavCardComponent,
    children: [
      { path: "channel-filter", component: UserChannelFilterComponent },
      { path: "user-formula", component: UserFormulaComponent },
      { path: "unit-mapping", component: UserUnitMappingComponent },
      { path: "api-token", component: ApiTokenComponent },
      { path: "user-settings", component: UserSettingsComponent },
      { path: "", redirectTo: "channel-filter", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(userPrefRoutes)],
  exports: [RouterModule],
})
export class UserPrefsRoutingModule {}
