/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { all, create, MathJsStatic, MathNode } from "mathjs";
import { InfoUnit } from "src/app/administration/units/unit.service";
import { Unit } from "../../administration/units/unit.model";
import { ChannelUnitUI } from "../../chartviewer/model/types/channel-unit-ui.class";
import { Channel } from "../../chartviewer/model/types/channel.class";

const math: MathJsStatic = create(all);

export class FormulaValidationResult {
  mathJsParsableFormula: string;
  parsedFormulaString: string;
  channelAliasList: string[];
  channelExpressions: string[];
  validatedChannelExpressions: string[];
  valid: boolean;

  constructor(
    mathJsParsableFormula: string,
    parsedFormulaString: string,
    parsedFormula: math.MathNode,
    channelAliasList: string[],
    channelExpressions: string[],
    validatedChannelExpressions: string[],
  ) {
    this.mathJsParsableFormula = mathJsParsableFormula;
    this.parsedFormulaString = parsedFormulaString;
    this.channelAliasList = channelAliasList;
    this.channelExpressions = channelExpressions;
    this.validatedChannelExpressions = validatedChannelExpressions;
    // - we need at least 1 input channel
    // - math JS parsing was successful
    // - all validated channel expressions, are equal with the input channel expressions
    this.valid =
      channelAliasList.length > 0 &&
      parsedFormula !== undefined &&
      channelExpressions.length === validatedChannelExpressions.length &&
      channelExpressions.every((ce, i) => ce.trim() === validatedChannelExpressions[i]);
  }
}

export class FormulaPreference {
  displName: string;
  formulaInput: string;
  channelExpressions: string[];
  channelAliasList: string[];
  mathJsParsableFormula?: string;
  formulaError?: string;

  constructor(
    displName: string,
    formulaInput: string,
    channelExpressions: string[] = [],
    channelAliasList: string[] = [],
    mathJsParsableFormula?: string,
  ) {
    this.displName = displName;
    this.formulaInput = formulaInput;
    this.channelExpressions = channelExpressions;
    this.channelAliasList = channelAliasList;
    this.mathJsParsableFormula = mathJsParsableFormula;
  }

  public hasInput() {
    return this.formulaInput && this.formulaInput.trim().length > 0;
  }

  private equalBracketCount(leftBracket: string, rightBracket: string): boolean {
    const leftBracketCount = this.formulaInput.replace(new RegExp("[^" + leftBracket + "]", "g"), "").length;
    const rightBracketCount = this.formulaInput.replace(new RegExp("[^" + rightBracket + "]", "g"), "").length;
    return leftBracketCount === rightBracketCount;
  }

  /**
   * for onKeyUp validation: when user is typing, decide when it makes sense to validate
   * @returns true | false
   */
  public canValidateOnFly(): boolean {
    return (
      this.hasInput() &&
      !this.formulaInput.endsWith(" ") &&
      !this.formulaInput.endsWith("$") &&
      this.formulaInput.trim().match(/[({./+*\^\-]$/) === null &&
      this.formulaInput.trim().match(/^[)}.*\^!%/]/) === null &&
      this.formulaInput.indexOf("(") <= this.formulaInput.lastIndexOf(")") &&
      this.formulaInput.indexOf("${") <= this.formulaInput.lastIndexOf("}") &&
      this.equalBracketCount("(", ")") &&
      this.equalBracketCount("{", "}")
    );
  }

  public isValid(): boolean {
    return (
      this.mathJsParsableFormula != undefined &&
      this.channelAliasList.length > 0 &&
      this.channelExpressions.length > 0 &&
      this.hasInput() &&
      !this.channelExpressions.some((c) => c.includes("`"))
    );
  }
}

export class FilteredFormulaPreference extends FormulaPreference {
  virtualId: string;
  // none unique list of (first found) matching channel ID's
  channelIds: string[];
  channels: Map<string, Channel>;

  constructor(fPref: FormulaPreference, prefIdx: number, channelIds: string[], channels: Map<string, Channel>) {
    super(fPref.displName, fPref.formulaInput, fPref.channelExpressions, fPref.channelAliasList, fPref.mathJsParsableFormula);

    this.virtualId = "-" + channels.values().next().value.channelGroupId + "." + prefIdx.toString();
    this.channelIds = channelIds;
    this.channels = channels;
  }

  public get channelGroupId(): string {
    return this.channels.values().next().value.channelGroupId;
  }

  public set channelGroupId(s: string) {}
}

export const TYPE_VIRTUAL_CHANNEL = "VirtualChannel";

export class VirtualChannelUnitUI extends ChannelUnitUI {
  // none unique list of (first found) matching channel ID's
  channelIds: string[];
  channels: Map<string, Channel>;
  channelAliasList: string[];
  mathJsParsableFormula: string;

  constructor(channel: Channel, infoUnit: InfoUnit, formulaPref: FilteredFormulaPreference) {
    super(channel, infoUnit);
    this.channelIds = formulaPref.channelIds;
    this.channels = formulaPref.channels;
    this.channelAliasList = formulaPref.channelAliasList;
    this.mathJsParsableFormula = formulaPref.mathJsParsableFormula;

    this.unitId = infoUnit.unit.id;
    this.unitName = infoUnit.unit.name;
  }

  public getFirstRequiredChannel() {
    if (this.channelIds && this.channelIds.length > 0) {
      return this.channels.get(this.channelIds[0]);
    } else {
      return undefined;
    }
  }
}

@Injectable()
export class UserFormulaBaseService {
  public readonly FORMULA_PREF_KEY = "user-prefs.formula-prefs";
  public readonly NEW_FORMULA_PREF_NAME_KEY = this.FORMULA_PREF_KEY + ".no-formula-pref-selected";
  public readonly VIRTUAL_UNIT = this.newVirtualUnit();
  public readonly VIRTUAL_INFO_UNIT = new InfoUnit(this.VIRTUAL_UNIT, "-1", [this.VIRTUAL_UNIT]);

  public testMode = false;

  constructor() {}

  private readonly placeHolders: string[] = Array.from("abcdefghijklmnopqrstuvwxyz");

  private reduceAndBalanceBrackets(unbalancedInput: string, leftBracket: string, rightBracket: string): string {
    // reduce ((....)) to (....)
    const reReduce = new RegExp(
      "[" + leftBracket + "]([" + leftBracket + "][^" + rightBracket + "]+[" + rightBracket + "])[" + rightBracket + "]",
      "g",
    );
    return this.reduceAndBalanceBracketsInternal(unbalancedInput, leftBracket, rightBracket, reReduce, "$1");
  }

  private reduceAndBalanceFormulaBrackets(unbalancedInput: string, leftBracket: string, rightBracket: string): string {
    const reReduce = new RegExp("(^|[^\\w\\d])[" + leftBracket + "]([\\d.#]+)[" + rightBracket + "]", "g");
    return this.reduceAndBalanceBracketsInternal(unbalancedInput, leftBracket, rightBracket, reReduce, "$1$2");
  }

  private reduceAndBalanceBracketsInternal(
    unbalancedInput: string,
    leftBracket: string,
    rightBracket: string,
    reReduce: RegExp,
    replace: string,
  ): string {
    let len = 0;
    // reduce (#), ((#)), ((([0-9.]))) --> #, #, [0-9.]
    const reEmpty = new RegExp("[" + leftBracket + "][s]*[" + rightBracket + "]", "g");
    do {
      len = unbalancedInput.length;
      unbalancedInput = unbalancedInput.replace(reReduce, replace).replace(reEmpty, "");
    } while (len > 0 && unbalancedInput.length < len);

    // handle (un-closed) brackets
    const leftBracketCount = unbalancedInput.replace(new RegExp("[^" + leftBracket + "]", "g"), "").length;
    const rightBracketCount = unbalancedInput.replace(new RegExp("[^" + rightBracket + "]", "g"), "").length;
    if (leftBracketCount < rightBracketCount) {
      // go right to left, remove ')'
      let diff = rightBracketCount - leftBracketCount;
      for (; diff > 0; diff--) {
        const pos = unbalancedInput.lastIndexOf(rightBracket);
        unbalancedInput = unbalancedInput.substring(0, pos) + unbalancedInput.slice(pos + 1);
      }
    } else if (rightBracketCount < leftBracketCount) {
      // go left to right, remove '('
      let diff = leftBracketCount - rightBracketCount;
      for (; diff > 0; diff--) {
        const pos = unbalancedInput.indexOf(leftBracket);
        unbalancedInput = unbalancedInput.substring(0, pos) + unbalancedInput.slice(pos + 1);
      }
    }

    return unbalancedInput;
  }

  /**
   * transform the free text input into parts, that can be processed better internally.
   * Reserved characters: '# ; « »' '\u00ab\u00bb'
   * Convert all '{ ... }' to '« ... »' and all '${ ... }' to '# ... ;'
   * @param freeStyleText
   * @returns
   */
  private prepareExtractionOfChannelNameParameters(freeStyleText: string): string {
    // '${ ... }' can contain regular expression syntax,
    // so it could contain ' { .. } ' again.
    // '${' -> '#' | '# # #' -> '#' | '{' -> '«'
    let preparedFormula = freeStyleText
      .replace(/[;#\u00ab\u00bb]/g, "")
      .replace(/[$][{]/g, "#")
      .replace(/[#\s]+#/g, "#")
      .replace(/##/g, "#")
      .replace(/[{]/g, "\u00ab");

    // replace « ... } to « ... »
    const re = /(\u00ab[^}\u00bb]+)([}]|$)/g;
    do {
      preparedFormula = preparedFormula.replace(re, "$1\u00bb");
    } while (re.test(preparedFormula));

    // all remaining '}' shall now be the end markers of ${ ... } channel name expressions
    preparedFormula = preparedFormula
      .replace(/[}]/g, ";")
      // insert missing ';' before next '#'
      .replace(/#([^#;]+)([/*%!\^+-]+[()/*%!\^+-]*)#/g, "#$1;$2#")
      .replace(/#([^#;]+)#/g, "#$1;#")
      .replace(/[\u00ab]/g, "{")
      .replace(/[\u00bb]/g, "}");

    // now channel names should be contained within:
    // ${ ... } -> # ... ;

    return this.reduceAndBalanceBrackets(preparedFormula, "#", ";");
  }

  private getPlaceHolderAt(idx: number): string {
    const grp = parseInt((idx / this.placeHolders.length).toString(), 10);
    return this.placeHolders[idx] + grp.toString();
  }

  /**
   * separate channel name expressions from formula expressions,
   * - validate mathjs.org formula
   * - validate channel name expressions
   * @param freeStyleText
   * @returns
   */
  public validate(freeStyleText: string): FormulaValidationResult {
    const channelExpressions: string[] = [];
    const channelAliasList: string[] = [];
    const rawFormula: string[] = [];

    const splittableText = this.prepareExtractionOfChannelNameParameters(freeStyleText);
    const splittedText: string[] = splittableText.split("#");
    splittedText.forEach((part) => {
      if (part.includes(";")) {
        const tuple = part.split(";");
        const chnlExpr = tuple[0].trim().length === 0 ? ".*" : tuple[0];
        channelAliasList.push(this.getPlaceHolderAt(channelExpressions.length));
        channelExpressions.push(chnlExpr);
        const tail = tuple[1].trim().length > 0 ? tuple[1] : "";
        rawFormula.push(tail);
      } else {
        rawFormula.push(part);
      }
    });

    const placeHolder = channelExpressions.length > 0 ? "#" : "";
    const cleanableFormula = rawFormula.join(placeHolder);
    const cleanedFormula = this.getCleanedFormula(cleanableFormula);

    let mathJsParsableFormula: string;
    if (channelExpressions.length > 0) {
      const parsableFormula: string[] = [];
      cleanedFormula.split(placeHolder).forEach((part, idx) => {
        parsableFormula.push(part);
        if (idx < channelAliasList.length) {
          parsableFormula.push(channelAliasList[idx]);
        }
      });
      mathJsParsableFormula = parsableFormula.join("");
    } else {
      mathJsParsableFormula = cleanedFormula;
    }

    let parsedFormulaString: string[] = [];
    let validatedChannelExpressions: string[] = [];
    let parsedFormula: MathNode;
    try {
      parsedFormula = math.parse(mathJsParsableFormula);
      // check, if formula can calculate anything:
      // mathjs.org will accept things like abc(a0 * b0) ^ xyz(e)
      // but neither 'abc()' nor 'xyz()' are defined, so we compile + evaluate with test-numbers
      const scopeDataObj: any = {};
      channelAliasList.forEach((al, idx) => {
        scopeDataObj[al] = idx + 1 + Number.parseFloat("0." + (channelAliasList.length - idx).toString());
      });
      this.evaluateFormula(parsedFormula.compile(), scopeDataObj);

      const mathJsparsedFormulaString = parsedFormula.toString();
      validatedChannelExpressions = channelExpressions.map((expr) => this.validateChannelExpression(expr));

      if (!this.testMode && channelAliasList.length === 0) {
        parsedFormulaString.push("Channel missing/kein Kanal angegeben: `");
      }

      // now replace the mathJS parameters, with the channelName expressions again
      this.restoreFromMathJsFormula(mathJsparsedFormulaString, validatedChannelExpressions, channelAliasList, parsedFormulaString);

      if (!this.testMode && channelAliasList.length === 0) {
        parsedFormulaString.push("`");
      }
    } catch (e) {
      let msg: string;
      if (typeof e === "string") {
        msg = e.toUpperCase();
      } else if (e instanceof Error) {
        msg = e.message;
      } else {
        msg = String(e);
      }
      if (msg.includes("(char ")) {
        // rewrite mathjs.org SyntaxException message
        const matches = msg.match(/[(]char\s+(\d+)[)]$/);
        const lastIdx = matches.length - 1;
        const matchPos = lastIdx >= 0 && /^\d+$/.test(matches[lastIdx]) ? Number.parseInt(matches[lastIdx], 10) : -1;
        if (matchPos > -1) {
          const collector: string[] = [];
          const leftPart = mathJsParsableFormula.substring(0, matchPos);
          const rightPart = mathJsParsableFormula.substring(matchPos);
          this.restoreFromMathJsFormula(leftPart, channelExpressions, channelAliasList, collector);
          const convertedLeftPart = collector.join("");
          this.restoreFromMathJsFormula(rightPart, channelExpressions, channelAliasList, collector);
          const head = [msg.replace(/^(.*[(]char\s+)\d+[)]$/, "$1")];
          head.push(convertedLeftPart.length.toString());
          head.push("): `");
          parsedFormulaString = head.concat(collector);
        }
      }
      if (parsedFormulaString.length === 0) {
        parsedFormulaString.push(msg + " in: `" + mathJsParsableFormula);
      }
      parsedFormulaString.push("`");
    }

    return new FormulaValidationResult(
      mathJsParsableFormula,
      parsedFormulaString.join(""),
      parsedFormula,
      channelAliasList,
      channelExpressions,
      validatedChannelExpressions,
    );
  }

  private restoreFromMathJsFormula(
    mathJsFormulaPart: string,
    channelExpressions: string[],
    channelAliasList: string[],
    collector: string[],
  ): void {
    if (mathJsFormulaPart.length === 0) {
      return;
    }
    // now replace the mathJS parameters, with the channelName expressions again
    let lastPos = -1;
    for (let parIdx = 0; parIdx < channelExpressions.length; parIdx++) {
      const pos = mathJsFormulaPart.indexOf(channelAliasList[parIdx]);
      if (lastPos < pos) {
        collector.push(mathJsFormulaPart.substring(lastPos, pos));
        lastPos = pos + channelAliasList[parIdx].length;
      }
      if (pos >= 0) {
        collector.push("${" + channelExpressions[parIdx] + "}");
      }
    }
    if (lastPos < 0) {
      lastPos = 0;
    }
    if (lastPos < mathJsFormulaPart.length) {
      collector.push(mathJsFormulaPart.substring(lastPos));
    }
  }

  private validateChannelExpression(channelExpression: string): string {
    let validatedChannelExpression: string;
    try {
      const trimmedExpr = channelExpression.trim();
      new RegExp(trimmedExpr).test("z");
      validatedChannelExpression = trimmedExpr;
    } catch (e) {
      // SyntaxError
      validatedChannelExpression = "#syntax error# `" + channelExpression + "` #syntax error#";
    }
    return validatedChannelExpression;
  }

  /**
   * exposed for unit test only.
   * This is mainly used internally, to clean formula input.
   *
   * @param rawFormula
   * @returns
   */
  public getCleanedFormula(rawFormula: string): string {
    // reserved chars:
    // [${}(),.^!*/%+-]
    //
    // ----------------
    // constants:
    // (e|pi|LN2|LN10|LOG2E|LOG10E|phi|SQRT1_2|SQRT2|tau)

    // subset of functions:
    // (abs|ceil|exp|expm1|fix|floor|log|log1[0p]|log2|pow|round|sqrt)

    // Trigometrie:
    // (a?(cos|cot|csc|sec|sin|tan)h?)
    //
    // [_0-2a-il-uwxEGLNOQ-T]

    // remove invalid input and constructs
    let cleanedFormula = rawFormula.replace(/[^#_a-il-uwxEGILN-T,\d(){}./*%!^+-]/g, "");

    // reduce repeated invalid input
    Array.from("{}/*%!+-.,_abcdefghilmnopqrstuwxEGILNOPQRST").forEach((c) => {
      const regExp = new RegExp("[" + c + "]{2,}", "g");
      cleanedFormula = cleanedFormula.replace(regExp, c);
    });
    cleanedFormula = cleanedFormula.replace(/[\^]{2,}/g, "^");

    cleanedFormula =
      "+" +
      cleanedFormula
        .replace(/flor/g, "floor")
        .replace(/[pP][iI]/g, "pi")
        .replace(/[lL][nN](2|10)/g, "LN$1")
        .replace(/[lL][oO][gG](2|10)E/g, "LOG$1E")
        .replace(/[sS][qQ][rR][tT]([12])/g, "SQRT$1");

    cleanedFormula = cleanedFormula
      // closing brackets at beginning
      .replace(/^([^{(]*)[)}]/, "$1")
      // opening brackets at end
      .replace(/[{(]([^)}]*)$/, "$1")
      // { ... ) ... } or { ... ( ... } -> remove '{}'
      .replace(/[{]([^}()]*[()][^}()]*)[}]/g, "$1")
      // ( ... } ... ) or ( ... { ... ) -> remove '{' or '}'
      .replace(/[(]([^){}]*)[{}]([^){}]*)[)]/g, "($1$2)")
      // evil things like '9(+)(*)(-)(/)(^)' -> '9+*-/^'
      .replace(/[({]([/*%!\^+-]+)[)}]/g, "$1")
      // evil things like '9(+(*(-)/)^)' -> '9+*-/^'
      .replace(/([/*\^+-]+)[({)}]+([/*\^+-]+)/g, "$1$2")
      // '+-' or '-+' still is '-' operator
      .replace(/[+][-]|[-][+]/g, "-")
      // '/*' or ' * /' still is '/' operator
      .replace(/[/][*]|[*][/]/g, "/")
      // switch position of operators and numbers, e.g. typo '+!5' -> '+5!'
      .replace(/([()/*\^+-])([!%])([\d.]+|#|[\w_012]+[(][\d.]+[)]|[\w_012]+[(]#[)])/g, "$1$3$2")
      // input like '+*-/^' -> '+'
      .replace(/([/*\^+-])[/*%!^+-]+/g, "$1")
      // add 0 in front of '.[0-9]'
      .replace(/(\D|^)[.](\d)/g, "$10.$2")
      // keep 1st '.' and replace all repeated '.' inside numbers
      .replace(/(\d+)[.]([\d.]+)/g, "$1;$2")
      .replace(/[.]/g, "")
      .replace(/;/g, ".")
      // switch position of operators and brackets, e.g. typo '+)5' -> ')+5'
      .replace(/([/*\^+-])[)]+([\d\w#]|$)/g, ")$1$2")
      .replace(/(\d#)[(]+([/*\^+-])/g, "$1$2(")
      .replace(/[({]+[/*%!\^+-]$/, "")
      // replace alterating use of operators and brackets, e.g. ''9^(+(-(*(/(9)+)-)^)*)'
      .replace(/[(][/*%!\^+][(]/g, "((")
      .replace(/[(][%!]/g, "(")
      .replace(/[)][/*\^+-][)]/g, "))")
      .replace(/[/*\^+-][)]/g, ")")
      .replace(/[{][/*%!\^+-][{]/g, "{{")
      .replace(/[{][/*%!\^+-]/g, "{")
      .replace(/[}][/*%!\^+-][}]/g, "}}")
      .replace(/[/*%!\^+-][}]/g, "}");

    // remove all other illegal things, like:
    // - place-holders,
    // - single characters,
    // - combinations of single characters + digits
    cleanedFormula = cleanedFormula
      .replace(/([\d.]+)[#a-zA-Z]([\d.]+)/g, "$1$2")
      .replace(/[\d.]+#/g, "#")
      .replace(/#[\d.]+/g, "#")
      // except (LOG2E|LOG10E|log10p)
      .replace(/[^A-Za-z_\d][^A-Za-z_\d(][\d.]*[02-9][1-9]([a-zA-Z])/g, "$1")
      .replace(/[^A-Za-z_\d][^A-Za-z_\d(][\d.]*[13-9]([A-DF-Za-oq-z])/g, "$1")
      .replace(/[\d.]+\d\d([Ep])/g, "$1")
      .replace(/[\d.]+2E/g, "E")
      .replace(/[\d.]*[02-9][1-9]([Ep])/g, "$1")
      // except (LOG2E|LOG10E|log10p|SQRT1_2|SQRT2|log1[0p]|log2|atan2)
      .replace(/([a-zA-Z_][A-FH-SU-Va-fh-mo-z])\d[\d.]+/g, "$1")
      .replace(/([a-zA-Z_][A-FH-SU-Va-fh-mo-z])[03-9][\d.]*/g, "$1")
      .replace(/([A-FH-SU-Va-fh-mo-z])[12][\d.]*/g, "$1")
      // single characters + digits between operators or brackets
      .replace(/[(){}/*\^+-][a-zA-Z][\d.]+([(){}+-])/g, "$1")
      .replace(/[(){}/*\^+-][a-zA-Z][\d.]+[*%!\^]/g, "")
      // constant 'e' is allowed
      .replace(/[/*\^+-][A-Za-df-z]([)}+-])/g, "$1")
      .replace(/([({])[A-Za-df-z]([)}+-])/g, "$1$2")
      .replace(/[/*\^+-][A-Za-df-z][/*%!\^]/g, "")
      .replace(/([({])[A-Za-df-z][/*%!\^]/g, "$1")
      .replace(/^[a-zA-Z][\d.]+([({+-])/, "$1")
      .replace(/^[a-zA-Z][\d.]+[%!/*\^]/, "")
      .replace(/([)}])[a-zA-Z][\d.]+$/, "$1")
      .replace(/[({/*\^+-][a-zA-Z][\d.]+$/, "");

    cleanedFormula = this.reduceAndBalanceFormulaBrackets(cleanedFormula, "(", ")");
    cleanedFormula = this.reduceAndBalanceFormulaBrackets(cleanedFormula, "{", "}");

    // remove operators at START and END
    return cleanedFormula.replace(/^[/*%!\^+]/, "").replace(/[/*\^+-]$/, "");
  }

  /**
   * create data object (so called 'scope') for mathjs.org formula evaluation
   * @param channelAliasList
   * @param paramList
   * @returns
   */
  public toScopeDataObject(channelAliasList: string[], paramList: number[]): any {
    const maxIdx = channelAliasList.length <= paramList.length ? channelAliasList.length : paramList.length;
    const scopeDataObj: any = maxIdx > 0 ? {} : undefined;
    for (let idx = 0; idx < maxIdx; idx++) {
      scopeDataObj[channelAliasList[idx]] = paramList[idx];
    }
    return scopeDataObj;
  }

  /**
   * for UnitTests / Karma Specs only
   * @param compilableFormula
   * @param scopeDataObj
   * @returns
   */
  public evaluateFormulaUT(compilableFormula: string, scopeDataObj?: any): number {
    return this.evaluateFormula(math.parse(compilableFormula).compile(), scopeDataObj);
  }

  /**
   * evaluate a mathjs.org compiled formula with concrete numeric channel data
   * @param compiledFormula
   * @param scopeDataObj
   * @returns
   */
  private evaluateFormula(compiledFormula: math.EvalFunction, scopeDataObj?: any): number {
    const result: any = scopeDataObj ? compiledFormula.evaluate(scopeDataObj) : compiledFormula.evaluate();
    return typeof result === "number" || result instanceof Number ? <number>result : 0.0;
  }

  /**
   * calculate all maesurement data of channels with a mathjs.org compiled formula
   * mapping between selected channel (name)'s and parameters/place holders
   * UNIQUE: [ 'a', 'b', 'a', 'c'].filter((v,i,self) => self.indexOf(v) === i);
   * MAP-TO: ['a0', 'b0', 'c0', 'd0']
   * @param mathJsParsableFormula
   * @param channelAliasList
   * @param channelIds -- amount of matched channel ID's must be equal to channelAliasList amount
   * @param channelData -- unique channel data to compute in same order like the Alias list (data for every channel ID only 1 time)
   * @returns
   */
  public evaluateDataWith(
    mathJsParsableFormula: string,
    channelAliasList: string[],
    channelIds: string[],
    channelData: Array<number[]>,
  ): Array<number> {
    const retVal: Array<number> = [];

    if (channelAliasList.length > 0) {
      const compiledFormula = math.parse(mathJsParsableFormula).compile();

      if (channelAliasList.length === channelIds.length) {
        const uniqueChannelIds = channelIds.filter((v, i, self) => self.indexOf(v) === i);
        const chanLenList = channelData
          .map((data) => data.length)
          .sort((a, b) => Math.sign(a - b))
          .filter((value, idx, self) => self.indexOf(value) === idx);
        const shortest = chanLenList[0];
        const longest = chanLenList[chanLenList.length - 1];

        // calculate result where all channels have the needed amount of data
        for (let dataIdx = 0; dataIdx < shortest; dataIdx++) {
          const params: number[] = [];
          for (let paramIdx = 0; paramIdx < channelAliasList.length; paramIdx++) {
            const chnlIdx = uniqueChannelIds.findIndex((chnlId) => chnlId === channelIds[paramIdx]);
            params.push(channelData[chnlIdx][dataIdx]);
          }
          retVal.push(this.evaluateFormula(compiledFormula, this.toScopeDataObject(channelAliasList, params)));
        }

        if (longest > shortest) {
          // fill up remaining channel data with values of 1st longest channel
          const firstMatchingChannelIdx = channelData.findIndex((data) => data.length === longest);
          for (let dataIdx = shortest; dataIdx < longest; dataIdx++) {
            retVal.push(channelData[firstMatchingChannelIdx][dataIdx]);
          }
        }
      }
    }
    return retVal;
  }

  /**
   * in case someone add's a JSON string manually to the User properties.
   * Here we make sure only valid data passes, un-parsed get parsed LAZY
   * @param loadedPreferences
   * @returns
   */
  public assureValidated(loadedPreferences: FormulaPreference[]): FormulaPreference[] {
    return loadedPreferences
      .filter((pref) => pref.formulaInput.trim().length > 0)
      .map((pref) => this.assureValid(pref))
      .filter((pref) => pref.isValid());
  }

  protected assureValid(formulaPref: FormulaPreference, forUI = false): FormulaPreference {
    const retVal = formulaPref;
    if (!retVal.isValid() && retVal.hasInput()) {
      const validationResult = this.validate(formulaPref.formulaInput);
      formulaPref.channelExpressions = validationResult.validatedChannelExpressions;
      formulaPref.channelAliasList = validationResult.channelAliasList;
      formulaPref.mathJsParsableFormula = validationResult.mathJsParsableFormula;
      if (validationResult.valid) {
        formulaPref.formulaInput = validationResult.parsedFormulaString;
      } else if (forUI) {
        formulaPref.formulaError = validationResult.parsedFormulaString;
      }
    }
    return retVal;
  }

  /**
   * validate user's formula input
   * @param formulaPref
   * @returns
   */
  public validateFormula(formulaPref: FormulaPreference): FormulaPreference {
    if (formulaPref === undefined) {
      return undefined;
    }
    // forcing it to re-validate
    formulaPref.mathJsParsableFormula = undefined;
    formulaPref.formulaError = undefined;
    return this.assureValid(formulaPref, true);
  }

  /**
   * get a valid clone for saving.
   * @param formulaPref
   * @returns
   */
  public getFormulaPreferenceCloneForSave(formulaPref: FormulaPreference): FormulaPreference {
    if (formulaPref === undefined) {
      return undefined;
    }
    const validated = this.assureValid(formulaPref);
    if (!validated.isValid()) {
      return undefined;
    }
    return this.getFormulaPreferenceClone(validated, validated.mathJsParsableFormula);
  }

  /**
   *
   * @param formulaPref
   * @returns
   */
  public getFormulaPreferenceCloneForEdit(formulaPref: FormulaPreference): FormulaPreference {
    return this.getFormulaPreferenceClone(formulaPref, formulaPref.mathJsParsableFormula);
  }

  /**
   * get a valid clone for editing.
   * @param formulaPref
   * @param mathJsParsableFormula
   * @returns
   */
  private getFormulaPreferenceClone(formulaPref: FormulaPreference, mathJsParsableFormula: string): FormulaPreference {
    if (formulaPref === undefined) {
      return undefined;
    }
    const ceClones: string[] = [].concat(formulaPref.channelExpressions);
    const calClones: string[] = [].concat(formulaPref.channelAliasList);
    return new FormulaPreference(formulaPref.displName, formulaPref.formulaInput, ceClones, calClones, mathJsParsableFormula);
  }

  /**
   *
   * @param allPrefs -- all defined formula preferences for all sources
   * @param currentYChannels -- available Y Channel list
   * @param isCaseSensitive -- match case sensitive or not
   */
  public filterFormulasForChannels(
    allPrefs: FormulaPreference[],
    currentYChannels: Channel[],
    isCaseSensitive: boolean,
  ): FilteredFormulaPreference[] {
    const retVal: FilteredFormulaPreference[] = [];

    if (currentYChannels && currentYChannels.length > 0 && allPrefs && allPrefs.length > 0) {
      const groupedChannels: Map<string, Channel[]> = new Map();
      currentYChannels.forEach((c) => {
        if (groupedChannels.has(c.channelGroupId)) {
          groupedChannels.get(c.channelGroupId).push(c);
        } else {
          groupedChannels.set(c.channelGroupId, [c]);
        }
      });
      allPrefs.forEach((pref, prefIdx) => {
        if (pref.isValid()) {
          let matchExpressions: RegExp[];
          if (isCaseSensitive) {
            matchExpressions = pref.channelExpressions.map((expr) => new RegExp(expr));
          } else {
            matchExpressions = pref.channelExpressions.map((expr) => new RegExp(expr, "i"));
          }
          groupedChannels.forEach((chnls, channelGroupId) => {
            const channelIds: string[] = [];
            const channels: Map<string, Channel> = new Map();
            matchExpressions.forEach((re) => {
              const matchingChannel = chnls.find((chnl) => re.test(chnl.name));
              if (matchingChannel) {
                channelIds.push(matchingChannel.id);
                if (!channels.has(matchingChannel.id)) {
                  channels.set(matchingChannel.id, matchingChannel);
                }
              }
            });
            if (channelIds.length === pref.channelExpressions.length) {
              retVal.push(new FilteredFormulaPreference(pref, prefIdx, channelIds, channels));
            }
          });
        }
      });
    }
    return retVal;
  }

  private newVirtualUnit(): Unit {
    const u = new Unit();
    u.name = "-";
    u.physDimId = "-1";
    return u;
  }

  /**
   * convert to Channel compatible data structure
   * @param matchingPrefs
   * @returns
   */
  public mapFormulasToChannels(matchingPrefs: FilteredFormulaPreference[]): VirtualChannelUnitUI[] {
    if (!matchingPrefs || matchingPrefs.length === 0) {
      return new Array<VirtualChannelUnitUI>();
    }
    return matchingPrefs.map((pref) => {
      const chnl = new Channel();
      chnl.type = TYPE_VIRTUAL_CHANNEL;
      chnl.name = pref.displName;
      chnl.axisType = "Y_AXIS";
      chnl.isIndependent = false;
      chnl.measurement = Array.from(pref.channels.values())[0].measurement;
      chnl.channelGroupId = pref.channelGroupId;
      chnl.id = pref.virtualId;
      return new VirtualChannelUnitUI(chnl, this.VIRTUAL_INFO_UNIT, pref);
    });
  }

  /**
   *
   * @param formulaPrefs
   * @returns
   */
  public sortFormulaPreferences<T extends FormulaPreference>(formulaPrefs: T[]): T[] {
    return formulaPrefs.sort((up1, up2) => {
      let retVal = 0;
      if (retVal === 0) {
        retVal = up1.displName.localeCompare(up2.displName);
      }
      return retVal;
    });
  }
}
