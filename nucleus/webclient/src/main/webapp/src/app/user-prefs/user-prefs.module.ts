/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

// Angular Imports
import { NgModule } from "@angular/core";

// 3rd party modules
import { ConfirmationService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
//import { DialogService } from 'primeng/api';
import { CheckboxModule } from "primeng/checkbox";
import { DialogModule } from "primeng/dialog";
import { DropdownModule } from "primeng/dropdown";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { MenuModule } from "primeng/menu";
import { TableModule } from "primeng/table";
import { TooltipModule } from "primeng/tooltip";

// MDM modules
import { MDMCoreModule } from "@core/mdm-core.module";
import { PreferenceService } from "@core/preference.service";

// This Module's Components
import { UserChannelFilterComponent } from "./components/user-channel-filter/user-channel-filter.component";
import { UserFormulaComponent } from "./components/user-formula/user-formula.component";
import { UserPrefsNavCardComponent } from "./components/user-prefs-nav-card/user-prefs-nav-card.component";
import { UserUnitMappingTableComponent } from "./components/user-unit-mapping/unit-filter-table.component";
import { UserUnitMappingComponent } from "./components/user-unit-mapping/user-unit-mapping.component";
// This Module's Services
import { AccordionModule } from "primeng/accordion";
import { InputNumberModule } from "primeng/inputnumber";
import { TabMenuModule } from "primeng/tabmenu";
import { ToolbarModule } from "primeng/toolbar";
import { PhysDimService } from "../administration/units/physicaldimension.service";
import { UnitService } from "../administration/units/unit.service";
import { ApiTokenComponent } from "./apitoken/apitoken.component";
import { ApiTokenService } from "./apitoken/apitoken.service";
import { UserSettingsComponent } from "./components/user-settings/user-settings.component";
import { UserChannelFilterPreferenceService } from "./services/user-channel-filter.service";
import { UserFormulaBaseService } from "./services/user-formula-base.service";
import { UserFormulaService } from "./services/user-formula.service";
import { UserUnitPreferenceService } from "./services/user-unit-mapping.service";
import { UserPrefsRoutingModule } from "./user-prefs.routing.module";
export { ChannelFilterPreference, UserChannelFilterPreferenceService } from "./services/user-channel-filter.service";
export { ScopedUnitPreference, UnitPreference, UserUnitPreferenceService } from "./services/user-unit-mapping.service";

@NgModule({
  imports: [
    UserPrefsRoutingModule,
    MDMCoreModule,
    ConfirmDialogModule,
    MenuModule,
    TableModule,
    ToolbarModule,
    TooltipModule,
    DialogModule,
    DropdownModule,
    InputTextModule,
    CheckboxModule,
    TabMenuModule,
    DropdownModule,
    InputTextareaModule,
    ConfirmDialogModule,
    AccordionModule,
    InputNumberModule,
  ],
  declarations: [
    UserPrefsNavCardComponent,
    UserUnitMappingTableComponent,
    UserUnitMappingComponent,
    UserFormulaComponent,
    UserChannelFilterComponent,
    ApiTokenComponent,
    UserSettingsComponent,
  ],
  exports: [UserPrefsNavCardComponent],
  providers: [
    ConfirmationService,
    //DialogService,
    PreferenceService,
    UnitService,
    PhysDimService,
    UserUnitPreferenceService,
    UserFormulaService,
    UserFormulaBaseService,
    ApiTokenService,
    ConfirmationService,
    UserChannelFilterPreferenceService,
  ],
})
export class UserPrefsModule {}
