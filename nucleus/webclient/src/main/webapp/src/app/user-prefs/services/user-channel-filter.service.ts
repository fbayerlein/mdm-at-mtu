/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { forkJoin, Observable } from "rxjs";
import { concatMap, map, take } from "rxjs/operators";
import { AuthenticationService } from "../../core/authentication/authentication.service";

class ChannelFilter {
  value: string;

  constructor(value?: string) {
    this.value = value || ".+";
  }
}

/**
 * the editable column needs to bind an object
 * directly bind + modify the string value of an array, does not work
 */
export class EditableChannelFilterPreference {
  source: string;
  displName: string;
  channelFilters: ChannelFilter[] = [];
  isDefault: boolean;

  constructor(channelFilterPref: ChannelFilterPreference) {
    this.source = channelFilterPref.source;
    this.displName = channelFilterPref.displName;
    this.channelFilters = channelFilterPref.channelFilters.map((cf) => new ChannelFilter(cf));
    this.isDefault = channelFilterPref.isDefault;
  }

  public getFilters(): string[] {
    return this.channelFilters.map((cf) => cf.value);
  }

  public addChannelFilter() {
    this.channelFilters.push(new ChannelFilter());
  }

  public toChannelFilterPrefence() {
    return new ChannelFilterPreference(
      this.displName,
      this.channelFilters.map((cf) => cf.value),
      this.source,
      this.isDefault,
    );
  }
}

export class ChannelFilterPreference {
  source: string;
  displName: string;
  channelFilters: string[] = [];
  isDefault: boolean;

  constructor(displName: string, channelFilters: string[] = [], source = "*", isDefault = false) {
    this.source = source;
    this.displName = displName;
    this.channelFilters = channelFilters;
    this.isDefault = isDefault;
  }
}

export const CHANNEL_FILTER_PREF_KEY = "user-prefs.channel-filter-prefs";

@Injectable()
export class UserChannelFilterPreferenceService {
  public readonly NEW_CHANNEL_FILTER_PREF_NAME_KEY = CHANNEL_FILTER_PREF_KEY + ".no-channel-filter-pref-selected";
  public readonly EMPTY_CHANNEL_FILTER_PREF: ChannelFilterPreference = new ChannelFilterPreference(this.NEW_CHANNEL_FILTER_PREF_NAME_KEY);

  constructor(private preferenceService: PreferenceService, private authService: AuthenticationService) {}

  getUserChannelFilterPreferences(): Observable<ChannelFilterPreference[]> {
    return forkJoin([
      this.authService.getCurrentUserName().pipe(take(1)),
      this.preferenceService.getPreferenceForScope(Scope.USER, CHANNEL_FILTER_PREF_KEY),
    ]).pipe(
      map(([username, preferences]) =>
        this.preferenceToChannelFilterPref(
          preferences.filter((pref) => pref.user === undefined || pref.user === "" || pref.user === username),
        ),
      ),
    );
  }

  saveUserChannelFilterPreferences(userUnitPrefs: ChannelFilterPreference[]) {
    return this.authService.getCurrentUserName().pipe(
      take(1),
      concatMap((username) => this.preferenceService.savePreference(this.ChannelFilterPreferenceToPreference(userUnitPrefs, username))),
    );
  }

  getChannelFilterPreferenceClone(chnlFltrPref: ChannelFilterPreference): EditableChannelFilterPreference {
    if (chnlFltrPref === undefined) {
      return undefined;
    }
    return new EditableChannelFilterPreference(chnlFltrPref);
  }

  getChannelFilterPreferenceCloneForEdit(chnlFltrPref: EditableChannelFilterPreference): EditableChannelFilterPreference {
    if (chnlFltrPref === undefined) {
      return undefined;
    }
    return new EditableChannelFilterPreference(this.cloneChannelFilterPreference(chnlFltrPref, true));
  }

  private cloneChannelFilterPreference(chnlFltrPref: EditableChannelFilterPreference, forEdit: boolean): ChannelFilterPreference {
    if (chnlFltrPref === undefined) {
      return undefined;
    }
    return new ChannelFilterPreference(
      chnlFltrPref.displName,
      forEdit ? chnlFltrPref.getFilters() : chnlFltrPref.getFilters().filter((cf) => cf && cf.trim().length > 0),
      chnlFltrPref.source,
      chnlFltrPref.isDefault,
    );
  }

  getChannelFilterPreferenceCloneForSave(chnlFltrPref: EditableChannelFilterPreference): ChannelFilterPreference {
    return this.cloneChannelFilterPreference(chnlFltrPref, false);
  }

  public sortChannelFilterPreferences<T extends ChannelFilterPreference>(chnlFltrPrefs: T[]): T[] {
    return chnlFltrPrefs.sort((up1, up2) => {
      let retVal = 0;
      if (retVal === 0) {
        retVal = up1.displName.localeCompare(up2.displName);
      }
      if (retVal === 0) {
        retVal = up1.source.localeCompare(up2.source);
      }
      return retVal;
    });
  }

  private preferenceToChannelFilterPref(prefs: Preference[]): ChannelFilterPreference[] {
    let result: ChannelFilterPreference[] = [];
    if (prefs !== undefined && prefs.length > 0) {
      prefs.forEach((p) => {
        result = result.concat(JSON.parse(p.value) as ChannelFilterPreference[]);
      });
      result.forEach((pref) => {
        if (pref.isDefault === undefined) {
          pref.isDefault = false;
        }
      });
    }
    return result;
  }

  private ChannelFilterPreferenceToPreference(userUnitPrefs: ChannelFilterPreference[], username: string) {
    const pref = new Preference();
    userUnitPrefs.forEach((pref) => {
      if (pref.isDefault === undefined) {
        pref.isDefault = false;
      }
    });
    pref.value = JSON.stringify(userUnitPrefs);
    pref.key = CHANNEL_FILTER_PREF_KEY;
    pref.scope = Scope.USER;
    pref.user = username;
    return pref;
  }
}
