/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, Output } from "@angular/core";
import { SelectItem } from "primeng/api";
import { PhysDimInfo, UnitInfo } from "../../../administration/units/unit.service";
import { UnitFilter } from "../../services/user-unit-mapping.service";

@Component({
  selector: "mdm-unit-filter-table",
  templateUrl: "unit-filter-table.component.html",
  styleUrls: ["./unit-filter-table.component.css"],
})
export class UserUnitMappingTableComponent {
  @Input() environments: SelectItem[];
  @Input() env: string;
  @Input() unitFilters: UnitFilter[];
  @Output() remove = new EventEmitter<UnitFilter>();

  @Input() paramPhysDims: PhysDimInfo[] = [];

  onSet(field: string, rowIdx: number): void {
    if (rowIdx >= 0 && rowIdx < this.unitFilters.length) {
      if (field === "pd") {
        const physDimUnitNames: string[] = this.getAvailableUnits(this.unitFilters[rowIdx]).map((uf) => uf.unitName);
        if (!physDimUnitNames.includes(this.unitFilters[rowIdx].unit)) {
          this.unitFilters[rowIdx].unit = "";
        }
      }
    }
  }

  addUnitFilter() {
    this.unitFilters.push(new UnitFilter("", ""));
  }

  removeUnitFilter(rowIdx: number) {
    if (rowIdx >= 0 && rowIdx < this.unitFilters.length) {
      this.remove.emit(this.unitFilters.splice(rowIdx, 1)[0]);
    }
  }

  getAvailableUnits(unitFilter: UnitFilter): UnitInfo[] {
    let retVal: UnitInfo[] = undefined;
    if (this.paramPhysDims !== undefined && unitFilter !== undefined && unitFilter.physDim !== undefined) {
      const physDim = this.paramPhysDims.find((pd) => pd.name === unitFilter.physDim);
      retVal = physDim ? physDim.unitList : undefined;
    }
    return retVal === undefined ? [] : retVal;
  }

  getSourceDisplName(): string {
    let envDisplName = this.env;
    if (this.environments !== undefined && this.env !== undefined) {
      const displNames: string[] = this.environments.filter((si) => si.value === this.env).map((si) => si.label);
      envDisplName = displNames.length === 0 ? this.env : displNames[0];
    }
    return envDisplName;
  }
}
