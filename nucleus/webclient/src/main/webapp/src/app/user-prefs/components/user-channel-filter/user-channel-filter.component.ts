/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Scope } from "@core/preference.service";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService, SelectItem, SelectItemGroup } from "primeng/api";
import { EditableColumn } from "primeng/table";
import { Subscription, take } from "rxjs";
import {
  ChannelFilterPreference,
  CHANNEL_FILTER_PREF_KEY,
  EditableChannelFilterPreference,
  UserChannelFilterPreferenceService,
} from "../../services/user-channel-filter.service";

@Component({
  selector: "mdm-user-channel-filter",
  templateUrl: "user-channel-filter.component.html",
  styleUrls: ["./user-channel-filter.component.css"],
  providers: [EditableColumn],
})
export class UserChannelFilterComponent implements OnInit, OnDestroy, IStatefulComponent {
  private readonly MSG_PREFIX: string = CHANNEL_FILTER_PREF_KEY;
  private readonly MSG_ERR_PREFIX: string = this.MSG_PREFIX + ".err-cannot-";
  private readonly MSG_POSTFIX: string = "-channel-filter-prefs";

  private userChnlFltrPrefList: SelectItem[] = [];
  private selectedChnlFltrPrefVal: string;
  public selectedChnlFltrPrefIdx = -1;
  currentChnlFltrPref: EditableChannelFilterPreference;
  public groupedUserChnlFltrPrefs = [] as SelectItemGroup[];
  public selectedChnlFltrPrefDisplayName: string;

  private chnlFltrPreferences: ChannelFilterPreference[] = [];
  labelNewChnlFltrPref = "";

  public isUserPrefChannelFilterOpen = true;

  tmpChnlFltrPrefDelete: ChannelFilterPreference;
  isShowDialogChnlFltrPrefDelete = false;
  tmpChnlFltrPrefEdit: ChannelFilterPreference;
  isShowDialogChnlFltrPrefEdit = false;
  isSaveMode = false;
  chnlFltrPrefAlreadyExists: string;

  loadSubcription: Subscription;

  environments: Node[];

  private sourceSelectList: SelectItem[] = [];

  takeOverDialogVisible = false;
  takeOverChannelFilterPreference = new ChannelFilterPreference(this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY);
  takeOverNameExists = false;

  constructor(
    private nodeService: NodeService,
    private userChnlFltrPrefService: UserChannelFilterPreferenceService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
    private stateService: StateService,
  ) {
    this.currentChnlFltrPref = this.getNewChannelFilterPreference();
    this.labelNewChnlFltrPref = this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY;
    this.stateService.register(this, "user-channel-filter");
  }

  getState(): unknown {
    return {}; // we do not save anything in global state
  }

  applyState(state: unknown) {
    if (state["channelFilterPreference"]) {
      this.takeOverChannelFilterPreference = state["channelFilterPreference"] as ChannelFilterPreference;
      this.takeOverChannelFilterPreference.isDefault = false;
      this.takeOverDialogVisible = true;
    }
  }

  takeOver() {
    this.userChnlFltrPrefService.getUserChannelFilterPreferences().subscribe((prefs) => {
      if (prefs.findIndex((p) => p.displName === this.takeOverChannelFilterPreference.displName) >= 0) {
        this.takeOverNameExists = true;
      } else {
        this.takeOverNameExists = false;
        prefs.push(this.takeOverChannelFilterPreference);
        this.userChnlFltrPrefService.saveUserChannelFilterPreferences(prefs).subscribe(() => {
          this.takeOverDialogVisible = false;
          this.reloadUserPrefs();
        });
      }
    });
  }

  getStateForTakeOver(): unknown {
    return {
      "user-channel-filter": {
        channelFilterPreference: this.currentChnlFltrPref.toChannelFilterPrefence(),
      },
    };
  }

  ngOnDestroy() {
    this.stateService.unregister(this);
  }

  ngOnInit() {
    this.nodeService
      .getRootNodes()
      .pipe(take(1))
      .subscribe({
        next: (envs) => {
          const globalNode = {
            id: "*",
            name: "Global",
            sourceName: "*",
            type: "Environment",
            sourceType: "Environment",
          } as Node;

          this.environments = [globalNode].concat(envs);
          this.mapChnlFltrPrefOptions();
        },
        error: (error) =>
          this.translateService
            .instant("user-prefs.unit-prefs.err-cannot-load-data-sources")
            .subscribe((msg) => this.notificationService.notifyError(msg, error)),
      });
    this.currentChnlFltrPref = this.getNewChannelFilterPreference();

    this.reloadUserPrefs();
  }

  private reloadUserPrefs(source?: string, displName?: string) {
    this.chnlFltrPreferences = [];
    this.currentChnlFltrPref = this.getNewChannelFilterPreference();
    this.loadSubcription = this.userChnlFltrPrefService
      .getUserChannelFilterPreferences()
      .pipe()
      .subscribe(
        (userPrefs) => this.mapChnlFltrPrefs(userPrefs, source, displName),
        (error) =>
          this.notificationService.notifyError(this.translateService.instant(this.MSG_ERR_PREFIX + "load" + this.MSG_POSTFIX), error),
      );
  }

  private mapChnlFltrPrefOptions() {
    if (this.environments !== undefined) {
      this.userChnlFltrPrefList = this.chnlFltrPreferences.map(
        (pref) =>
          <SelectItem>{
            value: this.getChannelFilterPreferenceStringValue(pref),
            label: this.getChannelFilterPreferenceDisplayName(pref),
          },
      );
      if (this.selectedChnlFltrPrefIdx >= 0) {
        this.selectedChnlFltrPrefDisplayName = this.userChnlFltrPrefList[this.selectedChnlFltrPrefIdx].label;
      }
      this.generateGroupedUserPreferences(this.userChnlFltrPrefList);
    }
  }

  private mapChnlFltrPrefs(chnlFltrPrefs: ChannelFilterPreference[], source?: string, displName?: string) {
    if (this.labelNewChnlFltrPref === "" || this.labelNewChnlFltrPref === this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY) {
      this.translateService
        .get(this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY)
        .subscribe((msg: string) => this.setNewChnlFltrPrefLabel(msg));
    }

    if (chnlFltrPrefs.length === 0) {
      this.chnlFltrPreferences = new Array<ChannelFilterPreference>();
      this.chnlFltrPreferences.push(
        this.userChnlFltrPrefService.getChannelFilterPreferenceCloneForSave(this.getNewChannelFilterPreference()),
      );
    } else {
      this.chnlFltrPreferences = this.userChnlFltrPrefService.sortChannelFilterPreferences(chnlFltrPrefs);
    }

    this.mapChnlFltrPrefOptions();

    if (source !== undefined && displName !== undefined) {
      const idx = this.chnlFltrPreferences.findIndex((up) => up.source === source && up.displName === displName);
      this.selectChannelFilterPreference(this.chnlFltrPreferences[idx]);
    } else {
      this.selectChannelFilterPreference(this.chnlFltrPreferences[0]);
      this.loadSubcription.unsubscribe();
    }
  }

  private setNewChnlFltrPrefLabel(label: string) {
    this.labelNewChnlFltrPref = label;
    this.userChnlFltrPrefList.forEach((pref) => {
      if (pref.value === this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY) {
        pref.label = label;
      }
    });
  }

  public onChnlFltrPreferenceDefaultChange(checked: boolean) {
    if (this.selectedChnlFltrPrefIdx >= 0) {
      if (checked && this.chnlFltrPreferences.length > 1) {
        this.chnlFltrPreferences.forEach((up, idx) => {
          up.isDefault = idx === this.selectedChnlFltrPrefIdx;
        });
      } else if (!checked && this.selectedChnlFltrPrefIdx < this.chnlFltrPreferences.length) {
        this.chnlFltrPreferences[this.selectedChnlFltrPrefIdx].isDefault = false;
      }
    }
  }

  private getChannelFilterPreferenceDisplayName(pref: ChannelFilterPreference): string {
    const sourceDisplNames: string[] = this.getSourceSelectList()
      .filter((sl) => sl.value === pref.source)
      .map((sl) => sl.label);
    return (
      (pref.displName === this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY ? this.labelNewChnlFltrPref : pref.displName) +
      " (" +
      (sourceDisplNames.length === 0 ? pref.source : sourceDisplNames[0]) +
      ")"
    );
  }

  private getChannelFilterPreferenceStringValue(pref: ChannelFilterPreference): string {
    return pref.displName + ":" + pref.source;
  }

  private selectChannelFilterPreference(pref: ChannelFilterPreference) {
    this.selectedChnlFltrPrefIdx = this.chnlFltrPreferences.findIndex((up) => up.source === pref.source && up.displName === pref.displName);
    this.selectedChnlFltrPrefVal = this.getChannelFilterPreferenceStringValue(pref);
    this.selectedChnlFltrPrefDisplayName = this.getChannelFilterPreferenceDisplayName(pref);
    this.currentChnlFltrPref = this.userChnlFltrPrefService.getChannelFilterPreferenceClone(pref);
    this.currentChnlFltrPref["json"] = JSON.stringify(this.currentChnlFltrPref);
  }

  private checkForChangeAndCallFunc(callback: Function) {
    let isChanged = false;
    if (
      this.currentChnlFltrPref !== undefined &&
      (this.selectedChnlFltrPrefVal !== this.labelNewChnlFltrPref || this.currentChnlFltrPref.channelFilters.length > 0)
    ) {
      isChanged =
        JSON.stringify(this.userChnlFltrPrefService.getChannelFilterPreferenceCloneForEdit(this.currentChnlFltrPref)) !==
        this.currentChnlFltrPref["json"];
    }
    if (isChanged) {
      this.translateService.get(this.MSG_PREFIX + ".msg-confirm-unsaved-change").subscribe((msg) =>
        this.confirmationService.confirm({
          message: msg,
          key: "chnlFltrPrefConfirmation",
          header: this.translateService.instant(this.MSG_PREFIX + ".confirmation-title"),
          accept: callback,
        }),
      );
    } else {
      callback.call(this);
    }
  }

  public onNewChnlFltrPref(e: any): void {
    e.stopPropagation();
    this.chnlFltrPrefAlreadyExists = undefined;
    this.checkForChangeAndCallFunc(() => this.confirmedNewChnlFltrPref());
  }

  confirmedNewChnlFltrPref(): void {
    this.chnlFltrPrefAlreadyExists = undefined;
    this.tmpChnlFltrPrefEdit = new ChannelFilterPreference("");
    this.isShowDialogChnlFltrPrefEdit = true;
    this.isSaveMode = false;
  }

  hideDialogChnlFltrPref(): void {
    if (this.isShowDialogChnlFltrPrefEdit) {
      this.cancelDialogChnlFltrPref();
    }
  }

  cancelDialogChnlFltrPref(): void {
    this.tmpChnlFltrPrefEdit = undefined;
    this.chnlFltrPrefAlreadyExists = undefined;
    this.isShowDialogChnlFltrPrefEdit = false;
    this.isSaveMode = false;
  }

  confirmDialogChnlFltrPref(): void {
    this.selectChannelFilterPreference(this.tmpChnlFltrPrefEdit);
    if (
      this.chnlFltrPreferences.length === 1 &&
      (this.chnlFltrPreferences[0].displName === this.labelNewChnlFltrPref ||
        this.chnlFltrPreferences[0].displName === this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY)
    ) {
      this.chnlFltrPreferences.splice(0, 1);
    }

    this.currentChnlFltrPref["json"] = "";
    const pref = this.userChnlFltrPrefService.getChannelFilterPreferenceCloneForSave(this.currentChnlFltrPref);
    pref["new"] = true;
    this.chnlFltrPreferences.push(pref);
    this.userChnlFltrPrefService.sortChannelFilterPreferences(this.chnlFltrPreferences);
    this.selectedChnlFltrPrefIdx = this.chnlFltrPreferences.findIndex((up) => up["new"] === true);
    delete pref["new"];

    this.mapChnlFltrPrefOptions();
    this.cancelDialogChnlFltrPref();
  }

  private displNameCheckUnique(): void {
    let chnlFltrPref = undefined;

    if (this.tmpChnlFltrPrefEdit !== undefined && this.tmpChnlFltrPrefEdit.displName.trim().length > 0) {
      chnlFltrPref = this.chnlFltrPreferences.find(
        (up) =>
          up.displName === this.tmpChnlFltrPrefEdit.displName &&
          up.source === this.tmpChnlFltrPrefEdit.source &&
          (up.displName !== this.currentChnlFltrPref.displName || up.source !== this.currentChnlFltrPref.source),
      );
    }

    if (chnlFltrPref !== undefined || this.labelNewChnlFltrPref === this.tmpChnlFltrPrefEdit.displName) {
      this.chnlFltrPrefAlreadyExists = this.translateService.instant(this.MSG_PREFIX + ".err-already-exists" + this.MSG_POSTFIX, {
        displName: this.tmpChnlFltrPrefEdit.displName,
      });
    } else {
      this.chnlFltrPrefAlreadyExists = undefined;
    }
  }

  public onShowChnlFltrPrefSaveModal(e: any): void {
    e.stopPropagation();
    this.tmpChnlFltrPrefEdit = this.userChnlFltrPrefService.getChannelFilterPreferenceCloneForSave(this.currentChnlFltrPref);
    this.tmpChnlFltrPrefEdit.displName =
      this.currentChnlFltrPref.displName === this.userChnlFltrPrefService.NEW_CHANNEL_FILTER_PREF_NAME_KEY ||
      this.currentChnlFltrPref.displName === this.labelNewChnlFltrPref
        ? ""
        : this.currentChnlFltrPref.displName;
    this.displNameCheckUnique();
    this.isShowDialogChnlFltrPrefEdit = true;
    this.isSaveMode = true;
  }

  public onKeyUpDialogChnlFltrPref(e: KeyboardEvent) {
    e.stopPropagation();
    if (this.tmpChnlFltrPrefEdit !== undefined && this.tmpChnlFltrPrefEdit.displName.trim().length > 0) {
      this.displNameCheckUnique();
      if (this.chnlFltrPrefAlreadyExists === undefined && e.code === "Enter") {
        if (this.isSaveMode) {
          this.saveDialogChnlFltrPref();
        } else {
          this.confirmDialogChnlFltrPref();
        }
      }
    }
  }

  saveDialogChnlFltrPref(): void {
    if (this.isShowDialogChnlFltrPrefEdit) {
      this.saveChnlFltrPrefs("save", () => this.cancelDialogChnlFltrPref(), this.tmpChnlFltrPrefEdit);
    }
    this.cancelDialogChnlFltrPref();
  }

  private saveChnlFltrPrefs(msgKey: string, cancelCallback: Function, changedPref?: ChannelFilterPreference) {
    const source = changedPref === undefined ? undefined : changedPref.source;
    const displName = changedPref === undefined ? undefined : changedPref.displName;

    const replacement: ChannelFilterPreference[] = new Array<ChannelFilterPreference>();
    for (let i = 0; i < this.chnlFltrPreferences.length; i++) {
      if (i === this.selectedChnlFltrPrefIdx) {
        if (changedPref !== undefined) {
          replacement.push(changedPref);
        }
      } else {
        replacement.push(this.chnlFltrPreferences[i]);
      }
    }
    this.userChnlFltrPrefService.saveUserChannelFilterPreferences(replacement).subscribe(
      () => {
        cancelCallback.call(this);
        if (changedPref === undefined) {
          this.reloadUserPrefs();
        } else {
          this.reloadUserPrefs(source, displName);
        }
      },
      (error) =>
        this.notificationService.notifyError(this.translateService.instant(this.MSG_ERR_PREFIX + msgKey + this.MSG_POSTFIX), error),
    );
  }

  public getSourceSelectList(): SelectItem[] {
    if (this.environments !== undefined && this.environments.length > 0 && this.sourceSelectList.length === 0) {
      this.sourceSelectList = this.environments.map(
        (env) =>
          <SelectItem>{
            value: env.sourceName,
            label: env.name,
          },
      );
    }

    return this.sourceSelectList;
  }

  public onDeleteChnlFltrPref(e: any) {
    e.stopPropagation();
    this.tmpChnlFltrPrefDelete = this.userChnlFltrPrefService.getChannelFilterPreferenceCloneForSave(this.currentChnlFltrPref);
    this.isShowDialogChnlFltrPrefDelete = true;
  }

  cancelRemoveChnlFltrPref() {
    this.tmpChnlFltrPrefDelete = undefined;
    this.isShowDialogChnlFltrPrefDelete = false;
  }

  confirmedRemoveChnlFltrPref() {
    if (this.isShowDialogChnlFltrPrefDelete) {
      this.saveChnlFltrPrefs("delete", () => this.cancelRemoveChnlFltrPref());
    }
    this.cancelRemoveChnlFltrPref();
  }

  private getNewChannelFilterPreference(): EditableChannelFilterPreference {
    return this.userChnlFltrPrefService.getChannelFilterPreferenceClone(this.userChnlFltrPrefService.EMPTY_CHANNEL_FILTER_PREF);
  }

  private generateGroupedUserPreferences(userChnlFltrPrefs: SelectItem[]): any {
    this.groupedUserChnlFltrPrefs = [];
    this.groupedUserChnlFltrPrefs.push({
      value: Scope.USER,
      items: userChnlFltrPrefs,
      label: Scope.toLabel(Scope.USER),
    });
    console.log(this.groupedUserChnlFltrPrefs);
  }

  public onClickChnlFltrPreference(userChnlFltrPref: SelectItem) {
    const idx = this.userChnlFltrPrefList.findIndex((uup) => uup.value === userChnlFltrPref.value);
    this.checkForChangeAndCallFunc(() => this.selectChannelFilterPreference(this.chnlFltrPreferences[idx]));

    if (document.getElementById("userChnlFltrPrefButton").getAttribute("aria-expanded") === "true") {
      document.getElementById("userChnlFltrPrefButton").click();
    }
  }

  public getSourceDisplName(): string {
    const src = this.currentChnlFltrPref.source;
    let envDisplName = src;
    if (this.environments !== undefined && src !== undefined) {
      const env: Node = this.environments.find((env) => env.sourceName === src);
      envDisplName = env === undefined ? src : env.name;
    }
    return envDisplName;
  }

  public removeChannelFilter(rowIdx: number): void {
    if (rowIdx >= 0 && rowIdx < this.currentChnlFltrPref.channelFilters.length) {
      this.currentChnlFltrPref.channelFilters.splice(rowIdx, 1);
    }
  }

  public addChannelFilter(): void {
    this.currentChnlFltrPref.addChannelFilter();
  }
}
