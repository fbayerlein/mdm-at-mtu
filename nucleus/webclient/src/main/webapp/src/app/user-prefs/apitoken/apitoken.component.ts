/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService } from "primeng/api";
import { ApiToken, ApiTokenService } from "./apitoken.service";

@Component({
  selector: "mdm-apitoken",
  templateUrl: "./apitoken.component.html",
})
export class ApiTokenComponent implements OnInit {
  public tableCols = [
    { field: "name", header: "user-prefs.apitoken.name" },
    { field: "created", header: "user-prefs.apitoken.created" },
  ];

  public tokenList: ApiToken[];

  // token to be created, carrying the user specified name
  public tokenToBeCreated: ApiToken;

  // token retrieved by the server, containing the token created by the server
  public createdToken: ApiToken;

  public displayCreateDialog = false;
  public displayTokenCreatedDialog = false;

  constructor(
    private apitokenService: ApiTokenService,
    private confirmationService: ConfirmationService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.refreshTokenList();
  }

  refreshTokenList() {
    this.apitokenService.getApiTokens().subscribe((tokens) => (this.tokenList = tokens));
  }

  showDialogToAdd() {
    this.tokenToBeCreated = new ApiToken();
    this.displayCreateDialog = true;
  }

  save() {
    this.apitokenService.createApiToken(this.tokenToBeCreated.name).subscribe((token) => {
      this.createdToken = token;
      this.tokenToBeCreated = undefined;
      this.displayCreateDialog = false;
      this.displayTokenCreatedDialog = true;
    });
  }

  closeTokenDialog() {
    this.apitokenService.getApiTokens().subscribe((tokens) => {
      this.tokenList = tokens;
      this.displayTokenCreatedDialog = false;
      this.createdToken = undefined;
    });
  }

  onRowDelete(tokenToDelete: ApiToken) {
    this.confirmationService.confirm({
      message: this.translateService.instant("user-prefs.apitoken.delete_confirmation", tokenToDelete),
      header: this.translateService.instant("user-prefs.apitoken.confimation_header"),
      acceptLabel: this.translateService.instant("user-prefs.apitoken.confimation_accept"),
      rejectLabel: this.translateService.instant("user-prefs.apitoken.confimation_reject"),
      accept: () => {
        this.apitokenService.deleteApiToken(tokenToDelete.id).subscribe((x) => {
          this.refreshTokenList();
        });
      },
    });
  }
}
