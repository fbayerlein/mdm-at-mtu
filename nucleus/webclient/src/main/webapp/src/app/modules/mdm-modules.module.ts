/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { FileExplorerModule } from "@file-explorer/file-explorer.module";
import { TabMenuModule } from "primeng/tabmenu";
import { ChartviewerModule } from "../chartviewer/chartviewer.module";
import { MDMCoreModule } from "../core/mdm-core.module";
import { MDMDetailModule } from "../details/mdm-detail.module";
import { MDMSearchModule } from "../search/mdm-search.module";
import { UserPrefsModule } from "../user-prefs/user-prefs.module";
import { MDMModulesRoutingModule } from "./mdm-modules-routing.module";
import { MDMModulesComponent } from "./mdm-modules.component";

@NgModule({
  imports: [
    MDMCoreModule,
    MDMModulesRoutingModule,
    MDMDetailModule,
    MDMSearchModule,
    ChartviewerModule,
    TabMenuModule,
    FileExplorerModule,
    UserPrefsModule,
  ],
  declarations: [MDMModulesComponent],
  exports: [MDMModulesComponent],
})
export class MDMModulesModule {}
