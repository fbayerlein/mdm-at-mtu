/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { AuthenticationService } from "@core/authentication/authentication.service";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { concatMap, startWith, Subscription, tap } from "rxjs";

@Component({
  selector: "mdm-modules",
  templateUrl: "mdm-modules.component.html",
  styleUrls: ["./mdm-modules.component.css"],
  providers: [],
})
export class MDMModulesComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  public links: MenuItem[] = [];

  constructor(private translateService: TranslateService, private authenticationService: AuthenticationService) {}

  ngOnInit(): void {
    const rightKeys = [
      { key: "mdm-modules.context-bulk-editor.show" },
      { key: "mdm-modules.details.show" },
      { key: "mdm-modules.mdm-search.show" },
      { key: "mdm-modules.quick-viewer.show" },
      { key: "mdm-modules.xy-chart-viewer.show" },
      { key: "mdm-modules.user-prefs.show" },
      { key: "mdm-modules.file-explorer.show" },
      { key: "mdm-modules.recent-changes.show" },
    ];
    this.sub = this.authenticationService
      .hasFunctionalRights(rightKeys)
      .pipe(
        concatMap((rights) =>
          this.translateService.onLangChange.pipe(
            startWith(""),
            tap(() => this.setLinks(rights)),
          ),
        ),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  private setLinks(rights: Map<string, boolean>) {
    this.links = [
      {
        label: this.translateService.instant("modules.mdm-modules.details"),
        routerLink: "details",
        visible: rights.get("mdm-modules.details.show"),
      },
      {
        label: this.translateService.instant("modules.mdm-modules.context-bulk-editor"),
        routerLink: "context-bulk-editor",
        visible: rights.get("mdm-modules.context-bulk-editor.show"),
      },
      {
        label: this.translateService.instant("modules.mdm-modules.mdm-search"),
        routerLink: "search",
        visible: rights.get("mdm-modules.mdm-search.show"),
      },
      {
        label: this.translateService.instant("modules.mdm-modules.quick-viewer"),
        routerLink: "quickviewer",
        visible: rights.get("mdm-modules.quick-viewer.show"),
      },
      {
        label: this.translateService.instant("modules.mdm-modules.xy-chart-viewer"),
        routerLink: "xychartviewer",
        visible: rights.get("mdm-modules.xy-chart-viewer.show"),
      },
      {
        label: this.translateService.instant("modules.mdm-modules.file-explorer"),
        routerLink: "files",
        visible: rights.get("mdm-modules.file-explorer.show"),
      },
      {
        label: this.translateService.instant("modules.mdm-modules.user-prefs"),
        routerLink: "user-prefs",
        visible: rights.get("mdm-modules.user-prefs.show"),
      },
      {
        label: this.translateService.instant("modules.mdm-modules.recent-changes"),
        routerLink: "recent-changes",
        visible: rights.get("mdm-modules.recent-changes.show"),
      },
    ];
  }
}
