import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { AuthenticationService } from "@core/authentication/authentication.service";
import { Preference, PreferenceConstants, PreferenceService, Scope } from "@core/preference.service";
import { DateTime } from "luxon";
import { ConfirmationService } from "primeng/api";
import { combineLatest, concatMap, finalize, map, of, take, tap } from "rxjs";
import { AnnouncementConfig } from "../administration/announcements/announcement-config.component";

export interface AnnouncementDontRemind {
  id: string;
  dismissed: DateTime;
}

@Component({
  selector: "mdm-announcement",
  templateUrl: "./announcement.component.html",
})
export class AnnouncementComponent implements OnInit {
  public allAnnouncementConfigs: AnnouncementConfig[];
  public announcementConfigs: AnnouncementConfig[];
  public selectedAnnouncementConfig: AnnouncementConfig;
  public displayAnnouncementDialog = false;
  public displayAnnouncementMessageDialog = false;
  public dontRemind = false;
  private dontRemindMessages: AnnouncementDontRemind[] = [];

  constructor(
    private sanitizer: DomSanitizer,
    private confirmationService: ConfirmationService,
    private authenticationService: AuthenticationService,
    private preferenceService: PreferenceService,
  ) {}

  ngOnInit() {
    this.loadAnnouncementMessages();
  }

  private loadAnnouncementMessages() {
    return combineLatest([
      this.authenticationService.getCurrentUserName().pipe(
        concatMap((user) => this.preferenceService.getPreferenceForUser(PreferenceConstants.ANNOUNCEMENT_MESSAGE_REMIND, user)),
        concatMap((dontRemindPref) => {
          if (dontRemindPref[0]?.value) {
            return of(JSON.parse(dontRemindPref[0].value) as AnnouncementDontRemind[]);
          } else {
            return of([] as AnnouncementDontRemind[]);
          }
        }),
        tap((dontRemindMessages) => (this.dontRemindMessages = dontRemindMessages)),
      ),
      this.preferenceService.getPreference(PreferenceConstants.ANNOUNCEMENT_MESSAGE),
    ])
      .pipe(
        map(([, data]) => {
          const a: AnnouncementConfig[] = [];
          data.map((property) => {
            if (
              property.key.startsWith(PreferenceConstants.ANNOUNCEMENT_MESSAGE) &&
              property.key !== PreferenceConstants.ANNOUNCEMENT_MESSAGE_REMIND
            ) {
              const id = property.key.substring(PreferenceConstants.ANNOUNCEMENT_MESSAGE.length + 1);
              const parsedValue = JSON.parse(property.value);
              a.push({
                id: id,
                message: {
                  header: parsedValue.header,
                  body: this.sanitizer.bypassSecurityTrustHtml(parsedValue.body),
                  validFrom: parsedValue.validFrom,
                },
              });
            }
          });
          return a;
        }),
        tap((announcementConfigs) => {
          if (announcementConfigs && announcementConfigs.length > 0) {
            this.allAnnouncementConfigs = announcementConfigs;
            this.announcementConfigs = this.loadAnnouncementsToDisplay(announcementConfigs);
            this.dontRemind = false;
          }
        }),
      )
      .subscribe();
  }

  private loadAnnouncementsToDisplay(announcementConfigs: AnnouncementConfig[]) {
    return (this.announcementConfigs = announcementConfigs.filter((config) => {
      const validFrom = config.message.validFrom;
      const foundDontRemind = this.dontRemindMessages?.find((msg) => msg.id === config.id);
      let dismissedDate: DateTime;
      if (foundDontRemind) {
        dismissedDate = foundDontRemind?.dismissed;
      }
      if (!foundDontRemind || validFrom > dismissedDate) {
        return config;
      }
    }));
  }

  public closeAnnouncement() {
    if (this.dontRemind) {
      this.saveDontRemindPreference(this.selectedAnnouncementConfig).subscribe();
    }
  }

  public onRemoveAnnouncement(config: AnnouncementConfig) {
    this.confirmationService.confirm({
      key: "announcements",
      icon: "pi pi-question-circle",
      accept: () =>
        this.saveDontRemindPreference(config).subscribe(() => {
          this.spliceDisplayedAnnouncements(config);
        }),
      rejectVisible: true,
      reject: () => this.spliceDisplayedAnnouncements(config),
    });
  }

  private spliceDisplayedAnnouncements(config: AnnouncementConfig) {
    const index = this.announcementConfigs.indexOf(config);
    if (index > -1) {
      this.announcementConfigs.splice(index, 1);
    }
  }

  public onDontRemind(event: any) {
    this.dontRemind = event.checked;
  }

  private saveDontRemindPreference(config: AnnouncementConfig) {
    return this.authenticationService.getCurrentUserName().pipe(
      take(1),
      concatMap((user) => this.preferenceService.savePreference(this.createDontRemindPreference(config, user))),
      finalize(() => this.loadAnnouncementMessages()),
    );
  }

  private createDontRemindPreference(config: AnnouncementConfig, user: string) {
    const existingMessageIndex = this.dontRemindMessages.findIndex((msg) => msg.id === config.id);
    if (existingMessageIndex !== -1) {
      this.dontRemindMessages[existingMessageIndex].dismissed = DateTime.now();
    } else {
      this.dontRemindMessages.push({ id: config.id, dismissed: DateTime.now() });
    }
    // Cleanup old ones
    if (this.dontRemindMessages) {
      this.dontRemindMessages.forEach((msg) => {
        if (this.allAnnouncementConfigs && this.allAnnouncementConfigs.find((ann) => ann.id === msg.id) === undefined) {
          const index = this.dontRemindMessages.indexOf(msg);
          this.dontRemindMessages.splice(index, 1);
        }
      });
    }

    const pref = new Preference();
    pref.key = PreferenceConstants.ANNOUNCEMENT_MESSAGE_REMIND;
    pref.scope = Scope.USER;
    pref.user = user;
    pref.value = JSON.stringify(this.dontRemindMessages);
    return pref;
  }
}
