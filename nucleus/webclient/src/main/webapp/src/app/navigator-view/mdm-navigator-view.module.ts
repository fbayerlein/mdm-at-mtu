/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { ScrollTopModule } from "primeng/scrolltop";
import { SplitterModule } from "primeng/splitter";
import { ToolbarModule } from "primeng/toolbar";
import { MDMBasketModule } from "../basket/mdm-basket.module";
import { MDMCoreModule } from "../core/mdm-core.module";
import { MDMModulesModule } from "../modules/mdm-modules.module";
import { MDMNavigatorModule } from "../navigator/mdm-navigator.module";
import { MDMNavigatorViewRoutingModule } from "./mdm-navigator-view-routing.module";
import { MDMNavigatorViewComponent } from "./mdm-navigator-view.component";

@NgModule({
  imports: [
    MDMCoreModule,
    MDMNavigatorViewRoutingModule,
    MDMNavigatorModule,
    MDMModulesModule,
    MDMBasketModule,
    SplitterModule,
    ScrollTopModule,
    ToolbarModule,
  ],
  declarations: [MDMNavigatorViewComponent],
  exports: [MDMNavigatorViewRoutingModule],
})
export class MDMNavigatorViewModule {}
