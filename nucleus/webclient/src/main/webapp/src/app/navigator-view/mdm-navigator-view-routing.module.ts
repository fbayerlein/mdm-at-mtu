/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MDMNavigatorViewComponent } from "./mdm-navigator-view.component";

const navigatorViewRoutes: Routes = [
  {
    path: "",
    component: MDMNavigatorViewComponent,
    children: [{ path: "", loadChildren: () => import("../modules/mdm-modules.module").then((mod) => mod.MDMModulesModule) }],
  },
];

@NgModule({
  imports: [RouterModule.forChild(navigatorViewRoutes)],
  exports: [RouterModule],
})
export class MDMNavigatorViewRoutingModule {}
