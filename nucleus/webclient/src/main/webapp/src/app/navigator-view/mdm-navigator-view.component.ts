/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
import { Component, OnDestroy, OnInit } from "@angular/core";
import { PreferenceService } from "@core/preference.service";
import { map, tap } from "rxjs";

import { Node } from "../navigator/node";

@Component({
  selector: "mdm-navigator-view",
  templateUrl: "mdm-navigator-view.component.html",
  styleUrls: ["mdm-navigator-view.component.css"],
})
export class MDMNavigatorViewComponent implements OnInit, OnDestroy {
  selectedNode = new Node();
  activeNode: Node;
  subscription: any;
  isAnnouncementHeaderActive = false;

  constructor(private preferenceService: PreferenceService) {}

  ngOnInit() {
    this.preferenceService
      .getPreference("announcement.header_is_active")
      .pipe(
        map((prefs) => prefs.map((p) => p.value)),
        tap((data) => (this.isAnnouncementHeaderActive = data.length > 0 && data.every((d) => d === "true"))),
      )
      .subscribe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  updateSelectedNode(node: Node) {
    this.selectedNode = node;
  }
  updateActiveNode(node: Node) {
    this.activeNode = node;
  }
}
