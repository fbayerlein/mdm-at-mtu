/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { BasketService } from "@basket/basket.service";
import { MDMItem } from "@core/mdm-item";
import { ArrayUtilService } from "@core/services/array-util.service";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { Chart } from "chart.js";
import "chartjs-adapter-luxon";
import zoomPlugin from "chartjs-plugin-zoom";
import * as cloneDeep from "lodash/cloneDeep";
import { MenuItem } from "primeng/api";
import { UIChart } from "primeng/chart";
import { forkJoin, Observable, Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { TimezoneService } from "src/app/timezone/timezone-service";
import {
  ChannelSelectionRow,
  ChannelUnitUI,
  ChartData,
  ChartDataSet,
  ChartToolbarProperties,
  ChartXyDataSet,
  MeasuredValues,
  RequestOptions,
} from "../../model/chartviewer.model";
import { Channel } from "../../model/types/channel.class";
import { ChannelGroup } from "../../model/types/channelgroup.class";
import { Measurement } from "../../model/types/measurement.class";
import { ChartViewerDataService } from "../../services/chart-viewer-data.service";
import { ChartViewerFormulaService } from "../../services/chart-viewer-formula.service";
import { ChartViewerService } from "../../services/chart-viewer.service";
import { ColorService } from "../basic-chart.component";
import { XyChartViewerDataComponent } from "./xy-chart-viewer-data.component";
//import "chartjs-adapter-luxon";

Chart.register(zoomPlugin);

@Component({
  selector: "mdm-xy-chart-viewer",
  templateUrl: "./xy-chart-viewer.component.html",
  styleUrls: ["../../chart-viewer.style.css"],
  providers: [ColorService],
})
export class XyChartViewerComponent implements OnInit, OnDestroy, OnChanges, IStatefulComponent {
  @ViewChild("xyChart")
  public chart: UIChart;

  @Input()
  public measurements: Measurement[];

  // popup window subscription
  windowChangeSubscribe: Subscription;

  // the chart data
  public origData: ChartData;
  public data: ChartData;

  // options for measured value data request
  private requestOptions = new RequestOptions({} as RequestOptions);
  // chart properties set via toolbar
  public toolbarProperties = new ChartToolbarProperties();

  xAxesScale: string;
  yAxesScale: string;

  selectedMeasurements: Measurement[];
  selectedChannelGroups: ChannelGroup[];

  public onMeasurementsSelected($event: Measurement[]) {
    this.selectedMeasurements = $event;
  }

  public onChannelGroupsSelected($event: ChannelGroup[]) {
    this.selectedChannelGroups = $event;
  }

  public selectedChannelRows: ChannelSelectionRow[] = [];
  public groupedSelectedChannelRows: [string, ChannelSelectionRow[]][] = [];

  tableChannels: Node[];

  contextMenu: MenuItem[];

  displayFormats = {
    displayFormats: {
      millisecond: "HH:mm:ss.SSS",
      second: "HH:mm:ss",
      minute: "HH:mm",
      hour: "HH",
    },
  };

  constructor(
    private chartviewerDataService: ChartViewerDataService,
    private arrayUtil: ArrayUtilService,
    private translateService: TranslateService,
    private xyChartViewerData: XyChartViewerDataComponent,
    private userFormulaService: ChartViewerFormulaService,
    private timezoneService: TimezoneService,
    private basketService: BasketService,
    private chartViewerService: ChartViewerService,
    private stateService: StateService,
    private route: ActivatedRoute,
    private colorService: ColorService,
  ) {
    this.stateService.register(this, "xy-chart-viewer");
  }

  getState() {
    return {
      toolbarProperties: this.toolbarProperties,
      requestOptions: this.requestOptions,
      xAxesScale: this.xAxesScale,
    };
  }

  applyState(state: unknown) {
    if (state["toolbarProperties"]) {
      this.toolbarProperties = state["toolbarProperties"];
    }

    if (state["requestOptions"]) {
      this.requestOptions = new RequestOptions(state["requestOptions"]);
    }

    this.xAxesScale = state["xAxesScale"];
    this.yAxesScale = state["yAxesScale"];
  }

  ngOnInit() {
    this.createContextMenu();
    this.initChartData();

    this.windowChangeSubscribe = this.xyChartViewerData.onShowWindowChange().subscribe(() => {
      // publish the data only when clicking the "open popup" button and when there is data to display
      if (this.chart.options.scales !== undefined) {
        const arr = [];
        arr.push(this.chart.options);
        arr.push(this.data);
        this.xyChartViewerData.charting.next(arr);
      }
    });

    this.timezoneService.getTimeZone().subscribe(() => {
      this.doChart();
    });
  }

  ngOnDestroy() {
    if (this.windowChangeSubscribe) {
      this.windowChangeSubscribe.unsubscribe();
    }
    this.stateService.unregister(this);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["selectedChannelRows"]) {
      this.onDataSelectionChanged(this.selectedChannelRows);
    }
  }

  private createContextMenu() {
    this.contextMenu = [
      {
        label: this.translateService.instant("chartviewer.reset-scale"),
        icon: "pi pi-fw pi-undo",
        command: () => this.chart.chart.resetZoom(),
      },
      {
        label: this.translateService.instant("chartviewer.x-axis"),
        icon: "pi pi-fw pi-angle-double-right",
        items: [
          {
            label: this.translateService.instant("chartviewer.scale.category"),
            icon: "pi pi-fw pi-chart-bar",
            command: () => this.xaxis("category"),
          },
          {
            label: this.translateService.instant("chartviewer.scale.linear"),
            icon: "pi pi-fw pi-chart-line",
            command: () => this.xaxis("linear"),
          },
          {
            label: this.translateService.instant("chartviewer.scale.logarithmic"),
            icon: "pi pi-fw",
            command: () => this.xaxis("logarithmic"),
          },
          {
            label: this.translateService.instant("chartviewer.scale.time"),
            icon: "pi pi-fw pi-clock",
            command: () => this.xaxis("time"),
          },
        ],
      },
      {
        label: this.translateService.instant("chartviewer.y-axis"),
        icon: "pi pi-fw pi-angle-double-up",
        items: [
          {
            label: this.translateService.instant("chartviewer.scale.category"),
            icon: "pi pi-fw pi-chart-bar",
            command: () => this.yaxis("category"),
          },
          {
            label: this.translateService.instant("chartviewer.scale.linear"),
            icon: "pi pi-fw pi-chart-line",
            command: () => this.yaxis("linear"),
          },
          {
            label: this.translateService.instant("chartviewer.scale.logarithmic"),
            icon: "pi pi-fw",
            command: () => this.yaxis("logarithmic"),
          },
          {
            label: this.translateService.instant("chartviewer.scale.time"),
            icon: "pi pi-fw pi-clock",
            command: () => this.yaxis("time"),
          },
        ],
      },
    ];
  }

  private xaxis(type: string) {
    this.xAxesScale = type;
    this.doChart();
  }

  private yaxis(type: string) {
    this.yAxesScale = type;
    this.doChart();
  }

  /**************** Html-template listeners *****************************/

  /**
   * Draws chart when data selection changes
   * @param selectedData data object
   */
  public onDataSelectionChanged(selectedData: ChannelSelectionRow[]) {
    // force change detection
    this.selectedChannelRows = selectedData;

    this.groupedSelectedChannelRows = this.groupChannelSelectionRowsByChannelGroup(this.selectedChannelRows);
    if (!this.selectedChannelRows || this.selectedChannelRows.length === 0) {
      this.colorService.resetAssignedColors();
    }
    this.doChart();
  }

  /**
   *  download measurement , channel group and channels depending on what is selected
   * @param event
   */
  onAddItemsToShoppingListChanged(event: string) {
    const selectedOption = event;
    if (selectedOption === "Measurement") {
      this.basketService.addAll(this.selectedMeasurements.map((node) => new MDMItem(node.source, node.type, node.id)));
    } else if (selectedOption === "ChannelGroup") {
      this.basketService.addAll(this.selectedChannelGroups.map((node) => new MDMItem(node.source, node.type, node.id)));
    } else if (selectedOption === "Channel") {
      this.basketService.addAll(
        this.selectedChannelRows.map((node) => new MDMItem(node.yChannel.source, node.yChannel.type, node.yChannel.id)),
      );
      this.basketService.addAll(
        this.selectedChannelRows.map((node) => new MDMItem(node.xChannel.source, node.xChannel.type, node.xChannel.id)),
      );
    }
  }

  /**
   * Handles toolbar property changes
   * @param properties
   */
  public onToolbarPropertiesChanged(properties: ChartToolbarProperties) {
    this.toolbarProperties = properties;
    this.chartViewerService.setRescpectFlags(properties.respectFlags);
    this.groupOrUngroupData();
    if (this.chart !== undefined) {
      this.chart.options = { ...this.chart.options, ...properties.options };
      this.chart.options.legend = this.generateChartLegend();
      if (this.chart.data != undefined) {
        this.chart.data.datasets.forEach((dataSet) => {
          dataSet.showLine = properties.showLines;
          dataSet.pointRadius = properties.drawPoints ? 3 : 0;
          dataSet.borderWidth = properties.lineWidth;
          dataSet.lineTension = properties.lineTension;
          dataSet.fill = properties.fillArea;
          if (typeof dataSet.switchCharting === "function") {
            dataSet.switchCharting(properties.relativeDrawing);
          }
        });
        this.chart.reinit();
      }
    }
    // Force refresh on chart
    this.groupedSelectedChannelRows = this.groupChannelSelectionRowsByChannelGroup(this.selectedChannelRows);
    this.doChart();
  }

  private groupOrUngroupData() {
    this.data = this.toolbarProperties.groupMeasurements ? this.groupByLabel(cloneDeep(this.origData)) : this.origData;
  }

  // Sets new data request options and redraws the graph
  public onRequestOptionsChanged(options: RequestOptions) {
    this.requestOptions = options;
    this.doChart();
  }

  /********** private methods / class logic ********************************************************/

  // empty chart on initialization
  private initChartData() {
    const dataset = new ChartDataSet("No data", []);
    dataset.borderColor = "#fff";
    this.origData = new ChartData([], [dataset]);
    this.groupOrUngroupData();
  }

  /**
   * Loads measured values for current selection and draws the new chart.
   */
  private doChart() {
    if (this.arrayUtil.isNotEmpty(this.selectedChannelRows) && this.requestOptions != undefined) {
      const channelsByGroup = this.groupChannelRowsByChannelGroup(this.selectedChannelRows);

      this.requestMeasuredValues(channelsByGroup)
        .pipe(
          map((mvs) =>
            this.selectedChannelRows.map((row) => {
              const xMv = mvs.find((t) => t.channel.id === row.xChannel.id);
              const yMv = mvs.find((t) => t.channel.id === row.yChannel.id);

              /*if (!xMv) {
              throw new Error("Cannot find measured values for Channel " + row.xChannel.name + " (Id=" + row.xChannel.id + ").");
            }

            if (!yMv) {
              throw new Error("Cannot find measured values for Channel " + row.yChannel.name + " (Id=" + row.yChannel.id + ").");
            }*/

              if (xMv && yMv) {
                // transfer the measurement name as optional label
                yMv.measuredValues.optionalLabel = yMv.channel.measurement;
                const dataSet = ChartXyDataSet.fromMeasuredValues(
                  xMv.measuredValues,
                  yMv.measuredValues,
                  row.yChannel.id,
                  this.chartViewerService,
                );

                return this.applyChartProperties(dataSet);
              }
            }),
          ),
          map((sets) => sets.filter((ds) => ds !== undefined)),
          map((arr) => {
            return arr.map((obj, index) => {
              if (this.toolbarProperties.multipleAxes) return { ...obj, yAxisID: "y" + index };
              else return obj;
            });
          }),
          map(
            (sets) =>
              new ChartData(
                [],
                sets.reduce((a, b) => a.concat(b), []),
              ),
          ),
        )
        .subscribe((resp) => this.applyData(resp));
    } else {
      this.initChartData();
    }
  }

  private groupByLabel(xs): ChartData {
    const reducedMap = xs.datasets.reduce(function (rv, x) {
      (rv[x["label"]] = rv[x["label"]] || []).push(x);
      return rv;
    }, {});
    const groupedDatasets = [];
    Object.keys(reducedMap).forEach((key) => {
      let groupedSubDataset = null;
      reducedMap[key].forEach((dataset) => {
        if (groupedSubDataset == null) {
          const newDataset = dataset;
          groupedSubDataset = newDataset;
        } else {
          groupedSubDataset.data.push(...dataset.data);
        }
      });
      groupedDatasets.push(groupedSubDataset);
    });
    return new ChartData([], groupedDatasets);
  }

  /**
   * Requests the values of all Channels for all ChannelGroups. Returns an Observable containing
   * all Channels together with their MeasuredValues
   * @param channelsByGroup
   */
  private requestMeasuredValues(channelsByGroup: Record<string, Channel[]>) {
    const measuredValues: Observable<{ channel: Channel; measuredValues: MeasuredValues }[]>[] = [];
    for (const channelGroupId in channelsByGroup) {
      const channelGroup = this.selectedChannelRows.find((row) => row.channelGroup.id === channelGroupId).channelGroup;

      const obs = this.chartviewerDataService
        .loadValues(
          channelGroup,
          channelsByGroup[channelGroupId],
          this.requestOptions.startIndex,
          this.requestOptions.requestSize,
          this.requestOptions.getChunks(),
        )
        .pipe(map((mv) => this.userFormulaService.mergeMeasuredValuesByName(channelsByGroup[channelGroupId], mv)));

      measuredValues.push(obs);
    }

    return forkJoin(measuredValues).pipe(
      map(
        (mv) =>
          [].concat(...mv) as {
            channel: Channel;
            measuredValues: MeasuredValues;
          }[],
      ),
    );
  }

  /**
   * Group the ChannelSelectionRows by ChannelGroup, e.g. the returned record has the ID of the
   * channelGroup as key and the list of corresponding Channels (X and Y Channels) as value.
   * @param channelRows
   */
  private groupChannelRowsByChannelGroup(channelRows: ChannelSelectionRow[]) {
    return channelRows.reduce((previous, currentItem) => {
      const group = currentItem.channelGroup.id;
      if (!previous[group]) {
        previous[group] = [];
      }
      if (currentItem.xChannel && previous[group].findIndex((c) => c.id === currentItem.xChannel.id) === -1) {
        previous[group].push(currentItem.xChannel);
      }
      if (currentItem.yChannel && previous[group].findIndex((c) => c.id === currentItem.yChannel.id) === -1) {
        previous[group].push(currentItem.yChannel);
      }
      return previous;
    }, {} as Record<string, Channel[]>);
  }

  /**
   * Apply the ChartData to data and options attributes of the chart component
   * @param xValues
   */
  private applyData(resp: ChartData) {
    this.origData = resp;
    this.groupOrUngroupData();
    const that = this;
    if (this.chart !== undefined) {
      this.chart.options = {
        scales: {
          x: {
            type: this.xAxesScale
              ? this.xAxesScale
              : resp.datasets.every((ds: ChartXyDataSet) => ds.xScalarType === "DATE")
              ? "time"
              : "linear",
            title: {
              display: true,
              text: resp.datasets
                .map((ds: ChartXyDataSet) => ds.xLabel)
                .filter((l) => l != undefined)
                .filter(this.distinct)
                .join(", "),
            },
            time: this.displayFormats,
            adapters: {
              date: {
                setZone: true,
                zone: this.timezoneService.getSelectedTimezone(),
              },
            },
          },
        },
        legend: this.generateChartLegend(),
        plugins: {
          zoom: {
            pan: {
              enabled: true,
              mode: "xy",
              onDrag: true,
              treshold: 10,
              mouseButtonPan: "left",
            },
            zoom: {
              wheel: {
                enabled: true,
              },
              pinch: {
                enabled: true,
              },
              mode: "xy",
              scaleMode: "xy",
            },
          },
          tooltip: {
            callbacks: {
              label: function (context) {
                const ret: string[] = [];
                ret.push(`${that.translateService.instant("chartviewer.label")}: ${context.label}`);
                ret.push(`${that.translateService.instant("chartviewer.value")}: ${context.formattedValue}`);

                const channel: ChannelSelectionRow = that.selectedChannelRows.find(
                  (selectedChannelRow) => selectedChannelRow.yChannel.id === context.dataset.uniqueId,
                ) as ChannelSelectionRow;

                for (let i = 0; i < (channel.yChannel as ChannelUnitUI).columns.length; i++) {
                  ret.push(`${(channel.yChannel as ChannelUnitUI).columnsName[i]}: ${(channel.yChannel as ChannelUnitUI).columns[i]}`);
                }

                return ret;
              },
            },
          },
        },
      };

      function remove_first_and_last(value, index, ticks) {
        if (ticks && (index === 0 || index === ticks.length - 1)) {
          return null;
        }
        return value;
      }

      // clean up settings - these values are automatically added so they must be deleted
      delete this.chart.options.scales["y"];
      for (let i = 0; i <= this.data.datasets.length; i++) {
        delete this.chart.options.scales["y" + i];
      }

      if (this.toolbarProperties.multipleAxes) {
        // multiple axes with matching color
        this.data.datasets.map((data, index) => {
          this.chart.options.scales["y" + index] = {
            ticks: { callback: remove_first_and_last },
            type: this.yAxesScale,
            time: this.displayFormats,
            title: {
              display: true,
              text: data.label,
              color: data.borderColor,
            },
          };
        });
      } else {
        // default single axis mode
        this.chart.options.scales["y"] = {
          ticks: { callback: remove_first_and_last },
          type: this.yAxesScale,
          time: this.displayFormats,
        };
      }
    }
  }

  /**
   * Generate the chart legend with different dash styles
   * Identical function is used in the popup html, changes must be synchronized
   */
  private generateChartLegend() {
    return {
      display: this.toolbarProperties.options.legend.display,
      labels: {
        generateLabels: function (chart) {
          const data = chart.data;
          if (data.datasets != null && data.datasets.length > 0) {
            return data.datasets.map(function (dataset, i) {
              const meta = chart.getDatasetMeta(i);
              const ds = dataset;
              const stroke = ds.borderColor;
              let bw = ds.borderWidth;
              if (ds.borderDash != null && ds.borderDash.length > 0) {
                bw = 2;
              }
              return {
                text: dataset.label,
                fillStyle: "#FFFFFF",
                strokeStyle: stroke,
                lineWidth: bw,
                lineDash: ds.borderDash,
                hidden: meta.hidden,
                index: i,
                datasetIndex: i,
              };
            });
          }
          return [];
        },
      },
    };
  }

  /******* Helping functions */
  private groupChannelSelectionRowsByChannelGroup(channelRows: ChannelSelectionRow[]) {
    const arrRet: [string, ChannelSelectionRow[]][] = [];

    if (channelRows != undefined) {
      const mapChannelSelectionRows = new Map<string, ChannelSelectionRow[]>();
      const mapChannelGroupIds = new Map<string, string>();
      const mapChannelFrequencies = new Map<string, string[]>();

      channelRows.forEach((row) => {
        if (!mapChannelSelectionRows.has(row.channelGroup.id)) {
          mapChannelSelectionRows.set(row.channelGroup.id, []);
          mapChannelGroupIds.set(row.channelGroup.id, row.channelGroup.name);
        }
        mapChannelSelectionRows.get(row.channelGroup.id).push(row);

        if (!mapChannelFrequencies.has(row.channelGroup.name)) {
          mapChannelFrequencies.set(row.channelGroup.name, []);
        }

        if (mapChannelFrequencies.get(row.channelGroup.name).indexOf(row.channelGroup.id) === -1) {
          mapChannelFrequencies.get(row.channelGroup.name).push(row.channelGroup.id);
        }
      });

      if (this.toolbarProperties.groupMeasurements) {
        mapChannelFrequencies.forEach((v, k) => {
          const channelGrouped: ChannelSelectionRow[] = [];
          v.forEach((groupId) => channelGrouped.push(...mapChannelSelectionRows.get(groupId)));
          arrRet.push([k, channelGrouped]);
        });
      } else {
        mapChannelSelectionRows.forEach((v, k) => arrRet.push([mapChannelGroupIds.get(k), v]));
      }
    }

    return arrRet.sort((a, b) => a[0].toLowerCase().localeCompare(b[0].toLowerCase()));
  }

  /**
   * Filter duplicate values and only return distinct values  as array.
   *
   * Usage: [].filter(this.distinct);
   *
   * @param value
   * @param index
   * @param self
   * @returns array with distinct values
   */
  private distinct(value, index, self) {
    return self.indexOf(value) === index;
  }

  private applyChartProperties(dataset: ChartXyDataSet) {
    if (!this.toolbarProperties.drawPoints) {
      dataset.pointRadius = 0;
    }
    dataset.showLine = this.toolbarProperties.showLines;
    dataset.fill = this.toolbarProperties.fillArea;
    dataset.lineTension = this.toolbarProperties.lineTension;
    dataset.borderWidth = this.toolbarProperties.lineWidth;

    const { color, dash } = this.colorService.getColorAndDash(dataset.label + dataset.uniqueId);
    dataset.borderColor = color;
    dataset.backgroundColor = color;
    dataset.borderDash = dash;
    dataset.switchCharting(this.toolbarProperties.relativeDrawing);

    return dataset;
  }
}
