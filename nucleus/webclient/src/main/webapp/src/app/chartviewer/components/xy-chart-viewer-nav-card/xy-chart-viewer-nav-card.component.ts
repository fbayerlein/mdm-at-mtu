/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { MDMIdentifier } from "@core/mdm-identifier";
import { MDMItem } from "@core/mdm-item";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { StateService } from "@core/services/state.service.ts";
import { NavigatorService } from "@navigator/navigator.service";
import { Node } from "@navigator/node";
import { forkJoin, Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { TYPE_CHANNEL, TYPE_CHANNELGROUP, TYPE_MEASUREMENT } from "../../model/constants";
import { Measurement } from "../../model/types/measurement.class";
import { QueryConfig } from "../../model/types/query-config.class";
import { ChartViewerDataService } from "../../services/chart-viewer-data.service";
import { ChartViewerService } from "../../services/chart-viewer.service";

@Component({
  selector: "mdm-xy-chart-viewer-nav-card",
  templateUrl: "./xy-chart-viewer-nav-card.component.html",
})
export class XyChartViewerNavCardComponent implements OnInit, OnDestroy {
  public measurements: Measurement[] = [];

  // Subscriptions
  private selectedNodeSub: Subscription;

  private appendNodeSub: Subscription;

  private ignoreFirstChange = false;

  constructor(
    private navigatorService: NavigatorService,
    private notificationService: MDMNotificationService,
    private chartViewerService: ChartViewerService,
    private chartViewerDataService: ChartViewerDataService,
    private stateService: StateService,
  ) {
    this.stateService.register(this, "xy-chart-viewer-nav-card");
  }

  getState() {
    return {
      measurements: this.measurements.map((m) => ({ source: m.source, type: m.type, id: m.id } as MDMIdentifier)),
    };
  }
  applyState(state: unknown) {
    if (state["measurements"]) {
      this.loadMeasurements(state["measurements"] as MDMIdentifier[], true);
      this.ignoreFirstChange = true;
    }
  }

  ngOnInit() {
    this.selectedNodeSub = this.navigatorService
      .onSelectionChanged()
      .pipe(filter((obj: Node | MDMItem): obj is Node => obj instanceof Node))
      .subscribe(
        (node) => this.selectedNodeChanged(node),
        (error) => this.notificationService.notifyError("details.mdm-detail-view.cannot-update-node", error),
      );
    this.appendNodeSub = this.chartViewerService
      .onAppendNodeChange()
      .pipe()
      .subscribe(
        (node) => this.appendNode(node),
        (error) => this.notificationService.notifyError("details.mdm-detail-view.cannot-update-node", error),
      );
    this.chartViewerService.onQueryConfigChange().subscribe((conf) => this.reloadOnConfigChange(conf));
  }

  ngOnDestroy() {
    this.selectedNodeSub.unsubscribe();
    this.appendNodeSub.unsubscribe();
    this.stateService.unregister(this);
  }

  /**
   * Handle node selection in navigation tree.
   * Loads necessary data for xy chart, if not present:
   *  - Measurement:
   *    + Loaded if a channel or channelgroup is selected that is not present.
   *    + Also loads all channels and channelgroups for that measurement.
   *  - Channelgroups:
   *    + Loaded if a measurement is selected/loaded that is not present.
   *    + Also loads all channels for all newly loaded channelgroups.
   *  - Channels:
   *    + Allways loaded if some node is not present.
   *
   * @param node the selected node
   */
  private selectedNodeChanged(node: Node) {
    if (this.ignoreFirstChange) {
      this.ignoreFirstChange = false;
      return;
    }
    this.measurements = [];
    if (node !== undefined) {
      switch (node.type) {
        case TYPE_MEASUREMENT:
          this.selectedMeasurement(node, false);
          break;
        case TYPE_CHANNELGROUP:
          this.selectedChannelGroup(node, false);
          break;
        case TYPE_CHANNEL:
          this.selectedChannel(node, false);
          break;
        default:
          this.measurements = [];
          this.chartViewerService.sendNodeMeta(undefined);
      }
    }
  }

  private appendNode(ident: MDMIdentifier) {
    if (ident !== undefined) {
      switch (ident.type) {
        case TYPE_MEASUREMENT:
          this.selectedMeasurement(ident, true);
          break;
        case TYPE_CHANNELGROUP:
          this.selectedChannelGroup(ident, true);
          break;
        case TYPE_CHANNEL:
          this.selectedChannel(ident, true);
          break;
        default:
        // we don't have a default in this case
      }
    }
  }

  // reload all, if channel is not present yet.
  private selectedChannel(channel: MDMIdentifier, append: boolean) {
    if (!this.isChannelPresent(channel)) {
      this.loadMeasurements([channel], append);
    } else if (append) {
      this.chartViewerService.sendAppendMetaNode(channel);
    } else {
      this.chartViewerService.sendNodeMeta(channel);
    }
  }

  private selectedChannelGroup(channelGroup: MDMIdentifier, append: boolean) {
    if (!this.isChannelGroupPresent(channelGroup)) {
      this.loadMeasurements([channelGroup], append);
    } else if (append) {
      this.chartViewerService.sendAppendMetaNode(channelGroup);
    } else {
      this.chartViewerService.sendNodeMeta(channelGroup);
    }
  }

  private selectedMeasurement(measurement: MDMIdentifier, append: boolean) {
    if (!this.measurements.find((mea) => mea.id === measurement.id)) {
      this.loadMeasurements([measurement], append);
    } else if (append) {
      this.chartViewerService.sendAppendMetaNode(measurement);
    } else {
      this.chartViewerService.sendNodeMeta(measurement);
    }
  }

  private isChannelGroupPresent(ident: MDMIdentifier) {
    if (this.measurements != null && this.measurements.length > 0) {
      return this.measurements.find((mea) => mea.findChannelGroup(ident.id) !== undefined);
    }
  }

  private isChannelPresent(ident: MDMIdentifier) {
    if (this.measurements != null && this.measurements.length > 0) {
      return this.measurements.find((mea) => mea.findChannel(ident.id) !== undefined);
    }
  }

  private loadMeasurements(idents: MDMIdentifier[], append: boolean) {
    forkJoin(idents.map((n) => this.chartViewerDataService.loadMeasurement(n))).subscribe((meas) => {
      this.measurements = [...this.measurements, ...meas];

      for (const ident of idents) {
        if (append) {
          this.chartViewerService.sendAppendMetaNode(ident);
        } else {
          this.chartViewerService.sendNodeMeta(ident);
        }
      }
    });
  }

  private reloadOnConfigChange(conf: QueryConfig) {
    const tmp = new Node();
    if (this.measurements.length > 0) {
      tmp.type = this.measurements[0].type;
      tmp.id = this.measurements[0].id;
      tmp.sourceName = this.measurements[0].source;
      this.chartViewerDataService.loadMeasurement(tmp).subscribe((mea) => {
        // this.chartViewerService.sendNodeMeta(tmp);
        this.measurements[0] = mea;
      });
    }
  }
}
