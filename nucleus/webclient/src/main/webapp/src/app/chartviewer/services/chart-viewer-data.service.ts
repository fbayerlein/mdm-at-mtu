/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { MDMIdentifier } from "@core/mdm-identifier";
import { Observable, of, throwError } from "rxjs";
import { catchError, flatMap, map } from "rxjs/operators";
import { PropertyService } from "../../core/property.service";
import { Node } from "../../navigator/node";
import { Query, QueryService, Row } from "../../tableview/query.service";
import { VirtualChannelUnitUI } from "../../user-prefs/services/user-formula-base.service";
import { MeasuredValuesResponse, PreviewValueList } from "../model/chartviewer.model";
import { TYPE_CHANNEL, TYPE_CHANNELGROUP, TYPE_MEASUREMENT } from "../model/constants";
import { ChannelUnitUI } from "../model/types/channel-unit-ui.class";
import { Channel } from "../model/types/channel.class";
import { ChannelGroup } from "../model/types/channelgroup.class";
import { Measurement } from "../model/types/measurement.class";

@Injectable({
  providedIn: "root",
})
export class ChartViewerDataService {
  private _contextUrl: string;

  public resultLimit: number = undefined;
  public resultOffset = 0;

  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json",
    }),
  };

  constructor(private http: HttpClient, private _prop: PropertyService, private queryService: QueryService) {
    this._contextUrl = _prop.getUrl("mdm/environments");
  }

  /**
   * Loads preview value (if chunks > 0) or measured values (if chunks === 0).
   * @param channelGroup channel group node
   * @param channels channel nodes
   * @param startIndex start index to load the data
   * @param requestSize number of requested values
   * @param chunks preview chunks. If 0, preview is disabled and original values are loaded
   */
  loadValues(channelGroup: ChannelGroup, channels: Channel[], startIndex = 0, requestSize = 0, chunks = 0) {
    const typedChannels = this.getTypedChannels(channels);
    if (chunks > 0) {
      return this.loadPreviewValues(channelGroup, typedChannels, chunks, startIndex, requestSize);
    } else {
      return this.loadMeasuredValues(channelGroup, typedChannels, startIndex, requestSize);
    }
  }

  private getTypedChannels(channels: Channel[]): ChannelUnitUI[] {
    const typedChannels = channels.map((c) => this.getTypedChannel(c));
    return [].concat(...typedChannels);
  }

  private getTypedChannel(channel: Channel): ChannelUnitUI[] {
    if (channel instanceof VirtualChannelUnitUI) {
      const required: ChannelUnitUI[] = [];
      (<VirtualChannelUnitUI>channel).channels.forEach((requiredChannelData) => required.push(new ChannelUnitUI(requiredChannelData)));
      return required;
    } else if (channel instanceof ChannelUnitUI) {
      return [<ChannelUnitUI>channel];
    } else {
      return [new ChannelUnitUI(channel)];
    }
  }

  /**
   * Loads measured values
   * @param channelGroup channel group node
   * @param channels channel nodes
   * @param startIndex start index to load the data
   * @param requestSize number of requested values
   */
  loadMeasuredValues(channelGroup: ChannelGroup, channels: ChannelUnitUI[], startIndex = 0, requestSize = 0) {
    const readRequest = {
      channelGroupId: channelGroup.id,
      channelIds: channels !== undefined ? channels.map((channel) => channel.id) : [],
      unitIds: channels !== undefined ? channels.map((channel) => channel.selectedUnitId) : [],
      start_index: startIndex,
      request_size: requestSize,
      valuesMode: "CALCULATED",
    };

    return this.http
      .post<MeasuredValuesResponse>(this._contextUrl + "/" + channelGroup.source + "/values/read", readRequest, this.httpOptions)
      .pipe(
        map((res) => res.values),
        map((measurementValues) => measurementValues.sort((a, b) => a.name.localeCompare(b.name))),
        catchError((e) => addErrorDescription(e, "Could not load measured values!")),
      );
  }

  /**
   * Loads preview values.
   * @param channelGroup channel group node
   * @param channels channel nodes
   * @param chunks preview chunks
   * @param startIndex start index to load the data
   * @param requestSize number of requested values
   */
  loadPreviewValues(channelGroup: ChannelGroup, channels: ChannelUnitUI[], chunks: number, startIndex = 0, requestSize = 0) {
    const readRequest = {
      channelGroupId: channelGroup.id,
      channelIds: channels !== undefined ? channels.map((channel) => channel.id) : [],
      unitIds: channels !== undefined ? channels.map((channel) => channel.selectedUnitId) : [],
      start_index: startIndex,
      request_size: requestSize,
      valuesMode: "CALCULATED",
    };
    const previewRequest = {
      numberOfChunks: chunks,
      readRequest: readRequest,
    };

    return this.http
      .post<PreviewValueList>(this._contextUrl + "/" + channelGroup.source + "/values/preview", previewRequest, this.httpOptions)
      .pipe(
        map((res) => res.avg),
        map((measurementValues) => measurementValues.sort((a, b) => a.name.localeCompare(b.name))),
        catchError((e) => addErrorDescription(e, "Could not load preview values!")),
      );
  }

  loadMeasurement(node: Node | MDMIdentifier) {
    if (this.isNode(node)) {
      return this.loadMeasurementByData(
        node.type,
        node.sourceName ? node.sourceName : node.source,
        node.id,
        node.name ? node.name : node.label,
      );
    }

    if (this.isMDMIdentifier(node)) {
      return this.loadMeasurementByData(node.type, node.source, node.id, undefined);
    }
  }

  private isMDMIdentifier(node: Node | MDMIdentifier): node is MDMIdentifier {
    return (node as MDMIdentifier).source !== undefined;
  }

  private isNode(node: Node | MDMIdentifier): node is Node {
    return (node as Node).sourceName !== undefined;
  }

  loadMeasurementByData(nodeType: string, nodeSource: string, nodeId: string, nodeName: string) {
    if (nodeType !== TYPE_MEASUREMENT && nodeType !== TYPE_CHANNELGROUP && nodeType !== TYPE_CHANNEL) {
      return throwError("Node must be of type: " + TYPE_MEASUREMENT + " or " + TYPE_CHANNELGROUP + " or " + TYPE_CHANNEL);
    }

    let measurementId: Observable<string[]>;

    if (nodeType === TYPE_MEASUREMENT && nodeName !== undefined) {
      measurementId = of([nodeId, nodeName]);
    } else {
      // query id of the measurement
      const queryMeasurementId = new Query();
      queryMeasurementId.columns = [TYPE_MEASUREMENT + ".Id", TYPE_MEASUREMENT + ".Name"];
      queryMeasurementId.resultType = TYPE_MEASUREMENT;
      queryMeasurementId.resultLimit = 1;
      queryMeasurementId.addFilter(nodeSource, nodeType + ".Id eq " + nodeId);

      measurementId = this.queryService.query(queryMeasurementId).pipe(
        map((sr) => [
          sr.rows.map((row) => Row.getColumn(row, TYPE_MEASUREMENT + ".Id")),
          sr.rows.map((row) => Row.getColumn(row, TYPE_MEASUREMENT + ".Name")),
        ]),
        map((ids) => (ids.length > 0 ? (ids as []) : undefined)),
      );
    }

    return measurementId.pipe(flatMap((meaId) => this.loadMeasurementById(nodeSource, meaId[0], meaId[1])));
  }

  loadMeasurementById(sourceName: string, measurementId: string, measurementName: string) {
    const query = new Query();
    query.addFilter(sourceName, "Measurement.Id eq " + measurementId);
    query.columns = [
      "Measurement.Id",
      "ChannelGroup.Id",
      "ChannelGroup.Name",
      "ChannelGroup.SubMatrixNoRows",
      "Channel.Id",
      "Channel.Name",
      "Unit.Id",
      "Unit.Name",
      "LocalColumn.axistype",
      "LocalColumn.IndependentFlag",
    ];
    query.resultType = "LocalColumn";
    query.resultOffset = this.resultOffset;
    query.resultLimit = 0; // request all results

    return this.queryService.query(query).pipe(
      map((sr) => sr.rows.map((row) => this.mapRow(row))),
      map((pairs) => {
        const mea = new Measurement();
        mea.type = TYPE_MEASUREMENT;
        mea.name = measurementName;

        pairs.forEach((entry) => {
          mea.put(entry.channelGroup, entry.channel);
          entry.channel.measurement = measurementName;
          mea.source = entry.channelGroup.source;
          mea.id = entry.meaId;
        });

        return mea;
      }),
    );
  }

  private mapRow(row: Row) {
    const channel = new Channel();
    channel.id = Row.getColumn(row, "Channel.Id");
    channel.name = Row.getColumn(row, "Channel.Name");
    channel.source = row.source;
    channel.channelGroupId = Row.getColumn(row, "ChannelGroup.Id");
    channel.axisType = Row.getColumn(row, "LocalColumn.axistype");

    const independent = Row.getColumn(row, "LocalColumn.IndependentFlag");
    channel.isIndependent = independent === "1" || independent === 1;

    channel.unitId = Row.getColumn(row, "Unit.Id");
    channel.unitName = Row.getColumn(row, "Unit.Name");

    const channelGroup = new ChannelGroup(
      row.source,
      Row.getColumn(row, "ChannelGroup.Id"),
      parseInt(Row.getColumn(row, "ChannelGroup.SubMatrixNoRows"), 10),
    );

    channelGroup.name = Row.getColumn(row, "ChannelGroup.Name");
    return { meaId: Row.getColumn(row, "Measurement.Id"), channelGroup: channelGroup, channel: channel };
  }
}
