/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

// entity types
export const TYPE_CHANNEL = "Channel";
export const TYPE_CHANNELGROUP = "ChannelGroup";
export const TYPE_MEASUREMENT = "Measurement";

export const CHART_COLORS = [
  "#ff0900",
  "#ffa100",
  "#adb100",
  "#1bff00",
  "#00bba4",
  "#0098ff",
  "#ffa4ff",
  "#e56b6b",
  "#0000ff",
  "#4c7700",
  "#9a44ff",
  "#925700",
  "#c1c300",
  "#006863",
  "#ff00cf",
  "#ff0900",
  "#ffa100",
  "#adb100",
  "#1bff00",
  "#00bba4",
  "#0098ff",
  "#ffa4ff",
  "#e56b6b",
  "#0000ff",
  "#4c7700",
  "#9a44ff",
  "#925700",
  "#c1c300",
  "#006863",
  "#ff00cf",
  "#ff0900",
  "#ffa100",
  "#adb100",
  "#1bff00",
  "#00bba4",
  "#0098ff",
  "#ffa4ff",
  "#e56b6b",
  "#0000ff",
  "#4c7700",
  "#9a44ff",
  "#925700",
  "#c1c300",
  "#006863",
  "#ff00cf",
];

export const CHART_DASHES = [
  // Dash lines are configured as [number, number] meaning [dash-length, gap-length] in pixels
  // The dash amount must match the chart_colors amount
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [7, 4],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
  [2, 2],
];
