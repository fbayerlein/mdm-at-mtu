/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from "@angular/core";
import { MDMIdentifier } from "@core/mdm-identifier";
import { PreferenceConstants, PreferenceService, Scope } from "@core/preference.service";
import { StateService } from "@core/services/state.service.ts";
import { TreeNode } from "primeng/api";
import { ChannelGroup } from "../../model/types/channelgroup.class";
import { Measurement } from "../../model/types/measurement.class";

@Component({
  selector: "mdm-channelgroup-selection-panel",
  templateUrl: "./channelgroup-selection-panel.component.html",
  styleUrls: ["../../chart-viewer.style.css", "./xy-chart-data-selection-panel.component.css"],
})
export class ChannelGroupSelectionPanelComponent implements OnInit, OnChanges, OnDestroy {
  @Input()
  public measurements: Measurement[] = [];

  @Output()
  public selectedMeasurementsChange = new EventEmitter<Measurement[]>();

  @Output()
  public selectedChannelGroupsChange = new EventEmitter<ChannelGroup[]>();

  public selectedMeasurements: Measurement[];
  public selectedChannelGroups: ChannelGroup[];

  public channelGroupsTree: TreeNode<Measurement | ChannelGroup>[] = [];
  public cgTreeCols = [{ field: "name", header: "Name" }];
  public selectedTreeNodes: TreeNode<Measurement | ChannelGroup>[] = [];

  private nodesToSelectFromState: MDMIdentifier[] = [];
  private isCaseSensitive = false;
  public channelGroupFilter: string;
  public panelCollapsed = false;

  constructor(private preferenceService: PreferenceService, private stateService: StateService) {
    this.stateService.register(this, "channelgroup-selection-panel");
  }

  getState(): unknown {
    return {
      // TODO expansion state
      channelGroupFilter: this.channelGroupFilter,
      panelCollapsed: this.panelCollapsed,
      selectedNodes: this.selectedTreeNodes.map((n) => ({ source: n.data.source, type: n.data.type, id: n.data.id } as MDMIdentifier)),
    };
  }

  applyState(state: unknown) {
    this.channelGroupFilter = state["channelGroupFilter"] || "";
    this.panelCollapsed = state["panelCollapsed"];
    this.nodesToSelectFromState = state["selectedNodes"] || [];
  }

  ngOnInit() {
    this.initChannelGroupTree(false);

    this.preferenceService.getPreferenceForScope(Scope.SYSTEM, PreferenceConstants.SEARCH_CASE_SENSITIVE).subscribe((preferences) => {
      if (preferences.length > 0) {
        this.isCaseSensitive = "true" === preferences[0].value;
      }
    });

    if (this.selectMDMIdentifiers(this.nodesToSelectFromState)) {
      this.onSelectedChannelGroupsChanged();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["measurements"]) {
      this.initChannelGroupTree(true);
      if (this.selectMDMIdentifiers(this.nodesToSelectFromState)) {
        this.onSelectedChannelGroupsChanged();
      }
    }
  }

  selectMDMIdentifiers(dataItems: Measurement[] | ChannelGroup[] | MDMIdentifier[]) {
    if (dataItems) {
      let selectionChanged = false;
      for (const dataItem of dataItems) {
        selectionChanged = this.selectMDMIdentifier(dataItem) || selectionChanged;
      }
      return selectionChanged;
    } else {
      return false;
    }
  }

  selectMDMIdentifier(dataItem: Measurement | ChannelGroup | MDMIdentifier) {
    let selectionChanged = false;
    if (dataItem.type === "Measurement") {
      for (const meaTreeNode of this.channelGroupsTree) {
        if (dataItem.id === meaTreeNode.data.id && dataItem.source === meaTreeNode.data.source && !this.isSelected(dataItem)) {
          this.selectedTreeNodes.push(meaTreeNode);
          selectionChanged = true;
        }
      }
    } else if (dataItem.type === "ChannelGroup") {
      for (const meaTreeNode of this.channelGroupsTree) {
        for (const cgTreeNode of meaTreeNode.children) {
          if (dataItem.id === cgTreeNode.data.id && dataItem.source === cgTreeNode.data.source && !this.isSelected(dataItem)) {
            this.selectedTreeNodes.push(cgTreeNode);
            selectionChanged = true;
          }
        }
      }
    }
    return selectionChanged;
  }

  ngOnDestroy(): void {
    this.stateService.unregister(this);
  }

  private isSelected(item: Measurement | ChannelGroup | MDMIdentifier) {
    return this.selectedTreeNodes.some((tn) => tn.data.type === item.type && tn.data.id === item.id);
  }
  public onSelectedChannelGroupsChanged() {
    this.selectedMeasurements = this.selectedTreeNodes.map((tn) => tn.data as Measurement).filter((d) => d.type === "Measurement");
    this.selectedChannelGroups = this.selectedTreeNodes.map((tn) => tn.data as ChannelGroup).filter((d) => d.type === "ChannelGroup");

    this.selectedMeasurementsChange.emit(this.selectedMeasurements);
    this.selectedChannelGroupsChange.emit(this.selectedChannelGroups);
  }

  public onFilterChannelGroups() {
    this.initChannelGroupTree(false);
  }

  public initChannelGroupTree(append: boolean) {
    const tree = [];

    // get expanded state of measurement nodes
    const expandedMeasurements = this.channelGroupsTree.filter((n) => n.expanded).map((n) => n.data.id);

    if (this.measurements) {
      this.measurements.forEach((mea) => {
        const measurementTreeNode = this.channelGroupsTree.find((tn) => tn.data.id === mea.id);
        if (append && measurementTreeNode !== undefined) {
          tree.push(measurementTreeNode);
        } else if (!append || measurementTreeNode === undefined) {
          const root = {
            data: mea,
            children: [],
            expanded: expandedMeasurements.some((id) => id === mea.id),
          } as TreeNode<Measurement | ChannelGroup>;

          for (const channelGroup of this.filter(mea.channelGroups, this.channelGroupFilter)) {
            const child = { data: channelGroup } as TreeNode<ChannelGroup>;
            root.children.push(child);
          }

          // only push into the tree if we have actual channel groups
          if (root.children.length > 0) {
            tree.push(root);
          }
        }
      });
    }
    this.channelGroupsTree = tree;
  }

  private filter<T extends { name: string }>(options: T[], filterString: string): T[] {
    if (filterString != undefined && filterString !== "") {
      const filterStr = this.isCaseSensitive ? filterString : filterString.toLowerCase();
      try {
        const regEx = new RegExp(filterStr, this.isCaseSensitive ? "" : "i");
        return options.filter((opt) => regEx.test(opt.name));
      } catch {
        return options.filter((opt) => (this.isCaseSensitive ? opt.name : opt.name.toLowerCase()).includes(filterStr));
      }
    } else {
      return options;
    }
  }
}
