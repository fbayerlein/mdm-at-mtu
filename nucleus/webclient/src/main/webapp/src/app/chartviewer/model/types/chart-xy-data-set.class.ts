/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DateTime } from "luxon";
import { Node } from "../../../navigator/node";
import { ChartViewerService } from "../../services/chart-viewer.service";
import { PrimeChartDataSet } from "../chartviewer.model";
import { ChartPoint } from "./chart-point.class";
import { MeasuredValues } from "./measured-values.class";

export class ChartXyDataSet implements PrimeChartDataSet {
  // contains the channel id to distinguish identical channels from multiple measurements
  uniqueId: string;

  // mandatory
  label: string;
  data: ChartPoint[];
  // the offset contains the value for relative charting to convert the dataset back to absolute charting
  offset: any;

  xScalarType?: string;
  yScalarType?: string;

  xLabel?: string;
  xUnit?: string;
  yUnit?: string;

  // remove?
  hidden?: boolean;
  measuredValues?: MeasuredValues;
  channel?: Node;

  // point styles
  pointBackgroundColor?: string;
  pointBorderColor?: string;
  pointHoverBackgroundColor?: string;
  pointHoverBorderColor?: string;
  pointRadius: number;
  pointHoverRadius?: number;

  // line styles
  borderWidth: number;
  fill: boolean;
  borderColor: string;
  lineTension: number;
  showLine: boolean;
  borderDash?: number[];

  // general
  backgroundColor?: string;

  public constructor(label: string, data: ChartPoint[]) {
    this.label = label;
    this.data = data;
  }

  /**
   * Creates a new ChartXyDataSet from the given x and y MeasuredValues
   * @param xData x data
   * @param yData y data
   * @returns initialized ChartXyDataSet with given x and y data
   */
  public static fromMeasuredValues(
    xData: MeasuredValues,
    yData: MeasuredValues,
    channelId: string,
    chartViewerService: ChartViewerService,
  ): ChartXyDataSet {
    const points = this.getDataPoints(xData, yData, chartViewerService);
    const dataset = new ChartXyDataSet(yData.name + " (" + yData.optionalLabel + ") [" + yData.unit + "]", points);
    dataset.xLabel = xData.name + " [" + xData.unit + "]";
    dataset.uniqueId = channelId;
    dataset.xUnit = xData !== undefined ? xData.unit : null;
    dataset.yUnit = yData.unit;
    dataset.xScalarType = xData.scalarType;
    dataset.yScalarType = yData.scalarType;
    return dataset;
  }

  /**
   * Merges x and y MeasuredValues and returns an array Of ChartPoints
   * @param xData x data
   * @param yData y data
   * @returns ChartPoint[]
   */
  private static getDataPoints(xData: MeasuredValues, yData: MeasuredValues, chartViewerService: ChartViewerService) {
    let xValues: any[];
    if (xData !== undefined) {
      if (xData.scalarType === "DATE") {
        // DATE are transmitted as strings
        xValues = (chartViewerService.getDataArrayWithFlagsRespection(xData).values as string[]).map((dateString) =>
          DateTime.fromISO(dateString),
        );
      } else {
        xValues = chartViewerService.getDataArrayWithFlagsRespection(xData).values;
      }
    }

    const yValues = chartViewerService.getDataArrayWithFlagsRespection(yData).values as any[];

    if (xValues !== undefined && yValues !== undefined) {
      if (xValues.length <= yData.length) {
        return xValues.map((x, i) => new ChartPoint(x, yValues[i]));
      } else {
        return yValues.map((y, i) => new ChartPoint(xValues[i], y));
      }
    }
  }

  public switchCharting(relative: boolean) {
    if (relative) {
      // switch from absolute to relative - get X from first data and set offset, adjust all datas
      if (!this.offset && this.data.length > 0) {
        this.offset = this.data[0].x;
        for (const dt of this.data) {
          dt.x -= this.offset;
        }
      }
    } else if (this.offset) {
      // switch from relative to absolute - adjust all datas and add the offset, clear offset
      for (const dt of this.data) {
        dt.x += this.offset;
      }
      this.offset = undefined;
    }
  }
}
