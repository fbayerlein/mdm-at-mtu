/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { ByteArray } from "./byte-array.class";
import { getDataArray, MeasuredValues } from "./measured-values.class";
import { NumberArray } from "./number-array.class";

describe("MeasuredValues", () => {
  it("should create an instance", () => {
    const measuredValues = new MeasuredValues();
    expect(measuredValues).toBeTruthy();
  });

  it("should have properties initialized correctly", () => {
    const measuredValues = new MeasuredValues();
    measuredValues.name = "Temperature";
    measuredValues.optionalLabel = "Indoor";
    measuredValues.unit = "°C";

    expect(measuredValues.name).toEqual("Temperature");
    expect(measuredValues.optionalLabel).toEqual("Indoor");
    expect(measuredValues.unit).toEqual("°C");
  });

  it("should parse BYTE values", () => {
    const measuredValues = new MeasuredValues();
    measuredValues.length = 10;
    measuredValues.scalarType = "BYTE";
    measuredValues.byteArray = new ByteArray();
    measuredValues.byteArray.values = "f4GDhYeKjI6Qkg==";
    measuredValues.flags = [true, false, true, false, true, false, true, false, true, false];

    const values = getDataArray(measuredValues, false);
    expect(values instanceof NumberArray).toBeTrue();
    expect(values.values.length).toEqual(10);
    expect(values.values).toEqual([127, 129, 131, 133, 135, 138, 140, 142, 144, 146]);
  });

  it("should parse BYTE values and evaluate flags", () => {
    const measuredValues = new MeasuredValues();
    measuredValues.length = 10;
    measuredValues.scalarType = "BYTE";
    measuredValues.byteArray = new ByteArray();
    measuredValues.byteArray.values = "f4GDhYeKjI6Qkg==";
    measuredValues.flags = [true, false, true, false, true, false, true, false, true, false];

    const values = getDataArray(measuredValues, true);
    expect(values instanceof NumberArray).toBeTrue();
    expect(values.values.length).toEqual(10);
    expect(values.values).toEqual([127, null, 131, null, 135, null, 140, null, 144, null]);
  });

  it("should parse BYTE empty array", () => {
    const measuredValues = new MeasuredValues();
    measuredValues.length = 0;
    measuredValues.scalarType = "BYTE";
    measuredValues.byteArray = new ByteArray();
    measuredValues.byteArray.values = "";
    measuredValues.flags = [];

    const values = getDataArray(measuredValues, false);
    expect(values instanceof NumberArray).toBeTrue();
    expect(values.values.length).toEqual(0);
    expect(values.values).toEqual([]);
  });

  it("should parse BYTE empty array and evaluate flags", () => {
    const measuredValues = new MeasuredValues();
    measuredValues.length = 0;
    measuredValues.scalarType = "BYTE";
    measuredValues.byteArray = new ByteArray();
    measuredValues.byteArray.values = "";
    measuredValues.flags = [];

    const values = getDataArray(measuredValues, true);
    expect(values instanceof NumberArray).toBeTrue();
    expect(values.values.length).toEqual(0);
    expect(values.values).toEqual([]);
  });
});
