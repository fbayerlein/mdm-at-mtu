/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { streamTranslate, TRANSLATE } from "@core/mdm-core.module";
import { MDMItem } from "@core/mdm-item";
import { IStatefulComponent, StateDTO, StateService } from "@core/services/state.service.ts";
import { TranslateService } from "@ngx-translate/core";
import { SelectItem } from "primeng/api";
import { Subscription } from "rxjs";
import { MDMNotificationService } from "../../../core/mdm-notification.service";
import { PreferenceService, Scope } from "../../../core/preference.service";
import { NavigatorService } from "../../../navigator/navigator.service";
import { Node } from "../../../navigator/node";
import { ChannelSelectionRow } from "../../model/chartviewer.model";
import { TYPE_CHANNEL, TYPE_CHANNELGROUP, TYPE_MEASUREMENT } from "../../model/constants";
import { Channel } from "../../model/types/channel.class";
import { ChannelGroup } from "../../model/types/channelgroup.class";
import { Measurement } from "../../model/types/measurement.class";
import { ChartViewerDataService } from "../../services/chart-viewer-data.service";
import { ChartViewerComponent } from "../chartviewer/chart-viewer.component";
import { DataTableComponent } from "../datatable/data-table.component";

@Component({
  selector: "mdm-chart-viewer-nav-card",
  templateUrl: "chart-viewer-nav-card.component.html",
  styleUrls: ["../../chart-viewer.style.css"],
  providers: [],
})
export class ChartViewerNavCardComponent implements OnInit, OnDestroy, IStatefulComponent {
  private subscription = new Subscription();

  modes: SelectItem[] = [];
  selectedMode = "chart";

  // public data for sub-components
  selectedChannelNodes: Node[] = [];

  cachedMeasurement: Measurement;
  measurement: Measurement;

  selectedChannelRows: ChannelSelectionRow[];

  totalCachedChannels = 0;

  // dynamic reloading
  limit = 10;
  offset = 0;
  loadInterval = 10;
  allLoaded = false;
  openChannels = { limit: 0, offset: 0, total: 0 };

  @ViewChild("chart")
  chartViewer: ChartViewerComponent;

  @ViewChild("table")
  dataTable: DataTableComponent;

  constructor(
    private navigatorService: NavigatorService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private preferenceService: PreferenceService,
    private chartService: ChartViewerDataService,
    private stateService: StateService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.stateService.register(this, "chart-viewer-nav-card");
  }

  ngOnInit() {
    streamTranslate(this.translateService, [TRANSLATE("chartviewer.chart"), TRANSLATE("chartviewer.table")]).subscribe((msg: string) => {
      this.modes = [
        { label: msg["chartviewer.chart"], value: "chart" },
        { label: msg["chartviewer.table"], value: "table" },
      ];
    });
    this.preferenceService.getPreferenceForScope(Scope.SYSTEM, "chart-viewer.channels.max-display").subscribe((preferences) => {
      if (preferences.length > 0) {
        this.limit = parseInt(preferences[0].value, 10);
      }
    });
    this.preferenceService.getPreferenceForScope(Scope.SYSTEM, "chart-viewer.channels.load-interval").subscribe((preferences) => {
      if (preferences.length > 0) {
        this.loadInterval = parseInt(preferences[0].value, 10);
      }
    });
    this.subscription.add(this.navigatorService.onSelectedNodesChanged().subscribe((nodes) => this.handleNodeSelection(nodes)));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.stateService.unregister(this);
  }

  generateLink() {
    let state = {
      "chart-viewer-nav-card": this.getState(),
    };
    if (this.selectedMode === "chart") {
      state = { ...this.chartViewer.getState(), ...state };
    } else {
      // state = { ...this.dataTable.getState(), ...state };
    }
    return state;
  }

  link: string;
  persistState() {
    const bookmarkUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;

    const state = this.generateLink();

    this.stateService
      .persistState(<StateDTO>{ component: "chart-viewer-nav-card", state: JSON.stringify(state) })
      .subscribe((s) => (this.link = bookmarkUrl + "?state=" + s.id));
  }

  getState() {
    return {
      selectedMode: this.selectedMode,
      selectedNodes: this.selectedChannelNodes.map((n) => {
        return {
          sourceName: n.sourceName,
          type: n.type,
          id: n.id,
        };
      }),
    };
  }

  applyState(state: unknown) {
    if (state) {
      this.selectedMode = state["selectedMode"] as string;
    }
  }

  private handleNodeSelection(nodes: (Node | MDMItem)[]) {
    if (!nodes || nodes.length === 0) {
      return;
    }

    const supportedNodes = nodes
      .map((obj) => obj as Node)
      .filter((node) => node.type === TYPE_MEASUREMENT || node.type === TYPE_CHANNELGROUP || node.type === TYPE_CHANNEL);

    this.selectedChannelNodes = supportedNodes.filter((nd) => nd.type === TYPE_CHANNEL);

    this.cleanData();
    if (this.selectedChannelNodes.length > 0) {
      this.loadData(this.selectedChannelNodes[0], this.selectedChannelNodes);
    } else if (supportedNodes.length > 0) {
      this.selectedChannelNodes = supportedNodes;
      this.loadData(this.selectedChannelNodes[0], this.selectedChannelNodes);
    }
  }

  cleanData() {
    this.cachedMeasurement = new Measurement();
    this.totalCachedChannels = 0;
    this.measurement = undefined;
    this.offset = 0;
    this.openChannels = { limit: 0, offset: 0, total: 0 };
    this.allLoaded = false;
  }

  loadData(node: Node, selectedChannelNodes?: Node[]) {
    this.chartService.loadMeasurement(node).subscribe((measurement) => {
      this.cachedMeasurement = measurement;
      this.displayNextChannels(measurement, selectedChannelNodes);
    });
  }

  displayNextChannels(measurement: Measurement, selectedChannelNodes: Node[]) {
    if (!this.selectedChannelNodes) {
      return;
    }

    if (this.selectedChannelNodes[0].type === TYPE_MEASUREMENT) {
      this.selectedChannelRows = measurement.allChannels().map((c) => new ChannelSelectionRow(measurement.findChannelGroupByChannel(c), c));
      this.sortSelectedChannelRows();
      this.restrictSelectedChannelRows();
    } else if (this.selectedChannelNodes[0].type === TYPE_CHANNELGROUP) {
      const channelGroup = measurement.findChannelGroup(this.selectedChannelNodes[0].id);
      this.selectedChannelRows = measurement
        .findChannels(measurement.findChannelGroup(this.selectedChannelNodes[0].id))
        .map((c) => new ChannelSelectionRow(channelGroup, c));
      this.sortSelectedChannelRows();
      this.restrictSelectedChannelRows();
    } else if (this.selectedChannelNodes[0].type === TYPE_CHANNEL) {
      const channel =
        selectedChannelNodes === undefined
          ? undefined
          : selectedChannelNodes.map((cn) => measurement.findChannel(cn.id)).filter((cn) => cn !== undefined);
      let channelGroup: ChannelGroup = undefined;
      let cGrpID: string = undefined;
      if (channel === undefined || channel.length === 0) {
        channelGroup = measurement.findChannelGroup(this.selectedChannelNodes[0].id);
      } else {
        channelGroup = measurement.findChannelGroupByChannel(channel[0]);
        cGrpID = channel[0].channelGroupId;
      }

      const channelsArr: Channel[] = [];
      channel.forEach((ch) => {
        if (measurement.getNumChannelGroupsForChannel(ch) > 1) {
          this.notificationService.notifyWarn(
            this.translateService.instant("chartviewer.chart-viewer-nav-card.channel-linked-to-multiple-channel-groups", {
              channelname: ch.name,
            }),
            null,
          );
        } else {
          channelsArr.push(ch);
        }
      });

      this.selectedChannelRows = channelsArr
        .filter((c) => cGrpID === undefined || c.channelGroupId === cGrpID)
        .map((c) => new ChannelSelectionRow(channelGroup, c));
      this.sortSelectedChannelRows();
    }
    this.measurement = measurement;
  }

  private sortSelectedChannelRows() {
    this.selectedChannelRows.sort((cr1, cr2) => cr1.yChannel.name.localeCompare(cr2.yChannel.name));
  }

  private restrictSelectedChannelRows() {
    if (this.selectedChannelRows.length > this.limit) {
      this.selectedChannelRows.splice(this.limit);
    }
  }

  loadMissingChannels() {
    this.displayNextChannels(this.cachedMeasurement, undefined);
  }
}
