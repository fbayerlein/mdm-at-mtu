/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { streamTranslate, TRANSLATE } from "@core/mdm-core.module";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem, SelectItem } from "primeng/api";
import { ChartToolbarProperties } from "../../model/chartviewer.model";
import { XyChartViewerDataComponent } from "../xy-chart-viewer/xy-chart-viewer-data.component";

@Component({
  selector: "mdm-xy-chart-viewer-toolbar",
  templateUrl: "./xy-chart-viewer-toolbar.component.html",
  styleUrls: ["../../chart-viewer.style.css"],
})
export class XyChartViewerToolbarComponent implements OnInit {
  // holds actual properties
  @Input()
  public toolbarProperties = new ChartToolbarProperties();

  @Output()
  public toolbarPropertiesChanged = new EventEmitter<ChartToolbarProperties>();

  @Output()
  public addItemsToShoppingList = new EventEmitter<string>();

  modes: SelectItem[] = [];

  options: MenuItem[];

  constructor(private xyChartViewerDataComponent: XyChartViewerDataComponent, private translateService: TranslateService) {}

  ngOnInit() {
    this.options = [
      {
        label: this.translateService.instant("chartviewer.xy-chart-data-selection-panel.measurement"),
        command: () => {
          this.onChangeOption("Measurement");
        },
      },
      {
        label: this.translateService.instant("chartviewer.xy-chart-data-selection-panel.channel-group"),
        command: () => {
          this.onChangeOption("ChannelGroup");
        },
      },
      {
        label: this.translateService.instant("chartviewer.xy-chart-data-selection-panel.channels"),
        command: () => {
          this.onChangeOption("Channel");
        },
      },
    ];

    streamTranslate(this.translateService, [TRANSLATE("chartviewer.chart"), TRANSLATE("chartviewer.table")]).subscribe((msg: string) => {
      this.modes = [
        { label: msg["chartviewer.chart"], value: "chart" },
        { label: msg["chartviewer.table"], value: "table" },
      ];
    });

    this.toolbarPropertiesChanged.emit(this.toolbarProperties);
  }

  public onChangeProperty() {
    this.toolbarPropertiesChanged.emit(this.toolbarProperties);
  }

  // displays/hides lines connecting data points
  public onToggleShowLines() {
    if (!this.toolbarProperties.showLines) {
      this.toolbarProperties.fillArea = false;
    }
    this.onChangeProperty();
  }

  public onOpenPopupWindow() {
    // trigger the open popup mechanism
    this.xyChartViewerDataComponent.sendShowWindowChange(true);
  }

  public onChangeOption(value: string) {
    this.addItemsToShoppingList.emit(value);
  }

  public onButtonClick() {
    this.addItemsToShoppingList.emit("Channel");
  }
}
