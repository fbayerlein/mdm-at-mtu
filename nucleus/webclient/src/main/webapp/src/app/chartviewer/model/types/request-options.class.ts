/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
export class RequestOptions {
  requestSize: number;
  startIndex: number;
  previewEnabled: boolean;
  numberOfChunks: number;

  constructor(r: RequestOptions) {
    this.requestSize = r.requestSize ?? 0;
    this.startIndex = r.startIndex ?? 0;
    this.previewEnabled = r.previewEnabled ?? true;
    this.numberOfChunks = r.numberOfChunks ?? 600;
  }

  getChunks() {
    if (this.previewEnabled) {
      return this.numberOfChunks;
    } else {
      return 0;
    }
  }

  isDefault() {
    return this.requestSize === 0 && this.startIndex === 0 && this.previewEnabled && this.numberOfChunks === 600;
  }
}
