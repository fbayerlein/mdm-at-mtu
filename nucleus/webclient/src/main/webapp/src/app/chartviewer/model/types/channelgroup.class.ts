/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { TYPE_CHANNELGROUP } from "../constants";

export class ChannelGroup {
  source: string;
  type = TYPE_CHANNELGROUP;
  id: string;
  name: string;
  numberOfRows: number;

  constructor(source: string, id: string, numberOfRows: number) {
    this.source = source;
    this.id = id;
    this.numberOfRows = numberOfRows;
  }
}
