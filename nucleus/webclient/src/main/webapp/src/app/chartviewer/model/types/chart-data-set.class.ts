/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Node } from "../../../navigator/node";
import { PrimeChartDataSet } from "../chartviewer.model";
import { MeasuredValues } from "./measured-values.class";

export class ChartDataSet implements PrimeChartDataSet {
  label: string;
  data: any[];
  hidden?: boolean;
  fill?: boolean;
  measuredValues?: MeasuredValues;
  channel?: Node;
  backgroundColor?: string;
  borderColor?: string;
  pointBackgroundColor?: string;
  pointBorderColor?: string;
  pointHoverBackgroundColor?: string;
  pointHoverBorderColor?: string;

  constructor(label: string, data: any[]) {
    this.label = label;
    this.data = data;
  }
}
