/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

export class ChartToolbarProperties {
  // either show chart or table
  public displayMode: "chart" | "table" = "chart";
  // if false, channels are not filtered accoring th axisType
  public xyAxisFilterActive = true;
  // if true, shows pannel with channel(-group) selection
  public showSelection = true;
  // if true, draws datapoints
  public drawPoints = false;
  // if true, hide flags which are false
  public respectFlags = true;
  // the charts borderwidth (width of lines, border for points)
  public lineWidth = 1;
  // if true, draws lines connecting datapoints
  public showLines = true;
  // if true, fills are below graph
  public fillArea = false;
  // the interpolation degree. 0 = linear, 0.4 = Bezier
  public lineTension = 0;
  // use relative charting based on the first dataset or absolute charting
  public relativeDrawing = false;
  // if true, group measurements
  public groupMeasurements = false;
  // if true, use multiple y-axes
  public multipleAxes = false;

  public options: { legend: { display: boolean } } = { legend: { display: true } };
}
