/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { CHART_COLORS, CHART_DASHES } from "../model/constants";

/**
 * Abstract chart class containing shared methods and attributes
 */
@Injectable()
export class ColorService {
  /**
   * Simple map with dataset hashcode to color code assignment to get a fixed color assignment
   * [0] = color
   * [>0] = hashcode
   */
  private assignedColors: string[][] = [];

  public getColorAndDash(label: string) {
    const dsHash = this.hashCode(label).toString(16);

    return {
      color: this.getChartColor(dsHash),
      dash: this.getDashStyle(dsHash),
    };
  }

  public resetAssignedColors() {
    this.assignedColors = [];
  }

  /**
   * Generate a random hex color
   */
  private generateRandomColor() {
    return "#" + ((Math.random() * 0xffffff) << 0).toString(16).padStart(6, "0");
  }

  /**
   * Get a color code based on the hashcode
   * If the hashcode is not requested yet, a new color is assigned
   * @param hash
   */
  private getChartColor(hash: string) {
    if (!this.assignedColors.find((ac) => ac.length > 1 && ac.includes(hash))) {
      // check if all colors are already used
      if (this.assignedColors.length >= CHART_COLORS.length) {
        // append a random generated color
        this.assignedColors.push([this.generateRandomColor(), hash]);
      } else {
        // the maximum of the pre-defined colors is not reached, push the color
        this.assignedColors.push([CHART_COLORS[this.assignedColors.length], hash]);
      }
    }
    return this.assignedColors.find((ac) => ac.length > 0 && ac.includes(hash))[0];
  }

  /**
   * Get a dash style based on the hashcode
   * If a hashcode is not requested yet, no dash style is returned
   * @param hash
   */
  private getDashStyle(hash: string) {
    for (let i = 0; i < this.assignedColors.length; i++) {
      if (CHART_DASHES.length > this.assignedColors.length && this.assignedColors[i].length > 1 && this.assignedColors[i].includes(hash)) {
        // assign the chart dashes as long as we have a defined color
        return CHART_DASHES[i];
      }
    }
    // return a solid line definition
    return [];
  }

  /**
   * Transform a dataset into a hashcode
   * @param s
   */
  private hashCode(s) {
    let h: number;
    for (let i = 0; i < s.length; i++) {
      h = (Math.imul(31, h) + s.charCodeAt(i)) | 0;
    }
    return h;
  }
}
