/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { TYPE_MEASUREMENT } from "../constants";
import { Channel } from "./channel.class";
import { ChannelGroup } from "./channelgroup.class";

export class Measurement {
  source: string;
  type = TYPE_MEASUREMENT;
  id: string;
  name: string;
  channelGroups: ChannelGroup[] = [];
  channels: Map<string, Channel[]> = new Map();

  constructor(m?: Measurement) {
    this.source = (m && m.source) || "";
    this.type = (m && m.type) || "";
    this.id = (m && m.id) || "";
    this.name = (m && m.name) || "";
    this.channelGroups = (m && m.channelGroups) || [];
    if (m) {
      this.channels = new Map(Object.entries(m.channels));
    }
  }

  put(channelGroup: ChannelGroup, channel: Channel) {
    const cg = this.findChannelGroup(channelGroup.id);
    if (cg === undefined) {
      this.channelGroups.push(channelGroup);
      this.channels.set(channelGroup.id, [channel]);
    } else {
      this.channels.get(channelGroup.id).push(channel);
    }
  }

  getFirstChannelGroup() {
    return this.channelGroups[0];
  }

  getIndependentChannel(channelGroup: ChannelGroup) {
    const independentChannels = (this.findChannels(channelGroup) || []).filter((c) => c.isIndependent);
    if (independentChannels.length > 0) {
      return independentChannels[0];
    } else {
      return undefined;
    }
  }

  hasChannelGroup(channelGroupId: string) {
    return this.findChannelGroup(channelGroupId) !== undefined;
  }

  hasChannel(channelId: string): unknown {
    return this.findChannel(channelId) !== undefined;
  }

  findChannelGroup(channelGroupId: string) {
    return this.channelGroups.find((cg) => cg.id === channelGroupId);
  }

  findChannelGroupByChannel(channel: Channel) {
    for (const entry of Array.from(this.channels.entries())) {
      const c = entry[1].find((chan) => chan.id === channel.id);
      if (c !== undefined) {
        return this.findChannelGroup(entry[0]);
      }
    }
    return undefined;
  }

  getNumChannelGroupsForChannel(channel: Channel) {
    let count = 0;
    for (const entry of Array.from(this.channels.entries())) {
      const c = entry[1].find((chan) => chan.id === channel.id);
      if (c !== undefined) {
        ++count;
      }
    }

    return count;
  }

  findChannel(channelId: string) {
    for (const entry of Array.from(this.channels.entries())) {
      const c = entry[1].find((channel) => channel.id === channelId);
      if (c !== undefined) {
        return c;
      }
    }
    return undefined;
  }
  findChannels(channelGroup: ChannelGroup) {
    return this.channels.get(channelGroup.id) || [];
  }

  allChannels() {
    const channels: Channel[] = [];

    for (const entry of Array.from(this.channels.values())) {
      channels.push(...entry);
    }
    return channels;
  }
}
