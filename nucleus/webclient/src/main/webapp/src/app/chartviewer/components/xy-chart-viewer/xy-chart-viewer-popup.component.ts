/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { ChartData, ChartXyDataSet } from "../../model/chartviewer.model";
import { XyChartViewerDataComponent } from "./xy-chart-viewer-data.component";

/**
 * The popup component handles the display of the window via a boolean flag.
 * If the flag is set to true from the button event, the window component will create itself in the onInit function
 * No template needed as the html code is directly written into the popup without any angular references
 */
@Component({
  selector: "mdm-xy-chart-viewer-popup",
  template: ``,
})
export class XyChartViewerPopupComponent implements OnInit, OnDestroy {
  chartingSubscribe: Subscription;
  // reference to the popup window
  externalWindow = null;

  constructor(private xyChartViewerDataComponent: XyChartViewerDataComponent) {}

  /**
   * Create the subscription hooks
   */
  ngOnInit(): void {
    this.chartingSubscribe = this.xyChartViewerDataComponent.charting.subscribe((value) => this.preparePopup(value));
  }

  /**
   * Cleanup the subscription hooks
   */
  ngOnDestroy(): void {
    // clean subscriptions
    if (this.chartingSubscribe) {
      this.chartingSubscribe.unsubscribe();
    }
    if (this.externalWindow) {
      this.externalWindow.close();
    }
  }

  /**
   * This method will prepare the data and transform it to latest ChartJS compatibility
   * @param value
   */
  preparePopup(value) {
    // deep-clone the object to avoid changes from the main window
    if (this.externalWindow) {
      this.externalWindow.close();
    }

    const options = JSON.parse(JSON.stringify(value[0]));

    const datastr = value[1];
    const datasets = [];

    // Transform the datasets to new objects to clear the _meta data
    for (const data of datastr.datasets) {
      const dataset = new ChartXyDataSet(data.label, data.data);
      for (const name in data) {
        if ("_meta" !== name && "data" !== name) {
          dataset[name] = data[name];
        }
      }
      datasets.push(dataset);
    }

    // copy the labels
    const data = new ChartData([], datasets);
    for (const name in datastr) {
      if (name.startsWith("label")) {
        data[name] = datastr[name];
      }
    }

    // transform scales from primeNG syntax to ChartJS syntax
    if (options.scales !== undefined) {
      if (options.scales.xAxes !== undefined && options.scales.xAxes.length > 0) {
        options.scales.x = {
          type: options.scales.xAxes[0].type,
          time: options.scales.xAxes[0].time.time,
          adapters: options.scales.xAxes[0].adapters,
        };
        if (options.scales.xAxes[0].scaleLabel !== undefined) {
          options.scales.x.title = options.scales.xAxes[0].scaleLabel;
          options.scales.x.title.text = options.scales.x.title.labelString;
        }
        options.scales.xAxes = undefined;
      }
      if (options.scales.yAxes !== undefined && options.scales.yAxes.length > 0) {
        options.scales.y = {
          type: options.scales.yAxes[0].type,
          time: options.scales.yAxes[0].time.time,
        };
        if (options.scales.yAxes[0].scaleLabel !== undefined) {
          options.scales.y.title = options.scales.yAxes[0].scaleLabel;
          options.scales.y.title.text = options.scales.y.title.labelString;
        }
        options.scales.yAxes = undefined;
      }
    }

    // create a final config object and transform to json for html rendering
    this.openPopup(
      JSON.stringify({
        type: "line",
        data: data,
        options: { scales: options.scales, legend: options.legend, maintainAspectRatio: false },
        plugins: options.plugins,
      }),
    );
  }

  /**
   * Create the popup window
   * We explicitly use the custom bundled chart.min.js from the asset folder here, as the node_module chart.js is name obfuscated
   * @param config the data payload written as json
   */
  openPopup(config) {
    let baseUrl = this.getBaseUrl();
    if (!baseUrl.endsWith("/")) {
      baseUrl += "/";
    }
    this.externalWindow = window.open("", "xy-chartviewer-popup", "width=800,height=600,left=100,top=100");
    this.externalWindow.document.write("<html><head><title>X/Y-ChartViewer Popup</title>");
    this.externalWindow.document.write('<script src="' + baseUrl + 'assets/js/Chart.min.js"></script>');
    this.externalWindow.document.write('<script src="' + baseUrl + 'assets/js/dateadapter.min.js"></script>');
    this.externalWindow.document.write('<script src="' + baseUrl + 'assets/js/chartjs-dateadapter-bridge.min.js"></script>');
    this.externalWindow.document.write('<script src="' + baseUrl + 'assets/js/luxon.min.js"></script>');
    this.externalWindow.document.write('<script src="' + baseUrl + 'assets/js/chartjs-adapter-luxon.min.js"></script>');
    this.externalWindow.document.write('<script src="' + baseUrl + 'assets/js/chartjs-plugin-zoom.min.js"></script>');
    this.externalWindow.document.write("</head><body>");
    this.externalWindow.document.write('<div style="position:relative;width:100%;height:100%;"><canvas id="xyPopupChart"></canvas></div>');
    this.externalWindow.document.write('<script type="text/javascript">var config = ');
    this.externalWindow.document.write(config);
    this.externalWindow.document.write(";");

    this.externalWindow.document.write('var canvas = document.getElementById("xyPopupChart");var ctx = canvas.getContext("2d");');
    this.externalWindow.document.write("var chart = new Chart(ctx, ");

    // generate the function for legend rendering - see ChartJS documentation
    this.externalWindow.document.write("{ type: config.type, data: config.data, options: { scales: config.options.scales, plugins: ");
    this.externalWindow.document.write("{zoom: config.plugins.zoom, legend: { display: config.options.legend.display, labels: { ");
    this.externalWindow.document.write("generateLabels: function (chart) {const data = chart.data;if (data.datasets.length) {");
    this.externalWindow.document.write("return data.datasets.map(function (dataset, i) {const meta = chart.getDatasetMeta(i);");
    this.externalWindow.document.write("const ds = dataset;const stroke = ds.borderColor;let bw = ds.borderWidth;");
    this.externalWindow.document.write("if (ds.borderDash != null && ds.borderDash.length > 0) {bw = 2;}return {text: dataset.label,");
    this.externalWindow.document.write("fillStyle: '#FFFFFF',strokeStyle: stroke,lineWidth: bw,lineDash: ds.borderDash,");
    this.externalWindow.document.write("hidden: meta.hidden,index: i,datasetIndex: i};});}return [];} } ");
    this.externalWindow.document.write("}}, maintainAspectRatio: config.options.maintainAspectRatio }, plugins: config.options.plugins }");

    this.externalWindow.document.write("); </script></body > </html>");
  }

  /**
   * Copy the base url from the main window
   * The base html tag does not work correctly
   */
  getBaseUrl() {
    let base = document.documentURI;
    base = base.substr(0, base.indexOf("navigator/xychartviewer"));
    return base;
  }
}
