/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from "@angular/core";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { LazyLoadEvent } from "primeng/api";
import { forkJoin, Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { ChannelSelectionRow, MeasuredValues } from "../../model/chartviewer.model";
import { Channel } from "../../model/types/channel.class";
import { ChannelGroup } from "../../model/types/channelgroup.class";
import { Measurement } from "../../model/types/measurement.class";
import { ChartViewerDataService } from "../../services/chart-viewer-data.service";
import { ChartViewerFormulaService } from "../../services/chart-viewer-formula.service";
import { ChartViewerService } from "../../services/chart-viewer.service";

@Component({
  selector: "mdm-data-table",
  templateUrl: "data-table.component.html",
  styleUrls: ["./data-table.component.css"],
  providers: [],
})
export class DataTableComponent implements OnChanges, OnDestroy, IStatefulComponent {
  @Input()
  measurement: Measurement;
  @Input()
  selectedChannelRows: ChannelSelectionRow[];

  readonly numberOfMeasurementsToKeepForState = 10;

  firstRow = 0;
  firstRows = [] as { id: string; firstRow: number }[];
  tableLoading = false;
  cols: { header: string; field: number }[] = [];
  totalRecords = 0;
  recordsPerPage = 10;
  //lastSelectedMeasurementId: string;
  datapoints = [];

  constructor(
    private chartviewerService: ChartViewerDataService,
    private userFormulaService: ChartViewerFormulaService,
    private chartViewerService: ChartViewerService,
    private stateService: StateService,
  ) {
    this.stateService.register(this, "chart-viewer-data-table");
  }

  getState() {
    // add new mapping if measurement is defined and first row is not the default 0
    if (this.measurement && this.measurement.id && this.firstRow > 0) {
      this.firstRows = this.firstRows.filter((fr) => fr.id !== this.measurement?.id);
      this.firstRows.push({ id: this.measurement?.id, firstRow: this.firstRow });
    }

    // only keep the last n entries
    this.firstRows = this.firstRows.slice(-this.numberOfMeasurementsToKeepForState);

    return {
      recordsPerPage: this.recordsPerPage,
      firstRows: this.firstRows,
    };
  }

  applyState(state: unknown) {
    if (state) {
      this.recordsPerPage = state["recordsPerPage"] || 10;
      this.firstRows = state["firstRows"] || [];
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (propName === "measurement") {
        this.firstRow = this.firstRows.find((e) => e.id === this.measurement.id)?.firstRow || 0;
      }
    }
  }

  ngOnDestroy() {
    this.stateService.unregister(this);
  }

  load(mv: MeasuredValues[]): Array<number[]> {
    const zip = (rows) => rows[0].map((_, c) => rows.map((row) => row[c]));
    // combine the columns of multiple data requests
    this.cols = this.cols.concat(
      mv
        .filter((m, i) => {
          if (!m) {
            return false;
          }
          for (let j = 0; j < this.cols.length; j++) {
            if (this.cols[j].header === m.name) {
              return false;
            }
          }
          return true;
        })
        .map((m, i) => {
          return { header: m.name, field: i };
        }),
    );
    return zip(mv.map((m) => (m ? this.chartViewerService.getDataArrayWithFlagsRespection(m).values : [])));
  }

  loadLazy(event: LazyLoadEvent) {
    if (!this.measurement) {
      return;
    }

    if (this.selectedChannelRows) {
      const channelsByGroup = this.groupChannelRowsByChannelGroup(this.selectedChannelRows);
      forkJoin(
        of(this.selectedChannelRows.map((x) => x.channelGroup.numberOfRows).reduce((prev, curr) => Math.max(prev, curr), 0)),
        this.requestMeasuredValues(channelsByGroup, event.first, event.rows),
      ).subscribe((res) => {
        this.totalRecords = res[0];
        this.datapoints = this.load(res[1].map((x) => x.measuredValues));
      });
    }
  }

  /**
   * Group the ChannelSelectionRows by ChannelGroup, e.g. the returned record has the ID of the
   * channelGroup as key and the list of corresponding Channels (X and Y Channels) as value.
   * @param channelRows
   */
  private groupChannelRowsByChannelGroup(channelRows: ChannelSelectionRow[]) {
    return channelRows.reduce((previous, currentItem) => {
      const group = currentItem.channelGroup.id;

      if (group != null) {
        if (!previous[group]) {
          previous[group] = {
            channelGroup: currentItem.channelGroup,
            channels: [],
          };
        }
        if (currentItem.xChannel != undefined && previous[group].channels.findIndex((c) => c.id === currentItem.xChannel.id) === -1) {
          previous[group].channels.push(currentItem.xChannel);
        }
        if (currentItem.yChannel != undefined && previous[group].channels.findIndex((c) => c.id === currentItem.yChannel.id) === -1) {
          previous[group].channels.push(currentItem.yChannel);
        }
      }

      return previous;
    }, {} as Record<string, { channelGroup: ChannelGroup; channels: Channel[] }>);
  }

  /**
   * Requests the values of all Channels for all ChannelGroups. Returns an Observable containing
   * all Channels together with their MeasuredValues
   * @param channelsByGroup
   */
  private requestMeasuredValues(
    channelsByGroup: Record<string, { channelGroup: ChannelGroup; channels: Channel[] }>,
    startIndex: number,
    requestSize: number,
  ) {
    const measuredValues: Observable<{ channel: Channel; measuredValues: MeasuredValues }[]>[] = [];
    for (const channelGroupId in channelsByGroup) {
      const channelGroup = this.selectedChannelRows.find((row) => row.channelGroup.id === channelGroupId).channelGroup;

      const rowCount = channelsByGroup[channelGroupId].channelGroup.numberOfRows;
      if (startIndex < rowCount) {
        const obs = this.chartviewerService
          .loadValues(channelGroup, channelsByGroup[channelGroupId].channels, startIndex, Math.min(requestSize, rowCount - startIndex), 0)
          .pipe(map((mv) => this.userFormulaService.mergeMeasuredValuesByName(channelsByGroup[channelGroupId].channels, mv)));

        measuredValues.push(obs);
      } else {
        console.warn(`StartIndex ${startIndex} is greater than rowCount ${rowCount} for channelGroupId ${channelGroupId}!`);
      }
    }
    return forkJoin(measuredValues).pipe(
      map(
        (mv) =>
          [].concat(...mv) as {
            channel: Channel;
            measuredValues: MeasuredValues;
          }[],
      ),
    );
  }
}
