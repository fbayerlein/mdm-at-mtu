/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

/**
 * Simple data component to exchange the chart data between the main window and the popup window
 */
@Injectable({
  providedIn: "root",
})
export class XyChartViewerDataComponent {
  private showWindow = new Subject<boolean>();

  charting = new Subject<any[]>();

  public onShowWindowChange() {
    return this.showWindow.asObservable();
  }

  public sendShowWindowChange(value: boolean) {
    this.showWindow.next(value);
  }
}
