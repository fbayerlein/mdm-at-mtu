/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Channel } from "./channel.class";
import { ChannelGroup } from "./channelgroup.class";

export class ChannelSelectionRow {
  channelGroup: ChannelGroup;
  xChannel: Channel;
  yChannel: Channel;

  constructor(channelGroup: ChannelGroup, yChannel: Channel, xChannel?: Channel) {
    this.channelGroup = channelGroup;
    this.yChannel = yChannel;
    this.xChannel = xChannel;
  }
}
