/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { PrimeChartData } from "../primeNgHelper/prime-chart-data.interface";
import { ChartDataSet } from "./chart-data-set.class";
import { ChartXyDataSet } from "./chart-xy-data-set.class";

export class ChartData implements PrimeChartData {
  labels: any[];
  datasets: (ChartDataSet | ChartXyDataSet)[];

  constructor(labels: any[], datasets: (ChartDataSet | ChartXyDataSet)[]) {
    // this.labels = labels;
    this.datasets = datasets;
  }
}
