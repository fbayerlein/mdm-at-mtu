/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { PortalModule } from "@angular/cdk/portal";
import { NgModule } from "@angular/core";
import { AccordionModule } from "primeng/accordion";
import { ConfirmationService } from "primeng/api";
import { ButtonModule } from "primeng/button";
import { ChartModule } from "primeng/chart";
import { CheckboxModule } from "primeng/checkbox";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DialogModule } from "primeng/dialog";
import { InputNumberModule } from "primeng/inputnumber";
import { InputTextModule } from "primeng/inputtext";
import { ListboxModule } from "primeng/listbox";
import { MultiSelectModule } from "primeng/multiselect";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { PanelModule } from "primeng/panel";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { SelectButtonModule } from "primeng/selectbutton";
import { SliderModule } from "primeng/slider";
import { SpinnerModule } from "primeng/spinner";
import { SplitButtonModule } from "primeng/splitbutton";
import { SplitterModule } from "primeng/splitter";
import { TableModule } from "primeng/table";
import { ToggleButtonModule } from "primeng/togglebutton";
import { TooltipModule } from "primeng/tooltip";
import { TreeTableModule } from "primeng/treetable";
import { VirtualScrollerModule } from "primeng/virtualscroller";
import { MDMCoreModule } from "../core/mdm-core.module";
import { TableViewModule } from "../tableview/tableview.module";
import { ChartViewerComponent } from "./components/chartviewer/chart-viewer.component";
import { ChartViewerNavCardComponent } from "./components/chatviewer-nav-card/chart-viewer-nav-card.component";
import { DataTableComponent } from "./components/datatable/data-table.component";
import { RequestOptionsComponent } from "./components/request-options/request-options.component";
import { ChannelGroupSelectionPanelComponent } from "./components/xy-chart-data-selection-panel/channelgroup-selection-panel.component";
import { XyChartDataSelectionPanelComponent } from "./components/xy-chart-data-selection-panel/xy-chart-data-selection-panel.component";
import { XyChartViewerNavCardComponent } from "./components/xy-chart-viewer-nav-card/xy-chart-viewer-nav-card.component";
import { XyChartViewerToolbarComponent } from "./components/xy-chart-viewer-toolbar/xy-chart-viewer-toolbar.component";
import { XyChartViewerPopupComponent } from "./components/xy-chart-viewer/xy-chart-viewer-popup.component";
import { XyChartViewerComponent } from "./components/xy-chart-viewer/xy-chart-viewer.component";

@NgModule({
  imports: [
    MDMCoreModule,
    CheckboxModule,
    ButtonModule,
    SelectButtonModule,
    TableModule,
    SliderModule,
    SplitterModule,
    InputTextModule,
    AccordionModule,
    ChartModule,
    ToggleButtonModule,
    SpinnerModule,
    MultiSelectModule,
    ListboxModule,
    PanelModule,
    ConfirmDialogModule,
    TooltipModule,
    DialogModule,
    VirtualScrollerModule,
    TreeTableModule,
    ScrollPanelModule,
    OverlayPanelModule,
    InputNumberModule,
    TableViewModule,
    PortalModule,
    TableModule,
    SplitButtonModule,
  ],
  declarations: [
    ChartViewerNavCardComponent,
    ChartViewerComponent,
    DataTableComponent,
    XyChartViewerComponent,
    XyChartViewerNavCardComponent,
    RequestOptionsComponent,
    XyChartViewerToolbarComponent,
    XyChartDataSelectionPanelComponent,
    XyChartViewerPopupComponent,
    ChannelGroupSelectionPanelComponent,
  ],
  exports: [ChartViewerNavCardComponent, XyChartViewerNavCardComponent],
  providers: [ConfirmationService],
})
export class ChartviewerModule {}
