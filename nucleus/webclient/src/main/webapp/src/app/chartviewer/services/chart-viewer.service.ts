/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { MDMIdentifier } from "@core/mdm-identifier";
import { Subject } from "rxjs";
import { MDMItem } from "../../core/mdm-item";
import { ChartPoint, ChartXyDataSet, MeasuredValues } from "../model/chartviewer.model";
import { Channel } from "../model/types/channel.class";
import { getDataArray } from "../model/types/measured-values.class";
import { QueryConfig } from "../model/types/query-config.class";

@Injectable({
  providedIn: "root",
})
export class ChartViewerService {
  private nodeMetaSubject = new Subject<MDMIdentifier>();

  // inbound from mdm-navigator component context menu
  private appendNodeSubject = new Subject<MDMIdentifier>();
  private appendMDMItemSubject = new Subject<MDMItem[]>();

  // outbound from xy-chart-viewer-nav-card after loading from server-side
  private appendNodeMetaSubject = new Subject<MDMIdentifier>();

  private selectChannelSubject = new Subject<Channel>();

  private queryConfigSubject = new Subject<QueryConfig>();

  private respectFlags: boolean;

  constructor() {}

  public setRescpectFlags(value: boolean) {
    this.respectFlags = value;
  }

  public sendNodeMeta(ident: MDMIdentifier) {
    this.nodeMetaSubject.next(ident);
  }

  public onNodeMetaChange() {
    return this.nodeMetaSubject.asObservable();
  }

  public sendAppendNode(ident: MDMIdentifier) {
    this.appendNodeSubject.next(ident);
  }

  public onAppendNodeChange() {
    return this.appendNodeSubject.asObservable();
  }

  public sendAppendMetaNode(ident: MDMIdentifier) {
    this.appendNodeMetaSubject.next(ident);
  }

  public onAppendNodeMetaChange() {
    return this.appendNodeMetaSubject.asObservable();
  }

  public sendAppendMDMItem(mdmItem: MDMItem[]) {
    this.appendMDMItemSubject.next(mdmItem);
  }

  public onAppendMDMItemChange() {
    return this.appendMDMItemSubject.asObservable();
  }

  public sendSelectChannel(channel: Channel) {
    this.selectChannelSubject.next(channel);
  }

  public onSelectChannelChange() {
    return this.selectChannelSubject.asObservable();
  }

  public sendQueryConfig(queryConfig: QueryConfig) {
    this.queryConfigSubject.next(queryConfig);
  }

  public onQueryConfigChange() {
    return this.queryConfigSubject.asObservable();
  }

  public getDataArrayWithFlagsRespection(measuredValues: MeasuredValues) {
    return getDataArray(measuredValues, this.respectFlags);
  }

  public toXyDataSet(xData: MeasuredValues, yData: MeasuredValues) {
    const xValues = (
      xData !== undefined && this.getDataArrayWithFlagsRespection(xData) !== undefined
        ? this.getDataArrayWithFlagsRespection(xData).values
        : undefined
    ) as number[];
    const yDataArray = this.getDataArrayWithFlagsRespection(yData);
    const yValues = (yDataArray !== undefined ? yDataArray.values : undefined) as number[];
    const points = this.getDataPoints(xValues, yValues);
    const dataset = new ChartXyDataSet(
      yData.name + (yData.optionalLabel ? " (" + yData.optionalLabel + ") " : "") + " [" + yData.unit + "]",
      points,
    );
    dataset.xUnit = xData !== undefined ? xData.unit : null;
    dataset.yUnit = yData.unit;
    return dataset;
  }

  private getDataPoints(xData: number[], yData: number[], startIndex = 0) {
    if (xData !== undefined && yData !== undefined && xData.length <= yData.length) {
      return xData.map((x, i) => new ChartPoint(x, yData[i]));
    } else if (xData !== undefined && yData !== undefined && xData.length > yData.length) {
      return yData.map((y, i) => new ChartPoint(xData[i], y));
    }
  }
}
