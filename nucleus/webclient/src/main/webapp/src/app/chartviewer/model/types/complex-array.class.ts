/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

export class ComplexNumber {
  re: number;
  im: number;

  constructor(re: number, im?: number) {
    this.re = re;
    this.im = im | 0;
  }
  public toString() {
    return `(${this.re}, ${this.im})`;
  }
}

export class ComplexArray {
  values: ComplexNumber[];
}
