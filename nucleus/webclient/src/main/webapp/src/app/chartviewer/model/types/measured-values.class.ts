/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { BooleanArray } from "./boolean-array.class";
import { ByteArray } from "./byte-array.class";
import { ComplexArray } from "./complex-array.class";
import { DateArray } from "./date-array.class";
import { NumberArray } from "./number-array.class";
import { StringArray } from "./string-array.class";

export class MeasuredValues {
  name: string;
  optionalLabel: string;
  unit: string;
  length: number;
  independent: boolean;
  axisType: string;
  scalarType: string;
  stringArray: StringArray;
  dateArray: DateArray;
  booleanArray: BooleanArray;
  byteArray: ByteArray;
  shortArray: NumberArray;
  integerArray: NumberArray;
  longArray: NumberArray;
  floatArray: NumberArray;
  doubleArray: NumberArray;
  // byteStreamArray: ByteStreamArray;
  floatComplexArray: ComplexArray;
  doubleComplexArray: ComplexArray;
  flags: boolean[];
}

export const TYPES_SCALAR_NUMERIC = ["SHORT", "INTEGER", "FLOAT", "LONG", "DOUBLE"];

/**
 * Extracts the data array from MeasuredValues und replaces invalid values with null
 * @param m MeasuredValues
 * @returns NumberArray | DateArray | BooleanArray | StringArray
 */
export function getDataArray(m: MeasuredValues, respectFlags: boolean) {
  if (m.scalarType === "INTEGER") {
    return respectFlags ? numberApplyFlags(m.integerArray, m.flags) : m.integerArray;
  } else if (m.scalarType === "LONG") {
    return respectFlags ? numberApplyFlags(m.longArray, m.flags) : m.longArray;
  } else if (m.scalarType === "FLOAT") {
    return respectFlags ? numberApplyFlags(m.floatArray, m.flags) : m.floatArray;
  } else if (m.scalarType === "DOUBLE") {
    return respectFlags ? numberApplyFlags(m.doubleArray, m.flags) : m.doubleArray;
  } else if (m.scalarType === "DATE") {
    return respectFlags ? dateApplyFlags(m.dateArray, m.flags) : m.dateArray;
  } else if (m.scalarType === "SHORT") {
    return respectFlags ? numberApplyFlags(m.shortArray, m.flags) : m.shortArray;
  } else if (m.scalarType === "BOOLEAN") {
    return respectFlags ? booleanApplyFlags(m.booleanArray, m.flags) : m.booleanArray;
  } else if (m.scalarType === "FLOAT_COMPLEX") {
    return respectFlags ? complexApplyFlags(m.floatComplexArray, m.flags) : m.floatComplexArray;
  } else if (m.scalarType === "DOUBLE_COMPLEX") {
    return respectFlags ? complexApplyFlags(m.doubleComplexArray, m.flags) : m.doubleComplexArray;
  } else if (m.scalarType === "BYTE") {
    return respectFlags ? numberApplyFlags(numberFromByteArray(m.byteArray), m.flags) : numberFromByteArray(m.byteArray);
  } else if (m.scalarType === undefined && m.stringArray !== undefined) {
    m.scalarType = "STRING";
    return respectFlags ? stringApplyFlags(m.stringArray, m.flags) : m.stringArray;
  } else {
    throw new Error("ScalarType " + m.scalarType + " not supported.");
  }
}
function complexApplyFlags(n: ComplexArray, flags: boolean[]) {
  const a = new StringArray();
  a.values = (n.values || []).map((n, i) => (flags[i] ? "(" + n.re + "," + (n.im | 0) + ")" : null));
  return a;
}

function numberFromByteArray(n: ByteArray) {
  const decodedString = atob(n.values); // Decode base64 string
  const a = new NumberArray();
  a.values = [];
  for (let i = 0; i < decodedString.length; i++) {
    a.values.push(decodedString.charCodeAt(i));
  }
  return a;
}

function numberApplyFlags(n: NumberArray, flags: boolean[]) {
  const a = new NumberArray();
  a.values = (n.values || []).map((n, i) => (flags[i] ? n : null));
  return a;
}

function dateApplyFlags(n: DateArray, flags: boolean[]) {
  const a = new DateArray();
  a.values = (n.values || []).map((n, i) => (flags[i] ? n : null));
  return a;
}
function booleanApplyFlags(n: BooleanArray, flags: boolean[]) {
  const a = new BooleanArray();
  a.values = (n.values || []).map((n, i) => (flags[i] ? n : null));
  return a;
}

function stringApplyFlags(s: StringArray, flags: boolean[]) {
  const a = new StringArray();
  a.values = (s.values || []).map((s, i) => (flags[i] ? s : null));
  return a;
}
