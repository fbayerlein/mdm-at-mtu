/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { InfoUnit } from "src/app/administration/units/unit.service";
import { UnitInfo } from "../../../administration/units/unit.model";
import { Channel } from "./channel.class";

/**
 * channel with unit setting
 */
export class ChannelUnitUI extends Channel {
  unit: InfoUnit;
  columns: string[];
  columnsName: string[];
  unitList: UnitInfo[];

  /**
   *
   * @param channel
   * @param unit the unit used by default
   * @param columns the columns used for display
   */
  constructor(channel: Channel, unit?: InfoUnit, columns?: string[]) {
    super();
    this.type = channel.type;
    this.source = channel.source;
    this.id = channel.id;
    this.name = channel.name;
    this.channelGroupId = channel.channelGroupId;
    this.measurement = channel.measurement;
    this.isIndependent = channel.isIndependent;
    this.axisType = channel.axisType;
    this.unitId = channel.unitId;
    this.unitName = channel.unitName;

    if (columns) {
      this.columns = columns;
    } else if (channel["columns"]) {
      this.columns = channel["columns"];
    } else {
      this.columns = [];
    }
    this.unit = unit;
    if (this.unit) {
      this.unitList = this.unit.alternatives.map((u) => new UnitInfo(u));
    }
  }

  public updateColumnData(index: number) {
    this.columns[index] = this.unit.unit.name;
  }

  public getSelectedUnitName() {
    if (this.unit && this.unit.unit) {
      return this.unit.unit.name;
    } else {
      return undefined;
    }
  }

  public setSelectedUnitName(newUnitName: string) {
    const newUnit = this.unit.alternatives.find((u) => u.name === newUnitName);
    if (newUnit) {
      this.unit = new InfoUnit(newUnit, this.unit.physDimId, this.unit.alternatives);
      this.unitId = newUnit.id;
      this.unitName = newUnit.name;
      this.unitList = this.unit.alternatives.map((u) => new UnitInfo(u));
      // Update columns array
    }
  }

  public set selectedUnitId(unitId: string) {
    const newUnit = this.unit.alternatives.find((u) => u.id === unitId);
    if (newUnit) {
      this.unit = new InfoUnit(newUnit, this.unit.physDimId, this.unit.alternatives);
      this.unitId = newUnit.id;
      this.unitName = newUnit.name;
      this.unitList = this.unit.alternatives.map((u) => new UnitInfo(u));
      // Update columns array
    }
  }

  public get selectedUnitId() {
    if (this.unit && this.unit.unit) {
      return this.unit.unit.id;
    } else {
      return ""; // empty string indicates default unit
    }
  }
}
