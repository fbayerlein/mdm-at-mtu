import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { Node } from "../../navigator/node";
import { ValueList } from "./value-list.interface";
import { ValueListService } from "./valuelist.service";

@Component({
  selector: "mdm-tag",
  templateUrl: "./value-list.component.html",
  styleUrls: ["./value-list.component.css"],
})
export class ValueListComponent implements OnInit {
  // dropdown selection
  environments: Node[];
  selectedEnvironment: string;

  valueLists: ValueList[];
  editorMode = false;
  selectedValueList: string;

  constructor(
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private nodeService: NodeService,
    private valueListService: ValueListService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.refreshValueList();
  }

  onEditModeChange(editMode: boolean) {
    this.editorMode = editMode;
  }

  onChangeSelectedValueList(valueList: string) {
    this.selectedValueList = valueList;
  }

  refreshValueList() {
    this.valueLists = undefined;
    if (this.selectedEnvironment !== undefined) {
      this.valueListService.readValueLists(this.selectedEnvironment).subscribe((data) => {
        this.valueLists = data;
      });
    }
  }
}
