/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { DialogModule } from "primeng/dialog";
import { InputTextModule } from "primeng/inputtext";
import { TableModule } from "primeng/table";
import { ToolbarModule } from "primeng/toolbar";
import { CatalogAttributesComponent } from "./catalog-attributes/catalog-attributes.component";
import { CatalogComponentComponent } from "./catalog-component/catalog-component.component";
import { CatalogManagerViewerComponent } from "./catalog-manager-viewer/catalog-manager-viewer.component";
import { CatalogManagerComponent } from "./catalog-manager.component";
import { CatalogManagerService } from "./catalog-manager.service";
import { CatalogRoutingModule } from "./catalog-routing.module";

@NgModule({
  imports: [
    MDMCoreModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ToolbarModule,
    InputTextModule,
    CatalogRoutingModule,
  ],
  declarations: [CatalogManagerComponent, CatalogComponentComponent, CatalogAttributesComponent, CatalogManagerViewerComponent],
  exports: [],
  providers: [CatalogManagerService],
})
export class CatalogModule {}
