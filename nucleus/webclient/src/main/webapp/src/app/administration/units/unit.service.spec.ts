/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule, HttpTestingController, RequestMatch } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { HttpErrorHandler } from "@core/http-error-handler";
import { PreferenceService } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { InfoUnit, SourceUnits, Unit, UnitService, UnitsPerSource } from "./unit.service";

describe("UnitService", () => {
  let httpTestingController: HttpTestingController;

  const reqMatch: RequestMatch = { method: "GET" };

  let unitService: UnitService;

  const testData = {
    type: "Unit",
    data: [
      {
        name: "m",
        id: "1",
        type: "Unit",
        sourceType: "Unit",
        sourceName: "MTU_TEST",
        attributes: [
          {
            name: "Description",
            value: "",
            unit: "",
            dataType: "STRING",
          },
          {
            name: "Factor",
            value: "1.0",
            unit: "",
            dataType: "DOUBLE",
          },
          {
            name: "DateCreated",
            value: "",
            unit: "",
            dataType: "DATE",
          },
          {
            name: "dB_reference_factor",
            value: "",
            unit: "",
            dataType: "FLOAT",
          },
          {
            name: "dB",
            value: "false",
            unit: "",
            dataType: "BOOLEAN",
          },
          {
            name: "MimeType",
            value: "application/x-asam.aounit",
            unit: "",
            dataType: "STRING",
          },
          {
            name: "Offset",
            value: "0.0",
            unit: "",
            dataType: "DOUBLE",
          },
          {
            name: "Name",
            value: "m",
            unit: "",
            dataType: "STRING",
          },
        ],
        relations: [
          {
            name: null,
            type: "MUTABLE",
            entityType: "PhysicalDimension",
            contextType: null,
            ids: ["3"],
          },
        ],
      },
      {
        name: "mm",
        id: "104",
        type: "Unit",
        sourceType: "Unit",
        sourceName: "MTU_TEST",
        attributes: [
          {
            name: "Description",
            value: "",
            unit: "",
            dataType: "STRING",
          },
          {
            name: "Factor",
            value: "0.0010000000474974513",
            unit: "",
            dataType: "DOUBLE",
          },
          {
            name: "DateCreated",
            value: "2014-02-11T16:37:21Z",
            unit: "",
            dataType: "DATE",
          },
          {
            name: "dB_reference_factor",
            value: "",
            unit: "",
            dataType: "FLOAT",
          },
          {
            name: "dB",
            value: "false",
            unit: "",
            dataType: "BOOLEAN",
          },
          {
            name: "MimeType",
            value: "application/x-asam.aounit",
            unit: "",
            dataType: "STRING",
          },
          {
            name: "Offset",
            value: "0.0",
            unit: "",
            dataType: "DOUBLE",
          },
          {
            name: "Name",
            value: "mm",
            unit: "",
            dataType: "STRING",
          },
        ],
        relations: [
          {
            name: null,
            type: "MUTABLE",
            entityType: "PhysicalDimension",
            contextType: null,
            ids: ["3"],
          },
        ],
      },
    ],
  };
  const unit1 = new Unit();
  const unit2 = new Unit();

  beforeEach(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UnitService, PropertyService, PreferenceService, HttpErrorHandler],
    });

    httpTestingController = TestBed.get(HttpTestingController);
    unitService = TestBed.get(UnitService);

    unit1.id = "1";
    unit1.name = "m";
    unit1.desc = "";
    unit1.cd = "";
    unit1.factor = 1;
    unit1.mt = "application/x-asam.aounit";
    unit1.offset = 0.0;
    unit1.physDimId = "3";

    unit2.id = "104";
    unit2.name = "mm";
    unit2.desc = "";
    unit2.cd = "2014-02-11T16:37:21Z";
    unit2.factor = 0.001;
    unit2.mt = "application/x-asam.aounit";
    unit2.offset = 0.0;
    unit2.physDimId = "3";
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it("getUnitMap", () => {
    unitService.getUnitMap(["MDMNVH"]).subscribe((x) => {
      expect(x).toEqual([
        new UnitsPerSource("MDMNVH", new SourceUnits([new InfoUnit(unit1, "3", [unit1, unit2]), new InfoUnit(unit2, "3", [unit1, unit2])])),
      ]);
    });

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/MDMNVH/units");

    // Assert that the request is a GET.
    expect(req.request.method).toEqual("GET");

    // Respond with mock data, causing Observable to resolve.
    // Subscribe callback asserts that correct data was returned.
    req.flush(testData);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });

  it("loadUnits", () => {
    unitService.loadUnits("MDMNVH").subscribe((x) => {
      expect(x).toEqual(
        new UnitsPerSource("MDMNVH", new SourceUnits([new InfoUnit(unit1, "3", [unit1, unit2]), new InfoUnit(unit2, "3", [unit1, unit2])])),
      );
    });

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/MDMNVH/units");

    // Assert that the request is a GET.
    expect(req.request.method).toEqual("GET");

    // Respond with mock data, causing Observable to resolve.
    // Subscribe callback asserts that correct data was returned.
    req.flush(testData);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });

  it("getUnitsForSource", () => {
    unitService.getUnitsForSource("MDMNVH").subscribe((x) => {
      expect(x.length).toBe(2);
    });

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/MDMNVH/units");

    // Assert that the request is a GET.
    expect(req.request.method).toEqual("GET");

    // Respond with mock data, causing Observable to resolve.
    // Subscribe callback asserts that correct data was returned.
    req.flush(testData);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });
});
