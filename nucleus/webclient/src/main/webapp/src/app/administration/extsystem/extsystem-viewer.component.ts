/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject } from "rxjs";
import { MDMNotificationService } from "../../core/mdm-notification.service";
import { Attribute, Node } from "../../navigator/node";
import { ExtSystemService } from "./extsystem.service";

@Component({
  selector: "mdm-extsystem-viewer",
  templateUrl: "./extsystem-viewer.component.html",
  styleUrls: ["./extsystem.component.css"],
})
export class ExtSystemViewerComponent implements OnInit {
  // passed down from parent
  @Input() extSystems: Node[];
  @Input() selectedEnvironment: string;

  @Output() editMode = new EventEmitter<boolean>();
  @Output() selectedES = new EventEmitter<string>();

  // dialog and loading states
  dialogExtSystemCreate = false;
  dialogExtSystemDelete = false;
  loadingExtSystemAttr = false;

  // temporary data for dialogs
  tmpExtSystemCreate: Node;
  tmpExtSystemDelete: Node;

  // external system attributes
  extSystemAttrs: Node[];
  bsExtSystemAttrs: BehaviorSubject<Node[]> = new BehaviorSubject<Node[]>(undefined);

  // table selection
  selectedExtSystem: Node;
  selectedExtSystemAttr: Node;

  constructor(
    private extSystemService: ExtSystemService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {
    this.bsExtSystemAttrs.subscribe((value) => {
      this.extSystemAttrs = value;
    });
  }

  ngOnInit() {
    this.bsExtSystemAttrs.next(undefined);
  }

  getExternalSystems() {
    const data = [];
    for (const i in this.extSystems) {
      if (this.extSystems[i].type === "ExtSystem") {
        data.push(this.extSystems[i]);
      }
    }
    return data;
  }

  getExternalSystemAttributes() {
    if (!this.extSystemAttrs) {
      return [];
    }
    return this.extSystemAttrs.filter((attr) => attr.type === "ExtSystemAttribute");
  }

  getMDMAttributes() {
    const ids = [];
    // need double forEach as the map function transfers the array into another array
    this.selectedExtSystemAttr.relations
      .filter((rel) => rel.entityType === "MDMAttribute")
      .map((rel) => rel.ids)
      .forEach((i) => i.forEach((id) => ids.push(id)));
    return this.extSystemAttrs.filter((attr) => attr.type === "MDMAttribute" && ids.find((i) => i === attr.id));
  }

  getAttributeValueFromNode(node: Node, attribute: string) {
    for (const attr of node.attributes) {
      if (attr.name === attribute) {
        return attr.value;
      }
    }
    return "";
  }

  getAttributeFromNode(node: Node, attribute: string) {
    if (node.attributes !== undefined) {
      for (const attr of node.attributes) {
        if (attr.name === attribute) {
          return attr;
        }
      }
    }
    return undefined;
  }

  onExtSystemRowSelect(event: any) {
    this.selectedExtSystemAttr = undefined;
    this.bsExtSystemAttrs.next(undefined);
    this.loadingExtSystemAttr = true;
    this.extSystemService.getExtSystemAttributesForScope(this.selectedEnvironment, this.selectedExtSystem.id).subscribe((attrs) => {
      this.bsExtSystemAttrs.next(attrs);
      this.loadingExtSystemAttr = false;
    });
  }

  onExtSystemRowUnselect(event: any) {
    this.bsExtSystemAttrs.next(undefined);
  }

  createAttribute(name: string, value?: string) {
    const attr = new Attribute();
    attr.dataType = "STRING";
    attr.unit = "";
    attr.value = value !== undefined ? value : "";
    attr.name = name;
    return attr;
  }

  getIndicesForIds(ids: string[]) {
    const indices = [];
    for (const id of ids) {
      for (const extSystem of this.extSystems) {
        if (extSystem.id === id) {
          indices.push(this.extSystems.indexOf(extSystem));
        }
      }
    }
    return indices;
  }

  addExtSystem() {
    this.tmpExtSystemCreate = new Node();
    this.tmpExtSystemCreate.type = "ExtSystem";
    this.tmpExtSystemCreate.sourceType = "ExtSystem";
    this.tmpExtSystemCreate.sourceName = this.selectedEnvironment;
    this.tmpExtSystemCreate.attributes = [];
    this.tmpExtSystemCreate.attributes.push(this.createAttribute("Description"));
    this.tmpExtSystemCreate.attributes.push(this.createAttribute("Name"));
    this.tmpExtSystemCreate.attributes.push(this.createAttribute("MimeType", "application/x-asam.aoany.extsystem"));
    this.dialogExtSystemCreate = true;
  }

  editExtSystem(extSystem?: Node) {
    if (extSystem != undefined) {
      this.tmpExtSystemCreate = extSystem;
      // this.dialogExtSystem = true;
      this.selectedES.next(extSystem.id);
      this.editMode.next(true);
    }
  }

  removeExtSystem(extSystem?: Node) {
    this.tmpExtSystemDelete = extSystem;
    this.dialogExtSystemDelete = true;
  }

  cancelRemoveExtSystem() {
    this.tmpExtSystemDelete = undefined;
    this.dialogExtSystemDelete = false;
  }

  confirmRemoveExtSystem() {
    if (this.tmpExtSystemDelete != undefined) {
      const idxES: number = this.extSystems.indexOf(this.tmpExtSystemDelete);
      if (idxES !== -1) {
        this.extSystems.splice(idxES, 1);
        if (this.tmpExtSystemDelete.relations !== undefined && this.tmpExtSystemDelete.relations.length > 0) {
          // remove all children
          let indices = new Array<number>();
          for (const relation of this.tmpExtSystemDelete.relations) {
            // the ext system attributes
            const indicesESA = this.getIndicesForIds(relation.ids);
            for (const ind of indicesESA) {
              for (const r of this.extSystems[ind].relations) {
                // the mdm attributes
                indices = indices.concat(this.getIndicesForIds(r.ids));
              }
            }
            indices = indices.concat(indicesESA);
          }
          indices.sort((a, b) => b - a);
          for (const i of indices) {
            this.extSystems.splice(i, 1);
          }
        }
      }
      if (this.tmpExtSystemDelete.id !== undefined && parseInt(this.tmpExtSystemDelete.id, 10) > 0) {
        this.extSystemService.deleteExtSystem(this.selectedEnvironment, this.tmpExtSystemDelete.id).subscribe();
      }
    }
    this.selectedExtSystem = undefined;
    this.tmpExtSystemDelete = undefined;
    this.dialogExtSystemDelete = false;
  }

  // for new external systems only
  saveDialogExtSystem() {
    // update the name attribute
    this.getAttributeFromNode(this.tmpExtSystemCreate, "Name").value = this.tmpExtSystemCreate.name;
    this.extSystemService.saveExtSystem(this.selectedEnvironment, this.tmpExtSystemCreate).subscribe(
      (response) => this.patchResponse(response.data as Node[]),
      (error) =>
        this.notificationService.notifyError(this.translateService.instant("administration.extsystem.err-cannot-save-ext-system"), error),
    );
    this.dialogExtSystemCreate = false;
  }

  cancelDialogExtSystem() {
    this.dialogExtSystemCreate = false;
    this.tmpExtSystemCreate = undefined;
  }

  patchResponse(nodes: Node[]) {
    for (const node of nodes) {
      if (node.name === this.tmpExtSystemCreate.name) {
        if (this.tmpExtSystemCreate.id === undefined) {
          this.extSystems.push(this.tmpExtSystemCreate);
        }
        this.tmpExtSystemCreate.id = node.id;
      }
    }
    this.tmpExtSystemCreate = undefined;
  }
}
