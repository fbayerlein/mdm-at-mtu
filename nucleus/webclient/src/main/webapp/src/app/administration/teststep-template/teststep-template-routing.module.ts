import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TeststepTemplateComponent } from "./teststep-template-component/teststep-template.component";
import { TeststepTemplateDetailsComponent } from "./teststep-template-details/teststep-template-details.component";
import { TeststepTemplateViewerComponent } from "./teststep-template-viewer/teststep-template-viewer.component";

const TestStepTemplateRoutes: Routes = [
  {
    path: "",
    component: TeststepTemplateViewerComponent,

    children: [
      {
        path: "",
        component: TeststepTemplateComponent,
        data: { breadcrumb: "Test step template" },
      },
      {
        path: ":selectedTestStepTplName/:selectedTestStepTplId",
        component: TeststepTemplateDetailsComponent,
        data: { breadcrumb: ":selectedTestStepTplName" },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(TestStepTemplateRoutes)],
  exports: [RouterModule],
})
export class TestStepTemplateRoutingModule {}
