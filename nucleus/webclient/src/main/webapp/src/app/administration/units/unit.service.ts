/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { Attribute, Node } from "@navigator/node";
import { forkJoin, Observable } from "rxjs";
import { catchError, map, take, tap } from "rxjs/operators";
import { CachableSourceService } from "./cachable-source.service";
import { PhysDimTuple } from "./physicaldimension.service";

/**
 * unit info used in drop down / multi-select
 */
export class UnitInfo {
  unitName: string;
  unitId: string;

  constructor(unit?: Unit) {
    this.unitName = (unit && unit.name) || "";
    this.unitId = (unit && unit.id) || "";
  }
}

/**
 * physical dimension info used in channel table row
 */
export class PhysDimInfo extends PhysDimTuple {
  unitList: UnitInfo[];
  unit: UnitInfo;

  constructor(physDim?: PhysDimTuple, units?: Unit[]) {
    super();
    this.id = (physDim && physDim.id) || "";
    this.name = (physDim && physDim.name) || "";
    this.unit = new UnitInfo();
    this.unitList = [this.unit];

    if (units !== undefined && units.length > 0 && this.id.trim().length > 0) {
      const physDimUnits: Unit[] = units.filter((u) => u.physDimId === this.id);
      if (physDimUnits.length > 0) {
        this.unitList = physDimUnits.map((u) => new UnitInfo(u));
        this.unit = this.unitList[0];
      }
    }
  }
}

export class Unit {
  physDimId: string;
  cd: string;
  mt: string;
  dB: boolean;
  dBRefFactor: number;
  offset: number;
  factor: number;
  desc: string;
  name: string;
  id: string;

  constructor() {
    this.dB = false;
    this.mt = "application/x-asam.aounit";
    this.cd = "";
    this.name = "";
    this.factor = 1.0;
    this.offset = 0.0;
    this.physDimId = "";
  }
}

export class InfoUnit {
  unit: Unit;
  physDimId: string;
  alternatives: Unit[];

  constructor(unit: Unit, physDimId: string, alternatives: Unit[]) {
    this.unit = unit;
    this.physDimId = physDimId;
    this.alternatives = alternatives;
  }
}

export class SourceUnits {
  [unitId: string]: InfoUnit;
  constructor(units: InfoUnit[]) {
    units.forEach((u) => (this[u.unit.id] = u));
  }
}

export class UnitsPerSource {
  [sourceName: string]: SourceUnits;
  constructor(source: string, units: SourceUnits) {
    this[source] = units;
  }
}

@Injectable()
export class UnitService extends CachableSourceService<Unit[]> {
  private prefEndpoint: string;

  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json",
    }),
  };
  unitService: any;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    super();
    this.prefEndpoint = this._prop.getUrl("mdm/environments/");
  }

  public getUnitMap(envs: string[]) {
    return forkJoin(envs.map((e) => this.loadUnits(e).pipe(take(1))));
  }

  public loadUnits(sourceName: string) {
    return this.getUnitsForSource(sourceName).pipe(map((u) => new UnitsPerSource(sourceName, this.groupUnits(u))));
  }

  private groupUnits(units: Unit[]) {
    const y: SourceUnits = {};
    const physDims = new Map<string, Unit[]>();

    units.forEach((u) => {
      y[u.id] = new InfoUnit(u, u.physDimId, []);
      const us = physDims.get(u.physDimId);
      if (us === undefined) {
        physDims.set(u.physDimId, [u]);
      } else {
        us.push(u);
      }
    });

    return new SourceUnits(
      units.map((u) => {
        y[u.id].alternatives = physDims.get(u.physDimId);
        return y[u.id];
      }),
    );
  }

  protected requestDataForSource(source: string): Observable<Unit[]> {
    return this.http.get(this.prefEndpoint + source + "/units").pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request units!")),
    );
  }

  /**
   * this method will cache data
   * @param source
   * @returns
   */
  public getUnitsForSource(source: string): Observable<Unit[]> {
    return super.getDataForSource(source);
  }

  /**
   * IMPORTANT: make sure the provided physDims have been read from same source
   * @param source
   * @param physDims
   * @returns
   */
  public getPhysDimInfosForSource(source: string, physDims: PhysDimTuple[]): Observable<PhysDimInfo[]> {
    return super.getDataForSource(source).pipe(map((units) => physDims.map((pd) => new PhysDimInfo(pd, units))));
  }

  private requestUnitForSource(source: string, id: string): Observable<Unit[]> {
    return this.http.get(this.prefEndpoint + source + "/units/" + id).pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request units!")),
    );
  }

  /**
   * this method will not cache data
   * but it will return cached data, if 'getUnitsForSource' was called already
   * and cache was initialized
   *
   * @param source
   * @param id
   * @returns
   */
  public getUnitForSource(source: string, id: string): Observable<Unit[]> {
    if (!this.cachedSources || !this.cachedSources.has(source)) {
      return this.requestUnitForSource(source, id);
    }
    return this.cachedSources.get(source).observable.pipe(map((values) => values.filter((u) => u.id === id)));
  }

  private deleteUnitForSourceFromCache(source: string, id: string, x: any) {
    if (x && this.cachedSources && this.cachedSources.has(source)) {
      const units = this.cachedSources.get(source).data;
      this.cachedSources.get(source).data = units.filter((u) => u.id !== id);
      this.cachedSources.get(source).setNext();
    }
  }

  public deleteUnitForSource(source: string, id: string): Observable<any> {
    return this.http.delete(this.prefEndpoint + source + "/units/" + id).pipe(
      catchError((e) => addErrorDescription(e, "Could not request units!")),
      tap((x) => this.deleteUnitForSourceFromCache(source, id, x)),
    );
  }

  private updateUnitForSourceInCache(source: string, changedUnits: Unit[]) {
    if (this.cachedSources && this.cachedSources.has(source)) {
      const units = this.cachedSources.get(source).data;
      const tmpUnits: Unit[] = [];
      changedUnits.forEach((u) => tmpUnits.push(u));
      const updatedUnits: Unit[] = [];
      for (const u of units) {
        const idx = tmpUnits.findIndex((tu) => tu.id === u.id);
        if (idx > 0) {
          updatedUnits.push(tmpUnits[idx]);
          tmpUnits.splice(idx, 1);
        } else {
          updatedUnits.push(u);
        }
      }
      this.cachedSources.get(source).data = updatedUnits;
      units.length = 0;
      tmpUnits.length = 0;
      this.cachedSources.get(source).setNext();
    }
  }

  public updateUnitForSource(source: string, unit: Unit): Observable<Unit[]> {
    const requestData = new Object();
    requestData["PhysicalDimension"] = unit.physDimId;
    requestData["Name"] = unit.name;
    requestData["Description"] = this.isSet(unit.desc) ? unit.desc : "";
    requestData["Factor"] = unit.factor;
    requestData["Offset"] = unit.offset;
    requestData["dB"] = unit.dB;

    if (unit.dBRefFactor !== null && unit.dBRefFactor !== undefined) {
      requestData["dB_reference_factor"] = this.toPrecision(unit.dBRefFactor, 5).toString();
    }

    return this.http.put(this.prefEndpoint + source + "/units/" + unit.id, JSON.stringify(requestData), this.httpOptions).pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => {
        const units = this.convert(res);
        this.updateUnitForSourceInCache(source, units);
        return units;
      }),
      catchError((e) => addErrorDescription(e, "Could not request units!")),
    );
  }

  private createUnitForSourceInCache(source: string, newUnits: Unit[]) {
    if (this.cachedSources && this.cachedSources.has(source)) {
      const units = this.cachedSources.get(source).data;
      newUnits.forEach((u) => units.push(u));
      this.cachedSources.get(source).setNext();
    }
  }

  public createUnitForSource(source: string, unit: Unit): Observable<Unit[]> {
    const requestData = new Object();
    requestData["Name"] = unit.name;
    requestData["PhysicalDimension"] = unit.physDimId;

    return this.http.post(this.prefEndpoint + source + "/units", JSON.stringify(requestData), this.httpOptions).pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => {
        const units = this.convert(res);
        this.createUnitForSourceInCache(source, units);
        return units;
      }),
      catchError((e) => addErrorDescription(e, "Could not request units!")),
    );
  }

  private convert(res: any[]): Unit[] {
    const nodes = <Node[]>res;

    const retVal = new Array<Unit>();

    for (const node of nodes) {
      retVal.push(this.convertFromNode(node));
    }
    return retVal;
  }

  private isSet(value: string): boolean {
    return value !== null && value !== undefined && value !== "";
  }

  private toPrecision(value: number, prec: number): number {
    let retVal = value;

    if (value !== null && value !== undefined) {
      const offs = Math.pow(10, prec);
      retVal = Math.round(offs * value) / offs;
    }
    return retVal;
  }

  private convertFromNode(qttNd: Node): Unit {
    const u = new Unit();
    u.id = qttNd.id;
    u.name = qttNd.name;

    const attribs = qttNd.attributes;

    u.desc = this.findAttrValue(attribs, "Description");
    u.cd = this.findAttrValue(attribs, "DateCreated");
    u.mt = this.findAttrValue(attribs, "MimeType");

    const f = this.findAttrValue(attribs, "Factor");
    if (this.isSet(f)) {
      u.factor = this.toPrecision(parseFloat(f), 7);
    }

    const o = this.findAttrValue(attribs, "Offset");
    if (this.isSet(o)) {
      u.offset = this.toPrecision(parseFloat(o), 7);
    }

    const dbrf = this.findAttrValue(attribs, "dB_reference_factor");
    if (this.isSet(dbrf)) {
      u.dBRefFactor = this.toPrecision(parseFloat(dbrf), 5);
    }

    const db = this.findAttrValue(attribs, "dB");
    if (this.isSet(db)) {
      u.dB = JSON.parse(db);
    } else {
      u.dB = false;
    }

    u.physDimId = this.findRelId(qttNd);

    return u;
  }

  private findAttrValue(attribs: Attribute[], attrName: string): string {
    const attr = attribs.find((a) => a.name === attrName);

    return attr !== undefined ? attr.value.toString() : null;
  }

  private findRelId(qttNd: Node): string {
    const rel = qttNd.relations.find((r) => r.entityType === "PhysicalDimension");

    return rel !== undefined ? rel.ids[0] : null;
  }
}
