export class TestTemplate {
  id: string;
  name: string;
  version: string;
  type: string;
  description: string;
  validFlag: string;
  dateCreated: string;
  templateTestStepUsage: string[];
}
