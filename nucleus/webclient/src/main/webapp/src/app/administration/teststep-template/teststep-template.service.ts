import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Node, NodeArray } from "@navigator/node";
import { map, Observable } from "rxjs";
import { TemplateRoot } from "../template-manager/models/template-root.model";
import { TestStepTemplate } from "./models/teststep-template.model";

@Injectable()
export class TestStepTemplateManagerService {
  defaultUrl: string = "/org.eclipse.mdm.nucleus/mdm/environments/";

  constructor(private http: HttpClient) {}

  getTplTests(source: string): Observable<TestStepTemplate[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplteststeps/")
      .pipe(map((res) => res.data.map((node) => this.convertToTestStepTemplate(node))));
  }
  convertToTestStepTemplate(node) {
    return {
      description: node.attributes.find((a) => a.name === "Description").value,
      name: node.name,
      id: node.id,
      type: node.type,
      dateCreated: node.attributes.find((a) => a.name === "DateCreated").value,
      validFlag: node.attributes.find((a) => a.name === "ValidFlag").value,
      version: node.attributes.find((a) => a.name === "Version").value,
      uutTplRootId: this.findRelId(node, "UNITUNDERTEST"),
      teTplRootId: this.findRelId(node, "TESTEQUIPMENT"),
      tsTplRootId: this.findRelId(node, "TESTSEQUENCE"),
    } as TestStepTemplate;
  }

  updateTestStepTpl(source: string, tplId: string, testStepTpl: any) {
    return this.http.put(this.defaultUrl + source + "/tplteststeps/" + tplId, testStepTpl);
  }
  CreateNewTestStepTpl(source: string, testStepTplName: any) {
    return this.http.post(this.defaultUrl + source + "/tplteststeps/", testStepTplName);
  }
  deleteTestStepTpl(source: string, tplId: string) {
    return this.http.delete(this.defaultUrl + source + "/tplteststeps/" + tplId);
  }

  getTplStepTestById(source: string, tplId: string) {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplteststeps/" + tplId)
      .pipe(map((res) => res.data.map((node) => this.convertToTestStepTemplate(node))));
  }
  getTemplateRoots(source: string, contextType: string): Observable<TemplateRoot[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplroots/" + contextType)
      .pipe(map((res) => res.data.map((data) => this.convertToTemplateRoot(data))));
  }
  convertToTemplateRoot(node) {
    return {
      description: node.attributes.find((a) => a.name === "Description").value,
      name: node.name,
      id: node.id,
      type: node.type,
      dateCreated: node.attributes.find((a) => a.name === "DateCreated").value,
      validFlag: node.attributes.find((a) => a.name === "ValidFlag").value,
      version: node.attributes.find((a) => a.name === "Version").value,
    } as TemplateRoot;
  }
  private findRelId(node: Node, contextType: string): string {
    const rel = node.relations.find((r) => r.contextType === contextType);
    return rel !== undefined ? rel.ids[0] : null;
  }
}
