import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { forkJoin } from "rxjs";
import { TranslationService } from "../../translation/translation.service";
import { Unit, UnitService } from "../../units/unit.service";
import { TemplateComponent } from "../models/template-component.model";
import { TemplateRoot } from "../models/template-root.model";
import { TemplateManagerService } from "../template-manager.service";

@Component({
  selector: "mdm-template-attributes",
  templateUrl: "./template-attributes.component.html",
  styleUrls: ["./template-attributes.component.css"],
})
export class TemplateAttributesComponent implements OnInit {
  tplComponent: TemplateComponent;
  selectedContextType: string;
  selectedTemplateName: string;
  selectedTplChildName: string;
  selectedTemplateId: string;
  selectedTemplateChildId: string;
  selectedTplComp: TemplateComponent;
  selectedEnvironment: string;
  parentId: string;
  selectedTplName: string;
  catalogAttr: any[];
  isValueReadOnly: boolean;
  units: Unit[];
  isOptional: boolean;
  isEditable: boolean;
  tplAttrs: any[];
  currentlyEditedAttr: any;
  currentlyEditedTpl: any;
  templateRoot: TemplateRoot;
  constructor(
    private templateManagerService: TemplateManagerService,
    private router: Router,
    private route: ActivatedRoute,
    private unitService: UnitService,
    private translationService: TranslationService,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.selectedContextType = this.route.snapshot.paramMap.get("contextType").toLocaleLowerCase();
    this.selectedTemplateId = this.route.snapshot.paramMap.get("selectedTplId");
    this.selectedTemplateChildId = this.route.snapshot.paramMap.get("selectedTplChildId");
    this.parentId = this.route.snapshot.paramMap.get("parentId");
    this.selectedTplName = this.route.snapshot.paramMap.get("selectedTplName");

    this.selectedTplChildName = this.route.snapshot.paramMap.get("selectedTplChildName");
    this.templateManagerService.getTemplateRootById(this.selectedEnvironment, this.selectedContextType, this.parentId).subscribe((res) => {
      this.templateRoot = res[0];
      if (this.templateRoot.validFlag === "EDITABLE") {
        this.isEditable = true;
      } else {
        this.isEditable = false;
      }
    });

    this.getTemplateComponentsById();
    this.getUnits();
  }

  getTemplateComponentsById() {
    if (this.selectedTemplateChildId) {
      this.templateManagerService
        .getTplComponentsByNestedComponentId(
          this.selectedEnvironment,
          this.selectedContextType,
          this.parentId,
          this.selectedTemplateId,
          this.selectedTemplateChildId,
        )
        .subscribe((res) => {
          this.tplComponent = res[0];

          const catalogComponentId = this.tplComponent.relations.find((a) => a.entityType === "CatalogComponent").ids[0];
          forkJoin([
            this.templateManagerService.getCatalogComponentById(this.selectedEnvironment, this.selectedContextType, catalogComponentId),
            this.templateManagerService.getCatalogAttributes(this.selectedEnvironment, this.selectedContextType, catalogComponentId),
          ]).subscribe(([catComp, res]) => {
            this.tplComponent.catalogComponent = catComp.name;
            this.catalogAttr = res;

            this.templateManagerService
              .getTemplateAttrsByNestedComponentId(
                this.selectedEnvironment,
                this.selectedContextType,
                this.parentId,
                this.selectedTemplateId,
                this.selectedTemplateChildId,
              )
              .subscribe((res) => {
                this.tplAttrs = res;
                this.catalogAttr = this.catalogAttr.map((catAttr) => {
                  const tplAttr = this.tplAttrs.find((tplAttr) => tplAttr.name === catAttr.name);
                  if (tplAttr) {
                    return {
                      ...catAttr,
                      defaultValue: tplAttr.defaultValue,
                      valueReadonly: tplAttr.valueReadonly,
                      obligatory: tplAttr.obligatory,
                      active: "true",
                      id: tplAttr.id,
                    };
                  }
                  return catAttr;
                });
              });
          });
        });
    } else {
      this.templateManagerService
        .getTemplateComponentsById(this.selectedEnvironment, this.selectedContextType, this.parentId, this.selectedTemplateId)
        .subscribe((res) => {
          this.tplComponent = res[0];

          const catalogComponentId = this.tplComponent.relations.find((a) => a.entityType === "CatalogComponent").ids[0];
          forkJoin([
            this.templateManagerService.getCatalogComponentById(this.selectedEnvironment, this.selectedContextType, catalogComponentId),
            this.templateManagerService.getCatalogAttributes(this.selectedEnvironment, this.selectedContextType, catalogComponentId),
          ]).subscribe(([catComp, res]) => {
            this.tplComponent.catalogComponent = catComp.name;
            this.catalogAttr = res;

            this.templateManagerService
              .getTemplateAttrsByComponentId(this.selectedEnvironment, this.selectedContextType, this.parentId, this.selectedTemplateId)
              .subscribe((res) => {
                this.tplAttrs = res;

                this.catalogAttr = this.catalogAttr.map((catAttr) => {
                  const tplAttr = this.tplAttrs.find((tplAttr) => tplAttr.name === catAttr.name);
                  if (tplAttr) {
                    return {
                      ...catAttr,
                      defaultValue: tplAttr.defaultValue,
                      valueReadonly: tplAttr.valueReadonly,
                      obligatory: tplAttr.obligatory,
                      active: "true",
                      id: tplAttr.id,
                    };
                  }
                  return catAttr;
                });
              });
          });
        });
    }
  }

  /**
   * load all units
   */
  getUnits() {
    this.unitService.getUnitsForSource(this.selectedEnvironment).subscribe((res) => {
      this.units = res;
    });
  }

  /**
   * Edit tpl component
   */
  onTplEditComplete(event) {
    if (event.data !== this.currentlyEditedTpl) {
      this.saveTplComponent(event.data);
    }
  }

  saveTplComponent(tempData) {
    if (!this.selectedTemplateChildId) {
      this.templateManagerService
        .updateTplComponent(this.selectedEnvironment, this.selectedContextType, this.parentId, this.selectedTemplateId, {
          Name: tempData.name,
          Optional: tempData.optional,
          TestStepSeriesVariable: tempData.testStepSeriesVariable,
          DefaultActive: tempData.defaultActive,
        })
        .subscribe((response) => {
          this.getTemplateComponentsById();
        });
    } else {
      this.templateManagerService
        .updateTplComponentChild(
          this.selectedEnvironment,
          this.selectedContextType,
          this.parentId,
          this.selectedTemplateId,
          this.selectedTemplateChildId,
          {
            Name: tempData.name,
            Optional: tempData.optional,
            TestStepSeriesVariable: tempData.testStepSeriesVariable,
            DefaultActive: tempData.defaultActive,
          },
        )
        .subscribe((response) => {
          this.getTemplateComponentsById();
        });
    }
  }

  onTplEditInit(event) {
    this.currentlyEditedTpl = { ...event.data };
  }

  /**
   * Edit attributes
   */
  onEditComplete(event) {
    if (event.data !== this.currentlyEditedAttr) {
      this.saveAttrs(event.data);
    }
  }

  onAttrEditInit(event) {
    this.currentlyEditedAttr = { ...event.data };
  }

  saveAttrs(tplAttr) {
    if (!this.selectedTemplateChildId) {
      this.templateManagerService
        .updateTplComponentAttrs(this.selectedEnvironment, this.selectedContextType, this.parentId, this.selectedTemplateId, tplAttr.id, {
          DefaultValue: tplAttr.defaultValue,
          Obligatory: tplAttr.obligatory,
          ValueReadonly: tplAttr.valueReadonly,
        })
        .subscribe((response) => {
          this.getTemplateComponentsById();
        });
    } else {
      this.templateManagerService
        .updateTplComponentChildAttrs(
          this.selectedEnvironment,
          this.selectedContextType,
          this.parentId,
          this.selectedTemplateId,
          this.selectedTemplateChildId,
          tplAttr.id,
          {
            DefaultValue: tplAttr.defaultValue,
            Obligatory: tplAttr.obligatory,
            ValueReadonly: tplAttr.valueReadonly,
          },
        )
        .subscribe((response) => {
          this.getTemplateComponentsById();
        });
    }
  }

  createTpl(event) {
    if (event.active === "false") {
      this.templateManagerService
        .deleteAttribute(this.selectedEnvironment, this.selectedContextType, this.parentId, this.selectedTemplateId, event.id)
        .subscribe((e) => this.getTemplateComponentsById());
    } else {
      this.templateManagerService
        .createAttribute(this.selectedEnvironment, this.selectedContextType, this.parentId, this.selectedTemplateId, {
          Name: event.name,
        })
        .subscribe((e) => this.getTemplateComponentsById());
    }
  }
}
