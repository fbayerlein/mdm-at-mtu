export class TestStepTemplateUsage {
  name: string;
  id: string;
  sortIndex: string;
  optional: string;
  defaultActive: string;
  testStepRelationId: string;
}
