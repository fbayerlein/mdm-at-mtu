import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "mdm-catalog-manager",
  templateUrl: "./catalog-manager.component.html",
  styleUrls: ["./catalog-manager.component.css"],
})
export class CatalogManagerComponent implements OnInit {
  selectedContextType: string;
  catalogTypes: any[];
  constructor(private router: Router, private route: ActivatedRoute, private translateService: TranslateService) {}

  ngOnInit() {
    this.catalogTypes = [
      { label: this.translateService.instant("administration.catalog.unit-under-test"), value: "UnitUnderTest" },
      { label: this.translateService.instant("administration.catalog.test-equipment"), value: "TestEquipment" },
      { label: this.translateService.instant("administration.catalog.test-sequence"), value: "TestSequence" },
    ];
  }

  onChangeContextType() {
    this.router.navigate([this.selectedContextType], { relativeTo: this.route });
  }
}
