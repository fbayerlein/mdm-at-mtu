import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { OrderListModule } from "primeng/orderlist";
import { TableModule } from "primeng/table";
import { ExtSystemEditorComponent } from "./extsystem-editor.component";
import { ExtSystemRoutingModule } from "./extsystem-routing.module";
import { ExtSystemViewerComponent } from "./extsystem-viewer.component";
import { ExtSystemComponent } from "./extsystem.component";
import { ExtSystemService } from "./extsystem.service";
@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ContextMenuModule,
    ExtSystemRoutingModule,
    OrderListModule,
  ],
  declarations: [ExtSystemComponent, ExtSystemViewerComponent, ExtSystemEditorComponent],
  exports: [],
  providers: [ExtSystemService],
})
export class ExtSystemModule {}
