import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ValueListComponent } from "./value-list.component";

const ValueListRoutes: Routes = [
  {
    path: "",
    component: ValueListComponent,
    children: [],
  },
];
@NgModule({
  imports: [RouterModule.forChild(ValueListRoutes)],
  exports: [RouterModule],
})
export class ValueListRoutingModule {}
