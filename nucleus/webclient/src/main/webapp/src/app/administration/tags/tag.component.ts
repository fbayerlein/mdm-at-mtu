/********************************************************************************
 * Copyright (c) 2025 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MDMTag, MDMTagService } from "src/app/mdmtag/mdmtag.service";

@Component({
  selector: "mdm-tag",
  templateUrl: "./tag.component.html",
})
export class TagComponent implements OnInit {
  // dropdown selection
  selectedEnvironment: string;

  tags: MDMTag[];

  constructor(private mdmTagService: MDMTagService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.loadTags();
  }

  loadTags() {
    this.tags = undefined;
    if (this.selectedEnvironment !== undefined) {
      this.mdmTagService.getAllMDMTags(this.selectedEnvironment).subscribe((data) => {
        console.log(data);
        this.tags = data;
      });
    }
  }
}
