import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { filter } from "rxjs";

@Component({
  selector: "mdm-catalog-manager-viewer",
  templateUrl: "./catalog-manager-viewer.component.html",
  styleUrls: ["./catalog-manager-viewer.component.css"],
})
export class CatalogManagerViewerComponent implements OnInit {
  breadcrumbItems: MenuItem[];
  constructor(private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {}
  ngOnInit() {
    this.breadcrumbItems = [{ label: this.route.snapshot.paramMap.get("selectedEnvironment"), disabled: true }];
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(() => {
      this.updateBreadcrumb();
    });
    this.updateBreadcrumb();
  }

  updateBreadcrumb(): void {
    const currentBreadcrumb: MenuItem[] = [...this.breadcrumbItems];
    let currentRoute = this.route;

    while (currentRoute.firstChild) {
      const childRoute = currentRoute.firstChild;
      if (childRoute.snapshot.data && childRoute.snapshot.data.breadcrumb) {
        const label = this.getLabel(childRoute.snapshot);
        if (label && !currentBreadcrumb.some((item) => item.label === label)) {
          const routerLink = this.getRouterLink(childRoute, currentBreadcrumb.length - 1);
          currentBreadcrumb.push({
            label,
            routerLink,
            command: () => this.onBreadcrumbItemClick(label, currentBreadcrumb.length - 1),
          });
        }
      }
      currentRoute = childRoute;
    }

    // Update the breadcrumb items
    this.breadcrumbItems = currentBreadcrumb;
  }

  private getLabel(snapshot: ActivatedRouteSnapshot): string | null {
    const routeData = snapshot.data;
    const params = snapshot.params;
    if (routeData.breadcrumb) {
      return routeData.breadcrumb.replace(/:(\w+)/g, (match, param) => {
        return params[param] || match;
      });
    }
    return null;
  }
  getRouterLink(route: ActivatedRoute, clickedIndex: number): string[] {
    const urlSegments: string[] = [];
    let currentRoute = route;

    while (currentRoute) {
      const segments = currentRoute.snapshot.url.map((segment) => segment.path);
      urlSegments.unshift(...segments);
      currentRoute = currentRoute.parent;
    }

    // Construct the router link
    return ["/", ...urlSegments];
  }
  onBreadcrumbItemClick(label: string, clickedIndex: number): void {
    // Navigate to the selected label
    const route = this.breadcrumbItems[clickedIndex].routerLink;
    this.router.navigate(route);

    // Update the breadcrumb to show only items until the selected label
    const breadcrumbToShow = this.breadcrumbItems.slice(0, clickedIndex + 1);
    this.breadcrumbItems.splice(0, this.breadcrumbItems.length, ...breadcrumbToShow);
  }
}
