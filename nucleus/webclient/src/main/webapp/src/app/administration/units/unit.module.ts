import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { OrderListModule } from "primeng/orderlist";
import { TableModule } from "primeng/table";
import { PhysDimService } from "./physicaldimension.service";
import { UnitRoutingModule } from "./unit-routing.module";
import { UnitViewerComponent } from "./unit-viewer.component";
import { UnitComponent } from "./unit.component";
import { UnitService } from "./unit.service";
@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ContextMenuModule,
    UnitRoutingModule,
    OrderListModule,
  ],
  declarations: [UnitComponent, UnitViewerComponent],
  exports: [],
  providers: [UnitService, PhysDimService],
})
export class UnitModule {}
