import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Node, NodeArray } from "@navigator/node";
import { map, Observable } from "rxjs";
import { TestStepTemplate } from "../teststep-template/models/teststep-template.model";
import { TestTemplate } from "./models/test-template.model";
import { TestStepTemplateUsage } from "./models/teststep-template-usage.model";

@Injectable()
export class TestTemplateManagerService {
  defaultUrl: string = "/org.eclipse.mdm.nucleus/mdm/environments/";

  constructor(private http: HttpClient) {}
  loadTplTests(source: string): Observable<TestTemplate[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tpltests/")
      .pipe(map((res) => res.data.map((node) => this.convertToTestTemplate(node))));
  }
  convertToTestTemplate(node) {
    return {
      description: node.attributes.find((a) => a.name === "Description").value,
      name: node.name,
      id: node.id,
      type: node.type,
      dateCreated: node.attributes.find((a) => a.name === "DateCreated").value,
      validFlag: node.attributes.find((a) => a.name === "ValidFlag").value,
      version: node.attributes.find((a) => a.name === "Version").value,
      templateTestStepUsage: this.findRelId(node, "TemplateTestStepUsage"),
    } as TestTemplate;
  }
  private findRelId(node: Node, entityType: string): string[] {
    const rel = node.relations.find((r) => r.entityType === entityType);
    return rel !== undefined ? rel.ids : null;
  }
  updateTestTpl(source: string, tplId: string, testTpl: any) {
    return this.http.put(this.defaultUrl + source + "/tpltests/" + tplId, testTpl);
  }
  CreateNewTestTpl(source: string, testTplName: any) {
    return this.http.post(this.defaultUrl + source + "/tpltests/", testTplName);
  }
  deleteTestTpl(source: string, tplId: string) {
    return this.http.delete(this.defaultUrl + source + "/tpltests/" + tplId);
  }
  loadTplTestById(source: string, tplId: string) {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tpltests/" + tplId)
      .pipe(map((res) => res.data.map((node) => this.convertToTestTemplate(node))));
  }
  loadTplTestStep(source: string): Observable<TestStepTemplate[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplteststeps/")
      .pipe(map((res) => res.data.map((node) => this.convertToTestStepTemplate(node))));
  }
  convertToTestStepTemplate(node) {
    return {
      description: node.attributes.find((a) => a.name === "Description").value,
      name: node.name,
      id: node.id,
      type: node.type,
      dateCreated: node.attributes.find((a) => a.name === "DateCreated").value,
      validFlag: node.attributes.find((a) => a.name === "ValidFlag").value,
      version: node.attributes.find((a) => a.name === "Version").value,
    } as TestStepTemplate;
  }
  loadTestStepTplById(source: string, tplId: string) {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplteststeps/" + tplId)
      .pipe(map((res) => res.data.map((node) => this.convertToTestStepTemplate(node))));
  }
  loadTplTestStepUsage(source: string, tplTestId: string): Observable<TestStepTemplateUsage[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tpltests/" + tplTestId + "/tplteststepusages")
      .pipe(map((res) => res?.data.map((node) => this.convertToTestStepTemplateUsage(node))));
  }
  loadTplTestStepUsageById(source: string, tplTestId: string, tplTestStepUsageId: string): Observable<TestStepTemplateUsage[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tpltests/" + tplTestId + "/tplteststepusages/" + tplTestStepUsageId)
      .pipe(map((res) => res.data.map((node) => this.convertToTestStepTemplateUsage(node))));
  }
  convertToTestStepTemplateUsage(node) {
    return {
      id: node.id,
      name: node.name,
      optional: node.attributes.find((a) => a.name === "Optional").value,
      defaultActive: node.attributes.find((a) => a.name === "DefaultActive").value,
      sortIndex: node.attributes.find((a) => a.name === "Sortindex").value,
      testStepRelationId: this.findRelId(node, "TemplateTestStep")[0],
    } as TestStepTemplateUsage;
  }
  createTestStepTemplateUsage(source: string, tplTestId: string, testStepTplUsage: any) {
    return this.http.post(this.defaultUrl + source + "/tpltests/" + tplTestId + "/tplteststepusages", testStepTplUsage);
  }
  deleteTestStepTemplateUsage(source: string, tplTestId: string, testStepTplUsageId: string) {
    return this.http.delete(this.defaultUrl + source + "/tpltests/" + tplTestId + "/tplteststepusages/" + testStepTplUsageId);
  }
  updateTestStepTemplateUsage(source: string, tplTestId: string, testStepTplUsageId: string, testStepTplUsage: any) {
    return this.http.put(
      this.defaultUrl + source + "/tpltests/" + tplTestId + "/tplteststepusages/" + testStepTplUsageId,
      testStepTplUsage,
    );
  }
}
