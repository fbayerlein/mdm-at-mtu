import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { DateTime } from "luxon";
import { MenuItem, TreeNode } from "primeng/api";
import { forkJoin, map, mergeMap, of, tap } from "rxjs";
import { TimezoneService } from "src/app/timezone/timezone-service";
import { TranslationService } from "../../translation/translation.service";
import { TemplateComponent } from "../models/template-component.model";
import { TemplateRoot } from "../models/template-root.model";
import { TemplateManagerService } from "../template-manager.service";
@Component({
  selector: "mdm-template-component",
  templateUrl: "./template-component.component.html",
  styleUrls: ["./template-component.component.css"],
})
export class TemplateComponentComponent implements OnInit {
  environment: string;
  contextType: string;

  isTableLoading = false;
  treeNodes: TreeNode<TemplateRoot | TemplateComponent>[];
  selectedTreeNode: TreeNode<TemplateRoot | TemplateComponent>;

  isShowArchived: boolean;
  contextMenuItems: MenuItem[];

  createTplRootDialog = {
    isVisible: false,
    newTplRootName: undefined as string,
  };

  createTplCompDialog = {
    isVisible: false,
    isChild: false,
    catalogComponents: undefined as Node[],
    selectedCatalogComponent: undefined as Node,
    selectedCatCompName: undefined as string,
  };

  deleteDialog = {
    isVisible: false,
    tplRootDelete: false,
    tplCompDelete: false,
  };

  constructor(
    private templateManagerService: TemplateManagerService,
    private router: Router,
    private route: ActivatedRoute,
    private timezoneService: TimezoneService,
    private translateService: TranslateService,
    private translationService: TranslationService,
  ) {}

  ngOnInit() {
    this.environment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.contextType = this.route.snapshot.paramMap.get("contextType");
  }

  onNodeExpand(event) {
    this.isTableLoading = true;

    const node = event.node;
    if (!node.parent) {
      this.templateManagerService
        .getTemplateComponents(this.environment, this.contextType, node.data.id)
        .pipe(
          tap((templateComponents) => {
            for (const templateComp of templateComponents) {
              this.templateManagerService
                .getCatalogComponentById(
                  this.environment,
                  this.contextType,
                  templateComp.relations.find((a) => a.entityType === "CatalogComponent").ids[0],
                )
                .subscribe((catComp) => {
                  templateComp.catalogComponent = catComp.name;
                });
              if (templateComp.relations.find((a) => a.entityType === "TemplateRoot").ids[0] === node.id) {
                node.tplComps = templateComponents;
              }
            }
          }),
        )
        .subscribe((templateComponents) => {
          node.children = templateComponents?.map((tc) => this.toTreeNodeChildren(tc));
          this.treeNodes = [...this.treeNodes];
          this.isTableLoading = false;
        });
    } else {
      this.templateManagerService
        .getTplComponentChildren(this.environment, this.contextType, node.parent.data.id, node.id)
        .subscribe((templateComponents) => {
          node.children = templateComponents?.map((tc) => this.toTreeNodeChildren(tc));
          this.treeNodes = [...this.treeNodes];
          this.isTableLoading = false;
        });
    }
  }
  selectedNode(selectedTreeNode) {
    this.contextMenuItems = [
      {
        label: this.translateService.instant("administration.templateManager.newCatalogComp"),
        icon: "pi pi-plus",
        command: () => this.showCreateNewCatalogComp(selectedTreeNode.parent),
        disabled:
          selectedTreeNode.data.validFlag === "VALID" ||
          selectedTreeNode.data.validFlag === "ARCHIVED" ||
          selectedTreeNode.parent?.data.validFlag === "ARCHIVED" ||
          selectedTreeNode.parent?.data.validFlag === "VALID",
      },
      {
        label: this.translateService.instant("administration.templateManager.setValid"),
        icon: "pi pi-check-circle",
        command: () => this.setValid(this.selectedTreeNode),
        disabled: selectedTreeNode.data.validFlag !== "EDITABLE",
      },
      {
        label: this.translateService.instant("administration.templateManager.setArchived"),
        icon: "pi pi-download",
        command: () => this.setToArchived(this.selectedTreeNode),
        disabled: selectedTreeNode.data.validFlag !== "VALID",
      },
      {
        label: this.translateService.instant("administration.catalog.showTranslations"),
        icon: "pi pi-language",
        command: () => this.onShowTranslations(),
      },
      {
        label: this.translateService.instant("administration.templateManager.delete"),
        icon: "pi pi-trash",
        command: () => {
          this.deleteDialog.isVisible = true;
          if (this.selectedTreeNode.parent === null) {
            this.deleteDialog.tplRootDelete = true;
          } else {
            this.deleteDialog.tplCompDelete = true;
          }
        },
        disabled:
          selectedTreeNode.data.validFlag === "VALID" ||
          selectedTreeNode.data.validFlag === "ARCHIVED" ||
          selectedTreeNode.parent?.data.validFlag === "ARCHIVED" ||
          selectedTreeNode.parent?.data.validFlag === "VALID",
      },
    ];
  }

  /**
   * load templates from root and convert them to TemplateRoot
   */
  loadTemplateRoots() {
    this.templateManagerService.getTemplateRoots(this.environment, this.contextType).subscribe((templateRoots) => {
      this.treeNodes = templateRoots?.map((tr) => this.toTreeNode(tr));
    });
  }

  toTreeNode(tplRoot: TemplateRoot) {
    return {
      label: tplRoot.name,
      data: {
        id: tplRoot.id,
        name: tplRoot.name,
        description: tplRoot.description,
        dateCreated: this.timezoneService.formatDateTime(DateTime.fromISO(tplRoot.dateCreated)),
        validFlag: tplRoot.validFlag,
        type: tplRoot.type,
        version: tplRoot.version,
      },
      leaf: false,
      selectable: false,
    } as TreeNode;
  }

  toTreeNodeChildren(tplComponent: TemplateComponent) {
    return {
      label: tplComponent.name,
      id: tplComponent.id,
      data: {
        id: tplComponent.id,
        name: tplComponent.name,
        catalogComponent: tplComponent.catalogComponent,
        optional: tplComponent.optional,
        defaultActive: tplComponent.defaultActive,
        testStepSeriesVariable: tplComponent.testStepSeriesVariable,
      },
      relation: tplComponent.relations,
      leaf: false,
    } as TreeNode;
  }

  updateTemplateRoot(tplData) {
    this.templateManagerService
      .updateTemplate(this.environment, this.contextType.toUpperCase(), tplData.data.id, {
        Description: tplData.data.description,
        Name: tplData.data.name,
      })
      .subscribe((response) => {
        this.loadTemplateRoots();
      });
  }

  onNodeSelect(selectedTreeNode) {
    if (selectedTreeNode.node.parent?.parent) {
      this.router.navigate(
        [
          selectedTreeNode.node.parent.label,
          selectedTreeNode.node.label,
          selectedTreeNode.node.parent.parent.data.id,
          selectedTreeNode.node.parent?.id,
          selectedTreeNode.node.id,
        ],
        { relativeTo: this.route },
      );
    } else if (selectedTreeNode.node.parent) {
      this.router.navigate([selectedTreeNode.node.label, selectedTreeNode.node.parent?.data.id, selectedTreeNode.node.id], {
        relativeTo: this.route,
      });
    }
  }

  showArchived(e) {
    if (e.checked === "true") {
      this.isShowArchived = true;
    } else {
      this.isShowArchived = false;
    }
  }
  setToArchived(tplData) {
    this.templateManagerService
      .updateTemplate(this.environment, this.contextType, tplData.data.id, { ValidFlag: "ARCHIVED" })
      .subscribe((response) => {
        this.loadTemplateRoots();
      });
  }

  showCreateTplRootDialog() {
    this.createTplRootDialog.isVisible = true;
  }
  createNewTplRoot() {
    this.templateManagerService
      .createTplRoot(this.environment, this.contextType, {
        Name: this.createTplRootDialog.newTplRootName,
      })
      .subscribe((res) => this.loadTemplateRoots());
    this.createTplRootDialog.isVisible = false;
  }

  showCreateNewCatalogComp(isChild: boolean) {
    this.templateManagerService.getCatalogComponents(this.environment, this.contextType).subscribe((res) => {
      this.createTplCompDialog.catalogComponents = res;
    });
    this.createTplCompDialog.isChild = isChild;
    this.createTplCompDialog.isVisible = true;
  }

  createNewComponent() {
    if (this.createTplCompDialog.isChild) {
      this.templateManagerService
        .createNewComponentForTemplateComp(
          this.environment,
          this.contextType,
          this.selectedTreeNode.parent.data.id,
          this.selectedTreeNode.data.id,
          {
            CatalogComponent: this.createTplCompDialog.selectedCatalogComponent.id,
            Name: this.createTplCompDialog.selectedCatCompName,
            TemplateComponent: this.selectedTreeNode.data.id,
          },
        )
        .subscribe(() => this.loadTemplateRoots());
    } else {
      this.templateManagerService
        .createNewComponentForTemplateRoot(this.environment, this.contextType, this.selectedTreeNode.data.id, {
          CatalogComponent: this.createTplCompDialog.selectedCatalogComponent.id,
          Name: this.createTplCompDialog.selectedCatCompName,
          TemplateRoot: this.selectedTreeNode.data.id,
        })
        .subscribe(() => this.loadTemplateRoots());
    }
    this.createTplCompDialog.selectedCatCompName = "";
    this.createTplCompDialog.selectedCatalogComponent = undefined;
    this.createTplCompDialog.isVisible = false;
  }

  showSelected(e) {
    this.createTplCompDialog.selectedCatalogComponent = e.value;
    this.createTplCompDialog.selectedCatCompName = this.createTplCompDialog.selectedCatalogComponent.name;
  }
  /*
  setSelectedOption(event) {
    this.newComponentName = event.name;
  }
  */

  deleteTplRoot() {
    this.templateManagerService
      .deleteTemplateRoot(this.environment, this.contextType, this.selectedTreeNode.data.id)
      .subscribe((res) => this.loadTemplateRoots());
    this.deleteDialog.tplRootDelete = false;
    this.deleteDialog.isVisible = false;
  }

  deleteTplComp() {
    if (this.selectedTreeNode.parent.parent) {
      this.templateManagerService
        .deleteTplCompChild(
          this.environment,
          this.contextType,
          this.selectedTreeNode.parent.parent.data.id,
          this.selectedTreeNode.parent?.data.id,
          this.selectedTreeNode.data.id,
        )
        .subscribe((res) => this.loadTemplateRoots());
    } else {
      this.templateManagerService
        .deleteTplComp(this.environment, this.contextType, this.selectedTreeNode.parent.data.id, this.selectedTreeNode.data.id)
        .subscribe((res) => this.loadTemplateRoots());
    }

    this.deleteDialog.tplCompDelete = false;
    this.deleteDialog.isVisible = false;
  }
  setValid(tplData) {
    this.templateManagerService
      .updateTemplate(this.environment, this.contextType, tplData.data.id, { ValidFlag: "VALID" })
      .subscribe((response) => {
        this.loadTemplateRoots();
      });
  }

  showTranslations = false;
  translations: {
    languages: string[];
    tplName: string;
    nameTranslations: string[];
    componentNames: {
      catalogName: string;
      componentName: string;
      translations: string[];
    }[];
  };
  onShowTranslations() {
    this.showTranslations = true;
    this.loadTranslations();
  }

  loadTranslations() {
    this.translations = undefined;

    let rootNode = this.selectedTreeNode;
    while (rootNode.parent) {
      rootNode = rootNode.parent;
    }
    if (rootNode.data["type"] !== "TemplateRoot") {
      throw new Error("Expected node type TemplateRoot, but got " + rootNode.data["type"]);
    }

    const tplName = rootNode.data.name;
    this.loadTemplateComponents(rootNode.data.id)
      .pipe(
        map((tcs) =>
          tcs.map((tc) => {
            return { catalogName: tc.catalogComponent, componentName: tc.name };
          }),
        ),
        mergeMap((components) =>
          this.translationService.loadTranslationsForComponentTemplate(this.environment, this.contextType, tplName, components),
        ),
      )
      .subscribe((t) => (this.translations = t));
  }

  loadTemplateComponents(tplRootId: string) {
    return this.templateManagerService.getTemplateComponents(this.environment, this.contextType, tplRootId).pipe(
      mergeMap((tplComps) =>
        forkJoin([
          of(tplComps),
          ...tplComps.map((tplComp) =>
            this.templateManagerService.getTplComponentChildren(this.environment, this.contextType, tplRootId, tplComp.id),
          ),
        ]).pipe(map((t) => t.flat().filter((c) => c !== undefined))),
      ),
      mergeMap((tplComps) => this.loadCatalogNames(this.environment, this.contextType, tplComps)),
    );
  }

  loadCatalogNames(environment: string, contextType: string, templateComponents: TemplateComponent[]) {
    if (templateComponents.length === 0) {
      return of([]);
    } else {
      return forkJoin(
        templateComponents.map((tc) =>
          this.templateManagerService
            .getCatalogComponentById(environment, contextType, tc.relations.find((a) => a.entityType === "CatalogComponent").ids[0])
            .pipe(
              map((catComp) => {
                tc.catalogComponent = catComp.name;
                return tc;
              }),
            ),
        ),
      );
    }
  }

  saveTranslations() {
    const tplName = this.selectedTreeNode.data.name;

    this.translationService
      .saveTranslationsForComponentTemplate(
        this.environment,
        this.contextType,
        tplName,
        this.translations.nameTranslations,
        this.translations.componentNames,
      )
      .subscribe();
    this.showTranslations = false;
  }

  closeTranslationsDialog() {
    this.showTranslations = false;
  }
}
