/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { Node } from "@navigator/node";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { CachableSourceService } from "./cachable-source.service";

export class PhysDimTuple {
  name: string;
  id: string;
}

@Injectable()
export class PhysDimService extends CachableSourceService<PhysDimTuple[]> {
  private prefEndpoint: string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    super();
    this.prefEndpoint = this._prop.getUrl("mdm/environments/");
  }

  protected requestDataForSource(source: string): Observable<PhysDimTuple[]> {
    return this.http.get(this.prefEndpoint + source + "/physicaldimensions").pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request physical dimensions!")),
    );
  }

  public getPhysDimsForSource(source: string): Observable<PhysDimTuple[]> {
    return super.getDataForSource(source);
  }

  private convert(res: any[]): PhysDimTuple[] {
    const physDims = <Node[]>res;
    const retVal = new Array<PhysDimTuple>();
    for (const nd of physDims) {
      retVal.push(this.convertFromNode(nd));
    }
    return retVal;
  }

  private convertFromNode(physDimNd: Node): PhysDimTuple {
    const pd = new PhysDimTuple();

    pd.id = physDimNd.id;
    pd.name = physDimNd.name;

    return pd;
  }
}
