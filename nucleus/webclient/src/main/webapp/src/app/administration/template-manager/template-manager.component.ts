import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { SelectItem } from "primeng/api";

@Component({
  selector: "mdm-template-manager",
  templateUrl: "./template-manager.component.html",
  styleUrls: ["./template-manager.component.css"],
})
export class TemplateManagerComponent implements OnInit {
  selectedContextType: string;
  contextTypes: SelectItem[];

  constructor(private route: ActivatedRoute, private router: Router, private translateService: TranslateService) {}

  ngOnInit() {
    this.contextTypes = [
      { label: this.translateService.instant("administration.templateManager.unit-under-test"), value: "unitundertest" },
      { label: this.translateService.instant("administration.templateManager.test-equipment"), value: "testequipment" },
      { label: this.translateService.instant("administration.templateManager.test-sequence"), value: "testsequence" },
    ];
  }

  onChangeContextType() {
    this.router.navigate([this.selectedContextType], { relativeTo: this.route });
  }
}
