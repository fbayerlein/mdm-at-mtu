import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UnitComponent } from "./unit.component";

const UnitRoutes: Routes = [
  {
    path: "",
    component: UnitComponent,
    children: [],
  },
];
@NgModule({
  imports: [RouterModule.forChild(UnitRoutes)],
  exports: [RouterModule],
})
export class UnitRoutingModule {}
