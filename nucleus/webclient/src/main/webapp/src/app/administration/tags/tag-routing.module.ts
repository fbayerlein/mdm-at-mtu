import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TagComponent } from "./tag.component";

const TagRoutes: Routes = [
  {
    path: "",
    component: TagComponent,
    children: [],
  },
];
@NgModule({
  imports: [RouterModule.forChild(TagRoutes)],
  exports: [RouterModule],
})
export class TagRoutingModule {}
