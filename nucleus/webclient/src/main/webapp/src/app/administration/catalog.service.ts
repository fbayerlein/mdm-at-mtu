/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { Node, NodeArray } from "@navigator/node";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

@Injectable()
export class CatalogService {
  private prefEndpoint: string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.prefEndpoint = _prop.getUrl("mdm/environments/");
  }

  getTplRootsForType(scope: string, type: string): Observable<Node[]> {
    return this.http.get<NodeArray>(this.prefEndpoint + scope + "/tplroots/" + type).pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request catalog element!")),
    );
  }

  getTplCompForRoot(scope: string, type: string, rootId: string): Observable<Node[]> {
    return this.http.get<NodeArray>(this.prefEndpoint + scope + "/tplroots/" + type + "/" + rootId + "/tplcomps").pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request catalog element!")),
    );
  }

  getTplAttrsForComp(scope: string, type: string, rootId: string, tplCompId: string): Observable<Node[]> {
    return this.http
      .get<NodeArray>(this.prefEndpoint + scope + "/tplroots/" + type + "/" + rootId + "/tplcomps/" + tplCompId + "/tplattrs")
      .pipe(
        map((response) => response.data),
        catchError((e) => addErrorDescription(e, "Could not request catalog element!")),
      );
  }

  getCatCompsForType(scope: string, type: string): Observable<Node[]> {
    return this.http.get<NodeArray>(this.prefEndpoint + scope + "/catcomps/" + type).pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request catalog element!")),
    );
  }

  getCatAttrsForComp(scope: string, type: string, catCompId: string): Observable<Node[]> {
    return this.http.get<NodeArray>(this.prefEndpoint + scope + "/catcomps/" + type + "/" + catCompId + "/catattrs").pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request catalog element!")),
    );
  }
}
