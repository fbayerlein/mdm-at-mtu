export class TestStepTemplate {
  id: string;
  name: string;
  version: string;
  type: string;
  description: string;
  validFlag: string;
  dateCreated: string;
  uutTplRootId?: string;
  teTplRootId?: string;
  tsTplRootId?: string;
}
