import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { DateTime } from "luxon";
import { TimezoneService } from "src/app/timezone/timezone-service";
import { TemplateRoot } from "../../template-manager/models/template-root.model";
import { TestStepTemplate } from "../models/teststep-template.model";
import { TestStepTemplateManagerService } from "../teststep-template.service";
@Component({
  selector: "mdm-teststep-template-details",
  templateUrl: "./teststep-template-details.component.html",
  styleUrls: ["./teststep-template-details.component.css"],
})
export class TeststepTemplateDetailsComponent implements OnInit {
  testStepTpls: TestStepTemplate[];
  selectedTestStepTpl: TestStepTemplate;
  currentlyEditedTpl: any;
  selectedTemplateId: string;
  selectedEnvironment: string;
  uutTestTpls: TemplateRoot[];
  teTestTpls: TemplateRoot[];
  tsTestTpls: TemplateRoot[];
  selectedUutTestTpl: TemplateRoot;
  selectedTsTestTpl: TemplateRoot;
  selectedTeTestTpl: TemplateRoot;
  selectedUutTestTplName: string;
  selectedTsTestTplName: string;
  selectedTeTestTplName: string;
  selectedTemplateName: string;
  constructor(
    private teststepTplService: TestStepTemplateManagerService,
    private route: ActivatedRoute,
    private timezoneService: TimezoneService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.selectedTemplateId = this.route.snapshot.paramMap.get("selectedTestStepTplId");
    this.selectedTemplateName = this.route.snapshot.paramMap.get("selectedTestStepTplName");
    this.getTestStepTplById();
    this.getTemplateRoots();
  }

  getTestStepTplById() {
    this.teststepTplService.getTplStepTestById(this.selectedEnvironment, this.selectedTemplateId).subscribe((res) => {
      this.testStepTpls = res;
      this.testStepTpls.map((res) => (res.dateCreated = this.timezoneService.formatDateTime(DateTime.fromISO(res.dateCreated))));
    });
  }
  onEditInit(event) {
    this.currentlyEditedTpl = { ...event.data };
  }

  updateTestStepTpl(tpl) {
    this.teststepTplService
      .updateTestStepTpl(this.selectedEnvironment, this.selectedTemplateId, {
        Name: tpl.name,
        Description: tpl.description,
        TemplateRootUnitUnderTest: tpl.uutTplRootId,
        TemplateRootTestSequence: tpl.tsTplRootId,
        TemplateRootTestEquipment: tpl.teTplRootId,
      })
      .subscribe((response) => {
        this.getTestStepTplById();
      });
  }
  onEditComplete(event) {
    if (event.data !== this.currentlyEditedTpl) {
      this.updateTestStepTpl(event.data);
    }
  }
  getTemplateRoots() {
    const contextTypes = ["UnitUnderTest", "TestEquipment", "TestSequence"];
    for (let contextType of contextTypes) {
      this.teststepTplService.getTemplateRoots(this.selectedEnvironment, contextType).subscribe((res) => {
        switch (contextType) {
          case "UnitUnderTest":
            this.uutTestTpls = res;
            break;
          case "TestEquipment":
            this.teTestTpls = res;
            break;
          case "TestSequence":
            this.tsTestTpls = res;
            break;
          default:
            break;
        }
      });
    }
  }

  deleteTsTpl(tpl) {
    this.teststepTplService
      .updateTestStepTpl(this.selectedEnvironment, this.selectedTemplateId, {
        TemplateRootTestSequence: null,
      })
      .subscribe((response) => {
        this.getTestStepTplById();
      });
  }
  deleteUutTpl(tpl) {
    this.teststepTplService
      .updateTestStepTpl(this.selectedEnvironment, this.selectedTemplateId, {
        TemplateRootUnitUnderTest: null,
      })
      .subscribe((response) => {
        this.getTestStepTplById();
      });
  }
  deleteTeTpl(tpl) {
    this.teststepTplService
      .updateTestStepTpl(this.selectedEnvironment, this.selectedTemplateId, {
        TemplateRootTestEquipment: null,
      })
      .subscribe((response) => {
        this.getTestStepTplById();
      });
  }
}
