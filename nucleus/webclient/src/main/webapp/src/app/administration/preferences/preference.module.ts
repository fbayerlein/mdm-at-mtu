/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { TabMenuModule } from "primeng/tabmenu";
import { MDMCoreModule } from "../../core/mdm-core.module";
import { PreferenceService } from "../../core/preference.service";
import { EditPreferenceComponent } from "./edit-preference.component";
import { PreferenceModuleComponent } from "./preference-module.component";
import { PreferenceRoutingModule } from "./preference-routing.module";
import { PreferenceComponent } from "./preference.component";

@NgModule({
  imports: [MDMCoreModule, FormsModule, ReactiveFormsModule, PreferenceRoutingModule, TabMenuModule, ConfirmDialogModule],
  declarations: [EditPreferenceComponent, PreferenceModuleComponent, PreferenceComponent],
  exports: [],
  providers: [PreferenceService],
})
export class PreferenceModule {}
