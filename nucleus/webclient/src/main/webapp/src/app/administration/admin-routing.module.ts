/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { functionalRightGuard } from "@core/authentication/authentication.service";
import { AdminModulesComponent } from "./admin-modules.component";
import { AnnouncementConfigComponent } from "./announcements/announcement-config.component";
import { RightsConfigComponent } from "./rights-config/rights-config.component";


const moduleRoutes: Routes = [
  {
    path: "",
    component: AdminModulesComponent,
    children: [
      {
        path: "preferences",
        loadChildren: () => import("./preferences/preference.module").then((mod) => mod.PreferenceModule),
        canActivate: [functionalRightGuard("administration.preferences.show")],
      },
      {
        path: "rights-config",
        component: RightsConfigComponent,
        canActivate: [functionalRightGuard("administration.rights-config.show3")],
      },
      {
        path: "announcements",
        component: AnnouncementConfigComponent,
        canActivate: [functionalRightGuard("administration.announcements.show")],
      },
      {
        path: ":selectedEnvironment",
        children: [
          {
            path: "catalog",
            loadChildren: () => import("./catalog-manager/catalog.module").then((mod) => mod.CatalogModule),
            canActivate: [functionalRightGuard("administration.catalogmanager.show")],
          },
          {
            path: "template",
            children: [
              {
                path: "component",
                loadChildren: () => import("./template-manager/template.module").then((mod) => mod.TemplateModule),
                canActivate: [functionalRightGuard("administration.templatemanager.show")],
              },
              {
                path: "teststep",
                loadChildren: () => import("./teststep-template/teststep-template.module").then((mod) => mod.TestStepTemplateModule),
                canActivate: [functionalRightGuard("administration.templatemanager.show")],
              },
              {
                path: "test",
                loadChildren: () => import("./test-template/test-template.module").then((mod) => mod.TestTemplateModule),
                canActivate: [functionalRightGuard("administration.templatemanager.show")],
              },
            ],
          },
          {
            path: "extsystems",
            loadChildren: () => import("./extsystem/extsystem.module").then((mod) => mod.ExtSystemModule),
            canActivate: [functionalRightGuard("administration.external-systems.show")],
          },
          {
            path: "units",
            loadChildren: () => import("./units/unit.module").then((mod) => mod.UnitModule),
            canActivate: [functionalRightGuard("administration.units.show")],
          },
          {
            path: "quantities",
            loadChildren: () => import("./quantities/quantity.module").then((mod) => mod.QuantityModule),
            canActivate: [functionalRightGuard("administration.quantities.show")],
          },
          {
            path: "tags",
            loadChildren: () => import("./tags/tag.module").then((mod) => mod.TagModule),
            canActivate: [functionalRightGuard("administration.tags.show")],
          },
          {
            path: "value-list",
            loadChildren: () => import("./value-list/value-list.module").then((mod) => mod.ValueListModule),
            canActivate: [functionalRightGuard("administration.value-list.show")],
          },
        ],
      },
      { path: "", redirectTo: "", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
