/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { MDMNotificationService } from "../../core/mdm-notification.service";
import { Preference, PreferenceService, Scope } from "../../core/preference.service";
import { Node } from "../../navigator/node";
import { NodeService } from "../../navigator/node.service";

@Component({
  selector: "mdm-edit-preference",
  templateUrl: "./edit-preference.component.html",
  styleUrls: ["./edit-preference.component.css"],
})
export class EditPreferenceComponent implements OnInit {
  @Input() scope: string;
  @Output() saved = new EventEmitter<Preference>();

  showSource: boolean;
  showUser: boolean;
  isKeyEmpty: boolean;
  isUserEmpty: boolean;
  preferenceForm: FormGroup;
  needSave = false;
  envs: Node[];

  public isDialogShown = false;

  constructor(
    private formBuilder: FormBuilder,
    private nodeService: NodeService,
    private preferenceService: PreferenceService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    let node: Node;
    this.nodeService.getNodes(node).subscribe(
      (env) => (this.envs = env),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("administration.edit-preference.err-cannot-load-data-source"),
          error,
        ),
    );
    this.setupForm(new Preference());
  }

  setupForm(preference: Preference) {
    this.setOptions(preference);
    this.preferenceForm = this.formBuilder.group({
      scope: [preference.scope],
      source: [preference.source],
      user: [preference.user],
      key: [preference.key, Validators.required],
      value: [preference.value, Validators.required],
      id: [preference.id],
    });
  }

  setOptions(preference: Preference) {
    this.needSave = false;
    this.isKeyEmpty = preference.key === "";
    switch (this.scope) {
      case Scope.SYSTEM:
        this.showSource = false;
        this.showUser = false;
        break;
      case Scope.SOURCE:
        this.showSource = true;
        this.showUser = false;
        break;
      case Scope.USER:
        this.showSource = false;
        this.showUser = true;
        break;
    }
  }

  showDialog(preference?: Preference) {
    if (preference === null || preference === undefined) {
      preference = new Preference();
      preference.scope = this.scope;
      if (this.scope === Scope.SOURCE) {
        preference.source = this.envs[0].sourceName;
      }
    }
    this.setupForm(preference);
    this.isDialogShown = true;
  }

  onSave() {
    this.needSave = true;
    this.isDialogShown = false;
    const preference = this.preferenceForm.value;

    this.preferenceService.savePreference(preference).subscribe({
      next: (p) => this.saved.emit(p),
      error: (error) =>
        this.notificationService.notifyError(this.translateService.instant("administration.preference.err-cannot-save-preference"), error),
    });
  }

  closeDialog() {
    this.isDialogShown = false;
  }
}
