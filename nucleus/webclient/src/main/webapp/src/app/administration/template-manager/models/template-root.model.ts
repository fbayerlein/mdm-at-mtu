import { TemplateComponent } from "./template-component.model";
export class TemplateRoot {
  id: string;
  name: string;
  version: string;
  type: string;
  description: string;
  validFlag: string;
  dateCreated: string;
  tplComps: TemplateComponent[];
}
