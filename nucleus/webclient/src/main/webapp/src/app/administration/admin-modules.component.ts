/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Profile, Role } from "@authentication/model";
import { AuthenticationService } from "@core/authentication/authentication.service";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { NodeproviderService } from "@navigator/nodeprovider.service";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { concatMap, map, mergeMap, startWith, Subscription, tap } from "rxjs";

@Component({
  selector: "mdm-admin-modules",
  templateUrl: "admin-modules.component.html",
  styleUrls: ["admin-modules.component.css"],
  providers: [],
})
export class AdminModulesComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  public links: MenuItem[] = [];
  public items: MenuItem[] = [];
  public environments: string[];
  public selectedEnvironment: string;
  private nodeServiceSubscription: Subscription;
  private lastUrl = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private authenticationService: AuthenticationService,
    private nodeproviderService: NodeproviderService,
    private nodeService: NodeService,
    private notificationService: MDMNotificationService,
  ) {}

  ngOnInit(): void {
    this.onDatasourceChanged();
    this.route.firstChild.paramMap.subscribe((params) => {
      const env = params.get("selectedEnvironment");
      this.selectedEnvironment = env === null ? undefined : env;
    });
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe(
      () => this.onDatasourceChanged(),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("navigator.mdm-navigator.err-cannot-update-navigation-tree"),
          error,
        ),
    );

    this.refreshMenuItems();
  }

  onDatasourceChanged() {
    this.nodeService
      .getRootNodes()
      .pipe(mergeMap((envs) => this.getPermittedEnvironments(envs)))
      .subscribe({
        next: (envs) => {
          this.environments = envs;
          if (this.selectedEnvironment === undefined && envs.length === 1) {
            this.selectedEnvironment = envs[0];
          }
        },
        error: (error) =>
          this.translateService
            .instant("basket.mdm-basket.err-cannot-load-sources")
            .subscribe((msg) => this.notificationService.notifyError(msg, error)),
      });
  }

  getPermittedEnvironments(envs: Node[]) {
    return this.authenticationService.getCurrentProfile().pipe(
      map((profile) => {
        if (profile.applicationUserDetails.roles.some((r) => Role.Admin === r)) {
          return envs.map((n) => n.sourceName);
        }

        return envs
          .filter((e) => {
            const specAdmRoleName = Role.SpecialistAdmin + "_" + e.sourceName;
            return this.profileHasAppRole(profile, specAdmRoleName) || this.profileHasDatasourceRole(profile, e.sourceName);
          })
          .map((n) => n.sourceName);
      }),
    );
  }

  private profileHasAppRole(profile: Profile, roleName: string) {
    for (const role of profile.applicationUserDetails.roles) {
      if (roleName === role) {
        return true;
      }
    }
    return false;
  }

  private profileHasDatasourceRole(profile: Profile, sourceName: string) {
    const dataSourceDetails = profile.dataSourceUserDetails[sourceName];
    if (dataSourceDetails) {
      for (const role of dataSourceDetails.roles) {
        if (Role.SpecialistAdmin === role) {
          return true;
        }
      }
    }
    return false;
  }
  refreshMenuItems() {
    const rightKeys = [
      { key: "administration.preferences.show" },
      { key: "administration.functional-rights.show" },
      { key: "administration.external-systems.show" },
      { key: "administration.units.show" },
      { key: "administration.quantities.show" },
      { key: "administration.tags.show" },
      { key: "administration.value-list.show" },
      { key: "administration.catalogmanager.show" },
      { key: "administration.templatemanager.show" },
      { key: "administration.announcements.show" },
    ];
    this.sub = this.authenticationService
      .hasFunctionalRights(rightKeys)
      .pipe(
        concatMap((rights) =>
          this.translateService.onLangChange.pipe(
            startWith(""),
            tap(() => {
              this.setLinks(rights);
              this.setItems(rights);
              this.refreshNavigation();
            }),
          ),
        ),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    if (this.nodeServiceSubscription) {
      this.nodeServiceSubscription.unsubscribe;
    }
    this.sub.unsubscribe();
  }

  private setLinks(rights: Map<string, boolean>) {
    this.links = [
      {
        label: this.translateService.instant("administration.admin-modules.preferences"),
        routerLink: "preferences",
        visible: rights.get("administration.preferences.show"),
      },
      {
        label: this.translateService.instant("administration.admin-modules.rights-config"),
        routerLink: "rights-config",
        visible: rights.get("administration.functional-rights.show"),
      },
      {
        label: this.translateService.instant("administration.admin-modules.announcements"),
        routerLink: "announcements",
        visible: rights.get("administration.announcements.show"),
      },
    ];
  }
  private setItems(rights: Map<string, boolean>) {
    this.items = [
      {
        label: this.translateService.instant("administration.admin-modules.catalog"),
        visible: rights.get("administration.catalogmanager.show"),
        command: () => this.navigate(["catalog"]),
      },
      {
        label: this.translateService.instant("administration.admin-modules.template"),
        visible: rights.get("administration.templatemanager.show"),
        command: () => this.navigate(["template", "component"]),
      },
      {
        label: this.translateService.instant("administration.admin-modules.testStepTpl"),
        visible: rights.get("administration.templatemanager.show"),
        command: () => this.navigate(["template", "teststep"]),
      },
      {
        label: this.translateService.instant("administration.admin-modules.testTpl"),
        visible: rights.get("administration.templatemanager.show"),
        command: () => this.navigate(["template", "test"]),
      },
      {
        disabled: true,
        target: "separator",
      },
      {
        label: this.translateService.instant("administration.admin-modules.units"),
        visible: rights.get("administration.units.show"),
        command: () => this.navigate(["units"]),
      },
      {
        label: this.translateService.instant("administration.admin-modules.quantities"),
        visible: rights.get("administration.quantities.show"),
        command: () => this.navigate(["quantities"]),
      },
      {
        label: this.translateService.instant("administration.admin-modules.tags"),
        visible: rights.get("administration.tags.show"),
        command: () => this.navigate(["tags"]),
      },
      {
        label: this.translateService.instant("administration.admin-modules.value-list"),
        visible: rights.get("administration.value-list.show"),
        command: () => this.navigate(["value-list"]),
      },
      {
        disabled: true,
        target: "separator",
      },
      {
        label: this.translateService.instant("administration.admin-modules.extsystems"),
        visible: rights.get("administration.external-systems.show"),
        command: () => this.navigate(["extsystems"]),
      },
    ];
  }

  navigate(url: string[]) {
    if (this.selectedEnvironment) {
      this.router.navigate(["administration", this.selectedEnvironment, ...url]);
    }
  }

  refreshNavigation() {
    if (this.selectedEnvironment) {
      this.router.navigate(["administration", this.selectedEnvironment]);
    }
  }

  onChangeEnviroment() {
    this.refreshMenuItems();
  }
}
