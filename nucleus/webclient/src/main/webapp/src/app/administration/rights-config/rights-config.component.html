<!--********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************-->
<div class="rights-config-wrapper">
  <div style="height: 100%; overflow: hidden">
    <p-treeTable
      [value]="treeData"
      styleClass="p-treetable-sm p-treetable-striped p-treetable-gridlines"
      [scrollable]="true"
      scrollHeight="flex"
    >
      <ng-template pTemplate="caption">
        <div class="flex align-items-center justify-content-between">
          <span style="align-items: center; display: flex">
            <p-checkbox [(ngModel)]="config.default" [binary]="true" inputId="binary"></p-checkbox>
            <label for="binary">{{ "administration.rights-config.default-condition-result" | translate }}</label>
          </span>
          <p-button (onClick)="save()">{{ "administration.rights-config.btn-save" | translate }}</p-button>
        </div>
      </ng-template>
      <ng-template pTemplate="header">
        <tr>
          <th>{{ "administration.rights-config.key" | translate }}</th>
          <th>{{ "administration.rights-config.condition" | translate }}</th>
          <th>{{ "administration.rights-config.description" | translate }}</th>
        </tr>
      </ng-template>
      <ng-template pTemplate="body" let-rowNode let-rowData="rowData">
        <tr>
          <td>
            <p-treeTableToggler [rowNode]="rowNode"></p-treeTableToggler>
            <span *ngIf="rowNode.node.type === 'functionalRight'" [title]="rowNode.node?.id">{{ rowNode.node?.label }}</span>
            <span *ngIf="rowNode.node.type === 'keyFragment'" [title]="rowNode.node?.id">{{ rowNode.node?.label }}</span>
          </td>
          <td>
            <div class="field">
              <input
                [id]="rowNode.node.id"
                type="text"
                pInputText
                [ngModel]="rowData.condition"
                (ngModelChange)="setCondition($event, rowData)"
                *ngIf="rowNode.node.type === 'functionalRight'"
                style="width: 100%"
                [class.ng-invalid]="rowData.invalid"
              />
              <small [id]="rowNode.node.id + '-help'" *ngIf="rowData.invalid" class="p-error block">{{ rowData.errorMessage }}</small>
            </div>
          </td>
          <td>{{ rowData?.description }}</td>
        </tr>
      </ng-template>
    </p-treeTable>
  </div>

  <p-panel header="Help on condition syntax" [toggleable]="true" [collapsed]="true" toggler="header">
    Conditions may include data references provided as:

    <ul>
      <li>string literals: <code>'a'</code> (defines the string <code>a</code>; string literals are case sensitive).</li>
      <li>
        string literal arrays: <code>('a', 'b', 'c')</code> (defines an array containing the strings <code>a</code>, <code>c</code> and
        <code>c</code>). However, string literal lists must not be nested.
      </li>
      <li>
        dynamic context data: <code>{{ "${test}" }}</code> (evaluates to the value of the variable with the name <code>test</code>).
      </li>
      <li>
        application scoped user data: <code>username</code> (evaluates to the value of the property with the name <code>username</code> of
        the user details provided on web server). Nested properties are supported by chaining field names separated by <code>.</code>.
      </li>
      <li>
        data source scoped user data: <code>sourceName.username</code> (evaluates to the value of the property with the name
        <code>username</code> of the user details provided in data source with name <code>sourceName</code>). Nested properties are
        supported by chaining field names separated by <code>.</code>.
      </li>
    </ul>

    <p>
      Condition expression syntax is developed to handle variables holding values of types string and string[] only. However, referencing
      other data will not result in an error but may cause unexpected outcome.
    </p>
    Conditions may include operators for strings and string arrays, resp.:

    <ul>
      <li>
        equals: <code>==</code> (compares two strings for equality, eg <code>{{ "${a}" }}=='a'</code>. Should a variable refer to an string
        array only the array references is respected.)
      </li>
      <li>
        not equals: <code>!=</code> (compares two strings for inequality, eg <code>{{ "${a}" }}!='b'</code> Should a variable refer to an
        string array only the array references is respected.)
      </li>
      <li>
        includes: <code>IN</code> (is case sensitive; checks if left side is included in right side, eg
        <code>{{ "${a}" }} IN ('a', 'b')</code>. Should either side not be an array it will be wrapped in one.)
      </li>
    </ul>

    Conditions may include logical operators for strings:

    <ul>
      <li>
        and: <code>&&</code> (connects two boolean expressions with a logical and, eg. <code>{{ "${a}" }}=='a' && {{ "${b}" }}!='c'</code>).
      </li>
      <li>
        or: <code>||</code> (connects two boolean expressions with a logical and, eg. <code>{{ "${a}" }}=='a' || {{ "${b}" }}!='c'</code>).
      </li>
      <li>
        negation: <code>!</code> (negates the following boolean expression , eg <code>!{{ "${a}" }}=='b'</code> is equal to
        <code>{{ "${a}" }}!='b'</code>).
      </li>
    </ul>

    Conditions may include parenthesis and blanks:

    <ul>
      <li>parenthesis are a part of the string literal arrays syntax.</li>
      <li>
        parenthesis may be used to override operator precedence ( eg. negates
        <code>!({{ "${a}" }}=='a' && {{ "${b}" }} IN ('a', 'b'))</code>).
      </li>
      <li>
        parenthesis may be used around boolean expressions for better readability ( eg. negates <code>!({{ "${a}" }}=='a')</code> is same as
        <code>!{{ "${a}" }}=='a'</code>).
      </li>
      <li>
        parenthesis must not be used around data expressions ( eg. negates <code>({{ "${a}" }})=='a'</code> and
        <code>'a' IN (('a', 'b'))</code> are invalid).
      </li>
      <li>blanks must not be used within operators (eg. <code>= =</code> and <code>I N</code> are invalid).</li>
      <li>
        blanks must not be used in opening tag of context data reference (eg. <code>{{ "$ {a}" }}</code> is invalid).
      </li>
      <li>
        blanks may be used with in parenthesis and curly brackets (eg. <code>{{ "${ a }" }}</code> and <code>( 'a'!='b' )</code> are valid).
      </li>
      <li>
        blanks may be used between any kind of expression or operator (eg: <code>'a' != 'b'</code> or <code>'a' IN ('a', 'b')</code> is
        valid).
      </li>
      <li>blanks in string literals are considered to be part of the string ('a' is not equal to 'a ').</li>
    </ul>

    <h3>Examples</h3>

    <ul>
      <li>
        If the user has the one of the ODS roles 'Admin' or 'MDMSystemAdministrator' and is part of the specified department (which in this
        case is a parameter provided by the specific functional right):
        <code>MDMNVH.roles IN ('Admin', 'MDMSystemAdministrator') && MDMNVH.department=={{ "${department}" }}</code>
      </li>

      <li>
        If the openMDM user has the role 'Admin' in openMDM:
        <code> "'Admin' IN roles " </code>
      </li>

      <li>
        If the ODS user is not in the specified department or if its openMDM username is 'John Doe':

        <code> "{{ "${department}" }} != MDMNVH.department || username == 'John Doe'" </code>
      </li>
    </ul>
  </p-panel>
</div>
