/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Node, NodeArray } from "@navigator/node";
import { map, Observable } from "rxjs";
import { ValueList } from "../value-list/model";
import { CatalogAttribute } from "./models/catalog-attribute.model";
import { CatalogComponent } from "./models/catalog-component.model";

@Injectable()
export class CatalogManagerService {
  defaultUrl: string = "/org.eclipse.mdm.nucleus/mdm/environments/";

  constructor(private http: HttpClient) {}

  loadCatalogComponent(source: string, contextType: string, catalogComponentId: string) {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/catcomps/" + contextType + "/" + catalogComponentId)
      .pipe(map((res) => (res.data.length == 1 ? this.convertToCatalogComponent(res.data[0]) : undefined)));
  }

  loadCatalogComponents(source: string, contextType: string) {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/catcomps/" + contextType)
      .pipe(map((res) => res.data.map((node) => this.convertToCatalogComponent(node))));
  }
  convertToCatalogComponent(catComponent: Node) {
    return {
      description: catComponent.attributes.find((a) => a.name === "Description").value,
      name: catComponent.name,
      id: catComponent.id,
      sourceType: catComponent.sourceType,
      attributes: catComponent.attributes,
      relations: catComponent.relations,
      sourceName: catComponent.sourceName,
      type: catComponent.type,
      source: catComponent.source,
    } as CatalogComponent;
  }
  loadCatalogAttributes(source: string, contextType: string, catCompId: string) {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/catcomps/" + contextType + "/" + catCompId + "/catattrs")
      .pipe(map((res) => res.data?.map((node) => this.convertToCatalogAttribute(node))));
  }

  convertToCatalogAttribute(catAttr: Node) {
    return {
      id: catAttr.id,
      name: catAttr.name,
      sortIndex: catAttr.attributes.find((a) => a.name === "Sortindex").value,
      unit: catAttr.attributes.find((a) => a.name === "Unit").value,
      dataType: catAttr.attributes.find((a) => a.name === "DataType").value,
      description: catAttr.attributes.find((a) => a.name === "Description").value,
      valueCopyable: catAttr.attributes.find((a) => a.name === "ValueCopyable").value,
      valueListRef: catAttr.attributes.find((a) => a.name === "ValueListRef").value,
      actionRequestClassname: catAttr.attributes.find((a) => a.name === "ActionRequestClassname").value,
      actionRequestParameter: catAttr.attributes.find((a) => a.name === "ActionRequestParameter").value,
      mimeType: catAttr.attributes.find((a) => a.name === "MimeType").value,
      valueListRelationId: this.findRelId(catAttr),
    } as CatalogAttribute;
  }
  private findRelId(node: Node): string {
    const rel = node.relations.find((r) => r.entityType === "ValueList");
    return rel !== undefined ? rel.ids[0] : null;
  }
  updateCatalogComponent(source: string, contextType: string, catCompId: string, catalog: any) {
    return this.http.put(this.defaultUrl + source + "/catcomps/" + contextType + "/" + catCompId, catalog);
  }
  updateCatalogAttributes(source: string, contextType: string, catCompId: string, attrId: string, catalogAttr: any) {
    return this.http.put(this.defaultUrl + source + "/catcomps/" + contextType + "/" + catCompId + "/catattrs/" + attrId, catalogAttr);
  }
  deleteCatalogAttribute(source: string, contextType: string, catCompId: string, attrId: string) {
    return this.http.delete(this.defaultUrl + source + "/catcomps/" + contextType + "/" + catCompId + "/catattrs/" + attrId);
  }
  createCatalogAttribute(source: string, contextType: string, catCompId: string, catalog: any) {
    return this.http.post(this.defaultUrl + source + "/catcomps/" + contextType + "/" + catCompId + "/catattrs", catalog);
  }
  createCatalogComponent(source: string, contextType: string, catalog: any) {
    return this.http.post(this.defaultUrl + source + "/catcomps/" + contextType, catalog);
  }
  getValueList(source: string): Observable<ValueList[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/valuelists ")
      .pipe(map((res) => res.data.map((node) => this.convertToValueList(node))));
  }
  convertToValueList(catValueList: Node) {
    return {
      name: catValueList.name,
      id: catValueList.id,
      sourceName: catValueList.sourceName,
      sourceType: catValueList.sourceType,
    } as ValueList;
  }
}
