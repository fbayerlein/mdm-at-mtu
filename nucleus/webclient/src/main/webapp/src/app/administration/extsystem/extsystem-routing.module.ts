import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ExtSystemComponent } from "./extsystem.component";

const ExtSystemRoutes: Routes = [
  {
    path: "",
    component: ExtSystemComponent,
    children: [],
  },
];
@NgModule({
  imports: [RouterModule.forChild(ExtSystemRoutes)],
  exports: [RouterModule],
})
export class ExtSystemRoutingModule {}
