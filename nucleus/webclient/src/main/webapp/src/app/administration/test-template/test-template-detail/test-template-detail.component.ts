import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DateTime } from "luxon";
import { forkJoin, Observable } from "rxjs";
import { TimezoneService } from "src/app/timezone/timezone-service";
import { TestStepTemplate } from "../../teststep-template/models/teststep-template.model";
import { TestTemplate } from "../models/test-template.model";
import { TestStepTemplateUsage } from "../models/teststep-template-usage.model";
import { TestTemplateManagerService } from "../test-template.service";
@Component({
  selector: "app-test-template-detail",
  templateUrl: "./test-template-detail.component.html",
  styleUrls: ["./test-template-detail.component.css"],
})
export class TestTemplateDetailComponent implements OnInit {
  testTpls: TestTemplate[];
  selectedTestTpl: TestTemplate;
  testStepTpls: TestStepTemplate[];
  selectedTestStepTpl: TestStepTemplate;
  currentlyEditedTpl: any;
  selectedTemplateId: string;
  selectedEnvironment: string;
  selectedTemplateName: string;
  selectedTestStepTplUsage: TestStepTemplateUsage;
  testStepTemplateUsage: TestStepTemplateUsage[];
  showCreate: boolean;
  showDeleteDialog: boolean;
  constructor(
    private testTplService: TestTemplateManagerService,
    private route: ActivatedRoute,
    private timezoneService: TimezoneService,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.selectedTemplateId = this.route.snapshot.paramMap.get("selectedTestTplId");
    this.selectedTemplateName = this.route.snapshot.paramMap.get("selectedTestTplName");
    this.loadTestTplById();
    this.loadTestStepTpls();
  }

  loadTestTplById() {
    this.testTplService.loadTplTestById(this.selectedEnvironment, this.selectedTemplateId).subscribe((res) => {
      this.testTpls = res;
      this.testTpls.map((res) => (res.dateCreated = this.timezoneService.formatDateTime(DateTime.fromISO(res.dateCreated))));
    });
  }
  loadTestStepTpls() {
    this.testTplService.loadTplTestStep(this.selectedEnvironment).subscribe((res) => {
      this.testStepTpls = res;
      this.loadTplTestStepUsage();
    });
  }
  loadTplTestStepUsage() {
    const updateRequests: Observable<any>[] = [];
    this.testTplService.loadTplTestStepUsage(this.selectedEnvironment, this.selectedTemplateId).subscribe((res) => {
      this.testStepTemplateUsage = res;
      this.testStepTemplateUsage?.forEach((data, index) => {
        const matchingTplIds = this.testStepTpls.find((tpl) => tpl.id === data.testStepRelationId);
        if (matchingTplIds) {
          data.name = matchingTplIds.name;
        }
        const id = data.id;
        data.sortIndex = String(index);
        const updateRequest = this.testTplService.updateTestStepTemplateUsage(this.selectedEnvironment, this.selectedTemplateId, id, {
          data,
        });
        updateRequests.push(updateRequest);
        return forkJoin(updateRequests);
      });
    });
  }
  createTestStepTplUsage(selectedTestStepTpl) {
    this.testTplService
      .createTestStepTemplateUsage(this.selectedEnvironment, this.selectedTemplateId, {
        TemplateTestStep: selectedTestStepTpl.id,
        TemplateTest: this.selectedTemplateId,
      })
      .subscribe((r) => this.loadTplTestStepUsage());
    this.selectedTestStepTpl = null;
    this.showCreate = false;
  }
  openCreateDialog() {
    this.showCreate = true;
  }

  onEditInit(event) {
    this.currentlyEditedTpl = { ...event.data };
  }
  updateTestTpl(tpl) {
    this.testTplService
      .updateTestTpl(this.selectedEnvironment, this.selectedTemplateId, {
        Name: tpl.name,
        Description: tpl.description,
      })
      .subscribe((response) => {
        this.loadTestTplById();
      });
  }
  onEditComplete(event) {
    if (event.data !== this.currentlyEditedTpl) {
      this.updateTestTpl(event.data);
    }
  }

  updateTestStepTpl(tpl) {
    this.testTplService
      .updateTestStepTemplateUsage(this.selectedEnvironment, this.selectedTemplateId, tpl.id, {
        Optional: tpl.optional,
        DefaultActive: tpl.defaultActive,
      })
      .subscribe((response) => {
        this.loadTestTplById();
      });
  }
  openDeleteDialog(tpl) {
    this.showDeleteDialog = true;
    this.selectedTestStepTplUsage = tpl;
  }
  deleteTestStepTplUsage() {
    this.testTplService
      .deleteTestStepTemplateUsage(this.selectedEnvironment, this.selectedTemplateId, this.selectedTestStepTplUsage.id)
      .subscribe((res) => this.loadTplTestStepUsage());
    this.showDeleteDialog = false;
  }
  onRowReorder(event) {
    const sortedTestStepTemplateUsages = [...this.testStepTemplateUsage];
    sortedTestStepTemplateUsages.forEach((element, index) => {
      element.sortIndex = index.toString();
      this.testTplService
        .updateTestStepTemplateUsage(this.selectedEnvironment, this.selectedTemplateId, element.id, {
          Sortindex: element.sortIndex,
        })
        .subscribe((response) => {
          console.log(this.testStepTemplateUsage);
        });
    });
  }
}
