import { Attribute, Relation } from "@navigator/node";

export class CatalogComponent {
  description: string;
  name: string;
  id: string;
  sourceType: string;
  attributes: Attribute[];
  relations: Relation[];
  sourceName: string;
  type: string;
  source: string;
}
