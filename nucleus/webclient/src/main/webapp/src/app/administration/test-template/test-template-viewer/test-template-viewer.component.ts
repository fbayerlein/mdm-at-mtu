import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { filter } from "rxjs";

@Component({
  selector: "mdm-test-template-viewer",
  templateUrl: "./test-template-viewer.component.html",
  styleUrls: ["./test-template-viewer.component.css"],
})
export class TestTemplateViewerComponent implements OnInit {
  breadcrumbItems: MenuItem[];
  constructor(private translateService: TranslateService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.breadcrumbItems = [{ label: this.getSelectedEnvironment(), disabled: true }];
  }
  ngOnInit() {
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(() => {
      this.updateBreadcrumb();
    });
    this.updateBreadcrumb();
  }

  getSelectedEnvironment() {
    const routeParams = this.activatedRoute.snapshot.paramMap;
    return routeParams.get("selectedEnvironment");
  }

  updateBreadcrumb(): void {
    const currentBreadcrumb: MenuItem[] = [...this.breadcrumbItems];
    let currentRoute = this.activatedRoute;

    while (currentRoute.firstChild) {
      const childRoute = currentRoute.firstChild;
      if (childRoute.snapshot.data && childRoute.snapshot.data.breadcrumb) {
        const label = this.getLabel(childRoute.snapshot);
        if (label && !currentBreadcrumb.some((item) => item.label === label)) {
          const routerLink = this.getRouterLink(childRoute, currentBreadcrumb.length - 1);
          currentBreadcrumb.push({
            label,
            routerLink,
            command: () => this.onBreadcrumbItemClick(label, currentBreadcrumb.length - 1),
          });
        }
      }
      currentRoute = childRoute;
    }

    this.breadcrumbItems = currentBreadcrumb;
  }

  private getLabel(snapshot: ActivatedRouteSnapshot): string | null {
    const routeData = snapshot.data;
    const params = snapshot.params;
    if (routeData.breadcrumb) {
      return routeData.breadcrumb.replace(/:(\w+)/g, (match, param) => {
        return params[param] || match;
      });
    }
    return null;
  }
  getRouterLink(route: ActivatedRoute, clickedIndex: number): string[] {
    const urlSegments: string[] = [];
    let currentRoute = route;

    while (currentRoute) {
      const segments = currentRoute.snapshot.url.map((segment) => segment.path);
      urlSegments.unshift(...segments);
      currentRoute = currentRoute.parent;
    }

    return ["/", ...urlSegments];
  }
  onBreadcrumbItemClick(label: string, clickedIndex: number): void {
    const route = this.breadcrumbItems[clickedIndex].routerLink;
    this.router.navigate(route);

    const breadcrumbToShow = this.breadcrumbItems.slice(0, clickedIndex + 1);
    this.breadcrumbItems.splice(0, this.breadcrumbItems.length, ...breadcrumbToShow);
  }
}
