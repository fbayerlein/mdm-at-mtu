/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { SelectItem } from "primeng/api";
import { finalize, forkJoin, take } from "rxjs";
import { MDMNotificationService } from "../../core/mdm-notification.service";
import { Unit, UnitService } from "../units/unit.service";
import { Quantity, QuantityService } from "./quantity.service";

@Component({
  selector: "mdm-qtt-viewer",
  templateUrl: "./quantity-viewer.component.html",
  styleUrls: ["./quantity-viewer.component.css"],
})
export class QuantityViewerComponent implements OnChanges {
  // passed down from parent
  paramQuantities: Quantity[];
  @Input() environment: string;
  // @Input()
  units: Unit[];

  // dialog and loading states
  dialogQuantityEdit = false;
  dialogCanEditName = false;
  dialogQuantityDelete = false;

  // temporary data for dialogs
  tmpQttEdit: Quantity;
  editTitle: string;
  tmpQttDelete: Quantity;
  qttAlreadyExists: string;

  private defaultUnitId: string;

  private dataTypeOpts: Array<SelectItem>;

  private dateFormat: string;

  // table selection
  selectedQtt: Quantity;

  // cloned quantities used in the UI / table
  public quantities: Quantity[];

  constructor(
    private quantityService: QuantityService,
    private unitService: UnitService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private datePipe: DatePipe,
  ) {
    this.editTitle = translateService.instant("administration.quantities.dialog-qtt-title");

    this.dateFormat = this.translateService.instant("details.mdm-detail-descriptive-data.transform-dateformat");

    if (this.dateFormat === undefined || this.dateFormat.trim() === "") {
      this.dateFormat = "yyyy-MM-dd HH:mm";
    }

    this.dataTypeOpts = new Array<SelectItem>();

    for (const dt of this.quantityService.getDataTypes()) {
      this.dataTypeOpts.push(<SelectItem>{ value: dt, label: dt });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (Object.prototype.hasOwnProperty.call(changes, "environment")) {
      this.environment = changes.environment.currentValue;
      this.reload();
    }
  }

  private reload() {
    if (this.environment !== undefined) {
      forkJoin([
        this.quantityService.getQuantitiesForSource(this.environment).pipe(take(1)),
        this.unitService.getUnitsForSource(this.environment).pipe(take(1)),
      ]).subscribe(([quantities, units]) => {
        this.paramQuantities = quantities;
        this.units = units;
        this.setQuantities();
      });
    } else {
      this.paramQuantities = [];
      this.units = [];
      this.quantities = [];
    }
  }

  getDataTypes() {
    return this.dataTypeOpts;
  }

  /**
   * !important! only generate this 1 time
   * otherwise the scrollable table will 'jump to end' or 'jump to start'
   * when clicking on a table row
   *
   * @returns cloned and modified quantities for the table UI
   */
  private setQuantities() {
    if (this.paramQuantities !== undefined && this.units !== undefined) {
      const archStatus = this.quantityService.getValidStati()[2];
      const quantities = [];
      for (const q of this.paramQuantities) {
        if (q.status !== archStatus) {
          quantities.push(this.cloneQuantity(q));
        }
      }
      this.quantities = quantities;
    } else {
      this.quantities = [];
    }
  }

  private cloneQuantity(qtt: Quantity): Quantity {
    const q = new Quantity();
    for (const prop in qtt) {
      q[prop] = qtt[prop];
    }
    q["unitName"] = this.getUnitDisplayName(qtt.unitId);
    q["fmtCreateDate"] = this.formatDate(qtt.cd);
    q["statusName"] = this.getLabelForStatus(qtt.status);
    return q;
  }

  private formatDate(dt: Date): string {
    let retVal = "";

    if (dt !== undefined && dt !== null && this.isValidDate(dt)) {
      retVal = this.datePipe.transform(dt, this.dateFormat, "+0000");
    }
    return retVal;
  }
  private isValidDate(d: Date) {
    return d instanceof Date && !isNaN(d.getTime());
  }

  private getUnitDisplayName(unitId: string): string {
    const unit = unitId !== undefined && this.units !== undefined ? this.units.find((u) => unitId === u.id) : undefined;

    return unit !== undefined ? unit.name : unitId;
  }

  private getDefaultUnitId(): string {
    if (this.defaultUnitId === undefined) {
      this.defaultUnitId = "1";

      if (this.units !== undefined && this.units.length > 0) {
        this.defaultUnitId = this.units[0].id;
      }
    }
    return this.defaultUnitId;
  }

  onQuantityRowSelect(event: any) {
    this.selectedQtt = event.data;

    if (this.selectedQtt.status === this.quantityService.getValidStati()[0]) {
      this.tmpQttEdit = this.cloneQuantity(event.data);
      this.setQuantitySelectedValues(this.tmpQttEdit);
      this.editTitle = this.translateService.instant("administration.quantities.dialog-qtt-edit-title");
      this.dialogQuantityEdit = true;

      const archStatus = this.quantityService.getValidStati()[2];
      const other = this.paramQuantities.find(
        (q) => q.mqName === this.tmpQttEdit.mqName && q.id !== this.tmpQttEdit.id && q.status !== archStatus,
      );

      this.dialogCanEditName = other === undefined;
    }
    return false;
  }

  private setQuantitySelectedValues(qtt: Quantity) {
    qtt["selectedUnit"] = this.units.find((u) => qtt.unitId === u.id);
    qtt["selectedDataType"] = <SelectItem>{ value: qtt.dataType, label: qtt.dataType };
  }

  private getLabelForStatus(status: string): string {
    let retValue = "";

    if (status !== undefined) {
      retValue = this.translateService.instant("administration.quantities.status-lbl-" + status.toLowerCase());
    }
    return retValue;
  }

  addQuantity() {
    this.qttAlreadyExists = undefined;
    this.editTitle = this.translateService.instant("administration.quantities.dialog-qtt-title");
    this.tmpQttEdit = new Quantity();
    this.tmpQttEdit.unitId = this.getDefaultUnitId();
    this.setQuantitySelectedValues(this.tmpQttEdit);
    this.dialogQuantityEdit = true;
    this.dialogCanEditName = true;
  }

  copyQuantity(qtt: Quantity) {
    this.qttAlreadyExists = undefined;
    const i18nCopyOf = this.translateService.instant("administration.quantities.txt-copy-of");
    let name = i18nCopyOf.replace(/\{0\}/g, "") + qtt.name;
    const archStatus = this.quantityService.getValidStati()[2];
    const cnt = this.paramQuantities.filter((q) => q.name === name && q.id !== qtt.id && q.status !== archStatus).length;

    if (cnt > 0) {
      name = i18nCopyOf.replace(/\{0\}/g, cnt + 1 + "") + qtt.name;
    }

    this.tmpQttEdit = this.makeNewCopy(qtt, name, 1);
    this.dialogQuantityEdit = true;
    this.dialogCanEditName = true;
  }

  newVersion(qtt: Quantity) {
    this.qttAlreadyExists = undefined;

    let version = qtt.version !== undefined ? qtt.version : 0;

    const archStatus = this.quantityService.getValidStati()[2];
    this.paramQuantities
      .filter((q) => q.mqName === qtt.mqName && q.id !== qtt.id && q.status !== archStatus && q.version > version)
      .forEach((q) => {
        if (version < q.version) {
          version = q.version;
        }
      });

    this.tmpQttEdit = this.makeNewCopy(qtt, qtt.name, version + 1);
    console.log(this.tmpQttEdit);
    this.dialogQuantityEdit = true;
    this.dialogCanEditName = false;
  }

  private makeNewCopy(qtt: Quantity, name: string, version: number): Quantity {
    this.editTitle = this.translateService.instant("administration.quantities.dialog-qtt-copy-title");
    const newQtt = new Quantity();
    newQtt.status = this.quantityService.getValidStati()[0];
    newQtt.id = "-" + qtt.id;
    newQtt.unitId = qtt.unitId;
    newQtt.name = name;
    newQtt.mqName = name;
    newQtt.desc = qtt.desc;
    newQtt.typeSize = qtt.typeSize;
    newQtt.version = version;
    newQtt.dataType = qtt.dataType;
    newQtt.cd = qtt.cd;
    this.setQuantitySelectedValues(newQtt);
    newQtt["unitName"] = this.getUnitDisplayName(qtt.unitId);
    newQtt["fmtCreateDate"] = this.formatDate(qtt.cd);
    newQtt["statusName"] = this.getLabelForStatus(qtt.status);
    return newQtt;
  }

  nameCheckUnique() {
    const validStatus = this.quantityService.getValidStati()[1];
    const qtt = this.paramQuantities.find(
      (q) => q.name === this.tmpQttEdit.name && q.id !== this.tmpQttEdit.id && q.status === validStatus,
    );

    if (qtt !== undefined) {
      this.qttAlreadyExists = this.translateService.instant("administration.quantities.err-already-exists-qtt");
    } else {
      this.qttAlreadyExists = undefined;
    }
  }

  removeQuantity(qtt?: Quantity) {
    this.tmpQttDelete = qtt;
    this.selectedQtt = qtt;
    this.dialogQuantityDelete = true;
    this.cancelDialogQuantity();
  }

  cancelRemoveQuantity() {
    this.tmpQttDelete = undefined;
    this.dialogQuantityDelete = false;
    this.dialogCanEditName = false;
  }

  private removeFromList(id: string, qtts: Quantity[]) {
    const idx = qtts.findIndex((q) => q.id === id);

    if (idx > -1) {
      qtts.splice(idx, 1);
    }
  }

  confirmRemoveQuantity() {
    if (this.tmpQttDelete !== undefined) {
      this.removeFromList(this.tmpQttDelete.id, this.paramQuantities);
      this.removeFromList(this.tmpQttDelete.id, this.quantities);

      if (this.tmpQttDelete.id !== undefined && parseInt(this.tmpQttDelete.id, 10) > 0) {
        this.quantityService
          .deleteQuantityForSource(this.environment, this.tmpQttDelete.id)
          .pipe(finalize(() => this.reload()))
          .subscribe();
      }
    }
    this.selectedQtt = undefined;
    this.cancelRemoveQuantity();
  }

  saveDialogQuantity() {
    const preSaveQuantity = this.tmpQttEdit;
    preSaveQuantity.unitId = preSaveQuantity["selectedUnit"]["id"];
    preSaveQuantity.dataType = preSaveQuantity["selectedDataType"]["value"];

    if (preSaveQuantity.id !== undefined && preSaveQuantity.id.trim().length > 0 && parseInt(preSaveQuantity.id, 10) > 0) {
      this.updateDialogQuantity(preSaveQuantity);
    } else {
      this.createDialogQuantity(preSaveQuantity);
    }

    this.cancelDialogQuantity();
  }

  /**
   *
   * @param qtt the quantity where the status will be changed
   * @param idx the status index
   */
  private updateQuantityStatus(qtt: Quantity, idx: number) {
    if (idx < this.quantityService.getValidStati().length && idx > -1 && qtt.status !== this.quantityService.getValidStati()[idx]) {
      const q = this.cloneQuantity(qtt);
      q.status = this.quantityService.getValidStati()[idx];
      this.updateDialogQuantity(q);
    }
  }

  archiveQuantity(qtt: Quantity) {
    if (qtt !== undefined) {
      this.removeFromList(qtt.id, this.paramQuantities);
      this.removeFromList(qtt.id, this.quantities);
      this.updateQuantityStatus(qtt, 2);
    }
  }

  publishQuantity(qtt: Quantity) {
    if (qtt !== undefined) {
      // deactivate all other valid ones? - no (in MDM4, multiple versions can be valid at the same time)
      this.updateQuantityStatus(qtt, 1);
    }
  }

  updateDialogQuantity(qtt: Quantity) {
    this.quantityService
      .updateQuantityForSource(this.environment, qtt)
      .pipe(finalize(() => this.reload()))
      .subscribe(
        (response) => this.patchResponse(response, qtt),
        (error) => {
          this.notificationService.notifyError(this.translateService.instant("administration.quantities.err-cannot-update-qtt"), error);
        },
      );
  }

  createDialogQuantity(qtt: Quantity) {
    qtt.mqName = qtt.name;

    this.quantityService
      .createQuantityForSource(this.environment, qtt)
      .pipe(finalize(() => this.reload()))
      .subscribe(
        (response) => this.patchResponse(response, qtt),
        (error) =>
          this.notificationService.notifyError(this.translateService.instant("administration.quantities.err-cannot-save-qtt"), error),
      );
  }

  hideDialogQuantity() {
    if (this.dialogQuantityEdit) {
      this.cancelDialogQuantity();
    }
  }

  cancelDialogQuantity() {
    this.dialogQuantityEdit = false;
    this.dialogCanEditName = false;
    this.tmpQttEdit = undefined;
    this.qttAlreadyExists = undefined;
  }

  private patchResponse(qtts: Quantity[], preSaveQuantity: Quantity) {
    let quantity = qtts.find((q) => q.name === preSaveQuantity.name || q.id === preSaveQuantity.id);

    if (quantity === undefined && qtts.length === 1) {
      quantity = qtts[0];
    }

    if (quantity !== undefined) {
      if (preSaveQuantity.id === undefined || preSaveQuantity.id.startsWith("-")) {
        // the API only saves name + unit when creating a qtt.
        // to copy all fields we need to update, the created quantity with the
        // data of our copy ...
        const mergedQtt = this.cloneQuantity(preSaveQuantity);
        mergedQtt.id = quantity.id;
        mergedQtt.cd = quantity.cd;
        mergedQtt.typeSize = quantity.typeSize;
        mergedQtt.rank = quantity.rank;

        if (preSaveQuantity.id === undefined) {
          mergedQtt.mqName = quantity.mqName;
        }
        this.updateCopiedQuantity(mergedQtt);
        this.paramQuantities.push(mergedQtt);
        this.selectedQtt = this.cloneQuantity(mergedQtt);
        this.quantities.push(this.selectedQtt);
      } else {
        const idx = this.paramQuantities.findIndex((q) => q.id === quantity.id);
        const archStatus = this.quantityService.getValidStati()[2];

        this.selectedQtt = this.cloneQuantity(quantity);
        if (idx > -1) {
          this.paramQuantities[idx] = quantity;
          const idx2 = this.quantities.findIndex((q) => q.id === quantity.id);
          this.quantities[idx2] = this.selectedQtt;
        } else if (quantity.status !== archStatus) {
          this.paramQuantities.push(quantity);
          this.quantities.push(this.selectedQtt);
        } else {
          // was archived
          this.selectedQtt = undefined;
        }
      }
    }
  }

  private updateCopiedQuantity(copy: Quantity) {
    this.quantityService
      .updateQuantityForSource(this.environment, copy)
      .pipe(finalize(() => this.reload()))
      .subscribe(
        (response) => {
          /* skip it, otherwise response goes into error message */
        },
        (error) =>
          this.notificationService.notifyError(this.translateService.instant("administration.quantities.err-cannot-save-qtt"), error),
      );
  }
}
