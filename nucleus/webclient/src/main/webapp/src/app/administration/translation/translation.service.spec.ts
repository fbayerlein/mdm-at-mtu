/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { TestBed } from "@angular/core/testing";

import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { PropertyService } from "@core/property.service";
import { MdmLocalizationService } from "@localization/mdm-localization.service";
import { TranslationService } from "./translation.service";

import { NodeService } from "@navigator/node.service";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { HttpLoaderFactory } from "src/app/app.module";
/* 
let dummy = {
  readTranslations(environment?: string) {

  }
};
*/
const environment = {
  type: "Environment",
  data: [
    {
      name: "my_ds",
      id: "1",
      type: "Environment",
      sourceType: "Environment",
      sourceName: "my_ds",
      attributes: [
        {
          name: "Timezone",
          value: "GMT",
          unit: "",
          dataType: "STRING",
          enumName: null,
        },
        {
          name: "MeaningOfAliases",
          value: [",language=en", ",language=de"],
          unit: "",
          dataType: "STRING_SEQUENCE",
          enumName: null,
        },
        {
          name: "Name",
          value: "my_ds",
          unit: "",
          dataType: "STRING",
          enumName: null,
        },
      ],
      relations: [],
    },
  ],
};

describe("TranslationService", () => {
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient],
          },
        }),
      ],
      providers: [TranslationService, HttpClient, PropertyService, TranslateService, NodeService, MdmLocalizationService],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it("should be created", () => {
    const service: TranslationService = TestBed.inject(TranslationService);
    expect(service).toBeTruthy();
  });

  it("loadTranslationsForCatalogComponent", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service.loadTranslationsForCatalogComponent("my_ds", "unitundertest", "vehicle", ["model", "length"]).subscribe((data) =>
      expect(data).toEqual({
        languages: ["en", "de"],
        componentName: ["Vehicle", "Fahrzeug"],
        attributes: {
          model: ["Model", "Modell"],
          length: ["Length", "Länge"],
        },
      }),
    );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    req.flush({
      mt: {
        "application/x-asam.aounitundertestpart.vehicle": ["Vehicle", "Fahrzeug"],
      },
      val: {
        "application/x-asam.aounitundertestpart.vehicle/name/vehicle": ["Vehicle", "Fahrzeug"],
        "application/x-asam.catunitundertestattr/name": {
          model: ["Model", "Modell"],
          length: ["Length", "Länge"],
        },
      },
      attr: {
        "application/x-asam.aounitundertestpart.vehicle": {
          model: ["Model", "Modell"],
          length: ["Length", "Länge"],
        },
      },
    });
    httpTestingController.verify();
  });

  it("loadTranslationsForCatalogComponentMissing", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service.loadTranslationsForCatalogComponent("my_ds", "unitundertest", "vehicle", ["model", "length"]).subscribe((data) =>
      expect(data).toEqual({
        languages: ["en", "de"],
        componentName: ["vehicle", "vehicle"],
        attributes: {
          model: ["model", "model"],
          length: ["length", "length"],
        },
      }),
    );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    req.flush({
      mt: {},
      val: {},
      attr: {},
    });
    httpTestingController.verify();
  });

  it("saveTranslationsForCatalogComponent", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .saveTranslationsForCatalogComponent("my_ds", "unitundertest", "vehicle", ["Vehicle", "Fahrzeug"], {
        model: ["Model", "Modell"],
        length: ["Length", "Länge"],
      })
      .subscribe((data) => expect(data).toEqual({}));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("POST");
    expect(req.request.body).toEqual({
      mt: {
        "application/x-asam.aounitundertestpart.vehicle": ["Vehicle", "Fahrzeug"],
      },
      val: {
        "application/x-asam.aounitundertestpart.vehicle/name/vehicle": ["Vehicle", "Fahrzeug"],
        "application/x-asam.aoany.catunitundertestattr/name": {
          model: ["Model", "Modell"],
          length: ["Length", "Länge"],
        },
      },
      attr: {
        "application/x-asam.aounitundertestpart.vehicle": {
          model: ["Model", "Modell"],
          length: ["Length", "Länge"],
        },
      },
    });

    req.flush({});
    httpTestingController.verify();
  });

  it("loadTranslationsForComponentTemplate", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .loadTranslationsForComponentTemplate("my_ds", "unitundertest", "car_template", [
        { catalogName: "vehicle", componentName: "car1" },
        { catalogName: "gearbox", componentName: "gearbox" },
      ])
      .subscribe((data) =>
        expect(data).toEqual({
          languages: ["en", "de"],
          tplName: "car_template",
          nameTranslations: ["Vehicle Template", "Fahrzeug Vorlage"],
          componentNames: [
            {
              catalogName: "vehicle",
              componentName: "car1",
              translations: ["Vehicle 1", "Fahrzeug 1"],
            },
            {
              catalogName: "gearbox",
              componentName: "gearbox",
              translations: ["Gear box", "Getriebe"],
            },
          ],
        }),
      );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    req.flush({
      mt: {
        "application/x-asam.aounitundertestpart.vehicle.car1": ["Vehicle 1", "Fahrzeug 1"],
        "application/x-asam.aounitundertestpart.gearbox.gearbox": ["Gear box", "Getriebe"],
      },
      val: {
        "application/x-asam.aoany.tplunitundertestroot": {
          name: {
            car_template: ["Vehicle Template", "Fahrzeug Vorlage"],
          },
        },
        "application/x-asam.aounitundertestpart.vehicle.car1/name/car1": ["Vehicle 1", "Fahrzeug 1"],
        "application/x-asam.aounitundertestpart.gearbox.gearbox/name/gearbox": ["Gear box", "Getriebe"],
      },
    });
    httpTestingController.verify();
  });

  it("loadTranslationsForComponentTemplate returns fallback translations", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .loadTranslationsForComponentTemplate("my_ds", "unitundertest", "car_template", [
        { catalogName: "vehicle", componentName: "car1" },
        { catalogName: "gearbox", componentName: "gearbox" },
      ])
      .subscribe((data) =>
        expect(data).toEqual({
          languages: ["en", "de"],
          tplName: "car_template",
          nameTranslations: ["Vehicle Template", "Fahrzeug Vorlage"],
          componentNames: [
            {
              catalogName: "vehicle",
              componentName: "car1",
              translations: ["Vehicle 1", "Fahrzeug 1"],
            },
            {
              catalogName: "gearbox",
              componentName: "gearbox",
              translations: ["Gear box", "Getriebe"],
            },
          ],
        }),
      );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    req.flush({
      val: {
        "application/x-asam.aoany.tplunitundertestroot": {
          name: {
            car_template: ["Vehicle Template", "Fahrzeug Vorlage"],
          },
        },
        "application/x-asam.aounitundertestpart.vehicle.car1": {
          name: {
            car1: ["Vehicle 1", "Fahrzeug 1"],
          },
        },
        "application/x-asam.aounitundertestpart.gearbox.gearbox": {
          name: {
            gearbox: ["Gear box", "Getriebe"],
          },
        },
      },
    });
    httpTestingController.verify();
  });

  it("loadTranslationsForComponentTemplate returns default translations", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .loadTranslationsForComponentTemplate("my_ds", "unitundertest", "car_template", [
        { catalogName: "vehicle", componentName: "car1" },
        { catalogName: "gearbox", componentName: "gearbox" },
      ])
      .subscribe((data) =>
        expect(data).toEqual({
          languages: ["en", "de"],
          tplName: "car_template",
          nameTranslations: ["car_template", "car_template"],
          componentNames: [
            {
              catalogName: "vehicle",
              componentName: "car1",
              translations: ["car1", "car1"],
            },
            {
              catalogName: "gearbox",
              componentName: "gearbox",
              translations: ["gearbox", "gearbox"],
            },
          ],
        }),
      );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    // no translations from backend call
    req.flush({});
    httpTestingController.verify();
  });

  it("saveTranslationsForComponentTemplate", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .saveTranslationsForComponentTemplate(
        "my_ds",
        "unitundertest",
        "car_template",
        ["Vehicle Template", "Fahrzeug Vorlage"],
        [
          {
            catalogName: "vehicle",
            componentName: "car1",
            translations: ["Vehicle 1", "Fahrzeug 1"],
          },
          {
            catalogName: "gearbox",
            componentName: "gearbox",
            translations: ["Gear box", "Getriebe"],
          },
        ],
      )
      .subscribe((data) => expect(data).toEqual({}));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("POST");
    expect(req.request.body).toEqual({
      mt: {
        "application/x-asam.aounitundertestpart.vehicle.car1": ["Vehicle 1", "Fahrzeug 1"],
        "application/x-asam.aounitundertestpart.gearbox.gearbox": ["Gear box", "Getriebe"],
      },
      val: {
        "application/x-asam.aoany.tplunitundertestroot/name/car_template": ["Vehicle Template", "Fahrzeug Vorlage"],
        "application/x-asam.aounitundertestpart.vehicle.car1/name/car1": ["Vehicle 1", "Fahrzeug 1"],
        "application/x-asam.aounitundertestpart.gearbox.gearbox/name/gearbox": ["Gear box", "Getriebe"],
      },
    });

    req.flush({});
    httpTestingController.verify();
  });

  it("saveTranslationsForTestStepTemplate", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .saveTranslationsForTestStepTemplate("my_ds", "PBN_UNECE_R51_RUN", ["PBN UNECE R51 run", "PBN UNECE R51 Lauf"])
      .subscribe((data) => expect(data).toEqual({}));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("POST");
    expect(req.request.body).toEqual({
      val: {
        "application/x-asam.aoany.tplteststep/name/pbn_unece_r51_run": ["PBN UNECE R51 run", "PBN UNECE R51 Lauf"],
      },
    });

    req.flush({});
    httpTestingController.verify();
  });

  it("loadTranslationsForTestStepTemplate", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service.loadTranslationsForTestStepTemplate("my_ds", "PBN_UNECE_R51_RUN").subscribe((data) =>
      expect(data).toEqual({
        languages: ["en", "de"],
        nameTranslations: ["PBN UNECE R51 run", "PBN UNECE R51 Lauf"],
      }),
    );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    req.flush({
      val: {
        "application/x-asam.aoany.tplteststep": {
          name: {
            pbn_unece_r51_run: ["PBN UNECE R51 run", "PBN UNECE R51 Lauf"],
          },
        },
      },
    });
    httpTestingController.verify();
  });

  it("saveTranslationsForTestTemplate", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .saveTranslationsForTestTemplate("my_ds", "PBN_UNECE_R51", ["PBN UNECE R51 en", "PBN UNECE R51 de"])
      .subscribe((data) => expect(data).toEqual({}));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("POST");
    expect(req.request.body).toEqual({
      val: {
        "application/x-asam.aoany.tpltest/name/pbn_unece_r51": ["PBN UNECE R51 en", "PBN UNECE R51 de"],
      },
    });

    req.flush({});
    httpTestingController.verify();
  });

  it("loadTranslationsForTestTemplate", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service.loadTranslationsForTestTemplate("my_ds", "PBN_UNECE_R51").subscribe((data) =>
      expect(data).toEqual({
        languages: ["en", "de"],
        nameTranslations: ["PBN UNECE R51 en", "PBN UNECE R51 de"],
      }),
    );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    req.flush({
      val: {
        "application/x-asam.aoany.tpltest": {
          name: {
            pbn_unece_r51: ["PBN UNECE R51 en", "PBN UNECE R51 de"],
          },
        },
      },
    });
    httpTestingController.verify();
  });

  it("saveTranslationsForEnum", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service
      .saveTranslationsForEnum("my_ds", "axistype", ["Axis type", "Achsentyp"], {
        0: ["X-Axis", "X-Achse"],
        1: ["Y-Axis", "Y-Achse"],
      })
      .subscribe((data) => expect(data).toEqual({}));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("POST");
    expect(req.request.body).toEqual({
      val: {
        "application/x-mdm.enumdef/name/axistype": ["Axis type", "Achsentyp"],
      },
      enum: {
        axistype: {
          0: ["X-Axis", "X-Achse"],
          1: ["Y-Axis", "Y-Achse"],
        },
      },
    });

    req.flush({});
    httpTestingController.verify();
  });

  it("loadTranslationsForEnum", () => {
    const service: TranslationService = TestBed.inject(TranslationService);

    service.loadTranslationsForEnum("my_ds", "axistype").subscribe((data) =>
      expect(data).toEqual({
        languages: ["en", "de"],
        nameTranslations: ["Axis type", "Achsentyp"],
        valueTranslations: {
          "0": ["X-Axis", "X-Achse"],
          "1": ["Y-Axis", "Y-Achse"],
        },
      }),
    );

    httpTestingController.match("/org.eclipse.mdm.nucleus/mdm/environments").forEach((req) => req.flush(environment));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/my_ds/mdmlocalizations");

    expect(req.request.method).toEqual("GET");

    req.flush({
      val: {
        "application/x-mdm.enumdef": {
          name: {
            axistype: ["Axis type", "Achsentyp"],
          },
        },
      },
      enum: {
        axistype: {
          0: ["X-Axis", "X-Achse"],
          1: ["Y-Axis", "Y-Achse"],
        },
      },
    });
    httpTestingController.verify();
  });
});
