import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { OrderListModule } from "primeng/orderlist";
import { TableModule } from "primeng/table";
import { TestTemplateComponent } from "./test-template-component/test-template.component";
import { TestTemplateDetailComponent } from "./test-template-detail/test-template-detail.component";
import { TestTemplateRoutingModule } from "./test-template-routing.module";
import { TestTemplateViewerComponent } from "./test-template-viewer/test-template-viewer.component";
import { TestTemplateManagerService } from "./test-template.service";
@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ContextMenuModule,
    TestTemplateRoutingModule,
    OrderListModule,
  ],
  declarations: [TestTemplateViewerComponent, TestTemplateComponent, TestTemplateDetailComponent],
  exports: [],
  providers: [TestTemplateManagerService],
})
export class TestTemplateModule {}
