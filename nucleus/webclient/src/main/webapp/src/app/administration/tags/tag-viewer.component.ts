/********************************************************************************
 * Copyright (c) 2025 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input } from "@angular/core";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { MDMTag, MDMTagService } from "src/app/mdmtag/mdmtag.service";
import { Attribute, Node } from "../../navigator/node";

@Component({
  selector: "mdm-tag-viewer",
  templateUrl: "./tag-viewer.component.html",
  styleUrls: ["./tag-viewer.component.css"],
})
export class TagViewerComponent {
  @Input() tags: MDMTag[];
  @Input() selectedEnvironment: string;

  tmpTagEdit: MDMTag;
  tmpTagDelete: MDMTag;
  editTitle = "";
  dialogTagEdit = false;
  dialogTagDelete = false;
  selectedTag: MDMTag;
  editMode = false;

  constructor(
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private nodeService: NodeService,
    private mdmTagService: MDMTagService,
  ) {}

  getAttributeValueFromNode(node: Node, attribute: string) {
    for (const attr of node.attributes) {
      if (attr.name === attribute) {
        return attr.value;
      }
    }
    return "";
  }

  createAttribute(name: string, value?: string) {
    const attr = new Attribute();
    attr.dataType = "STRING";
    attr.unit = "";
    attr.value = value !== undefined ? value : "";
    attr.name = name;
    return attr;
  }

  getAttributeFromNode(node: Node, attribute: string) {
    if (node.attributes !== undefined) {
      for (const attr of node.attributes) {
        if (attr.name === attribute) {
          return attr;
        }
      }
    }
    return undefined;
  }

  addTag() {
    this.editTitle = this.translateService.instant("administration.tags.dialog-add-tag-title");
    this.tmpTagEdit = new MDMTag();
    this.tmpTagEdit.type = "MDMTag";
    this.tmpTagEdit.sourceType = "MDMTag";
    this.tmpTagEdit.sourceName = this.selectedEnvironment;
    this.tmpTagEdit.attributes = [];
    this.tmpTagEdit.attributes.push(this.createAttribute("Description"));
    this.tmpTagEdit.attributes.push(this.createAttribute("Name"));
    this.tmpTagEdit.attributes.push(this.createAttribute("MimeType", "application/x-asam.aoany.mdmtag"));

    this.dialogTagEdit = true;
    this.editMode = false;
  }

  onTagRowSelect() {
    this.editTitle = this.translateService.instant("administration.tags.dialog-edit-tag-title");
    this.tmpTagEdit = structuredClone(this.selectedTag);
    this.tmpTagEdit.name = this.getAttributeFromNode(this.tmpTagEdit, "Name").value as string;
    this.tmpTagEdit.description = this.getAttributeFromNode(this.tmpTagEdit, "Description").value as string;
    this.dialogTagEdit = true;
    this.editMode = true;
  }

  saveDialogTag() {
    // update the name and description attribute
    this.getAttributeFromNode(this.tmpTagEdit, "Name").value = this.tmpTagEdit.name;
    this.getAttributeFromNode(this.tmpTagEdit, "Description").value = this.tmpTagEdit.description;
    this.mdmTagService.saveMDMTag(this.selectedEnvironment, this.tmpTagEdit).subscribe(
      (response) => this.patchResponse(response.data),
      (error) => this.notificationService.notifyError(this.translateService.instant("administration.tags.err-cannot-save-tag"), error),
    );
    this.dialogTagEdit = false;
  }

  cancelDialogTag() {
    this.dialogTagEdit = false;
    this.tmpTagEdit = undefined;
  }

  removeTag(tag: MDMTag) {
    this.tmpTagDelete = tag;
    this.dialogTagDelete = true;
  }

  cancelRemoveTag() {
    this.tmpTagDelete = undefined;
    this.dialogTagDelete = false;
  }

  confirmRemoveTag() {
    if (this.tmpTagDelete != undefined) {
      if (this.tmpTagDelete.id !== undefined && parseInt(this.tmpTagDelete.id, 10) > 0) {
        this.mdmTagService.deleteMDMTag(this.selectedEnvironment, this.tmpTagDelete.id).subscribe(
          () => {
            const idxES: number = this.tags.indexOf(this.tmpTagDelete);
            if (idxES !== -1) {
              this.tags.splice(idxES, 1);
              this.tmpTagDelete = undefined;
            }
          },
          (error) =>
            this.notificationService.notifyError(this.translateService.instant("administration.value-list.err-cannot-delete-tag"), error),
        );
      }
    }

    this.dialogTagDelete = false;
  }

  patchResponse(nodes: MDMTag[]) {
    console.log(nodes);
    for (const node of nodes) {
      if (node.name === this.tmpTagEdit.name) {
        if (this.tmpTagEdit.id === undefined) {
          this.tags.push(this.tmpTagEdit);
        } else {
          const idx = this.tags.findIndex((tag: MDMTag) => tag.id === node.id);
          this.tags[idx] = structuredClone(node);
        }
        this.tmpTagEdit.id = node.id;
      }
    }
    this.tmpTagEdit = undefined;
  }
}
