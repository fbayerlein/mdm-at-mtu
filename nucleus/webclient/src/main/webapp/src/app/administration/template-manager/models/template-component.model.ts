import { Attribute, Relation } from "@navigator/node";
export class TemplateComponent {
  id: string;
  name: string;
  catalogComponent: string;
  optional: boolean;
  defaultActive: boolean;
  testStepSeriesVariable: boolean;
  attributes: Attribute[];
  relations: Relation[];
}
