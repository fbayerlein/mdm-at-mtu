/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { finalize, forkJoin, take } from "rxjs";
import { MDMNotificationService } from "../../core/mdm-notification.service";
import { PhysDimService, PhysDimTuple } from "./physicaldimension.service";
import { Unit, UnitService } from "./unit.service";

@Component({
  selector: "mdm-unit-viewer",
  templateUrl: "./unit-viewer.component.html",
  styleUrls: ["./unit-viewer.component.css"],
})
export class UnitViewerComponent implements OnChanges {
  // passed down from parent
  paramUnits: Unit[];
  @Input() environment: string;
  physDims: PhysDimTuple[];

  // dialog and loading states
  dialogUnitEdit = false;
  dialogCanRemove = false;
  dialogUnitDelete = false;

  // temporary data for dialogs
  tmpUnitEdit: Unit;
  unitEditTitle: string;
  tmpUnitDelete: Unit;
  unitAlreadyExists: string;

  private defaultPysDimId: string;

  // table selection
  selectedUnit: Unit;

  // cloned units used in the UI / table
  public units: Unit[];

  constructor(
    private unitService: UnitService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private physDimService: PhysDimService,
  ) {
    this.unitEditTitle = translateService.instant("administration.units.dialog-unit-title");
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (Object.prototype.hasOwnProperty.call(changes, "environment")) {
      this.environment = changes.environment.currentValue;
      this.reload();
    }
  }

  private reload() {
    if (this.environment !== undefined) {
      this.physDimService.resetCache();
      this.unitService.resetCache();
      forkJoin([
        this.physDimService.getPhysDimsForSource(this.environment).pipe(take(1)),
        this.unitService.getUnitsForSource(this.environment).pipe(take(1)),
      ]).subscribe(([physDims, units]) => {
        this.physDims = physDims;
        this.paramUnits = units;
        this.setUnits();
      });
    } else {
      this.physDims = [];
      this.paramUnits = [];
      this.units = [];
    }
  }

  /**
   * !important! only generate this 1 time
   * otherwise the scrollable table will 'jump to end' or 'jump to start'
   * when clicking on a table row
   *
   * @returns cloned and modified units for the table UI
   */
  private setUnits() {
    if (this.paramUnits !== undefined && this.physDims !== undefined) {
      const units = [];
      for (const u of this.paramUnits) {
        units.push(this.cloneUnit(u));
      }
      this.units = units;
    } else {
      this.units = [];
    }
  }

  private cloneUnit(unit: Unit): Unit {
    const u = new Unit();
    for (const prop in unit) {
      u[prop] = unit[prop];
    }
    u["physDimName"] = this.getPhysDimDisplayName(u.physDimId);
    return u;
  }

  private getPhysDimDisplayName(physDimId: string): string {
    const physDim = physDimId !== undefined && this.physDims !== undefined ? this.physDims.find((pd) => physDimId === pd.id) : undefined;

    return physDim !== undefined ? physDim.name : physDimId;
  }

  private getDefaultPysDimId(): string {
    if (this.defaultPysDimId === undefined) {
      this.defaultPysDimId = "1";

      if (this.physDims !== undefined && this.physDims.length > 0) {
        const physDim = this.physDims.find((pd) => "none" === pd.name);

        this.defaultPysDimId = physDim !== undefined ? physDim.id : this.physDims[0].id;
      }
    }
    return this.defaultPysDimId;
  }

  onUnitRowSelect(event: any) {
    this.selectedUnit = event.data;
    this.tmpUnitEdit = this.cloneUnit(event.data);
    this.tmpUnitEdit["selectedPhysDim"] = this.physDims.find((pd) => this.tmpUnitEdit.physDimId === pd.id);
    this.unitEditTitle = this.translateService.instant("administration.units.dialog-unit-edit-title");
    this.dialogUnitEdit = true;
    this.dialogCanRemove = true;
  }

  addUnit() {
    this.unitAlreadyExists = undefined;
    this.unitEditTitle = this.translateService.instant("administration.units.dialog-unit-title");
    this.tmpUnitEdit = new Unit();
    this.tmpUnitEdit.physDimId = this.getDefaultPysDimId();
    this.tmpUnitEdit["selectedPhysDim"] = this.physDims.find((pd) => this.tmpUnitEdit.physDimId === pd.id);
    this.dialogUnitEdit = true;
    this.dialogCanRemove = false;
  }

  copyUnit(unit?: Unit) {
    if (unit !== undefined) {
      this.unitAlreadyExists = undefined;
      const i18nCopyOf = this.translateService.instant("administration.units.txt-copy-of");
      let name = i18nCopyOf.replace(/\{0\}/g, "") + unit.name;
      const cnt = this.paramUnits.filter((u) => u.name === name).length;

      if (cnt > 0) {
        name = i18nCopyOf.replace(/\{0\}/g, cnt + 1 + "") + unit.name;
      }

      this.unitEditTitle = this.translateService.instant("administration.units.dialog-unit-copy-title");
      this.tmpUnitEdit = new Unit();
      this.tmpUnitEdit.id = "-" + unit.id;
      this.tmpUnitEdit.physDimId = unit.physDimId;
      this.tmpUnitEdit.name = name;
      this.tmpUnitEdit.desc = unit.desc;
      this.tmpUnitEdit.factor = unit.factor;
      this.tmpUnitEdit.offset = unit.offset;
      this.tmpUnitEdit.dB = unit.dB;
      this.tmpUnitEdit.dBRefFactor = unit.dBRefFactor;
      this.tmpUnitEdit["selectedPhysDim"] = this.physDims.find((pd) => this.tmpUnitEdit.physDimId === pd.id);
      this.dialogUnitEdit = true;
      this.dialogCanRemove = false;
    }
  }

  nameCheckUnique() {
    const unit = this.paramUnits.find((u) => u.name === this.tmpUnitEdit.name);

    if (unit !== undefined) {
      this.unitAlreadyExists = this.translateService.instant("administration.units.err-already-exists-unit");
    } else {
      this.unitAlreadyExists = undefined;
    }
  }

  removeUnit(unit?: Unit) {
    this.tmpUnitDelete = unit;
    this.selectedUnit = unit;
    this.dialogUnitDelete = true;
    this.cancelDialogUnit();
  }

  cancelRemoveUnit() {
    this.tmpUnitDelete = undefined;
    this.dialogUnitDelete = false;
    this.dialogCanRemove = false;
  }

  private removeFromList(id: string, units: Unit[]) {
    const idx = units.findIndex((u) => u.id === id);

    if (idx > -1) {
      units.splice(idx, 1);
    }
  }

  confirmRemoveUnit() {
    if (this.tmpUnitDelete !== undefined) {
      this.removeFromList(this.tmpUnitDelete.id, this.paramUnits);
      this.removeFromList(this.tmpUnitDelete.id, this.units);

      if (this.tmpUnitDelete.id !== undefined && parseInt(this.tmpUnitDelete.id, 10) > 0) {
        this.unitService
          .deleteUnitForSource(this.environment, this.tmpUnitDelete.id)
          .pipe(finalize(() => this.reload()))
          .subscribe();
      }
    }
    this.selectedUnit = undefined;
    this.cancelRemoveUnit();
  }

  saveDialogUnit() {
    const preSaveUnit = this.tmpUnitEdit;
    preSaveUnit.physDimId = preSaveUnit["selectedPhysDim"]["id"];

    if (preSaveUnit.id !== undefined && preSaveUnit.id.trim().length > 0 && parseInt(preSaveUnit.id, 10) > 0) {
      this.updateDialogUnit(preSaveUnit);
    } else {
      this.createDialogUnit(preSaveUnit);
    }

    this.cancelDialogUnit();
  }

  updateDialogUnit(unit: Unit) {
    console.log("updateDialogUnit");
    this.unitService
      .updateUnitForSource(this.environment, unit)
      .pipe(finalize(() => this.reload()))
      .subscribe(
        (response) => this.patchResponse(response, unit),
        (error) => {
          this.notificationService.notifyError(this.translateService.instant("administration.units.err-cannot-update-unit"), error);
        },
      );
  }

  createDialogUnit(unit: Unit) {
    console.log("createDialogUnit");
    this.unitService
      .createUnitForSource(this.environment, unit)
      .pipe(finalize(() => this.reload()))
      .subscribe(
        (response) => this.patchResponse(response, unit),
        (error) => this.notificationService.notifyError(this.translateService.instant("administration.units.err-cannot-save-unit"), error),
      );
  }

  hideDialogUnit() {
    if (this.dialogUnitEdit) {
      this.cancelDialogUnit();
    }
  }

  cancelDialogUnit() {
    this.dialogUnitEdit = false;
    this.dialogCanRemove = false;
    this.tmpUnitEdit = undefined;
    this.unitAlreadyExists = undefined;
  }

  private patchResponse(units: Unit[], preSaveUnit: Unit) {
    const unit = units.find((u) => u.name === preSaveUnit.name || u.id === preSaveUnit.id);

    if (unit !== undefined) {
      if (preSaveUnit.id === undefined || preSaveUnit.id.startsWith("-")) {
        // the API only saves name + physical dimension when creating a unit.
        // to copy all fields we need to update, the created unit with the
        // data of our copy ...
        preSaveUnit.id = unit.id;
        this.paramUnits.push(preSaveUnit);
        this.updateCopiedUnit(preSaveUnit);
        this.selectedUnit = this.cloneUnit(preSaveUnit);
        this.units.push(this.selectedUnit);
      } else {
        const idx = this.paramUnits.findIndex((u) => u.id === unit.id);

        this.selectedUnit = this.cloneUnit(unit);
        if (idx > -1) {
          this.paramUnits[idx] = unit;
          const idx2 = this.units.findIndex((u) => u.id === unit.id);
          this.units[idx2] = this.selectedUnit;
        } else {
          this.paramUnits.push(unit);
          this.units.push(this.selectedUnit);
        }
      }
    }
  }

  private updateCopiedUnit(copy: Unit) {
    console.log("updateCopiedUnit");
    this.unitService
      .updateUnitForSource(this.environment, copy)
      .pipe(finalize(() => this.reload()))
      .subscribe(
        (response) => {
          /* skip it, otherwise response goes into error message */
        },
        (error) => this.notificationService.notifyError(this.translateService.instant("administration.units.err-cannot-save-unit"), error),
      );
  }
}
