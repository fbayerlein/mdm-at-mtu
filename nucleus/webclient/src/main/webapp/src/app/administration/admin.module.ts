/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { MDMDetailModule } from "@details/mdm-detail.module";
import { AttributeValidatorService } from "@details/services/attribute-validator.service";
import { AutoCompleteModule } from "primeng/autocomplete";
import { CheckboxModule } from "primeng/checkbox";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { MenuModule } from "primeng/menu";
import { PanelModule } from "primeng/panel";
import { SplitterModule } from "primeng/splitter";
import { TableModule } from "primeng/table";
import { ToolbarModule } from "primeng/toolbar";
import { TreeTableModule } from "primeng/treetable";
import { AdminModulesComponent } from "./admin-modules.component";
import { AdminRoutingModule } from "./admin-routing.module";
import { AnnouncementConfigEditorComponent } from "./announcements/announcement-config-editor/announcement-config-editor.component";
import { AnnouncementConfigComponent } from "./announcements/announcement-config.component";
import { CatalogModule } from "./catalog-manager/catalog.module";
import { CatalogService } from "./catalog.service";
import { ExtSystemModule } from "./extsystem/extsystem.module";
import { ExtSystemService } from "./extsystem/extsystem.service";
import { PreferenceModule } from "./preferences/preference.module";
import { QuantityModule } from "./quantities/quantity.module";
import { QuantityService } from "./quantities/quantity.service";
import { RightsConfigComponent } from "./rights-config/rights-config.component";
import { RightsConfigService } from "./rights-config/rights-config.service";
import { TagModule } from "./tags/tag.module";
import { TemplateModule } from "./template-manager/template.module";
import { TestStepTemplateModule } from "./teststep-template/teststep-template.module";
import { PhysDimService } from "./units/physicaldimension.service";
import { UnitModule } from "./units/unit.module";
import { UnitService } from "./units/unit.service";
import { ValueListModule } from "./value-list/value-list.module";

@NgModule({
  imports: [
    AdminRoutingModule,
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    PreferenceModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    SplitterModule,
    MenuModule,
    InputTextModule,
    InputTextareaModule,
    MDMDetailModule,
    ToolbarModule,
    CatalogModule,
    TemplateModule,
    TestStepTemplateModule,
    ExtSystemModule,
    UnitModule,
    QuantityModule,
    TagModule,
    ValueListModule,
    TreeTableModule,
    PanelModule,
    ConfirmDialogModule,
  ],
  declarations: [
    AdminModulesComponent,
    // UnitEditorComponent,
    RightsConfigComponent,
    AnnouncementConfigComponent,
    AnnouncementConfigEditorComponent,
  ],
  exports: [AdminModulesComponent],
  providers: [
    ExtSystemService,
    CatalogService,
    UnitService,
    PhysDimService,
    QuantityService,
    DatePipe,
    RightsConfigService,
    AttributeValidatorService,
  ],
})
export class AdminModule {}
