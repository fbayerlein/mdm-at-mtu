import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject } from "rxjs";
import { Node } from "../../navigator/node";
import { ValueListValue } from "./value-list-value.interface";
import { ValueList } from "./value-list.interface";
import { ValueListService } from "./valuelist.service";

@Component({
  selector: "mdm-value-list-editor",
  templateUrl: "./value-list-editor.component.html",
  styleUrls: ["./value-list.component.css"],
})
export class ValueListEditorComponent implements OnInit {
  @Input() valueLists: ValueList[];
  @Input() selectedEnvironment: string;
  @Input() selectedVL: string;

  @Output() editMode = new EventEmitter<boolean>();

  selectedValueList: ValueList;
  tableValueLists: ValueList[] = [];

  valueListValues: ValueListValue[] = [];
  bsValueListValues: BehaviorSubject<ValueListValue[]> = new BehaviorSubject<ValueListValue[]>(undefined);

  loadingValueListValues = false;
  tmpValueListValue: ValueListValue;

  constructor(
    private valueListService: ValueListService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.bsValueListValues.subscribe((value) => {
      this.valueListValues = value;
    });

    this.bsValueListValues.next(undefined);

    for (const valueList of this.valueLists) {
      if (valueList.id === this.selectedVL) {
        this.selectedValueList = valueList;
        this.tableValueLists.push(this.selectedValueList);
        break;
      }
    }

    this.loadingValueListValues = true;
    this.valueListService.readValueListValues(this.selectedEnvironment, this.selectedValueList.id).subscribe((attrs) => {
      this.bsValueListValues.next(attrs);
      this.loadingValueListValues = false;
    });
  }

  saveValueList() {
    this.valueListService.saveValueList(this.selectedEnvironment, this.selectedValueList).subscribe(
      () => {
        this.editMode.emit(false);
      },
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("administration.value-list.err-cannot-update-value-list"),
          error,
        ),
    );
  }

  addValueListValue() {
    this.tmpValueListValue = {
      value: `Value ${this.valueListValues ? this.valueListValues.length : ""}`,
    };

    this.valueListService.saveValueListValue(this.selectedEnvironment, this.tmpValueListValue, this.selectedValueList.id).subscribe(
      (response) => this.saveValueListValueSuccess(response.data),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("administration.value-list.err-cannot-create-value-list-value"),
          error,
        ),
    );
  }

  updateValueListValue(valueListValue: ValueListValue) {
    this.valueListService.saveValueListValue(this.selectedEnvironment, valueListValue, this.selectedValueList.id).subscribe();
  }

  saveValueListValueSuccess(nodes: Node[]) {
    console.log(nodes);
    for (const node of nodes) {
      if (node.name === this.tmpValueListValue.name && this.tmpValueListValue.id === undefined) {
        this.tmpValueListValue.id = node.id;
      }
    }
    this.valueListValues ? this.valueListValues.push(this.tmpValueListValue) : (this.valueListValues = [this.tmpValueListValue]);
    this.tmpValueListValue = undefined;
  }

  removeValueListValue(valueListValue: ValueListValue) {
    if (valueListValue.id !== undefined && parseInt(valueListValue.id, 10) > 0 && this.valueListValues.indexOf(valueListValue) !== -1) {
      this.valueListService.deleteValueListValue(this.selectedEnvironment, this.selectedValueList.id, valueListValue.id).subscribe();
      const idxES: number = this.valueListValues.indexOf(valueListValue);
      if (idxES !== -1) {
        this.valueListValues.splice(idxES, 1);
      }
    }
  }
}
