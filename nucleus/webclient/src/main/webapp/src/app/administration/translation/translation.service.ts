/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MdmLocalizationService } from "@localization/mdm-localization.service";
import { NodeArray } from "@navigator/node";
import { forkJoin, map, Observable } from "rxjs";

export class MdmLocalization {
  name: string;
  localizedNames: string[];
}

@Injectable({
  providedIn: "root",
})
export class TranslationService {
  defaultUrl = "/org.eclipse.mdm.nucleus/mdm/environments/";

  readonly httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json+packed",
    }),
  };

  constructor(private http: HttpClient, private mdmLocalizationService: MdmLocalizationService) {}

  /*getLanuages() {
    return this.t.getLanguages;
  }*/

  getTranslations(source: string): Observable<MdmLocalization[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/mdmlocalizations")
      .pipe(map((res) => res.data.map((node) => this.convertToMdmLocalization(node))));
  }

  loadTranslationsForCatalogComponent(
    source: string,
    contextType: string,
    catalogComponentName: string,
    attributeNames: string[],
  ): Observable<{
    languages: string[];
    componentName: string[];
    attributes: {
      [attr: string]: string[];
    };
  }> {
    const catComp1 = `application/x-asam.ao${contextType}part.${catalogComponentName}`.toLowerCase();

    return forkJoin({
      languages: this.mdmLocalizationService.getLanguages(),
      translations: this.mdmLocalizationService.loadTranslationMap(source),
    }).pipe(
      map(({ languages: languages, translations: t }) => {
        let translations = [catalogComponentName, catalogComponentName];
        if (t["mt"][catComp1]) {
          translations = t["mt"][catComp1] as string[];
        }

        let attributes = attributeNames.reduce((prev, attr) => {
          prev[attr] = [attr, attr];
          return prev;
        }, {} as { [attr: string]: string[] });

        if (t["attr"][catComp1]) {
          attributes = Object.entries(t["attr"][catComp1]).reduce((prev, [key, value]) => {
            console.log(key, value);
            prev[key] = value as string[];
            return prev;
          }, {} as { [attr: string]: string[] });
        }
        return {
          languages: languages,
          componentName: translations,
          attributes: attributes,
        };
      }),
    );
  }

  saveTranslationsForCatalogComponent(
    source: string,
    contextType: string,
    catalogComponentName: string,
    nameTranslations: string[],
    attributeTranslations: { [key: string]: string[] },
  ) {
    // CatalogComponentName:
    // mdm://i18n/mt/application%2Fx-asam.aounitundertestpart.vehicle
    const catComp1 = `application/x-asam.ao${contextType}part.${catalogComponentName}`.toLowerCase();
    // mdm://i18n/val/application%2Fx-asam.aounitundertestpart.vehicle/name/vehicle
    const catComp2 = `${catComp1}/name/${catalogComponentName}`.toLowerCase();

    // CatalogAttr:
    // mdm://i18n/attr/application%2Fx-asam.aounitundertestpart.vehicle/manufacturer
    const compAttr = `${catComp1}`;
    // mdm://i18n/val/application%2Fx-asam.aoany.catunitundertestattr/name/manufacturer
    const catAttr = `application/x-asam.aoany.cat${contextType}attr/name`.toLowerCase();

    const value = {
      mt: {
        [catComp1]: nameTranslations,
      },
      val: {
        [catComp2]: nameTranslations,
        [catAttr]: attributeTranslations,
      },
      attr: {
        [compAttr]: attributeTranslations,
      },
    };
    console.log(value);

    return this.http.post(this.defaultUrl + source + "/mdmlocalizations", value, this.httpOptions);
  }

  loadTranslationsForComponentTemplate(
    source: string,
    contextType: string,
    templateName: string,
    componentNames: { catalogName: string; componentName: string }[],
  ): Observable<{
    languages: string[];
    tplName: string;
    nameTranslations: string[];
    componentNames: {
      catalogName: string;
      componentName: string;
      translations: string[];
    }[];
  }> {
    return forkJoin({
      languages: this.mdmLocalizationService.getLanguages(),
      translations: this.mdmLocalizationService.loadTranslationMap(source),
    }).pipe(
      map(({ languages: languages, translations: t }) => {
        // Prüflings-Vorlage:
        // mdm://i18n/val/application%2Fx-asam.aoany.tplunitundertestroot/name/m01_tmp_uut
        const template = `application/x-asam.aoany.tpl${contextType}root`.toLowerCase();

        let translations = [templateName, templateName];
        if (
          t["val"] !== undefined &&
          t["val"][template] !== undefined &&
          t["val"][template]["name"] !== undefined &&
          t["val"][template]["name"][templateName.toLowerCase()] !== undefined
        ) {
          translations = t["val"][template]["name"][templateName.toLowerCase()] as string[];
        }

        const r = [];
        for (const e of componentNames) {
          // mdm://i18n/mt/application%2Fx-asam.aounitundertestpart.vehicle.myvehicle
          const tplComp1 = `application/x-asam.ao${contextType}part.${e.catalogName}.${e.componentName}`.toLowerCase();
          // mdm://i18n/val/application%2Fx-asam.aounitundertestpart.vehicle.myvehicle/name/myvehicle
          const tplComp2 = `${tplComp1}/name/${e.componentName}`.toLowerCase();

          if (t["mt"] !== undefined && t["mt"][tplComp1] !== undefined) {
            r.push({ catalogName: e.catalogName, componentName: e.componentName, translations: t["mt"][tplComp1] });
          } else if (
            t["val"] !== undefined &&
            t["val"][tplComp1] !== undefined &&
            t["val"][tplComp1]["name"] !== undefined &&
            t["val"][tplComp1]["name"][e.componentName.toLowerCase()] !== undefined
          ) {
            r.push({
              catalogName: e.catalogName,
              componentName: e.componentName,
              translations: t["val"][tplComp1]["name"][e.componentName.toLowerCase()],
            });
          } else {
            console.log("default");
            r.push({
              catalogName: e.catalogName,
              componentName: e.componentName,
              translations: [e.componentName, e.componentName],
            });
          }
        }

        return {
          languages: languages,
          tplName: templateName,
          nameTranslations: translations,
          componentNames: r,
        };
      }),
    );
  }

  saveTranslationsForComponentTemplate(
    source: string,
    contextType: string,
    templateName: string,
    nameTranslations: string[],
    componentNames: {
      catalogName: string;
      componentName: string;
      translations: string[];
    }[],
  ) {
    // Prüflings-Vorlage:
    // mdm://i18n/val/application%2Fx-asam.aoany.tplunitundertestroot/name/m01_tmp_uut
    const template = `application/x-asam.aoany.tpl${contextType}root/name/${templateName}`.toLowerCase();

    const value = {
      mt: {},
      val: {
        [template]: nameTranslations,
      },
    };

    // Prüflingskomponenten:
    for (const e of componentNames) {
      // mdm://i18n/mt/application%2Fx-asam.aounitundertestpart.vehicle.myvehicle
      const tplComp1 = `application/x-asam.ao${contextType}part.${e.catalogName}.${e.componentName}`.toLowerCase();
      // mdm://i18n/val/application%2Fx-asam.aounitundertestpart.vehicle.myvehicle/name/myvehicle
      const tplComp2 = `${tplComp1}/name/${e.componentName}`.toLowerCase();
      value["mt"][tplComp1] = e.translations;
      value["val"][tplComp2] = e.translations;
    }

    console.log(value);

    return this.http.post(this.defaultUrl + source + "/mdmlocalizations", value, this.httpOptions);
  }

  loadTranslationsForTestStepTemplate(source: string, templateName: string) {
    return forkJoin({
      languages: this.mdmLocalizationService.getLanguages(),
      translations: this.mdmLocalizationService.loadTranslationMap(source),
    }).pipe(
      map(({ languages: languages, translations: t }) => {
        // Messschritt-Vorlage:
        // mdm://i18n/val/application%2Fx-asam.aoany.tplteststep/name/pbn_unece_r51_run
        const template = `application/x-asam.aoany.tplteststep`.toLowerCase();

        let translations = [templateName, templateName];
        if (
          t["val"] !== undefined &&
          t["val"][template] !== undefined &&
          t["val"][template]["name"] !== undefined &&
          t["val"][template]["name"][templateName.toLowerCase()] !== undefined
        ) {
          translations = t["val"][template]["name"][templateName.toLowerCase()] as string[];
        }

        return {
          languages: languages,
          nameTranslations: translations,
        };
      }),
    );
  }

  saveTranslationsForTestStepTemplate(source: string, templateName: string, nameTranslations: string[]) {
    // Messschritt-Vorlage:
    // mdm://i18n/val/application%2Fx-asam.aoany.tplteststep/name/pbn_unece_r51_run
    const template = `application/x-asam.aoany.tplteststep/name/${templateName}`.toLowerCase();

    const value = {
      val: {
        [template]: nameTranslations,
      },
    };

    return this.http.post(this.defaultUrl + source + "/mdmlocalizations", value, this.httpOptions);
  }

  loadTranslationsForTestTemplate(source: string, templateName: string) {
    return forkJoin({
      languages: this.mdmLocalizationService.getLanguages(),
      translations: this.mdmLocalizationService.loadTranslationMap(source),
    }).pipe(
      map(({ languages: languages, translations: t }) => {
        // Versuchs-Vorlage:
        // mdm://i18n/val/application%2Fx-asam.aoany.tpltest/name/pbn_unece_r51
        const template = `application/x-asam.aoany.tpltest`.toLowerCase();

        let translations = [templateName, templateName];
        if (
          t["val"] !== undefined &&
          t["val"][template] !== undefined &&
          t["val"][template]["name"] !== undefined &&
          t["val"][template]["name"][templateName.toLowerCase()] !== undefined
        ) {
          translations = t["val"][template]["name"][templateName.toLowerCase()] as string[];
        }

        return {
          languages: languages,
          nameTranslations: translations,
        };
      }),
    );
  }

  saveTranslationsForTestTemplate(source: string, templateName: string, nameTranslations: string[]) {
    // Versuchs-Vorlage:
    // mdm://i18n/val/application%2Fx-asam.aoany.tpltest/name/pbn_unece_r51
    const template = `application/x-asam.aoany.tpltest/name/${templateName}`.toLowerCase();

    const value = {
      val: {
        [template]: nameTranslations,
      },
    };

    return this.http.post(this.defaultUrl + source + "/mdmlocalizations", value, this.httpOptions);
  }

  loadTranslationsForEnum(source: string, enumValueName: string) {
    return forkJoin({
      languages: this.mdmLocalizationService.getLanguages(),
      translations: this.mdmLocalizationService.loadTranslationMap(source),
    }).pipe(
      map(({ languages: languages, translations: t }) => {
        // mdm://i18n/val/application%2Fx-mdm.enumdef/name/axistype
        // mdm://i18n/enum/axistype/0
        const enumDef = "application/x-mdm.enumdef";

        let translations = [enumValueName, enumValueName];
        if (
          t["val"] !== undefined &&
          t["val"][enumDef] !== undefined &&
          t["val"][enumDef]["name"] !== undefined &&
          t["val"][enumDef]["name"][enumValueName.toLowerCase()] !== undefined
        ) {
          translations = t["val"][enumDef]["name"][enumValueName.toLowerCase()] as string[];
        }

        const valueTranslations = {} as {
          [value: string]: string[];
        };

        if (t["enum"] !== undefined && t["enum"][enumValueName.toLowerCase()] !== undefined) {
          for (const key of Object.keys(t["enum"][enumValueName.toLowerCase()])) {
            valueTranslations[key] = t["enum"][enumValueName.toLowerCase()][key] as string[];
          }
        }

        return {
          languages: languages,
          nameTranslations: translations,
          valueTranslations: valueTranslations,
        };
      }),
    );
  }

  saveTranslationsForEnum(source: string, enumName: string, trans: string[], enumValues: { [key: string]: string[] }) {
    // mdm://i18n/val/application%2Fx-mdm.enumdef/name/axistype
    // mdm://i18n/enum/axistype/0

    const enumNameKey = `application/x-mdm.enumdef/name/${enumName}`;
    const enumValuesKey = `${enumName}`;

    const value = {
      val: {
        [enumNameKey]: trans,
      },
      enum: {
        [enumValuesKey]: enumValues,
      },
    };
    console.log(value);

    return this.http.post(this.defaultUrl + source + "/mdmlocalizations", value, this.httpOptions);
  }

  convertToMdmLocalization(node) {
    return {
      name: node.attributes.find((a) => a.name === "Description").value,
      localizedNames: node.name,
    } as MdmLocalization;
  }
}
