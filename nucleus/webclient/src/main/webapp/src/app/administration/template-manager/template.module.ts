import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { TableModule } from "primeng/table";
import { TooltipModule } from "primeng/tooltip";
import { TreeModule } from "primeng/tree";
import { TreeTableModule } from "primeng/treetable";
import { TemplateAttributesComponent } from "./template-attributes/template-attributes.component";
import { TemplateComponentComponent } from "./template-component/template-component.component";
import { TemplateManagerViewerComponent } from "./template-manager-viewer/template-manager-viewer.component";
import { TemplateManagerComponent } from "./template-manager.component";
import { TemplateManagerService } from "./template-manager.service";
import { TemplateRoutingModule } from "./template-routing.module";
@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    TemplateRoutingModule,
    TreeModule,
    TreeTableModule,
    TooltipModule,
  ],
  declarations: [TemplateManagerComponent, TemplateComponentComponent, TemplateManagerViewerComponent, TemplateAttributesComponent],
  exports: [],
  providers: [TemplateManagerService],
})
export class TemplateModule {}
