/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { forkJoin } from "rxjs";
import { TranslationService } from "../../translation/translation.service";
import { Unit, UnitService } from "../../units/unit.service";
import { ValueList } from "../../value-list/model";
import { CatalogManagerService } from "../catalog-manager.service";
import { CatalogAttribute } from "../models/catalog-attribute.model";
import { CatalogComponent } from "../models/catalog-component.model";
import { DataType } from "../models/data-type.enum";

@Component({
  selector: "mdm-catalog-attributes",
  templateUrl: "./catalog-attributes.component.html",
  styleUrls: ["./catalog-attributes.component.css"],
})
export class CatalogAttributesComponent implements OnInit {
  selectedContextType: string;
  selectedEnvironment: string;
  selectedCatalogId: string;

  catalogComponent: CatalogComponent;
  catalogAttributes: CatalogAttribute[];
  selectedCatalogAttribute: CatalogAttribute;
  dialogCatalogAttributeCreate = false;
  dialogCatalogAttributeDelete = false;
  selectedCatalogAttributeId: string;

  newAttributeInstance = {
    Name: "",
    DataType: DataType.STRING,
  };

  dataTypes = Object.keys(DataType)
    .filter((d) => d !== DataType.BLOB && d !== DataType.UNKNOWN)
    .sort();

  valueLists: ValueList[];
  units: Unit[];

  currentlyEditedAttr: CatalogAttribute;

  showTranslations = false;
  translations: {
    languages: string[];
    componentName: string[];
    attributes: {
      [attr: string]: string[];
    };
  };

  constructor(
    private catalogManagerService: CatalogManagerService,
    private unitService: UnitService,
    private route: ActivatedRoute,
    private translationService: TranslationService,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.selectedContextType = this.route.snapshot.paramMap.get("contextType");
    this.selectedCatalogId = this.route.snapshot.paramMap.get("id");
    this.loadCatalogAttributes();
    this.getValueLists();
    this.getUnits();
  }

  /**
   * load catalog attributes
   */
  loadCatalogAttributes() {
    forkJoin([
      this.catalogManagerService.loadCatalogComponent(this.selectedEnvironment, this.selectedContextType, this.selectedCatalogId),
      this.catalogManagerService.loadCatalogAttributes(this.selectedEnvironment, this.selectedContextType, this.selectedCatalogId),
    ]).subscribe(([catComp, attrs]) => {
      this.catalogComponent = catComp;
      this.catalogAttributes = attrs;
    });
  }

  updateCatalogattributes(catAttrs) {
    this.catalogManagerService
      .updateCatalogAttributes(this.selectedEnvironment, this.selectedContextType, this.selectedCatalogId, catAttrs.id, {
        Description: catAttrs.description,
        ValueListRef: catAttrs.valueListRef,
        ValueCopyable: catAttrs.valueCopyable,
        Unit: catAttrs.unit,
      })
      .subscribe((response) => {
        this.loadCatalogAttributes();
      });
  }

  /**
   * delete attribute
   */
  openDeleteAttributeDialog(id: string) {
    this.dialogCatalogAttributeDelete = true;
    this.selectedCatalogAttributeId = id;
  }
  closeDeleteAttributeDialog() {
    this.dialogCatalogAttributeDelete = false;
  }
  deleteAttribute() {
    this.catalogManagerService
      .deleteCatalogAttribute(this.selectedEnvironment, this.selectedContextType, this.selectedCatalogId, this.selectedCatalogAttributeId)
      .subscribe((res) => {
        this.catalogAttributes = this.catalogAttributes.filter((item) => item.id !== this.selectedCatalogAttributeId);
      });
    this.dialogCatalogAttributeDelete = false;
  }

  /**
   *  create catalog attribute
   */
  openCreateAttributeDialog() {
    this.newAttributeInstance.Name = "";
    this.dialogCatalogAttributeCreate = true;
  }

  closeCreateAttributeDialog() {
    this.dialogCatalogAttributeCreate = false;
  }

  createCatalogAttribute() {
    this.dialogCatalogAttributeCreate = false;
    this.catalogManagerService
      .createCatalogAttribute(this.selectedEnvironment, this.selectedContextType, this.selectedCatalogId, this.newAttributeInstance)
      .subscribe(() => this.loadCatalogAttributes());
  }

  getValueLists() {
    this.catalogManagerService.getValueList(this.selectedEnvironment).subscribe((data) => {
      this.valueLists = data;
    });
  }
  /**
   * load all units
   */
  getUnits() {
    this.unitService.getUnitsForSource(this.selectedEnvironment).subscribe((res) => {
      this.units = res;
    });
  }

  /**
   * Edit attributes
   */
  onEditComplete(event) {
    if (event.data !== this.currentlyEditedAttr) {
      this.updateCatalogattributes(event.data);
    }
  }

  onAttrEditInit(event) {
    this.currentlyEditedAttr = { ...event.data };
  }

  onShowTranslations() {
    this.showTranslations = true;
    this.loadTranslations();
  }

  loadTranslations() {
    this.translationService
      .loadTranslationsForCatalogComponent(
        this.selectedEnvironment,
        this.selectedContextType,
        this.catalogComponent.name,
        this.catalogAttributes.map((ca) => ca.name),
      )
      .subscribe((t) => (this.translations = t));
  }

  saveTranslations() {
    this.translationService
      .saveTranslationsForCatalogComponent(
        this.selectedEnvironment,
        this.selectedContextType,
        this.catalogComponent.name,
        this.translations.componentName,
        this.translations.attributes,
      )
      .subscribe();
    this.showTranslations = false;
  }

  closeTranslationsDialog() {
    this.showTranslations = false;
  }
}
