/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { PropertyService } from "../../core/property.service";
import { Attribute, Node } from "../../navigator/node";

export class Quantity {
  // dimension: any;
  id: string; // hidden
  mqName: string; // hidden
  typeSize: number; // hidden
  rank: number; // hidden
  unitId: string;
  mt: string; // read only
  status: string;
  version: number;
  cd: Date; // read only
  dataType: string;
  desc: string;
  name: string;

  constructor() {
    this.mt = "application/x-asam.aoquantity";
    this.cd = null;
    this.name = "";
    this.mqName = "";
    this.typeSize = 1;
    this.rank = 1;
    this.dataType = "STRING";
    this.version = 1;
    this.status = "EDITABLE"; /* 0: EDITABLE, 1: VALID, 2: ARCHIVED */
    // this.dimension = new Array<number>();
    this.unitId = "";
  }
}

@Injectable()
export class QuantityService {
  private prefEndpoint: string;

  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json",
    }),
  };

  private dataTypes = new Array<string>(
    "STRING",
    "DATE",
    "BOOLEAN",
    "BYTE",
    "SHORT",
    "INTEGER",
    "LONG",
    "FLOAT",
    "DOUBLE",
    "BYTE_STREAM",
    "FLOAT_COMPLEX",
    "DOUBLE_COMPLEX",
    "ENUMERATION",
    "FILE_LINK",
    "BLOB",
  );

  private validStati = new Array<string>("EDITABLE", "VALID", "ARCHIVED");

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.prefEndpoint = _prop.getUrl("mdm/environments/");
  }

  getDataTypes(): string[] {
    return this.dataTypes;
  }

  getValidStati(): string[] {
    return this.validStati;
  }

  getQuantitiesForSource(source: string): Observable<Quantity[]> {
    return this.http.get(this.prefEndpoint + source + "/quantities").pipe(
      map((res) => ((res as any)?.hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request quantities!")),
    );
  }

  getQuantitiesForSource2(source: string): Observable<Quantity[]> {
    return this.http.get(this.prefEndpoint + source + "/quantities").pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request quantities!")),
    );
  }

  getQuantityForSource(source: string, id: string): Observable<Quantity[]> {
    return this.http.get(this.prefEndpoint + source + "/quantities/" + id).pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request quantities!")),
    );
  }

  deleteQuantityForSource(source: string, id: string) {
    return this.http
      .delete(this.prefEndpoint + source + "/quantities/" + id)
      .pipe(catchError((e) => addErrorDescription(e, "Could not request quantities!")));
  }

  updateQuantityForSource(source: string, quantity: Quantity): Observable<Quantity[]> {
    if (quantity.mqName === undefined || quantity.mqName.trim().length === 0) {
      quantity.mqName = quantity.name;
    }
    const requestData = new Object();
    requestData["Name"] = quantity.name;
    requestData["DefMQName"] = quantity.mqName;
    requestData["Unit"] = parseInt(quantity.unitId, 10);
    requestData["Description"] = this.isSet(quantity.desc) ? quantity.desc : "";
    requestData["Version"] = quantity.version.toString();
    requestData["DefDataType"] = quantity.dataType;
    requestData["ValidFlag"] = quantity.status;

    // these values currently cannot be edited
    // requestData['DefTypeSize'] = quantity.typeSize;
    // requestData['DefaultRank'] = quantity.rank;

    // makes no sense to let user edit MIME-Type, or create date
    // requestData['MimeType'] = quantity.mt;
    // requestData['DateCreated'] = quantity.cd;

    return this.http.put(this.prefEndpoint + source + "/quantities/" + quantity.id, JSON.stringify(requestData), this.httpOptions).pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request quantities!")),
    );
  }

  createQuantityForSource(source: string, quantity: Quantity): Observable<Quantity[]> {
    const requestData = new Object();
    requestData["Name"] = quantity.name;
    requestData["Unit"] = quantity.unitId;

    return this.http.post(this.prefEndpoint + source + "/quantities", JSON.stringify(requestData), this.httpOptions).pipe(
      map((res) => ((res as any).hasOwnProperty("data") ? ((res as any).data as []) : [])),
      map((res) => this.convert(res)),
      catchError((e) => addErrorDescription(e, "Could not request quantities!")),
    );
  }

  private convert(res: any[]): Quantity[] {
    const nodes = <Node[]>res;

    const retVal = new Array<Quantity>();

    for (const node of nodes) {
      retVal.push(this.convertFromNode(node));
    }
    return retVal;
  }

  private isSet(value: string): boolean {
    return value !== null && value !== undefined && value !== "";
  }

  private convertFromNode(qttNd: Node): Quantity {
    const q = new Quantity();
    q.id = qttNd.id;
    q.name = qttNd.name;

    const attribs = qttNd.attributes;

    q.desc = this.findAttrValue(attribs, "Description");
    const dateString = this.findAttrValue(attribs, "DateCreated");
    q.cd = dateString === undefined ? undefined : new Date(dateString);
    q.mt = this.findAttrValue(attribs, "MimeType");
    q.dataType = this.findAttrValue(attribs, "DefDataType");
    q.status = this.findAttrValue(attribs, "ValidFlag");
    q.mqName = this.findAttrValue(attribs, "DefMQName");

    const ts = this.findAttrValue(attribs, "DefTypeSize");
    if (this.isSet(ts)) {
      q.typeSize = parseInt(ts, 10);
    }

    const vers = this.findAttrValue(attribs, "Version");
    if (this.isSet(vers)) {
      q.version = parseInt(vers, 10);
    }

    const rnk = this.findAttrValue(attribs, "DefaultRank");
    if (this.isSet(rnk)) {
      q.rank = parseInt(rnk, 10);
    }

    q.unitId = this.findRelId(qttNd);

    return q;
  }

  private findAttrValue(attribs: Attribute[], attrName: string): string {
    const attr = attribs.find((a) => a.name === attrName);

    return attr !== undefined ? attr.value.toString() : null;
  }

  private findRelId(qttNd: Node): string {
    const rel = qttNd.relations.find((r) => r.entityType === "Unit");

    return rel !== undefined ? rel.ids[0] : null;
  }
}
