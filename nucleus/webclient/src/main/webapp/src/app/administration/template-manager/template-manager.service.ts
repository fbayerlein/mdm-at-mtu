import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Node, NodeArray } from "@navigator/node";
import { map, Observable } from "rxjs";
import { CatalogAttribute } from "../catalog-manager/models/catalog-attribute.model";
import { TemplateAttribute } from "./models/template-attribute.model";
import { TemplateComponent } from "./models/template-component.model";
import { TemplateRoot } from "./models/template-root.model";
@Injectable()
export class TemplateManagerService {
  defaultUrl: string = "/org.eclipse.mdm.nucleus/mdm/environments/";

  constructor(private http: HttpClient) {}

  getTemplateRoots(source: string, contextType: string): Observable<TemplateRoot[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplroots/" + contextType)
      .pipe(map((res) => res.data.map((data) => this.convertToTemplateRoot(data))));
  }
  convertToTemplateRoot(node) {
    return {
      description: node.attributes.find((a) => a.name === "Description").value,
      name: node.name,
      id: node.id,
      type: node.type,
      dateCreated: node.attributes.find((a) => a.name === "DateCreated").value,
      validFlag: node.attributes.find((a) => a.name === "ValidFlag").value,
      version: node.attributes.find((a) => a.name === "Version").value,
      tplComps: node.tplComps,
    } as TemplateRoot;
  }
  getTemplateRootById(source: string, contextType: string, tplRootId: string): Observable<TemplateRoot[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId)
      .pipe(map((res) => res.data.map((data) => this.convertToTemplateRoot(data))));
  }

  getTemplateComponents(source: string, contextType: string, tplRootId: string): Observable<TemplateComponent[]> {
    return this.http.get<NodeArray>(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/").pipe(
      map((res) => {
        if (res) {
          return res.data?.map((data) => this.convertToTemplateComponent(data));
        } else {
          return [] as TemplateComponent[];
        }
      }),
    );
  }

  convertToTemplateComponent(node: Node) {
    return {
      id: node.id,
      name: node.name,
      //catalogComponent: node.catalogComponent,
      //type: node.type,
      optional: node.attributes.find((a) => a.name === "Optional").value === "true",
      defaultActive: node.attributes.find((a) => a.name === "DefaultActive").value === "true",
      testStepSeriesVariable: node.attributes.find((a) => a.name === "TestStepSeriesVariable").value === "true",
      attributes: node.attributes,
      relations: node.relations,
    } as TemplateComponent;
  }

  getTemplateComponentsById(source: string, contextType: string, tplRootId: string, tplCompId: string): Observable<TemplateComponent[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId)
      .pipe(map((res) => res.data.map((data) => this.convertToTemplateComponent(data))));
  }
  updateTemplate(source: string, contextType: string, tplRootId: string, template: any) {
    return this.http.put(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId, template);
  }
  getTemplateAttrsByComponentId(
    source: string,
    contextType: string,
    tplRootId: string,
    tplCompId: string,
  ): Observable<TemplateAttribute[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId + "/tplattrs")
      .pipe(map((res) => res.data.map((node) => this.convertToTemplateAttributes(node))));
  }
  getTemplateAttrsByNestedComponentId(
    source: string,
    contextType: string,
    tplRootId: string,
    compId: string,
    TplcompChildId: string,
  ): Observable<TemplateAttribute[]> {
    return this.http
      .get<NodeArray>(
        this.defaultUrl +
          source +
          "/tplroots/" +
          contextType +
          "/" +
          tplRootId +
          "/tplcomps/" +
          compId +
          "/tplcomps/" +
          TplcompChildId +
          "/tplattrs",
      )
      .pipe(map((res) => res.data.map((node) => this.convertToTemplateAttributes(node))));
  }
  getTplComponentsByNestedComponentId(
    source: string,
    contextType: string,
    tplRootId: string,
    TplCompId: string,
    TplCompChildId: string,
  ): Observable<TemplateComponent[]> {
    return this.http
      .get<NodeArray>(
        this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + TplCompId + "/tplcomps/" + TplCompChildId,
      )
      .pipe(map((res) => res.data.map((data) => this.convertToTemplateComponent(data))));
  }
  convertToTemplateAttributes(templateAttrs) {
    return {
      id: templateAttrs.id,
      defaultValue: templateAttrs.attributes.find((a) => a.name === "DefaultValue").value,
      valueReadonly: templateAttrs.attributes.find((a) => a.name === "ValueReadonly").value,
      obligatory: templateAttrs.attributes.find((a) => a.name === "Obligatory").value,
      mimeType: templateAttrs.attributes.find((a) => a.name === "MimeType").value,
      relations: templateAttrs.relations,
      name: templateAttrs.attributes.find((a) => a.name === "Name").value,
    } as TemplateAttribute;
  }
  getCatalogComponentById(source: string, contextType: string, tplRootId: string): Observable<Node> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/catcomps/" + contextType + "/" + tplRootId)
      .pipe(map((res) => res.data[0]));
  }
  getCatalogComponents(source: string, contextType: string): Observable<Node[]> {
    return this.http.get<NodeArray>(this.defaultUrl + source + "/catcomps/" + contextType).pipe(map((res) => res.data));
  }
  getCatalogAttributes(source: string, contextType: string, catCompId: string) {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/catcomps/" + contextType + "/" + catCompId + "/catattrs")
      .pipe(map((res) => res.data.map((node) => this.convertToCatalogAttributes(node))));
  }
  convertToCatalogAttributes(node: Node) {
    return {
      id: node.id,
      dataType: node.attributes.find((a) => a.name === "DataType").value,
      unit: node.attributes.find((a) => a.name === "Unit").value,
      relations: node.relations,
      name: node.attributes.find((a) => a.name === "Name").value,
      mimeType: node.attributes.find((a) => a.name === "MimeType").value,
      sortIndex: node.attributes?.find((a) => a.name === "Sortindex").value,
    } as CatalogAttribute;
  }
  updateTplComponent(source: string, contextType: string, tplRootId: string, tplCompId: string, tpl: any) {
    return this.http.put(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId, tpl);
  }
  updateTplComponentChild(source: string, contextType: string, tplRootId: string, tplCompId: string, tplCompChildId: string, tpl: any) {
    return this.http.put(
      this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId + "/tplcomps/" + tplCompChildId,
      tpl,
    );
  }
  updateTplComponentAttrs(source: string, contextType: string, tplRootId: string, tplCompId: string, tplAttrId: string, tpl: any) {
    return this.http.put(
      this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId + "/tplattrs/" + tplAttrId,
      tpl,
    );
  }
  updateTplComponentChildAttrs(
    source: string,
    contextType: string,
    tplRootId: string,
    tplCompId: string,
    tplChildCompId: string,
    tplAttrId: string,
    tpl: any,
  ) {
    return this.http.put(
      this.defaultUrl +
        source +
        "/tplroots/" +
        contextType +
        "/" +
        tplRootId +
        "/tplcomps/" +
        tplCompId +
        "/tplcomps/" +
        tplChildCompId +
        "/tplattrs/" +
        tplAttrId,
      tpl,
    );
  }
  createTplRoot(source: string, contextType: string, tpl: any) {
    return this.http.post(this.defaultUrl + source + "/tplroots/" + contextType, tpl);
  }
  deleteAttribute(source: string, contextType: string, tplRootId: string, tplCmpId: string, attrId: string) {
    return this.http.delete(
      this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCmpId + "/tplattrs/" + attrId,
    );
  }
  createAttribute(source: string, contextType: string, tplRootId: string, tplCmpId: string, tplAttr: any) {
    return this.http.post(
      this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCmpId + "/tplattrs",
      tplAttr,
    );
  }
  createNewComponentForTemplateRoot(source: string, contextType: string, tplRootId: string, templateComp: any) {
    return this.http.post(`${this.defaultUrl}${source}/tplroots/${contextType}/${tplRootId}/tplcomps/`, templateComp);
  }
  createNewComponentForTemplateComp(source: string, contextType: string, tplRootId: string, tplCompId: string, templateComp: any) {
    return this.http.post(`${this.defaultUrl}${source}/tplroots/${contextType}/${tplRootId}/tplcomps/${tplCompId}/tplcomps/`, templateComp);
  }
  getTplComponentChildren(source: string, contextType: string, tplRootId: string, TplCompId: string): Observable<TemplateComponent[]> {
    return this.http
      .get<NodeArray>(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + TplCompId + "/tplcomps")
      .pipe(map((res) => res?.data.map((data) => this.convertToTemplateComponent(data))));
  }
  deleteTemplateRoot(source: string, contextType: string, tplRootId: string) {
    return this.http.delete(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId);
  }
  deleteTplComp(source: string, contextType: string, tplRootId: string, tplCompId: string) {
    return this.http.delete(this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId);
  }
  deleteTplCompChild(source: string, contextType: string, tplRootId: string, tplCompId: string, tplCompChildId: string) {
    return this.http.delete(
      this.defaultUrl + source + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId + "/tplcomps/" + tplCompChildId,
    );
  }
}
