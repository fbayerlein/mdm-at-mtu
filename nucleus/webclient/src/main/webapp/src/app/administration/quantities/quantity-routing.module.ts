import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { QuantityComponent } from "./quantity.component";

const QuantityRoutes: Routes = [
  {
    path: "",
    component: QuantityComponent,
    children: [],
  },
];
@NgModule({
  imports: [RouterModule.forChild(QuantityRoutes)],
  exports: [RouterModule],
})
export class QuantityRoutingModule {}
