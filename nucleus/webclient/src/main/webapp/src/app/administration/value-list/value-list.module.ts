import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { OrderListModule } from "primeng/orderlist";
import { TableModule } from "primeng/table";

import { ValueListEditorComponent } from "./value-list-editor.component";
import { ValueListRoutingModule } from "./value-list-routing.module";
import { ValueListViewerComponent } from "./value-list-viewer.component";
import { ValueListComponent } from "./value-list.component";
import { ValueListService } from "./valuelist.service";
@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ContextMenuModule,
    ValueListRoutingModule,
    OrderListModule,
  ],
  declarations: [ValueListComponent, ValueListViewerComponent, ValueListEditorComponent],
  exports: [],
  providers: [ValueListService],
})
export class ValueListModule {}
