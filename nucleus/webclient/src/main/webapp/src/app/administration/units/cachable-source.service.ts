/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { map, Observable, ReplaySubject, Subject } from "rxjs";

export class DataHolder<T> {
  data: T;
  subject: Subject<T>;
  observable: Observable<T>;

  constructor() {
    this.subject = new ReplaySubject(1);
    this.observable = this.subject.asObservable();
  }

  public setNext() {
    this.subject.next(this.data);
  }
}
export abstract class CachableSourceService<T> {
  protected cachedSources: Map<string, DataHolder<T>> = new Map();

  protected abstract requestDataForSource(source: string): Observable<T>;

  protected getDataForSource(source: string): Observable<T> {
    let dataHolder: DataHolder<T> = undefined;
    if (!this.cachedSources.has(source)) {
      dataHolder = new DataHolder();
      this.requestDataForSource(source)
        .pipe(
          map((value) => {
            dataHolder.data = value;
            return value;
          }),
        )
        .subscribe(
          (next) => {
            dataHolder.subject.next(next);
            this.cachedSources.set(source, dataHolder);
          },
          (error) => {
            dataHolder.subject.error(error);
            this.cachedSources.set(source, dataHolder);
          },
        );
    } else {
      dataHolder = this.cachedSources.get(source);
    }
    return dataHolder.observable;
  }

  public resetCache(): void {
    this.cachedSources.clear();
  }
}
