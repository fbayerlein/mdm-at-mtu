/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FunctionalRightConfig, JsonValue } from "@authentication/model";
import { ConditionParserService } from "@core/authentication/condition-parser/condition-parser.service";
import { FunctionalRight } from "@core/authentication/model/functional-right.type";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { TranslateService } from "@ngx-translate/core";
import { TreeNode } from "primeng/api";
import { forkJoin } from "rxjs";
import { RightsConfigService } from "./rights-config.service";

@Component({
  selector: "mdm-rights-config",
  templateUrl: "rights-config.component.html",
  styleUrls: ["rights-config.component.css"],
})
export class RightsConfigComponent implements OnInit {
  public config: FunctionalRightConfig = {};
  public treeData: TreeNode<FunctionalRight | string>[] = [];

  constructor(
    private router: Router,
    private translateService: TranslateService,
    private rightsConfigService: RightsConfigService,
    private conditionParser: ConditionParserService,
    private notificationService: MDMNotificationService,
  ) {}

  ngOnInit(): void {
    forkJoin([this.rightsConfigService.readDefaultConfig(), this.rightsConfigService.readConfig()]).subscribe(([defaultConfig, config]) => {
      this.config = this.rightsConfigService.deepMerge({}, defaultConfig, config);
      this.treeData = this.transformToTree(this.config.functionalRights, "");
    });
  }

  public save() {
    try {
      const newConfig = this.transformToJson();

      this.rightsConfigService
        .saveConfig(newConfig)
        .subscribe(() =>
          this.notificationService.notifyInfo("Configuration saved.", "Functional rights configuration saved successfully."),
        );
    } catch (err: unknown) {
      const error = err as Error;
      this.notificationService.notifyWarn("Could not save functional rights.", error.message);
    }
  }

  private isFunctionalRight(right: JsonValue<FunctionalRight>): right is FunctionalRight {
    return right != null && Object.prototype.hasOwnProperty.call(right, "condition");
  }

  private transformToTree(functionalRights: JsonValue<FunctionalRight>, prefix: string): TreeNode<FunctionalRight | string>[] {
    const children = [] as TreeNode<FunctionalRight | string>[];

    if (!functionalRights) {
      return children;
    }

    for (const key of Object.keys(functionalRights)) {
      const fullKey = prefix === "" ? key : prefix + "." + key;

      if (this.isFunctionalRight(functionalRights[key])) {
        children.push({
          label: key,
          //leaf: true,
          key: fullKey,
          type: "functionalRight",
          data: {
            condition: functionalRights[key]["condition"],
            description: functionalRights[key]["description"],
            example: functionalRights[key]["example"],
          } as FunctionalRight,
        } as TreeNode<FunctionalRight>);
      } else {
        children.push({
          label: key,
          //leaf: false,
          key: fullKey,
          type: "keyFragment",
          data: key,
          children: this.transformToTree(functionalRights[key], prefix === "" ? key : prefix + "." + key),
        } as TreeNode<string>);
      }
    }

    return children;
  }

  private transformToJson() {
    return {
      default: this.config.default,
      functionalRights: this.treeToJson(this.treeData),
    } as FunctionalRightConfig;
  }

  private treeToJson(treeData: TreeNode<string | FunctionalRight>[]): JsonValue<FunctionalRight> {
    const r = {} as JsonValue<FunctionalRight>;
    for (const node of treeData) {
      if (node.type === "keyFragment") {
        r[node.data as string] = this.treeToJson(node.children);
      } else if (node.type === "functionalRight") {
        r[node.label] = node.data as FunctionalRight;
        if (r[node.label].invalid) {
          throw new Error("Condition invalid. " + node.key + ": " + r[node.label].errorMessage);
        }
      }
    }

    return r;
  }

  setCondition($event, rowData) {
    try {
      this.conditionParser.parse($event);
      rowData.condition = $event;
      rowData.invalid = false;
      rowData.errorMessage = undefined;
    } catch (e: unknown) {
      rowData.invalid = true;
      if (e["pos"]) {
        rowData.errorMessage = `'${e["errorMessage"]}' at Position ${e["pos"]["index"]}`;
      } else {
        rowData.errorMessage = e["errorMessage"];
      }
    }
  }
}
