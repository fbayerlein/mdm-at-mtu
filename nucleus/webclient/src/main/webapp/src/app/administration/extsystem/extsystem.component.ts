/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject } from "rxjs";
import { MDMNotificationService } from "../../core/mdm-notification.service";
import { Node } from "../../navigator/node";
import { NodeService } from "../../navigator/node.service";
import { ExtSystemService } from "./extsystem.service";

@Component({
  selector: "mdm-extsystem",
  templateUrl: "./extsystem.component.html",
  styleUrls: ["./extsystem.component.css"],
})
export class ExtSystemComponent implements OnInit {
  // available external systems
  extSystems: Node[];
  bsExtSystems: BehaviorSubject<Node[]> = new BehaviorSubject<Node[]>(undefined);
  selectedEnvironment: string;

  editorMode = false;
  selectedExtSystem: string = undefined;

  // dropdown selection
  environments: Node[];

  //  unknown -> remove
  scope: string;

  constructor(
    private extSystemService: ExtSystemService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private nodeService: NodeService,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.getExtSystems();
  }

  getExtSystems() {
    this.extSystemService.getExtSystemForScope(this.selectedEnvironment).subscribe((res) => {
      this.extSystems = res;
    });
  }

  onEditModeChange(editMode: boolean) {
    this.editorMode = editMode;
  }

  onChangeSelectedExtSystem(extSystem: string) {
    this.selectedExtSystem = extSystem;
  }

  onChangeExtSystem(event: any) {
    this.extSystems = undefined;
    if (this.selectedEnvironment !== undefined) {
      this.scope = this.selectedEnvironment;
    }
    this.extSystemService.getExtSystemForScope(this.scope).subscribe((es) => this.bsExtSystems.next(es));
  }

  onPageSwitchToEdit() {
    this.onEditModeChange(true);
  }

  onPageBack() {
    this.onEditModeChange(false);
  }
}
