/********************************************************************************
 * Copyright (c) 2015-2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, InjectionToken } from "@angular/core";
import { FunctionalRightConfig } from "@authentication/model";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { MessageService } from "primeng/api";
import { forkJoin, map, Observable, shareReplay } from "rxjs";

export const FUNCTIONAL_RIGHTS_URLS = new InjectionToken<string>("functionalRights");

@Injectable()
export class RightsConfigService {
  private static readonly PREF_KEY = "rights_config";

  constructor(
    private http: HttpClient,
    private preferenceService: PreferenceService,
    private messenger: MessageService,
    private prop: PropertyService,
    @Inject(FUNCTIONAL_RIGHTS_URLS) private functionalRightsUrls: string[],
  ) {}

  readDefaultConfig(): Observable<FunctionalRightConfig> {
    return forkJoin(this.functionalRightsUrls.map((url) => this.http.get(url))).pipe(map((results) => this.deepMerge({}, ...results)));
  }

  readConfig(): Observable<FunctionalRightConfig> {
    return this.preferenceService.getPreference(RightsConfigService.PREF_KEY).pipe(
      map((prefs) => this.parsePref(prefs)),
      shareReplay(1),
    );
  }

  saveConfig(newConfig: FunctionalRightConfig) {
    return this.preferenceService.savePreference(this.configToPreference(newConfig));
  }

  isObject(item: any) {
    return item && typeof item === "object" && !Array.isArray(item);
  }

  deepMerge(target: any, ...sources: any) {
    if (!sources.length) {
      return target;
    }
    const source = sources.shift();

    if (this.isObject(target) && this.isObject(source)) {
      for (const key in source) {
        if (this.isObject(source[key])) {
          if (!target[key]) {
            Object.assign(target, { [key]: {} });
          }
          this.deepMerge(target[key], source[key]);
        } else {
          Object.assign(target, { [key]: source[key] });
        }
      }
    }

    return this.deepMerge(target, ...sources);
  }

  private configToPreference(config: FunctionalRightConfig) {
    const pref = new Preference();
    pref.value = JSON.stringify(config, undefined, 2);
    pref.key = RightsConfigService.PREF_KEY;
    pref.scope = Scope.SYSTEM;
    return pref;
  }

  private parsePref(prefs: Preference[]): FunctionalRightConfig {
    try {
      return prefs?.[0] ? JSON.parse(prefs[0].value) : { default: true };
    } catch {
      this.messenger.add({
        severity: "warn",
        summary: "Rights config preference is not parsable.",
      });
      return {};
    }
  }
}
