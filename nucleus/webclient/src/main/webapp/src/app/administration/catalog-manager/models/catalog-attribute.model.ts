import { MDMLinkExt } from "@navigator/node";
import { DataType } from "./data-type.enum";

export class CatalogAttribute {
  id: string;
  name: string;
  unit?: string;
  dataType: DataType;
  sortIndex: string;
  description?: string;
  valueCopyable?: string;
  valueListRef?: string;
  actionRequestClassname?: string;
  actionRequestParameter?: string;
  mimeType: string;
  value?: string | string[] | MDMLinkExt | MDMLinkExt[] | MDMLinkExt[][];
}
