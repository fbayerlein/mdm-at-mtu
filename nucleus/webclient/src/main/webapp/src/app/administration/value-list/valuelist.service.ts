/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { getValue } from "@core/utils/node-utils";
import { Node, NodeArray } from "@navigator/node";
import { catchError, map, mergeMap as concatMap, Observable, of } from "rxjs";
import { ValueList, ValueListValue } from "./model";

@Injectable({
  providedIn: "root",
})
export class ValueListService {
  private resourcePath: (sourceName: string) => string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.resourcePath = (sourceName) => this._prop.getUrl("mdm/environments/") + sourceName + "/valuelists";
  }

  /**
   * Reads {@link ValueList}.
   *
   * @param source the source name
   * @param valueListId id of the value list
   * @param includeValues flag to include(= true)/exclude(=false) value list values
   * @returns the value list
   */
  public readValueList(source: string, valueListId: string, includeValues: boolean) {
    const path = this.resourcePath(source) + "/" + valueListId;
    return this.http.get<NodeArray>(path).pipe(
      map((response) => this.toValueList(response.data[0])),
      concatMap((valueList) => this.addValueListValues(valueList, includeValues)),
      catchError((e) => addErrorDescription(e, "Could not request ValueList!")),
    );
  }

  /**
   * Reads all {@link ValueList}s.
   *
   * @param source the source name
   * @param name the value list name
   * @returns the value list
   */
  public readValueLists(source: string, params?: HttpParams) {
    return this.http.get<NodeArray>(this.resourcePath(source), { params }).pipe(
      map((response) => response.data.map((vl) => this.toValueList(vl))),
      catchError((e) => addErrorDescription(e, "Could not request ValueList by name!")),
    );
  }

  /**
   * Reads {@link ValueList} by name.
   * Assumes ValueList name is unique.
   *
   * @param source the source name
   * @param name the value list name
   * @param includeValues flag to include(= true)/exclude(=false) value list values
   * @returns the value list
   */
  public readValueListByName(source: string, name: string, includeValues: boolean) {
    const params = new HttpParams().set("filter", "ValueList.Name eq '" + name + "'");
    return this.readValueLists(source, params).pipe(
      map((valueLists) => valueLists[0]),
      concatMap((valueList) => this.addValueListValues(valueList, includeValues)),
    );
  }

  /**
   * Reads {@link ValueListValue}s for the {@link ValueList} and writes them into the value list.
   *
   * @param valueList the valeu list
   * @param includeValues flag to include(= true)/exclude(=false) value list values
   * @returns the value list with values
   */
  private addValueListValues(valueList: ValueList, includeValues: boolean) {
    return (includeValues && valueList?.id ? this.readValueListValues(valueList.sourceName, valueList.id) : of(undefined)).pipe(
      map((values) => {
        valueList.values = values;
        return valueList;
      }),
    );
  }

  /**
   * Reads {@link ValueListValue}s for the {@link ValueList} with given id.
   *
   * @param source the source name
   * @param valueListId primary key of the value list
   * @returns value list values
   */
  public readValueListValues(source: string, valueListId: string): Observable<ValueListValue[]> {
    const path = this.resourcePath(source) + "/" + valueListId + "/values";
    return this.http.get<NodeArray>(path).pipe(
      map((response) => this.toValueListValues(response?.data)),
      catchError((e) => addErrorDescription(e, "Could not read ValueListValues!")),
    );
  }

  /**
   * Creates or updates {@link ValueList}
   *
   * @param sourceName name of the source
   * @param body the new value list dto
   * @returns the new value list object
   */
  public saveValueList(sourceName: string, valueList: ValueList) {
    const path = this.resourcePath(sourceName);

    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    const structure = {};
    structure["Name"] = valueList.name;
    structure["Description"] = valueList.description;

    if (parseInt(valueList.id, 10) > 0) {
      // update
      return this.http.put<NodeArray>(path + "/" + valueList.id, JSON.stringify(structure), options).pipe(
        map((res) => this.toValueList(res?.data?.[0])),
        catchError((e) => addErrorDescription(e, "Could not create ValueList!")),
      );
    } else {
      structure["ValueList"] = valueList;
      // create
      return this.http.post<NodeArray>(path, JSON.stringify(structure), options).pipe(
        map((res) => this.toValueList(res?.data?.[0])),
        catchError((e) => addErrorDescription(e, "Could not create ValueList!")),
      );
    }
  }

  /**
   * Removes {@link ValueList}
   *
   * @param sourceName name of the source
   * @param valueListId the new value list id
   * @returns nothing
   */
  public deleteValueList(sourceName: string, valueListId: string) {
    const path = this.resourcePath(sourceName) + "/" + valueListId;
    return this.http.delete<NodeArray>(path).pipe(catchError((e) => addErrorDescription(e, "Could not delete ValueList!")));
  }

  /**
   * Creates or updates {@link ValueListValue}
   *
   * @param sourceName name of the source
   * @param valueListValue the new value list value dto
   * @param valueListId value list id
   * @returns the new value list value object
   */
  saveValueListValue(sourceName: string, valueListValue: ValueListValue, valueListId: string) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    const structure = {};
    structure["Name"] = valueListValue.value;
    structure["Value"] = valueListValue.value;

    if (parseInt(valueListValue.id, 10) > 0) {
      structure["Description"] = valueListValue.description;
      // update
      return this.http
        .put<any>(this.resourcePath(sourceName) + "/" + valueListId + "/values/" + valueListValue.id, JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    } else {
      console.log(structure);
      structure["ValueListValue"] = valueListValue;
      // only provide the ID as the backend will evaluate the whole json string
      structure["ValueList"] = valueListId;
      // create
      return this.http
        .post<any>(this.resourcePath(sourceName) + "/" + valueListId + "/values", JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    }
  }

  /**
   * Creates {@link ValueListValue}s for the {@link ValueList} with given id.
   *
   * @param sourceName name of the source
   * @param valueListId Id of the value list
   * @param body the new value list value dto
   * @returns the new value list value object
   */
  public createValueListValue(sourceName: string, valueListId: string, body: { Value: string }) {
    const path = this.resourcePath(sourceName) + "/" + valueListId + "/values";
    return this.http.post<NodeArray>(path, body).pipe(
      map((res) => this.toValueListValue(res?.data?.[0])),
      catchError((e) => addErrorDescription(e, "Could not create ValueListValue!")),
    );
  }

  /**
   * Updates {@link ValueListValue}s with given Id for the {@link ValueList} with given id.
   *
   * @param sourceName name of the source
   * @param valueListId Id of the value list
   * @param valueListValueId Id of the value list value
   * @param body the new value list value dto
   * @returns the updated value list value object
   */
  public updateValueListValue(sourceName: string, valueListId: string, valueListValueId: string, body: { Value: string }) {
    const path = this.resourcePath(sourceName) + "/" + valueListId + "/values/" + valueListValueId;
    return this.http.put<NodeArray>(path, body).pipe(
      map((res) => this.toValueListValue(res?.data?.[0])),
      catchError((e) => addErrorDescription(e, "Could not update ValueListValue!")),
    );
  }

  /**
   * Deletes {@link ValueListValue}s with given Id for the {@link ValueList} with given id.
   *
   * @param sourceName name of the source
   * @param valueListId Id of the value list
   * @param valueListValueId Id of the value list value
   * @returns the deleted value list value object
   */
  public deleteValueListValue(sourceName: string, valueListId: string, valueListValueId: string) {
    const path = this.resourcePath(sourceName) + "/" + valueListId + "/values/" + valueListValueId;
    return this.http.delete<NodeArray>(path).pipe(
      map((res) => this.toValueListValue(res?.data?.[0])),
      catchError((e) => addErrorDescription(e, "Could not delete ValueListValue!")),
    );
  }

  /**
   * Maps node to {@link ValueList} (without value list values)
   *
   * @param node node describing the value list
   * @returns value list object
   */
  private toValueList(node: Node) {
    if (node) {
      const valueList: ValueList = {};
      valueList.id = node.id;
      valueList.name = node.name;
      valueList.sourceName = node.sourceName;
      valueList.sourceType = node.sourceType;
      valueList.type = node.type;
      valueList.description = getValue(node, "Description");
      valueList.mimeType = getValue(node, "MimeType");
      return valueList;
    }
  }

  /**
   * Maps nodes to {@link ValueListValue}s
   *
   * @param nodes nodes describing the value list values
   * @returns array of value list value objects
   */
  private toValueListValues(nodes: Node[]) {
    return nodes?.map((node) => this.toValueListValue(node));
  }

  /**
   * Maps node to {@link ValueListValue}
   *
   * @param node node describing the value list value
   * @returns value list value object
   */
  private toValueListValue(node: Node) {
    if (node) {
      const valueListValue: ValueListValue = {};
      valueListValue.id = node.id;
      valueListValue.value = node.name;
      valueListValue.sourceName = node.sourceName;
      valueListValue.sourceType = node.sourceType;
      valueListValue.type = node.type;
      valueListValue.dataType = getValue(node, "DataType");
      valueListValue.description = getValue(node, "Description");
      valueListValue.mimeType = getValue(node, "MimeType");
      valueListValue.value = getValue(node, "Value");
      return valueListValue;
    }
  }
}
