import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TestTemplateComponent } from "./test-template-component/test-template.component";
import { TestTemplateDetailComponent } from "./test-template-detail/test-template-detail.component";

import { TestTemplateViewerComponent } from "./test-template-viewer/test-template-viewer.component";

const TestTemplateRoutes: Routes = [
  {
    path: "",
    component: TestTemplateViewerComponent,
    children: [
      {
        path: "",
        component: TestTemplateComponent,
        data: { breadcrumb: "Test template" },
      },
      {
        path: ":selectedTestTplName/:selectedTestTplId",
        component: TestTemplateDetailComponent,
        data: { breadcrumb: ":selectedTestTplName" },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(TestTemplateRoutes)],
  exports: [RouterModule],
})
export class TestTemplateRoutingModule {}
