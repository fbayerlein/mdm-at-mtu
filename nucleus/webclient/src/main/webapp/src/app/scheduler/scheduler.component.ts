/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { LazyLoadEvent, SelectItem } from "primeng/api";
import { BehaviorSubject } from "rxjs";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { JobInfo, SchedulerService } from "./scheduler.service";

@Component({
  selector: "importscheduler-scheduler",
  templateUrl: "./scheduler.component.html",
  styleUrls: ["./scheduler.component.css"],
  providers: [DatePipe],
})
export class SchedulerComponent implements OnInit, OnDestroy {
  // available external systems
  jobs: JobInfo[];
  bsJobs: BehaviorSubject<JobInfo[]> = new BehaviorSubject<JobInfo[]>(undefined);

  // table selection
  cols: any;
  jobStates: SelectItem[];
  totalRecords: number;
  loading = false;
  rowsPerPage = 100;
  currentPage = 0;
  filterJobState: string;
  filterStateFile: string;
  filterDateFrom: Date;
  filterDateTo: Date;

  // dialog and loading states
  dialogDelete = false;
  dialogReschedule = false;

  // for the confirmation dialog
  tmpDelete: JobInfo;
  tmpReschedule: JobInfo;

  // overlay text to display
  overlayText: string;
  overlayTitle: string;
  showInfoDialog = false;

  dialogRescheduleMultiple = false;
  rescheduleDays: number;

  constructor(
    private schedulerService: SchedulerService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private datepipe: DatePipe,
  ) {
    this.bsJobs.subscribe((value) => {
      this.jobs = value;
    });
  }

  ngOnInit() {
    this.bsJobs.next(undefined);

    this.cols = [
      { field: "jobObject", header: "importscheduler.scheduler.stateFile", width: "30%" },
      { field: "fileDate", header: "importscheduler.scheduler.fileDate", width: "10%" },
      { field: "jobState", header: "importscheduler.scheduler.jobState", width: "10%" },
      { field: "infoShort", header: "importscheduler.scheduler.info", width: "35%" },
      { field: "btn-info", header: "", width: "5%" },
      { field: "btn-reschedule", header: "", width: "5%" },
      { field: "btn-delete", header: "", width: "5%" },
    ];

    this.rescheduleDays = 7;

    this.reloadData(this.currentPage, this.rowsPerPage);
  }

  ngOnDestroy() {}

  /**
   * This method appends the data
   */
  loadJobsLazy(event: LazyLoadEvent) {
    this.loading = true;
    this.currentPage = event.first;
    this.reloadData(this.currentPage, this.currentPage + this.rowsPerPage);
  }

  /**
   * This method reloads the complete dataset
   */
  changeFilter() {
    this.reloadData(this.currentPage, this.currentPage + this.rowsPerPage);
  }

  reloadData(from: number, to: number) {
    const dateFromFormated = this.formatDate(this.filterDateFrom);
    const dateToFormated = this.formatDate(this.filterDateTo);

    this.schedulerService
      .getTotalJobs(this.filterJobState, this.filterStateFile, dateFromFormated, dateToFormated)
      .subscribe((es) => (this.totalRecords = es));
    this.schedulerService
      .getPagedJobs(from, to, this.filterJobState, this.filterStateFile, dateFromFormated, dateToFormated)
      .subscribe((es) => {
        for (const ji of es) {
          if (ji.info.length > 97) {
            ji.infoShort = ji.info.substring(0, 100) + "...";
          } else {
            ji.infoShort = ji.info;
          }
        }
        this.reloadImportStateCounts();
        this.bsJobs.next(es);
        this.loading = false;
      });
  }

  reloadImportStateCounts() {
    const dateFromFormated = this.formatDate(this.filterDateFrom);
    const dateToFormated = this.formatDate(this.filterDateTo);

    this.schedulerService.getStateCount(this.filterStateFile, dateFromFormated, dateToFormated).subscribe((es) => {
      this.jobStates = [
        { label: this.translateService.instant("importscheduler.scheduler.job-state.all") + " (" + es["all"] + ")", value: null },
        {
          label: this.translateService.instant("importscheduler.scheduler.job-state.scheduled") + " (" + es["scheduled"] + ")",
          value: "SCHEDULED",
        },
        {
          label: this.translateService.instant("importscheduler.scheduler.job-state.running") + " (" + es["running"] + ")",
          value: "RUNNING",
        },
        { label: this.translateService.instant("importscheduler.scheduler.job-state.done") + " (" + es["done"] + ")", value: "DONE" },
        { label: this.translateService.instant("importscheduler.scheduler.job-state.failed") + " (" + es["failed"] + ")", value: "FAILED" },
        {
          label: this.translateService.instant("importscheduler.scheduler.job-state.unknown") + " (" + es["unknown"] + ")",
          value: "UNKNOWN",
        },
      ];
    });
  }

  formatDate(dateVal: Date) {
    let ret = null;

    if (dateVal != null) {
      ret = this.datepipe.transform(dateVal, "yyyyMMddHHmm");
    }

    return ret;
  }

  updateJobById(jobId: string) {
    this.schedulerService.updateJob(jobId).subscribe((es) => {
      const tmpJob = es as JobInfo[];
      if (tmpJob && tmpJob[0]) {
        for (const i in this.jobs) {
          if (this.jobs[i].jobId == jobId) {
            this.jobs[i].jobState = tmpJob[0].jobState;
            this.jobs[i].throwable = tmpJob[0].throwable;
            this.jobs[i].info = tmpJob[0].info;
            this.jobs[i].fileDate = tmpJob[0].fileDate;
          }
        }
      }
    });
  }

  removeJob(selected?: JobInfo) {
    this.tmpDelete = selected;
    this.dialogDelete = true;
  }

  cancelRemoveJob() {
    this.tmpDelete = undefined;
    this.dialogDelete = false;
  }

  confirmRemoveJob() {
    let id = undefined;
    if (this.tmpDelete != undefined) {
      id = this.tmpDelete.jobId;
      this.schedulerService.deleteJob(this.tmpDelete.jobId).subscribe();
    }
    this.tmpDelete = undefined;
    this.dialogDelete = false;
    this.reloadData(this.currentPage, this.currentPage + this.rowsPerPage);
  }

  rescheduleJob(selected?: JobInfo) {
    this.tmpReschedule = selected;
    this.dialogReschedule = true;
  }

  cancelRescheduleJob() {
    this.tmpReschedule = undefined;
    this.dialogReschedule = false;
  }

  confirmRescheduleJob() {
    let id = undefined;
    if (this.tmpReschedule != undefined) {
      id = this.tmpReschedule.jobId;
      this.tmpReschedule.jobState = "scheduled";
      this.schedulerService.rescheduleJob(this.tmpReschedule.jobId).subscribe();
    }
    this.tmpReschedule = undefined;
    this.dialogReschedule = false;
    if (id) {
      setTimeout(() => {
        this.updateJobById(id);
      }, 1000);
    }
  }

  rescheduleMultiple() {
    this.dialogRescheduleMultiple = true;
  }

  cancelRescheduleMultiple() {
    this.dialogRescheduleMultiple = false;
  }

  confirmRescheduleMultiple() {
    this.schedulerService.rescheduleMultible(this.rescheduleDays).subscribe((data) => {
      setTimeout(() => {
        this.changeFilter();
      }, 1000);
    });

    this.dialogRescheduleMultiple = false;
  }

  openOverlay(stateFile: string, text: string) {
    this.overlayTitle = stateFile;
    this.overlayText = text;
    this.showInfoDialog = true;
  }

  getDateFromTimestamp(timestamp) {
    return timestamp;
  }

  getStateFile(jobObject) {
    return jobObject["stateFile"];
  }
}
