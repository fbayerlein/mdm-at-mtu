/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, throwError as observableThrowError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { HttpErrorHandler } from "../core/http-error-handler";
import { PropertyService } from "../core/property.service";

export class JobInfo {
  jobId: string;
  jobState: string;
  info: string;
  infoShort: string;
  directory: string;
  stateFile: string;
  fileDate: string;
  throwable: string;
}

export class StateCount {
  all: number;
  scheduled: number;
  running: number;
  done: number;
  failed: number;
  unknown: number;
  throwable: string;
}

@Injectable({
  providedIn: "root",
})
export class SchedulerService {
  private prefEndpoint: string;

  constructor(private http: HttpClient, private httpErrorHandler: HttpErrorHandler, private _prop: PropertyService) {
    this.prefEndpoint = _prop.getUrl("mdm/importscheduler");
  }

  isActive() {
    return this.http.get<boolean>(this.prefEndpoint + "/active").pipe(
      catchError((e) => {
        console.warn("Checking if importscheduler is active failed with exception. Importscheduler is inactive.", e);
        return of(false);
      }),
    );
  }

  getAllJobs(): Observable<JobInfo[]> {
    return this.http.get<any>(this.prefEndpoint + "/jobs").pipe(
      map((r) => r.jobs as JobInfo[]),
      catchError(this.handleError),
    );
  }

  getPagedJobs(
    from: number,
    to: number,
    filterJobState: string,
    filterStateFile: string,
    filterDateFrom: string,
    filterDateTo: string,
  ): Observable<JobInfo[]> {
    const queryStr = this.generateQueryStr(filterJobState, filterStateFile, filterDateFrom, filterDateTo);

    return this.http.get<any>(this.prefEndpoint + "/pagedjobs/" + from + "/" + to + queryStr).pipe(
      map((r) => r.jobs as JobInfo[]),
      catchError(this.handleError),
    );
  }

  getStateCount(filterStateFile: string, filterDateFrom: string, filterDateTo: string): Observable<StateCount[]> {
    const queryStr = this.generateQueryStr(null, filterStateFile, filterDateFrom, filterDateTo);

    return this.http.get<StateCount[]>(this.prefEndpoint + "/statecount" + queryStr).pipe(catchError(this.handleError));
  }

  getTotalJobs(filterJobState: string, filterStateFile: string, filterDateFrom: string, filterDateTo: string): Observable<number> {
    const queryStr = this.generateQueryStr(filterJobState, filterStateFile, filterDateFrom, filterDateTo);

    return this.http.get<number>(this.prefEndpoint + "/jobcount" + queryStr).pipe(catchError(this.handleError));
  }

  generateQueryStr(filterJobState: string, filterStateFile: string, filterDateFrom: string, filterDateTo: string) {
    let queryStr = "?jobstate=" + filterJobState;

    if (filterStateFile != null) {
      queryStr = queryStr + "&stateFile=" + filterStateFile;
    }

    if (filterDateFrom != null) {
      queryStr = queryStr + "&dateFrom=" + filterDateFrom;
    }

    if (filterDateTo != null) {
      queryStr = queryStr + "&dateTo=" + filterDateTo;
    }

    return queryStr;
  }

  updateJob(jobId: string) {
    return this.http.get<any>(this.prefEndpoint + "/jobbyid/" + jobId).pipe(
      map((response) => response.jobs as JobInfo[]),
      catchError(this.handleError),
    );
  }

  rescheduleJob(jobId: string) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };
    const structure = {};

    return this.http.put(this.prefEndpoint + "/" + jobId, JSON.stringify(structure), options).pipe(catchError(this.handleError));
  }

  rescheduleMultible(countDays: number) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };
    const structure = {};

    return this.http
      .post(this.prefEndpoint + "/reschedule/" + countDays, JSON.stringify(structure), options)
      .pipe(catchError(this.handleError));
  }

  deleteJob(jobId: string) {
    return this.http.delete(this.prefEndpoint + "/" + jobId).pipe(catchError(this.handleError));
  }

  private handleError(e: Error | any) {
    if (e && e instanceof Response) {
      const response = <Response>e;
      if (response.status !== 200) {
        return observableThrowError(
          "Could not process scheduler! " + "Please check if application server is running and scheduler is configured correctly.",
        );
      }
    }
    if (this.httpErrorHandler) {
      return this.httpErrorHandler.handleError(e);
    }
    return observableThrowError("An unspecified error occured.");
  }
}
