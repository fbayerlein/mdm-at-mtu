/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
import { Pipe, PipeTransform } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { DateTime } from "luxon";

import { Attribute, ContextGroup, MDMLinkExt } from "../navigator/node";
import { TimezoneService } from "../timezone/timezone-service";

@Pipe({ name: "attributeValue", pure: false })
export class AttributeValuePipe implements PipeTransform {
  constructor(private translateService: TranslateService, private timezoneService: TimezoneService) {}

  transform(attr: Attribute, contextGroup?: ContextGroup, showAll: boolean = true, elementsInLimitedView: number = 3) {
    let display: string;

    if (attr != undefined && attr.value != undefined) {
      let sliced = false;
      let value = contextGroup != undefined ? attr.value[contextGroup] : attr.value;

      if (value != undefined) {
        if (Array.isArray(value) && value.length > elementsInLimitedView && !showAll) {
          value = value.slice(0, elementsInLimitedView);
          sliced = true;
        }

        switch (attr.dataType) {
          case "DATE":
            if (value) {
              display = this.formatDateTime(value);
            } else {
              display = value;
            }
            break;
          case "DATE_SEQUENCE":
            if (Array.isArray(value)) {
              display = value.map((v) => this.formatDateTime(v)).join(", ");
            } else {
              display = value;
            }
            break;
          case "FILE_LINK": {
            const link: MDMLinkExt = Object.assign(new MDMLinkExt(), value);
            display = link.fileName;
            break;
          }
          case "FILE_LINK_SEQUENCE":
          case "FILE_RELATION": {
            const links = value as MDMLinkExt[];
            if (links != undefined) {
              switch (links.length) {
                case 0:
                  display = this.translateService.instant("navigator.attribute-value.msg-no-files-attached");
                  break;
                case 1:
                  display = this.translateService.instant("navigator.attribute-value.msg-one-file-attached");
                  break;
                default:
                  display = this.translateService.instant("navigator.attribute-value.msg-x-files-attached", {
                    numberOfFiles: links.length,
                  });
              }
            }
            break;
          }
          case "ENUMERATION": {
            // for editing the enum, the value includes the Enum name
            const enumValue = value as string;
            if (enumValue === undefined) {
              display = "";
            } else {
              display = enumValue;
            }
            break;
          }
          case "ENUMERATION_SEQUENCE": {
            const enumValues = value as string[];
            // for editing the enum, the value includes the Enum name
            if (enumValues === undefined || !Array.isArray(enumValues)) {
              display = "";
            } else {
              display = enumValues.join(", ");
            }
            break;
          }
          default:
            if (Array.isArray(value)) {
              display = value.join(", ");
            } else {
              display = value;
            }
        }
      }
      if (sliced) {
        display = display + "...";
      }
    }
    return display;
  }

  private formatDateTime(value: string) {
    const backendDate = DateTime.fromISO(value, { zone: this.timezoneService.getSelectedTimezone() });
    return this.timezoneService.formatDateTime(backendDate);
  }
}
