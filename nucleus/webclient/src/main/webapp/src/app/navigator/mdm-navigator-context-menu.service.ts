/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { streamTranslate } from "@core/mdm-core.module";
import { MDMIdentifier } from "@core/mdm-identifier";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { combineLatest, concatMap, defaultIfEmpty, forkJoin, map, mergeMap, Observable, of } from "rxjs";
import { MDMTreeNode } from "./nodeprovider.service";

export class MenuItemRegistration {
  menuItem: MenuItem;
  predicate?: (node: MDMIdentifier) => boolean = () => true;
  sortIndex? = 1000;
  lazyChildren?: (parent: MenuItem, currentNode: MDMIdentifier, selectedNodes?: MDMIdentifier[]) => Observable<MenuItem[]>;
}

@Injectable({
  providedIn: "root",
})
export class MdmNavigatorContextMenuService {
  registeredItems: MenuItemRegistration[] = [];

  constructor(private translateService: TranslateService) {}

  register(reg: MenuItemRegistration) {
    const r = Object.assign(new MenuItemRegistration(), reg);

    streamTranslate(this.translateService, r.menuItem.label).subscribe((msg: string) => (r.menuItem.label = msg));

    const index = this.registeredItems.findIndex((x) => x.menuItem.id === r.menuItem.id);

    if (index >= 0) {
      this.registeredItems[index] = r;
    } else {
      this.registeredItems.push(r);
    }
  }

  buildContextMenu(selectedNode: MDMTreeNode, selectedNodes?: MDMTreeNode[]): Observable<MenuItem[]> {
    if (!selectedNode) {
      return of([]);
    }
    return forkJoin(
      this.registeredItems
        .filter((item) => item.predicate(selectedNode))
        .sort((a, b) => a.sortIndex - b.sortIndex)
        .map((item) => this.transform(item, selectedNode, selectedNodes)),
    ).pipe(
      map((items) => {
        return items.filter((item) => item.items === undefined || item.items.length > 0);
      }),
    );
  }

  private transform(item: MenuItemRegistration, selectedNode: MDMTreeNode, selectedNodes?: MDMTreeNode[]): Observable<MenuItem> {
    if (item.lazyChildren) {
      return item.lazyChildren(item.menuItem, selectedNode, selectedNodes).pipe(
        concatMap((items) => this.applyStateAndTranslateItems(items, selectedNode, selectedNodes)),
        mergeMap((items) => {
          item.menuItem.items = items;
          return this.applyStateAndTranslate(item.menuItem, selectedNode, selectedNodes);
        }),
      );
    } else {
      item.menuItem.items = undefined;
      return this.applyStateAndTranslate(item.menuItem, selectedNode, selectedNodes);
    }
  }

  private applyStateAndTranslate(menuItem: MenuItem, selectedNode: MDMTreeNode, selectedNodes?: MDMTreeNode[]) {
    return this.translateItem(this.applyState(menuItem, selectedNode, selectedNodes));
  }

  private applyStateAndTranslateItems(items: MenuItem[], selectedNode: MDMTreeNode, selectedNodes?: MDMTreeNode[]) {
    return combineLatest(items.map((i) => this.applyStateAndTranslate(i, selectedNode, selectedNodes))).pipe(defaultIfEmpty([]));
  }

  private translateItem(item: MenuItem) {
    return this.translateService.get(item.label).pipe(
      map((label) => {
        item.label = label;
        return item;
      }),
    );
  }

  private applyState(menuItem: MenuItem, selectedNode: MDMTreeNode, selectedNodes?: MDMTreeNode[]) {
    menuItem.state = {
      ...menuItem.state,
      node: selectedNode,
      selectedNodes: selectedNodes,
    };

    return menuItem;
  }
}
