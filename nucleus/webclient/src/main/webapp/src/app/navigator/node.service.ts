/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { EventEmitter, Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { MDMItem } from "@core/mdm-item";
import { PropertyService } from "@core/property.service";
import { Observable, of } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Node, NodeArray } from "./node";

@Injectable()
export class NodeService {
  private activeDatasources: string[] = [];

  public datasourcesChanged = new EventEmitter<string[]>();

  public parentNodeRequestEvent: EventEmitter<Node> = new EventEmitter<Node>();
  public parentNodeResponseEvent: EventEmitter<any[]> = new EventEmitter<any[]>();

  private _nodeUrl: string;
  private _nodeProviderEndpoint: string;
  private _defNodeProv = "generic";

  static mapSourceNameToName(environments: Node[], sourceName: string) {
    const env = environments.find((n) => n.sourceName === sourceName);
    return env ? env.name : sourceName;
  }

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this._nodeUrl = this._prop.getUrl("mdm/environments");
  }

  /*
   * The datasource has changed, the components must reload environment data.
   */
  setActiveDatasource(datasources: string[]) {
    this.activeDatasources = datasources;
    this.datasourcesChanged.emit(this.activeDatasources);
  }

  getActiveDatasources() {
    return this.activeDatasources;
  }

  searchNodes(query, env, type) {
    return this.http.get<NodeArray>(this._nodeUrl + "/" + env + "/" + this.typeToUrl(type) + "?" + query).pipe(
      map((res) => res.data),
      catchError((e) => addErrorDescription(e, "Could not request Nodes!")),
    );
  }

  searchFT(query, env) {
    return this.http.get<NodeArray>(this._nodeUrl + "/" + env + "/search?q=" + query).pipe(
      map((res) => res.data),
      catchError((e) => addErrorDescription(e, "Could not request Nodes!")),
    );
  }

  getNodes(node?: Node) {
    if (node === undefined) {
      return this.getRootNodes();
    }
    return this.getNode(this.getUrl(node));
  }

  addNode(name: string): Observable<Node> {
    const body = JSON.stringify({ name });
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    return this.http.post<NodeArray>(this._nodeUrl, body, options).pipe(
      map((res) => res.data[0]),
      catchError((e) => addErrorDescription(e, "Could not add Node!")),
    );
  }

  deleteNode(node: Node) {
    return this.http.delete<NodeArray>(this.getUrl(node)).pipe(
      map((res) => res.data),
      catchError((e) => addErrorDescription(e, "Could not delete Node!")),
    );
  }

  compareNode(node1, node2) {
    if (node1 === undefined || node2 === undefined) {
      return;
    }
    const n1 = node1.name + node1.id + node1.type + node1.sourceName;
    const n2 = node2.name + node2.id + node2.type + node2.sourceName;
    return n1 === n2;
  }

  getRootNodes() {
    return this.http.get<NodeArray>(this._nodeUrl).pipe(
      map((res) => res.data),
      catchError((e) => addErrorDescription(e, "Could not request RootNodes!")),
    );
  }

  getNodeFromItem(mdmItem: MDMItem) {
    if (mdmItem.type === "Environment") {
      return this.getNode(this._nodeUrl + "/" + mdmItem.source).pipe(
        map((nodes) => this.patchNode(mdmItem, nodes && nodes.length > 0 ? nodes[0] : undefined)),
      );
    } else {
      if (mdmItem.id !== undefined) {
        // non-virtual nodes
        return this.getNode(this._nodeUrl + "/" + mdmItem.source + "/" + this.typeToUrl(mdmItem.type) + "/" + mdmItem.id).pipe(
          map((nodes) => this.patchNode(mdmItem, nodes && nodes.length > 0 ? nodes[0] : undefined)),
        );
      } else {
        // virtual node
        const node = new Node();
        node.type = mdmItem.type;
        node.sourceName = mdmItem.source;
        return of(undefined);
      }
    }
  }

  patchNode(mdmItem: MDMItem, node: Node) {
    if (node !== undefined) {
      // patch the id attribute into the mdmentity node for the advanced search filter linking
      node.idAttribute = mdmItem.idAttribute;
    }
    return node;
  }

  getNodesFromItem(mdmItem: MDMItem) {
    return this.getNode(this._nodeUrl + "/" + mdmItem.source + "/" + this.typeToUrl(mdmItem.type) + "/" + mdmItem.id).pipe(
      map((nodes) => (nodes && nodes.length > 0 ? nodes[0] : undefined)),
    );
  }

  typeToUrl(type: string) {
    switch (type) {
      case "StructureLevel":
        return "pools";
      case "MeaResult":
        return "measurements";
      case "SubMatrix":
        return "channelgroups";
      case "MeaQuantity":
        return "channels";
      case "Quantity":
        return "quantities";
      default:
        return type.toLowerCase() + "s";
    }
  }

  getNode(url: string, params?: HttpParams) {
    return this.http.get<NodeArray>(url, { params }).pipe(
      map((res) => res.data),
      map((nodes) => nodes.map((n) => Object.assign(new Node(), n))),
      catchError((e) => addErrorDescription(e, "Could not request Nodes!")),
    );
  }
  /*
  getBoolean(url: string) {
    return this.http.get<Boolean>(url).pipe(
      catchError(e => addErrorDescription(e, 'Could not request Nodes!')));
  }

  getString(url: string) {
    return this.http.get<String>(url).pipe(
      catchError(e => addErrorDescription(e, 'Could not request Nodes!'))
    );
  }
*/
  getNodesByUrl(url: string, params?: HttpParams) {
    if (url.startsWith("/")) {
      return this.getNode(this._nodeUrl + url, params);
    } else {
      return this.getNode(this._nodeUrl + "/" + url, params);
    }
  }

  nodeToUrl(mdmItem: MDMItem, activeNodeProvider: string) {
    if (mdmItem.id) {
      return mdmItem.source + "/" + this.typeToUrl(mdmItem.type) + "/" + mdmItem.id;
    } else {
      return mdmItem.source + "/virtual/" + activeNodeProvider + "/" + mdmItem.serial;
    }
  }

  getNodesBySerial(url: string) {
    return this.getNode(this._nodeProviderEndpoint + "/" + this._defNodeProv + "/" + url);
  }

  getNodesByAbsoluteUrl(url: string) {
    return this.getNode(url);
  }

  getNodeByRelation(sourceName: string, type: string, id: string) {
    return this.getNode(this._nodeUrl + "/" + sourceName + "/" + this.typeToUrl(type) + "/" + id);
  }

  private getUrl(node: Node) {
    return this._nodeUrl + "/" + node.sourceName + "/" + node.type + "/" + node.id;
  }
}
