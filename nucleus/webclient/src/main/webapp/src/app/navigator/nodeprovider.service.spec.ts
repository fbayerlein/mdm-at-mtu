/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Preference, PreferenceService } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { NodeService } from "@navigator/node.service";
import { TranslateModule } from "@ngx-translate/core";
import { Observable, of as observableOf } from "rxjs";
import { QueryService } from "../tableview/query.service";
import { NodeProviderUIWrapper } from "./nodeprovider-ui-wrapper";
import { NodeproviderService } from "./nodeprovider.service";

declare function require(path: string): any;
const defaultNodeProvider = require("../navigator/defaultnodeprovider.json");

class TestPreferenceService {
  getPreferenceForScope(scope: string, key?: string): Observable<Preference[]> {
    const p = new Preference();
    p.value = JSON.stringify(defaultNodeProvider);
    return observableOf([p]);
  }
}

describe("NodeproviderService", () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        PropertyService,
        {
          provide: PreferenceService,
          useClass: TestPreferenceService,
        },
        NodeproviderService,
        MDMNotificationService,
        QueryService,

        NodeService,
      ],
    });

    // Inject the http service and test controller for each test
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it("getNodeproviders", (done: DoneFn) => {
    const testData: NodeProviderUIWrapper = {
      index: 0,
      name: "Default",
      id: "default",
    };

    const service = TestBed.inject(NodeproviderService);
    service.getNodeproviders().subscribe((value) => {
      expect(value).toEqual([testData]);
      done();
    });

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/nodeprovider");
    expect(req.request.method).toEqual("GET");
    req.flush([testData]);

    httpTestingController.verify();
  });
});
