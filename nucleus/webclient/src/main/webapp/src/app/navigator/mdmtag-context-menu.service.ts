/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { MDMIdentifier } from "@core/mdm-identifier";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { map, Observable, of, Subscription } from "rxjs";
import { MDMTagService } from "../mdmtag/mdmtag.service";
import { MdmNavigatorContextMenuService } from "./mdm-navigator-context-menu.service";
import { Node } from "./node";
import { NodeService } from "./node.service";
import { MDMTreeNode } from "./nodeprovider.service";

@Injectable({
  providedIn: "root",
})
export class MdmTagContextMenuService {
  ICON_UNCHECKED = "pi pi-circle";
  ICON_CHECKED = "pi pi-check-circle";

  mdmtagsMap: Map<string, Node[]> = new Map();
  tagableMenuMap: Map<string, MenuItem[]> = new Map();

  nodeServiceSubscription: Subscription;

  constructor(
    private mdmTagService: MDMTagService,
    private nodeService: NodeService,
    private menuService: MdmNavigatorContextMenuService,
    private translateService: TranslateService,
    private notificationService: MDMNotificationService,
  ) {
    this.onDatasourceChanged(this.nodeService.getActiveDatasources());
    this.subscribeToDatasourceChanged();

    this.menuService.register({
      menuItem: { id: "mdmtag", label: "navigator.mdm-navigator.add-to-mdmtag", icon: "fa fa-tags", items: [] },
      predicate: (node) => node.type === "Test" || node.type === "TestStep" || node.type === "Measurement",
      lazyChildren: (parent, node) => this.getTagMenuItems(node),
      sortIndex: 110,
    });
  }

  subscribeToDatasourceChanged() {
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe(
      (sources) => this.onDatasourceChanged(sources),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("navigator.mdm-navigator.err-cannot-update-navigation-tree"),
          error,
        ),
    );
  }

  onDatasourceChanged(sources: string[]) {
    this.loadMDMTags(sources);
  }

  loadMDMTags(sources: string[]) {
    for (const source of sources) {
      this.mdmTagService.getAllMDMTags(source).subscribe((data) => {
        this.mdmtagsMap.set(source, data);
      });
    }
  }

  getTagMenuItems(node: MDMTreeNode | MDMIdentifier | Node): Observable<MenuItem[]> {
    if (node.id) {
      return this.mdmTagService.getMDMTagOfTagable(node.source, node).pipe(
        map((tagData) => {
          const subMenuMDMTag: MenuItem[] = [];
          for (const mdmtag of this.mdmtagsMap.get(node.source)) {
            subMenuMDMTag.push(this.toMenuItem(tagData, mdmtag, subMenuMDMTag));
          }
          if (subMenuMDMTag.length === 0) {
            return [{ label: "navigator.mdm-navigator.no-tags-defined", disabled: true } as MenuItem];
          } else {
            return subMenuMDMTag;
          }
        }),
      );
    } else {
      return of([]);
    }
  }

  toMenuItem(tagData: Node[], mdmtag: Node, subMenuMDMTag: MenuItem[]) {
    const isTaged = tagData.some((element) => element.name === mdmtag.name);

    return {
      id: mdmtag.id,
      label: mdmtag.name,
      icon: isTaged ? this.ICON_CHECKED : this.ICON_UNCHECKED,
      state: { isTaged: isTaged },
      command: (event) => {
        this.handleTagging(event, mdmtag, subMenuMDMTag);
      },
    } as MenuItem;
  }

  handleTagging(event: { item: MenuItem }, mdmTag: Node, subMenu: MenuItem[]) {
    const menuItem = this.getMenuItemById(subMenu, mdmTag.id);

    if (menuItem != null && menuItem.state.isTaged) {
      this.removeTagEntity(event.item, mdmTag, subMenu);
    } else {
      this.tagEntity(event.item, mdmTag, subMenu);
    }
  }

  getMenuItemById(menuItems: MenuItem[], id: string) {
    for (const mi of menuItems) {
      if (id === mi.id) {
        return mi;
      }
    }
  }

  tagEntity(item: MenuItem, mdmTag: Node, subMenu: MenuItem[]) {
    const selectedNodes = (item.state["selectedNodes"] || []) as MDMTreeNode[];
    if (selectedNodes != null && selectedNodes.length > 0) {
      const mdmtreenode = selectedNodes[0];
      this.mdmTagService
        .tagEntity(mdmTag.sourceName, mdmTag, { source: mdmtreenode.source, type: mdmtreenode.type, id: mdmtreenode.id } as Node)
        .subscribe();
      const menuItem = this.getMenuItemById(subMenu, mdmTag.id);
      if (menuItem != null) {
        menuItem.icon = this.ICON_CHECKED;
        menuItem.state.isTaged = true;
      }
    }
  }

  removeTagEntity(item: MenuItem, mdmTag: Node, subMenu: MenuItem[]) {
    const selectedNodes = (item.state["selectedNodes"] || []) as MDMTreeNode[];
    if (selectedNodes != null && selectedNodes.length > 0) {
      const mdmtreenode = selectedNodes[0];
      this.mdmTagService
        .removeTagEntity(mdmTag.sourceName, mdmTag, { source: mdmtreenode.source, type: mdmtreenode.type, id: mdmtreenode.id } as Node)
        .subscribe();
      const menuItem = this.getMenuItemById(subMenu, mdmTag.id);
      if (menuItem != null) {
        menuItem.icon = this.ICON_UNCHECKED;
        menuItem.state.isTaged = false;
      }
    }
  }
}
