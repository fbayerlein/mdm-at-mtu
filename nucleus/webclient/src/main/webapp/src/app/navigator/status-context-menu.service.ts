/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { MDMIdentifier } from "@core/mdm-identifier";
import { MDMItem } from "@core/mdm-item";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { map, mergeMap, Observable, of, Subscription } from "rxjs";
import { StatusService } from "../status/status.service";
import { MdmNavigatorContextMenuService } from "./mdm-navigator-context-menu.service";
import { NavigatorService } from "./navigator.service";
import { Node } from "./node";
import { NodeService } from "./node.service";
import { MDMTreeNode } from "./nodeprovider.service";

@Injectable({
  providedIn: "root",
})
export class StatusContextMenuService {
  statusMap: Map<string, Node[]> = new Map();
  statusMap4Obj: Map<string, Map<string, string[]>> = new Map();

  nodeServiceSubscription: Subscription;

  constructor(
    private statusService: StatusService,
    private nodeService: NodeService,
    private menuService: MdmNavigatorContextMenuService,
    private translateService: TranslateService,
    private notificationService: MDMNotificationService,
    private navigatorService: NavigatorService,
  ) {
    this.onDatasourceChanged(this.nodeService.getActiveDatasources());
    this.subscribeToDatasourceChanged();

    this.menuService.register({
      menuItem: { id: "status", label: "navigator.mdm-navigator.set-status", icon: "fa fa-tags", items: [] },
      predicate: (node) => node.type === "Test" || node.type === "TestStep",
      lazyChildren: (parent, node) => this.getStatusMenuItems(node),
      sortIndex: 110,
    });
  }

  subscribeToDatasourceChanged() {
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe(
      (sources) => this.onDatasourceChanged(sources),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("navigator.mdm-navigator.err-cannot-update-navigation-tree"),
          error,
        ),
    );
  }

  onDatasourceChanged(sources: string[]) {
    this.loadStatus(sources);
  }

  loadStatus(sources: string[]) {
    for (const source of sources) {
      this.statusService.findAllStatus(source).subscribe((data) => {
        this.statusMap.set(source, data);
        this.statusMap4Obj.set(source, new Map());
      });
      this.statusService.findSystemParameter4Status(source).subscribe((data) => {
        data.forEach((node) => {
          this.statusMap4Obj.get(node.sourceName).set(node.name, this.getStatus4Object(node));
        });
      });
    }
  }

  getStatus4Object(node: Node) {
    if (node != undefined) {
      const attr = node.attributes.find((attr) => "Value".localeCompare(attr.name) === 0);
      return attr.value.toString().split(";");
    }
    return undefined;
  }

  getStatusMenuItems(node: MDMTreeNode | MDMIdentifier | Node): Observable<MenuItem[]> {
    if (this.statusMap4Obj.get(node.source)) {
      return this.nodeService.getNodeFromItem(node as MDMItem).pipe(
        mergeMap((nodeFromItem) => this.statusService.loadStatus(nodeFromItem)),
        map((statusName) => {
          const availableStatus = this.statusMap4Obj.get(node.source).get("org.eclipse.mdm.status." + node.type.toLowerCase());
          const subMenuStatus: MenuItem[] = [];
          this.statusMap.get(node.source).forEach((statusNode) => {
            if (availableStatus === undefined || availableStatus.find((as) => statusNode.name === as)) {
              subMenuStatus.push({
                id: statusNode.id,
                label: statusNode.name,
                icon: statusNode.name === statusName ? "pi pi-check" : null,
                disabled: statusNode.name === statusName,
                state: { isTaged: false },
                command: (event) => {
                  this.setStatus(event, statusNode);
                },
              });
            }
          });
          return subMenuStatus;
        }),
      );
    } else {
      return of([]);
    }
  }

  setStatus(event: { item: MenuItem }, statusNode: Node) {
    const selectedNodes = (event.item.state["selectedNodes"] || []) as MDMTreeNode[];
    if (selectedNodes) {
      for (const mdmNode of selectedNodes) {
        if (mdmNode && mdmNode.id) {
          this.statusService.updateStatus(mdmNode.source, mdmNode.type, mdmNode.id, statusNode.id).subscribe({
            next: (updatedNodes) => {
              const mdmItems = updatedNodes.data.map((n) => {
                return { source: n.sourceName, type: n.type, id: n.id } as MDMItem;
              });
              if (mdmItems.length > 0) {
                this.navigatorService.fireSelectedItem(mdmItems[0]);
              }

              this.notificationService.notifyInfo(
                this.translateService.instant("navigator.mdm-navigator.set-status"),
                this.translateService.instant("navigator.mdm-navigator.set_status-done"),
              );
            },
            error: (e) =>
              this.notificationService.notifyError(
                this.translateService.instant("navigator.mdm-navigator.err-status-setting"),
                e.error.clientMessage,
              ),
          });
        }
      }
    }
  }
}
