/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { Message, MessageService } from "primeng/api";
import { Subscription, tap } from "rxjs";
import { MDMNotificationService } from "./mdm-notification.service";
import { PreferenceService } from "./preference.service";

@Component({
  selector: "mdm-notifications",
  templateUrl: "./mdm-notification.component.html",
})
export class MDMNotificationComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  notificationLifeSpan = 3000;
  lastMessages: Message[] = [];
  constructor(
    private notificationsService: MDMNotificationService,
    private messageService: MessageService,
    private preferenceService: PreferenceService,
  ) {}

  ngOnInit() {
    this.preferenceService
      .getPreference("notification.message.life_span")
      .pipe(tap((pref) => (this.notificationLifeSpan = +pref[0]?.value || 3000)))
      .subscribe();

    this.subscribeToNotifications();
  }

  subscribeToNotifications() {
    this.subscription = this.notificationsService.notificationChange.subscribe((notification) => {
      this.lastMessages.push({ ...notification, sticky: true });
      this.lastMessages.slice(-100, -1);
      notification.life = this.notificationLifeSpan;
      this.messageService.add(notification);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  copyTraceID(textToCopy: string) {
    const selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = textToCopy;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();

    document.execCommand("copy");
    document.body.removeChild(selBox);

    /*if (navigator.clipboard) {
      window.navigator.clipboard.writeText(textToCopy).then(() => {
        alert('Copied to Clipboard')
      }, (error) => {
        console.log(error)
      });
    } else {
      console.log('Browser do not support Clipboard API')
    }*/
  }
}
