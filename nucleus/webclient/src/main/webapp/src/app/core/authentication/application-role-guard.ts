/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate } from "@angular/router";
import { of } from "rxjs";
import { AuthenticationService } from "./authentication.service";

@Injectable({
  providedIn: "root",
})
export class ApplicationRoleGuard implements CanActivate {
  route: ActivatedRouteSnapshot;

  constructor(private authStateService: AuthenticationService) {}

  canActivate(route: ActivatedRouteSnapshot) {
    if (route.data.roles) {
      // check if current user has any role the route requires
      return this.authStateService.isCurrentUserInRole(route.data.roles);
    }
    return of(true);
  }
}
