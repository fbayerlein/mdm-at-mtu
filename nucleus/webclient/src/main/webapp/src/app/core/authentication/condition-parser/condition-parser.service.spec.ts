/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { TestBed } from "@angular/core/testing";
import { TokenError } from "typescript-parsec";
import { ConditionParserService } from "./condition-parser.service";

describe("ConditionParserService", () => {
  let service: ConditionParserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConditionParserService);
  });

  describe("General", () => {
    it("should be created", () => {
      expect(service).toBeTruthy();
    });
  });

  describe("Boolean literals should be valid", () => {
    it("true should be true", () => expect(service.parse("true")).toBeTrue());
    it("false should be false", () => expect(service.parse("false")).toBeFalse());
    it("invalidString should throw exception", () =>
      expect(() => service.parse("invalidString")).toThrow(new TokenError(undefined, "Unable to consume token: <END-OF-FILE>")));
  });

  describe("String literal comparison: equals, not equals", () => {
    const testStringsAeqAtrue = [
      "'a'=='a'",
      "'a'  == 'a'",
      "('a' == 'a')",
      "(('a' =='a'))",
      "'a' == 'a'",
      "'' == ''",
      "' ' == ' '",
      "'A' == 'A'",
    ];
    testStringsAeqAtrue.forEach((testString) => it(testString + " should be true", () => expect(service.parse(testString)).toBeTrue()));

    const testStringsAneqAfalse = [
      "'a'!='a'",
      "'a'  != 'a'",
      "('a' != 'a')",
      "( ('a' !='a'))",
      "'a' != 'a'",
      "'' != ''",
      "' ' != ' '",
      "'A' != 'A'",
    ];
    testStringsAneqAfalse.forEach((testString) => it(testString + " should be false", () => expect(service.parse(testString)).toBeFalse()));

    const testStringsAeqAfalse = [
      "'a'=='b'",
      "'a'=='aa'",
      "'a'=='A'",
      "'a'=='a '",
      "'a'==' a'",
      "'a'==''",
      "'a'==' '",
      "'' == ' '",
      "'a'  == 'b'",
      "('a' == 'b')",
      "(('a' =='b'))",
      "'a' == 'b'",
      "'b'=='a'",
      "'b'  == 'a'",
      "('b' == 'a')",
      "(('b' =='a') )",
      "'b' == 'a'",
    ];
    testStringsAeqAfalse.forEach((testString) => it(testString + " should be false", () => expect(service.parse(testString)).toBeFalse()));

    const testStringsAneqAtrue = [
      "'a'!='A'",
      "'a'!='a '",
      "'a'!=' a'",
      "'a'!=''",
      "'a'!=' '",
      "'' != ' '",
      "'a'!='b'",
      "'a'  != 'b'",
      "('a' != 'b')",
      "(('a' !='b'))",
      "'a' != 'b'",
      "'b'!='a'",
      "'b'  != 'a'",
      "('b' != 'a')",
      "(('b' !='a'))",
      "'b' != 'a'",
    ];
    testStringsAneqAtrue.forEach((testString) => it(testString + " should be true", () => expect(service.parse(testString)).toBeTrue()));

    const testStringsBrokenStringLiteral = [
      "'a'!=",
      "'a'  != 'b",
      "'a' != b'",
      "((!='a'))",
      "b' != 'a'",
      "'b != 'a')",
      "('a' == 'b'",
      "('a' =='b'))",
      "'b'!'a'",
      "'b'='a'",
      "'a'==='a'",
      "'b'!=='a'",
      "'b'!'a'",
      "'b'='a'",
      "'b'= ='a'",
      "'b'! ='a'",
    ];
    testStringsBrokenStringLiteral.forEach((testString) =>
      it(testString + " should fail on incomplete or broken syntax", () => expect(() => service.parse(testString)).toThrowError()),
    );
  });

  describe("Structural data comparison: equals, not equals", () => {
    const structuralData = {
      a: "a",
      b: "b",
      c: undefined,
      d: null,
      A: {
        a: "a",
        b: "b",
      },
      B: {
        a: "Ba",
        c: undefined,
        d: null,
      },
    };
    describe("Structural data:" + JSON.stringify(structuralData), () => {
      const testStringsAeqAtrue = ["a == a", "a == A.a", "A.a == a", "A.a == A.a", "c == B.c", "d == B.d"];
      testStringsAeqAtrue.forEach((testString) =>
        it(testString + " should be true", () => expect(service.parse(testString, structuralData)).toBeTrue()),
      );

      const testStringsAneqAfalse = ["a != a", "a != A.a", "A.a != a", "A.a != A.a", "c != B.c", "d != B.d"];
      testStringsAneqAfalse.forEach((testString) =>
        it(testString + " with structural data should be false", () => expect(service.parse(testString, structuralData)).toBeFalse()),
      );

      const testStringsAeqAfalse = [
        "a == b",
        "a == A.b",
        "A.a == B.a",
        "A.a == A.b",
        "A.a == b",
        "a == c",
        "A.a == c",
        "A.a == c",
        "c == d",
      ];
      testStringsAeqAfalse.forEach((testString) =>
        it(testString + " should be false", () => expect(service.parse(testString, structuralData)).toBeFalse()),
      );

      const testStringsAneqAtrue = [
        "a != b",
        "a != A.b",
        "A.a != B.a",
        "A.a != A.b",
        "A.a != b",
        "a != c",
        "A.a != c",
        "A.a != c",
        "c != d",
      ];
      testStringsAneqAtrue.forEach((testString) =>
        it(testString + " should be true", () => expect(service.parse(testString, structuralData)).toBeTrue()),
      );

      const testStringsBrokenStringLiteral = [".a == b", "a == A.b.", "A.a == B..a", "A.a == (A.)b"];
      testStringsBrokenStringLiteral.forEach((testString) =>
        it(testString + " should fail on incomplete or broken syntax", () => expect(() => service.parse(testString)).toThrowError()),
      );
    });

    describe("Structural data undefined/empty", () => {
      const testStringsAeqAtrue = ["a == a", "a == A.a", "A.a == a", "A.a == A.a", "c == B.c", "d == B.d"];
      testStringsAeqAtrue.forEach((testString) => {
        it(testString + " should be true", () => expect(service.parse(testString)).toBeTrue());
        it(testString + " should be true", () => expect(service.parse(testString, null)).toBeTrue());
        it(testString + " should be true", () => expect(service.parse(testString, {})).toBeTrue());
      });

      const testStringsAneqAfalse = ["a != a", "a != A.a", "A.a != a", "A.a != A.a", "c != B.c", "d != B.d"];
      testStringsAneqAfalse.forEach((testString) => {
        it(testString + " should be false", () => expect(service.parse(testString)).toBeFalse());
        it(testString + " should be false", () => expect(service.parse(testString, null)).toBeFalse());
        it(testString + " should be false", () => expect(service.parse(testString, {})).toBeFalse());
      });
    });
  });

  describe("String literal comparison to context data variable", () => {
    const testStringsEq = [
      "'a'==${a}",
      "'a'  == ${a}",
      "('a' == ${a})",
      "(('a' ==${a}))",
      "'a' == ${a}",
      "${a}=='a'",
      "${a}  == 'a'",
      "(${a} == 'a')",
      "((${a} =='a'))",
      "${a} == 'a'",
    ];
    describe('Equals with const a = "a"', () => {
      testStringsEq.forEach((testString) =>
        it(testString + " should be true", () => {
          const a = "a";
          expect(service.parse(testString, undefined, { a })).toBeTrue();
        }),
      );
    });
    describe("Equals with other value then 'a'", () => {
      const aValues = ["", " ", " a", "A", "a ", " a ", "b", "aa", null, undefined];
      testStringsEq.forEach((testString) => {
        aValues.forEach((a) => {
          it(testString + ' with const a = "' + a + '" should be false', () => {
            expect(service.parse(testString, { a })).toBeFalse();
          });
        });
        it(testString + " with context not containing a should be false", () => {
          expect(service.parse(testString, { b: "b" })).toBeFalse();
          expect(service.parse(testString, {})).toBeFalse();
          expect(service.parse(testString)).toBeFalse();
        });
      });
    });

    const testStringsNeq = [
      "'a'!=${a}",
      "'a'  != ${a}",
      "('a' != ${a})",
      "(('a' !=${a}))",
      "'a' != ${a}",
      "${a}!='a'",
      "${a}  != 'a'",
      "(${a} != 'a')",
      "((${a} !='a'))",
      "${a} != 'a'",
    ];
    describe('Not equals with const a = "a"', () => {
      testStringsNeq.forEach((testString) =>
        it(testString + " should be false", () => {
          const a = "a";
          expect(service.parse(testString, undefined, { a })).toBeFalse();
        }),
      );
    });
    describe("Not equals with other value then 'a'", () => {
      const aValues = ["", " ", " a", "A", "a ", " a ", "b", "aa", null, undefined];
      testStringsNeq.forEach((testString) => {
        aValues.forEach((a) => {
          it(testString + ' with const a = "' + a + '" should be true', () => {
            expect(service.parse(testString, { a })).toBeTrue();
          });
        });
        it(testString + " with context not containing a should be true", () => {
          expect(service.parse(testString, { b: "b" })).toBeTrue();
          expect(service.parse(testString, {})).toBeTrue();
          expect(service.parse(testString)).toBeTrue();
        });
      });
    });
    describe("Equals with broken syntax", () => {
      const testStringsBroken = [
        "'a'= =${a}",
        "'a'=${a}",
        "'a'=={a}",
        "'a'==$a}",
        "'a'==${a",
        "'a'==$${a}",
        "'a'==${{a}",
        "'a'==${{a}}",
        "'a'==${a}}",
      ];
      testStringsBroken.forEach((testString) =>
        it(testString + " should fail", () => {
          const a = "a";
          expect(() => service.parse(testString, undefined, { a })).toThrow();
        }),
      );
    });
    describe("Not equals with broken syntax", () => {
      const testStringsBroken = [
        "'a'! =${a}",
        "'a'!!=${a}",
        "'a'!={a}",
        "'a'!=$a}",
        "'a'!=${a",
        "'a'!=$${a}",
        "'a'!=${{a}",
        "'a'!=${{a}}",
        "'a'!=${a}}",
      ];
      testStringsBroken.forEach((testString) =>
        it(testString + " should fail", () => {
          const a = "a";
          expect(() => service.parse(testString, undefined, { a })).toThrow();
        }),
      );
    });
  });

  describe("Includes for string literals", () => {
    const testStringsTrue = [
      "'a' IN ('a')",
      "'' IN ('')",
      "' ' IN (' ')",
      "'a' IN ('a', 'b')",
      "'a' IN (   'a',      'b')",
      "'a' IN ('a',      'b' )",
      "'a' IN ('b', 'a')",
      "'a' IN ('a', 'a')",
      "'a' IN ('', 'a')",
      "('a' IN ('', 'a'))",
      "('a') IN ('a')",
      "('a') IN 'a'",
      "'a' IN 'a'",
    ];
    testStringsTrue.forEach((testString) => it(testString + " should be true", () => expect(service.parse(testString)).toBeTrue()));
    const testStringsFalse = [
      "'a' IN ('A')",
      "'a' IN ('b')",
      "'a' IN (' a')",
      "'a' IN ('a ')",
      "'a' IN ('')",
      "'a' IN (' ')",
      "'a' IN ('b', 'c')",
      "('a' IN ('', 'b'))",
    ];
    testStringsFalse.forEach((testString) => it(testString + " should be false", () => expect(service.parse(testString)).toBeFalse()));

    it("Should fail on incomplete or broken syntax", () => {
      const testStrings = [
        "'a' iN ('a','b')",
        "'a' in ('a','b')",
        "'a' In ('a','b')",
        "'a' I ('a','b')",
        "'a' n ('a','b')",
        "'a' ('a')",
        "'a' IN ('a)",
        "'a' IN ('a'",
        "'a' IN ('a''b')",
        "'a IN ('a')",
        "'a' IN (('a')",
        "'a' IN ('a'))",
        "'a' IN ()",
        "'a' IN (,)",
      ];
      testStrings.forEach((testString) => expect(() => service.parse(testString)).toThrow());
    });
  });

  describe("Includes for context data varaible and string literals", () => {
    const testStringsWithA = [
      "${a} IN ('a')",
      "${a} IN 'a'",
      "${a} IN ('a', 'b')",
      "${a} IN (   'a',      'b')",
      "${a} IN ('a',      'b' )",
      "${a} IN ('b', 'a')",
      "${a} IN ('a', 'a')",
      "${a} IN ('', 'a')",
      "(${a} IN ('', 'a'))",
      "('a') IN ${a}",
      "'a' IN ${a} ",
      "('a', 'b') IN ${a}",
      "(   'a',      'b') IN ${a}",
      "('a',      'b' ) IN ${a}",
      "('b', 'a') IN ${a}",
      "('a', 'a') IN ${a}",
      "('', 'a') IN ${a}",
      "(('', 'a') IN ${a})",
    ];

    testStringsWithA.forEach((testString) =>
      it(testString + ' with const a = "a" should be true', () => {
        const a = "a";
        expect(service.parse(testString, undefined, { a })).toBeTrue();
      }),
    );

    testStringsWithA.forEach((testString) => {
      it(testString + ' with const a = "c" should be false', () => {
        const a = "c";
        expect(service.parse(testString, undefined, { a })).toBeFalse();
      });
      it(testString + " with a is undefined should be false", () => {
        expect(service.parse(testString)).toBeFalse();
        expect(service.parse(testString, undefined, { a: null })).toBeFalse();
        expect(service.parse(testString, undefined, { a: undefined })).toBeFalse();
        expect(service.parse(testString, undefined, { x: "x" })).toBeFalse();
        expect(service.parse(testString, undefined, {})).toBeFalse();
      });
    });
    const testStringsWithOutA = [
      "${a} IN 'A'",
      "${a} IN ('A')",
      "${a} IN ('b')",
      "${a} IN (' a')",
      "${a} IN ('a ')",
      "${a} IN ('')",
      "${a} IN (' ')",
      "${a} IN ('b', 'c')",
      "(${a} IN ('', 'b'))",
      "'A' IN ${a}",
      "('A') IN ${a}",
      "('b') IN ${a}",
      "(' a') IN ${a}",
      "('a ') IN ${a}",
      "('') IN ${a}",
      "(' ') IN ${a}",
      "('b', 'c') IN ${a}",
      "(('', 'b') IN ${a})",
    ];
    testStringsWithOutA.forEach((testString) => {
      it(testString + ' with const a = "a" should be false', () => {
        const a = "a";
        expect(service.parse(testString, undefined, { a })).toBeFalse();
      });
      it(testString + " with a is undefined should be false", () => {
        expect(service.parse(testString)).toBeFalse();
        expect(service.parse(testString, undefined, { a: null })).toBeFalse();
        expect(service.parse(testString, undefined, { a: undefined })).toBeFalse();
        expect(service.parse(testString, undefined, { x: "x" })).toBeFalse();
        expect(service.parse(testString, undefined, {})).toBeFalse();
      });
    });
  });

  describe("Includes for context data", () => {
    const testStringsWithA = ["${a} IN ${a}", "'a' IN ${a}", "('a') IN ${a}"];
    // eslint-disable-next-line no-sparse-arrays
    const as = ["a", ["a"], ["b", "a"], [undefined, "a"], [, "a"], [null, "a"]];
    testStringsWithA.forEach((testString) =>
      as.forEach((a) =>
        it(testString + " with const a = " + a.toString() + " should be true", () => {
          expect(service.parse(testString, undefined, { a })).toBeTrue();
        }),
      ),
    );

    const testStringsWithOutA = ["${a} IN ${b}", "'a' IN ${b}", "('a') IN ${b}"];
    testStringsWithOutA.forEach((testString) => {
      const a = "a";
      // eslint-disable-next-line no-sparse-arrays
      const bs = ["b", "", " ", ["b"], [, "b"], ["A"], [""], [" "], [], null, undefined, [null], [undefined], [["a"]]];
      bs.forEach((b) => {
        it(testString + ' with const a = "a" and const b = ' + b?.toString() + " should be false", () => {
          expect(service.parse(testString, undefined, { a })).toBeFalse();
        });
      });
    });

    const testStringsBroken = [
      "${a} iN ('a','b')",
      "${a} in ('a','b')",
      "${a} In ('a','b')",
      "${a} I ('a','b')",
      "${a} n ('a','b')",
      "${a} ('a')",
      "${a} IN ('a)",
      "${a} IN ('a'",
      "${a} IN ('a''b')",
      "{a} IN ('a')",
      "$a} IN ('a')",
      "${a IN ('a')",
      "${a} IN (('a')",
      "${a} IN ('a'))",
      "('a','b') iN ${a}",
      "${a} in ('a','b') in ${a}",
      "${a} In ('a','b') In ${a}",
      "${a} I ('a','b') I ${a}",
      "${a} n ('a','b') n ${a}",
      "('a') ${a}",
      "('a) IN ${a}",
      "('a' IN ${a}",
      "('a''b') IN ${a}",
      "('a') IN {a}",
      "('a') IN $a}",
      "('a') IN ${a",
      "(('a') IN ${a}",
      "('a')) IN ${a}",
      "${a IN ${a}",
      "${a} in ${a}",
    ];
    testStringsBroken.forEach((testString) =>
      it("Should fail on incomplete or broken syntax", () => {
        const a = "a";
        expect(() => service.parse(testString, undefined, { a })).toThrow();
      }),
    );
  });

  describe("Negation", () => {
    it("Negate true comparison should be false", () => {
      const a = "a";
      const testStrings = ["!('a' == 'a')", "!('a' != 'b')", "!('a' == ${a})"];
      testStrings.forEach((testString) => expect(service.parse(testString, undefined, { a })).toBeFalse());
    });
    it("Negate false comparison should be true", () => {
      const a = "a";
      const testStrings = ["!('a' == 'b')", "!('a' != 'a')", "!('a' != ${a})"];
      testStrings.forEach((testString) => expect(service.parse(testString, undefined, { a })).toBeTrue());
    });
    it("Negate true inclusion should be false", () => {
      const a = "a";
      const testStrings = ["!('a' IN ('a'))", "!(${a} IN ('a'))"];
      testStrings.forEach((testString) => expect(service.parse(testString, undefined, { a })).toBeFalse());
    });
    it("Negate false inclusion should be true", () => {
      const a = "a";
      const testStrings = ["!('a' IN ('b'))", "!(${a} IN ('b'))"];
      testStrings.forEach((testString) => expect(service.parse(testString, undefined, { a })).toBeTrue());
    });
    it("Negate false logic expression should be true", () => {
      const a = "a";
      const testStrings = ["!(('a' == 'b') && ('a' == 'b'))", "!(('a' == 'b') || ('a' == 'b'))"];
      testStrings.forEach((testString) => expect(service.parse(testString, undefined, { a })).toBeTrue());
    });
    it("Negate true logic expression should be false", () => {
      const a = "a";
      const testStrings = ["!(('a' == 'a') && ('a' == 'a'))", "!(('a' == 'a') || ('a' == 'b'))"];
      testStrings.forEach((testString) => expect(service.parse(testString, undefined, { a })).toBeFalse());
    });
  });

  describe("AND", () => {
    it("True comparisons or inclusions connected by AND should be true", () => {
      const testStrings = [
        "'a'=='a'&&'b'=='b'",
        "'a' == 'a'&&'b' == 'b'",
        "('a' == 'a') && ('b' == 'b')",
        "('a' == 'a') && ('b' == 'b') && ('c' == 'c')",
        "('a' == 'a') && ('a' IN ('a'))",
        "('a' IN ('a')) && ('a' IN ('a')) && ('a' IN ('a'))",
      ];
      testStrings.forEach((testString) => expect(service.parse(testString)).toBeTrue());
    });
    it("comparisons or inclusions connected by AND should be false if at least one is false", () => {
      const testStrings = [
        "'a'!='a'&&'b'=='b'",
        "'a' != 'a' && 'b' == 'b'",
        "('a' != 'a') && ('b' == 'b')",
        "('a' == 'a') && ('b' != 'b')",
        "('a' != 'a') && ('b' != 'b')",
        "('a' != 'a') && ('a' IN ('a'))",
        "('a' == 'a') && ('a' IN ('b'))",
        "('a' != 'a') && ('a' IN ('b'))",
        "('a' IN ('b')) && ('a' IN ('a'))",
        "('a' IN ('a')) && ('a' IN ('b'))",
        "('a' IN ('b')) && ('a' IN ('b'))",
        "('a' IN ('b')) && ('a' IN ('b'))",
      ];
      testStrings.forEach((testString) => expect(service.parse(testString)).toBeFalse());
    });
    it("should fail on incomplete or broken syntax", () => {
      const testStrings = [
        "('a' != 'a') & ('b' != 'b')",
        "('a' != 'a') &&& ('b' != 'b') && ('c' != 'c')",
        "('a' != 'a') & & ('a' IN ('b'))",
      ];
      testStrings.forEach((testString) => expect(() => service.parse(testString)).toThrow());
    });
  });

  describe("OR", () => {
    describe("Connecting expressions where at least one is true", () => {
      const testStrings = [
        "'a' == 'a' || 'b' == 'b'",
        "('a' == 'a') || ('b' == 'b')",
        "('a' == 'a') || ('b' == 'b') || ('c' == 'c')",
        "('a' != 'a') || ('b' == 'b')",
        "('a' == 'a') || ('b' != 'b')",
        "('a' == 'a') || ('a' IN ('a'))",
        "('a' != 'a') || ('a' IN ('a'))",
        "('a' == 'a') || ('a' IN ('b'))",
        "('a' IN ('a')) || ('a' IN ('a'))",
        "('a' IN ('b')) || ('a' IN ('a'))",
        "('a' IN ('a')) || ('a' IN ('b'))",
        "('a' IN ('a')) || ('a' IN ('b')) || ('a' IN ('b'))",
      ];
      testStrings.forEach((testString) => it(testString + " should be true", () => expect(service.parse(testString)).toBeTrue()));
    });
    describe("Connecting expressions where all are false", () => {
      const testStrings = [
        "'a' != 'a' || 'b' != 'b'",
        "('a' != 'a') || ('b' != 'b')",
        "('a' != 'a') || ('b' != 'b') || ('c' != 'c')",
        "('a' != 'a') || ('a' IN ('b'))",
        "('a' IN ('b')) || ('a' IN ('b')) || ('a' IN ('b'))",
      ];
      testStrings.forEach((testString) => it(testString + " should be true", () => expect(service.parse(testString)).toBeFalse()));
    });
    describe("Incomplete or broken syntax", () => {
      const testStrings = [
        "('a' != 'a') | ('b' != 'b')",
        "('a' != 'a') ||| ('b' != 'b') || ('c' != 'c')",
        "('a' != 'a') | | ('a' IN ('b'))",
      ];
      testStrings.forEach((testString) => it(testString + " should fail", () => expect(() => service.parse(testString)).toThrow()));
    });
  });

  /**
   * Better tests????
   */
  describe("Combining AND and OR", () => {
    const testStringsTrue = [
      "'a' == 'a' && 'a' == 'b' || 'b' == 'b'",
      "'a' == 'a' && 'a' == 'a' || 'a' == 'b'",
      "('a' == 'b' || 'a' == 'a') && ('a' == 'a')",
      "'a' == 'a' || ('a' == 'a' && 'a' == 'b')",
      "('a' == 'a') && (('a' == 'b') || ('b' == 'b'))",
      "('a' == 'a') && ('a' == 'a') || ('a' == 'b')",
      "('a' == 'b') || ('a' == 'a') && ('a' == 'a')",
      "('a' == 'a') || ('a' == 'a') && ('a' == 'b')",
    ];
    testStringsTrue.forEach((testString) => it(testString + " should be true", () => expect(service.parse(testString)).toBeTrue()));

    const testStringsFalse = [
      "'a' == 'a' && ('a' == 'b') || ('a' == 'b')",
      "('a' == 'b') && 'a' == 'a' || ('a' == 'b')",
      "('a' == 'b') || (('a' == 'b') && ('a' == 'a'))",
      "(('a' == 'b') || ('a' == 'a')) && ('a' == 'b')",
      "('a' == 'a' && 'a' == 'b') || ('a' == 'b')",
      "('a' == 'b') && ('a' == 'a' || 'a' == 'b')",
      "(('a' == 'b') || ('a' == 'b') && ('a' == 'a'))",
      "('a' == 'b') || ('a' == 'a') && ('a' == 'b')",
    ];
    testStringsFalse.forEach((testString) => it(testString + " should be false", () => expect(service.parse(testString)).toBeFalse()));
  });

  describe("Complex statment testing", () => {
    const structuralData = {
      a: "a",
      b: "b",
      c: undefined,
      d: null,
      A: {
        a: "a",
        b: "b",
      },
      B: {
        a: "Ba",
        c: undefined,
        d: null,
      },
      C: {
        a: ["a"],
        c: ["a", "b"],
      },
    };
    const a = "a";
    const b = "b";
    describe('With const a = "a"; const b = "b"; and structural data: ' + JSON.stringify(structuralData), () => {
      const testStringsTrue = [
        "${a} == a",
        "${a} IN C.a",
        "!(${b} IN C.a) ",
        "A.a IN C.a",
        "${b} == a || C.a != a",
        "!(a != ${b} && C.a IN a.a) || A.a IN ('x', 'y', 'a')",
      ];
      testStringsTrue.forEach((testString) =>
        it(testString + " should be true", () => expect(service.parse(testString, structuralData, { a, b })).toBeTrue()),
      );

      const testStringsFalse = [
        "${a} != a",
        "!(${a} IN C.a)",
        "${b} IN C.a ",
        "A.a == C.a",
        "!(${b} == a || C.a != a)",
        "!(a != ${b} && C.a IN A.a) || A.a IN ('x', 'y', 'z')",
      ];
      testStringsFalse.forEach((testString) =>
        it(testString + " should be false", () => expect(service.parse(testString, structuralData, { a, b })).toBeFalse()),
      );
    });
  });
});
