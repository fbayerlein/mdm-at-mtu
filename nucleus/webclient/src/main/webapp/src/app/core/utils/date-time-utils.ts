import { DateTime } from "luxon";

export function toMdmRestApiString(dateTime: DateTime) {
  return dateTime?.toUTC().toISO({ suppressMilliseconds: true });
}
