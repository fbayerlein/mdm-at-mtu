/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input } from "@angular/core";
import { StateService } from "@core/services/state.service.ts";

@Component({
  selector: "mdm-sharelink",
  templateUrl: "./sharelink.component.html",
})
export class SharelinkComponent {
  @Input()
  componentFilter = ".*";

  @Input()
  customStateCallback: () => unknown;

  link: string;

  constructor(private stateService: StateService) {}

  copyInputMessage(inputElement) {
    //inputElement.setSelectionRange(0, this.value.length)
    inputElement.select();
    document.execCommand("copy");
    inputElement.setSelectionRange(0, 0);
  }

  persistState() {
    const bookmarkUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;

    if (this.customStateCallback) {
      return this.stateService
        .persistCustomState(this.customStateCallback())
        .subscribe((s) => (this.link = bookmarkUrl + "?state=" + s.id));
    } else {
      return this.stateService.persistCurrentState(this.componentFilter).subscribe((s) => (this.link = bookmarkUrl + "?state=" + s.id));
    }
  }
}
