/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpErrorResponse } from "@angular/common/http";
import { ErrorHandler, Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { streamTranslate, TRANSLATE } from "../core/mdm-core.module";
import { MDMErrorResponse } from "./mdm-error-response";
import { MDMNotificationService } from "./mdm-notification.service";

@Injectable()
export class MDMErrorHandler extends ErrorHandler {
  msgsError: string =
    "Application Error$An application error has occurred." + " For a detailed error message please open your browser's developer console.";

  constructor(private notificationService: MDMNotificationService, private translateService: TranslateService) {
    super();
    streamTranslate(this.translateService, TRANSLATE("core.mdm-error-handler.strings-error-message")).subscribe(
      (msg: string) => (this.msgsError = msg),
    );
  }

  handleError(response: HttpErrorResponse, clientMessage?: string) {
    if (response && response.name === "HttpErrorResponse") {
      if (response.error && response.error.traceID) {
        this.handleMDMError(<MDMErrorResponse>response.error);
      } else if (response.status === 424) {
        this.notificationService.notifyError("Please relogin.", "The underlying datasource was disconnected.");
      } else {
        const detailMessage = this.extractDetails(response);
        this.notificationService.notifyError(this.msgsError.split("$")[0], this.msgsError.split("$")[1] + "\n" + detailMessage);
      }
    } else {
      super.handleError(response);
    }
  }

  handleMDMError(error: MDMErrorResponse, clientMessage?: string) {
    const cm = clientMessage || error.clientMessage;

    if (cm) {
      console.error("Client message:", cm, "Backend message:", error);
      this.notificationService.notifyMessage({ severity: "error", summary: cm, detail: error.message, data: error });
    } else {
      console.error("Backend message:", error);
      this.notificationService.notifyMessage({ severity: "error", summary: error.message, data: error });
    }
  }

  extractDetails(error) {
    let message = "";

    if (typeof error === "string") {
      message = error;
    } else if (typeof error === "object") {
      if (error.message !== undefined) {
        message = error.message;
      }
    }

    // check if it is a java stacktrace
    if (message.indexOf("\tat ") > -1 && message.indexOf(".java") > -1) {
      message = message.substr(0, message.indexOf("\tat "));
    } else if (message.indexOf("    at ") > -1) {
      // check if it is a angular stacktrace
      message = message.substr(0, message.indexOf("    at "));
    }

    return message;
  }
}
