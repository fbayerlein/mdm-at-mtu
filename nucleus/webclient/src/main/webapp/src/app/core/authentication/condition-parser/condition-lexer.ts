/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { buildLexer } from "typescript-parsec";
import { TokenKind } from "./token-kind.enum";

export const lexer = buildLexer([
  [true, /^(true)|(false)/g, TokenKind.BooleanLiteral],
  [true, /^'(.*?)'/g, TokenKind.StringLiteral],
  [true, /^(&&|\|\|)/g, TokenKind.LogicalConnector],
  [true, /^(==|!=)/g, TokenKind.Comparator],
  [true, /^!/g, TokenKind.Not],
  [true, /^IN/g, TokenKind.In],
  [true, /^IN_LIKE/g, TokenKind.In_Like],
  [true, /^\(/g, TokenKind.LParen],
  [true, /^\)/g, TokenKind.RParen],
  [true, /^,/g, TokenKind.Comma],
  [false, /^\s+/g, TokenKind.Space],
  [true, /^\${[ ]*\w+(\.\w+)*[ ]*}/g, TokenKind.ContextData],
  [true, /^(\w+|\*)(\.\w+)*/g, TokenKind.StructuralData],
]);
