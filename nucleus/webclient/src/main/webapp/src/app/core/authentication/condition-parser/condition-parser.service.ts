/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { alt, apply, expectEOF, expectSingleResult, kmid, list_sc, lrec_sc, Parser, rule, seq, str, tok, Token } from "typescript-parsec";
import { lexer } from "./condition-lexer";
import { TokenKind } from "./token-kind.enum";

@Injectable({
  providedIn: "root",
})
export class ConditionParserService {
  private booleanLiteralParser = apply(tok(TokenKind.BooleanLiteral), (x) => this.applyBooleanLiteral(x));
  private stringLiteralParser = apply(tok(TokenKind.StringLiteral), (x) => this.applyStringLiteral(x));
  private stringLiteralListParser: Parser<TokenKind, string[]> = kmid(str("("), list_sc(this.stringLiteralParser, str(",")), str(")"));
  private structuralDataParser = apply(tok(TokenKind.StructuralData), (x) => this.applyStructuralData(x));
  private contextDataParser = apply(tok(TokenKind.ContextData), (x) => this.applyContextData(x));
  private variableParser = alt(this.stringLiteralParser, this.structuralDataParser, this.contextDataParser);
  private comparatorParser = apply(seq(this.variableParser, tok(TokenKind.Comparator), this.variableParser), (x) =>
    this.applyComparator(x),
  );
  private includesParser = apply(
    seq(alt(this.variableParser, this.stringLiteralListParser), str("IN"), alt(this.variableParser, this.stringLiteralListParser)),
    (x) => this.applyIncludes(x),
  );

  private includesLikeParser = apply(
    seq(alt(this.variableParser, this.stringLiteralListParser), str("IN_LIKE"), alt(this.variableParser, this.stringLiteralListParser)),
    (x) => this.applyIncludesLike(x),
  );

  private COMP_EXP = rule<TokenKind, boolean>();
  private AND_EXP = rule<TokenKind, boolean>();
  private OR_EXP = rule<TokenKind, boolean>();
  private NEG_EXP = rule<TokenKind, boolean>();

  private structuralData: object;
  private contextData: object;

  constructor() {
    this.OR_EXP.setPattern(lrec_sc(this.AND_EXP, seq(str("||"), this.AND_EXP), this.applyLogicalConnector));
    this.AND_EXP.setPattern(lrec_sc(this.NEG_EXP, seq(str("&&"), this.NEG_EXP), this.applyLogicalConnector));
    this.NEG_EXP.setPattern(alt(this.COMP_EXP, apply(seq(str("!"), this.COMP_EXP), this.applyNegation)));
    this.COMP_EXP.setPattern(
      alt(
        this.comparatorParser,
        this.includesParser,
        this.includesLikeParser,
        this.booleanLiteralParser,
        kmid(str("("), this.OR_EXP, str(")")),
      ),
    );
  }

  public parse(input: string, structuralData?: object, contextData?: object) {
    this.structuralData = structuralData;
    this.contextData = contextData;
    return expectSingleResult(expectEOF(this.OR_EXP.parse(lexer.parse(input))));
  }

  private applyBooleanLiteral(token: Token<TokenKind>): boolean {
    return token.text === "true";
  }

  private applyStringLiteral(token: Token<TokenKind>): string {
    return token.text.substring(1, token.text.length - 1);
  }

  private applyStructuralData(token: Token<TokenKind>): undefined | string | string[] {
    return token.text.split(".").reduce((p, c) => (p ? p[c] : undefined), this.structuralData);
  }

  private applyContextData(token: Token<TokenKind>): undefined | string | string[] {
    return token.text
      .substring(2, token.text.length - 1)
      .trim()
      .split(".")
      .reduce((p, c) => (p ? p[c] : undefined), this.contextData);
  }

  private applyNegation(value: [Token<TokenKind>, boolean]): boolean {
    switch (value[0].text) {
      case "!":
        return !value[1];
      default:
        throw new Error(`Unknown negation operator: ${value[0].text}`);
    }
  }

  private applyComparator(value: [string | string[], Token<TokenKind>, string | string[]]): boolean {
    switch (value[1].text) {
      case "==":
        return value[0] === value[2];
      case "!=":
        return value[0] !== value[2];
      default:
        throw new Error(`Unknown comparator: ${value[1].text}`);
    }
  }

  private applyIncludes(value: [string | string[], Token<TokenKind>, string | string[]]): boolean {
    switch (value[1].text) {
      case "IN":
        if (!Array.isArray(value[0])) {
          value[0] = [value[0]];
        }
        if (!Array.isArray(value[2])) {
          value[2] = [value[2]];
        }
        return value[0].some((v) => value[2].includes(v));
      default:
        throw new Error(`Unknown includes operator: ${value[1].text}`);
    }
  }

  private applyIncludesLike(value: [string | string[], Token<TokenKind>, string | string[]]): boolean {
    switch (value[1].text) {
      case "IN_LIKE":
        if (!Array.isArray(value[0])) {
          value[0] = [value[0]];
        }
        if (!Array.isArray(value[2])) {
          value[2] = [value[2]];
        }
        return value[0].some((v) => this.includesLike(value[2], v));
      default:
        throw new Error(`Unknown includes operator: ${value[1].text}`);
    }
  }

  private includesLike(values: string | string[], val: string): boolean {
    if (values == null || val == null) {
      return false;
    }

    for (const arrayVal of values) {
      if (this.like(arrayVal, val)) {
        return true;
      }
    }

    return false;
  }

  private like(valWithWildcard, val) {
    // If we reach at the end of both strings,
    // we are done
    if (valWithWildcard.length == 0 && val.length == 0) return true;

    // Make sure that the characters after '*'
    // are present in second string.
    // This function assumes that the first
    // string will not contain two consecutive '*'
    if (valWithWildcard.length > 1 && valWithWildcard[0] == "*" && val.length == 0) return false;

    // If the first string contains '?',
    // or current characters of both strings match
    if (
      (valWithWildcard.length > 1 && valWithWildcard[0] == "?") ||
      (valWithWildcard.length != 0 && val.length != 0 && valWithWildcard[0] == val[0])
    )
      return this.like(valWithWildcard.substring(1), val.substring(1));

    // If there is *, then there are two possibilities
    // a) We consider current character of second string
    // b) We ignore current character of second string.
    if (valWithWildcard.length > 0 && valWithWildcard[0] == "*")
      return this.like(valWithWildcard.substring(1), val) || this.like(valWithWildcard, val.substring(1));

    return false;
  }

  private applyLogicalConnector(first: boolean, second: [Token<TokenKind>, boolean]): boolean {
    switch (second[0].text) {
      case "&&":
        return first && second[1];
      case "||":
        return first || second[1];
      default:
        throw new Error(`Unknown binary operator: ${second[0].text}`);
    }
  }
}
