/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

export class MDMIdentifier {
  source: string;
  type: string;
  id: string;

  constructor(source: string, type: string, id: string) {
    this.source = source;
    this.type = type;
    this.id = id;
  }
}

export function equalsMDMIdentifier(item1: MDMIdentifier, item2: MDMIdentifier) {
  return item1.source === item2.source && item1.type === item2.type && item1.id === item2.id;
}
