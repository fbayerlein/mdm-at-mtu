/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable, isDevMode } from "@angular/core";
import { throwError as observableThrowError } from "rxjs";

@Injectable()
export class HttpErrorHandler {
  handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      if (error.status === 424) {
        errMsg = this.logout();
      } else {
        try {
          const body = error || "";
          const err = JSON.stringify(body);
          errMsg = `${error.status} - ${error.statusText || ""} ${err}`;
        } catch (e) {
          if (isDevMode()) {
            errMsg = `${error.status} - ${error.statusText || ""} ${error.text()}`;
          } else {
            errMsg = `Please contact the administrator. Status code: ${error.status} - ${error.statusText || ""}`;
          }
        }
      }
    } else if (error.status === 424) {
      // error may be a HttpErrorResponse
      errMsg = this.logout();
    } else if (error.error != undefined && error.error.error instanceof SyntaxError) {
      // Result is the Login-Page?
      errMsg = error.error.text;
      if (this.isLoginPage(errMsg)) {
        window.location.reload();
      }
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return observableThrowError(errMsg);
  }

  private logout() {
    window.location.assign(window.origin + "/org.eclipse.mdm.nucleus/mdm/logout");
    return "Reconnect";
  }

  private isLoginPage(errMsg: string) {
    return errMsg.includes("Login</button>") || errMsg.includes("SAMLRequest");
  }
}
