/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { inject, Injectable, OnDestroy } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivateFn } from "@angular/router";
import { FunctionalRight, FunctionalRightConfig, JsonValue, Profile, Role } from "@core/authentication/model/model";
import { addErrorDescription } from "@core/core.functions";
import { Preference, PreferenceService } from "@core/preference.service";
import { NodeService } from "@navigator/node.service";
import { MessageService } from "primeng/api";
import { combineLatest, forkJoin, Observable, of, ReplaySubject, Subscription } from "rxjs";
import { catchError, map, shareReplay, startWith, switchMap, take, tap } from "rxjs/operators";
import { PropertyService } from "../property.service";
import { ConditionParserService } from "./condition-parser/condition-parser.service";
import { LoginDialogService } from "./login-dialog/login-dialog.service";

export function functionalRightGuard(key: string, contextData?: { [key: string]: string }): CanActivateFn {
  return (route: ActivatedRouteSnapshot) => {
    const cd = Object.assign(route.params ?? {}, contextData ?? {});
    return inject(AuthenticationService).hasFunctionalRight(key, cd);
  };
}

@Injectable({
  providedIn: "root",
})
export class AuthenticationService implements OnDestroy {
  private static readonly PREF_KEY = "rights_config";
  private rightsConfigPref: Observable<FunctionalRightConfig> = this.preferenceService.getPreference(AuthenticationService.PREF_KEY).pipe(
    map((prefs) => this.parsePref(prefs)),
    shareReplay(1),
  );

  private currentProfile = new ReplaySubject<Profile>();
  private sub = new Subscription();

  constructor(
    private http: HttpClient,
    private prop: PropertyService,
    private navigatorService: NodeService,
    private loginService: LoginDialogService,
    private preferenceService: PreferenceService,
    private conditionParser: ConditionParserService,
    private messenger: MessageService,
  ) {
    this.sub.add(
      combineLatest([
        this.navigatorService.datasourcesChanged.asObservable(),
        this.loginService.reloginSuccessful.asObservable().pipe(startWith(true)),
      ])
        .pipe(
          switchMap(() => this.readProfile()),
          tap((p) => this.currentProfile.next(p)),
        )
        .subscribe(),
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  public getCurrentProfile() {
    return this.currentProfile.asObservable();
  }

  public getCurrentUserName(sourceName?: string) {
    return this.getUserDetails(sourceName).pipe(map((user) => user?.username));
  }

  private getUserDetails(sourceName?: string) {
    return this.getCurrentProfile().pipe(
      map((profile) => (sourceName ? profile?.dataSourceUserDetails[sourceName] : profile.applicationUserDetails)),
    );
  }

  public isCurrentUserInRole(roles: string | string[], sourceName?: string) {
    if (roles === undefined) {
      return of(true);
    } else if (typeof roles === "string") {
      roles = [roles];
    }

    return this.getUserDetails(sourceName).pipe(map((user) => user?.roles.filter((role) => roles.includes(role)).length > 0));
  }

  public hasFunctionalRights(requests: { key: string; contextData?: { [key: string]: string } }[]) {
    return forkJoin(requests.map((r) => this.hasFunctionalRight(r.key, r.contextData))).pipe(
      map((rights) => {
        const rightsMap = new Map<string, boolean>();
        rights.forEach((hasRight, index) => {
          rightsMap.set(requests[index].key, hasRight);
        });
        return rightsMap;
      }),
    );
  }

  public hasFunctionalRight(key: string, contextData?: { [key: string]: string }) {
    return forkJoin([this.rightsConfigPref.pipe(take(1)), this.getCurrentProfile().pipe(take(1))]).pipe(
      map(([rightsConfig, user]) => {
        let hasRight = rightsConfig.default ?? user.applicationUserDetails.roles.includes(Role.Admin);
        const right = key.split(".").reduce<JsonValue<FunctionalRight>>((p, c) => (p ? p[c] : undefined), rightsConfig.functionalRights);
        if (this.isFunctionalRight(right)) {
          if (typeof right.condition === "boolean") {
            hasRight = right.condition;
          } else if (typeof right.condition === "string") {
            hasRight = this.conditionParser.parse(
              right.condition,
              { ...user.applicationUserDetails, ...user.dataSourceUserDetails },
              contextData,
            );
          }
        }
        return hasRight;
      }),
    );
  }

  private parsePref(prefs: Preference[]): FunctionalRightConfig {
    try {
      return prefs?.[0] ? JSON.parse(prefs[0].value) : { default: true };
    } catch {
      this.messenger.add({
        severity: "warn",
        summary: "Rights config preference is not parsable.",
      });
      return {};
    }
  }

  private readProfile() {
    return this.http
      .get<Profile>(this.prop.getUrl("mdm/profile/current"), {
        params: {
          roles: Object.values(Role),
        },
      })
      .pipe(catchError((e) => addErrorDescription(e, "Could not load current profile!")));
  }

  private isFunctionalRight(right: JsonValue<FunctionalRight>): right is FunctionalRight {
    return right != null && Object.prototype.hasOwnProperty.call(right, "condition");
  }
}
