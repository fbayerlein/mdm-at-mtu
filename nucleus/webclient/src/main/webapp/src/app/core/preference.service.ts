/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError, map, mergeMap, take } from "rxjs/operators";
import { addErrorDescription } from "./core.functions";
import { PropertyService } from "./property.service";

export class Scope {
  public static readonly SYSTEM = "SYSTEM";
  public static readonly SOURCE = "SOURCE";
  public static readonly USER = "USER";

  static toLabel(scope: string) {
    return scope.charAt(0).toUpperCase() + scope.slice(1).toLowerCase();
  }
}

export interface StatusPreferences {
  format?: { [key: string]: string };
}

export class Preference {
  scope: string;
  source?: string;
  user?: string;
  key: string;
  value: string;
  id: number;

  static sortByScope(p1: Preference, p2: Preference) {
    const getPriority = (scope: string) => {
      switch (scope) {
        case Scope.SYSTEM:
          return 1;
        case Scope.SOURCE:
          return 2;
        case Scope.USER:
          return 3;
        default:
          return 4;
      }
    };
    return getPriority(p1.scope) - getPriority(p2.scope);
  }

  constructor() {
    this.key = "";
  }
}

export class PreferenceConstants {
  public static readonly SEARCH_CASE_SENSITIVE = "search.case_sensitive";

  public static readonly ANNOUNCEMENT_MESSAGE = "announcement.message";
  public static readonly ANNOUNCEMENT_MESSAGE_REMIND = "announcement.dont_remind";
}

@Injectable()
export class PreferenceService {
  private prefEndpoint: string;

  private prop: PropertyService;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.prefEndpoint = _prop.getUrl("mdm/preferences");
    this.prop = _prop;
  }

  loadPreference424() {
    return this.http.get<any>(this.prop.getUrl("mdm/shoppingbasket/disconnected"));
  }

  getPreferenceForScope(scope: string, key?: string): Observable<Preference[]> {
    if (key === null || key === undefined) {
      key = "";
    }
    return this.http.get<any>(this.prefEndpoint + "?scope=" + scope + "&key=" + key).pipe(
      map((response) => <Preference[]>(response as any).preferences),
      take(1),
      catchError((e) => addErrorDescription(e, "Could not request preference for scope " + scope + " and key " + key)),
    );
  }

  getPreference(key?: string): Observable<Preference[]> {
    if (key === null || key === undefined) {
      key = "";
    }
    return this.http.get<any>(this.prefEndpoint + "?key=" + key).pipe(
      map((r) => <Preference[]>(r as any).preferences),
      take(1),
      catchError((e) => addErrorDescription(e, "Could not request preference for key " + key)),
    );
  }

  getPreferenceForUser(key: string, user: string): Observable<Preference[]> {
    if (key === null || key === undefined) {
      key = "";
    }
    return this.http.get<any>(this.prefEndpoint + "?key=" + key + "&user=" + user).pipe(
      map((r) => <Preference[]>(r as any).preferences),
      take(1),
      catchError((e) => addErrorDescription(e, "Could not request preference for key " + key + " and user " + user)),
    );
  }

  savePreference(preference: Preference): Observable<Preference> {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    preference.scope = preference.scope.toUpperCase();

    return this.http.put(this.prefEndpoint, JSON.stringify(preference), options).pipe(
      map((r) => <Preference>(r as any).preferences),
      take(1),
      catchError((e) => addErrorDescription(e, "Could not save preference!")),
    );
  }

  deletePreference(id: number) {
    return this.http.delete(this.prefEndpoint + "/" + id).pipe(
      take(1),
      catchError((e) => addErrorDescription(e, "Could not delete preference!")),
    );
  }

  deletePreferenceByScopeAndKey(scope: string, key: string) {
    return this.getPreferenceForScope(scope, key).pipe(
      mergeMap((p) => this.deletePreference(p.filter((n) => n.key === key)[0].id)),
      take(1),
    );
  }
}
