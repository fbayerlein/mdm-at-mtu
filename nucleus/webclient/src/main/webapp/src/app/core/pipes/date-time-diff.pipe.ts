import { Pipe, PipeTransform } from "@angular/core";
import { DateTime, DurationUnit } from "luxon";

@Pipe({
  name: "luxonDiff",
})
export class DateTimeDiffPipe implements PipeTransform {
  transform(a: DateTime, b: DateTime, unit: DurationUnit): number {
    return a && b ? a.diff(b, unit)[unit] : undefined;
  }
}
