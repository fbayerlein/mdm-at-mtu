import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { DateTime, SystemZone, Zone } from "luxon";
import { Calendar } from "primeng/calendar";
import { iif, map, Observable, of, Subscription, switchMap, tap } from "rxjs";
import { TimezoneService } from "src/app/timezone/timezone-service";

/**
 * The MdmPCalendarWrapperComponent encapsulates the PrimeNg calendar component
 * and handles the latter's lack of time zone support.
 * The wrapper component is working with the selected time zone for openMDM5 Web.
 *
 * PrimeNg calender is allways working with JavaScript {@link Date} in client time zone
 * and offers no other time zone support. As a workarround the wrapper manipulates the
 * zoned input {@link DateTime} before it is passed to the PrimeNg calendar. First the
 * time zone is shifted to client timezone. Then the difference of the offsets of client
 * and selected time zones is added.
 *
 * Hence, the PrimeNg calendar is working with a technically wrong date but it appears
 * to be consistent for the user.
 *
 * Before output the wrapper reverts the date manipulations. However, the time zone of
 * the output date is set to selected time zone for openMDM5 Web (or optionally explicitly given timezone),
 * which might differ from the input dates time zone.
 *
 * **Note:** For now the wrapper implements the PrimeNg Calender API only to an extend
 * that was neccessary to fullfill requirements at time of implementation.
 */
@Component({
  selector: "mdm-p-calendar-wrapper",
  templateUrl: "./mdm-p-calendar-wrapper.component.html",
})
export class MdmPCalendarWrapperComponent implements OnChanges, OnDestroy, OnInit {
  @ViewChild(Calendar) calendar: Calendar;

  @Input() public showTime: boolean;
  @Input() public showSeconds: boolean;
  @Input() public showIcon: boolean;
  @Input() public inputId: string;
  @Input() public placeholder: string;
  @Input() public styleClass: object;
  @Input() public style: object;
  @Input() public inputStyle: object;
  @Input() public minDate: DateTime;
  @Input() public maxDate: DateTime;
  @Input() public appendTo: string;
  @Input() public disabled: boolean;
  @Input() public hideOnDateTimeSelect: boolean;
  @Input() public showButtonBar: boolean;
  // optional: Overrides openMdm5 time zone
  @Input() public zone: string | Zone | undefined;

  //Support for luxon dateTime model
  @Input() public dateTime: DateTime;
  @Output() public dateTimeChange = new EventEmitter<DateTime>();
  @Output() public selectDateTime = new EventEmitter<{ value: DateTime }>();

  public pDate: Date;
  public pMinDate: Date;
  public pMaxDate: Date;

  public hourFormat: Observable<string>;

  private sub = new Subscription();
  private timezoneSub = new Subscription();

  constructor(private timezoneService: TimezoneService, private translate: TranslateService) {}

  ngOnInit(): void {
    // Listen to time zone changes for readjusting the date
    this.initClienTimeZoneMode();
    // Manual check required since PrimeNg does not check for changes in locale / hour format.
    this.sub.add(this.translate.onLangChange.subscribe(() => setTimeout(() => this.calendar.updateInputfield())));
    this.hourFormat = this.timezoneService.getDateTimeConfigChanged().pipe(
      map(() => this.timezoneService.getHourFormatType()),
      switchMap((format) => iif(() => format === "auto", this.translate.stream("primeNg.hourFormat"), of(format === "h11" ? "12" : "24"))),
      tap(() => setTimeout(() => this.calendar.updateInputfield())),
    );
  }

  private initClienTimeZoneMode() {
    this.timezoneSub.unsubscribe();
    this.timezoneSub.add(
      this.timezoneService.getTimeZone().subscribe((zone) => {
        this.init(zone);
      }),
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (Object.prototype.hasOwnProperty.call(changes, "zone")) {
      if (changes.zone.currentValue) {
        // Stop listening to client time zone via service and override time zone with given value
        this.timezoneSub.unsubscribe();
        this.init(changes.zone.currentValue);
      } else {
        this.initClienTimeZoneMode();
      }
    } else {
      if (Object.prototype.hasOwnProperty.call(changes, "dateTime")) {
        this.pDate = this.toClientDate(changes.dateTime.currentValue);
      }
      if (Object.prototype.hasOwnProperty.call(changes, "minDate")) {
        this.pMinDate = this.toClientDate(changes.minDate.currentValue);
      }
      if (Object.prototype.hasOwnProperty.call(changes, "maxDate")) {
        this.pMaxDate = this.toClientDate(changes.maxDate.currentValue);
      }
    }
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.timezoneSub.unsubscribe();
  }

  private init(zone: string | Zone) {
    this.zone = zone;
    this.pDate = this.toClientDateJs(this.dateTime);
    this.pMinDate = this.toClientDateJs(this.minDate);
    this.pMaxDate = this.toClientDateJs(this.maxDate);
  }

  /**
   * Listener to forward select event
   * @param event
   */
  onSelect(event: { value: Date }) {
    this.dateTimeChange.emit(this.toMDMDateTime(this.pDate));
    this.selectDateTime.emit({ value: this.toMDMDateTime(event.value) });
  }

  /**
   * Converts luxon dateTime (in selected mdm-timezone) to a java script date shifted by timezone offset such
   * that it appears to be the same but in client timezone
   * @param dateTime
   * @returns
   */
  private toClientDate(dateTime: DateTime) {
    return this.toClientDateJs(dateTime);
  }

  /**
   * Converts luxon dateTime (in selected mdm-timezone) to a java script date shifted by timezone offset such
   * that it appears to be the same but in client timezone
   *
   * @param dateTime the input dateTime
   * @param minutes the offset dif
   * @returns js date in client time zone shifted by offset
   */
  private toClientDateJs(dateTime: DateTime): Date {
    const minutes = this.getTimeZoneOffsetDiff(this.zone, dateTime);
    return dateTime?.setZone(new SystemZone())?.plus({ minutes }).toJSDate() ?? null;
  }

  /**
   * Converts shifted java script date back to luxon dateTime (in selected mdm timezone).
   * @param date
   * @returns
   */
  private toMDMDateTime(date: Date) {
    const minutes = this.getTimeZoneOffsetDiff(this.zone, DateTime.fromJSDate(date));
    return DateTime.fromJSDate(date, { zone: this.zone }).minus({ minutes });
  }

  /**
   * Evaluates difference between selected timezon in MDM and clients timezone
   * @returns
   */
  private getTimeZoneOffsetDiff(zone: string | Zone, dateTime: DateTime) {
    const dt = dateTime ?? DateTime.now();
    const clientOffset = dt.setZone(SystemZone.instance).offset; // what prime uses
    const mdmOffset = dt.setZone(zone).offset; // mdm timezone offset
    const offsetDiff = mdmOffset - clientOffset;
    return offsetDiff;
  }
}
