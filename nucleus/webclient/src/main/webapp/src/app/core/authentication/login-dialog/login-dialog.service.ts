/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { PropertyService } from "@core/property.service";
import { Observable, Subject } from "rxjs";

@Injectable({ providedIn: "root" })
export class LoginDialogService {
  public showDialogEvent = new Subject<boolean>();
  public showOnlyInfoDialogEvent = new Subject<boolean>();
  public reloginSuccessful = new Subject<boolean>();

  constructor(private http: HttpClient, private propertyService: PropertyService) {}

  public showDialog(): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      this.showDialogEvent.next(true);
      this.reloginSuccessful.subscribe((r) => observer.next(r));
    });
  }

  public showOnlyInfoDialog(): Observable<boolean> {
    return new Observable<boolean>(() => {
      this.showOnlyInfoDialogEvent.next(true);
    });
  }

  public login(username: string, password: string) {
    const formData = new HttpParams().set("j_username", username).set("j_password", password);

    return this.http.post(this.propertyService.getUrl("j_security_check"), formData, { responseType: "text", observe: "response" });
  }
}
