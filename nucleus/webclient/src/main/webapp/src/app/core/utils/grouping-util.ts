export function groupBy<K, T>(data: T[], aggregation: ((t: T) => K) | keyof T) {
  return data?.reduce((p, c) => {
    const key = aggregation instanceof Function ? aggregation(c) : (c[aggregation] as any as K);
    if (!p.has(key)) {
      p.set(key, []);
    }
    p.get(key).push(c);
    return p;
  }, new Map<K, T[]>());
}
