/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of, ReplaySubject, Subscription } from "rxjs";
import { catchError, map, mergeMap, shareReplay, tap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class MdmLocalizationService {
  private contextUrl: string;

  private publisher = new ReplaySubject<object>(1);
  // cache for translationMaps, maped to source name
  private replayer: Observable<{ [key: string]: Observable<object> }>;
  private defaultMimeTypes: Observable<{ [sourceName: string]: Observable<{ [key: string]: string }> }>;

  // cache for index of language per environment
  private languageIndices: { [env: string]: { [langCode: string]: number } } = {};

  private lastEnvironment: string;

  private nodeServiceSubscription: Subscription;

  constructor(
    private http: HttpClient,
    private prop: PropertyService,
    private translateService: TranslateService,
    private nodeService: NodeService,
  ) {
    // initialize translations for each present environment
    this.initializeTranslations();
    this.subscribeToDatasourceChanges();

    this.contextUrl = this.prop.getUrl("mdm/environments");

    this.defaultMimeTypes = this.nodeService.getNodes().pipe(
      map((envs) =>
        envs.reduce((previous, current) => {
          previous[current.sourceName] = this.http
            .get(this.contextUrl + "/" + current.sourceName + "/mdmlocalizations/mimetypes")
            .pipe(shareReplay(1)) as Observable<{ [key: string]: string }>;
          return previous;
        }, {} as { [sourceName: string]: Observable<{ [type: string]: string }> }),
      ),
      shareReplay(1),
      catchError((e) => addErrorDescription(e, "Could not load localization mimetypes!")),
    );
    // listener for language change on ngx-translate.
    this.translateService.onLangChange.subscribe(() => this.emitTranslations());
  }

  subscribeToDatasourceChanges() {
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe(() => this.initializeTranslations());
  }

  /*
   * initialize translations for each present environment
   */
  initializeTranslations() {
    this.replayer = this.nodeService.getNodes().pipe(
      tap((envs) => this.initTranslationIndices(envs)),
      map((envs) =>
        envs.reduce((previous, current) => {
          previous[current.sourceName] = this.loadTranslationMap(current.sourceName);
          return previous;
        }, {}),
      ),
      shareReplay(1),
    );
  }

  /**
   * Reloads translation map if none is present or present translations refer to other source.
   * Returns an Observable holding the current translations.
   * @param environment
   */
  public readTranslations(environment?: string) {
    if (environment != undefined && environment.length > 0) {
      this.lastEnvironment = environment;
    }
    this.emitTranslations();
    return this.publisher.asObservable();
  }

  public getLanguageIndex(sourceName?: string) {
    const src = sourceName || this.lastEnvironment;
    if (src != undefined && src.length > 0 && this.languageIndices[src]) {
      return this.languageIndices[src][this.translateService.currentLang] || this.languageIndices[src][this.translateService.defaultLang];
    }
  }

  public getLanguages() {
    return of(["en", "de"]);
    /* TODO: cannot accessing the environments before setting the active datasources; thus hardcoding the languages 
    return this.nodeService.getNodes().pipe(
      map(envs => Array.from(new Set(envs.map(env => this.extractLanguageCodes(env))
        .filter(codes => codes != undefined)
        .reduce((prev, current) => prev.concat(current), [])))
      )
    )*/
  }

  public getDefaultMimeType(type: string, sourceName?: string): Observable<string> {
    return this.defaultMimeTypes.pipe(
      mergeMap((mimeTypesByEnv) => (sourceName ? mimeTypesByEnv[sourceName] : mimeTypesByEnv[Object.keys(mimeTypesByEnv)[0]])),
      map((m) => m[type] || type),
    );
  }

  /**
   * Last translationMap is sent again. This triggers changeDetection for Observable in async pipe.
   * Hence the mdmTranslation pipe does not need to be impure and still reacts on language changes via ngx.
   */
  private emitTranslations() {
    if (this.replayer != undefined && this.lastEnvironment != undefined) {
      this.replayer
        .pipe(
          mergeMap((replay) => replay?.[this.lastEnvironment]),
          tap((translations) => this.publisher.next(translations)),
        )
        .subscribe();
    }
  }

  /**
   * Dispatches network request for translation map and caches response-observable in the replayer-map.
   * @param environment
   */
  public loadTranslationMap(environment: string) {
    const path = this.contextUrl + "/" + environment + "/mdmlocalizations";
    return this.http.get(path, { headers: { Accept: "application/json+packed" } }).pipe(
      catchError((e) => addErrorDescription(e, "Could not request localizations!")),
      shareReplay(1),
    );
  }

  private initTranslationIndices(environments: Node[]) {
    environments.forEach(
      (env) =>
        (this.languageIndices[env.sourceName] = this.extractLanguageCodes(env).reduce((previous, current, index) => {
          previous[current] = index;
          return previous;
        }, {})),
    );
  }

  private extractLanguageCodes(environment: Node) {
    const aliases = environment.attributes.find((attr) => attr.name === "MeaningOfAliases");

    const arr = <string[]>aliases.value;

    const retVal = [];

    for (const str of arr) {
      retVal.push(str.replace(",language=", ""));
    }

    return retVal;
  }
}
