/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { PropertyService } from "@core/property.service";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { forkJoin as observableForkJoin, Observable } from "rxjs";
import { catchError, map, mergeMap, publishReplay, refCount } from "rxjs/operators";
import { Localization } from "./localization";

@Injectable()
export class LocalizationService {
  private _nodeUrl: string;

  private cache: Observable<Localization[]>;

  constructor(
    private http: HttpClient,
    private _prop: PropertyService,
    private _node: NodeService,
    private notificationService: MDMNotificationService,
  ) {
    this._nodeUrl = _prop.getUrl("mdm/environments");
  }

  // Caches valueLists if cache is empty. Then returns observable containing cached valueLists.
  getLocalizations() {
    if (!this.cache) {
      this.cache = this._node.getNodes(undefined).pipe(
        mergeMap((envs) => this.initLocalizations(envs)),
        publishReplay(1),
        refCount(),
      );
    }
    return this.cache;
  }

  private initLocalizations(envs: Node[]) {
    return observableForkJoin(envs.map((env) => this.getLocalization(env))).pipe(map((locs) => locs.reduce((a, b) => a.concat(b), [])));
  }

  private getLocalization(node: Node): Observable<Localization[]> {
    let url = this._nodeUrl + "/" + node.sourceName;
    if (node.sourceType === "Environment") {
      url = url + "/localizations?all=true";
    } else {
      url = url + "/" + node.type.toLowerCase() + "s/localizations";
    }
    return this.get(url);
  }

  private get(url: string) {
    return this.http.get<any>(url).pipe(
      map((res) => <Localization[]>res.data),
      catchError((e) => addErrorDescription(e, "Could not request localizations!")),
    );
  }
}
