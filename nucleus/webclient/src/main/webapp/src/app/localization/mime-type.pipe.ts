/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Pipe, PipeTransform } from "@angular/core";
import { Attribute } from "@navigator/node";

@Pipe({
  name: "mimeType",
})
export class MimeTypePipe implements PipeTransform {
  transform(node: { attributes: Attribute[] }) {
    if (node != undefined) {
      const attr = node.attributes.find((attr) => attr.name === "MimeType");
      if (attr) {
        return attr.value as string;
      }
    }
  }
}
