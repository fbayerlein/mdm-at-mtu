/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Pipe, PipeTransform } from "@angular/core";
import { map } from "rxjs";
import { LocalizationService } from "../localization/localization.service";
import { Localization } from "./localization";

@Pipe({
  name: "mdmdatasourcetranslate",
  pure: false,
})
export class MDMDataSourceTranslationPipe implements PipeTransform {
  private translation: string;

  constructor(private localService: LocalizationService) {}

  transform(type: string, attr?: string): any {
    return type ? this.getTrans(type, attr) : type;
  }

  private getTrans(type: string, attr: string) {
    this.localService
      .getLocalizations()
      .pipe(map((locs) => this.getTranslation(locs, type, attr)))
      .subscribe(
        (t) => (this.translation = t),
        (error) => (this.translation = type),
      );

    return this.translation;
  }

  private getTranslation(locs: Localization[], type: string, attr: string) {
    const trans = attr ? type + "." + attr : type;
    const temp = locs.find((l) => l.name === trans);
    return temp ? temp.localizedName : trans;
  }
}
