/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Pipe, PipeTransform } from "@angular/core";
import { combineLatest, of } from "rxjs";
import { map } from "rxjs/operators";
import { MdmLocalizationService } from "./mdm-localization.service";

@Pipe({
  name: "mdmTranslate",
})
export class MdmTranslatePipe implements PipeTransform {
  private static readonly ATTRIBUTE_KEY = "attr";
  private static readonly VALUE_KEY = "val";
  private static readonly MIME_TYPE_KEY = "mt";

  constructor(private localizationService: MdmLocalizationService) {}

  transform(mimeTypeOrType: string, sourceName?: string, attributeName?: string, valueName?: string) {
    const defaultResponse = attributeName || mimeTypeOrType;
    if (mimeTypeOrType === undefined || mimeTypeOrType === null) {
      return of(defaultResponse);
    } else {
      return combineLatest([
        this.localizationService.readTranslations(sourceName),
        this.localizationService.getDefaultMimeType(mimeTypeOrType),
      ]).pipe(
        map(([translationMap, mimeType]) => this.lookUpTranslations(mimeType, attributeName, valueName, translationMap)),
        map((translations) => this.getCurrent(translations, sourceName) || defaultResponse),
      );
    }
  }

  /**
   * Should select the translation from array by currently selected language in ngx-translate service ( or app component)
   * @param translations
   */
  private getCurrent(translations: string[], sourceName?: string) {
    const index = this.localizationService.getLanguageIndex(sourceName);
    if (index != undefined) {
      return translations[index];
    }
  }

  /**
   * Returns translation array of all languages in given Translation map.
   *
   * @param mimeType        mimetype
   * @param attributeName   optional: attribute name
   * @param valueName       optional: value name
   * @param translationMap  hierachiecal map with all translations
   */
  private lookUpTranslations(mimeType: string, attributeName: string, valueName: string, translationMap: any) {
    let result: string[];
    if (translationMap != undefined) {
      if (attributeName != undefined) {
        const attrName = attributeName.toLocaleLowerCase();
        if (valueName != undefined) {
          /**
           * case: attributeName and valueName are defined, key is supposed to be of the form:
           * val/{mimetype}/{valueName}/{attributeName}
           * eg: "val/application%2Fx-asam.aoany.cattestequipmentattr/label/blocksize"
           */
          result = this.lookUpValue(valueName, translationMap, mimeType, attrName);
        } else {
          /**
           * case: attributeName is defined, but valueName is not defined, key is supposed to be of the form:
           * attr/{valueName}/{attributeName}
           * eg: "attr/application%2Fx-asam.aotestsequencepart.to_start"
           */
          result = this.lookUpAttribute(translationMap, mimeType, attrName);
        }
      } else {
        /**
         * case: Neither attributeName nor valueName is defined, key is supposed to be of the form:
         * mt/{mimetype}
         * eg: "mt/application%2Fx-asam.aoany.cattestequipmentattr"
         */
        result = this.lookUpMimeType(translationMap, mimeType);
      }
    }
    return result;
  }

  /**
   * Returns mimeType translation array for given mimeType.
   * Tries to find exact match on mimeType, if none found tries to find matching super-mimetype.
   * @param translationMap looks up
   * @param mimeType
   */
  private lookUpMimeType(translationMap: any, mimeType: string) {
    const splitIndex = mimeType.lastIndexOf(".");
    let result: string[] = [];
    if (
      translationMap.hasOwnProperty(MdmTranslatePipe.MIME_TYPE_KEY) &&
      translationMap[MdmTranslatePipe.MIME_TYPE_KEY].hasOwnProperty(mimeType)
    ) {
      result = translationMap[MdmTranslatePipe.MIME_TYPE_KEY][mimeType];
    } else if (splitIndex > -1) {
      result = this.lookUpMimeType(translationMap, mimeType.substring(0, splitIndex));
    }
    return result;
  }

  /**
   * Returns attribute translation array for given mimeType.
   * Tries to find exact match on mimeType, if none found tries to find matching super-mimetype.
   * @param translationMap
   * @param mimeType
   * @param attrName
   */
  private lookUpAttribute(translationMap: any, mimeType: string, attrName: string) {
    const splitIndex = mimeType.lastIndexOf(".");
    let result: string[] = [];
    if (
      translationMap.hasOwnProperty(MdmTranslatePipe.ATTRIBUTE_KEY) &&
      translationMap[MdmTranslatePipe.ATTRIBUTE_KEY].hasOwnProperty(mimeType) &&
      translationMap[MdmTranslatePipe.ATTRIBUTE_KEY][mimeType].hasOwnProperty(attrName)
    ) {
      result = translationMap[MdmTranslatePipe.ATTRIBUTE_KEY][mimeType][attrName];
    } else if (splitIndex > -1) {
      result = this.lookUpAttribute(translationMap, mimeType.substring(0, splitIndex), attrName);
    }
    return result;
  }

  /**
   * Returns value translation array for exact given mimeType.
   * @param valueName
   * @param translationMap
   * @param mimeType
   * @param attrName
   */
  private lookUpValue(valueName: string, translationMap: any, mimeType: string, attrName: string) {
    let result: string[] = [];
    const valName = valueName.toLocaleLowerCase();
    if (
      translationMap.hasOwnProperty(MdmTranslatePipe.VALUE_KEY) &&
      translationMap[MdmTranslatePipe.VALUE_KEY].hasOwnProperty(mimeType) &&
      translationMap[MdmTranslatePipe.VALUE_KEY][mimeType].hasOwnProperty(valName) &&
      translationMap[MdmTranslatePipe.VALUE_KEY][mimeType][valName].hasOwnProperty(attrName)
    ) {
      result = translationMap[MdmTranslatePipe.VALUE_KEY][mimeType][valName][attrName];
    }
    return result;
  }
}
