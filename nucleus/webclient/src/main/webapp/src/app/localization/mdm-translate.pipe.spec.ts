/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { TestBed } from "@angular/core/testing";
import { of } from "rxjs";
import { MdmLocalizationService } from "./mdm-localization.service";
import { MdmTranslatePipe } from "./mdm-translate.pipe";

describe("MdmTranslatePipe", () => {
  let mdmLocalizationService: MdmLocalizationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MdmTranslatePipe],
      providers: [
        {
          provide: MdmLocalizationService,
          useValue: jasmine.createSpyObj(MdmLocalizationService, ["readTranslations", "getDefaultMimeType", "getLanguageIndex"]),
        },
        MdmTranslatePipe,
      ],
    }).compileComponents();

    mdmLocalizationService = TestBed.inject(MdmLocalizationService);
  });

  it('translates tpltestequipment name"', (done) => {
    const loc = TestBed.inject(MdmLocalizationService) as jasmine.SpyObj<MdmLocalizationService>;

    loc.readTranslations.and.returnValue(
      of({
        mt: {
          "application/x-asam.aotestequipmentpart.device.datalogger": ["Device", "Gerät"],
        },
      }),
    );

    loc.getDefaultMimeType.and.returnValue(of("application/x-asam.aotestequipmentpart.device.datalogger"));
    loc.getLanguageIndex.and.returnValue(1);

    const pipe = TestBed.inject(MdmTranslatePipe);
    pipe.transform("application/x-asam.aotestequipmentpart.device.datalogger", "MDM").subscribe((result) => {
      expect(result).toBe("Gerät");
      done();
    });
  });

  it('translates tpltestequipment attribute"', (done) => {
    const loc = TestBed.inject(MdmLocalizationService) as jasmine.SpyObj<MdmLocalizationService>;

    loc.readTranslations.and.returnValue(
      of({
        attr: {
          "application/x-asam.aotestequipmentpart.device": {
            serial_nr: ["Serial number", "Seriennummer"],
          },
        },
      }),
    );
    loc.getDefaultMimeType.and.returnValue(of("application/x-asam.aotestequipmentpart.device"));
    loc.getLanguageIndex.and.returnValue(1);

    const pipe = TestBed.inject(MdmTranslatePipe);
    pipe.transform("application/x-asam.aotestequipmentpart.device", "MDM", "serial_nr").subscribe((result) => {
      expect(result).toBe("Seriennummer");
      done();
    });
  });
});
