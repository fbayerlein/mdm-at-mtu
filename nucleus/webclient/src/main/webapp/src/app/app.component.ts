/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { AuthenticationService } from "@core/authentication/authentication.service";
import { Profile } from "@core/authentication/model/model";
import { MdmLocalizationService } from "@localization/mdm-localization.service";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import ISO6391 from "iso-639-1";
import { PrimeNGConfig, SelectItem, SelectItemGroup } from "primeng/api";
import { MultiSelect } from "primeng/multiselect";
import { Observable, Subscription } from "rxjs";
import { concatMap, finalize, map, startWith, tap } from "rxjs/operators";
import { VERSION } from "../environments/version";
import { AvailableDatasourceService } from "./available-datasource/available-datasource.service";
import { Preference, PreferenceService, Scope } from "./core/preference.service";
import { SchedulerService } from "./scheduler/scheduler.service";
import { HourFormatType, TimezoneService } from "./timezone/timezone-service";

@Component({
  selector: "mdm-app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit, OnDestroy {
  public static readonly DEFAULT_LANG_CODE = "en";

  public version = VERSION;
  private sub: Subscription;

  public languages: SelectItem[] = [];
  public dateFormatTypes: SelectItem[] = [
    { label: "Absolute", value: "absolute" },
    { label: "Relative", value: "relative" },
    { label: "Both", value: "combined" },
  ];
  public selectedDateFormatType: "absolute" | "relative" | "combined" = "absolute";
  public selectedIsoFormat = false;
  public hourFormatTypes: SelectItem<HourFormatType>[] = [
    { label: "Auto", value: "auto" },
    { label: "12h", value: "h11" },
    { label: "24h", value: "h23" },
  ];
  public selectedHourFormatType: HourFormatType;

  public timezones: SelectItemGroup[] = [];
  public selectedLanguage: string;
  public selectedTimezone: string;

  public datasources: SelectItem[] = [];
  public selectedDatasources: string[] = [];
  public standardDatasources: string[];
  private preferenceName = "app.components.standard_datasource";
  public selectedPreferenceRow: SelectItem[] = [];
  public displayDatasourceDialog = false;

  public canShowApplication = false;

  public links: { label: string; routerLink: string; visible: boolean }[];
  public displayAboutDialog = false;
  public user: Profile = null;
  public rolesTooltip: string;

  @ViewChild("datasourcemultiselect")
  public datasourceMultiselect: MultiSelect;

  constructor(
    private translate: TranslateService,
    private localizationService: MdmLocalizationService,
    private authService: AuthenticationService,
    private nodeService: NodeService,
    private preferenceService: PreferenceService,
    private dsService: AvailableDatasourceService,
    private schedulerService: SchedulerService,
    private timezoneService: TimezoneService,
    private translateService: TranslateService,
    private authenticationService: AuthenticationService,
    private primeNgConfig: PrimeNGConfig,
  ) {}

  ngOnInit() {
    this.initLanguageSettings();

    this.initStandarDatasource(this.loadPreference(this.preferenceName)).subscribe((value) => {
      this.standardDatasources = value;
      this.initDataSources();
    });

    this.authService.getCurrentProfile().subscribe((value) => {
      this.user = value;
      this.rolesTooltip = value?.applicationUserDetails?.roles.join(", ");
      //this.initLinks();
    });

    const rightKeys = [{ key: "administration.show" }, { key: "scheduler.show" }];
    this.sub = this.authenticationService
      .hasFunctionalRights(rightKeys)
      .pipe(
        concatMap((rights) =>
          this.translateService.onLangChange.pipe(
            startWith(""),
            tap(() => this.initLinks(rights)),
          ),
        ),
      )
      .subscribe();

    this.selectedDateFormatType = this.timezoneService.getDateFormatType();
    this.selectedHourFormatType = this.timezoneService.getHourFormatType();
    this.selectedIsoFormat = this.timezoneService.getIsoFormat();

    this.timezoneService.getTimeZone().subscribe((timezone) => {
      this.initTimezoneSetting(timezone);
    });

    this.translate.onLangChange.subscribe(() => {
      this.primeNgConfig.setTranslation(this.translate.instant("primeNg"));
      this.initTimezoneSetting();
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  private initLinks(rights: Map<string, boolean>) {
    const links = [
      {
        label: this.translateService.instant("administration.link") as string,
        routerLink: "/administration",
        visible: rights.get("administration.show"),
      },
    ];
    this.schedulerService
      .isActive()
      .pipe(
        tap((active) => {
          if (active) {
            links.push({
              label: this.translateService.instant("importscheduler.link") as string,
              routerLink: "/scheduler",
              visible: rights.get("scheduler.show"),
            });
          }
        }),
        finalize(
          () => (this.links = links), //.filter((link) => link.roles.some((role) => this.user.applicationUserDetails.roles.includes(role)))),
        ),
      )
      .subscribe();
  }

  private initDataSources() {
    this.dsService.getAvailableDatasource().subscribe((value) => {
      this.datasources = value.map((ds) => {
        return { label: ds, value: ds };
      });
      if (this.standardDatasources === null || this.standardDatasources.length === 0) {
        this.displayDatasourceDialog = true;
      } else {
        this.datasources.filter((sourceName) =>
          this.standardDatasources.forEach((sp) => (sp === sourceName.label ? this.selectedDatasources.push(sourceName.value) : null)),
        );

        this.selectedPreferenceRow = this.selectedDatasources.map((sourceName) => {
          return { label: sourceName, value: sourceName };
        });
        this.onSelectDatasources();
      }
    });

    this.nodeService.datasourcesChanged.subscribe((sources) => {
      this.selectedDatasources = sources;
    });
  }

  private initTimezoneSetting(timezone?: string) {
    this.timezones = [
      {
        label: this.translate.instant("app.previously-selected"),
        items: this.timezoneService.getPreviouslySelected().map((tzn) => {
          return { label: tzn, value: tzn };
        }),
      },
      {
        label: this.translate.instant("app.timezones"),
        items: this.timezoneService.getRestOfAvailableTimezones().map((tzn) => {
          return { label: tzn, value: tzn };
        }),
      },
    ];
    this.selectedTimezone = timezone;
  }

  private initLanguageSettings() {
    this.localizationService.getLanguages().subscribe((langs) => {
      this.languages = Array.from(langs).map((l) => {
        return { label: ISO6391.getNativeName(l), value: l };
      });
      this.translate.langs = this.languages.map((i) => i.value);

      this.translate.setDefaultLang(AppComponent.DEFAULT_LANG_CODE);
      const browserLang = this.translate.getBrowserLang();
      if (this.translate.langs.findIndex((l) => l === browserLang) > -1) {
        this.selectedLanguage = browserLang;
      } else {
        this.selectedLanguage = this.translate.defaultLang;
      }
      this.translate.use(this.selectedLanguage);
    });
  }

  public onSelectDatasources() {
    this.dsService.setDatasource(this.selectedDatasources).subscribe((value) => {
      if (this.languages === undefined || this.languages.length === 0) {
        this.initLanguageSettings();
      }
      this.nodeService.setActiveDatasource(this.selectedDatasources);
      this.standardDatasources = [];
      this.selectedPreferenceRow.forEach((pp) => this.standardDatasources.push(pp.label));
      this.canShowApplication = true;
    });
  }

  saveDatasourcePreference() {
    let werte = "";
    this.selectedPreferenceRow.forEach((pp) => (werte += pp.label + "\n"));
    this.preferenceService.savePreference(this.datasourceToPreference(werte.substr(0, werte.length - 1))).subscribe((value) => {
      this.selectedDatasources = [];
      this.selectedPreferenceRow.forEach((pp) => this.selectedDatasources.push(pp.value));
      this.onSelectDatasources();
      this.displayDatasourceDialog = false;
    });
  }

  private datasourceToPreference(datasource: string) {
    const pref = new Preference();
    pref.value = datasource;
    pref.key = this.preferenceName;
    pref.scope = Scope.USER;
    return pref;
  }

  public onSelectLanguage() {
    this.translate.use(this.selectedLanguage);
  }

  public onSelectTimezone() {
    this.timezoneService.setTimezone(this.selectedTimezone);
  }

  public onSelectDateFormatType($event) {
    this.timezoneService.setDateFormatType(this.selectedDateFormatType);
    $event.originalEvent.stopPropagation();
  }

  public onSelectHourFormatType(event: { originalEvent: Event }) {
    this.timezoneService.setHourFormatType(this.selectedHourFormatType);
    event.originalEvent.stopPropagation();
  }

  public onSelectIsoFormat($event) {
    this.timezoneService.setIsoFormat(this.selectedIsoFormat);
    $event.originalEvent.stopPropagation();
  }

  public onShowAboutDialog() {
    this.displayAboutDialog = true;
  }

  private initStandarDatasource(pref: Observable<Preference>): Observable<string[]> {
    return pref.pipe(map((p) => (p === undefined ? null : p.value.split("\n"))));
  }

  private loadPreference(preferenceName: string): Observable<Preference> {
    return this.preferenceService.getPreferenceForScope("USER", preferenceName).pipe(map((p) => p[0]));
  }
}
