/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Inject, Injectable, OnDestroy } from "@angular/core";
import { Components, ContextType } from "@details/model/details.model";
import { ContextService } from "@details/services/context.service";
import { Attribute, ContextGroup, Node } from "@navigator/node";
import { BehaviorSubject, concatMap, finalize, forkJoin, of, ReplaySubject, Subscription, take, tap } from "rxjs";
import { AttributeDTO } from "./model/attribute-dto.type";
import { ContextDescribableProvider } from "./model/context-describable-provider.interface";
import { COLUMN_KEY_SEPARATOR } from "./model/context-explorer.constants";

type ContextGroupType = "measured" | "ordered";
type ComponentDTO = { name: string; attributes: Attribute[] };
type ContextGroupDTO = { [key in ContextType]?: ComponentDTO[] };
type ContextDTO = { [key in ContextGroupType]?: ContextGroupDTO };

@Injectable()
export class ContextBulkEditorModelService implements OnDestroy {
  private currentContexts: Map<Node, Components> = new Map();
  private contextProvider = new ReplaySubject<Map<Node, Components>>(1);
  private isLoading = new BehaviorSubject<boolean>(false);
  private subscription = new Subscription();

  constructor(
    private contextService: ContextService,
    @Inject("ContextDescribableProvider") private contextDescribableProvider: ContextDescribableProvider,
  ) {
    this.reload();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public isModelLoading() {
    return this.isLoading.asObservable();
  }

  public reload() {
    this.isLoading.next(true);
    this.currentContexts.clear();
    this.contextDescribableProvider
      .getContextDescribables()
      .pipe(
        take(1),
        concatMap((contextDescribables) => this.processContexts(contextDescribables)),
        finalize(() => {
          this.contextProvider.next(this.currentContexts);
          this.isLoading.next(false);
        }),
      )
      .subscribe();
  }

  private processContexts(contextDescribables: Node[]) {
    if (contextDescribables?.length > 0) {
      return forkJoin(contextDescribables.map((ctxDescr) => this.contextService.getContext(ctxDescr))).pipe(
        tap((contexts) => this.resetContexts(contexts, contextDescribables)),
      );
    } else {
      return of(undefined);
    }
  }

  private resetContexts(contexts: Components[], contextDescribables: Node[]) {
    contexts.forEach((context, index) => this.currentContexts.set(contextDescribables[index], context));
  }

  public getContexts() {
    return this.contextProvider.asObservable();
  }

  public saveContexts(attributes: AttributeDTO[], contextType: ContextType) {
    this.isLoading.next(true);
    const contextDescribables = Array.from(this.currentContexts.keys());
    const contextDTOsMap = this.toContextDTOsMap(attributes, contextType);
    forkJoin(
      contextDescribables
        .map((contextDescribable) => {
          const changes = contextDTOsMap.get(contextDescribable.type + COLUMN_KEY_SEPARATOR + contextDescribable.id);
          if (changes) {
            return this.contextService.putContext(contextDescribable, changes);
          }
        })
        .filter((changes) => !!changes),
    )
      .pipe(
        tap((contexts) => this.resetContexts(contexts, contextDescribables)),
        finalize(() => {
          this.contextProvider.next(this.currentContexts);
          this.isLoading.next(false);
        }),
      )
      .subscribe();
  }

  /**
   * Builds dtos for update context request.
   *
   * Uses objects instead of nested es6-maps since serializing the latter is not working propperly.
   *
   * @param attributeDTOs
   * @param contextType
   * @returns
   */
  private toContextDTOsMap(attributeDTOs: AttributeDTO[], contextType: ContextType) {
    return attributeDTOs.reduce((changes, attributeDTO) => {
      attributeDTO.contextDescribableKeys.forEach((key) => {
        // Magic number: key starts with key of context describable and ends with separator followed by context group integer (e.g. ...#0)
        const contextDescribableKey = key.slice(0, -2);
        // Magic number: key ends with separator followed by context group integer (e.g. ...#0)
        const contextGroup = +key.slice(-1) === ContextGroup.MEASURED ? "measured" : "ordered";

        const contextDTO = changes.get(contextDescribableKey) ?? {};
        const groupDTO = contextDTO[contextGroup] ?? {};
        const componentDTOs = groupDTO[contextType] ?? [];

        let componentDTO = componentDTOs.find((component) => component.name === attributeDTO.componentName);
        if (componentDTO) {
          componentDTO.attributes.push(attributeDTO.attribute);
        } else {
          componentDTO = { name: attributeDTO.componentName, attributes: [attributeDTO.attribute] };
          componentDTOs.push(componentDTO);
        }

        groupDTO[contextType] = componentDTOs;
        contextDTO[contextGroup] = groupDTO;
        changes.set(contextDescribableKey, contextDTO);
      });
      return changes;
    }, new Map<string, ContextDTO>());
  }
}
