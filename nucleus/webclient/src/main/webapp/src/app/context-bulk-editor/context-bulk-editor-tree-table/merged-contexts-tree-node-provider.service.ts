/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { groupBy } from "@core/utils/grouping-util";
import { getValue } from "@core/utils/node-utils";
import { Components, ContextAttributeIdentifier, ContextType } from "@details/model/details.model";
import { Attribute, ContextGroup, Node, Relation } from "@navigator/node";
import { TreeNode } from "primeng/api";
import { AttributeDTO } from "../model/attribute-dto.type";
import { ContextAttributeData, ContextData } from "../model/context-data.type";
import { COLUMN_KEY_SEPARATOR } from "../model/context-explorer.constants";

type AllocatedAttribute = { attribute: Attribute; columnKey: string };
type ContextComponentsMeta = { name: string; mimeType: string; sourceName: string };

@Injectable()
export class MergedContextsTreeNodeProvider {
  // jst, 17.10.2024: Arbitrary but fix, neccessary since mandatory to reuse AttributEditor. However, the actual value does no matter.
  private static readonly ATTRIBUT_IDENTIFIER_CONTEXT_GROUP = ContextGroup.ORDERED;
  private static readonly PATH_ID_SEPARATOR = ";";

  public getTreeNodes(contexts: Map<Node, Components>, contextType: ContextType, contextGroups: ContextGroup[]) {
    const components = this.flattenContextComponents(contexts, contextType);
    const byParent = groupBy(components, (component) => this.findTemplateRelation(component)?.parentId);
    return this.createTreeNodes(byParent, [], contextGroups, contextType);
  }

  /**
   * Creates a temporary relation holding the information to which context describable the context component belongs to.
   * This is neccessary since components from contexts of different context describables are merged by name later.
   * In the flattening process the implizit information is lost and thus made explicit here.
   *
   * @param contextDescribable the context describable
   * @returns the relation
   */
  private createContextDescribableRelation(contextDescribable: Node): Relation {
    return {
      entityType: contextDescribable.type,
      name: "ContextDescribable",
      contextType: undefined,
      ids: [contextDescribable.id],
      parentId: undefined,
      type: "TEMPORARY",
    };
  }

  /**
   * Returns context components for given context type from all contexts.
   *
   * @param contexts the contexts
   * @param contextType the context type
   * @returns all context components
   */
  private flattenContextComponents(contexts: Map<Node, Components>, contextType: ContextType) {
    return Array.from(contexts.entries())
      .flatMap(([contextDescribable, context]) => {
        context[contextType]?.forEach((component) => component.relations.push(this.createContextDescribableRelation(contextDescribable)));
        return context[contextType];
      })
      .filter((component) => !!component);
  }

  /**
   * Creates context components tree nodes recursively, including context attribute tree nodes as children.
   *
   * Context components and attributes are merged by name within each tree level.
   *
   * @param byParent context components mapped to parent id
   * @param parentIdPath path of parent ids to this level
   * @param contextGroups  respected context groups
   * @returns
   */
  private createTreeNodes(
    byParent: Map<string, Node[]>,
    parentIdPath: string[],
    contextGroups: ContextGroup[],
    contextType: ContextType,
  ): TreeNode<ContextData>[] {
    const components = this.getContextComponents(parentIdPath, byParent);
    if (!components || components.length === 0) {
      return [];
    }
    const byName = groupBy<string, Node>(components, "name");
    return Array.from(byName.entries())?.map(([componentName, comps]) => {
      /**
       * jst, 15.10.2024:
       * Components name, source name and mime type are mandatory for some downstream operations like e.g. translation.
       * Source name and mime type might be ambigious, since context components are merged by name only.
       *
       * Solutions:
       * 1) Source name
       * For now the source name is assumed to be unambigious, since  this feature only supports context describables form same data source.
       * Thus, magic number comps?.[0].sourceName is justified.
       * 2) Mime type
       * a) @TODO jst, 24.10.2024 dbl check other solution like multiple translations/names? Otherwise using mime type of first component is flawed but still the best guess.
       * b) Mime type does not depend on the context group, but might be undefined for one of them.
       */
      const componentsMeta: ContextComponentsMeta = {
        mimeType: getValue<string[]>(comps?.[0], "MimeType")?.find((m) => !!m),
        sourceName: comps?.[0]?.sourceName,
        name: componentName,
      };
      const nextParentIdPath = parentIdPath.concat(this.extractTemplateIdsDistinct(comps));
      const treeNode: TreeNode<ContextData> = {
        label: componentName,
        key: this.toNodeKey(nextParentIdPath),
        expanded: true,
        styleClass: "context-component-tree-table-row",
        type: "component",
        data: {
          dataKey: this.toNodeKey(nextParentIdPath),
          ...componentsMeta,
        },
        children: [
          // create attribute tree nodes
          ...this.toContextAttributeTreeNodes(componentsMeta, comps, nextParentIdPath, contextGroups, contextType),
          // recursively call this method for next level
          ...this.createTreeNodes(byParent, nextParentIdPath, contextGroups, contextType),
        ],
      };
      treeNode.leaf = !!treeNode.children;
      return treeNode;
    });
  }

  /**
   * Gets all context components for next level.
   * @param parentIdPath path of parent ids to this level
   * @param byParent context components mapped to parent id
   * @returns
   */
  private getContextComponents(parentIdPath: string[], byParent: Map<string, Node[]>) {
    const length = parentIdPath.length;
    if (length === 0) {
      // get root context components
      return byParent.get(null);
    } else {
      /**
       * get flat list of all context components with parent id in current path segment (may contain multiple ids, since context components
       * merged by name might point to diferent template ids)
       */
      return parentIdPath[length - 1]
        .split(MergedContextsTreeNodeProvider.PATH_ID_SEPARATOR)
        .filter((currentParentId) => byParent.has(currentParentId))
        .flatMap((currentParentId) => byParent.get(currentParentId));
    }
  }

  /**
   * Returns distinct set of template ids for given context components.
   *
   * Magic number: the relation ids should hold exactly one value.
   * @param components
   * @returns the template ids
   */
  private extractTemplateIdsDistinct(components: Node[]) {
    return [...new Set(components.map((component) => this.findTemplateRelation(component)?.ids?.[0]))].join(
      MergedContextsTreeNodeProvider.PATH_ID_SEPARATOR,
    );
  }

  /**
   * Return template relation for context component.
   * @param contextComponent the context component
   * @returns the template component relation
   */
  private findTemplateRelation(contextComponent: Node) {
    return contextComponent.relations.find((r) => r.entityType === "TemplateComponent");
  }

  /**
   * Generates column key stub for the context component (from the temporary context describable relation).
   *
   * @param contextComponent
   * @returns
   */
  private findColumnKeyStub(contextComponent: Node) {
    const relation = contextComponent.relations.find((r) => r.name === "ContextDescribable");
    // jst, 15.10.2024: magic number, the relation ids should hold exactly one value.
    return this.toColumnKeyStub(relation.entityType, relation.ids[0]);
  }

  /**
   * Generates column key stub from context describables type and id.
   * The stub is lagging the context group information.
   *
   * @param type context describable type
   * @param id context describable id
   * @returns column key stub
   */
  public toColumnKeyStub(type: string, id: string): string {
    return type + COLUMN_KEY_SEPARATOR + id + COLUMN_KEY_SEPARATOR;
  }

  /**
   * Generates attribute tree nodes. Merges attributes of all given components by name.
   *
   * componentsMeta is assumed to be true for all components. Also see comment in @see createTreeNodes.
   *
   * @param componentsMeta description of context components
   * @param components context components
   * @param parentIdPath path of parent ids to this level
   * @param contextGroups context groups
   * @returns
   */
  private toContextAttributeTreeNodes(
    componentsMeta: ContextComponentsMeta,
    components: Node[],
    parentIdPath: string[],
    contextGroups: ContextGroup[],
    contextType: ContextType,
  ): TreeNode[] {
    const attrs = this.allocateAttributeToContextDescribable(components);
    return Array.from(groupBy(attrs, (attr) => attr.attribute.name).entries()).map(([attributeName, entry]) =>
      this.toContextAttributeTreeNode(attributeName, componentsMeta, entry, parentIdPath, contextGroups, contextType),
    );
  }

  /**
   * Returns all attributes from all components as flat list. However, each attribute is allocated to its origin context describable.
   * @param components the context components
   * @returns the allocated attributes
   */
  private allocateAttributeToContextDescribable(components: Node[]): AllocatedAttribute[] {
    return components.flatMap((component) => {
      const columnKey = this.findColumnKeyStub(component);
      return (
        component.attributes
          // jst, 15.10.2024: Name and mime type are not supposed to be displayed as rows
          .filter((attribute) => attribute.name !== "Name" && attribute.name !== "MimeType")
          .map((attribute) => ({ columnKey, attribute }))
      );
    });
  }

  /**
   * Generates attribute tree node.
   * @param attributeName attribute name
   * @param componentsMeta description of context component
   * @param allocatedAttributes attributes allocated to context describable
   * @param parentIdPath path of parent ids to this level
   * @param contextGroups context group
   * @returns attribute tree node
   */
  private toContextAttributeTreeNode(
    attributeName: string,
    componentsMeta: ContextComponentsMeta,
    allocatedAttributes: AllocatedAttribute[],
    parentIdPath: string[],
    contextGroups: ContextGroup[],
    contextType: ContextType,
  ): TreeNode<ContextAttributeData> {
    const key = this.toNodeKey(parentIdPath, attributeName);
    return {
      label: attributeName,
      key,
      expanded: false,
      leaf: true,
      styleClass: "context-attribute-tree-table-row",
      type: "attribute",
      data: {
        dataKey: key, // PrimeNg dataKey field in treetable cannot refer to (mandatory) unique node key.
        name: attributeName,
        globalAttribute: this.createGlobalAttribute(allocatedAttributes, componentsMeta, contextType),
        ...Object.fromEntries(
          contextGroups.flatMap((contextGroup) =>
            allocatedAttributes
              .filter((aa) => aa.attribute.value[contextGroup] !== undefined)
              .flatMap((aa) => [
                // "value" entry as plain string for filter and other table features work propperly
                [aa.columnKey + contextGroup, aa.attribute.value[contextGroup] + (aa.attribute.unit ? " " + aa.attribute.unit : "")],
                // "attribute" entry as complex object to allow reusing the attribute viewer
                [
                  aa.columnKey + contextGroup + "#attributeIdentifier",
                  this.toAttributeIdentifier(aa, componentsMeta, contextType, contextGroup),
                ],
              ]),
          ),
        ),
        mimeType: componentsMeta.mimeType,
        sourceName: componentsMeta.sourceName,
        deleteValue: false,
      },
    };
  }

  /**
   * Creates model entity for attribute editor
   *
   * @param attributeName attribute name
   * @param componentsMeta description of context component
   * @returns the {@link ContextAttributeIdentifier}
   */
  private createGlobalAttribute(
    allocatedAttributes: AllocatedAttribute[],
    componentsMeta: ContextComponentsMeta,
    contextType: ContextType,
  ): ContextAttributeIdentifier {
    const identifier = this.toAttributeIdentifier(
      allocatedAttributes?.[0],
      componentsMeta,
      contextType,
      MergedContextsTreeNodeProvider.ATTRIBUT_IDENTIFIER_CONTEXT_GROUP,
    );
    // Adjust unit and value for global attribute (beware: value needs new array reference, otherwise messes up first column.)
    identifier.attribute.unit = allocatedAttributes.find((attribute) => attribute.attribute?.unit !== "")?.attribute.unit ?? "";
    const val = [];
    val[MergedContextsTreeNodeProvider.ATTRIBUT_IDENTIFIER_CONTEXT_GROUP] = "";
    identifier.attribute.value = val;

    return identifier;
  }

  private toAttributeIdentifier(
    allocatedAttribute: AllocatedAttribute,
    componentsMeta: ContextComponentsMeta,
    contextType: ContextType,
    contextGroup: ContextGroup,
  ): ContextAttributeIdentifier {
    const attribute = { ...allocatedAttribute?.attribute };
    const contextDescribable = {
      name: allocatedAttribute?.columnKey.split(COLUMN_KEY_SEPARATOR)?.[0],
    } as Node;
    const contextComponent = {
      name: componentsMeta.name,
      sourceName: componentsMeta.sourceName,
    } as Node;
    const identifier = new ContextAttributeIdentifier(contextDescribable, contextComponent, attribute, contextGroup, contextType);
    identifier.sourceName = componentsMeta.sourceName;
    return identifier;
  }

  /**
   * Generates unique key for each tree node (row, resp.) by chaining input params.
   *
   * @param parentIdPath path of parent ids to this level
   * @param names component and (if so) attribute names
   * @returns
   */
  private toNodeKey(parentIdPath: string[], ...names: string[]) {
    return [...parentIdPath, ...names].join(".");
  }

  /**
   * Creates dto from attribute tree node holding mandatory information for context update request
   * @param treeNode the tree node
   * @returns the dto
   */
  public toAttributeDTO(treeNode: TreeNode<ContextAttributeData>): AttributeDTO {
    const attribute: Attribute = {
      name: treeNode.data.globalAttribute.attribute.name,
      dataType: treeNode.data.globalAttribute.attribute.dataType,
      unit: treeNode.data.globalAttribute.attribute.unit,
      value: treeNode.data.deleteValue
        ? ""
        : treeNode.data.globalAttribute.attribute.value[MergedContextsTreeNodeProvider.ATTRIBUT_IDENTIFIER_CONTEXT_GROUP],
    };
    const componentName = treeNode.data.globalAttribute.contextComponent.name;
    const contextDescribableKeys = Object.keys(treeNode.data).filter((key) => key.includes(COLUMN_KEY_SEPARATOR));
    return { attribute, componentName, contextDescribableKeys };
  }
}
