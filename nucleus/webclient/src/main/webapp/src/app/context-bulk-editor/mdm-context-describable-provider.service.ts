/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { BasketService } from "@basket/basket.service";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { forkJoin, Observable } from "rxjs";
import { ContextDescribableProvider } from "./model/context-describable-provider.interface";

@Injectable()
export class MdmContextDescribableProviderService implements ContextDescribableProvider {
  private static readonly TYPES = ["TestStep", "Measurement"];

  constructor(private basketService: BasketService, private nodeService: NodeService) {}

  /**
   * Reads context describable entites distinctly with given mime type based on shoping basket items with given types.
   * @returns the context describables
   */
  public getContextDescribables(): Observable<Node[]> {
    return forkJoin(this.getItems().map((item) => this.nodeService.getNodesFromItem(item)));
  }

  /**
   * @returns basket items of relevant type
   */
  private getItems() {
    return this.basketService.getItems().filter((item) => MdmContextDescribableProviderService.TYPES.includes(item.type));
  }
}
