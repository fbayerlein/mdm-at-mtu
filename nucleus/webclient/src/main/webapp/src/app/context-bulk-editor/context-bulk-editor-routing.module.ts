/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ContextBulkEditorMainComponent } from "./context-bulk-editor-main/context-bulk-editor-main.component";
import { ContextBulkEditorTreeTableComponent } from "./context-bulk-editor-tree-table/context-bulk-editor-tree-table.component";

const contextExplorerRoutes: Routes = [
  {
    path: "",
    component: ContextBulkEditorMainComponent,
    children: [
      {
        path: ":contextType",
        component: ContextBulkEditorTreeTableComponent,
      },
      { path: "", redirectTo: "uut", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(contextExplorerRoutes)],
  exports: [RouterModule],
})
export class ContextBulkEditorRoutingModule {}
