/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Preference, PreferenceService } from "@core/preference.service";
import { Components, ContextType } from "@details/model/details.model";
import { getContextType, isContextTypeAbbr } from "@details/model/types/context-type";
import { ContextGroup, Node } from "@navigator/node";
import { TreeNode } from "primeng/api";
import { combineLatest, Subscription } from "rxjs";
import { ContextBulkEditorModelService } from "../context-bulk-editor-model.service";
import { ContextAttributeData, ContextData } from "../model/context-data.type";
import { MergedContextsTreeNodeProvider } from "./merged-contexts-tree-node-provider.service";

type ColumnConfig = { header: string; groupHeader?: string; field: string; placeholder?: string };

@Component({
  selector: "mdm-context-bulk-editor-tree-table",
  templateUrl: "./context-bulk-editor-tree-table.component.html",
  styleUrls: ["./context-bulk-editor-tree-table.component.css"],
  providers: [MergedContextsTreeNodeProvider],
})
export class ContextBulkEditorTreeTableComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();
  public treeNodes: TreeNode<ContextData>[];
  public selectedNodes: TreeNode<ContextData>[];
  public showGroupHeader: boolean;
  public columns: ColumnConfig[] = [];
  public frozenColumns: ColumnConfig[] = [];
  public isLoading = false;
  public rowTrackBy = (index: unknown, item: { node: TreeNode<ContextData> }) => {
    return item.node.key;
  };
  #isInEditMode = false;

  private contextType: ContextType;

  constructor(
    private activatedRoute: ActivatedRoute,
    private model: ContextBulkEditorModelService,
    private treeNodeProvider: MergedContextsTreeNodeProvider,
    private preferenceService: PreferenceService,
  ) {}

  ngOnInit(): void {
    this.subscription.add(this.model.isModelLoading().subscribe((isLoading) => (this.isLoading = isLoading)));
    this.subscription.add(
      combineLatest([
        this.activatedRoute.paramMap,
        this.model.getContexts(),
        this.preferenceService.getPreference("context-bulk-editor.config"),
      ]).subscribe(([paramMap, contexts, configPref]) => {
        const contextGroups = this.extractContextGroup(configPref);
        this.contextType = this.extractContextType(paramMap);
        this.initTreeTable(contexts, contextGroups);
      }),
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Setter for edit mode.
   * Ensures frozen columns are set accordingly.
   */
  private set isInEditMode(isInEditMode: boolean) {
    this.#isInEditMode = isInEditMode;
    this.initFrozenColumns();
  }

  /**
   * Getter for edit mode
   */
  public get isInEditMode() {
    return this.#isInEditMode;
  }

  public onSynchronizeContextDescribables() {
    this.model.reload();
  }

  /**
   * Button-Listener for entering editing mode.
   * @param event
   */
  public onEdit() {
    this.isInEditMode = true;
  }

  /**
   * Button-Listener for canceling editing mode.
   * @param event
   */
  public onCancelEdit() {
    this.isInEditMode = false;
    this.resetGlobalAttributesValues(this.treeNodes);
  }

  /**
   * Button-Listener for ending editing mode with saving changes.
   * @param event
   */
  public onSaveChanges() {
    const changedAttributes = this.extractChangedAttributes(this.treeNodes).map((attributeNode) =>
      this.treeNodeProvider.toAttributeDTO(attributeNode),
    );
    this.model.saveContexts(changedAttributes, this.contextType);
    this.isInEditMode = false;
  }

  /**
   * Extracts all such nodes from tree representing changed attributes.
   *
   * @param treeNodes
   * @returns
   */
  private extractChangedAttributes(treeNodes: TreeNode<ContextData>[]) {
    return treeNodes.reduce((p, c) => {
      if (this.isAttributeTreeNode(c) && (c.data.globalAttribute.attribute.value != "" || c.data.deleteValue)) {
        p.push(c);
      } else if (c.children) {
        p = p.concat(this.extractChangedAttributes(c.children));
      }
      return p;
    }, [] as TreeNode<ContextAttributeData>[]);
  }

  /**
   * Recursively resets values in "global value" column for all tree nodes.
   * @param treeNodes the tree(section)
   */
  private resetGlobalAttributesValues(treeNodes: TreeNode<ContextData>[]) {
    treeNodes.forEach((treeNode) => {
      if (this.isAttributeTreeNode(treeNode)) {
        treeNode.data.globalAttribute.attribute.value = [""];
        treeNode.data.deleteValue = false;
      } else if (treeNode.children) {
        this.resetGlobalAttributesValues(treeNode.children);
      }
    });
  }

  /**
   * Typeguard for tree nodes representing attributes
   * @param treeNode the tree node to check
   * @returns
   */
  private isAttributeTreeNode(treeNode: TreeNode<ContextData>): treeNode is TreeNode<ContextAttributeData> {
    return treeNode.type === "attribute";
  }

  /**
   * Initializes the tree table.
   * @param paramMap
   * @param contexts
   */
  private initTreeTable(contexts: Map<Node, Components>, contextGroups: ContextGroup[]) {
    console.log(contexts);
    if (contexts?.size > 0) {
      this.initFrozenColumns();
      this.initColumns(contextGroups, contexts);
    } else {
      this.frozenColumns = [];
      this.columns = [
        {
          header: "context-bulk-editor.context-bulk-editor-tree-table.name",
          field: "name",
        },
      ];
    }
    this.treeNodes = this.treeNodeProvider.getTreeNodes(contexts, this.contextType, contextGroups);
  }

  /**
   * Initializes the decription for the (regular) dynamic columns of the tree table.
   * @param contextGroups respected context groups
   * @param contexts contexts mapped to the related context describable
   */
  private initColumns(contextGroups: ContextGroup[], contexts: Map<Node, Components>) {
    this.showGroupHeader = contextGroups.length > 1;
    const columns: ColumnConfig[] = Array.from(contexts.keys()).flatMap((contextDescribable) =>
      contextGroups.map((contextGroup) => ({
        groupHeader: contextDescribable.name,
        header: this.showGroupHeader
          ? "context-bulk-editor.context-bulk-editor-tree-table." + ContextGroup[contextGroup].toLowerCase()
          : contextDescribable.name,
        field: this.toColumnKey(contextDescribable, contextGroup),
        placeholder: "context-bulk-editor.context-bulk-editor-tree-table.value",
      })),
    );
    this.columns = columns;
  }

  /**
   * Generates column key from the context describable for given context group.
   *
   * @param contextDescribable
   * @param contextGroup
   * @returns
   */
  private toColumnKey(contextDescribable: Node, contextGroup: ContextGroup): string {
    return this.treeNodeProvider.toColumnKeyStub(contextDescribable.type, contextDescribable.id) + contextGroup;
  }

  /**
   * Initializes the decription for the frozen dynamic columns of the tree table.
   *
   * jst, 15.10.2024: PrimeNg-TreeTable needs this "dynamic" column configuration for displaying headers properly.
   * Therefore this somewhat cumbersome solution is inevitable.
   */
  private initFrozenColumns() {
    const columns: ColumnConfig[] = [
      {
        header: "context-bulk-editor.context-bulk-editor-tree-table.name",
        field: "name",
      },
    ];
    if (this.isInEditMode) {
      columns.push({
        header: "context-bulk-editor.context-bulk-editor-tree-table.global-value",
        field: "globalAttribute",
      });
    }
    this.frozenColumns = columns;
  }

  /**
   * Evaluates context type from URL-Parameter
   * @param paramMap routing parameters
   * @returns the context type
   */
  private extractContextType(paramMap: ParamMap): ContextType {
    const contextType = paramMap.get("contextType")?.toUpperCase();
    return isContextTypeAbbr(contextType) ? getContextType(contextType) : "UNITUNDERTEST";
  }

  /**
   * Evaluates context group from configuration via preference
   * @param configPref
   * @returns
   */
  private extractContextGroup(configPref: Preference[]) {
    let contextGroups: ContextGroup[];
    const configString = configPref?.[0]?.value;
    if (configString) {
      let pref: string | number | (string | number)[] = JSON.parse(configString)?.contextGroups;
      if (!Array.isArray(pref)) {
        pref = [pref];
      }
      contextGroups = pref.map((entry) => {
        if (entry === 0 || entry === 1) {
          return ContextGroup[ContextGroup[entry]];
        } else if (typeof entry === "string") {
          return ContextGroup[entry.toUpperCase()];
        }
      });
    } else {
      contextGroups = [ContextGroup.ORDERED, ContextGroup.MEASURED];
    }
    return contextGroups;
  }
}
