/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MDMCoreModule } from "@core/mdm-core.module";
import { MDMDetailModule } from "@details/mdm-detail.module";
import { TranslateModule } from "@ngx-translate/core";
import { InputTextModule } from "primeng/inputtext";
import { TabMenuModule } from "primeng/tabmenu";
import { ToggleButtonModule } from "primeng/togglebutton";
import { TooltipModule } from "primeng/tooltip";
import { TreeTableModule } from "primeng/treetable";
import { ContextBulkEditorMainComponent } from "./context-bulk-editor-main/context-bulk-editor-main.component";
import { ContextBulkEditorRoutingModule } from "./context-bulk-editor-routing.module";
import { ContextBulkEditorTreeTableComponent } from "./context-bulk-editor-tree-table/context-bulk-editor-tree-table.component";

@NgModule({
  declarations: [ContextBulkEditorMainComponent, ContextBulkEditorTreeTableComponent],
  imports: [
    // Angular
    CommonModule,
    // Mdm
    ContextBulkEditorRoutingModule,
    MDMCoreModule,
    MDMDetailModule,
    // PrimeNg
    InputTextModule,
    TabMenuModule,
    TreeTableModule,
    ToggleButtonModule,
    TooltipModule,
    // Other 3rd Parties
    TranslateModule.forChild(),
  ],
})
export class ContextBulkEditorModule {}
