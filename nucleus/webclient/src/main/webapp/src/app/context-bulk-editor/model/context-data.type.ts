/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { ContextAttributeIdentifier } from "@details/model/details.model";

export type ContextComponentData = {
  dataKey: string; // PrimeNg dataKey field in treetable cannot refer to (mandatory) unique node key.
  name: string;
  mimeType: string;
  sourceName: string;
};
export type ContextAttributeData = ContextComponentData & { globalAttribute: ContextAttributeIdentifier; deleteValue: boolean };
export type ContextData = ContextComponentData | ContextAttributeData;
