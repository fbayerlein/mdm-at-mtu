/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { startWith, Subscription } from "rxjs";
import { ContextBulkEditorModelService } from "../context-bulk-editor-model.service";

@Component({
  selector: "mdm-context-bulk-editor-main",
  templateUrl: "./context-bulk-editor-main.component.html",
  styleUrls: ["./context-bulk-editor-main.component.css"],
  providers: [ContextBulkEditorModelService],
})
export class ContextBulkEditorMainComponent implements OnInit, OnDestroy {
  // @Input()
  // public contextDescribables: Node[];
  public menuItems: MenuItem[] = [];
  private globalSubscription = new Subscription();

  constructor(private modelService: ContextBulkEditorModelService, private translate: TranslateService) {}

  ngOnInit(): void {
    this.globalSubscription.add(this.translate.onLangChange.pipe(startWith("")).subscribe(() => this.initMenu()));
  }

  ngOnDestroy(): void {
    this.globalSubscription.unsubscribe();
  }

  // ngOnChanges(changes: SimpleChanges): void {
  //   if (Object.prototype.hasOwnProperty.call(changes, "contextDescribables")) {
  //     this.initModel();
  //   }
  // }

  private initMenu() {
    this.menuItems = [
      { label: this.translate.instant("details.mdm-detail.unit-under-test"), routerLink: "uut" },
      { label: this.translate.instant("details.mdm-detail.test-sequence"), routerLink: "ts" },
      { label: this.translate.instant("details.mdm-detail.test-equipment"), routerLink: "te" },
    ];
  }

  // private initModel() {
  //   if (this.contextDescribables) {
  //     this.modelService.reloadContexts(this.contextDescribables);
  //   }
  // }
}
