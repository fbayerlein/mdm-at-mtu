/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule, HttpTestingController, RequestMatch } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { MDMItem } from "@core/mdm-item";
import { PropertyService } from "@core/property.service";
import { of as observableOf } from "rxjs";
import { Query, QueryService, SearchResult } from "./query.service";

describe("QueryService", () => {
  let httpTestingController: HttpTestingController;

  const reqPostMatch: RequestMatch = { method: "POST" };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PropertyService, QueryService],
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe("query()", () => {
    it("should return result for simple query", async(
      inject([QueryService], (queryService) => {
        const mockResponse = {
          rows: [{ source: "MDMNVH", type: "Test", id: "id1", columns: [{ type: "Test", attribute: "Name", value: "TestNumberOne" }] }],
        };

        queryService.query(new Query()).subscribe((results) => {
          expect(results.rows.length).toBe(1);
          expect(results.rows[0].columns[0].value).toEqual("TestNumberOne");
        });

        const req = httpTestingController.expectOne(reqPostMatch);
        req.flush(mockResponse);
      }),
    ));
  });

  describe("queryType()", () => {
    it("should quote IDs", async(
      inject([QueryService], (queryService) => {
        const spy = spyOn(queryService, "query").and.returnValue(observableOf(new SearchResult()));

        // case insensitive query by default
        const query = new Query();
        query.resultType = "TestStep";
        query.addFilter("MDM", 'Test.Id ci_eq "id1"');
        query.columns = ["TestStep.Name", "TestStep.Id"];

        queryService.queryType("TestStep", [{ source: "MDM", type: "Test", id: "id1" }], ["TestStep.Name"]).subscribe((results) => {
          expect(queryService.query).toHaveBeenCalledWith(query);
        });
      }),
    ));
  });

  describe("queryItems()", () => {
    it("should return result for simple query", async(
      inject([QueryService], (queryService) => {
        const mockResponse = {
          rows: [{ source: "MDMNVH", type: "Test", id: "id1", columns: [{ type: "Test", attribute: "Name", value: "TestNumberOne" }] }],
        };
        const item = new MDMItem("MDMNVH", "Test", "id1");

        const result = queryService.queryItems([item], ["Test.Name"]);
        expect(result.length).toBe(1);
        result[0].subscribe((results) => {
          expect(results.rows.length).toBe(1);
          expect(results.rows[0].type).toEqual("Test");
          expect(results.rows[0].columns[0].value).toEqual("TestNumberOne");
        });

        const req = httpTestingController.expectOne(reqPostMatch);
        req.flush(mockResponse);
      }),
    ));
  });
});
