/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { MdmLocalizationService } from "@localization/mdm-localization.service";
import { MdmTranslatePipe } from "@localization/mdm-translate.pipe";
import { TranslateService } from "@ngx-translate/core";
import { TreeNode } from "primeng/api";
import { AutoComplete } from "primeng/autocomplete";
import { Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { Node } from "../navigator/node";
import { NodeService } from "../navigator/node.service";
import { SearchAttribute, SearchService } from "../search/search.service";
import { SearchattributeTreeComponent } from "../searchattribute-tree/searchattribute-tree.component";
import { View, ViewColumn } from "./tableview.service";

@Component({
  selector: "mdm-edit-view",
  templateUrl: "./editview.component.html",
  styles: [".remove {color:black; cursor: pointer; float: right}", ".icon { cursor: pointer; margin: 0px 5px; }"],
})
export class EditViewComponent implements OnInit, AfterViewInit, OnDestroy {
  public dialogVisible = false;

  @ViewChild(SearchattributeTreeComponent) tree: SearchattributeTreeComponent;

  environments: Node[] = [];
  isReadOnly = false;
  currentView: View = new View();
  searchAttributes: { [env: string]: SearchAttribute[] } = {};

  allSuggestions: { label: string; group: string; attribute: SearchAttribute }[] = [];
  filteredSuggestions: { label: string; group: string; attribute: SearchAttribute }[] = [];
  selectedSuggestion: SearchAttribute;

  nodeServiceSubscription: Subscription;

  @ViewChild("autocomplete") autoComplete: AutoComplete;

  @Output()
  coloumnsSubmitted = new EventEmitter<View>();
  private allUniqueBoTypeTranslations: any[] = [];
  private allAttribueTranslations: any[] = [];
  private subscription = new Subscription();

  constructor(
    private nodeService: NodeService,
    private searchService: SearchService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private localizationService: MdmLocalizationService,
  ) {}

  ngOnInit() {
    this.initNodes();
    this.subscribeToDatasourceChanges();
  }

  subscribeToDatasourceChanges() {
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe(() => this.initNodes());
  }

  initNodes() {
    this.subscription.add(
      this.nodeService.getNodes().subscribe(
        (envs) => {
          this.searchService
            .loadSearchAttributesStructured(envs.map((e) => e.sourceName))
            .pipe(map((attrs) => attrs["measurements"]))
            .subscribe(
              (attrs) => this.refreshValues(attrs, envs), //refreshTypeAheadValues
              (error) =>
                this.notificationService.notifyError(
                  this.translateService.instant("tableview.editview.err-cannot-load-search-attributes"),
                  error,
                ),
            );
        },
        (error) => this.notificationService.notifyError(this.translateService.instant("tableview.editview.err-cannot-load-nodes"), error),
      ),
    );
    this.subscription.add(this.translateService.onLangChange.subscribe(() => this.onLanguageChanged()));
  }

  ngAfterViewInit() {
    this.subscription.add(
      this.tree.onNodeSelect$.subscribe(
        (node) => this.selectNode(node),
        (error) => this.notificationService.notifyError(this.translateService.instant("tableview.editview.err-cannot-select-node"), error),
      ),
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  refreshValues(searchAttributes: { [env: string]: SearchAttribute[] }, environments: Node[]) {
    this.searchAttributes = searchAttributes;
    this.environments = environments;

    this.onLanguageChanged();
  }

  private onLanguageChanged() {
    this.allUniqueBoTypeTranslations = [];
  }

  asyncTranslateBoTypesAndAttributes() {
    if (this.allUniqueBoTypeTranslations.length === 0) {
      const allAttributes: SearchAttribute[] = Object.keys(this.searchAttributes)
        .map((env) => this.searchAttributes[env])
        .reduce((acc, value) => acc.concat(value), <SearchAttribute[]>[]);

      this.allAttribueTranslations = [];

      const allBoTypes: any[] = allAttributes.map((sa) => {
        return { key: sa.source + "." + sa.boType, boType: sa.boType, label: sa.boType };
      });
      allBoTypes.sort((bo1, bo2) => bo1["key"].localeCompare(bo2["key"]));
      const allUniqueBoTypes: any[] = [];
      allBoTypes.forEach((bo1) => {
        if (allUniqueBoTypes.findIndex((bo2) => bo1["key"].localeCompare(bo2["key"]) === 0) === -1) {
          allUniqueBoTypes.push(bo1);
        }
      });

      const translatePipe: MdmTranslatePipe = new MdmTranslatePipe(this.localizationService);

      allUniqueBoTypes.forEach((bt, i) => {
        translatePipe
          .transform(bt["boType"], bt["source"])
          .subscribe((result) => {
            // for some keys we have 3 or more different translations
            // and thus 3 or more iterations inside the subscription
            // e.g. 'Channel' = 'Channel'; 'Channel' = 'Measurement quantity'
            // take the last one like in attribute tree using Translate pipe
            // to get the same displayed results
            bt["label"] = result;
            if (this.allUniqueBoTypeTranslations.length < allUniqueBoTypes.length) {
              delete bt["boType"];
              this.allUniqueBoTypeTranslations.push(bt);
            }
            if (this.allUniqueBoTypeTranslations.length === allUniqueBoTypes.length && allUniqueBoTypes.length - 1 === i) {
              this.onTranslationDone();
            }
          })
          .unsubscribe();
      });

      allAttributes.forEach((attr, i) => {
        translatePipe
          .transform(attr.boType, attr.source, attr.attrName)
          .subscribe((result) => {
            if (this.allAttribueTranslations.length <= i) {
              const item = {
                key: attr.source + "." + attr.boType + "." + attr.attrName,
                label: result,
              };
              this.allAttribueTranslations.push(item);
              if (allAttributes.length - 1 === i) {
                this.onTranslationDone();
              }
            }
          })
          .unsubscribe();
      });
    }
  }

  /**
   * this is called, when all subscriptions finished updating the
   * asynchronous translation result for bo types or attributes
   */
  private onTranslationDone() {
    if (this.allUniqueBoTypeTranslations.length > 0 && this.allAttribueTranslations.length > 0) {
      const allAttributes: SearchAttribute[] = Object.keys(this.searchAttributes)
        .map((env) => this.searchAttributes[env])
        .reduce((acc, value) => acc.concat(value), <SearchAttribute[]>[]);

      // space will not work with type-ahead values
      const ar = allAttributes.map((sa) => {
        return { label: this.findBoTypeTranslation(sa) + "." + this.findAttributeTranslation(sa), group: sa.boType, attribute: sa };
      });

      this.allSuggestions = this.uniqueBy(ar, (p) => p.label);
    }
  }

  private findBoTypeTranslation(attr: SearchAttribute): string {
    const key = attr.source + "." + attr.boType;
    const retVal = this.allUniqueBoTypeTranslations.find((bt) => bt["key"].localeCompare(key) === 0);
    return retVal === undefined ? attr.boType : retVal["label"];
  }

  private findAttributeTranslation(attr: SearchAttribute): string {
    const key = attr.source + "." + attr.boType + "." + attr.attrName;
    const retVal = this.allAttribueTranslations.find((a) => a["key"].localeCompare(key) === 0);
    return retVal === undefined ? attr.attrName : retVal["label"];
  }

  showDialog(currentView: View) {
    this.asyncTranslateBoTypesAndAttributes();

    this.currentView = currentView; //classToClass
    this.isNameReadOnly();

    this.selectedSuggestion = undefined;
    this.filteredSuggestions = [];
    this.autoComplete.hide();
    this.tree.expandRootNodeOnly();

    this.dialogVisible = true;
    return this.coloumnsSubmitted;
  }

  closeDialog() {
    this.dialogVisible = false;
  }

  remove(col: ViewColumn) {
    this.currentView.columns = this.currentView.columns.filter((c) => c !== col);
  }

  isLast(col: ViewColumn) {
    return this.currentView.columns.indexOf(col) === this.currentView.columns.length - 1;
  }

  isFirst(col: ViewColumn) {
    return this.currentView.columns.indexOf(col) === 0;
  }

  moveUp(col: ViewColumn) {
    if (!this.isFirst(col)) {
      const oldIndex = this.currentView.columns.indexOf(col);
      const otherCol = this.currentView.columns[oldIndex - 1];
      this.currentView.columns[oldIndex] = otherCol;
      this.currentView.columns[oldIndex - 1] = col;
    }
  }

  moveDown(col: ViewColumn) {
    if (!this.isLast(col)) {
      const oldIndex = this.currentView.columns.indexOf(col);
      const otherCol = this.currentView.columns[oldIndex + 1];
      this.currentView.columns[oldIndex] = otherCol;
      this.currentView.columns[oldIndex + 1] = col;
    }
  }

  selectNode(node: TreeNode) {
    if (node.type !== "attribute") {
      return;
    }
    this.pushViewCol(new ViewColumn(node.parent.label, node.label));
  }

  pushViewCol(viewCol: ViewColumn) {
    if (viewCol && this.currentView.columns.findIndex((c) => ViewColumn.equals(viewCol, c)) === -1) {
      this.currentView.columns.push(viewCol);
    } else {
      this.notificationService.notifyInfo("Info", this.translateService.instant("tableview.editview.attribute-already-selected"));
    }
  }

  isAsc(col: ViewColumn) {
    return col.sortOrder === 1;
  }
  isDesc(col: ViewColumn) {
    return col.sortOrder === -1;
  }
  isNone(col: ViewColumn) {
    return col.sortOrder === null;
  }

  toggleSort(col: ViewColumn) {
    if (col.sortOrder === null) {
      View.setSortOrder(this.currentView, col.type, col.name, 1);
    } else if (col.sortOrder === 1) {
      View.setSortOrder(this.currentView, col.type, col.name, -1);
    } else if (col.sortOrder === -1) {
      View.setSortOrder(this.currentView, col.type, col.name, null);
    }
  }

  private uniqueBy<T>(a: T[], key: (T) => any) {
    const seen = {};
    return a.filter(function (item) {
      const k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
  }

  public onSelect(event) {
    this.pushViewCol(new ViewColumn(event.attribute.boType, event.attribute.attrName));
    this.selectedSuggestion = undefined;
    this.filteredSuggestions = [];
    this.autoComplete.hide();
  }

  search(event) {
    this.filteredSuggestions = this.allSuggestions.filter(
      (v) =>
        v.label.includes(event.query) &&
        !this.currentView.columns.find((col) => col.name === v.attribute.attrName && col.type === v.attribute.boType),
    );
  }

  private isNameReadOnly() {
    return (this.isReadOnly = this.currentView.name === "" ? false : true);
  }

  applyChanges() {
    if (this.currentView.columns.length > 0) {
      this.coloumnsSubmitted.emit(this.currentView);
      this.closeDialog();
    } else {
      this.notificationService.notifyError(
        this.translateService.instant("tableview.editview.err-cannot-save-empty-view-title"),
        this.translateService.instant("tableview.editview.err-cannot-save-empty-view-content"),
      );
    }
  }
}
