/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
import { Pipe, PipeTransform } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { DateTime } from "luxon";

import { ContextGroup, MDMLinkExt } from "../navigator/node";
import { TimezoneService } from "../timezone/timezone-service";
import { ViewValue } from "./tableview.component";

@Pipe({ name: "viewValue" })
export class ViewValuePipe implements PipeTransform {
  constructor(private translateService: TranslateService, private timezoneService: TimezoneService) {}

  transform(attr: ViewValue, contextGroup?: ContextGroup, maxArrayItems = 2) {
    return this.ellipsizeArray(this.format(attr, contextGroup), maxArrayItems);
  }

  format(attr: ViewValue, contextGroup?: ContextGroup) {
    if (attr === undefined) {
      return "";
    }
    let value = attr["measured"];
    if (contextGroup === ContextGroup.ORDERED) {
      value = attr["ordered"];
    }

    if (value === undefined) {
      return "";
    }

    switch (attr.dataType) {
      case "DATE":
        return this.formatDate(value);
      case "DATE_SEQUENCE":
        if (Array.isArray(value)) {
          return value.map((v) => this.formatDate(v));
        } else {
          return value;
        }
      case "FILE_LINK":
        return Object.assign(new MDMLinkExt(), value).fileName;
      case "FILE_LINK_SEQUENCE":
      case "FILE_RELATION":
        switch ((value as MDMLinkExt[]).length) {
          case 0:
            return this.translateService.instant("navigator.attribute-value.msg-no-files-attached");
          case 1:
            return this.translateService.instant("navigator.attribute-value.msg-one-file-attached");
          default:
            return this.translateService.instant("navigator.attribute-value.msg-x-files-attached", {
              numberOfFiles: (value as MDMLinkExt[]).length,
            });
        }
      default:
        return value;
    }
  }

  private formatDate(dateValue) {
    return this.timezoneService.formatDateTime(this.parseBackendDate(dateValue));
  }
  private parseBackendDate(value: string) {
    return DateTime.fromISO(value, {
      zone: this.timezoneService.getSelectedTimezone(),
    });
  }

  private ellipsizeArray(fieldValue, size = 2) {
    if (fieldValue && Array.isArray(fieldValue) && fieldValue.length > size) {
      return fieldValue.slice(0, size).join(",") + ",...";
    }
    return fieldValue;
  }
}
