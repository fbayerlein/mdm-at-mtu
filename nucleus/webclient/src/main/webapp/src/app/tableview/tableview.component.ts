/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem, SortEvent } from "primeng/api";
import { BasketService } from "../basket/basket.service";
import { streamTranslate, TRANSLATE } from "../core/mdm-core.module";
import { NavigatorService } from "../navigator/navigator.service";
import { Node } from "../navigator/node";
import { NodeService } from "../navigator/node.service";
import { SearchAttribute } from "../search/search.service";
import { Row, SearchResult } from "./query.service";
import { View, ViewColumn } from "./tableview.service";

export class ViewValue {
  measured: any;
  ordered: any;
  dataType: string;

  static sort(value1: ViewValue, value2: ViewValue) {
    let v1 = value1.measured;
    if (value1.measured === null || value1.measured === undefined) {
      v1 = value1.ordered;
    }

    let v2 = value2.measured;
    if (value2.measured === null || value2.measured === undefined) {
      v2 = value2.ordered;
    }

    if (v1 === null && v2 !== null) {
      return -1;
    } else if (v1 !== null && v2 === null) {
      return 1;
    } else if (v1 === null && v2 === null) {
      return 0;
    } else if (!isNaN(v1) && !isNaN(v2)) {
      if (v1 === v2) {
        return 0;
      } else {
        return +v1 < +v2 ? -1 : 1;
      }
    } else {
      return String(v1).localeCompare(String(v2));
    }
  }
}
export class TurboViewColumn {
  type: string;
  attribute: string;
  width: number;
  sortOrder: number;

  field: string;
  header: string;

  constructor(type: string, attribute: string, width?: number, sortOrder?: number) {
    this.type = type;
    this.attribute = attribute;
    this.sortOrder = sortOrder != undefined ? sortOrder : 0;
    this.width = width;

    this.field = this.type + "_" + this.attribute;
    this.header = this.type + " " + this.attribute;
  }
}

@Component({
  selector: "mdm-tableview",
  templateUrl: "tableview.component.html",
  styleUrls: ["./tableview.component.css"],
})
export class TableviewComponent implements OnInit, OnChanges {
  public static readonly pageSize = 5;
  readonly buttonColumns = 3;

  @Input() view: View;
  @Input() results: SearchResult;

  /** @todo: Bad practice. value defined in template will be assumed to be a string.
  example: in template: [isShopable]="false", in component if(isShopable) {console.log('yes')} else { console.log('no')} will log yes!
  **/
  @Input() isShopable = false;
  @Input() isRemovable = false;
  @Input() menuItems: MenuItem[] = [];
  @Input() selectedEnvs: Node[];
  @Input() searchAttributes: { [env: string]: SearchAttribute[] };
  @Input() environments: Node[];
  @Input() loading: true;
  @Input() loadingIcon = "fa-spinner";
  @Input() idField = "id";

  @Output() selectedRow = new EventEmitter<Row>();
  @Output() selectedRows = new EventEmitter<Row[]>();

  public menuSelectedRow: Row;
  public columnsToShow: ViewColumn[];
  public readonly buttonColStyle = { width: "3%" };
  public btnColHidden = false;

  public viewRows: Row[];
  public turboViewColumns: TurboViewColumn[];
  public selectedViewRows: Row[] = [];

  constructor(
    private basketService: BasketService,
    private navigatorService: NavigatorService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.menuItems.push({
      id: "sc-ctx-show-tree",
      label: "Show in tree",
      icon: "fa fa-tree",
      command: (event) => this.openInTree(this.menuSelectedRow),
    });
    this.menuItems.push({
      id: "sc-ctx-reset",
      label: "Reset selection",
      icon: "fa fa-square-o",
      command: (event) => {
        this.selectedViewRows = [];
        this.menuSelectedRow = undefined;
        this.selectedRow.emit(undefined);
      },
    });

    // only exchange the label as the subscribe gets executed on a language change
    streamTranslate(this.translateService, TRANSLATE("tableview.tableview.show-in-tree")).subscribe((labels) => {
      const existingItem = this.menuItems.find((mi) => mi.id === "sc-ctx-show-tree");
      if (existingItem) {
        existingItem.label = labels;
      }
    });

    streamTranslate(this.translateService, TRANSLATE("tableview.tableview.reset-selection")).subscribe((labels) => {
      const existingItem = this.menuItems.find((mi) => mi.id === "sc-ctx-reset");
      if (existingItem) {
        existingItem.label = labels;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["results"] && this.view != undefined) {
      this.customSort({
        field: View.getSortField(this.view),
        order: View.getSortOrder(this.view),
      });
      this.viewRows = this.results.rows.map((row) => this.mapRow2View(row));
    }
    if (changes["view"] && this.view != undefined) {
      this.turboViewColumns = this.view.columns.map((viewCol) => this.mapColumn2Turbo(viewCol));
    }
    if (changes["selectedEnvs"] || changes["view"]) {
      this.updateColumnsToShow();
    }
  }

  mapRow2View(row: Row) {
    const viewRow: Row = new Row();
    row.columns.forEach((col) => (viewRow[col.type + "_" + col.attribute] = this.value2View(col.value, col.orderedValue, col.valueType)));
    viewRow["id"] = row.id;
    viewRow["type"] = row.type;
    viewRow["source"] = row.source;
    return viewRow;
  }

  value2View(measured: any, ordered: any, valueType: string) {
    const v = new ViewValue();
    v.dataType = valueType;
    if (measured === null || measured === undefined) {
      v.measured = "-";
    } else {
      v.measured = this.stringify(measured, valueType);
    }

    if (ordered === null) {
      // context attribute, but ordered value absent
      v.ordered = "-";
    } else if (ordered === undefined) {
      // no context attribute, ordered and measured value are the same
      v.ordered = v.measured;
    } else {
      v.ordered = this.stringify(ordered, valueType);
    }

    return v;
  }

  stringify(value: any, valueType: string) {
    if (valueType === "FILE_LINK") {
      return value["fileName"] ? value["fileName"] : "-";
    } else if (valueType === "FILE_LINK_SEQUENCE") {
      return (value as any[]).map((v) => this.stringify(v, "FILE_LINK")).join(", ");
    } else {
      return value;
    }
  }

  mapColumn2Turbo(viewCol: ViewColumn) {
    return new TurboViewColumn(viewCol.type, viewCol.name, viewCol.width);
  }

  /** @TODO: update for viewRows/columns
   **/
  updateColumnsToShow() {
    if (
      this.view &&
      this.selectedEnvs &&
      this.selectedEnvs.length > 0 &&
      this.searchAttributes &&
      this.searchAttributes[this.selectedEnvs[0].sourceName]
    ) {
      let relevantCols: string[] = [];
      this.selectedEnvs.forEach(
        (env) =>
          (relevantCols = relevantCols.concat(
            this.searchAttributes[env.sourceName].map((sa) => sa.boType.toLowerCase() + sa.attrName.toLowerCase()),
          )),
      );
      relevantCols = Array.from(new Set(relevantCols));

      this.view.columns.filter((col) => {
        if (relevantCols.findIndex((ct) => ct === col.type.toLowerCase() + col.name.toLowerCase()) > -1) {
          col.hidden = false;
        } else {
          col.hidden = true;
        }
        this.btnColHidden = false;
      });
    } else if (this.view && this.selectedEnvs && this.selectedEnvs.length === 0) {
      this.btnColHidden = true;
      this.view.columns.forEach((vc) => (vc.hidden = true));
    }
  }

  onContextMenuSelect(event: any) {
    this.menuSelectedRow = event.data;
    this.selectedRow.emit(this.menuSelectedRow);
  }

  /** @todo: p-table is not supporting manual column width. Might be kept as guide line in case
   * future version of p-table support checkbox with multi selection.
   *   @deprecated
   **/
  onColResize(event: any) {
    const index = event.element.cellIndex - this.buttonColumns;
    if (index > -1) {
      this.view.columns[index].width = event.element.clientWidth;
    }
  }

  onColReorder(event: any) {
    const removed = this.view.columns.splice(event.dragIndex, 1);
    if (this.view.columns.length === event.dropIndex) {
      this.view.columns.push(removed[0]);
    } else {
      const result: ViewColumn[] = [];
      this.view.columns.forEach((vc, idx) => {
        if (idx === event.dropIndex) {
          result.push(removed[0]);
        }
        result.push(vc);
      });
      this.view.columns = result;
    }
  }

  onSort(event: any) {
    if (this.view != undefined) {
      this.view.sortField = event.field;
      this.view.sortOrder = event.order;
    }
  }

  customSort(event: SortEvent) {
    const comparer = function (row1: Row, row2: Row): number {
      const value1 = row1[event.field] as ViewValue;
      const value2 = row2[event.field] as ViewValue;

      return ViewValue.sort(value1, value2) * event.order;
    };
    if (event.data) {
      event.data.sort(comparer);
    }
  }

  functionalityProvider(row: Row) {
    const item = Row.getItem(row);
    if (this.isShopable) {
      this.basketService.add(item);
    } else {
      this.basketService.remove(item);
    }
  }

  /** @todo: should be converted to pipe. avoid function calls from template (apart from event binding) for performance.
   **/
  getNodeClass(type: string) {
    switch (type) {
      case "StructureLevel":
        return "pool";
      case "MeaResult":
        return "measurement";
      case "SubMatrix":
        return "channelgroup";
      case "MeaQuantity":
        return "channel";
      default:
        return type.toLowerCase();
    }
  }

  geColValueForRow(value: string) {
    if (typeof value === "string" && value != null) {
      const pos = value.indexOf("/OrderedValue:");
      if (pos > 0) {
        return value.substring(0, pos);
      }
    }
    return value;
  }

  hasOrderedValue(value: string) {
    console.log(value);
    if (typeof value === "string" && value != null) {
      return value.indexOf("/OrderedValue:") > 0;
    }
    return false;
  }

  getOrderedToolTip(value: string, ordered: string, measured: string) {
    if (typeof value === "string" && value != null) {
      const pos = value.indexOf("/OrderedValue:");
      if (pos > 0) {
        return measured + ": " + value.substring(0, pos) + "\n" + ordered + ": " + value.substring(pos + 14);
      }
    }
    return value;
  }

  /** @todo: should be converted to pipe. avoid function calls from template (apart from event binding) for performance.
   **/
  getRowTitle(row: Row) {
    return (
      this.translateService.instant("tableview.tableview.tooltip-open-in") +
      ": " +
      [NodeService.mapSourceNameToName(this.environments, row.source), row.type, row.id].join("/")
    );
  }

  /** @todo: should be converted to pipe. avoid function calls from template for performance.
   *           option b) convert to componant field, since its not depending on result data.
   **/
  getIconTitle() {
    return this.isShopable
      ? TRANSLATE("tableview.tableview.tooltip-add-to-shopping-basket")
      : TRANSLATE("tableview.tableview.tooltip-remove-from-shopping-basket");
  }

  openInTree(row?: Row) {
    if (row != undefined) {
      this.selectedViewRows = [row];
    }
    if (this.selectedViewRows != undefined && this.selectedViewRows.length === 0 && this.menuSelectedRow != undefined) {
      this.navigatorService.fireOnOpenInTree([Row.getItem(this.menuSelectedRow)]);
    } else if (row) {
      this.navigatorService.fireOnOpenInTree([Row.getItem(row)]);
    } else {
      this.navigatorService.fireOnOpenInTree(this.selectedViewRows.map((r) => Row.getItem(r)));
    }
  }

  onSelectionChanged($event) {
    this.selectedRows.emit(this.selectedViewRows);
  }
}
