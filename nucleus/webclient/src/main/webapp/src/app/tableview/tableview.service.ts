/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { EventEmitter, Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { defaultIfEmpty, filter, map } from "rxjs/operators";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { Preference, PreferenceService, Scope } from "../core/preference.service";

export class ViewColumn {
  type: string;
  name: string;
  hidden = false;
  sortOrder: number;
  width: number;

  constructor(type: string, name: string, width?: number, sortOrder?: number) {
    this.type = type;
    this.name = name;
    this.width = width;
    this.sortOrder = sortOrder;
  }

  public static equals(vc1: ViewColumn, vc2: ViewColumn) {
    return vc1.type === vc2.type && vc1.name === vc2.name;
  }
}

export class View {
  name: string;
  columns: ViewColumn[] = [];
  sortOrder: number;
  sortField: string;

  constructor(name?: string, cols?: ViewColumn[]) {
    this.name = name || ""; // TODO TRANSLATION: find a way to name a new view "New view" in current language
    this.columns = cols || [];
  }

  public static setSortOrder(view: View, type: string, name: string, order: any) {
    view.columns.forEach((c) => {
      if (c.type === type && c.name === name) {
        c.sortOrder = order;
      } else {
        c.sortOrder = null;
      }
    });
  }

  public static getSortOrder(view: View) {
    const col = view.columns.find((c) => c.sortOrder !== null);
    if (col) {
      return col.sortOrder;
    }
  }

  public static getSortField(view: View) {
    const col = view.columns.find((c) => c.sortOrder !== null);
    if (col) {
      return col.type + "." + col.name;
    }
  }
}

export class PreferenceView {
  scope: string;
  view: View;

  constructor(scope: string, view?: View) {
    this.scope = scope;
    this.view = view || new View();
  }
}

export class Style {
  [field: string]: string;
}

@Injectable()
export class ViewService {
  public viewSaved$ = new EventEmitter();
  public viewDeleted$ = new EventEmitter();

  readonly preferencePrefix = "tableview.view.";
  private views: View[] = [];

  defaultPrefViews = [new PreferenceView(Scope.SYSTEM, new View("Standard", [new ViewColumn("Test", "Name")]))];

  constructor(
    private prefService: PreferenceService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {}

  getViews() {
    return this.prefService.getPreference(this.preferencePrefix).pipe(
      map((preferences) => preferences.map((p) => new PreferenceView(p.scope, JSON.parse(p.value) as View))),
      filter((prefViews) => !(prefViews === undefined || prefViews.length === 0)),
      defaultIfEmpty(this.defaultPrefViews),
    );
  }

  saveView(view: View) {
    const pref = new Preference();
    pref.value = JSON.stringify(view);
    pref.key = this.preferencePrefix + view.name;
    pref.scope = Scope.USER;
    return this.prefService.savePreference(pref);
  }

  getDefaultView() {
    return this.prefService.getPreference("tableview.defaultView").pipe(
      filter((data) => !(data === undefined || data.length === 0)),
      map((data) => JSON.parse(data[0].value)),
      defaultIfEmpty(""),
    );
  }

  setDefaultView(view: View) {
    const pref = new Preference();
    pref.value = JSON.stringify(view.name);
    pref.key = "tableview.defaultView";
    pref.scope = Scope.USER;
    return this.prefService.savePreference(pref);
  }

  deleteView(name: string) {
    return this.prefService.deletePreferenceByScopeAndKey(Scope.USER, "tableview.view." + name);
  }
}
