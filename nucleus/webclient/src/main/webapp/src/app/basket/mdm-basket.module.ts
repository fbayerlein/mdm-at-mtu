/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { MDMDetailModule } from "@details/mdm-detail.module";
import { FileExplorerModule } from "@file-explorer/file-explorer.module";
import { AccordionModule } from "primeng/accordion";
import { ConfirmationService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DialogModule } from "primeng/dialog";
import { FileUploadModule } from "primeng/fileupload";
import { PanelModule } from "primeng/panel";
import { SplitButtonModule } from "primeng/splitbutton";
import { TableModule } from "primeng/table";
import { MDMCoreModule } from "../core/mdm-core.module";
import { TableViewModule } from "../tableview/tableview.module";
import { MDMBasketComponent } from "./mdm-basket.component";

@NgModule({
  imports: [
    MDMCoreModule,
    TableViewModule,
    TableModule,
    PanelModule,
    FileUploadModule,
    FileExplorerModule,
    SplitButtonModule,
    ConfirmDialogModule,
    AccordionModule,
    DialogModule,
    MDMDetailModule,
  ],
  declarations: [MDMBasketComponent],
  exports: [MDMBasketComponent],
  providers: [ConfirmationService],
})
export class MDMBasketModule {}
