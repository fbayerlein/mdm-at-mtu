/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule, HttpTestingController, RequestMatch } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { PreferenceService, Scope } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { BasketService } from "./basket.service";

describe("BasketService", () => {
  let httpTestingController: HttpTestingController;

  const reqMatch: RequestMatch = { method: "GET" };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BasketService, PropertyService, PreferenceService],
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe("check init", () => {
    it("create service", async(
      inject([BasketService], (basketService) => {
        expect(basketService).toBeTruthy();
      }),
    ));
  });

  describe("getFileExtension()", () => {
    it("should return value configured in preference", async(
      inject([BasketService], (basketService) => {
        const mockResponse = {
          preferences: [
            {
              id: 2,
              key: "shoppingbasket.fileextensions",
              scope: Scope.SYSTEM,
              source: null,
              user: null,
              value: '[ { "label": "MyTool", "extension": "mdm-mytool" }, { "label": "OtherTool", "extension": "mdm-other" } ]',
            },
          ],
        };

        basketService.getFileExtensions().subscribe(
          (ext) =>
            expect(ext).toEqual([
              { label: "MyTool", extension: "mdm-mytool" },
              { label: "OtherTool", extension: "mdm-other" },
            ]),
          (err) => expect(err).toBeUndefined(),
        );

        const req = httpTestingController.expectOne(reqMatch);
        req.flush(mockResponse);
      }),
    ));

    it("should return empty array (default) if no preferences were found", async(
      inject([BasketService], (basketService) => {
        const mockResponse = {
          preferences: [],
        };

        basketService.getFileExtensions().subscribe(
          (ext) => expect(ext).toEqual([]),
          (err) => expect(err).toBeUndefined(),
        );

        const req = httpTestingController.expectOne(reqMatch);
        req.flush(mockResponse);
      }),
    ));

    it("should return empty array (default) if no preferences value is invalid", async(
      inject([BasketService], (basketService) => {
        const mockResponse = {
          preferences: [
            {
              id: 2,
              key: "shoppingbasket.fileextensions",
              scope: Scope.SYSTEM,
              source: null,
              user: null,
              value: "asdf",
            },
          ],
        };

        basketService.getFileExtensions().subscribe((ext) => expect(ext).toEqual([]));

        const req = httpTestingController.expectOne(reqMatch);
        req.flush(mockResponse);
      }),
    ));
  });
});
