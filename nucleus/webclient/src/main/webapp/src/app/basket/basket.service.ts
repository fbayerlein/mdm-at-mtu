/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { EventEmitter, Injectable, Output } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { equalsMDMItem, MDMItem } from "@core/mdm-item";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { of } from "rxjs";
import { catchError, defaultIfEmpty, flatMap, map } from "rxjs/operators";

export class Basket {
  name: string;
  items: MDMItem[] = [];

  constructor(name: string, items: MDMItem[]) {
    this.name = name;
    this.items = items;
  }
}

@Injectable()
export class BasketService {
  @Output() itemsAdded$ = new EventEmitter<MDMItem[]>();
  @Output() itemsRemoved$ = new EventEmitter<MDMItem[]>();
  readonly preferencePrefix = "basket.nodes.";
  readonly preferenceFileextensions = "shoppingbasket.fileextensions";

  items: MDMItem[] = [];

  constructor(private _pref: PreferenceService, private http: HttpClient, private _prop: PropertyService) {}

  public add(item: MDMItem) {
    const existingItem = this.items.find((i) => equalsMDMItem(i, item));

    if (!existingItem) {
      this.items.push(item);
      this.itemsAdded$.emit([item]);
    }
  }

  public addMany(items: MDMItem[]) {
    const itemsToAdd: MDMItem[] = items.filter((item: MDMItem) => !this.items.find((i) => equalsMDMItem(i, item)));

    if (itemsToAdd.length > 0) {
      this.items.push(...itemsToAdd);
      this.itemsAdded$.emit(itemsToAdd);
    }
  }

  /**
   * This will just add the MDMItem to the internal list without emitting it.
   * This is a late callback method when emitting virtual nodes and the real nodes
   * are loaded afterwards only.
   * @param item
   */
  public addOnly(item: MDMItem) {
    const existingItem = this.items.find((i) => equalsMDMItem(i, item));

    if (!existingItem) {
      this.items.push(item);
    }
  }

  public addAll(items: MDMItem[]) {
    const newItemsWithoutExisting = items.filter(
      (newItem) => this.items.findIndex((existingItem) => equalsMDMItem(existingItem, newItem)) < 0,
    );

    if (newItemsWithoutExisting) {
      // only add real nodes, skip virtual nodes
      newItemsWithoutExisting.filter((itm) => itm.id !== undefined).forEach((item) => this.items.push(item));
      this.itemsAdded$.emit(newItemsWithoutExisting);
    }
  }

  public remove(item: MDMItem) {
    const itemsToRemove = this.items.filter((i) => equalsMDMItem(i, item));

    if (itemsToRemove.length >= 0) {
      itemsToRemove.forEach((i) => (this.items = this.items.filter((it) => !equalsMDMItem(it, i))));
      this.itemsRemoved$.emit(itemsToRemove);
    }
  }

  removeAll() {
    this.items = [];
  }

  saveBasketWithName(name: string) {
    return this.saveBasket(new Basket(name, this.items));
  }

  saveBasket(basket: Basket) {
    return this._pref.savePreference(this.basketToPreference(basket)).subscribe();
  }

  getBaskets() {
    return this._pref.getPreference(this.preferencePrefix).pipe(map((preferences) => preferences.map((p) => this.preferenceToBasket(p))));
  }

  getItems() {
    return this.items;
  }

  setItems(items: MDMItem[]) {
    this.items = items;
  }

  getBasketAsXml(basket: Basket) {
    return this.http
      .post(this._prop.getUrl("mdm/shoppingbasket"), basket, { responseType: "text" })
      .pipe(catchError((e) => addErrorDescription(e, "Could not load shopping basket as XML.")));
  }

  getBasketAsAtfx(basket: Basket, filetype: string) {
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/xml" }),
      responseType: "blob" as "json",
      observe: "response" as "body",
      params: new HttpParams().set("quantities_in_header", "true"),
    };

    return this.getBasketAsXml(basket).pipe(
      flatMap((b) => this.http.post<any>(this._prop.getUrl("mdm/export/" + filetype), b, httpOptions)),
      catchError((e) => addErrorDescription(e, "Could not export shopping basket.")),
    );
  }

  getFileExtensions() {
    return this._pref.getPreferenceForScope(Scope.SYSTEM, this.preferenceFileextensions).pipe(
      flatMap((prefs) => prefs),
      map((pref) => JSON.parse(pref.value)),
      catchError((e) => {
        console.log('Unable to parse value of preference "' + this.preferenceFileextensions + '"!');
        return of([]);
      }),
      defaultIfEmpty([]),
    );
  }

  private preferenceToBasket(pref: Preference) {
    return JSON.parse(pref.value) as Basket;
  }

  private basketToPreference(basket: Basket) {
    const pref = new Preference();
    pref.value = JSON.stringify(basket);
    pref.key = this.preferencePrefix + basket.name;
    pref.scope = Scope.USER;
    return pref;
  }
}
