/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EventEmitter, Injectable, Output } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { MDMIdentifier } from "@core/mdm-identifier";
import { PropertyService } from "@core/property.service";
import { Node, NodeArray } from "@navigator/node";
import { EMPTY, Observable } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

export class MDMTag extends Node {
  description: string;
}

@Injectable()
export class MDMTagService {
  private prefEndpoint: string;
  @Output() tagUpdated: EventEmitter<void> = new EventEmitter<void>();

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.prefEndpoint = _prop.getUrl("mdm/environments/");
  }

  getAllMDMTags(scope: string): Observable<MDMTag[]> {
    if (!scope || scope === "") {
      return EMPTY;
    }
    return this.http.get<any>(this.prefEndpoint + scope + "/mdmtags").pipe(
      map((response) => (<MDMTag[]>response.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request mdmtags!")),
    );
  }

  saveMDMTag(scope: string, tag: MDMTag) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    const structure = {};
    structure["Name"] = tag.name;

    if (parseInt(tag.id, 10) > 0) {
      // update
      structure["Description"] = tag.description;
      return this.http.put<any>(this.prefEndpoint + scope + "/mdmtags/" + tag.id, JSON.stringify(structure), options).pipe(
        tap(() => this.tagUpdated.emit()),
        catchError((e) => addErrorDescription(e, "Could not request MDMtags! ")),
      );
    } else {
      structure["MDMTag"] = tag;
      // create
      return this.http.post<any>(this.prefEndpoint + scope + "/mdmtags", JSON.stringify(structure), options).pipe(
        tap(() => this.tagUpdated.emit()),
        catchError((e) => addErrorDescription(e, "Could not request MDMtags! ")),
      );
    }
  }

  public deleteMDMTag(scope: string, tagId: string) {
    return this.http.delete<NodeArray>(this.prefEndpoint + scope + "/mdmtags/" + tagId).pipe(
      tap(() => this.tagUpdated.emit()),
      catchError((e) => addErrorDescription(e, "Could not delete MDMtags!")),
    );
  }

  getMDMTagOfTagable(scope: string, tagable: MDMIdentifier): Observable<Node[]> {
    if (!scope || scope === "") {
      return EMPTY;
    }
    let typeURL = "/tests/";

    if (tagable.type === "TestStep") {
      typeURL = "/teststeps/";
    } else if (tagable.type === "Measurement") {
      typeURL = "/measurements/";
    }

    const url = this.prefEndpoint + scope + typeURL + tagable.id + "/mdmtags";

    return this.http.get<NodeArray>(url).pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request mdmtags!")),
    );
  }

  tagEntity(scope: string, mdmTag: MDMIdentifier, entity: Node) {
    if (!scope || scope === "") {
      return EMPTY;
    }
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };
    const url = this.prefEndpoint + scope + "/mdmtags/" + mdmTag.id + "/tag";

    const structure = {};
    structure[entity.type] = entity.id;

    return this.http.put<NodeArray>(url, JSON.stringify(structure), options).pipe(
      map((response) => response.data),
      tap(() => this.tagUpdated.emit()),
      catchError((e) => addErrorDescription(e, "Could not request mdmtags!")),
    );
  }

  removeTagEntity(scope: string, mdmTag: MDMIdentifier, entity: MDMIdentifier) {
    if (!scope || scope === "") {
      return EMPTY;
    }
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };
    const url = this.prefEndpoint + scope + "/mdmtags/" + mdmTag.id + "/remove";
    return this.http.put<NodeArray>(url, JSON.stringify(this.getMDMTagRequest(entity)), options).pipe(
      map((response) => response.data),
      tap(() => this.tagUpdated.emit()),
      catchError((e) => addErrorDescription(e, "Could not request mdmtags!")),
    );
  }

  getMDMTagRequest(entity: MDMIdentifier) {
    const mdmTagRequest = {};

    if (entity.type === "Test") {
      mdmTagRequest["Test"] = entity.id;
    } else if (entity.type === "TestStep") {
      mdmTagRequest["TestStep"] = entity.id;
    } else if (entity.type === "Measurement") {
      mdmTagRequest["Measurement"] = entity.id;
    }

    return mdmTagRequest;
  }
}
