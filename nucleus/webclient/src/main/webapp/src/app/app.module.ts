/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BasketService } from "@basket/basket.service";
import { HttpErrorHandler } from "@core/http-error-handler";
import { MDMCoreModule } from "@core/mdm-core.module";
import { MDMErrorHandler } from "@core/mdm-error-handler";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { StaleSessionInterceptor } from "@core/stale-session.interceptor";
import { FileImportService } from "@file-explorer/services/file-import.service";
import { FilesAttachableService } from "@file-explorer/services/files-attachable.service";
import { LocalizationService } from "@localization/localization.service";
import { MdmLocalizationService } from "@localization/mdm-localization.service";
import { NavigatorService } from "@navigator/navigator.service";
import { NodeService } from "@navigator/node.service";
import { NodeproviderService } from "@navigator/nodeprovider.service";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { ITranslationResource, MultiTranslateHttpLoader } from "ngx-translate-multi-http-loader";
import { ConfirmationService } from "primeng/api";
import { CheckboxModule } from "primeng/checkbox";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DialogModule } from "primeng/dialog";
import { DropdownModule } from "primeng/dropdown";
import { MessagesModule } from "primeng/messages";
import { SelectButtonModule } from "primeng/selectbutton";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FUNCTIONAL_RIGHTS_URLS } from "./administration/rights-config/rights-config.service";
import { AnnouncementComponent } from "./announcement/announcement.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CreateEntityMenuItemService } from "./entity-creator/menuitem.service";
import { FilereleaseService } from "./filerelease/filerelease.service";
import { MDMTagService } from "./mdmtag/mdmtag.service";
import { NoticeComponent } from "./notice.component";
import { StatusService } from "./status/status.service";
import { QueryService } from "./tableview/query.service";
import { ViewService } from "./tableview/tableview.service";
import { UserSettingsService } from "./user-prefs/services/user-settings.service";

class MDMTranslateHttpLoader extends MultiTranslateHttpLoader {
  constructor(http: HttpClient, resources: ITranslationResource[]) {
    super(http, resources);
  }

  getTranslation(lang: string): Observable<object> {
    try {
      return super.getTranslation(lang).pipe(catchError((e) => super.getTranslation("en")));
    } catch {
      return of({});
    }
  }
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new MDMTranslateHttpLoader(http, [{ prefix: "assets/i18n/", suffix: ".json" }]);
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ConfirmDialogModule,
    HttpClientModule,
    FormsModule,
    MDMCoreModule,
    DialogModule,
    AppRoutingModule,
    DropdownModule,
    SelectButtonModule,
    CheckboxModule,
    MessagesModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  declarations: [AppComponent, NoticeComponent, AnnouncementComponent],
  providers: [
    ConfirmationService,
    NodeService,
    LocalizationService,
    MdmLocalizationService,
    FilereleaseService,
    BasketService,
    NavigatorService,
    QueryService,
    NodeproviderService,
    MDMNotificationService,
    FilesAttachableService,
    FileImportService,
    ViewService,
    HttpErrorHandler,
    TranslateService,
    MDMTagService,
    CreateEntityMenuItemService,
    StatusService,
    UserSettingsService,
    { provide: ErrorHandler, useClass: MDMErrorHandler },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: StaleSessionInterceptor,
      multi: true,
    },
    {
      provide: FUNCTIONAL_RIGHTS_URLS,
      useValue: ["assets/functional-rights/rights_config.default.json"],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
