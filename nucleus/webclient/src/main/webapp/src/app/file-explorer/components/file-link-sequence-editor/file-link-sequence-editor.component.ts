/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnInit } from "@angular/core";
import { ContextAttributeIdentifier } from "@details/model/details.model";
import { MDMLinkExt } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { DialogService } from "primeng/dynamicdialog";
import { ContextFilesService } from "../../services/context-files.service";
import { FileExplorerDialogComponent } from "../file-explorer-dialog/file-explorer-dialog.component";

@Component({
  selector: "mdm-file-link-sequence-editor",
  templateUrl: "./file-link-sequence-editor.component.html",
})
export class FileLinkSequenceEditorComponent implements OnInit {
  @Input() contextAttribute: ContextAttributeIdentifier;

  public link: MDMLinkExt;
  public links: MDMLinkExt[];

  constructor(
    private dialogService: DialogService,
    private contextFilesService: ContextFilesService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    if (this.contextAttribute.attribute != undefined) {
      this.contextFilesService.getMDMFileExts(this.contextAttribute).subscribe((mdmFileExts) => {
        this.links = mdmFileExts;
        setTimeout(() => {
          this.onShowFileExplorerDialog();
        });
      });
    }
  }

  public onShowFileExplorerDialog() {
    let title = this.contextAttribute.contextComponent.name;
    if (this.contextAttribute.attribute != undefined) {
      title += "." + this.contextAttribute.attribute.name;
    }
    const hdr = this.translateService.instant("file-explorer.file-link-sequence-editor.hdr-file-upload-wizard", { name: title });

    const ref = this.dialogService.open(FileExplorerDialogComponent, {
      data: {
        contextDescribable: this.contextAttribute.contextDescribable,
        contextComponent: this.contextAttribute.contextComponent,
        attribute: this.contextAttribute.attribute,
        contextGroup: this.contextAttribute.contextGroup,
        contextType: this.contextAttribute.contextType,
        readOnly: false,
        useLink: true,
      },
      header: hdr,
      width: "70%",
    });
  }
}
