/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { MDMErrorResponse } from "@core/mdm-error-response";
import { MDMItem } from "@core/mdm-item";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService, SelectItem } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { FileUpload } from "primeng/fileupload";
import { AvailableDatasourceService } from "src/app/available-datasource/available-datasource.service";
import { FileImportRow, FormatSize } from "../../model/file-explorer.model";
import { FileImportService } from "../../services/file-import.service";
import { FileReaderService } from "../../services/file-reader.service";

@Component({
  selector: "mdm-file-import-dialog",
  templateUrl: "./file-import-dialog.component.html",
  styleUrls: ["../file-explorer.css"],
})
export class FileImportDialogComponent implements OnInit, OnChanges {
  @Input()
  public selectedDatasource: string;

  @Input()
  public showDialog = false;

  @Output()
  public showDialogChange = new EventEmitter<boolean>();

  // make enum accessible from html template
  public formatSize = FormatSize;

  // holding table selection for context menu
  public selectedFileMetasImport: FileImportRow[] = [];
  // holding selected node and context info incase files is uploaded to context attribute
  public mdmItem: MDMItem;

  public fileTypes: SelectItem[] = [
    { label: "ATFx", value: "atfx" },
    { label: "CSV", value: "csv" },
  ];

  public datasources: SelectItem[] = [];
  public fileType = this.fileTypes[0].value;

  public showErrorDialog = false;
  public error: MDMErrorResponse;

  constructor(
    private fileImportService: FileImportService,
    private fileReaderService: FileReaderService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private dsService: AvailableDatasourceService,
    private notificationService: MDMNotificationService,
  ) {}

  ngOnInit() {
    this.dsService.getAvailableDatasource().subscribe((value) => {
      this.datasources = value.map((ds) => {
        return { label: ds, value: ds } as SelectItem;
      });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const log: string[] = [];
    for (const propName in changes) {
      if (propName === "showDialog" && changes[propName].currentValue === true) {
        this.resetFileUploadSelection();
      }
    }
  }

  // helping function to clear file selection for upload overlay panel
  private resetFileUploadSelection() {
    this.selectedFileMetasImport = [];
    this.updateFileType();
  }

  private updateFileType() {
    if (this.selectedFileMetasImport.length === 0) {
      this.fileType = "atfx";
      return;
    }

    for (const f of this.selectedFileMetasImport) {
      if (f.file && f.file.name.toLowerCase().endsWith("csv")) {
        this.fileType = "csv";
      }
    }
    // atfx gets preference
    for (const f of this.selectedFileMetasImport) {
      if (f.file && f.file.name.toLowerCase().endsWith("atfx")) {
        this.fileType = "atfx";
      }
    }
  }
  // listener to removeFiles from selection for upload
  public onRemoveFile(event: MouseEvent, row: FileImportRow) {
    const index = this.selectedFileMetasImport.findIndex((fm) => fm === row);
    if (index > -1) {
      this.selectedFileMetasImport.splice(index, 1);
      this.updateFileType();
    }
  }

  // listener to clear file selection for upload
  public onClearSelectedFileMetasUpload(event: MouseEvent) {
    this.resetFileUploadSelection();
  }

  @ViewChild(FileUpload)
  private fileupload!: FileUpload;

  // listener to add files to upload selection
  public async onSelectFileForImport(selection: { files: File[] }) {
    const files = selection.files;
    for (let i = 0; i < files.length; i++) {
      const dataUrl = await this.fileReaderService.readFile(files[i]);
      if (this.selectedFileMetasImport.findIndex((fm) => fm.file.name === files[i].name) === -1) {
        this.selectedFileMetasImport.push(new FileImportRow(files[i], dataUrl));
        this.updateFileType();
      } else {
        this.notificationService.notifyWarn(
          "File could not be added.",
          "The selection already contains a file with the name '" + files[i].name + "'.",
        );
      }
    }
    // clear component or else no more files can be selected
    this.fileupload.clear();
  }

  // listener to import selected files
  // import selected files, closes dialog, and returns Observable for server response
  public onImportSelectedFiles() {
    this.fileImportService.importFiles(this.selectedDatasource, this.selectedFileMetasImport, this.fileType).subscribe({
      next: (data) => {
        this.notificationService.notifySuccess(
          this.translateService.instant("file-explorer.file-import.msg-ttl-file-imported"),
          this.translateService.instant("file-explorer.file-import.msg-detail-file-imported"),
        );
        this.closeDialog();
      },
      error: (e) => this.showError(e),
    });
  }

  public closeDialog() {
    this.showDialog = false;
    this.showDialogChange.emit(this.showDialog);
  }

  public showError(e: Error) {
    this.error = (e as any).error as MDMErrorResponse;
    this.showErrorDialog = true;
  }

  public closeErrorDialog() {
    this.showErrorDialog = false;
    this.error = undefined;
    this.closeDialog();
  }
}
