/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { Attribute, ContextGroup, Node } from "@navigator/node";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "mdm-file-explorer-dialog",
  templateUrl: "./file-explorer-dialog.component.html",
})
export class FileExplorerDialogComponent implements OnInit {
  public contextComponent: Node;
  public contextDescribable: Node;
  public attribute: Attribute;
  public contextGroup: ContextGroup;
  public contextType: string;
  public readOnly: boolean;
  public useLink: boolean;

  public constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {}

  ngOnInit() {
    if (this.config != undefined) {
      this.contextComponent = this.config.data.contextComponent as Node;
      this.contextDescribable = this.config.data.contextDescribable as Node;
      this.attribute = this.config.data.attribute as Attribute;
      this.contextGroup = this.config.data.contextGroup as ContextGroup;
      this.contextType = this.config.data.contextType as string;
      this.readOnly = this.config.data.readOnly as boolean;
      this.useLink = this.config.data.useLink as boolean;
    }
  }
}
