/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Pipe, PipeTransform } from "@angular/core";
import { MDMLinkExt } from "@navigator/node";

@Pipe({
  name: "fileName",
})
export class FileNamePipe implements PipeTransform {
  transform(link: MDMLinkExt): any {
    return link != undefined ? Object.assign(new MDMLinkExt(), link).fileName : "";
  }
}
