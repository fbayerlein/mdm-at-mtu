/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { TRANSLATE } from "@core/mdm-core.module";
import { ContextAttributeIdentifier } from "@details/model/details.model";
import { Attribute, ContextGroup, FileSize, MDMLinkExt, Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import * as FileSaver from "file-saver";
import { ConfirmationService, MenuItem } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { Observable, throwError } from "rxjs";
import { map } from "rxjs/operators";
import { FileExplorerColumn, FileExplorerRow, FileLinkContextWrapper, FileUploadRow } from "../../model/file-explorer.model";
import { ContextFilesService } from "../../services/context-files.service";
import { FileService } from "../../services/file.service";
import { FilesAttachableService } from "../../services/files-attachable.service";
import { FileUploadDialogComponent } from "../file-upload-dialog/file-upload-dialog.component";

@Component({
  selector: "mdm-file-explorer",
  templateUrl: "./file-explorer.component.html",
  styleUrls: ["../file-explorer.css"],
  providers: [ConfirmationService],
})
export class FileExplorerComponent implements OnInit, OnChanges {
  private readonly CONTEXT_COMPONENT = "ContextComponent";

  // holding column configuration for table
  public columnConfigs: FileExplorerColumn[] = [];
  // holding file metadata for table
  public fileMetas: FileExplorerRow[] = [];
  // holding table selection
  public selectedFileMeta: FileExplorerRow;
  // holding node selected in navigator
  @Input() public node: Node;
  @Input() public contextDescribable?: Node;
  @Input() public attribute: Attribute;
  @Input() public contextGroup?: ContextGroup;
  @Input() public contextType?: string;
  @Input() public readOnly: boolean;
  @Input() public useLink: boolean;

  //public links: MDMLinkExt[];

  public displayCreateLinkDialog = false;
  public link = "";

  // items for context menu
  public ctxMenuItems: MenuItem[];

  public constructor(
    private filesAttachableService: FilesAttachableService,
    private contextFilesService: ContextFilesService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private fileService: FileService,
  ) {}

  public ngOnInit() {
    this.initColumns();
    // workaround for ctx menu translation, since pipe cannot be passed into primeng contextmenu.
    this.initCtxMenu();
    this.translateService.onLangChange.subscribe(() => this.initCtxMenu());
    this.init();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["node"]) {
      this.init();
    }
  }

  // initialize component data
  private init() {
    if (this.node != undefined) {
      this.fileMetas = [];
      this.selectedFileMeta = undefined;

      // case for files attached to context attributes
      if (this.node.type === this.CONTEXT_COMPONENT || this.contextDescribable != undefined) {
        if (this.attribute != undefined) {
          this.contextFilesService
            .getMDMFileExts(this.getIdent())
            .pipe(map((mdmLinkExts) => mdmLinkExts.map((mdmLinkExt) => this.link2Row(mdmLinkExt))))
            .subscribe((mdmFileExts) => {
              this.fileMetas = mdmFileExts;
              this.loadFileSizes();
            });
        }
      } else {
        // case for files attached to filesAttachables
        this.filesAttachableService
          .getMDMFileExts(this.node)
          .pipe(map((mdmLinkExts) => mdmLinkExts.map((mdmLinkExt) => this.link2Row(mdmLinkExt))))
          .subscribe((mdmFileExts) => {
            this.fileMetas = mdmFileExts;
            this.loadFileSizes();
          });
      }
    }
  }

  // defines column configuration.
  private initColumns() {
    this.columnConfigs = [];
    this.columnConfigs.push(new FileExplorerColumn("filename", TRANSLATE("file-explorer.file-explorer.hdr-filename")));
    this.columnConfigs.push(new FileExplorerColumn("description", TRANSLATE("file-explorer.file-explorer.hdr-description")));
    this.columnConfigs.push(new FileExplorerColumn("type", TRANSLATE("file-explorer.file-explorer.hdr-type")));
    this.columnConfigs.push(new FileExplorerColumn("size", TRANSLATE("file-explorer.file-explorer.hdr-size")));
  }

  // defines context menu configuration.
  private initCtxMenu() {
    this.ctxMenuItems = [];
    this.translateService
      .get("file-explorer.file-explorer.btn-preview-file")
      .subscribe((t) => this.ctxMenuItems.push({ label: t, icon: "fa fa-search", command: (event) => this.onPreviewFile(event) }));
    this.translateService
      .get("file-explorer.file-explorer.btn-download-file")
      .subscribe((t) =>
        this.ctxMenuItems.push({ label: t, icon: "fa fa-arrow-circle-o-down", command: (event) => this.onDownloadFile(event) }),
      );
    this.translateService
      .get("file-explorer.file-explorer.btn-delete-file")
      .subscribe((t) => this.ctxMenuItems.push({ label: t, icon: "pi pi-times", command: (event) => this.onDeleteFile(event) }));
  }

  // listener to refresh selected node
  public onRefresh() {
    this.reloadSelectedNode();
  }

  // reloads the selected node (and consequently all file data)
  private reloadSelectedNode() {
    this.init();
  }

  // mapping function to create row data object from MDMLinkExt array entry
  private link2Row(mdmLinkExt: MDMLinkExt) {
    if (mdmLinkExt === undefined) {
      return new FileExplorerRow(undefined, undefined, undefined, undefined, "-");
    }

    return new FileExplorerRow(mdmLinkExt.identifier, mdmLinkExt.fileName, mdmLinkExt.description, mdmLinkExt.mimeType, mdmLinkExt.size);
  }

  // loads file sizes
  private loadFileSizes() {
    // case for files attached to context attributes
    if (this.node.type === this.CONTEXT_COMPONENT || this.contextDescribable != undefined) {
      this.contextFilesService.loadFileSizes(this.getIdent()).subscribe((fileSizes) => this.updateFileSizes(fileSizes));
    } else {
      // case for files attached to filesAttachables
      this.filesAttachableService.loadFileSizes(this.node).subscribe((fileSizes) => this.updateFileSizes(fileSizes));
    }
  }

  // load size information from server for single file
  private loadSizeLazy(identifier: string) {
    // case for files attached to context attributes
    if (this.contextDescribable != undefined) {
      if (this.contextGroup != undefined) {
        this.contextFilesService.loadFileSize(identifier, this.getIdent()).subscribe((fileSize) => this.updateFileSize(fileSize));
      }
    } else {
      // case for files attached to filesAttachables
      this.filesAttachableService.loadFileSize(identifier, this.node).subscribe((fileSize) => this.updateFileSize(fileSize));
    }
  }

  // update size in row data for table
  private updateFileSizes(fileSizes: FileSize[]) {
    if (this.fileMetas != undefined && fileSizes != undefined && fileSizes.length > 0) {
      fileSizes.forEach((fileSize) => this.updateFileSize(fileSize));
    }
  }

  // update size in row data for table for single file
  private updateFileSize(fileSize: FileSize) {
    this.fileMetas.find((fm) => fm.identifier === fileSize.identifier).size = fileSize.size;
  }

  public onShowUploadDialog() {
    if (this.readOnly || (this.attribute?.dataType === "FILE_LINK" && this.fileMetas.length === 1)) {
      return;
    }
    let ref: DynamicDialogRef;
    const hdr = this.translateService.instant("file-explorer.file-explorer.hdr-file-upload-wizard", { name: this.node.name });
    // case for files attached to context attributes
    if (this.contextDescribable != undefined) {
      ref = this.dialogService.open(FileUploadDialogComponent, {
        data: {
          contextDescribable: this.contextDescribable,
          node: this.node,
          contextType: this.contextType,
          contextGroup: this.contextGroup,
          attribute: this.attribute,
        },
        header: hdr,
        width: "70%",
      });
      // case for files attached to filesAttachables
    } else {
      ref = this.dialogService.open(FileUploadDialogComponent, {
        data: { node: this.node, attribute: this.attribute },
        header: hdr,
        width: "70%",
      });
    }

    ref.onClose.subscribe((linkObs) => this.handleUploadDialogResponse(linkObs));
  }

  public onShowCreatLinkDialog() {
    if (
      !(
        this.readOnly ||
        this.attribute?.dataType === "FILE_RELATION" ||
        (this.attribute?.dataType === "FILE_LINK" && this.fileMetas.length === 1)
      )
    ) {
      this.displayCreateLinkDialog = true;
    }
  }

  public saveLink() {
    const row = new FileUploadRow(new File([""], this.link, { type: "application/x-openmdm.link" }), this.link, "");
    this.handleUploadDialogResponse(this.contextFilesService.uploadFiles([row], this.getIdent()));
    this.displayCreateLinkDialog = false;
    this.link = "";
  }

  // reflects file upload in client side state
  private handleUploadDialogResponse(linkObs: Observable<MDMLinkExt>) {
    if (linkObs != undefined) {
      linkObs.subscribe((link) => this.handleUpload(link));
    }
  }

  // listener to load blob from db and initialize download dialog for saving file to local filesystem.
  public onDownloadFile(event: MouseEvent) {
    if (this.selectedFileMeta != undefined) {
      this.loadFile().subscribe((blob) => FileSaver.saveAs(blob, this.selectedFileMeta.filename));
    }
  }

  private loadFile() {
    if (this.contextDescribable != undefined) {
      // case for files attached to context attributes
      if (this.contextGroup != undefined) {
        return this.contextFilesService.loadFile(this.selectedFileMeta.identifier, this.getIdent());
      }
      return throwError("ContextGroup is undefined!");
    } else {
      // case for files attached to filesAttachables
      return this.filesAttachableService.loadFile(this.selectedFileMeta.identifier, this.node);
    }
  }

  // listener for file preview
  public onPreviewFile(event: MouseEvent) {
    if (this.selectedFileMeta != undefined) {
      this.loadFile().subscribe((blob) => this.handlePreview(blob));
    }
  }

  // handle preview for different browsers. Opens download dialog if preview not possible.
  private handlePreview(blob: Blob) {
    if ("application/octet-stream" === blob.type || blob.type === undefined) {
      FileSaver.saveAs(blob, this.selectedFileMeta.filename);
    } else {
      window.open(URL.createObjectURL(blob));
    }
  }

  // listener for delete file button. Opens confirm dialog.
  public onDeleteFile(event: MouseEvent) {
    this.translateService.get("file-explorer.file-explorer.msg-confirm-delete-file-from-db").subscribe((msg) =>
      this.confirmationService.confirm({
        message: msg,
        key: "fileExplorerConfirmation",
        accept: () => this.deleteFileConfirmed(),
      }),
    );
  }

  // Actually triggers file delete from db, when confirmed in delete dialog
  public deleteFileConfirmed() {
    if (this.contextDescribable != undefined) {
      this.contextFilesService.deleteFile(this.selectedFileMeta.identifier, this.getIdent()).subscribe((link) => this.handleDelete(link));
    } else {
      // case for files attached to filesAttachables
      this.filesAttachableService.deleteFile(this.selectedFileMeta.identifier, this.node).subscribe((link) => this.handleDelete(link));
    }
    this.selectedFileMeta = undefined;
  }

  // removes row to fileMetas to reflect server side file delete in client state.
  private removeRow(link: MDMLinkExt) {
    if (link != undefined) {
      const i = this.fileMetas.findIndex((row) => link.identifier === row.identifier);
      if (i >= 0) {
        this.fileMetas.splice(i, 1);
      }
    }
  }

  // adds row to fileMetas to reflect file upload in client state. Lazy loads filesize.
  private addRow(link: MDMLinkExt) {
    if (link != undefined) {
      this.fileMetas.push(this.link2Row(link));
      if (link.size === undefined) {
        this.loadSizeLazy(link.identifier);
      }
    }
  }

  private handleUpload(link: MDMLinkExt) {
    this.addRow(link);
    if (this.contextGroup != undefined) {
      let value = this.attribute.value[this.contextGroup];
      if (value === undefined || value === "") {
        value = [];
        this.attribute.value[this.contextGroup] = value;
      }
      if (this.attribute.dataType === "FILE_LINK") {
        this.attribute.value[this.contextGroup] = link;
      } else {
        value.push(link);
      }
      this.fireOnChange();
    }
  }

  private handleDelete(link: MDMLinkExt) {
    this.removeRow(link);
    if (this.contextGroup != undefined) {
      if (this.attribute.dataType === "FILE_LINK") {
        this.attribute.value[this.contextGroup] = [];
      } else {
        const index = this.attribute.value[this.contextGroup].findIndex((p) => p.identifier === link.identifier);
        this.attribute.value[this.contextGroup].splice(index, 1);
      }
      this.fireOnChange();
    }
  }

  private fireOnChange() {
    const fileLinkContextWrapper = new FileLinkContextWrapper();
    fileLinkContextWrapper.attribute = this.attribute;
    fileLinkContextWrapper.contextComponent = this.node;
    fileLinkContextWrapper.contextType = this.contextType;
    this.fileService.fireOnFileChanged(fileLinkContextWrapper);
  }

  private getIdent() {
    return new ContextAttributeIdentifier(this.contextDescribable, this.node, this.attribute, this.contextGroup, this.contextType);
  }
}
