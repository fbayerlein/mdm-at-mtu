/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { FileImportRow } from "../model/file-explorer.model";

export class ImportResponse {
  state: string;
  message: string;
  stacktrace: string;
}

@Injectable()
export class FileImportService {
  private contextUrl: string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.contextUrl = _prop.getUrl("mdm/environments");
  }

  public importFiles(sourceName: string, files: FileImportRow[], type: string) {
    const formData = new FormData();
    files.forEach((file) => {
      formData.append(file.file.name, file.file);
    });

    const httpOptions = {
      params: new HttpParams().set("quantities_in_header", "true").set("add_measurement_points", "false").set("requireTemplate", "false"),
    };

    return this.http.post<ImportResponse>(this.contextUrl + "/" + sourceName + "/import/" + type, formData, httpOptions).pipe(
      map((r) => {
        if (r.state !== "OK") {
          const error = new Error(r.message);
          error.stack = r.stacktrace;
          return throwError(() => error);
        } else {
          return r;
        }
      }),
      catchError((e) => addErrorDescription(e, "Unexpected error during file import!")),
    );
  }
}
