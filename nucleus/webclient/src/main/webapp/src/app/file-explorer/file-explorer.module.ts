/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
// Angular Imports
import { NgModule } from "@angular/core";

// 3rd party modules
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { DialogService, DynamicDialogModule } from "primeng/dynamicdialog";
import { FileUploadModule } from "primeng/fileupload";
import { InputTextModule } from "primeng/inputtext";
import { MenuModule } from "primeng/menu";
import { PanelModule } from "primeng/panel";
import { SplitButtonModule } from "primeng/splitbutton";
import { TableModule } from "primeng/table";

// MDM modules
import { MDMCoreModule } from "../core/mdm-core.module";

// This Module's Components
import { FileAttributeDisplayComponent } from "./components/file-attribute-display/file-attribute-display.component";
import { FileAttributeViewerComponent } from "./components/file-attribute-viewer/file-attribute-viewer.component";
import { FileExplorerDialogComponent } from "./components/file-explorer-dialog/file-explorer-dialog.component";
import { FileExplorerNavCardComponent } from "./components/file-explorer-nav-card/file-explorer-nav-card.component";
import { FileExplorerComponent } from "./components/file-explorer/file-explorer.component";
import { FileImportDialogComponent } from "./components/file-import/file-import-dialog.component";
import { FileLinkSequenceEditorComponent } from "./components/file-link-sequence-editor/file-link-sequence-editor.component";
import { FileUploadDialogComponent } from "./components/file-upload-dialog/file-upload-dialog.component";
import { ThumbnailComponent } from "./components/thumbnail/thumbnail.component";
import { FileNamePipe } from "./pipes/file-name.pipe";
import { FileSizePipe } from "./pipes/file-size.pipe";

@NgModule({
  imports: [
    MDMCoreModule,
    TableModule,
    DynamicDialogModule,
    DialogModule,
    FileUploadModule,
    ConfirmDialogModule,
    ContextMenuModule,
    InputTextModule,
    MenuModule,
    PanelModule,
    SplitButtonModule,
  ],
  declarations: [
    ThumbnailComponent,
    FileSizePipe,
    FileUploadDialogComponent,
    FileNamePipe,
    FileExplorerComponent,
    FileExplorerDialogComponent,
    FileAttributeDisplayComponent,
    FileAttributeViewerComponent,
    FileLinkSequenceEditorComponent,
    FileExplorerNavCardComponent,
    FileImportDialogComponent,
  ],
  exports: [
    FileExplorerDialogComponent,
    FileExplorerNavCardComponent,
    FileAttributeViewerComponent,
    FileLinkSequenceEditorComponent,
    FileAttributeDisplayComponent,
    FileImportDialogComponent,
  ],
  providers: [FileNamePipe, FileSizePipe, DialogService],
  entryComponents: [FileUploadDialogComponent, FileExplorerDialogComponent, FileImportDialogComponent],
})
export class FileExplorerModule {}
