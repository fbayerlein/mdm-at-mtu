/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

export class FileExplorerRow {
  filename: string;
  description: string;
  type: string;
  size: string;
  // identifier is unique for one file-attachable MDM object
  // and is used as row identifier: fileExplorerRow <-> MDMLinkExt
  identifier: string;

  constructor(identifier: string, filename: string, description: string, type: string, size: string) {
    this.identifier = identifier;
    this.filename = filename;
    this.description = description;
    this.type = type;
    this.size = size;
  }
}
