/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { PropertyService } from "@core/property.service";
import { ContextAttributeIdentifier } from "@details/model/details.model";
import { FileSize, MDMLinkExt } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { from } from "rxjs";
import { catchError, concatMap, tap } from "rxjs/operators";
import { FileUploadRow } from "../model/file-explorer.model";

@Injectable({
  providedIn: "root",
})
export class ContextFilesService {
  private contextUrl: string;

  constructor(
    private http: HttpClient,
    private notificationService: MDMNotificationService,
    private _prop: PropertyService,
    private translateService: TranslateService,
  ) {
    this.contextUrl = _prop.getUrl("mdm/environments");
  }

  public getUrl(ident: ContextAttributeIdentifier, remotePath: string) {
    const val = this.getopenMDMLink(ident, remotePath);
    if (val !== undefined) {
      return val;
    }
    return this.getFileEndpoint(ident) + "/" + this.escapeUrlCharacters(remotePath);
  }

  // datatypes FIL_LINK and FILE_LINK_SEQUENCE can be links instead of files
  private getopenMDMLink(ident: ContextAttributeIdentifier, remotePath: string) {
    if (ident.attribute.dataType === "FILE_LINK") {
      if (
        ident.attribute.value[ident.contextGroup].identifier == remotePath &&
        ident.attribute.value[ident.contextGroup].mimeType == "application/x-openmdm.link"
      ) {
        return ident.attribute.value[ident.contextGroup].description;
      }
    } else if (ident.attribute.dataType === "FILE_LINK_SEQUENCE") {
      const value = ident.attribute.value.valueOf();
      const val = value[ident.contextGroup].find((v) => v.identifier == remotePath && v.mimeType == "application/x-openmdm.link");
      return val === undefined ? undefined : val.description;
    }
    return undefined;
  }

  private getFileEndpoint(ident: ContextAttributeIdentifier) {
    return (
      this.contextUrl +
      "/" +
      ident.contextComponent.sourceName +
      "/" +
      this.typeToUrl(ident.contextComponent.sourceType) +
      "/" +
      ident.contextComponent.id +
      "/contexts" +
      "/" +
      ident.contextType +
      "/" +
      this.contextGroupToUrl(ident.contextGroup) +
      "/" +
      this.escapeUrlCharacters(ident.contextDescribable.name) +
      "/" +
      this.escapeUrlCharacters(ident.attribute.name) +
      "/files"
    );
  }

  // get file information from server
  public getMDMFileExts(ident: ContextAttributeIdentifier) {
    const endpoint = this.getFileEndpoint(ident) + "/";

    return this.http.get<MDMLinkExt[]>(endpoint).pipe(catchError((e) => addErrorDescription(e, "Could not request file information!")));
  }

  // loads file size from server for file with given remote path
  public loadFileSize(fileIdentifier: string, ident: ContextAttributeIdentifier) {
    const endpoint = this.getFileEndpoint(ident) + "/size/" + this.escapeUrlCharacters(fileIdentifier);

    return this.http.get<FileSize>(endpoint).pipe(catchError((e) => addErrorDescription(e, "Could not request file size!")));
  }

  // loads file sizes from server
  public loadFileSizes(ident: ContextAttributeIdentifier) {
    const endpoint = this.getFileEndpoint(ident) + "/sizes";

    return this.http.get<FileSize[]>(endpoint).pipe(catchError((e) => addErrorDescription(e, "Could not request file sizes!")));
  }

  // loads file in context from server, returns file as blob.
  public loadFile(fileIdentifier: string, ident: ContextAttributeIdentifier) {
    const endpoint = this.getFileEndpoint(ident) + "/" + this.escapeUrlCharacters(fileIdentifier);

    return this.http
      .get<Blob>(endpoint, { responseType: "blob" as "json" })
      .pipe(catchError((e) => addErrorDescription(e, "Could not request file content!")));
  }

  // Uploads multiple files. Http requests are chained to avoid concurrency in server side update process.
  public uploadFiles(files: FileUploadRow[], ident: ContextAttributeIdentifier) {
    return from(files).pipe(concatMap((file) => this.uploadFile(file, ident)));
  }

  // upload file
  public uploadFile(fileData: FileUploadRow, ident: ContextAttributeIdentifier) {
    const fd = new FormData();
    fd.append("file", fileData.file, fileData.file.name);
    fd.append("lastModified", fileData.file.lastModified.toString());
    fd.append("mimeType", fileData.file.type);
    fd.append("description", fileData.description);

    const endpoint = this.getFileEndpoint(ident);

    return this.http.post<MDMLinkExt>(endpoint, fd).pipe(
      tap((link) =>
        this.notificationService.notifySuccess(
          this.translateService.instant("file-explorer.context-files.msg-ttl-file-created"),
          this.translateService.instant("file-explorer.context-files.msg-detail-file-created", {
            filename: link.fileName === undefined ? "???" : link.fileName,
          }),
        ),
      ),
      catchError((e) => addErrorDescription(e, "Could upload file!")),
    );
  }

  // delete file from server
  public deleteFile(identifier: string, ident: ContextAttributeIdentifier) {
    const endpoint = this.getFileEndpoint(ident) + "/" + this.escapeUrlCharacters(identifier);

    return this.http.delete<MDMLinkExt>(endpoint).pipe(
      tap((flink) =>
        this.notificationService.notifySuccess(
          this.translateService.instant("file-explorer.context-files.msg-ttl-file-deleted"),
          this.translateService.instant("file-explorer.context-files.msg-detail-file-deleted", {
            filename: flink.fileName === undefined ? "???" : flink.fileName,
          }),
        ),
      ),
      catchError((e) => addErrorDescription(e, "Could not delete file!")),
    );
  }

  // helping function to map sourceType to proper url for endpoint.
  private typeToUrl(type: string) {
    if (type != undefined) {
      switch (type) {
        case "MeaResult":
          return "measurements";
        default:
          return type.toLowerCase() + "s";
      }
    }
  }

  private contextGroupToUrl(type: number) {
    switch (type) {
      case 0:
        return "ordered";
      case 1:
        return "measured";
    }
  }

  // replaces / by %2F to not break url
  private escapeUrlCharacters(urlString: string) {
    return urlString.replace(/\//g, "%2F").replace(/\./g, "%2E").replace(/ /g, "%20");
  }
}
