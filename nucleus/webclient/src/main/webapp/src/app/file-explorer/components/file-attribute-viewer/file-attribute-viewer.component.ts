/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input } from "@angular/core";
import { ContextAttributeIdentifier } from "@details/model/details.model";
import { ContextFilesService } from "../../services/context-files.service";

@Component({
  selector: "mdm-file-attribute-viewer",
  templateUrl: "./file-attribute-viewer.component.html",
})
export class FileAttributeViewerComponent {
  @Input() contextAttribute: ContextAttributeIdentifier;

  constructor(private contextFilesService: ContextFilesService) {}

  getUrl(remotePath: string) {
    if (remotePath != undefined) {
      return this.contextFilesService.getUrl(this.contextAttribute, remotePath);
    }
  }
}
