/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "mdm-thumbnail",
  templateUrl: "thumbnail.component.html",
})
export class ThumbnailComponent implements OnInit {
  @Input() imgUrl?: any;
  @Input() type: string;
  @Input() size = "2";

  public superType: string;
  public subType: string;

  public ngOnInit() {
    if (this.type != undefined) {
      const parts = this.type.split("/");
      this.superType = parts[0];
      this.subType = parts[1];
    }
  }

  // TODO: support more documents type. render thumbnail for movie files. render thumbnail for more image files.
}
