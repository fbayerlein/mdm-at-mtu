/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Node, NodeArray } from "@navigator/node";
import { Observable, throwError as observableThrowError } from "rxjs";
import { catchError, concatMap, map } from "rxjs/operators";
import { HttpErrorHandler } from "../core/http-error-handler";
import { MDMItem } from "../core/mdm-item";
import { PropertyService } from "../core/property.service";
import { TemplateTest, TemplateTestStep, TemplateTestStepUsage } from "./model/versionable";

@Injectable({
  providedIn: "root",
})
export class EntityCreatorService {
  private _nodeUrl: string;

  constructor(private httpClient: HttpClient, private httpErrorHandler: HttpErrorHandler, private _prop: PropertyService) {
    this._nodeUrl = this._prop.getUrl("mdm/environments");
  }

  /**
   *
   * @param source
   * @returns
   */
  getValidTestTemplates(source: string) {
    const url = this._nodeUrl + "/" + source + "/tpltests";
    return this.httpClient.get<NodeArray>(url).pipe(
      map((res) => res.data.map((d) => Object.assign(new TemplateTest(), d as TemplateTest))),
      map((tplTests) => tplTests.filter((t) => t.isValid())),
      catchError(this.httpErrorHandler.handleError),
    );
  }

  loadValidTestStepTemplates(source: string, testStep: MDMItem) {
    return this.httpClient.get<NodeArray>(`${this._nodeUrl}/${source}/tests/${testStep.id}`).pipe(
      map((res) => res.data[0]),
      map((test) => this.findRelationId(test, "TemplateTest")),
      concatMap((templateTestId) => this.loadUsedTestStepTemplateIdsByTemplateTestId(source, templateTestId)),
      concatMap((templateTestStepIds) => this.loadTemplateTestSteps(source, templateTestStepIds)),
      map((templateTestSteps) => templateTestSteps.filter((t) => t.isValid())),
    );
  }

  loadUsedTestStepTemplateIdsByTemplateTestId(source: string, templateTestId: string) {
    const usagesUrl = `${this._nodeUrl}/${source}/tpltests/${templateTestId}/tplteststepusages`;
    return this.httpClient.get<NodeArray>(usagesUrl).pipe(
      map((res) => res.data.map((d) => Object.assign(new TemplateTestStepUsage(), d) as TemplateTestStepUsage)),
      map((tplUsages) => tplUsages.map((u) => this.findRelationId(u, "TemplateTest"))),
      catchError(this.httpErrorHandler.handleError),
    );
  }

  loadTemplateTestSteps(source: string, templateTestStepIds: string[]) {
    return this.httpClient.get<NodeArray>(`${this._nodeUrl}/${source}/tplteststeps`).pipe(
      map((res) => res.data.map((d) => Object.assign(new TemplateTestStep(), d) as TemplateTestStep)),
      map((res) => res.filter((n) => templateTestStepIds.find((x) => x === n.id))),
      catchError(this.httpErrorHandler.handleError),
    );
  }

  private findRelationId(node: Node, relatedEntityType: string) {
    const relation = node.relations.find((r) => r.entityType === relatedEntityType);
    if (!relation) {
      throw new Error("No relation to entity type " + relatedEntityType + " found at entity " + node);
    }
    if (relation.ids.length == 0) {
      throw new Error("No related entities " + relatedEntityType + " fouund at entity " + node);
    }

    return relation.ids[0];
  }

  getTemplateTestStep(node: Node) {
    const url =
      this._nodeUrl +
      "/" +
      node.sourceName +
      "/tplteststeps/" +
      node.relations.filter((rel) => rel.entityType === "TemplateTestStep")[0].ids[0].toString(); // TODO cannot read properties of undefined (reading 'ids')
    return this.httpClient.get<NodeArray>(url).pipe(
      map((res) => res["data"]),
      catchError(this.httpErrorHandler.handleError),
    );
  }

  getTemplateComponents(entity: Node, contextType: string) {
    const url =
      this._nodeUrl +
      "/" +
      entity.sourceName +
      "/tplroots/" +
      contextType +
      "/" +
      entity.relations.filter((rel) => rel.contextType === contextType)[0].ids[0].toString() +
      "/tplcomps";
    return this.httpClient.get<NodeArray>(url).pipe(
      map((res) => res.data),
      catchError(this.httpErrorHandler.handleError),
    );
  }

  getTemplateAttributtes(sourceName: string, contextType: string, tplRootId: string, tplCompId: string) {
    const url = this._nodeUrl + "/" + sourceName + "/tplroots/" + contextType + "/" + tplRootId + "/tplcomps/" + tplCompId + "/tplattrs";
    return this.httpClient.get<NodeArray>(url).pipe(
      map((res) => res.data),
      catchError(this.httpErrorHandler.handleError),
    );
  }

  /**
   *
   * @param parentMDMItem
   * @param newEntityName
   * @param selectedTemplateItem
   * @returns
   */
  addNewEntity(parentMDMItem: MDMItem, newEntityName: string, selectedTemplateItem: Node): Observable<Node> {
    const url = this.getChildUrl(parentMDMItem);
    const body = JSON.stringify(this.getBody(parentMDMItem, newEntityName, selectedTemplateItem));

    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    return this.httpClient.post<NodeArray>(url, body, options).pipe(
      map((res) => res.data[0]),
      catchError(this.handleError),
    );
  }

  /**
   *
   * @param e
   * @returns
   */
  private handleError(e: Error | any) {
    if (e instanceof HttpErrorResponse) {
      return observableThrowError(e.error);
    }

    if (this.httpErrorHandler !== undefined) {
      return this.httpErrorHandler.handleError(e);
    } else if (e != undefined) {
      return observableThrowError(e.toString());
    } else {
      return observableThrowError("unknown error");
    }
  }

  /**
   *
   * @param parentMDMItem
   * @returns
   */
  private getChildUrl(parentMDMItem: MDMItem) {
    let childType = "";
    switch (parentMDMItem.type) {
      case "Environment":
        childType = "projects";
        break;
      case "Project":
        childType = "pools";
        break;
      case "Pool":
        childType = "tests";
        break;
      case "Test":
        childType = "teststeps";
        break;
      case "TestStep":
        childType = "measurements";
        break;
    }

    return this._nodeUrl + "/" + parentMDMItem.source + "/" + childType;
  }

  /**
   *
   * @param parentMDMItem
   * @param entityName
   * @param selectedTemplateItem
   * @returns
   */
  private getBody(parentMDMItem: MDMItem, entityName: string, selectedTemplateItem: Node) {
    const body = {};
    body["Name"] = entityName;
    switch (parentMDMItem.type) {
      case "Project":
        body["Project"] = parentMDMItem.id;
        break;
      case "Pool":
        body["Pool"] = parentMDMItem.id;
        if (selectedTemplateItem != null) {
          body["TemplateTest"] = selectedTemplateItem.id;
        }
        body["WithTestSteps"] = false;
        break;
      case "Test":
        body["Test"] = parentMDMItem.id;
        if (selectedTemplateItem != null) {
          body["TemplateTestStep"] = selectedTemplateItem.id;
        }
        break;
      case "TestStep":
        body["TestStep"] = parentMDMItem.id;
    }

    return body;
  }
}
