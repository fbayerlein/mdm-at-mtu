/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { ConfirmationService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DialogModule } from "primeng/dialog";
import { DialogService, DynamicDialogModule } from "primeng/dynamicdialog";
import { MDMCoreModule } from "../core/mdm-core.module";
import { CreateEntityComponent } from "../entity-creator/components/create-entity-dialog.component";
import { CreateEntityMenuItemService } from "./menuitem.service";

@NgModule({
  imports: [MDMCoreModule, DialogModule, DynamicDialogModule, ConfirmDialogModule],
  declarations: [CreateEntityComponent],
  exports: [CreateEntityComponent],
  entryComponents: [CreateEntityComponent],
  providers: [CreateEntityMenuItemService, DialogService, ConfirmationService],
})
export class EntityCreatorModule {}
