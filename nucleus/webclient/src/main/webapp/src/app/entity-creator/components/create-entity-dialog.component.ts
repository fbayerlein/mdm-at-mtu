/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { AfterViewInit, Component, OnInit } from "@angular/core";
import { Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService, SelectItem } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { MDMItem } from "../../core/mdm-item";
import { EntityCreatorService } from "../../entity-creator/entity-creator.service";

@Component({
  selector: "mdm-create-entity-dialog",
  templateUrl: "./create-entity-dialog.component.html",
})
export class CreateEntityComponent implements OnInit, AfterViewInit {
  public parentMDMItem: MDMItem;

  public dialogTitle: string;
  public inputPlaceholder: string;
  public inputTooltip: string;

  public displayTemplateItems: boolean;
  public templates: Node[];
  public templateItems: SelectItem[];
  public selectedTemplateItem: Node;
  public templateType: string;

  public validEntityName: boolean;

  constructor(
    private entityCreatorService: EntityCreatorService,
    private translateService: TranslateService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private confirmationService: ConfirmationService,
  ) {}

  ngOnInit() {
    this.parentMDMItem = this.config.data.parentItem.item.state.node;
    this.validEntityName = false;

    if (this.parentMDMItem != undefined) {
      const childType = this.getChildEntityType();
      // title for dialog
      this.dialogTitle = this.translateService.instant("entity-creator.dialog.title", { type: childType });

      // tooltip and placeholder for input
      this.inputPlaceholder = this.translateService.instant("entity-creator.dialog.input-placeholder", { type: childType });
      this.inputTooltip = this.translateService.instant("entity-creator.dialog.input-tooltip", { type: childType });

      // combobox for TemplateTest or TemplateTeststep
      if (this.parentMDMItem.type === "Pool") {
        this.displayTemplateItems = true;
        this.templateType = "TemplateTest";
        const items = this.entityCreatorService.getValidTestTemplates(this.parentMDMItem.source);
        items.subscribe({
          next: (templates) => {
            this.templates = templates;
            this.templateItems = [];
            this.templates.forEach((entity) =>
              this.templateItems.push({
                label: entity.name + " (version: " + entity.attributes.filter((attr) => attr.name === "Version")[0].value + ")",
                value: entity,
              }),
            );
          },
          error: (e) => console.log(e),
        });
      } else if (this.parentMDMItem.type === "Test") {
        this.displayTemplateItems = true;
        this.templateType = "TemplateTestStep";
        const items = this.entityCreatorService.loadValidTestStepTemplates(this.parentMDMItem.source, this.parentMDMItem);
        items.subscribe({
          next: (templates) => {
            this.templates = templates;
            this.templateItems = [];
            this.templates.forEach((entity) =>
              this.templateItems.push({
                label: entity.name + " (version: " + entity.attributes.filter((attr) => attr.name === "Version")[0].value + ")",
                value: entity,
              }),
            );
          },
          error: (e) => console.log(e),
        });
      } else {
        this.displayTemplateItems = false;
      }

      this.selectedTemplateItem = null;
    }
  }

  private getChildEntityType() {
    switch (this.parentMDMItem.type) {
      case "Environment":
        return "Project";
      case "Project":
        return "StructureLevel";
      case "Pool":
        return "Test";
      case "Test":
        return "Teststep";
      case "TestStep":
        return "Measurement";
      default:
        return "";
    }
  }

  ngAfterViewInit() {
    if (this.parentMDMItem != undefined) {
      setTimeout(() => {
        this.config.header = this.dialogTitle;
      });
    }
  }

  onSelectTemplateItem(e: any) {
    this.selectedTemplateItem = e.value;
  }

  closeDialog() {
    this.ref.close();
  }

  createChildEntity(newChildEntityName: string) {
    this.entityCreatorService.addNewEntity(this.parentMDMItem, newChildEntityName, this.selectedTemplateItem).subscribe(
      (r) => this.ref.close(true),
      (e) => this.handleError(e, newChildEntityName),
    );

    this.closeDialog();
  }

  /**
   *
   * @param e
   * @param newChildEntityName
   */
  private handleError(e: Error | any, newChildEntityName: string) {
    const title = this.translateService.instant("entity-creator.error-dialog.title");
    const errorMessage = this.translateService.instant("entity-creator.error-dialog.content", { entityName: newChildEntityName });
    this.confirmationService.confirm({
      header: title,
      message: errorMessage,
      acceptLabel: "OK",
      rejectVisible: false,
    });
  }

  /**
   * Check on invalid child entity name for new child entity
   *
   * @param childEntityName
   */
  isValidChildEntityName(childEntityName: string) {
    this.validEntityName = false;
    if (childEntityName != undefined && childEntityName.length > 1) {
      this.validEntityName = true;
    }
  }
}
