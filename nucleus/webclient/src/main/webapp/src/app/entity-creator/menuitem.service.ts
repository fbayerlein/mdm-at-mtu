/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { MdmNavigatorContextMenuService } from "@navigator/mdm-navigator-context-menu.service";
import { TranslateService } from "@ngx-translate/core";
import { DialogService } from "primeng/dynamicdialog";
import { BehaviorSubject } from "rxjs";
import { MDMItem } from "../core/mdm-item";
import { CreateEntityComponent } from "./components/create-entity-dialog.component";

@Injectable({
  providedIn: "root",
})
export class CreateEntityMenuItemService {
  public needRefresh: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
    private menuService: MdmNavigatorContextMenuService,
  ) {
    const projectLabel = this.translateService.instant("entity-creator.menuitems.create-new", { type: "Project" });

    this.menuService.register({
      menuItem: {
        id: "craete-project",
        label: projectLabel,
        icon: "fa icon project",
        command: (node) => this.openCreateChildEntityDialog(node),
      },
      predicate: (node) => node.type === "Environment",
    });
    this.menuService.register({
      menuItem: {
        id: "craete-pool",
        label: this.translateService.instant("entity-creator.menuitems.create-new", { type: "SubProject" }),
        icon: "fa icon pool",
        command: (node) => this.openCreateChildEntityDialog(node),
      },
      predicate: (node) => node.type === "Project",
    });

    this.menuService.register({
      menuItem: {
        id: "craete-test",
        label: this.translateService.instant("entity-creator.menuitems.create-new", { type: "Test" }),
        icon: "fa icon test",
        command: (node) => this.openCreateChildEntityDialog(node),
      },
      predicate: (node) => node.type === "Pool",
    });

    this.menuService.register({
      menuItem: {
        id: "craete-teststep",
        label: this.translateService.instant("entity-creator.menuitems.create-new", { type: "TestStep" }),
        icon: "fa icon teststep",
        command: (node) => this.openCreateChildEntityDialog(node),
      },
      predicate: (node) => node.type === "Test",
    });

    this.menuService.register({
      menuItem: {
        id: "craete-measurement",
        label: this.translateService.instant("entity-creator.menuitems.create-new", { type: "Measurement" }),
        icon: "fa icon measurement",
        command: (node) => this.openCreateChildEntityDialog(node),
      },
      predicate: (node) => node.type === "TestStep",
    });
  }

  /**
   * This menuitem-event-methode called by any selected node.
   * Create child entity should be possible by only one selected node
   *
   * @param parentMDMItem
   */
  openCreateChildEntityDialog(parentMDMItem: MDMItem) {
    const ref = this.dialogService.open(CreateEntityComponent, {
      data: {
        parentItem: parentMDMItem,
      },
      header: "",
      width: "30%",
    });

    ref.onClose.subscribe((successfullyCreated: boolean) => {
      this.needRefresh.next(successfullyCreated);
    });
  }
}
