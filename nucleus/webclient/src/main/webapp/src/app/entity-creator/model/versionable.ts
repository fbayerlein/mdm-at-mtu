/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Node } from "@navigator/node";

export class Versionable extends Node {
  getVersion(): string {
    return this.attributes.filter((attr) => attr.name === "Version")[0].value as string;
  }
  getState(): "EDITABLE" | "VALID" | "ARCHIVED" {
    const state = this.attributes.filter((attr) => attr.name === "ValidFlag")[0].value as string;
    if (state === "EDITABLE" || state === "VALID" || state === "ARCHIVED") {
      return state;
    } else {
      throw new Error("Invalid value '" + state + "' of Attribute ValidFlag found for " + this.type + " with ID " + this.id + ".");
    }
  }

  isValid() {
    return this.getState() === "VALID";
  }
}

export class TemplateTest extends Versionable {}

export class TemplateTestStep extends Versionable {}

export class TemplateTestStepUsage extends Versionable {}
