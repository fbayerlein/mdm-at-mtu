import { Component, Input, OnChanges, SimpleChanges, ViewChild } from "@angular/core";
import { BasketService } from "@basket/basket.service";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { tap } from "rxjs";
import { Node } from "src/app/navigator/node";
import { Condition, Operator, SearchFilter } from "src/app/search/filter.service";
import { SearchAttribute, SearchService } from "src/app/search/search.service";
import { QueryService, Row, SearchResult } from "src/app/tableview/query.service";
import { TableviewComponent } from "src/app/tableview/tableview.component";
import { View } from "src/app/tableview/tableview.service";

@Component({
  selector: "mdm-view-table",
  templateUrl: "./view-table.component.html",
  styleUrls: ["./view-table.component.css"],
})
export class ViewTableComponent implements OnChanges {
  @Input() type: "CreationDate" | "ModifiedDate";
  @Input() environments: Node[];
  @Input() allSearchAttributes: {
    [type: string]: { [env: string]: SearchAttribute[] };
  } = {};

  @ViewChild(TableviewComponent)
  tableViewComponent: TableviewComponent;
  lgQuickPlotModal = false;
  public selectedItems: Row[];
  results: SearchResult = new SearchResult();
  selectedView: View;
  contextMenuItems: MenuItem[] = [
    {
      label: "Add to shopping basket",
      icon: "fa fa-shopping-cart",
      command: () => this.addSelectionToBasket(),
    },
  ];
  loading = false;
  maxResults = 1000;

  constructor(
    private basketService: BasketService,
    private queryService: QueryService,
    private searchService: SearchService,
    private translateService: TranslateService,
    private notificationService: MDMNotificationService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      switch (propName) {
        case "environments": {
          this.load();
        }
      }
    }
  }

  onViewChanged(view: View) {
    this.selectedView = structuredClone(view);
    this.load();
  }

  selected2Basket(e: Event) {
    e.stopPropagation();
    this.basketService.addMany(this.tableViewComponent.selectedViewRows.map((selectedRow: Row) => Row.getItem(selectedRow)));
  }

  quickPlotMetadataTrends(e: Event) {
    this.selectedItems = [];
    if (this.tableViewComponent.selectedViewRows) {
      this.selectedItems = this.results.rows.filter((row: Row) => {
        return (
          this.tableViewComponent.selectedViewRows.findIndex((selectedRow: Row) => {
            return row.id === selectedRow.id;
          }) > -1
        );
      });
    }

    if (this.selectedItems.length > 0) {
      this.lgQuickPlotModal = true;
    }

    e.stopPropagation();
  }

  addSelectionToBasket() {
    this.basketService.add(Row.getItem(this.tableViewComponent.menuSelectedRow));
  }

  load(): void {
    if (!this.selectedView) return;
    this.loading = true;

    const searchDate: Date = new Date();
    searchDate.setDate(searchDate.getDate() - 1);
    const conditions: Condition[] = [
      new Condition("Measurement", this.type, Operator.GREATER_THAN, [searchDate.toISOString().split(".")[0]]),
    ];
    if (this.type === "ModifiedDate") {
      conditions.push(new Condition("Measurement", "CreationDate", Operator.GREATER_THAN, [searchDate.toISOString().split(".")[0]]));
    }
    const currentFilter: SearchFilter = new SearchFilter(
      "Newly created",
      this.environments.map((env) => env.sourceName),
      "Measurement",
      "",
      conditions,
    );
    const query = this.searchService.convertToQuery(currentFilter, this.allSearchAttributes, this.selectedView, true, false);
    this.queryService
      .query(query)
      .pipe(tap((result) => this.generateWarningsIfMaxResultsReached(result)))
      .subscribe({
        next: (r) => {
          this.results = r;
          this.loading = false;
        },
        error: (e) => {
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-process-search-query"), e);
          this.loading = false;
        },
      });
  }

  generateWarningsIfMaxResultsReached(result: SearchResult) {
    const resultsPerSource = result.rows
      .map((r) => r.source)
      .reduce((prev, item) => {
        prev[item] ? (prev[item] += 1) : (prev[item] = 1);
        return prev;
      }, {});

    Object.keys(resultsPerSource)
      .filter((source) => resultsPerSource[source] > this.maxResults)
      .forEach((source) =>
        this.notificationService.notifyWarn(
          this.translateService.instant("search.mdm-search.errheading-too-many-search-results"),
          this.translateService.instant("search.mdm-search.err-too-many-search-results", { numresults: this.maxResults, source: source }),
        ),
      );
  }
}
