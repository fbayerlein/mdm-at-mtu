import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { mergeMap, Subscription } from "rxjs";
import { Node } from "src/app/navigator/node";
import { SearchAttribute, SearchService } from "src/app/search/search.service";
import { ViewTableComponent } from "../view-table/view-table.component";

@Component({
  selector: "mdm-last-modified",
  templateUrl: "./last-modified.component.html",
  styleUrls: ["./last-modified.component.css"],
})
export class LastModifiedComponent implements OnInit, OnDestroy {
  @ViewChild(ViewTableComponent) viewTable: ViewTableComponent;
  environments: Node[];

  allSearchAttributes: {
    [type: string]: { [env: string]: SearchAttribute[] };
  } = {};
  nodeServiceSubscription: Subscription;

  constructor(
    private translateService: TranslateService,
    private nodeService: NodeService,
    private notificationService: MDMNotificationService,
    private searchService: SearchService,
  ) {}

  ngOnInit(): void {
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe(() => this.init());
    this.init();
  }

  ngOnDestroy(): void {
    if (this.nodeServiceSubscription) {
      this.nodeServiceSubscription.unsubscribe;
    }
  }

  init(): void {
    this.nodeService
      .getNodes()
      .pipe(
        mergeMap((envs) => {
          this.environments = [...envs];
          return this.searchService.loadSearchAttributesStructured(envs.map((env) => env.sourceName));
        }),
      )
      .subscribe({
        next: (attrs) => {
          this.allSearchAttributes = { ...attrs };
        },
        error: (error) =>
          this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-load-data-sources"), error),
      });
  }
}
