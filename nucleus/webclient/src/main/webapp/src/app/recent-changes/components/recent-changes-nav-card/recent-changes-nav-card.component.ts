/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component } from "@angular/core";
import { streamTranslate, TRANSLATE } from "@core/mdm-core.module";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "mdm-recent-changes-nav-card",
  templateUrl: "./recent-changes-nav-card.component.html",
  styleUrls: ["./recent-changes-nav-card.component.css"],
})
export class RecentChangesNavCardComponent {
  links = [];

  constructor(private translateService: TranslateService) {
    streamTranslate(this.translateService, [
      TRANSLATE("recent-changes.links.newly-created"),
      TRANSLATE("recent-changes.links.last-modified"),
    ]).subscribe((msg: string) => {
      this.links = [
        { label: msg["recent-changes.links.newly-created"], routerLink: "newly-created" },
        { label: msg["recent-changes.links.last-modified"], routerLink: "last-modified" },
      ];
    });
  }
}
