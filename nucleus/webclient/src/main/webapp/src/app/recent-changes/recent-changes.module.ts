import { NgModule } from "@angular/core";
import { MDMCoreModule } from "@core/mdm-core.module";
import { MDMDetailModule } from "@details/mdm-detail.module";
import { PanelModule } from "primeng/panel";
import { TabMenuModule } from "primeng/tabmenu";
import { TableViewModule } from "../tableview/tableview.module";
import { LastModifiedComponent } from "./components/last-modified/last-modified.component";
import { NewlyCreatedComponent } from "./components/newly-created/newly-created.component";
import { RecentChangesNavCardComponent } from "./components/recent-changes-nav-card/recent-changes-nav-card.component";
import { ViewTableComponent } from "./components/view-table/view-table.component";
import { RecentChangesRoutingModule } from "./recent-changes.routing.module";

@NgModule({
  imports: [RecentChangesRoutingModule, MDMCoreModule, TabMenuModule, PanelModule, TableViewModule, MDMDetailModule],
  declarations: [RecentChangesNavCardComponent, NewlyCreatedComponent, LastModifiedComponent, ViewTableComponent],
  exports: [RecentChangesNavCardComponent],
  providers: [],
})
export class RecentChangesModule {}
