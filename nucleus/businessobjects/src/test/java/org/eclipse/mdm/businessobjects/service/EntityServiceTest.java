/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.mdm.api.base.model.ContextRoot;
import org.junit.Test;

import io.vavr.collection.List;

public class EntityServiceTest {

	@Test
	public void testSatisfiesParameters() throws Exception {
		EntityService entityService = new EntityService();

		Class<?>[] parameter = new Class[] { java.util.List.class, ContextRoot.class };
		List<Class<?>> required = List.of(ArrayList.class, ContextRoot.class);

		assertThat(entityService.satisfiesParameters(parameter, required)).isTrue();
	}

	@Test
	public void testSatisfiesParametersNotSubClassSuperClass() throws Exception {
		EntityService entityService = new EntityService();

		Class<?>[] parameter = new Class[] { ArrayList.class, ContextRoot.class };
		List<Class<?>> required = List.of(java.util.List.class, ContextRoot.class);

		assertThat(entityService.satisfiesParameters(parameter, required)).isFalse();
	}

	@Test
	public void testSatisfiesParametersNot() throws Exception {
		EntityService entityService = new EntityService();

		Class<?>[] parameter = new Class[] { HashMap.class, ContextRoot.class };
		List<Class<? extends Object>> required = List.of(java.util.List.class, ContextRoot.class);

		assertThat(entityService.satisfiesParameters(parameter, required)).isFalse();
	}

	@Test
	public void testSatisfiesParametersAutoBoxing() throws Exception {
		EntityService entityService = new EntityService();

		Class<?>[] parameter = new Class[] { boolean.class, ContextRoot.class };
		List<Class<?>> required = List.of(Boolean.class, ContextRoot.class);

		assertThat(entityService.satisfiesParameters(parameter, required)).isTrue();
	}

	@Test
	public void testSatisfiesParametersAutoUnboxing() throws Exception {
		EntityService entityService = new EntityService();

		Class<?>[] parameter = new Class[] { Boolean.class, ContextRoot.class };
		List<Class<?>> required = List.of(boolean.class, ContextRoot.class);

		assertThat(entityService.satisfiesParameters(parameter, required)).isTrue();
	}

	@Test
	public void testSatisfiesParametersNotLength() throws Exception {
		EntityService entityService = new EntityService();

		Class<?>[] parameter = new Class[] { Boolean.class, ContextRoot.class, boolean.class };
		List<Class<?>> required = List.of(boolean.class, ContextRoot.class);

		assertThat(entityService.satisfiesParameters(parameter, required)).isFalse();
	}

}
