/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.boundary.integrationtest;

import org.eclipse.mdm.businessobjects.boundary.ResourceConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Test class for {@TestResource}.
 * 
 * @author Johannes Stamm, Peak Solution GmbH Nuernberg
 * @see EntityResourceIntegrationTest
 *
 */
public class TestWithTemplateResourceIntegrationTest extends EntityResourceIntegrationTest {
	@BeforeClass
	public static void prepareTestData() {
		getLogger().debug("Preparing TestResourceIntegrationTest");

		// prepare test data for creating the parent Pool
		PoolResourceIntegrationTest.prepareTestData();
		PoolResourceIntegrationTest.createEntity();
		// prepare test data for creating the linked TemplateTest
		TemplateTestResourceIntegrationTest.prepareTestData();
		TemplateTestResourceIntegrationTest.createEntity();

		// set up test data
		setContextClass(TestWithTemplateResourceIntegrationTest.class);

		putTestDataValue(TESTDATA_RESOURCE_URI, "/tests");
		putTestDataValue(TESTDATA_ENTITY_NAME, "testTest");
		putTestDataValue(TESTDATA_ENTITY_TYPE, "Test");

		JsonObject json = new JsonObject();
		json.add(ResourceConstants.ENTITYATTRIBUTE_NAME, new JsonPrimitive(getTestDataValue(TESTDATA_ENTITY_NAME)));
		json.add("Pool", new JsonPrimitive(getTestDataValue(PoolResourceIntegrationTest.class, TESTDATA_ENTITY_ID)));
		json.add("TemplateTest",
				new JsonPrimitive(getTestDataValue(TemplateTestResourceIntegrationTest.class, TESTDATA_ENTITY_ID)));

		putTestDataValue(TESTDATA_CREATE_JSON_BODY, json.toString());
	}

	@AfterClass
	public static void tearDownAfterClass() {
		// call tearDownAfterClass() of parent entity
		setContextClass(PoolResourceIntegrationTest.class);
		PoolResourceIntegrationTest.tearDownAfterClass();

		// delete Template
		setContextClass(TemplateTestResourceIntegrationTest.class);
		TemplateTestResourceIntegrationTest.deleteEntity();
	}
}
