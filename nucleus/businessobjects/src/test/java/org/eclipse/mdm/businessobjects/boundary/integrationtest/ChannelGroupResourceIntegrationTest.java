/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.boundary.integrationtest;

import org.eclipse.mdm.businessobjects.boundary.ResourceConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Test class for {@ChannelGroupResource}.
 * 
 * @author Johannes Stamm, Peak Solution GmbH Nuernberg
 * @see EntityResourceIntegrationTest
 *
 */
public class ChannelGroupResourceIntegrationTest extends EntityResourceIntegrationTest {
	@BeforeClass
	public static void prepareTestData() {
		getLogger().debug("Preparing ChannelGroupResourceIntegrationTest");

		// prepare test data for creating the parent Measurement
		MeasurementResourceIntegrationTest.prepareTestData();
		MeasurementResourceIntegrationTest.createEntity();

		// set up test data
		setContextClass(ChannelGroupResourceIntegrationTest.class);

		putTestDataValue(TESTDATA_RESOURCE_URI, "/channelgroups");
		putTestDataValue(TESTDATA_ENTITY_NAME, "testChannelGroup");
		putTestDataValue(TESTDATA_ENTITY_TYPE, "ChannelGroup");
		putTestDataValue(TESTDATA_NUMBER_OF_VALUES, "42");

		// json object for teststep entity
		JsonObject json = new JsonObject();
		json.add(ResourceConstants.ENTITYATTRIBUTE_NAME, new JsonPrimitive(getTestDataValue(TESTDATA_ENTITY_NAME)));
		json.add(ResourceConstants.ENTITYATTRIBUTE_NUMBER_OF_VALUES,
				new JsonPrimitive(getTestDataValue(TESTDATA_NUMBER_OF_VALUES)));
		json.add("Measurement",
				new JsonPrimitive(getTestDataValue(MeasurementResourceIntegrationTest.class, TESTDATA_ENTITY_ID)));

		putTestDataValue(TESTDATA_CREATE_JSON_BODY, json.toString());
	}

	@AfterClass
	public static void tearDownAfterClass() {
		// call tearDownAfterClass() of parent entity
		setContextClass(MeasurementResourceIntegrationTest.class);
		MeasurementResourceIntegrationTest.tearDownAfterClass();
	}
}
