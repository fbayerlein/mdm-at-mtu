/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.boundary.integrationtest;

import org.eclipse.mdm.businessobjects.boundary.ResourceConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class TestStepContextResourceIntegrationTest extends ContextDescribableResourceIntegrationTest {

	@BeforeClass
	public static void prepareTestData() {
		getLogger().debug("Preparing TestStepContextResourceIntegrationTest");
		TemplateAttributeUUTResourceIntegrationTest.prepareTestData();
		TemplateAttributeUUTResourceIntegrationTest.createEntity();
		String attrName = getTestDataValue(TESTDATA_ENTITY_NAME);
		String compName = getTestDataValue(TemplateComponentUUTResourceIntegrationTest.class, TESTDATA_ENTITY_NAME);

		// prepare test data for creating the linked tempaltes
		TestStepWithTemplateResourceIntegrationTest.prepareTestData();
		TestStepWithTemplateResourceIntegrationTest.createEntity();
		// get id from teststep (contextclass
		// TestStepWithTemplateResourceIntegrationTest)
		String id = getTestDataValue(TESTDATA_ENTITY_ID);

		// create measurement for this teststep
		setContextClass(MeasurementResourceIntegrationTest.class);
		putTestDataValue(TESTDATA_RESOURCE_URI, "/measurements");
		putTestDataValue(TESTDATA_ENTITY_NAME, "testMeasurement");
		putTestDataValue(TESTDATA_ENTITY_TYPE, "Measurement");

		JsonObject json1 = new JsonObject();
		json1.add(ResourceConstants.ENTITYATTRIBUTE_NAME, new JsonPrimitive(getTestDataValue(TESTDATA_ENTITY_NAME)));
		json1.add("TestStep", new JsonPrimitive(id));

		putTestDataValue(TESTDATA_CREATE_JSON_BODY, json1.toString());
		MeasurementResourceIntegrationTest.createEntity();

		// set up test data
		setContextClass(TestStepContextResourceIntegrationTest.class);

		putTestDataValue(TESTDATA_ENTITY_ID, id);
		putTestDataValue(TESTDATA_CONTEXT_GROUP, "ordered");
		putTestDataValue(TESTDATA_RESOURCE_URI, "/teststeps");
		putTestDataValue(TESTDATA_ENTITY_NAME, "testTestStep");
		putTestDataValue(TESTDATA_ENTITY_TYPE, "TestStep");

		JsonObject json = createContextRequest(compName, attrName);
		putTestDataValue(TESTDATA_UPDATE_JSON_BODY, json.toString());
	}

	@AfterClass
	public static void tearDownAfterClass() {
		// call tearDownAfterClass() of parent entity (without template)
		setContextClass(TestStepWithTemplateResourceIntegrationTest.class);
		TestStepWithTemplateResourceIntegrationTest.deleteEntity();
		TestStepWithTemplateResourceIntegrationTest.tearDownAfterClass();

		setContextClass(TemplateAttributeUUTResourceIntegrationTest.class);
		TemplateAttributeUUTResourceIntegrationTest.tearDownAfterClass();

	}
}
