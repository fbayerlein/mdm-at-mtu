/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.protobuf.Mdm;
import org.junit.Before;
import org.junit.Test;

public class ProtobufConverterTest {

	ChannelGroup channelGroup = mock(ChannelGroup.class);
	Channel channel1 = mock(Channel.class);
	Channel channel2 = mock(Channel.class);
	Unit unit1 = mock(Unit.class);
	Unit unit2 = mock(Unit.class);
	EntityManager em = mock(EntityManager.class);
	ApplicationContext context = mock(ApplicationContext.class);

	@Before
	public void init() {
		when(channelGroup.getID()).thenReturn("1");
		when(unit1.getID()).thenReturn("1");
		when(unit2.getID()).thenReturn("2");
		when(channel1.getID()).thenReturn("1");
		when(channel1.getUnit()).thenReturn(unit1);
		when(channel2.getID()).thenReturn("2");
		when(channel2.getUnit()).thenReturn(unit2);

		when(em.load(eq(ChannelGroup.class), anyString())).thenReturn(channelGroup);
		when(em.load(eq(Channel.class), anyList())).thenReturn(Arrays.asList(channel1, channel2));
		when(em.load(eq(Unit.class), anyList())).thenReturn(Arrays.asList(unit1, unit2));

		when(context.getEntityManager()).thenReturn(Optional.of(em));
	}

	@Test
	public void testRequestChannelWithDefaultUnit() {
		ReadRequest readRequest = ProtobufConverter.convert(context,
				Mdm.ReadRequest.newBuilder().setChannelGroupId("1").addChannelIds("1").build());

		Map<Channel, Unit> channels = new HashMap<>();
		channels.put(channel1, unit1);
		assertThat(readRequest).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channels", channels)
				.hasFieldOrPropertyWithValue("loadAllChannels", false);
	}

	@Test
	public void testRequestChannelWithExplicitUnit() {
		ReadRequest channelWithUnit = ProtobufConverter.convert(context,
				Mdm.ReadRequest.newBuilder().setChannelGroupId("1").addChannelIds("1").addUnitIds("1").build());

		Map<Channel, Unit> channels = new HashMap<>();
		channels.put(channel1, unit1);
		assertThat(channelWithUnit).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channels", channels)
				.hasFieldOrPropertyWithValue("loadAllChannels", false);
	}

	@Test
	public void testRequestAllChannelsWithDefaultUnits() {
		ReadRequest allChannelWithoutUnits = ProtobufConverter.convert(context,
				Mdm.ReadRequest.newBuilder().setChannelGroupId("1").build());

		assertThat(allChannelWithoutUnits).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channels", Collections.emptyMap())
				.hasFieldOrPropertyWithValue("loadAllChannels", true);
	}

	@Test
	public void testRequestAllChannelsWithExplicitUnits() {
		ReadRequest allChannelWithUnits = ProtobufConverter.convert(context, Mdm.ReadRequest.newBuilder()
				.setChannelGroupId("1").addChannelIds("1").addChannelIds("2").addUnitIds("2").addUnitIds("1").build());

		Map<Channel, Unit> channels = new HashMap<>();
		channels.put(channel1, unit2);
		channels.put(channel2, unit1);
		assertThat(allChannelWithUnits).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channels", channels)
				.hasFieldOrPropertyWithValue("loadAllChannels", false);
	}

	@Test
	public void testSuperfluousUnitsAreIgnored() {
		ReadRequest allChannelWithUnits = ProtobufConverter.convert(context, Mdm.ReadRequest.newBuilder()
				.setChannelGroupId("1").addChannelIds("1").addUnitIds("2").addUnitIds("1").build());

		Map<Channel, Unit> channels = new HashMap<>();
		channels.put(channel1, unit2);
		assertThat(allChannelWithUnits).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channels", channels)
				.hasFieldOrPropertyWithValue("loadAllChannels", false);
	}

	@Test
	public void testLessUnitsThenChannels() {
		ReadRequest allChannelWithUnits = ProtobufConverter.convert(context, Mdm.ReadRequest.newBuilder()
				.setChannelGroupId("1").addChannelIds("1").addChannelIds("2").addUnitIds("2").build());

		Map<Channel, Unit> channels = new HashMap<>();
		channels.put(channel1, unit2);
		channels.put(channel2, unit2); // no unitId was supplied -> default unit
		assertThat(allChannelWithUnits).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channels", channels)
				.hasFieldOrPropertyWithValue("loadAllChannels", false);
	}

	@Test
	public void testEmptyStringAsUnitIdIsInterpretedAsDefaultUnit() {
		ReadRequest allChannelWithUnits = ProtobufConverter.convert(context, Mdm.ReadRequest.newBuilder()
				.setChannelGroupId("1").addChannelIds("1").addChannelIds("2").addUnitIds("").addUnitIds("1").build());

		Map<Channel, Unit> channels = new HashMap<>();
		channels.put(channel1, unit1); // empty unitId was supplied -> default unit
		channels.put(channel2, unit1);
		assertThat(allChannelWithUnits).hasFieldOrPropertyWithValue("channelGroup", channelGroup)
				.hasFieldOrPropertyWithValue("channels", channels)
				.hasFieldOrPropertyWithValue("loadAllChannels", false);
	}

}
