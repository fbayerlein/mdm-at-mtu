package org.eclipse.mdm.businessobjects.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.NewCookie;

import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ProjectResourceTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	@org.junit.jupiter.api.Test
	public void testCreateProject(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot();

		MDMEntityResponse projects = target.path("mdm/environments/MDM/projects").request().cookie(sessionCookie)
				.get(MDMEntityResponse.class);
		assertThat(projects.getType()).isEqualTo("Project");
		assertThat(projects.getData()).hasSize(0);

//		MDMEntityResponse project = target.path("mdm/environments/MDM/projects").request().post(
//				Entity.entity("{ \"Name\": \"MyNewProject\" }", MediaType.APPLICATION_JSON), MDMEntityResponse.class);
//
//		assertThat(project.getType()).isEqualTo("Project");
//		assertThat(project.getData()).hasSize(1);
	}

}
