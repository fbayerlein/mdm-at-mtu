/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.boundary.integrationtest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.restassured.http.ContentType;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;

/*
* @author Johannes Stamm, Peak Solution GmbH Nuernberg
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContextDescribableResourceIntegrationTest extends ResourceIntegrationTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContextDescribableResourceIntegrationTest.class);

	@Test
	public void test1FindContext() {
		// only execute if not skipped by implementing test class
		Assume.assumeFalse(testsToSkip.get(getContextClass()).get().contains(TestType.FIND));

		String uri = getTestDataValue(TESTDATA_RESOURCE_URI) + "/" + getTestDataValue(TESTDATA_ENTITY_ID) + "/contexts";

		LOGGER.debug(getContextClass().getSimpleName() + ".find() sending GET to " + uri);

		ExtractableResponse<Response> response = given().get(uri).then().log().ifError().contentType(ContentType.JSON)
				.body("data.first().ordered.UNITUNDERTEST.first().attributes",
						hasItem(allOf(hasEntry("name", "Name"),
								hasEntry("value",
										getTestDataValue(TemplateComponentUUTResourceIntegrationTest.class,
												TESTDATA_ENTITY_NAME)))))
				.body("data.first().ordered.UNITUNDERTEST.first().attributes",
						hasItem(allOf(hasEntry("name",
								getTestDataValue(TemplateAttributeUUTResourceIntegrationTest.class,
										TESTDATA_ENTITY_NAME)),
								hasEntry("value", ""))))
				.body("data.first().ordered.TESTSEQUENCE", equalTo(new ArrayList<>()))
				.body("data.first().ordered.TESTEQUIPMENT", equalTo(new ArrayList<>()))
				.body("data.first().measured", equalTo(new HashMap<>())).extract();

		LOGGER.debug(getContextClass().getSimpleName() + " found " + response.asString());
	}

	@Test
	public void test2FindContextTEQ() {
		// only execute if not skipped by implementing test class
		Assume.assumeFalse(testsToSkip.get(getContextClass()).get().contains(TestType.FIND));

		String uri = getTestDataValue(TESTDATA_RESOURCE_URI) + "/" + getTestDataValue(TESTDATA_ENTITY_ID)
				+ "/contexts/testequipment";

		LOGGER.debug(getContextClass().getSimpleName() + ".find() sending GET to " + uri);

		Map<String, List<Object>> excpectedAnswer = new HashMap<>();
		excpectedAnswer.put("TESTEQUIPMENT", new ArrayList<>());

		ExtractableResponse<Response> response = given().get(uri).then().log().ifError().contentType(ContentType.JSON)
				.body("data.first().ordered", equalTo(excpectedAnswer))
				.body("data.first().measured", equalTo(new HashMap<>())).extract();

		LOGGER.debug(getContextClass().getSimpleName() + " found " + response.asString());
	}

	@Test
	public void test3FindContextTSQ() {
		// only execute if not skipped by implementing test class
		Assume.assumeFalse(testsToSkip.get(getContextClass()).get().contains(TestType.FIND));

		String uri = getTestDataValue(TESTDATA_RESOURCE_URI) + "/" + getTestDataValue(TESTDATA_ENTITY_ID)
				+ "/contexts/testsequence";

		LOGGER.debug(getContextClass().getSimpleName() + ".find() sending GET to " + uri);

		Map<String, List<Object>> excpectedAnswer = new HashMap<>();
		excpectedAnswer.put("TESTSEQUENCE", new ArrayList<>());

		ExtractableResponse<Response> response = given().get(uri).then().log().ifError().contentType(ContentType.JSON)
				.body("data.first().ordered", equalTo(excpectedAnswer))
				.body("data.first().measured", equalTo(new HashMap<>())).extract();

		LOGGER.debug(getContextClass().getSimpleName() + " found " + response.asString());
	}

	@Test
	public void test4FindContextUUT() {
		// only execute if not skipped by implementing test class
		Assume.assumeFalse(testsToSkip.get(getContextClass()).get().contains(TestType.FIND));

		String uri = getTestDataValue(TESTDATA_RESOURCE_URI) + "/" + getTestDataValue(TESTDATA_ENTITY_ID)
				+ "/contexts/unitundertest";

		LOGGER.debug(getContextClass().getSimpleName() + ".find() sending GET to " + uri);

		ExtractableResponse<Response> response = given().get(uri).then().log().ifError().contentType(ContentType.JSON)
				.body("data.first().ordered.UNITUNDERTEST", notNullValue())
				.body("data.first().ordered.TESTEQUIPMENT", nullValue())
				.body("data.first().ordered.TESTSEQUENCE", nullValue())
				.body("data.first().measured", equalTo(new HashMap<>())).extract();

		LOGGER.debug(getContextClass().getSimpleName() + " found " + response.asString());
	}

	@Test
	public void test5UpdateContext() {
		// only execute if not skipped by implementing test class
		Assume.assumeFalse(testsToSkip.get(getContextClass()).get().contains(TestType.UPDATE_CONTEXT));

		String uri = getTestDataValue(TESTDATA_RESOURCE_URI) + "/" + getTestDataValue(TESTDATA_ENTITY_ID) + "/contexts";

		LOGGER.debug(getContextClass().getSimpleName() + ".update() sending PUT to " + uri + " with: "
				+ getTestDataValue(TESTDATA_UPDATE_JSON_BODY));

		ExtractableResponse<Response> response = given().contentType(ContentType.JSON)
				.body(getTestDataValue(TESTDATA_UPDATE_JSON_BODY)).put(uri).then().log().ifError()
				.contentType(ContentType.JSON)
				.body("data.first().ordered.UNITUNDERTEST.first().attributes",
						hasItem(allOf(hasEntry("name", "Name"),
								hasEntry("value",
										getTestDataValue(TemplateComponentUUTResourceIntegrationTest.class,
												TESTDATA_ENTITY_NAME)))))
				.body("data.first().ordered.UNITUNDERTEST.first().attributes",
						hasItem(allOf(hasEntry("name",
								getTestDataValue(TemplateAttributeUUTResourceIntegrationTest.class,
										TESTDATA_ENTITY_NAME)),
								hasEntry("value", "updated"))))
				.body("data.first().measured.UNITUNDERTEST.first().attributes",
						hasItem(allOf(hasEntry("name", "Name"),
								hasEntry("value",
										getTestDataValue(TemplateComponentUUTResourceIntegrationTest.class,
												TESTDATA_ENTITY_NAME)))))
				.body("data.first().measured.UNITUNDERTEST.first().attributes", hasItem(allOf(hasEntry("name",
						getTestDataValue(TemplateAttributeUUTResourceIntegrationTest.class, TESTDATA_ENTITY_NAME)),
						hasEntry("value", "updated"))))
				.extract();

		LOGGER.debug(getContextClass().getSimpleName() + " updated " + response.asString());
	}

	public static JsonObject createContextRequest(String compName, String attrName) {
		String req = "{\r\n" + "  \"data\": [{\r\n" + "    \"measured\": {\r\n" + "      \"UNITUNDERTEST\": [{\r\n"
				+ "        \"attributes\": [{\r\n" + "          \"name\": \"Name\",\r\n" + "          \"value\": \""
				+ compName + "\"\r\n" + "        },{\r\n" + "          \"name\": \"" + attrName + "\",\r\n"
				+ "          \"value\": \"updated\"\r\n" + "        }]\r\n" + "      }]\r\n" + "    },\r\n"
				+ "    \"ordered\": {\r\n" + "      \"UNITUNDERTEST\": [{\r\n" + "        \"attributes\": [{\r\n"
				+ "          \"name\": \"" + attrName + "\",\r\n" + "          \"value\": \"updated\"\r\n"
				+ "        },{\r\n" + "          \"name\": \"Name\",\r\n" + "          \"value\": \"" + compName
				+ "\"\r\n" + "        }]\r\n" + "      }]\r\n" + "    }\r\n" + "  }]\r\n" + "}";

		return new JsonParser().parse(req).getAsJsonObject();
	}

	/**
	 * Gets the logger
	 * 
	 * @return logger configured to current context class
	 */
	public static Logger getLogger() {
		return LOGGER;
	}
}
