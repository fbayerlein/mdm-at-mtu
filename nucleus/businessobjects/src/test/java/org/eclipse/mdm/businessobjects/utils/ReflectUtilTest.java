/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class ReflectUtilTest {

	@Test
	public void testApplyValueBoolean() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Boolean.class, boolean.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(boolean.class, Boolean.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(boolean.class, boolean.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Boolean.class, Boolean.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Boolean.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Boolean.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(boolean.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, boolean.class)).isFalse();
	}

	@Test
	public void testApplyValueByte() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Byte.class, byte.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(byte.class, Byte.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(byte.class, byte.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Byte.class, Byte.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Byte.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Byte.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(byte.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, byte.class)).isFalse();
	}

	@Test
	public void testApplyValueShort() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Short.class, short.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(short.class, Short.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(short.class, short.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Short.class, Short.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Short.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Short.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(short.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, short.class)).isFalse();
	}

	@Test
	public void testApplyValueInt() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Integer.class, int.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(int.class, Integer.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(int.class, int.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Integer.class, Integer.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Integer.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Integer.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(int.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, int.class)).isFalse();
	}

	@Test
	public void testApplyValueLong() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Long.class, long.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(long.class, Long.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(long.class, long.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Long.class, Long.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Long.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Long.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(long.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, long.class)).isFalse();
	}

	@Test
	public void testApplyValueFloat() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Float.class, float.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(float.class, Float.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(float.class, float.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Float.class, Float.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Float.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Float.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(float.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, float.class)).isFalse();
	}

	@Test
	public void testApplyValueDouble() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Double.class, double.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(double.class, Double.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(double.class, double.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Double.class, Double.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Double.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Double.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(double.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, double.class)).isFalse();
	}

	@Test
	public void testApplyValueChar() throws Exception {
		assertThat(ReflectUtil.isAssignableTo(Character.class, char.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(char.class, Character.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(char.class, char.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(Character.class, Character.class)).isTrue();
		assertThat(ReflectUtil.isAssignableTo(String.class, Character.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(Character.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(char.class, String.class)).isFalse();
		assertThat(ReflectUtil.isAssignableTo(String.class, char.class)).isFalse();
	}
}
