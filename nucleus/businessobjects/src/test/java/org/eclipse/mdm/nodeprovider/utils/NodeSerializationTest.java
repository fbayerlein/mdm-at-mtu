/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.nodeprovider.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class NodeSerializationTest {
	@Test
	public void testSerializeDeserialize() {
		EntityType entityType = mockEntityType();

		Node node = SerializationUtil.createNode("MDMNVH", "Test", "123", "Id", Filter.idOnly(entityType, "123"),
				"xyz");

		String serial = SerializationUtil.serializeNode(node);
		Node deserNode = SerializationUtil.deserializeNode(serial);

		assertThat(deserNode).isEqualTo(node);
		assertThat(deserNode).hasFieldOrPropertyWithValue("source", "MDMNVH")
				.hasFieldOrPropertyWithValue("type", "Test").hasFieldOrPropertyWithValue("label", "xyz");
	}

	private EntityType mockEntityType() {

		Attribute attribute = Mockito.mock(Attribute.class);
		Mockito.when(attribute.getName()).thenReturn("Id");
		Mockito.when(attribute.getValueType()).thenReturn(ValueType.STRING);
		Mockito.when(attribute.createValue(Mockito.any(), Mockito.anyString())).thenAnswer(new Answer<Value>() {
			@Override
			public Value answer(InvocationOnMock invocation) {
				return ValueType.STRING.create("Id", "", true, invocation.getArgument(1));
			}
		});

		EntityType entityType = Mockito.mock(EntityType.class);
		Mockito.when(entityType.getIDAttribute()).thenReturn(attribute);
		Mockito.when(entityType.getAttribute(Mockito.eq("Id"))).thenReturn(attribute);
		Mockito.when(entityType.getName()).thenReturn("Test");
		Mockito.when(attribute.getEntityType()).thenReturn(entityType);

		return entityType;
	}
}
