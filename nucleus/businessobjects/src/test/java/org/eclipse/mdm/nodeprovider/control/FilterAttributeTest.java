/*******************************************************************************
 *  Copyright (c) 2022 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.nodeprovider.control;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.nodeprovider.entity.FilterAttribute;
import org.eclipse.mdm.nodeprovider.entity.NodeLevel;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderRoot;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.google.common.collect.ImmutableMap;

@Disabled
@Testcontainers(disabledWithoutDocker = true)
public class FilterAttributeTest {

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	static ApplicationContext context;

	private static ConnectorService connectorService = Mockito.mock(ConnectorService.class);
	private static MDMExpressionLanguageService elService = new MDMExpressionLanguageService();

	private static String envName;
	private static String sourceName;
	private static Project createdProject;
	private static List<Pool> createdPools;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		Mockito.when(connectorService.getContexts()).thenReturn(Arrays.asList(context));
		Mockito.when(connectorService.getContextByName(any())).thenReturn(context);

		EntityManager em = context.getEntityManager().get();
		EntityFactory f = context.getEntityFactory().get();

		Environment e = em.loadAll(Environment.class).get(0);

		envName = e.getName();
		sourceName = e.getSourceName();

		Transaction t = em.startTransaction();
		t.delete(em.loadAll(Project.class, "NPFilterAttributeTest"));
		createdProject = f.createProject("NPFilterAttributeTest");
		t.create(Arrays.asList(createdProject));
		Pool p1 = f.createPool("Test_1", createdProject);
		Pool p2 = f.createPool("Test_2", createdProject);
		Pool p3 = f.createPool("Test_3", createdProject);
		Pool p4 = f.createPool("Test", createdProject);
		Pool p5 = f.createPool("MyPool", createdProject);
		createdPools = Arrays.asList(p1, p2, p3, p4, p5);
		t.create(createdPools);
		t.commit();
	}

	@AfterAll
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			EntityManager em = context.getEntityManager().get();
			Transaction t = em.startTransaction();
			t.delete(em.loadAll(Project.class, "NPFilterAttributeTest"));
			t.commit();

			context.close();
		}
	}

//	@Test
//	public void testNodeProviderWithCustomFilter() {
//		ModelManager modelManager = context.getModelManager().get();
//
//		// Environment
//		NodeLevel env = new NodeLevel(modelManager.getEntityType(Environment.class));
//
//		// Project
//		NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));
//
//		// Pool
//		EntityType etPool = modelManager.getEntityType(Pool.class);
//
//		NodeLevel pool = new NodeLevel(etPool, new FilterAttribute(etPool.getNameAttribute(), ComparisonOperator.LIKE,
//				"${mdm:substring(Name, '_', 0, 'SomethingElse')}*"), etPool.getNameAttribute());
//		pool.setVirtual(true);
//		pool.setLabelExpression("${mdm:substring(Name, '_', 0, 'SomethingElse')}");
//
//		env.setChild(project);
//		project.setChild(pool);
//
//		NodeProviderRoot npr = new NodeProviderRoot("NPFilterAttributeTest", "NPFilterAttributeTest",
//				ImmutableMap.of("*", env));
//
//		GenericNodeProvider np = new GenericNodeProvider(connectorService, npr, elService);
//
//		List<Node> roots = np.getRoots();
//		assertThat(roots).containsExactly(SerializationUtil.createNode(sourceName, "Environment", 1, "1", "", envName));
//
//		List<Node> projects = np.getChildren(roots.get(0));
//		Node p = projects.stream().filter(n -> "NPFilterAttributeTest".equals(n.getLabel())).findFirst().get();
//
//		assertThat(p).isEqualTo(SerializationUtil.createNode(sourceName, "Project", 2, createdProject.getID(),
//				"Project.Id eq \"" + createdProject.getID() + "\"", "NPFilterAttributeTest"));
//
//		List<Node> pools = np.getChildren(p);
//		assertThat(pools).containsExactly(
//				SerializationUtil.createNode(sourceName, "Pool", 3, null,
//						"Project.Id eq \"" + createdProject.getID() + "\" and Pool.Name lk \"MyPool*\"", "MyPool"),
//				SerializationUtil.createNode(sourceName, "Pool", 3, null,
//						"Project.Id eq \"" + createdProject.getID() + "\" and Pool.Name lk \"Test*\"", "Test"));
//	}
//
//	@Test
//	public void testNodeProviderWithCustomFilterFromJson() {
//
//		String json = new BufferedReader(new InputStreamReader(
//				SerializationUtil.class.getResourceAsStream("/np_filter_attribute_test.json"), StandardCharsets.UTF_8))
//						.lines().collect(Collectors.joining("\n"));
//
//		NodeProviderRoot npr = SerializationUtil.deserializeNodeProviderRoot(connectorService, json);
//
//		GenericNodeProvider np = new GenericNodeProvider(connectorService, npr, elService);
//
//		List<Node> roots = np.getRoots();
//		assertThat(roots).containsExactly(SerializationUtil.createNode(sourceName, "Environment", 1, "1", "", envName));
//
//		List<Node> projects = np.getChildren(roots.get(0));
//		Node p = projects.stream().filter(n -> "NPFilterAttributeTest".equals(n.getLabel())).findFirst().get();
//
//		assertThat(p).isEqualTo(SerializationUtil.createNode(sourceName, "Project", 2, createdProject.getID(),
//				"Project.Id eq \"" + createdProject.getID() + "\"", "NPFilterAttributeTest"));
//
//		List<Node> pools = np.getChildren(p);
//		assertThat(pools).containsExactly(
//				SerializationUtil.createNode(sourceName, "Pool", 3, null,
//						"Project.Id eq \"" + createdProject.getID() + "\" and Pool.Name lk \"MyPool*\"", "MyPool"),
//				SerializationUtil.createNode(sourceName, "Pool", 3, null,
//						"Project.Id eq \"" + createdProject.getID() + "\" and Pool.Name lk \"Test*\"", "Test"));
//	}
}
