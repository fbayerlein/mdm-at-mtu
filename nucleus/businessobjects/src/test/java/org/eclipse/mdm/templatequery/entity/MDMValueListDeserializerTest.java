/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.templatequery.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.groups.Tuple;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MDMValueListDeserializerTest {

	final ObjectMapper objectMapper = new ObjectMapper();

	@Test
	public void testDeserialize() throws Exception {
		String json = "{ \"name\": \"myTplAttr\", \"id\": \"\", \"type\": \"TemplateAttribute\", \"sourceType\": \"TplUnitUnderTestAttr\", \"sourceName\": \"MDM\", \"attributes\": ["
				+ "{ \"name\": \"myStrings\", \"dataType\": \"STRING_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"test\", \"test2\"] },"
				+ "{ \"name\": \"myString\", \"dataType\": \"STRING\", \"unit\": \"m\", \"value\": \"test\" }" + "]}";

		TemplateEntityDTO value = objectMapper.readValue(json, TemplateEntityDTO.class);

		assertThat(value.getAttributes()).hasSize(2)
				.extracting(Value::getName, Value::getUnit, Value::getValueType, Value::extract).containsExactly(
						Tuple.tuple("myStrings", "m", ValueType.STRING_SEQUENCE, new String[] { "test", "test2" }),
						Tuple.tuple("myString", "m", ValueType.STRING, "test"));
	}
}
