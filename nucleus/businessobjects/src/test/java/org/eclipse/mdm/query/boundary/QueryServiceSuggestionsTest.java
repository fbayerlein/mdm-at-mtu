/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.boundary;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Arrays;
import java.util.UUID;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.NewCookie;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.businessobjects.utils.JsonMessageBodyProvider;
import org.eclipse.mdm.businessobjects.utils.Serializer;
import org.eclipse.mdm.query.entity.SuggestionRequest;
import org.eclipse.mdm.query.entity.SuggestionResponse;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class QueryServiceSuggestionsTest {

	// Be sure global.properties contains
	// businessobjects.query.suggestionsforsequenceattributes=true

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	@BeforeAll
	public static void setup() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		ef.createCatalogAttribute("model", ValueType.STRING, ccVehicle);
		ef.createCatalogAttribute("identifier", ValueType.STRING_SEQUENCE, ccVehicle);
		ef.createCatalogAttribute("int", ValueType.INTEGER, ccVehicle);
		ef.createCatalogAttribute("ints", ValueType.INTEGER_SEQUENCE, ccVehicle);
		ef.createCatalogAttribute("date", ValueType.DATE, ccVehicle);
		ef.createCatalogAttribute("dates", ValueType.DATE_SEQUENCE, ccVehicle);

		TemplateRoot trUut = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uut");
		ef.createTemplateComponent("vehicle", trUut, ccVehicle, true);

		TemplateTestStep templateTestStep = ef.createTemplateTestStep("MyTemplateTestStep");
		templateTestStep.setTemplateRoot(trUut);

		TemplateTest templateTest = ef.createTemplateTest("MyTemplateTest");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle));
			t.create(Arrays.asList(trUut));
			t.create(Arrays.asList(templateTestStep));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool, templateTest, false);
		TestStep testStep = ef.createTestStep("MyTestStep", test, templateTestStep);
		Measurement measurement1 = ef.createMeasurement("MyMeasurement1", testStep);

		ContextComponent measuredVehicle1 = measurement1.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		measuredVehicle1.getValue("model").set("Summit");
		measuredVehicle1.getValue("identifier").set(new String[] { "abc", "abcd", "abcde", "xyz", "abxy" });
		measuredVehicle1.getValue("int").set(3);
		measuredVehicle1.getValue("ints").set(new int[] { 1234, 123, 932, 1 });
		measuredVehicle1.getValue("date").set(Serializer.parseDate("2024-08-12T12:13:14Z"));
		measuredVehicle1.getValue("dates")
				.set(new Instant[] { Instant.parse("2023-01-01T12:13:14Z"), Instant.parse("2022-01-01T12:13:14Z") });

		Measurement measurement2 = ef.createMeasurement("MyMeasurement2", testStep);

		ContextComponent measuredVehicle2 = measurement2.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		measuredVehicle2.getValue("model").set("PMV");
		measuredVehicle2.getValue("identifier").set(new String[] { "xyz", "abc_2", "xyz" });
		measuredVehicle2.getValue("int").set(42);
		measuredVehicle2.getValue("ints").set(new int[] { 932, 665, 12345 });
		measuredVehicle2.getValue("date").set(Serializer.parseDate("2024-08-12T07:13:14Z"));
		measuredVehicle2.getValue("dates")
				.set(new Instant[] { Instant.parse("2023-01-01T12:13:14Z"), Instant.parse("2024-01-01T12:13:14Z") });

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsString(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("model");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("Summit", "PMV");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsStringWithPrefix(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("model");
		request.setPrefix("P");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("PMV");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsStringSequence(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("identifier");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("abc", "abcd", "abcde", "xyz", "abxy", "abc_2");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsStringSequenceWithPrefix(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("identifier");
		request.setPrefix("abc");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("abc", "abcd", "abcde", "abc_2");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsInteger(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("int");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("0", "3", "42"); // TODO Should 0 be returned?
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsIntegerWithPrefix(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("int");
		request.setPrefix("4");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("42");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsIntegerSequence(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("ints");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("1234", "123", "932", "1", "665", "12345");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsIntegerSequenceWithPrefix(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("ints");
		request.setPrefix("123");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("1234", "123", "12345");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsDate(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("date");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("2024-08-12T12:13:14Z", "2024-08-12T07:13:14Z");
	}

	@org.junit.jupiter.api.Test
	void testSuggestionsDateSequence(NewCookie sessionCookie) throws DataAccessException {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class);

		SuggestionRequest request = new SuggestionRequest();
		request.setSourceNames(Arrays.asList("MDM"));
		request.setType("vehicle");
		request.setAttrName("dates");

		assertThat(target.path("mdm/suggestions").request().cookie(sessionCookie).post(Entity.json(request),
				SuggestionResponse.class))
						.extracting(SuggestionResponse::getData, as(InstanceOfAssertFactories.list(String.class)))
						.contains("2023-01-01T12:13:14Z", "2022-01-01T12:13:14Z", "2024-01-01T12:13:14Z");
	}

}
