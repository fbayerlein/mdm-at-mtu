/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.businessobjects.utils.JsonMessageBodyProvider;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.eclipse.mdm.protobuf.Mdm;
import org.eclipse.mdm.protobuf.Mdm.ChannelValuesList;
import org.eclipse.mdm.protobuf.Mdm.ChannelValuesList.ChannelValues;
import org.eclipse.mdm.protobuf.Mdm.DoubleArray;
import org.eclipse.mdm.protobuf.Mdm.FloatArray;
import org.eclipse.mdm.protobuf.Mdm.IntegerArray;
import org.eclipse.mdm.protobuf.Mdm.MeasuredValues;
import org.eclipse.mdm.protobuf.Mdm.MeasuredValuesList;
import org.eclipse.mdm.protobuf.Mdm.ReadChannelValuesRequest;
import org.eclipse.mdm.protobuf.Mdm.ReadRequest;
import org.eclipse.mdm.protobuf.Mdm.ScalarType;
import org.eclipse.mdm.protobuf.Mdm.StringArray;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.ExplicitData;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ValuesResourceTest {

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	static Quantity quantity;
	static TestStep testStep;
	static Unit unitKg;
	static Unit unitG;

	Measurement measurement;
	ChannelGroup channelGroup;
	Channel channel;

	@BeforeAll
	public static void setup() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		unitKg = em.loadAll(Unit.class, "kg").get(0);
		unitG = em.loadAll(Unit.class, "g").get(0);

		quantity = ef.createQuantity("MyQuantity");

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		testStep = ef.createTestStep("MyTestStep", test);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project));
			t.commit();
		}
		context.close();
	}

	@BeforeEach
	private void createMeasurement() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		measurement = ef.createMeasurement("MyMeasurement" + System.currentTimeMillis(), testStep);
		channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		channel = ef.createChannel("mass", measurement, quantity, unitKg);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.commit();
		}
		context.close();
	}

	@org.junit.jupiter.api.Test
	void writeInDefaultUnit(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		WriteRequestList.WriteRequest writeRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setFloatArray(FloatArray.newBuilder().addValues(1125f).addValues(742.3f).addValues(0.3f))
						.addFlags(true).addFlags(true).addFlags(true))
				.build();
		WriteRequestList list = WriteRequestList.newBuilder().addValues(writeRequest).build();

		target.path("mdm/environments/MDM/values/write").request().cookie(sessionCookie)
				.post(Entity.entity(list, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF), Void.class);

		// Validate
		ReadRequest readRequest = ReadRequest.newBuilder().setChannelGroupId(channelGroup.getID())
				.addChannelIds(channel.getID()).build();

		MeasuredValuesList measuredValuesList = target.path("mdm/environments/MDM/values/read").request()
				.cookie(sessionCookie)
				.post(Entity.entity(readRequest, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						MeasuredValuesList.class);

		assertThat(measuredValuesList).isEqualTo(MeasuredValuesList.newBuilder()
				.addValues(MeasuredValues.newBuilder().setName("mass").setUnit("kg").setLength(3)
						.setScalarType(ScalarType.FLOAT)
						.setFloatArray(FloatArray.newBuilder().addValues(1125f).addValues(742.3f).addValues(0.3f))
						.addFlags(true).addFlags(true).addFlags(true))
				.build());
	}

	@org.junit.jupiter.api.Test
	void convertsUnitOnInitialWrite(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		// Channel1 is created with unit kg and we append values in unit g.
		WriteRequestList.WriteRequest writeRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setFloatArray(FloatArray.newBuilder().addValues(1125f).addValues(742.0f).addValues(0.3f))
						.addFlags(true).addFlags(true).addFlags(true))
				.setSourceUnitId(unitG.getID()).build();
		WriteRequestList list = WriteRequestList.newBuilder().addValues(writeRequest).build();

		target.path("mdm/environments/MDM/values/write").request().cookie(sessionCookie)
				.post(Entity.entity(list, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF), Void.class);

		// Validate
		ReadRequest readRequest = ReadRequest.newBuilder().setChannelGroupId(channelGroup.getID())
				.addChannelIds(channel.getID()).build();

		MeasuredValuesList measuredValuesList = target.path("mdm/environments/MDM/values/read").request()
				.cookie(sessionCookie)
				.post(Entity.entity(readRequest, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						MeasuredValuesList.class);

		assertThat(measuredValuesList).isEqualTo(MeasuredValuesList.newBuilder()
				.addValues(MeasuredValues.newBuilder().setName("mass").setUnit("kg").setLength(3)
						.setScalarType(ScalarType.FLOAT)
						.setFloatArray(FloatArray.newBuilder().addValues(1.125f).addValues(0.742f).addValues(0.0003f))
						.addFlags(true).addFlags(true).addFlags(true))
				.build());
	}

	@org.junit.jupiter.api.Test
	void convertsUnitOnInitialAppend(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		// Channel1 is created with unit kg and we append values in unit g.
		WriteRequestList.WriteRequest writeRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setFloatArray(FloatArray.newBuilder().addValues(1125f).addValues(742.0f).addValues(0.3f))
						.addFlags(true).addFlags(true).addFlags(true))
				.setSourceUnitId(unitG.getID()).build();
		WriteRequestList list = WriteRequestList.newBuilder().addValues(writeRequest).build();

		target.path("mdm/environments/MDM/values/append").request().cookie(sessionCookie)
				.post(Entity.entity(list, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF), Void.class);

		// Validate
		ReadRequest readRequest = ReadRequest.newBuilder().setChannelGroupId(channelGroup.getID())
				.addChannelIds(channel.getID()).build();

		MeasuredValuesList measuredValuesList = target.path("mdm/environments/MDM/values/read").request()
				.cookie(sessionCookie)
				.post(Entity.entity(readRequest, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						MeasuredValuesList.class);

		assertThat(measuredValuesList).isEqualTo(MeasuredValuesList.newBuilder()
				.addValues(MeasuredValues.newBuilder().setName("mass").setUnit("kg").setLength(3)
						.setScalarType(ScalarType.FLOAT)
						.setFloatArray(FloatArray.newBuilder().addValues(1.125f).addValues(0.742f).addValues(0.0003f))
						.addFlags(true).addFlags(true).addFlags(true))
				.build());
	}

	@org.junit.jupiter.api.Test
	void convertsUnitOnAppend(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		// First we write data with length 0, to create the localcolumns
		WriteRequestList.WriteRequest writeRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder().setFloatArray(FloatArray.getDefaultInstance()))
				.setSourceUnitId(unitKg.getID()).build();

		WriteRequestList list = WriteRequestList.newBuilder().addValues(writeRequest).build();

		target.path("mdm/environments/MDM/values/write").request().cookie(sessionCookie)
				.post(Entity.entity(list, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF), Void.class);

		// Then we append data in unit g which should be converted to kg
		WriteRequestList.WriteRequest appendRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setFloatArray(FloatArray.newBuilder().addValues(1125f).addValues(742.0f).addValues(0.3f))
						.addFlags(true).addFlags(true).addFlags(true))
				.setSourceUnitId(unitG.getID()).build();

		WriteRequestList appendList = WriteRequestList.newBuilder().addValues(appendRequest).build();

		target.path("mdm/environments/MDM/values/append").request().cookie(sessionCookie)
				.post(Entity.entity(appendList, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF), Void.class);

		// Validate
		ReadRequest readRequest = ReadRequest.newBuilder().setChannelGroupId(channelGroup.getID())
				.addChannelIds(channel.getID()).build();

		MeasuredValuesList measuredValuesList = target.path("mdm/environments/MDM/values/read").request()
				.cookie(sessionCookie)
				.post(Entity.entity(readRequest, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						MeasuredValuesList.class);

		assertThat(measuredValuesList).isEqualTo(MeasuredValuesList.newBuilder()
				.addValues(MeasuredValues.newBuilder().setName("mass").setUnit("kg").setLength(3)
						.setScalarType(ScalarType.FLOAT)
						.setFloatArray(FloatArray.newBuilder().addValues(1.125f).addValues(0.742f).addValues(0.0003f))
						.addFlags(true).addFlags(true).addFlags(true))
				.build());
	}

	@org.junit.jupiter.api.Test
	void writeUnitForStringChannel(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		WriteRequestList.WriteRequest writeRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setStringArray(StringArray.newBuilder().addValues("v1").addValues("v2").addValues("v3"))
						.addFlags(true).addFlags(true).addFlags(true))
				.setSourceUnitId(unitKg.getID()).build();

		WriteRequestList list = WriteRequestList.newBuilder().addValues(writeRequest).build();

		target.path("mdm/environments/MDM/values/write").request().cookie(sessionCookie)
				.post(Entity.entity(list, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF), Void.class);

		// Validate
		ReadRequest readRequest = ReadRequest.newBuilder().setChannelGroupId(channelGroup.getID())
				.addChannelIds(channel.getID()).build();

		MeasuredValuesList measuredValuesList = target.path("mdm/environments/MDM/values/read").request()
				.cookie(sessionCookie)
				.post(Entity.entity(readRequest, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						MeasuredValuesList.class);

		assertThat(measuredValuesList).isEqualTo(MeasuredValuesList.newBuilder()
				.addValues(MeasuredValues.newBuilder().setName("mass").setUnit("kg").setLength(3)
						.setScalarType(ScalarType.STRING)
						.setStringArray(StringArray.newBuilder().addValues("v1").addValues("v2").addValues("v3"))
						.addFlags(true).addFlags(true).addFlags(true))
				.build());
	}

	@org.junit.jupiter.api.Test
	void writeWithoutFlags(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		WriteRequestList.WriteRequest writeRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setStringArray(StringArray.newBuilder().addValues("v1").addValues("v2").addValues("v3")))
				.build();

		WriteRequestList list = WriteRequestList.newBuilder().addValues(writeRequest).build();

		target.path("mdm/environments/MDM/values/write").request().cookie(sessionCookie)
				.post(Entity.entity(list, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF), Void.class);

		// Validate
		ReadRequest readRequest = ReadRequest.newBuilder().setChannelGroupId(channelGroup.getID())
				.addChannelIds(channel.getID()).build();

		MeasuredValuesList measuredValuesList = target.path("mdm/environments/MDM/values/read").request()
				.cookie(sessionCookie)
				.post(Entity.entity(readRequest, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						MeasuredValuesList.class);

		assertThat(measuredValuesList).isEqualTo(MeasuredValuesList.newBuilder()
				.addValues(MeasuredValues.newBuilder().setName("mass").setUnit("kg").setLength(3)
						.setScalarType(ScalarType.STRING)
						.setStringArray(StringArray.newBuilder().addValues("v1").addValues("v2").addValues("v3"))
						.addFlags(true).addFlags(true).addFlags(true))
				.build());
	}

	@org.junit.jupiter.api.Test
	void appendWithoutFlags(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(JsonMessageBodyProvider.class)
				.register(ProtobufMessageBodyProvider.class);

		WriteRequestList.WriteRequest writeRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setStringArray(StringArray.newBuilder().addValues("v1").addValues("v2").addValues("v3")))
				.build();

		WriteRequestList list = WriteRequestList.newBuilder().addValues(writeRequest).build();

		target.path("mdm/environments/MDM/values/write").request().cookie(sessionCookie)
				.post(Entity.entity(list, MediaType.APPLICATION_JSON), Void.class);

		WriteRequestList.WriteRequest appendRequest = WriteRequestList.WriteRequest.newBuilder()
				.setChannelGroupId(channelGroup.getID()).setChannelId(channel.getID()).setAxisType(Mdm.AxisType.X_AXIS)
				.setExplicit(ExplicitData.newBuilder()
						.setStringArray(StringArray.newBuilder().addValues("v4").addValues("v5").addValues("v6")))
				.build();

		WriteRequestList appendList = WriteRequestList.newBuilder().addValues(appendRequest).build();

		target.path("mdm/environments/MDM/values/append").request().cookie(sessionCookie)
				.accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(appendList, MediaType.APPLICATION_JSON), Void.class);

		// Validate
		ReadRequest readRequest = ReadRequest.newBuilder().setChannelGroupId(channelGroup.getID())
				.addChannelIds(channel.getID()).build();

		MeasuredValuesList measuredValuesList = target.path("mdm/environments/MDM/values/read").request()
				.cookie(sessionCookie)
				.post(Entity.entity(readRequest, MediaType.APPLICATION_JSON), MeasuredValuesList.class);

		assertThat(measuredValuesList).isEqualTo(MeasuredValuesList.newBuilder()
				.addValues(MeasuredValues.newBuilder().setName("mass").setUnit("kg").setLength(6)
						.setScalarType(ScalarType.STRING)
						.setStringArray(StringArray.newBuilder().addValues("v1").addValues("v2").addValues("v3")
								.addValues("v4").addValues("v5").addValues("v6"))
						.addFlags(true).addFlags(true).addFlags(true).addFlags(true).addFlags(true).addFlags(true))
				.build());
	}

	@org.junit.jupiter.api.Test
	void readChannelValues(NewCookie sessionCookie) throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		measurement = ef.createMeasurement("MyMeasurement" + System.currentTimeMillis(), testStep);
		channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		Channel channel1 = ef.createChannel("name", measurement, quantity);
		Channel channel2 = ef.createChannel("index", measurement, quantity);
		Channel channel3 = ef.createChannel("mass", measurement, quantity);

		WriteRequest w1 = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS).explicit()
				.stringValues(new String[] { "one", "two", "three" }).build();
		WriteRequest w2 = WriteRequest.create(channelGroup, channel2, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 0, 1, 2 }).build();
		WriteRequest w3 = WriteRequest.create(channelGroup, channel3, AxisType.Y_AXIS).explicit()
				.doubleValues(new double[] { 42.0, 3.14159, 2.79 }).build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.writeMeasuredValues(Arrays.asList(w1, w2, w3));
			t.commit();
		}
		context.close();

		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		ReadChannelValuesRequest request = ReadChannelValuesRequest.newBuilder()
				.setFilter("Measurement.Id eq \"" + measurement.getID() + "\"").setValuesFilter("").build();

		ChannelValuesList channelValuesList = target.path("mdm/environments/MDM/values/data-read").request()
				.cookie(sessionCookie).post(Entity.entity(request, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						ChannelValuesList.class);

		// Validate
		assertThat(channelValuesList).isEqualTo(ChannelValuesList.newBuilder()
				.addValues(ChannelValues.newBuilder().setChannelId(channel1.getID()).setName("name")
						.setChannelGroupId(channelGroup.getID())
						.setStringArray(StringArray.newBuilder().addValues("one").addValues("two").addValues("three"))
						.addAllFlags(Arrays.asList(true, true, true)).build())
				.addValues(ChannelValues.newBuilder().setChannelId(channel2.getID()).setName("index")
						.setChannelGroupId(channelGroup.getID())
						.setIntegerArray(IntegerArray.newBuilder().addValues(0).addValues(1).addValues(2))
						.addAllFlags(Arrays.asList(true, true, true)).build())
				.addValues(ChannelValues.newBuilder().setChannelId(channel3.getID()).setName("mass")
						.setChannelGroupId(channelGroup.getID())
						.setDoubleArray(DoubleArray.newBuilder().addValues(42.0).addValues(3.14159).addValues(2.79))
						.addAllFlags(Arrays.asList(true, true, true)).build())
				.build());

		ReadChannelValuesRequest request2 = ReadChannelValuesRequest.newBuilder()
				.setFilter("Measurement.Id eq \"" + measurement.getID() + "\"")
				.setValuesFilter("name in ('one', 'two') and (index le 1)").build();

		ChannelValuesList channelValuesList2 = target.path("mdm/environments/MDM/values/data-read").request()
				.cookie(sessionCookie).post(Entity.entity(request2, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						ChannelValuesList.class);

		// Validate
		assertThat(channelValuesList2).isEqualTo(ChannelValuesList.newBuilder()
				.addValues(ChannelValues.newBuilder().setChannelId(channel1.getID()).setName("name")
						.setChannelGroupId(channelGroup.getID())
						.setStringArray(StringArray.newBuilder().addValues("one").addValues("two"))
						.addAllFlags(Arrays.asList(true, true)).build())
				.addValues(ChannelValues.newBuilder().setChannelId(channel2.getID()).setName("index")
						.setChannelGroupId(channelGroup.getID())
						.setIntegerArray(IntegerArray.newBuilder().addValues(0).addValues(1))
						.addAllFlags(Arrays.asList(true, true)).build())
				.addValues(ChannelValues.newBuilder().setChannelId(channel3.getID()).setName("mass")
						.setChannelGroupId(channelGroup.getID())
						.setDoubleArray(DoubleArray.newBuilder().addValues(42.0).addValues(3.14159))
						.addAllFlags(Arrays.asList(true, true)).build())
				.build());

		ReadChannelValuesRequest request3 = ReadChannelValuesRequest.newBuilder()
				.setFilter("Measurement.Id eq \"" + measurement.getID() + "\"")
				.setValuesFilter("(name not_in ('four', 'five') or mass lt 40.0) and (index bw 1 and 2)").build();

		ChannelValuesList channelValuesList3 = target.path("mdm/environments/MDM/values/data-read").request()
				.cookie(sessionCookie).post(Entity.entity(request3, ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF),
						ChannelValuesList.class);

		// Validate
		assertThat(channelValuesList3).isEqualTo(ChannelValuesList.newBuilder()
				.addValues(ChannelValues.newBuilder().setChannelId(channel1.getID()).setName("name")
						.setChannelGroupId(channelGroup.getID())
						.setStringArray(StringArray.newBuilder().addValues("two").addValues("three"))
						.addAllFlags(Arrays.asList(true, true)).build())
				.addValues(ChannelValues.newBuilder().setChannelId(channel2.getID()).setName("index")
						.setChannelGroupId(channelGroup.getID())
						.setIntegerArray(IntegerArray.newBuilder().addValues(1).addValues(2))
						.addAllFlags(Arrays.asList(true, true)).build())
				.addValues(ChannelValues.newBuilder().setChannelId(channel3.getID()).setName("mass")
						.setChannelGroupId(channelGroup.getID())
						.setDoubleArray(DoubleArray.newBuilder().addValues(3.14159).addValues(2.79))
						.addAllFlags(Arrays.asList(true, true)).build())
				.build());
	}
}
