package org.eclipse.mdm.templatequery;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.UUID;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.NewCookie;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.templatequery.entity.TemplateEntityDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityRootDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityTplCompDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityTplRootDTO;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class TemplateQueryResourceTest {

	@Container
	public static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	public static ApplicationContext context;

	@BeforeAll
	static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		ef.createCatalogAttribute("vin", ValueType.STRING, ccVehicle);

		CatalogComponent ccEngine = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "engine");
		ef.createCatalogAttribute("mkb", ValueType.STRING, ccEngine);

		CatalogComponent ccExhaust = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "exhaust");
		ef.createCatalogAttribute("diameter", ValueType.STRING, ccExhaust);

		CatalogComponent ccExhaustPiping = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "exhaustPiping");
		ef.createCatalogAttribute("length", ValueType.INTEGER, ccExhaustPiping);

		TemplateRoot trUut = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uut");
		TemplateComponent tcVehicle = ef.createTemplateComponent("vehicle", trUut, ccVehicle, true);
		TemplateComponent tcEngine = ef.createTemplateComponent("engine", tcVehicle, ccEngine, true);
		TemplateComponent tcExhaust = ef.createTemplateComponent("exhaust", tcVehicle, ccExhaust, true);
		TemplateComponent tcExhaustPiping = ef.createTemplateComponent("exhaustPiping", tcExhaust, ccExhaustPiping,
				true);

		TemplateTestStep templateTestStep = ef.createTemplateTestStep("MyTemplateTestStep");
		templateTestStep.setTemplateRoot(trUut);

		TemplateTest templateTest = ef.createTemplateTest("MyTemplateTest");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle, ccEngine, ccExhaust, ccExhaustPiping));
			t.create(Arrays.asList(trUut));
			t.create(Arrays.asList(templateTestStep));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}
	}

	@org.junit.jupiter.api.Test
	void testTemplateQuery(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot();

		TemplateEntityRootDTO rootDto = target.path("mdm/environments/MDM/templatequery/unitundertest").request()
				.cookie(sessionCookie).get(TemplateEntityRootDTO.class);
		assertThat(rootDto.getContextType()).isEqualTo("UNITUNDERTEST");

		assertThat(rootDto.getTemplateRoots()).extracting(TemplateEntityDTO::getName).contains("uut");

		TemplateEntityTplRootDTO uut = (TemplateEntityTplRootDTO) rootDto.getTemplateRoots().get(0);
		assertThat(uut.getTplComps()).extracting(TemplateEntityDTO::getName).contains("vehicle");

		TemplateEntityTplCompDTO vehicle = (TemplateEntityTplCompDTO) uut.getTplComps().get(0);
		assertThat(vehicle.getTplComps()).extracting(TemplateEntityDTO::getName).contains("engine", "exhaust");
		assertThat(vehicle.getTplAttrs()).extracting(TemplateEntityDTO::getName).contains("vin");
		assertThat(vehicle.getCatComp().getName()).isEqualTo("vehicle");

		TemplateEntityTplCompDTO engine = (TemplateEntityTplCompDTO) vehicle.getTplComps().get(0);
		assertThat(engine.getTplComps()).isEmpty();
		assertThat(engine.getTplAttrs()).extracting(TemplateEntityDTO::getName).contains("mkb");
		assertThat(engine.getCatComp().getName()).isEqualTo("engine");

		TemplateEntityTplCompDTO exhaust = (TemplateEntityTplCompDTO) vehicle.getTplComps().get(1);
		assertThat(exhaust.getTplComps()).extracting(TemplateEntityDTO::getName).contains("exhaustPiping");
		assertThat(exhaust.getTplAttrs()).extracting(TemplateEntityDTO::getName).contains("diameter");
		assertThat(exhaust.getCatComp().getName()).isEqualTo("exhaust");
	}
}
