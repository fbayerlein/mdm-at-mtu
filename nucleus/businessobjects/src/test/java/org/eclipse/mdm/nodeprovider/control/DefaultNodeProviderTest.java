/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.nodeprovider.control;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.eclipse.mdm.businessobjects.control.NavigationActivity;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class DefaultNodeProviderTest {

	private static final String SOURCE = "TESTSOURCE";
	private static final String ENVIRONMENT = "Environment";
	private static final String PROJECT = "Project";
	private static final String POOL = "Pool";
	private static final String TEST = "Test";
	private static final String TESTSTEP = "TestStep";
	private static final String MEASUREMENT = "Measurement";
	private static final String CHANNELGROUP = "ChannelGroup";
	private static final String CHANNEL = "Channel";

	private static final String ENVIRONMENT_ID = "1";
	private static final String PROJECT_ID = "10";
	private static final String POOL_ID = "100";
	private static final String TEST_ID = "1000";
	private static final String TESTSTEP_ID = "10000";
	private static final String MEASUREMENT_ID = "100000";
	private static final String CHANNELGROUP_ID = "1000000";
	private static final String CHANNEL_ID = "10000000";

	private static DefaultNodeProvider defaultNodeProvider;

	@BeforeAll
	public static void setup() throws ConnectionException {

		BaseEntity environment = mockEntity(Environment.class, ENVIRONMENT, ENVIRONMENT_ID);
		BaseEntity project = mockEntity(Project.class, PROJECT, PROJECT_ID);
		BaseEntity pool1 = mockEntity(Pool.class, POOL, POOL_ID);
		BaseEntity test1 = mockEntity(org.eclipse.mdm.api.base.model.Test.class, TEST, TEST_ID);
		BaseEntity teststep1 = mockEntity(TestStep.class, TESTSTEP, TESTSTEP_ID);
		BaseEntity measurement1 = mockEntity(Measurement.class, MEASUREMENT, MEASUREMENT_ID);
		BaseEntity channelgroup = mockEntity(ChannelGroup.class, CHANNELGROUP, CHANNELGROUP_ID);
		BaseEntity channel1 = mockEntity(Channel.class, CHANNEL, CHANNEL_ID);

		NavigationActivity navigationActivity = mock(NavigationActivity.class);
		doReturn(Collections.singletonList(environment)).when(navigationActivity).getEnvironments();
		doReturn(Collections.singletonList(project)).when(navigationActivity).getProjects(eq(SOURCE));
		doReturn(Collections.singletonList(pool1)).when(navigationActivity).getPools(eq(SOURCE), eq(PROJECT_ID));
		doReturn(Collections.singletonList(test1)).when(navigationActivity).getTests(eq(SOURCE), eq(POOL_ID));
		doReturn(Collections.singletonList(teststep1)).when(navigationActivity).getTestSteps(eq(SOURCE), eq(TEST_ID));
		doReturn(Collections.singletonList(measurement1)).when(navigationActivity).getMeasurements(eq(SOURCE),
				eq(TESTSTEP_ID));
		doReturn(Collections.singletonList(channelgroup)).when(navigationActivity).getChannelGroups(eq(SOURCE),
				eq(MEASUREMENT_ID));
		doReturn(Collections.singletonList(channel1)).when(navigationActivity).getChannels(eq(SOURCE),
				eq(CHANNELGROUP_ID));

		defaultNodeProvider = new DefaultNodeProvider(navigationActivity, mockConnectorService());
	}

	@Test
	public void testFromRoot() {
		List<Node> environments = defaultNodeProvider.getRoots();

		assertThat(environments).hasSize(1);
		Node environment = environments.get(0);

		assertThat(environment.getId()).isEqualTo(ENVIRONMENT_ID);
		assertThat(environment.getSource()).isEqualTo(SOURCE);
		assertThat(environment.getType()).isEqualTo(ENVIRONMENT);

		List<Node> projects = defaultNodeProvider.getChildren(environment);

		assertThat(projects).hasSize(1);
		Node project = projects.get(0);

		assertThat(project.getId()).isEqualTo(PROJECT_ID);
		assertThat(project.getSource()).isEqualTo(SOURCE);
		assertThat(project.getType()).isEqualTo(PROJECT);

		List<Node> pools = defaultNodeProvider.getChildren(project);

		assertThat(pools).hasSize(1);
		Node pool = pools.get(0);

		assertThat(pool.getId()).isEqualTo(POOL_ID);
		assertThat(pool.getSource()).isEqualTo(SOURCE);
		assertThat(pool.getType()).isEqualTo(POOL);

		List<Node> tests = defaultNodeProvider.getChildren(pool);

		assertThat(tests).hasSize(1);
		Node test = tests.get(0);

		assertThat(test.getId()).isEqualTo(TEST_ID);
		assertThat(test.getSource()).isEqualTo(SOURCE);
		assertThat(test.getType()).isEqualTo(TEST);

		List<Node> teststeps = defaultNodeProvider.getChildren(test);

		assertThat(teststeps).hasSize(1);
		Node teststep = teststeps.get(0);

		assertThat(teststep.getId()).isEqualTo(TESTSTEP_ID);
		assertThat(teststep.getSource()).isEqualTo(SOURCE);
		assertThat(teststep.getType()).isEqualTo(TESTSTEP);

		List<Node> measurements = defaultNodeProvider.getChildren(teststep);

		assertThat(measurements).hasSize(1);
		Node measurement = measurements.get(0);

		assertThat(measurement.getId()).isEqualTo(MEASUREMENT_ID);
		assertThat(measurement.getSource()).isEqualTo(SOURCE);
		assertThat(measurement.getType()).isEqualTo(MEASUREMENT);

		List<Node> channelgroups = defaultNodeProvider.getChildren(measurement);

		assertThat(channelgroups).hasSize(1);
		Node channelgroup = channelgroups.get(0);

		assertThat(channelgroup.getId()).isEqualTo(CHANNELGROUP_ID);
		assertThat(channelgroup.getSource()).isEqualTo(SOURCE);
		assertThat(channelgroup.getType()).isEqualTo(CHANNELGROUP);

		List<Node> channels = defaultNodeProvider.getChildren(channelgroup);

		assertThat(channels).hasSize(1);
		Node channel = channels.get(0);

		assertThat(channel.getId()).isEqualTo(CHANNEL_ID);
		assertThat(channel.getSource()).isEqualTo(SOURCE);
		assertThat(channel.getType()).isEqualTo(CHANNEL);
	}

	private static ConnectorService mockConnectorService() {
		ConnectorService connectorService = mock(ConnectorService.class);

		ApplicationContext mockedApplicationContext = mockApplicationContext();

		Mockito.when(connectorService.getContexts()).thenReturn(Arrays.asList(mockedApplicationContext));
		Mockito.when(connectorService.getContextByName(anyString())).thenReturn(mockedApplicationContext);

		return connectorService;
	}

	private static ApplicationContext mockApplicationContext() {
		ApplicationContext applicationContext = mock(ApplicationContext.class);
		EntityManager entityManager = mockEntityManager();
		ModelManager modelManager = mockModelManager();

		Mockito.when(applicationContext.getEntityManager()).thenReturn(Optional.of(entityManager));
		Mockito.when(applicationContext.getModelManager()).thenReturn(Optional.of(modelManager));

		return applicationContext;
	}

	private static ModelManager mockModelManager() {
		ODSModelManager modelManger = mock(ODSModelManager.class);

		doAnswer(onMock -> {
			EntityType entityType = mock(EntityType.class);
			Attribute idAttribute = mock(Attribute.class);
			doReturn("Id").when(idAttribute).getName();
			Value value = mock(Value.class);
			doReturn(ValueType.STRING).when(value).getValueType();

			doReturn(entityType).when(idAttribute).getEntityType();
			doReturn(entityType).when(idAttribute).getEntityType();
			doReturn(idAttribute).when(entityType).getIDAttribute();

			Entity e = onMock.getArgument(0);
			doReturn("").when(e).getName();
			String type = e.getTypeName();

			doReturn(type).when(entityType).getName();
			if (ENVIRONMENT.equals(type)) {
				doReturn(ENVIRONMENT_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			} else if (PROJECT.equals(type)) {
				doReturn(PROJECT_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			} else if (POOL.equals(type)) {
				doReturn(POOL_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			} else if (TEST.equals(type)) {
				doReturn(TEST_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			} else if (TESTSTEP.equals(type)) {
				doReturn(TESTSTEP_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			} else if (MEASUREMENT.equals(type)) {
				doReturn(MEASUREMENT_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			} else if (CHANNELGROUP.equals(type)) {
				doReturn(CHANNELGROUP_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			} else if (CHANNEL.equals(type)) {
				doReturn(CHANNEL_ID).when(value).extract();
				doReturn(value).when(idAttribute).createValue(anyString(), any());
			}
			return entityType;
		}).when(modelManger).getEntityType(any(Entity.class));

//        doAnswer(onMock -> {
//        	return onMock.
//        }).when(modelManger.listEntityTypes());

		return modelManger;
	}

	private static EntityManager mockEntityManager() {
		EntityManager entityManager = mock(EntityManager.class);

		// TODO
		return entityManager;
	}

	private static <T extends BaseEntity> BaseEntity mockEntity(Class<T> mockedClass, String typeName, String id) {
		BaseEntity entity = mock(mockedClass);

		doReturn(SOURCE).when(entity).getSourceName();
		doReturn(typeName).when(entity).getTypeName();
		doReturn(id).when(entity).getID();
		return entity;
	}
}
