/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;

import org.assertj.core.groups.Tuple;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Parameter;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ParameterResourceTest {

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	static TestStep testStep;
	Measurement measurement;
	ParameterSet parameterSet;
	Parameter parameter1;
	Parameter parameter2;

	@BeforeAll
	static void setup() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		testStep = ef.createTestStep("MyTestStep", test);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
		context.close();
	}

	@BeforeEach
	void initData() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		measurement = ef.createMeasurement("MyMeasurement" + System.currentTimeMillis(), testStep);
		parameterSet = ef.createParameterSet("MyParameterSet" + System.currentTimeMillis(), measurement);
		parameter1 = ef.createParameter("MyParameter1" + System.currentTimeMillis(), "value1", parameterSet);
		parameter2 = ef.createParameter("MyParameter2" + System.currentTimeMillis(), 123, parameterSet);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.commit();
		}
		context.close();
	}

	@org.junit.jupiter.api.Test
	void createParameter(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String createParameterSet = "{ \"Name\": \"MyParameter\", \"stringValue\": \"test123\", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" }";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parameters").request()
				.cookie(sessionCookie)
				.post(Entity.entity(createParameterSet, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("Parameter");
		assertThat(response.getData()).satisfiesOnlyOnce(e -> {
			assertThat(e.getName()).isEqualTo("MyParameter");
			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});
	}

	@org.junit.jupiter.api.Test
	void createParametersInBatchMode(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String createParameters = "[\n";
		createParameters += "{ \"Name\": \"StringParam\", \"stringValue\": \"test123\", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"DateParam\", \"dateValue\": \"20241023121314\", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"BooleanParam\", \"booleanValue\": true, \"ParameterSet\": \""
				+ parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"ByteParam\", \"byteValue\": 123, \"ParameterSet\": \"" + parameterSet.getID()
				+ "\" },\n";
		createParameters += "{ \"Name\": \"ShortParam\", \"shortValue\": " + Short.MAX_VALUE + ", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"IntegerParam\", \"integerValue\": " + Integer.MAX_VALUE
				+ ", \"ParameterSet\": \"" + parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"LongParam\", \"longValue\": " + Long.MAX_VALUE + ", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"FloatParam\", \"floatValue\": " + Float.MAX_VALUE + ", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"DoubleParam\", \"doubleValue\": " + Double.MAX_VALUE
				+ ", \"ParameterSet\": \"" + parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"FloatComplexParam\", \"floatComplexValue\": \"3.14 0.01\", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" },\n";
		createParameters += "{ \"Name\": \"DoubleComplexParam\", \"doubleComplexValue\": \"1.234 5.678\", \"ParameterSet\": \""
				+ parameterSet.getID() + "\" }\n";
		createParameters += "]";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parameters").request()
				.cookie(sessionCookie)
				.post(Entity.entity(createParameters, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("Parameter");
//		assertThat(response.getData()).extracting(MDMEntity::getName, p.);

		assertThat(response.getData()).element(0).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("StringParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "test123"),
							Tuple.tuple("DataType", "ENUMERATION", "STRING"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});

		assertThat(response.getData()).element(1).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("DateParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "20241023121314"),
							Tuple.tuple("DataType", "ENUMERATION", "DATE"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});

		assertThat(response.getData()).element(2).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("BooleanParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "true"),
							Tuple.tuple("DataType", "ENUMERATION", "BOOLEAN"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});

		assertThat(response.getData()).element(3).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("ByteParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "123"),
							Tuple.tuple("DataType", "ENUMERATION", "BYTE"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});
		assertThat(response.getData()).element(4).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("ShortParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "" + Short.MAX_VALUE),
							Tuple.tuple("DataType", "ENUMERATION", "SHORT"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});
		assertThat(response.getData()).element(5).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("IntegerParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "" + Integer.MAX_VALUE),
							Tuple.tuple("DataType", "ENUMERATION", "INTEGER"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});
		assertThat(response.getData()).element(6).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("LongParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "" + Long.MAX_VALUE),
							Tuple.tuple("DataType", "ENUMERATION", "LONG"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});
		assertThat(response.getData()).element(7).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("FloatParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "" + Float.MAX_VALUE),
							Tuple.tuple("DataType", "ENUMERATION", "FLOAT"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});
		assertThat(response.getData()).element(8).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("DoubleParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "" + Double.MAX_VALUE),
							Tuple.tuple("DataType", "ENUMERATION", "DOUBLE"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});

		assertThat(response.getData()).element(9).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("FloatComplexParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "3.14 0.01"),
							Tuple.tuple("DataType", "ENUMERATION", "FLOAT_COMPLEX"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});

		assertThat(response.getData()).element(10).satisfies(e -> {
			assertThat(e.getName()).isEqualTo("DoubleComplexParam");
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "1.234 5.678"),
							Tuple.tuple("DataType", "ENUMERATION", "DOUBLE_COMPLEX"));

			assertThat(e.getRelations()).satisfiesOnlyOnce(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("ParameterSet");
				assertThat(rel.getIds()).containsExactly(parameterSet.getID());
			});
		});
	}

	@org.junit.jupiter.api.Test
	void updateParameter(NewCookie sessionCookie) throws Exception {
		Parameter parameter = createTestParameter().get(0);

		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String updateParameter = "{\"Value\": \"test123\"}";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parameters/" + parameter.getID()).request()
				.cookie(sessionCookie)
				.put(Entity.entity(updateParameter, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("Parameter");
		assertThat(response.getData()).satisfiesOnlyOnce(e -> {
			assertThat(e.getName()).isEqualTo(parameter.getName());
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "test123"),
							Tuple.tuple("DataType", "ENUMERATION", "STRING"));
		});
	}

	@org.junit.jupiter.api.Test
	void updateParameterInBatchMode(NewCookie sessionCookie) throws Exception {
		List<Parameter> parameters = createTestParameter();

		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String updateParameter = "{\n";
		updateParameter += "\"" + parameters.get(0).getID() + "\": {\"Value\": \"test123\"},\n";
		updateParameter += "\"" + parameters.get(1).getID() + "\": {\"Value\": \"42\"}\n";
		updateParameter += "}";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parameters").request()
				.cookie(sessionCookie)
				.put(Entity.entity(updateParameter, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("Parameter");
		assertThat(response.getData()).satisfiesOnlyOnce(e -> {
			assertThat(e.getName()).isEqualTo(parameters.get(0).getName());
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "test123"),
							Tuple.tuple("DataType", "ENUMERATION", "STRING"));
		}).satisfiesOnlyOnce(e -> {
			assertThat(e.getName()).isEqualTo(parameters.get(1).getName());
			assertThat(e.getAttributes()).extracting(a -> a.getName(), a -> a.getDataType(), a -> a.getValue())
					.containsOnlyOnce(Tuple.tuple("Value", "STRING", "42"),
							Tuple.tuple("DataType", "ENUMERATION", "INTEGER"));
		});
	}

	private List<Parameter> createTestParameter() throws Exception {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		List<Parameter> list = new ArrayList<>();
		list.add(ef.createParameter("MyParameter1" + System.currentTimeMillis(), "value1", parameterSet));
		list.add(ef.createParameter("MyParameter2" + System.currentTimeMillis(), 123, parameterSet));

		try (Transaction t = em.startTransaction()) {
			t.create(list);
			t.commit();
		}
		context.close();

		return list;
	}
}