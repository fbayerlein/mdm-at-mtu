/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.nodeprovider.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.assertj.core.groups.Tuple;
import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableMap;

public class NodeProviderRootTest {

	private ApplicationContext context;

	@BeforeEach
	public void init() throws ConnectionException {
		Map<String, String> map = ImmutableMap.of("atfxfile", "../../api/atfxadapter/src/test/resources/Right_Acc.atfx",
				"freetext.active", "false");

		context = new ATFXContextFactory().connect("ATFX", map);
	}

	@AfterEach
	public void close() throws ConnectionException {
		context.close();
	}

	private NodeProviderRoot getNodeProviderRoot(ApplicationContext context, NodeLevel nl) {
		NodeProviderRoot npr = new NodeProviderRoot("generic_measured", "Measured");
		npr.setContexts(ImmutableMap.of("*", SerializationUtil.convertToJsonNode(nl)));
		return npr;
	}

	@Test
	public void testGetNodeLevelByNode() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure(context, ContextState.MEASURED));

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Environment").setLevel(1).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "Environment")
						.hasFieldOrPropertyWithValue("level", 1);

		assertThat(
				np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Project").setLevel(2).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "Project")
						.hasFieldOrPropertyWithValue("level", 2);

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("TestStep").setLevel(5).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "TestStep")
						.hasFieldOrPropertyWithValue("level", 5);

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Measurement").setLevel(6).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "MeaResult")
						.hasFieldOrPropertyWithValue("level", 6);

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("ChannelGroup").setLevel(7).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "SubMatrix")
						.hasFieldOrPropertyWithValue("level", 7);

		assertThat(
				np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Channel").setLevel(8).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "MeaQuantity")
						.hasFieldOrPropertyWithValue("level", 8);

		// Type is invalid, but NodeLevel is identified by level
		assertThat(
				np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("invalid").setLevel(5).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "TestStep")
						.hasFieldOrPropertyWithValue("level", 5);
	}

	@Test
	public void testGetNodeLevelByNodeWithVirtualNodes() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure2(context, ContextState.MEASURED));

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Environment").setLevel(1).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "Environment")
						.hasFieldOrPropertyWithValue("level", 1);

		assertThat(
				np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Project").setLevel(2).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "Project")
						.hasFieldOrPropertyWithValue("level", 2);

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Measurement").setLevel(3).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "MeaResult")
						.hasFieldOrPropertyWithValue("level", 3);

		assertThat(np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Test").setLevel(4).build()))
				.hasFieldOrPropertyWithValue("entityType.name", "Test").hasFieldOrPropertyWithValue("level", 4);

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Measurement").setLevel(5).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "MeaResult")
						.hasFieldOrPropertyWithValue("level", 5);

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Measurement").setLevel(6).build()))
						.hasFieldOrPropertyWithValue("entityType.name", "MeaResult")
						.hasFieldOrPropertyWithValue("level", 6);

	}

	@Test
	public void testGetNodeLevelByNodeWithoutLevelSet() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure(context, ContextState.MEASURED));

		assertThat(np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Project").build()))
				.hasFieldOrPropertyWithValue("entityType.name", "Project").hasFieldOrPropertyWithValue("level", 2);

		assertThat(np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("TestStep").build()))
				.hasFieldOrPropertyWithValue("entityType.name", "TestStep").hasFieldOrPropertyWithValue("level", 5);
	}

	@Test
	public void testGetNodeLevelByNodeWithoutLevelSetWithVirtualNodes() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure2(context, ContextState.MEASURED));

		assertThat(np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Environment").build()))
				.hasFieldOrPropertyWithValue("entityType.name", "Environment").hasFieldOrPropertyWithValue("level", 1);

		assertThat(np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Project").build()))
				.hasFieldOrPropertyWithValue("entityType.name", "Project").hasFieldOrPropertyWithValue("level", 2);

		// Cannot get virtual NodeLevel without specifying nodelevel
		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Measurement").setIdAttribute("DateCreated").build()))
						.isNull();

		assertThat(np.getNodeLevel(context, Node.newBuilder().setSource("NVHDEMO").setType("Test").build()))
				.hasFieldOrPropertyWithValue("entityType.name", "Test").hasFieldOrPropertyWithValue("level", 4);

		assertThat(np.getNodeLevel(context,
				Node.newBuilder().setSource("NVHDEMO").setType("Measurement").setId("Id").build()))
						.hasFieldOrPropertyWithValue("entityType.name", "MeaResult")
						.hasFieldOrPropertyWithValue("level", 6);
	}

	@Test
	public void testGetNodeLevel() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure(context, ContextState.MEASURED));

		NodeLevel nl1 = np.getNodeLevel(context,
				SerializationUtil.createNode("NVHDEMO", np.getNodeLevel(context), "Id", "Name"));

		assertThat(nl1.getEntityType().getName()).isEqualTo("Environment");
		assertThat(nl1.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("Id", ComparisonOperator.EQUAL, "${Id}"));
		assertThat(nl1.getContextState()).isNull();
		assertThat(nl1.isVirtual()).isFalse();

		NodeLevel nl2 = np.getNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl1.getChild(), "Id", "Name"));
		assertThat(nl2.getEntityType().getName()).isEqualTo("Project");
		assertThat(nl2.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("Id", ComparisonOperator.EQUAL, "${Id}"));
		assertThat(nl2.getContextState()).isNull();
		assertThat(nl2.isVirtual()).isFalse();

		NodeLevel nl3 = np.getNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl2.getChild(), "Id", "Name"));
		assertThat(nl3.getEntityType().getName()).isEqualTo("vehicle");
		assertThat(nl3.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("model", ComparisonOperator.EQUAL, "${model}"));
		assertThat(nl3.getContextState()).isEqualTo(ContextState.MEASURED);
		assertThat(nl3.isVirtual()).isTrue();

		NodeLevel nl4 = np.getNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl3.getChild(), "Id", "Name"));
		assertThat(nl4.getEntityType().getName()).isEqualTo("Test");
		assertThat(nl4.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("Id", ComparisonOperator.EQUAL, "${Id}"));
		assertThat(nl4.getContextState()).isNull();
		assertThat(nl4.isVirtual()).isFalse();

		NodeLevel nl5 = np.getNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl4.getChild(), "Id", "Name"));
		assertThat(nl5.getEntityType().getName()).isEqualTo("TestStep");
		assertThat(nl5.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("Id", ComparisonOperator.EQUAL, "${Id}"));
		assertThat(nl5.getContextState()).isNull();
		assertThat(nl5.isVirtual()).isFalse();

		NodeLevel nl6 = np.getNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl5.getChild(), "Id", "Name"));
		assertThat(nl6.getEntityType().getName()).isEqualTo("MeaResult");
		assertThat(nl6.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("Id", ComparisonOperator.EQUAL, "${Id}"));
		assertThat(nl6.getContextState()).isNull();
		assertThat(nl6.isVirtual()).isFalse();

		NodeLevel nl7 = np.getNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl6.getChild(), "Id", "Name"));
		assertThat(nl7.getEntityType().getName()).isEqualTo("SubMatrix");
		assertThat(nl7.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("Id", ComparisonOperator.EQUAL, "${Id}"));
		assertThat(nl7.getContextState()).isNull();
		assertThat(nl7.isVirtual()).isFalse();

		NodeLevel nl8 = np.getNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl7.getChild(), "Id", "Name"));
		assertThat(nl8.getEntityType().getName()).isEqualTo("MeaQuantity");
		assertThat(nl8.getFilterAttributes())
				.extracting(fa -> fa.getAttribute().getName(), FilterAttribute::getOperator, FilterAttribute::getValue)
				.containsExactly(Tuple.tuple("Id", ComparisonOperator.EQUAL, "${Id}"));
		assertThat(nl8.getContextState()).isNull();
		assertThat(nl8.isVirtual()).isFalse();
	}

//	@Test
//	public void testGetNodeLevelInvalid() throws ConnectionException {
//		NodeProviderRoot np = getNodeProviderRoot(context, createStructure(context, ContextState.MEASURED));
//
//		assertThat(np.getNodeLevel(context, "InvalidType", null)).isNull();
//	}

	@Test
	public void testGetParentNodeLevel() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure(context, ContextState.MEASURED));

		NodeLevel nl1 = np.getNodeLevel(context, 1);
		NodeLevel nl2 = np.getNodeLevel(context, 2);
		NodeLevel nl3 = np.getNodeLevel(context, 3);
		NodeLevel nl4 = np.getNodeLevel(context, 4);

		assertThat(np.getParentNodeLevel(context, nl1)).isNull();
		assertThat(np.getParentNodeLevel(context, nl2)).isEqualTo(nl1);
		assertThat(np.getParentNodeLevel(context, nl3)).isEqualTo(nl2);
		assertThat(np.getParentNodeLevel(context, nl4)).isEqualTo(nl3);
	}

	@Test
	public void testGetChildNodeLevel() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure(context, ContextState.MEASURED));

		NodeLevel nl1 = np.getNodeLevel(context, 1);
		NodeLevel nl2 = np.getNodeLevel(context, 2);
		NodeLevel nl3 = np.getNodeLevel(context, 3);
		NodeLevel nl4 = np.getNodeLevel(context, 4);

		assertThat(np.getChildNodeLevel(context, null)).contains(nl1);
		assertThat(np.getChildNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl1, "Id", "Name")))
				.contains(nl2);
		assertThat(np.getChildNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl2, "Id", "Name")))
				.contains(nl3);
		assertThat(np.getChildNodeLevel(context, SerializationUtil.createNode("NVHDEMO", nl3, "model", "model")))
				.contains(nl4);
	}

	@Test
	public void testPrecision() throws ConnectionException {
		NodeProviderRoot np = getNodeProviderRoot(context, createStructure2(context, ContextState.MEASURED));

		NodeLevel envLevel = np.getNodeLevel(context, 1);
		NodeLevel projectLevel = np.getNodeLevel(context, 2);
		NodeLevel measurementYearLevel = np.getNodeLevel(context, 3);
		NodeLevel testLevel = np.getNodeLevel(context, 4);
		NodeLevel measurementDayLevel = np.getNodeLevel(context, 5);
		NodeLevel measurementLevel = np.getNodeLevel(context, 6);
		NodeLevel channelGroupLevel = np.getNodeLevel(context, 7);
		NodeLevel channelLevel = np.getNodeLevel(context, 8);

		assertThat(envLevel.getChild()).isEqualTo(projectLevel);
		assertThat(projectLevel.getChild()).isEqualTo(measurementYearLevel);
		assertThat(measurementYearLevel.getChild()).isEqualTo(testLevel);
		assertThat(testLevel.getChild()).isEqualTo(measurementDayLevel);
		assertThat(measurementDayLevel.getChild()).isEqualTo(measurementLevel);
		assertThat(measurementLevel.getChild()).isEqualTo(channelGroupLevel);
		assertThat(channelGroupLevel.getChild()).isEqualTo(channelLevel);

		assertThat(np.getChildNodeLevel(context, null)).contains(envLevel);

		assertThat(np.getChildNodeLevel(context,
				SerializationUtil.createNode("NVHDEMO", "Environment", "1", "Id", "", "Name"))).contains(projectLevel);

		assertThat(np.getChildNodeLevel(context, SerializationUtil.createNode("NVHDEMO", projectLevel, "Id", "Name")))
				.contains(measurementYearLevel);

		assertThat(np.getChildNodeLevel(context,
				SerializationUtil.createNode("NVHDEMO", measurementYearLevel, "DateCreated", "DateCreated")))
						.contains(testLevel);

		assertThat(np.getChildNodeLevel(context, SerializationUtil.createNode("NVHDEMO", testLevel, "Id", "Name")))
				.contains(measurementDayLevel);

		assertThat(np.getChildNodeLevel(context,
				SerializationUtil.createNode("NVHDEMO", measurementDayLevel, "Id", "Name"))).contains(measurementLevel);

		assertThat(
				np.getChildNodeLevel(context, SerializationUtil.createNode("NVHDEMO", measurementLevel, "Id", "Name")))
						.contains(channelGroupLevel);

		assertThat(
				np.getChildNodeLevel(context, SerializationUtil.createNode("NVHDEMO", channelGroupLevel, "Id", "Name")))
						.contains(channelLevel);
	}

	/**
	 * Nodeprovider Definition with the following structure:
	 * 
	 * Environment
	 * 
	 * +Project
	 * 
	 * ++Pool
	 * 
	 * +++Test
	 *
	 * ++++TestStep
	 * 
	 * +++++Measurement
	 * 
	 * ++++++ChannelGroup
	 * 
	 * +++++++Channel
	 * 
	 * @param context
	 * @param contextState
	 * @return
	 */
	private static NodeLevel createStructure(ApplicationContext context, ContextState contextState) {
		ModelManager modelManager = context.getModelManager().get();

		NodeLevel env = new NodeLevel(modelManager.getEntityType(Environment.class));
		NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));
		EntityType vehicle = modelManager.getEntityType("vehicle");

		NodeLevel vehicleModel = new NodeLevel(vehicle, vehicle.getAttribute("model"));
		vehicleModel.setVirtual(true);
		vehicleModel.setContextState(contextState);
		NodeLevel test = new NodeLevel(modelManager.getEntityType(org.eclipse.mdm.api.base.model.Test.class));

		NodeLevel testStep = new NodeLevel(modelManager.getEntityType(TestStep.class));
		NodeLevel measurement = new NodeLevel(modelManager.getEntityType(Measurement.class));
		NodeLevel channelGroup = new NodeLevel(modelManager.getEntityType(ChannelGroup.class));
		NodeLevel channel = new NodeLevel(modelManager.getEntityType(Channel.class));

		env.setChild(project);
		project.setChild(vehicleModel);
		vehicleModel.setChild(test);
		test.setChild(testStep);
		testStep.setChild(measurement);
		measurement.setChild(channelGroup);
		channelGroup.setChild(channel);

		return env;
	}

	/**
	 * Nodeprovider Definition with the following structure:
	 * 
	 * Environment
	 * 
	 * +Project
	 * 
	 * ++[YEAR]
	 *
	 * +++Test
	 * 
	 * ++++[DAY]
	 * 
	 * +++++Measurement
	 * 
	 * ++++++ChannelGroup
	 * 
	 * +++++++Channel
	 * 
	 * @param context
	 * @param contextState
	 * @return
	 */
	private static NodeLevel createStructure2(ApplicationContext context, ContextState contextState) {
		ModelManager modelManager = context.getModelManager().get();

		EntityType etMeasurement = modelManager.getEntityType(Measurement.class);

		NodeLevel env = new NodeLevel(modelManager.getEntityType(Environment.class));
		NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));
		NodeLevel test = new NodeLevel(modelManager.getEntityType(org.eclipse.mdm.api.base.model.Test.class));

		NodeLevel measurementYear = new NodeLevel(etMeasurement, etMeasurement.getAttribute("DateCreated"));
		measurementYear.setVirtual(true);
		measurementYear.setValuePrecision(ValuePrecision.YEAR);
		measurementYear.setContextState(contextState);

		NodeLevel measurementDay = new NodeLevel(etMeasurement, etMeasurement.getAttribute("DateCreated"));
		measurementDay.setVirtual(true);
		measurementDay.setValuePrecision(ValuePrecision.DAY);
		measurementDay.setContextState(contextState);

		NodeLevel measurement = new NodeLevel(etMeasurement);
		NodeLevel channelGroup = new NodeLevel(modelManager.getEntityType(ChannelGroup.class));
		NodeLevel channel = new NodeLevel(modelManager.getEntityType(Channel.class));

		env.setChild(project);
		project.setChild(measurementYear);
		measurementYear.setChild(test);
		test.setChild(measurementDay);
		measurementDay.setChild(measurement);
		measurement.setChild(channelGroup);
		channelGroup.setChild(channel);

		return env;
	}
}
