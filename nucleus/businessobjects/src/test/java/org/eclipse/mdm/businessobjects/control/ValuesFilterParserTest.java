/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.control;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Collections;

import org.eclipse.mdm.api.base.massdata.ValuesFilter.ValuesCondition;
import org.eclipse.mdm.api.base.massdata.ValuesFilter.ValuesConjunction;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.junit.jupiter.api.Test;

public class ValuesFilterParserTest {

	@Test
	void testDecimalValue() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 eq 3.2")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.EQUAL, "3.2"));
	}

	@Test
	void testLongValue() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 eq 3")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.EQUAL, "3"));
	}

	@Test
	void testBooleanValue() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 eq true")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.EQUAL, "true"));
	}

	@Test
	void testStringValue() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 eq 'five'")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.EQUAL, "five"));
	}

	@Test
	void testWronglyQuotedValueThrowsException() {
		assertThatThrownBy(() -> ValuesFilterParser.parseFilterString("MyChannel2 eq \"five\"")).hasMessage(
				"Could not parse filter string 'MyChannel2 eq \"five\"'. Error: line 1:14 mismatched input '\"five\"' expecting {DECIMAL, LONG, BOOL, STRINGLITERAL}");
	}

	@Test
	void testBetween() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 bw 3 and 5")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.BETWEEN, Arrays.asList("3", "5")));
	}

	@Test
	void testInSet() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 in (3, 5)")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.IN_SET, Arrays.asList("3", "5")));
	}

	@Test
	void testNotInSet() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 not_in (3, 5)")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.NOT_IN_SET, Arrays.asList("3", "5")));
	}

	@Test
	void testIsNull() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 is_null")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.IS_NULL, Collections.emptyList()));
	}

	@Test
	void testIsNotNull() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 is_not_null")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.IS_NOT_NULL, Collections.emptyList()));
	}

	@Test
	void testLike() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 lk 'My*2'")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.LIKE, Arrays.asList("My*2")));
	}

	@Test
	void testILike() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel2 ci_lk 'my*2'")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel2", ComparisonOperator.CASE_INSENSITIVE_LIKE,
						Arrays.asList("my*2")));
	}

	@Test
	void testConjuctions() {
		assertThat(ValuesFilterParser
				.parseFilterString("MyChannel2 eq 3 and (MyChannel3 eq 'five' or MyChannel3 eq 'three')"))
						.usingRecursiveComparison().isEqualTo(
								ValuesConjunction.and(new ValuesCondition("MyChannel2", ComparisonOperator.EQUAL, "3"),
										ValuesConjunction.or(
												new ValuesCondition("MyChannel3", ComparisonOperator.EQUAL, "five"),
												new ValuesCondition("MyChannel3", ComparisonOperator.EQUAL, "three"))));
	}

	@Test
	void testChannelNamesWithSpaces() {
		assertThat(ValuesFilterParser.parseFilterString("\"MyChannel 2\" eq 3")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel 2", ComparisonOperator.EQUAL, "3"));

		assertThat(ValuesFilterParser.parseFilterString("\"My Channel  2[   ]\" eq 3")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("My Channel  2[   ]", ComparisonOperator.EQUAL, "3"));
	}

	@Test
	void testChannelNamesWithSpacesAndQuotes() {
		assertThat(ValuesFilterParser.parseFilterString("\"MyChannel \\\"2\\\"\" eq 3")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel \"2\"", ComparisonOperator.EQUAL, "3"));
	}

	@Test
	void testChannelNameWithSingleQuotes() {
		assertThat(ValuesFilterParser.parseFilterString("\"MyChannel '2'\" eq 3")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel '2'", ComparisonOperator.EQUAL, "3"));
	}

	@Test
	void testChannelNameWithUnderscore() {
		assertThat(ValuesFilterParser.parseFilterString("MyChannel_2 eq 3")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel_2", ComparisonOperator.EQUAL, "3"));
	}

	@Test
	void testChannelNameWithStar() {
		assertThat(ValuesFilterParser.parseFilterString("\"MyChannel*2\" eq 3")).usingRecursiveComparison()
				.isEqualTo(new ValuesCondition("MyChannel*2", ComparisonOperator.EQUAL, "3"));

		assertThatThrownBy(() -> ValuesFilterParser.parseFilterString("MyChannel*2 eq 3")).hasMessage(
				"Could not parse filter string 'MyChannel*2 eq 3'. Error: line 1:9 token recognition error at: '*'");
	}
}
