/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.mdm.nodeprovider.control;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import javax.el.ValueExpression;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.nodeprovider.entity.AttributeContainerList;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class MDMExpressionLanguageServiceTest {

	MDMExpressionLanguageService elService = new MDMExpressionLanguageService();

	@Test
	public void testFormatDate() {
		Attribute attribute = Mockito.mock(Attribute.class);
		Mockito.when(attribute.getName()).thenReturn("DateCreated");
		Mockito.when(attribute.getValueType()).thenReturn(ValueType.DATE);

		ValueExpression expr = elService
				.parseValueExpression("${mdm:formatDate(DateCreated, \"YYYY 'KW' ww\", 'undefined')}", String.class);

		AttributeContainer validDate = new AttributeContainer(ContextState.MEASURED, attribute, true,
				Instant.parse("1970-01-01T00:00:00Z"));

		assertThat(elService.evaluateValueExpression(expr, AttributeContainerList.of(validDate)))
				.isEqualTo("1970 KW 01");

		AttributeContainer invalidDate = new AttributeContainer(ContextState.MEASURED, attribute, false,
				Instant.parse("1970-01-01T00:00:00Z"));

		assertThat(elService.evaluateValueExpression(expr, AttributeContainerList.of(invalidDate)))
				.isEqualTo("undefined");
	}

	@Test
	public void testEvaluateInt() {
		Attribute attribute = Mockito.mock(Attribute.class);
		Mockito.when(attribute.getName()).thenReturn("index");
		Mockito.when(attribute.getValueType()).thenReturn(ValueType.LONG);

		ValueExpression expr = elService.parseValueExpression("${index}", Integer.class);

		AttributeContainer validDate = new AttributeContainer(ContextState.MEASURED, attribute, true, 42);

		assertThat(elService.evaluateValueExpression(expr, AttributeContainerList.of(validDate))).isEqualTo(42);
	}
}
