/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.boundary.integrationtest;

import org.eclipse.mdm.businessobjects.boundary.ResourceConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Test class for {@MeasurementResource}.
 * 
 * @author Johannes Stamm, Peak Solution GmbH Nuernberg
 * @see EntityResourceIntegrationTest
 *
 */
public class MeasurementResourceIntegrationTest extends EntityResourceIntegrationTest {
	@BeforeClass
	public static void prepareTestData() {
		getLogger().debug("Preparing MeasurementResourceIntegrationTest");

		// prepare test data for creating the parent TestStep
		TestStepResourceIntegrationTest.prepareTestData();
		TestStepResourceIntegrationTest.createEntity();

		// set up test data
		setContextClass(MeasurementResourceIntegrationTest.class);

		putTestDataValue(TESTDATA_RESOURCE_URI, "/measurements");
		putTestDataValue(TESTDATA_ENTITY_NAME, "testMeasurement");
		putTestDataValue(TESTDATA_ENTITY_TYPE, "Measurement");

		JsonObject json = new JsonObject();
		json.add(ResourceConstants.ENTITYATTRIBUTE_NAME, new JsonPrimitive(getTestDataValue(TESTDATA_ENTITY_NAME)));
		json.add("TestStep",
				new JsonPrimitive(getTestDataValue(TestStepResourceIntegrationTest.class, TESTDATA_ENTITY_ID)));

		putTestDataValue(TESTDATA_CREATE_JSON_BODY, json.toString());
	}

	@AfterClass
	public static void tearDownAfterClass() {
		setContextClass(TestStepResourceIntegrationTest.class);
		TestStepResourceIntegrationTest.tearDownAfterClass();
	}
}
