/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.boundary.integrationtest;

import org.eclipse.mdm.businessobjects.boundary.ResourceConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Test class for {@TestStepResource}.
 * 
 * @author Johannes Stamm, Peak Solution GmbH Nuernberg
 * @see EntityResourceIntegrationTest
 *
 */
public class TestStepResourceIntegrationTest extends EntityResourceIntegrationTest {
	@BeforeClass
	public static void prepareTestData() {
		getLogger().debug("Preparing TestStepResourceIntegrationTest");

		// prepare test data for creating the parent Test
		TestResourceIntegrationTest.prepareTestData();
		TestResourceIntegrationTest.createEntity();

		// set up test data
		setContextClass(TestStepResourceIntegrationTest.class);

		putTestDataValue(TESTDATA_RESOURCE_URI, "/teststeps");
		putTestDataValue(TESTDATA_ENTITY_NAME, "testTestStep");
		putTestDataValue(TESTDATA_ENTITY_TYPE, "TestStep");

		// json object for teststep entity
		JsonObject json = new JsonObject();
		json.add(ResourceConstants.ENTITYATTRIBUTE_NAME, new JsonPrimitive(getTestDataValue(TESTDATA_ENTITY_NAME)));
		json.add("Test", new JsonPrimitive(getTestDataValue(TestResourceIntegrationTest.class, TESTDATA_ENTITY_ID)));

		putTestDataValue(TESTDATA_CREATE_JSON_BODY, json.toString());
	}

	@AfterClass
	public static void tearDownAfterClass() {
		// call tearDownAfterClass() of parent entity
		setContextClass(TestResourceIntegrationTest.class);
		TestResourceIntegrationTest.tearDownAfterClass();
	}
}
