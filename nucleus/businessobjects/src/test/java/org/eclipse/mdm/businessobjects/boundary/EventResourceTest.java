package org.eclipse.mdm.businessobjects.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.notification.NotificationFilter.ModificationType;
import org.eclipse.mdm.businessobjects.entity.EventDataList;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.MDMNotificationFilter;
import org.eclipse.mdm.businessobjects.utils.JacksonConfig;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.google.common.collect.ImmutableSet;

@Testcontainers(disabledWithoutDocker = true)
class EventResourceTest {

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	@org.junit.jupiter.api.Test
	void testEventNotification(NewCookie sessionCookie) throws InterruptedException {
		WebTarget target = glassfish.getRoot().register(new JacksonConfig());

		MDMNotificationFilter filter = new MDMNotificationFilter();
		filter.setEntityTypes(ImmutableSet.of("Project"));
		filter.setTypes(ImmutableSet.copyOf(ModificationType.values()));

		Response r = target.path("mdm/events/MDM").path("myRef").request().cookie(sessionCookie)
				.post(Entity.entity(filter, MediaType.APPLICATION_JSON));
		try {
			assertThat(r.getStatus()).isEqualTo(Status.OK.getStatusCode());
			assertThat(r.readEntity(String.class)).isEmpty();
		} finally {
			r.close();
		}

		MDMEntityResponse project = target.path("mdm/environments/MDM/projects").request().cookie(sessionCookie).post(
				Entity.entity("{ \"Name\": \"MyNewProject\" }", MediaType.APPLICATION_JSON), MDMEntityResponse.class);

		assertThat(project.getType()).isEqualTo("Project");
		assertThat(project.getData()).hasSize(1);

		Thread.sleep(5000);

		EventDataList resp = target.path("mdm/events/MDM").path("myRef").request(MediaType.APPLICATION_JSON)
				.cookie(sessionCookie).get(EventDataList.class);

		assertThat(resp.getData()).hasSize(1).allSatisfy(eventData -> {
			assertThat(eventData.getReceived().atZone(ZoneId.systemDefault()))
					.isAfter(ZonedDateTime.now().minusSeconds(10)).isBefore(ZonedDateTime.now());
			assertThat(eventData.getType()).isEqualTo("instanceCreated");
			assertThat(eventData.getMdmEntity()).usingRecursiveComparison().isEqualTo(project);
		});
	}

}
