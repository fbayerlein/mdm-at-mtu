/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class MDMValueDeserializerTest {

	final ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void init() {
		final SimpleModule module = new SimpleModule();
		module.addDeserializer(Value.class, new MDMValueDeserializer());

		objectMapper.registerModule(module);
		objectMapper.setConfig(objectMapper.getDeserializationConfig().withAttribute("sourceName", "MDM"));
	}

	@Test
	public void testString() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myString\", \"dataType\": \"STRING\", \"unit\": \"m\", \"value\": \"test\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myString").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.STRING)
				.hasFieldOrPropertyWithValue("measured", "test");
	}

	@Test
	public void testStringSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myStrings\", \"dataType\": \"STRING_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"test1\", \"test2\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myStrings").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.STRING_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new String[] { "test1", "test2" });
	}

	@Test
	public void testDate() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myDate\", \"dataType\": \"DATE\", \"unit\": \"m\", \"value\": \"2018-01-02T12:13:14Z\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myDate").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.DATE)
				.hasFieldOrPropertyWithValue("measured", Instant.parse("2018-01-02T12:13:14Z"));
	}

	@Test
	public void testDateSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myDates\", \"dataType\": \"DATE_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"2018-01-02T12:13:14Z\", \"2019-12-11T13:03:59Z\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myDates").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.DATE_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured",
						new Instant[] { Instant.parse("2018-01-02T12:13:14Z"), Instant.parse("2019-12-11T13:03:59Z") });
	}

	@Test
	public void testBoolean() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myBoolean\", \"dataType\": \"BOOLEAN\", \"unit\": \"m\", \"value\": \"true\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myBoolean").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.BOOLEAN)
				.hasFieldOrPropertyWithValue("measured", true);
	}

	@Test
	public void testBooleanSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myBooleans\", \"dataType\": \"BOOLEAN_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"true\", \"false\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myBooleans").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.BOOLEAN_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new boolean[] { true, false });
	}

	@Test
	public void testByte() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myByte\", \"dataType\": \"BYTE\", \"unit\": \"m\", \"value\": \"127\" }", Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myByte").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.BYTE)
				.hasFieldOrPropertyWithValue("measured", (byte) 127);
	}

	@Test
	public void testByteSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myBytes\", \"dataType\": \"BYTE_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"127\", \"-56\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myBytes").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.BYTE_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new byte[] { (byte) 127, (byte) -56 });
	}

	@Test
	public void testShort() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myShort\", \"dataType\": \"SHORT\", \"unit\": \"m\", \"value\": \"123\" }", Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myShort").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.SHORT)
				.hasFieldOrPropertyWithValue("measured", (short) 123);
	}

	@Test
	public void testShortSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myShorts\", \"dataType\": \"SHORT_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"123\", \"32123\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myShorts").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.SHORT_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new short[] { (short) 123, (short) 32123 });
	}

	@Test
	public void testInteger() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myInteger\", \"dataType\": \"INTEGER\", \"unit\": \"m\", \"value\": \"123\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myInteger").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.INTEGER)
				.hasFieldOrPropertyWithValue("measured", 123);
	}

	@Test
	public void testIntegerSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myIntegers\", \"dataType\": \"INTEGER_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"123\", \"42\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myIntegers").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.INTEGER_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new int[] { 123, 42 });
	}

	@Test
	public void testLong() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myLong\", \"dataType\": \"LONG\", \"unit\": \"m\", \"value\": \"3123123123\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myLong").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.LONG)
				.hasFieldOrPropertyWithValue("measured", 3_123_123_123L);
	}

	@Test
	public void testLongSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myLongs\", \"dataType\": \"LONG_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"3123123123\", \"42\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myLongs").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.LONG_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new long[] { 3_123_123_123L, 42L });
	}

	@Test
	public void testFloat() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myFloat\", \"dataType\": \"FLOAT\", \"unit\": \"m\", \"value\": \"1.234\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myFloat").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.FLOAT)
				.hasFieldOrPropertyWithValue("measured", 1.234f);
	}

	@Test
	public void testFloatSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myFloats\", \"dataType\": \"FLOAT_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"1.234\", \"47.11\"] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myFloats").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.FLOAT_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new float[] { 1.234f, 47.11f });
	}

	@Test
	public void testDouble() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myDouble\", \"dataType\": \"DOUBLE\", \"unit\": \"m\", \"value\": \"1.23456789\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myDouble").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.DOUBLE)
				.hasFieldOrPropertyWithValue("measured", 1.23456789);
	}

	@Test
	public void testDoubleSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myDoubles\", \"dataType\": \"DOUBLE_SEQUENCE\", \"unit\": \"m\", \"value\": [ \"1.23456789\", \"15.08\" ]}",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myDoubles").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.DOUBLE_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured", new double[] { 1.23456789, 15.08 });
	}

	@Test
	public void testEnumeration() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myEnum\", \"dataType\": \"ENUMERATION\", \"enumerationName\": \"SequenceRepresentation\", \"unit\": \"m\", \"value\": \"EXPLICIT\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myEnum").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.ENUMERATION)
				.hasFieldOrPropertyWithValue("measured", SequenceRepresentation.EXPLICIT);
	}

	@Test
	public void testEnumerationFallback() throws Exception {
		// no enumerationName given -> fallback to enumerationName=ValueType
		Value value = objectMapper.readValue(
				"{ \"name\": \"myEnum\", \"dataType\": \"ENUMERATION\", \"unit\": \"m\", \"value\": \"LONG_SEQUENCE\" }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myEnum").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.ENUMERATION)
				.hasFieldOrPropertyWithValue("measured", ValueType.LONG_SEQUENCE);
	}

	@Test
	public void testEnumerationSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myEnums\", \"dataType\": \"ENUMERATION_SEQUENCE\", \"enumerationName\": \"ValueType\", \"unit\": \"m\", \"value\": [ \"LONG_SEQUENCE\", \"FILE_LINK\" ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myEnums").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.ENUMERATION_SEQUENCE).hasFieldOrPropertyWithValue(
						"measured", new EnumerationValue[] { ValueType.LONG_SEQUENCE, ValueType.FILE_LINK });
	}

	@Test
	public void testFileLink() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myFileLink\", \"dataType\": \"FILE_LINK\", \"unit\": \"m\", \"value\": {\"remotePath\": \"$(DISK1)myFile1.bin\", \"mimeType\": \"application/octet-stream\", \"description\": \"myDesc\", \"fileServiceType\": \"EXTREF\"} }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myFileLink").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.FILE_LINK)
				.hasFieldOrPropertyWithValue("measured", FileLink.newRemote("$(DISK1)myFile1.bin",
						new MimeType("application/octet-stream"), "myDesc", 0, value, FileServiceType.EXTREF));
	}

	@Test
	public void testFileLinkSequence() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myFileLinks\", \"dataType\": \"FILE_LINK_SEQUENCE\", \"unit\": \"m\", \"value\": [ "
						+ "{\"remotePath\": \"$(DISK1)myFile1.bin\", \"mimeType\": \"application/octet-stream\", \"description\": \"myDesc\", \"fileServiceType\": \"EXTREF\"}, "
						+ "{\"remotePath\": \"$(DISK1)test.txt\", \"mimeType\": \"text/plain\", \"description\": \"text file\", \"fileServiceType\": \"EXTREF\"} "
						+ " ] }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myFileLinks").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.FILE_LINK_SEQUENCE)
				.hasFieldOrPropertyWithValue("measured",
						new FileLink[] {
								FileLink.newRemote("$(DISK1)myFile1.bin", new MimeType("application/octet-stream"),
										"myDesc", 0, value, FileServiceType.EXTREF),
								FileLink.newRemote("$(DISK1)test.txt", new MimeType("text/plain"), "text file", 0,
										value, FileServiceType.EXTREF) });
	}

	@Test
	@Ignore // TODO
	public void testFileRelation() throws Exception {

		Value value = objectMapper.readValue(
				"{ \"name\": \"myFileLink\", \"dataType\": \"FILE_RELATION\", \"unit\": \"m\", \"value\": {\"remotePath\": \"$(DISK1)myFile1.bin\", \"mimeType\": \"application/octet-stream\", \"description\": \"myDesc\", \"fileServiceType\": \"EXTREF\"} }",
				Value.class);

		assertThat(value).hasFieldOrPropertyWithValue("name", "myFileLink").hasFieldOrPropertyWithValue("unit", "m")
				.hasFieldOrPropertyWithValue("valueType", ValueType.FILE_RELATION)
				.hasFieldOrPropertyWithValue("measured", FileLink.newRemote("$(DISK1)myFile1.bin",
						new MimeType("application/octet-stream"), "myDesc", 0, value, FileServiceType.EXTREF));
	}
}
