/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
//@TestInstance(Lifecycle.PER_CLASS)
public class TestStepResourceUpdateTemplateTest {

	private static final Log LOG = LogFactory.getLog(TestStepResourceUpdateTemplateTest.class);

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	public static ApplicationContext context;

	public static TemplateTest templateTest;

	public static TemplateTestStep templateTestStepFull;
	public static TemplateTestStep templateTestStepVehicleOneAttr;

	public static Test myTest;

	@BeforeAll
	public static void setup() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		CatalogAttribute caVin = ef.createCatalogAttribute("vin", ValueType.STRING, ccVehicle);
		ef.createCatalogAttribute("model", ValueType.STRING, ccVehicle);

		CatalogComponent ccEngine = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "engine");
		ef.createCatalogAttribute("power", ValueType.INTEGER, ccEngine);

		TemplateRoot trUutFull = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutFull");
		ef.createTemplateComponent("uutFullVehicle", trUutFull, ccVehicle, true);
		ef.createTemplateComponent("uutFullEngine", trUutFull, ccEngine, true);

		TemplateRoot trUutVehicleOneAttr = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uutVehicleOneAttr");
		TemplateComponent tplCompVehicleOneAttr = ef.createTemplateComponent("uutVehicleOneAttrVehicle",
				trUutVehicleOneAttr, ccVehicle, false);
		ef.createTemplateAttribute(caVin, tplCompVehicleOneAttr);

		templateTestStepFull = ef.createTemplateTestStep("TplFull");
		templateTestStepFull.setTemplateRoot(trUutFull);

		templateTestStepVehicleOneAttr = ef.createTemplateTestStep("TplVehicleOneAttr");
		templateTestStepVehicleOneAttr.setTemplateRoot(trUutVehicleOneAttr);

		templateTest = ef.createTemplateTest("TplTest");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepFull);
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStepVehicleOneAttr);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle, ccEngine));
			t.create(Arrays.asList(trUutFull, trUutVehicleOneAttr));
			t.create(Arrays.asList(templateTestStepFull, templateTestStepVehicleOneAttr));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);

		myTest = ef.createTest("MyTest", pool, templateTest, false);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");
		context.close();
	}

	@org.junit.jupiter.api.Test
	public void testSetTemplateUsingRestApi(NewCookie sessionCookie) throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		// create new TestStep with template TplVehicleOneAttr
		TestStep testStep = ef.createTestStep("testSetTemplateUsingRestApi", myTest, templateTestStepVehicleOneAttr);
		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(testStep));
			t.commit();
		}
		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplVehicleOneAttr");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot.getContextComponents().size()).isEqualTo(1);
			Optional<ContextComponent> engineContextComponent = uutContextRoot
					.getContextComponent("uutVehicleOneAttrVehicle");
			assertThat(engineContextComponent.isPresent()).isTrue();
			final Map<String, Value> attributes = engineContextComponent.get().getValues();
			assertThat(attributes.containsKey("vin")).isTrue();
			assertThat(attributes.containsKey("model")).isFalse();
		}

		{
			WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);
			final String updateTemplate = "{\"TemplateTestStep\": " + templateTestStepFull.getID() + " }";
			final String updateUrl = "mdm/environments/MDM/teststeps/" + testStep.getID();
			LOG.info("PUT '" + updateTemplate + "' to '" + updateUrl + "'");
			final MDMEntityResponse response = target.path(updateUrl).request().cookie(sessionCookie)
					.put(Entity.entity(updateTemplate, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);
			LOG.info("PUT finished");
			assertThat(response.getType()).isEqualTo("TestStep");
			assertThat(response.getData().size()).isEqualTo(1);
		}

		{
			final TestStep testStep_loaded = em.load(TestStep.class, testStep.getID());
			assertThat(testStep_loaded).extracting(ts -> TemplateTestStep.of(ts).get())
					.extracting(TemplateTestStep::getName).isEqualTo("TplFull");
			final Map<ContextType, ContextRoot> context_loaded = testStep_loaded.loadContexts(em,
					ContextType.UNITUNDERTEST);
			assertThat(context_loaded.containsKey(ContextType.UNITUNDERTEST)).isTrue();
			final ContextRoot uutContextRoot = context_loaded.get(ContextType.UNITUNDERTEST);
			assertThat(uutContextRoot.getContextComponents().size()).isEqualTo(2);
			Optional<ContextComponent> vehicleContextComponent = uutContextRoot.getContextComponent("uutFullVehicle");
			assertThat(vehicleContextComponent.isPresent()).isTrue();
			final Map<String, Value> vehicleAttributes = vehicleContextComponent.get().getValues();
			assertThat(vehicleAttributes.containsKey("vin")).isTrue();
			assertThat(vehicleAttributes.containsKey("model")).isTrue();
			Optional<ContextComponent> engineContextComponent = uutContextRoot.getContextComponent("uutFullEngine");
			assertThat(engineContextComponent.isPresent()).isTrue();
			final Map<String, Value> engineAttributes = engineContextComponent.get().getValues();
			assertThat(engineAttributes.containsKey("power")).isTrue();
		}

		context.close();
	}
}
