/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.openapi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import org.eclipse.mdm.openapi.ProtobufFileDescriptorModelResolver;
import org.eclipse.mdm.protobuf.Mdm;
import org.eclipse.mdm.protobuf.Mdm.MeasuredValuesList;
import org.eclipse.mdm.protobuf.Mdm.PreviewRequest;
import org.eclipse.mdm.protobuf.Mdm.PreviewValuesList;
import org.eclipse.mdm.protobuf.Mdm.ReadRequest;
import org.eclipse.mdm.protobuf.Mdm.UpdateRequest;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.converter.ModelConverterContextImpl;
import io.swagger.v3.oas.models.media.Schema;

public class ProtobufFileDescriptorModelResolverTest {

	@Test
	public void testResolveEnum() throws IOException {
		ProtobufFileDescriptorModelResolver p = new ProtobufFileDescriptorModelResolver();

		ModelConverterContext context = new ModelConverterContextImpl(p);

		Schema<?> schema = p.resolve(new AnnotatedType(Mdm.AxisType.class), context, Collections.emptyIterator());

		assertThat(schema.getName()).isEqualTo("AxisType");
		assertThat(schema.getOneOf()).hasSize(3).extracting(s -> s.getTitle()).contains("X_AXIS", "Y_AXIS", "XY_AXIS");
	}

	@Test
	public void testResolveType() throws IOException {
		ProtobufFileDescriptorModelResolver p = new ProtobufFileDescriptorModelResolver();

		ModelConverterContext context = new ModelConverterContextImpl(p);

		Schema<?> schema = p.resolve(new AnnotatedType(Mdm.ReadRequest.class), context, Collections.emptyIterator());

		assertThat(schema.getName()).isEqualTo("ReadRequest");
		assertThat(schema.getProperties()).containsKeys("channel_group_id", "channel_ids", "request_size",
				"start_index", "unit_ids", "values_mode");

	}

	@Test
	public void testResolvesString() throws IOException {
		ProtobufFileDescriptorModelResolver p = new ProtobufFileDescriptorModelResolver();

		ModelConverterContext context = new ModelConverterContextImpl(p);

		Schema<?> stringSchema = new Schema<>();
		stringSchema.setType("String");
		ModelConverter mockedModelConverter = Mockito.mock(ModelConverter.class);
		Mockito.when(mockedModelConverter.resolve(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(stringSchema);

		AnnotatedType type = new AnnotatedType(String.class);
		Iterator<ModelConverter> iterator = Arrays.asList(mockedModelConverter).iterator();

		Schema<?> schema = p.resolve(type, context, iterator);

		assertThat(schema.getType()).isEqualTo("String");
		verify(mockedModelConverter).resolve(Mockito.eq(type), Mockito.eq(context), Mockito.eq(iterator));
	}

	@ParameterizedTest
	@ValueSource(classes = { ReadRequest.class, MeasuredValuesList.class, WriteRequestList.class, PreviewRequest.class,
			UpdateRequest.class, PreviewValuesList.class })
	public void testAll(Class<?> clazz) throws IOException {
		ProtobufFileDescriptorModelResolver p = new ProtobufFileDescriptorModelResolver();

		ModelConverterContext context = new ModelConverterContextImpl(p);

		assertThat(p.resolve(new AnnotatedType(clazz), context, Collections.emptyIterator())).isNotNull();
	}
}
