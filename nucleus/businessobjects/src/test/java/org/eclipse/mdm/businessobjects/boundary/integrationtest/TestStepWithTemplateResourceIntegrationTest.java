/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.boundary.integrationtest;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Test class for {@TestStepResource}.
 * 
 * @author Johannes Stamm, Peak Solution GmbH Nuernberg
 * @see EntityResourceIntegrationTest
 *
 */
public class TestStepWithTemplateResourceIntegrationTest extends EntityResourceIntegrationTest {
	@BeforeClass
	public static void prepareTestData() {
		getLogger().debug("Preparing TestStepWithTemplateResourceIntegrationTest");

		// prepare test data for creating the parent {@Test}
		TestWithTemplateResourceIntegrationTest.prepareTestData();
		TestWithTemplateResourceIntegrationTest.createEntity();

		// prepare test data for creating the TestStep
		TemplateTestStepResourceIntegrationTest.prepareTestData();
		TemplateTestStepResourceIntegrationTest.createEntity();
		TemplateTestStepResourceIntegrationTest.updateEntity();

		// prepare test data for creating the linked templates
		TemplateTestStepUsageResourceIntegrationTest.prepareTestData();
		TemplateTestStepUsageResourceIntegrationTest.createEntity();

		// set up test data
		setContextClass(TestStepWithTemplateResourceIntegrationTest.class);

		putTestDataValue(TESTDATA_RESOURCE_URI, "/teststeps");
		putTestDataValue(TESTDATA_ENTITY_NAME, "testTestStep");
		putTestDataValue(TESTDATA_ENTITY_TYPE, "TestStep");

		// json object for teststep entity
		JsonObject json = new JsonObject();
		json.add("Test",
				new JsonPrimitive(getTestDataValue(TestWithTemplateResourceIntegrationTest.class, TESTDATA_ENTITY_ID)));
		json.add("TemplateTestStep",
				new JsonPrimitive(getTestDataValue(TemplateTestStepResourceIntegrationTest.class, TESTDATA_ENTITY_ID)));

		putTestDataValue(TESTDATA_CREATE_JSON_BODY, json.toString());
	}

	@AfterClass
	public static void tearDownAfterClass() {
		// call tearDownAfterClass() of parent entity (without template)
		setContextClass(TestResourceIntegrationTest.class);
		TestResourceIntegrationTest.tearDownAfterClass();

		// delete Templates
		setContextClass(TemplateTestStepUsageResourceIntegrationTest.class);
		TemplateTestStepUsageResourceIntegrationTest.tearDownAfterClass();
	}
}
