/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.businessobjects.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class ServiceUtilsTest {

	@Test
	public void testWorkaroundForTypeMapping() {
		assertThat(ServiceUtils.workaroundForTypeMapping("asdf")).isEqualTo("asdf");
		assertThat(ServiceUtils.workaroundForTypeMapping("Project")).isEqualTo("Project");

		assertThat(ServiceUtils.workaroundForTypeMapping("StructureLevel")).isEqualTo("Pool");
		assertThat(ServiceUtils.workaroundForTypeMapping("MeaResult")).isEqualTo("Measurement");
		assertThat(ServiceUtils.workaroundForTypeMapping("SubMatrix")).isEqualTo("ChannelGroup");
		assertThat(ServiceUtils.workaroundForTypeMapping("MeaQuantity")).isEqualTo("Channel");
	}

	@Test
	public void testInvertMapping() {
		assertThat(ServiceUtils.invertMapping("asdf")).isEqualTo("asdf");
		assertThat(ServiceUtils.invertMapping("Project")).isEqualTo("Project");

		assertThat(ServiceUtils.invertMapping("Pool")).isEqualTo("StructureLevel");
		assertThat(ServiceUtils.invertMapping("Measurement")).isEqualTo("MeaResult");
		assertThat(ServiceUtils.invertMapping("ChannelGroup")).isEqualTo("SubMatrix");
		assertThat(ServiceUtils.invertMapping("Channel")).isEqualTo("MeaQuantity");
	}

}
