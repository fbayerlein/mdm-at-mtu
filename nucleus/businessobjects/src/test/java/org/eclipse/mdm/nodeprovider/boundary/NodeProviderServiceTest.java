/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.nodeprovider.control.NodeProviderRepository;
import org.eclipse.mdm.nodeprovider.entity.NodeLevel;
import org.eclipse.mdm.nodeprovider.entity.NodeProvider;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderRoot;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.preferences.controller.PreferenceService;
import org.eclipse.mdm.preferences.entity.PreferenceMessage;
import org.eclipse.mdm.preferences.entity.PreferenceMessage.Scope;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.glassfish.security.common.PrincipalImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.google.common.collect.ImmutableMap;

@Testcontainers(disabledWithoutDocker = true)
public class NodeProviderServiceTest {

	@Container
	public static OdsServerContainer odsServer1 = OdsServerContainer.create();

	@Container
	public static OdsServerContainer odsServer2 = OdsServerContainer.create();

	private static ApplicationContext context1;
	private static ApplicationContext context2;

	@BeforeAll
	public static void setUpBeforeClass() throws ConnectionException {
		setupServer1();
		setupServer2();
	}

	static void setupServer1() throws ConnectionException {
		context1 = new ODSHttpContextFactory().connect("MDM1", odsServer1.getConnectionParameters());

		EntityFactory ef = context1.getEntityFactory().get();
		EntityManager em = context1.getEntityManager().get();

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		ef.createCatalogAttribute("model", ValueType.STRING, ccVehicle);
		ef.createCatalogAttribute("vin", ValueType.STRING, ccVehicle);

		TemplateRoot trUut = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uut");
		ef.createTemplateComponent("vehicle", trUut, ccVehicle, true);

		TemplateTestStep templateTestStep = ef.createTemplateTestStep("MyTemplateTestStep");
		templateTestStep.setTemplateRoot(trUut);

		TemplateTest templateTest = ef.createTemplateTest("TplTest1");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle));
			t.create(Arrays.asList(trUut));
			t.create(Arrays.asList(templateTestStep));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project1 = ef.createProject("PMV Summit");
		Project project2 = ef.createProject("PMV Model P");
		Project project3 = ef.createProject("PMV 2PV");

		Pool pool = ef.createPool("MyPool1", project1);
		org.eclipse.mdm.api.base.model.Test test = ef.createTest("MyTest1", pool, templateTest, false);
		TestStep testStep = ef.createTestStep("MyTestStep1", test, templateTestStep);
		Measurement measurement = ef.createMeasurement("MyMeasurement1", testStep);
		measurement.setDateCreated(Instant.parse("2024-02-13T12:13:14Z"));

		ContextComponent measuredVehicle = measurement.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		measuredVehicle.getValue("model").set("Model P");
		measuredVehicle.getValue("vin").set("1234");

		Pool pool2 = ef.createPool("MyPool2", project1);
		org.eclipse.mdm.api.base.model.Test test2 = ef.createTest("MyTest2", pool2, templateTest, false);
		TestStep testStep2 = ef.createTestStep("MyTestStep2", test2, templateTestStep);
		Measurement measurement2 = ef.createMeasurement("MyMeasurement2", testStep2);
		measurement2.setDateCreated(Instant.parse("2024-02-13T12:13:14Z"));

		ContextComponent measuredVehicle2 = measurement2.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		measuredVehicle2.getValue("model").set("Summit");
		measuredVehicle2.getValue("vin").set("4711");

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project1, project2, project3));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");

		context1.close();
		// After modifying the model we have to reconnect
		context1 = new ODSHttpContextFactory().connect("MDM1", odsServer1.getConnectionParameters());
	}

	static void setupServer2() throws ConnectionException {
		context2 = new ODSHttpContextFactory().connect("MDM2", odsServer2.getConnectionParameters());

		EntityFactory ef = context2.getEntityFactory().get();
		EntityManager em = context2.getEntityManager().get();

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		ef.createCatalogAttribute("model", ValueType.STRING, ccVehicle);

		TemplateRoot trUut = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uut");
		ef.createTemplateComponent("vehicle", trUut, ccVehicle, true);

		TemplateTestStep templateTestStep = ef.createTemplateTestStep("MyTemplateTestStep");
		templateTestStep.setTemplateRoot(trUut);

		TemplateTest templateTest = ef.createTemplateTest("TplTest1");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle));
			t.create(Arrays.asList(trUut));
			t.create(Arrays.asList(templateTestStep));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project = ef.createProject("MyProject");

		Pool pool = ef.createPool("MyPool", project);
		org.eclipse.mdm.api.base.model.Test test = ef.createTest("MyTest", pool, templateTest, false);
		ef.createTestStep("MyTestStep", test, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");
	}

	@Test
	void testRoot() throws Exception {
		NodeProviderService npService = mockNodeProviderService();
		NodeProvider nodeProvider = npService.getNodeProvider("generic_measured");

		List<Node> roots = nodeProvider.getRoots();

		assertThat(roots).containsExactly(SerializationUtil.createNode("MDM1", "Environment", 1, "1", "", "MDM"),
				SerializationUtil.createNode("MDM2", "Environment", 1, "1", "", "MDM"));

		List<Node> level1 = nodeProvider
				.getChildren(roots.stream().filter(n -> "MDM".equals(n.getLabel())).findFirst().get());

		assertThat(level1).containsExactly(
				SerializationUtil.createNode("MDM1", "Project", 2, "3", "Project.Id eq \"3\"", "PMV 2PV 3"),
				SerializationUtil.createNode("MDM1", "Project", 2, "2", "Project.Id eq \"2\"", "PMV Model P 2"),
				SerializationUtil.createNode("MDM1", "Project", 2, "1", "Project.Id eq \"1\"", "PMV Summit 1"));

		List<Node> level2 = nodeProvider.getChildren(getNodeWithLabel(level1, "PMV Summit 1"));

		assertThat(level2).containsExactly(
				SerializationUtil.createNode("MDM1", "vehicle", 3, "",
						"Project.Id eq \"1\" and vehicle.model eq \"Model P\"", "Model: Model P"),
				SerializationUtil.createNode("MDM1", "vehicle", 3, "",
						"Project.Id eq \"1\" and vehicle.model eq \"Summit\"", "Model: Summit"));

		List<Node> level3a = nodeProvider.getChildren(getNodeWithLabel(level2, "Model: Summit"));

		assertThat(level3a).containsExactly(SerializationUtil.createNode("MDM1", "Measurement", 4, "",
				"Project.Id eq \"1\" and vehicle.model eq \"Summit\" and MEASURED.Measurement.DateCreated bw (\'2024-02-13T00:00:00Z\', \'2024-02-13T23:59:59.999999999Z\')",
				"2024 KW 07"));

		List<Node> level3b = nodeProvider.getChildren(getNodeWithLabel(level2, "Model: Model P"));

		assertThat(level3b).containsExactly(SerializationUtil.createNode("MDM1", "Measurement", 4, "",
				"Project.Id eq \"1\" and vehicle.model eq \"Model P\" and MEASURED.Measurement.DateCreated bw (\'2024-02-13T00:00:00Z\', \'2024-02-13T23:59:59.999999999Z\')",
				"2024 KW 07"));

		List<Node> level4a = nodeProvider.getChildren(getNodeWithLabel(level3a, "2024 KW 07"));

		assertThat(level4a).containsExactly(SerializationUtil.createNode("MDM1", "Test", 5, "2",
				"Project.Id eq \"1\" and vehicle.model eq \"Summit\" and MEASURED.Measurement.DateCreated bw (\'2024-02-13T00:00:00Z\', \'2024-02-13T23:59:59.999999999Z\') and Test.Id eq \"2\"",
				"MyTest2"));

		List<Node> level4b = nodeProvider.getChildren(getNodeWithLabel(level3b, "2024 KW 07"));

		assertThat(level4b).containsExactly(SerializationUtil.createNode("MDM1", "Test", 5, "1",
				"Project.Id eq \"1\" and vehicle.model eq \"Model P\" and MEASURED.Measurement.DateCreated bw (\'2024-02-13T00:00:00Z\', \'2024-02-13T23:59:59.999999999Z\') and Test.Id eq \"1\"",
				"MyTest1"));
	}

	/**
	 * NodeProvider defines two datasources MDM1 and MDM2 explicitly.
	 * ConnectorService only returns MDM1. Expected: Nodeprovider still works even,
	 * if MDM2 is not available.
	 * 
	 * @throws IOException
	 */
	@Test
	void testLazyLoading() throws IOException {
		ConnectorService connectorService = Mockito.mock(ConnectorService.class);
		Mockito.when(connectorService.getContexts()).thenReturn(Arrays.asList(context1));

		PreferenceMessage preferenceMessage = new PreferenceMessage();
		preferenceMessage.setId(1L);
		preferenceMessage.setKey("nodeprovider.test_lazy");
		preferenceMessage.setScope(Scope.SYSTEM);
		preferenceMessage.setValue(Files.readAllLines(Paths.get("src/test/resources/nodeprovider_testLazyLoading.json"))
				.stream().collect(Collectors.joining()));

		PreferenceService preferenceService = Mockito.mock(PreferenceService.class);
		Mockito.when(
				preferenceService.getPreferences(Mockito.eq("system"), Mockito.eq("nodeprovider."), Mockito.isNull()))
				.thenReturn(Arrays.asList(preferenceMessage));

		NodeProviderRepository nodeProviders = new NodeProviderRepository(connectorService, preferenceService,
				new PrincipalImpl("sa"));

		NodeProviderService repo = new NodeProviderService(nodeProviders);

		assertThat(repo.getNodeProviderIDs()).containsExactly("default", "test_lazy");
		NodeProvider np = repo.getNodeProvider("test_lazy");

		assertThat(np).isNotNull();

		List<Node> roots = np.getRoots();
		assertThat(roots).containsExactly(SerializationUtil.createNode("MDM1", "Environment", 1, "1", "", "MDM"));

	}

	private Node getNodeWithLabel(List<Node> nodes, String label) {
		return nodes.stream().filter(p -> label.equalsIgnoreCase(p.getLabel())).findAny().get();
	}

	@Test
	void testNodeProviderIsSerializable() {
		NodeLevel nl = createStructure(context1, ContextState.MEASURED);
		NodeProviderRoot npr = new NodeProviderRoot("generic_measured", "Measured");
		npr.setParsedContexts(ImmutableMap.of("*", nl));

		assertThat(SerializationUtil.serializeNodeProviderRoot(npr)).isNotNull();
	}

	private NodeLevel createStructure(ApplicationContext context, ContextState contextState) {
		ModelManager modelManager = context.getModelManager().get();

		NodeLevel env = new NodeLevel(modelManager.getEntityType(Environment.class));
		NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));
		EntityType vehicle = modelManager.getEntityType("vehicle");

		NodeLevel vehicleModel = new NodeLevel(vehicle, vehicle.getAttribute("model"));
		vehicleModel.setVirtual(true);
		vehicleModel.setContextState(contextState);
		NodeLevel test = new NodeLevel(modelManager.getEntityType(org.eclipse.mdm.api.base.model.Test.class));

		env.setChild(project);
		project.setChild(vehicleModel);
		vehicleModel.setChild(test);

		NodeLevel testStep = new NodeLevel(modelManager.getEntityType(TestStep.class));
		NodeLevel measurement = new NodeLevel(modelManager.getEntityType(Measurement.class));
		NodeLevel channelGroup = new NodeLevel(modelManager.getEntityType(ChannelGroup.class));
		NodeLevel channel = new NodeLevel(modelManager.getEntityType(Channel.class));

		test.setChild(testStep);
		testStep.setChild(measurement);
		measurement.setChild(channelGroup);
		channelGroup.setChild(channel);

		return env;
	}

	private static NodeProviderService mockNodeProviderService() throws Exception {
		ConnectorService connectorService = Mockito.mock(ConnectorService.class);
		Mockito.when(connectorService.getContexts()).thenReturn(Arrays.asList(context1, context2));

		PreferenceMessage preferenceMessage = new PreferenceMessage();
		preferenceMessage.setId(1L);
		preferenceMessage.setKey("nodeprovider.generic_measured");
		preferenceMessage.setScope(Scope.SYSTEM);
		preferenceMessage.setValue(Files.readAllLines(Paths.get("src/test/resources/nodeprovider_generic.json"))
				.stream().collect(Collectors.joining()));

		PreferenceService preferenceService = Mockito.mock(PreferenceService.class);
		Mockito.when(
				preferenceService.getPreferences(Mockito.eq("system"), Mockito.eq("nodeprovider."), Mockito.isNull()))
				.thenReturn(Arrays.asList(preferenceMessage));

		NodeProviderRepository nodeProviders = new NodeProviderRepository(connectorService, preferenceService,
				new PrincipalImpl("sa"));

		return new NodeProviderService(nodeProviders);
	}
}
