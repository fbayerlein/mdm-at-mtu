/*******************************************************************************
 *  Copyright (c) 2022 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.nodeprovider.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.atfxadapter.ATFXContextFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderRoot;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.collect.ImmutableMap;

public class SerializationUtilTest {

	private ApplicationContext context;

	private ConnectorService connectorService;

	@Before
	public void init() throws ConnectionException {
		Map<String, String> map = ImmutableMap.of("atfxfile", "../../api/atfxadapter/src/test/resources/Right_Acc.atfx",
				"freetext.active", "false");

		context = new ATFXContextFactory().connect("ATFX", map);

		connectorService = Mockito.mock(ConnectorService.class);
		Mockito.when(connectorService.getContexts()).thenReturn(Arrays.asList(context));
	}

	@After
	public void close() throws ConnectionException {
		context.close();
	}

	@Test
	public void test() {
		String json = new BufferedReader(new InputStreamReader(
				SerializationUtil.class.getResourceAsStream("/np_filter_attribute_test.json"), StandardCharsets.UTF_8))
						.lines().collect(Collectors.joining("\n"));

		NodeProviderRoot npr = SerializationUtil.deserializeNodeProviderRoot(connectorService, json);
		assertThat(npr.getNodeLevel(context, null)).isNotNull();
	}
}
