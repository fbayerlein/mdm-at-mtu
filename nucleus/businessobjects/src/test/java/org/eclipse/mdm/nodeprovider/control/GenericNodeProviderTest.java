/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.nodeprovider.control;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.eclipse.mdm.nodeprovider.utils.SerializationUtil.createNode;
import static org.mockito.ArgumentMatchers.any;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.model.VersionState;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.nodeprovider.entity.FilterAttribute;
import org.eclipse.mdm.nodeprovider.entity.NodeLevel;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderRoot;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.opentest4j.AssertionFailedError;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.google.common.collect.ImmutableMap;

@Testcontainers(disabledWithoutDocker = true)
class GenericNodeProviderTest {

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	static ApplicationContext context;

	@BeforeAll
	static void setUpBeforeClass() throws ConnectionException {
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();
		org.eclipse.mdm.api.base.model.Enumeration<?> enumVersionState = EnumRegistry.getInstance().get("MDM",
				EnumRegistry.VERSION_STATE);

		CatalogComponent ccVehicle = ef.createCatalogComponent(ContextType.UNITUNDERTEST, "vehicle");
		ef.createCatalogAttribute("vin", ValueType.STRING, ccVehicle);
		ef.createCatalogAttribute("model", ValueType.STRING, ccVehicle);
		ef.createCatalogAttribute("type", ValueType.ENUMERATION, enumVersionState, ccVehicle);

		TemplateRoot trUut = ef.createTemplateRoot(ContextType.UNITUNDERTEST, "uut");
		ef.createTemplateComponent("vehicle", trUut, ccVehicle, true);

		TemplateTestStep templateTestStep = ef.createTemplateTestStep("MyTemplateTestStep");
		templateTestStep.setTemplateRoot(trUut);

		TemplateTest templateTest = ef.createTemplateTest("TplTest1");
		ef.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, templateTestStep);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(ccVehicle));
			t.create(Arrays.asList(trUut));
			t.create(Arrays.asList(templateTestStep));
			t.create(Arrays.asList(templateTest));
			t.commit();
		}

		Project project1 = ef.createProject("PMV 2PV");
		Project project2 = ef.createProject("PMV Model P");
		Project project3 = ef.createProject("PMV Summit");
		Project project4 = ef.createProject("PMV Summit (Copy)");

		Pool pool1 = ef.createPool("PBN Measurements", project3);
		Pool pool2 = ef.createPool("SVN Measurements", project3);

		// Pool with same name as pool1 is needed for
		// WhenGetTreePathWithAmbigiousParentWithNameFilter#testGetParentNodeVehicle
		ef.createPool("PBN Measurements", project4);

		Test test1 = ef.createTest("PBN_UNECE_R51", pool1, templateTest, false);
		Test test2 = ef.createTest("SVN_UNECE_R51", pool2, templateTest, false);

		// TestStep under PBN
		TestStep testestStep1 = ef.createTestStep("PBN_UNECE_R51_Right_Acc_50", test1, templateTestStep);
		ContextComponent orderedVehicle = testestStep1.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		orderedVehicle.getValue("vin").set("815");
		orderedVehicle.getValue("model").set("Summit");

		Measurement measurement1 = ef.createMeasurement("MyMeaResult1", testestStep1);
		ContextComponent measuredVehicle = measurement1.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		measuredVehicle.getValue("vin").set("1234");
		measuredVehicle.getValue("model").set("Summit");
		measuredVehicle.getValue("type").set(VersionState.VALID);

		Quantity quantity = ef.createQuantity("MyQuantity");

		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement1);
		Channel channel1 = ef.createChannel("X-Axis", measurement1, quantity);
		Channel channel2 = ef.createChannel("CHANNEL01", measurement1, quantity);

		WriteRequest values1 = WriteRequest.create(channelGroup, channel1, AxisType.X_AXIS)
				.implicitLinear(ScalarType.LONG, 0, 1).independent().build();
		WriteRequest values2 = WriteRequest.create(channelGroup, channel2, AxisType.Y_AXIS).explicit()
				.longValues(new long[] { 12, -1, 42 }).build();

		// TestStep under SVN
		TestStep testestStep2 = ef.createTestStep("SVN_UNECE_R51_Right_50", test2, templateTestStep);
		ContextComponent orderedVehicle1 = testestStep2.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		orderedVehicle1.getValue("vin").set("1234");
		orderedVehicle1.getValue("model").set("Model P");

		Measurement measurement2 = ef.createMeasurement("MyMeaResult2", testestStep2);
		ContextComponent measuredVehicle1 = measurement2.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		measuredVehicle1.getValue("vin").set("1234");
		measuredVehicle1.getValue("model").set("Model P");
		measuredVehicle1.getValue("type").set(VersionState.ARCHIVED);

		Measurement measurement3 = ef.createMeasurement("MyMeaResult3", testestStep2);
		ContextComponent measuredVehicle2 = measurement3.loadContexts(em, ContextType.UNITUNDERTEST)
				.get(ContextType.UNITUNDERTEST).getContextComponent("vehicle").get();
		measuredVehicle2.getValue("vin").set("1234");
		measuredVehicle2.getValue("model").set("Model P");

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project1, project2, project3, project4));
			t.writeMeasuredValues(Arrays.asList(values1, values2));
			t.commit();
		}
		em.loadAll(TestStep.class, "*");

		// We have to close and open the context so the components are available in the
		// SearchQueries
		context.close();
		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());
	}

	private NodeProviderRoot getNodeProviderRoot() {
		NodeLevel nl = createStructure(context, ContextState.MEASURED);
		NodeProviderRoot npr = new NodeProviderRoot("generic_measured", "Measured");
		npr.setContexts(ImmutableMap.of("*", SerializationUtil.convertToJsonNode(nl)));

		return npr;
	}

	private GenericNodeProvider getGenericNodeProvider(NodeProviderRoot npr) {
		ConnectorService connectorService = Mockito.mock(ConnectorService.class);
		Mockito.when(connectorService.getContexts()).thenReturn(Arrays.asList(context));
		Mockito.when(connectorService.getContextByName(any())).thenReturn(context);

		MDMExpressionLanguageService elService = new MDMExpressionLanguageService();

		return new GenericNodeProvider(connectorService, npr, elService);
	}

	private Node findNodeWithLabel(List<Node> nodes, String label) {
		return nodes.stream().filter(n -> n.getLabel().equals(label)).findFirst().orElseThrow(
				() -> new AssertionFailedError("No node with label " + label + " found in " + nodes + "."));
	}

	@org.junit.jupiter.api.Test
	void testGetName() {
		NodeProviderRoot npr = getNodeProviderRoot();
		GenericNodeProvider np = getGenericNodeProvider(npr);

		assertThat(np.getName()).isEqualTo("Measured");

		assertThat(npr.getName()).isEqualTo("Measured");
		assertThat(npr.getId()).isEqualTo("generic_measured");
	}

	@org.junit.jupiter.api.Test
	void testGetRoots() {
		GenericNodeProvider np = getGenericNodeProvider(getNodeProviderRoot());

		assertThat(np.getRoots()).extracting("source", "type", "id", "label", "level")
				.contains(tuple("MDM", "Environment", "1", "MDM", 1));
	}

	@org.junit.jupiter.api.Test
	void testGetChildren() {
		GenericNodeProvider np = getGenericNodeProvider(getNodeProviderRoot());

		List<Node> roots = np.getRoots();

		List<Node> projects = np.getChildren(roots.get(0));
		assertThat(projects).extracting("source", "type", "id", "label", "level", "filter").containsExactly(
				tuple("MDM", "Project", "1", "PMV 2PV", 2, "Project.Id eq \"1\""),
				tuple("MDM", "Project", "2", "PMV Model P", 2, "Project.Id eq \"2\""),
				tuple("MDM", "Project", "3", "PMV Summit", 2, "Project.Id eq \"3\""),
				tuple("MDM", "Project", "4", "PMV Summit (Copy)", 2, "Project.Id eq \"4\""));

		List<Node> vehicleTypes = np.getChildren(findNodeWithLabel(projects, "PMV Summit"));
		assertThat(vehicleTypes).extracting("source", "type", "id", "label", "level", "filter").containsExactly(
				tuple("MDM", "vehicle", "", "Model P", 3, "Project.Id eq \"3\" and vehicle.model eq \"Model P\""),
				tuple("MDM", "vehicle", "", "Summit", 3, "Project.Id eq \"3\" and vehicle.model eq \"Summit\""));

		List<Node> tests = np.getChildren(findNodeWithLabel(vehicleTypes, "Summit"));
		assertThat(tests).extracting("source", "type", "id", "label", "level", "filter")
				.containsExactly(tuple("MDM", "Test", "1", "PBN_UNECE_R51", 4,
						"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\""));

		List<Node> testSteps = np.getChildren(findNodeWithLabel(tests, "PBN_UNECE_R51"));
		assertThat(testSteps).extracting("source", "type", "id", "label", "level", "filter").containsExactly(tuple(
				"MDM", "TestStep", "1", "PBN_UNECE_R51_Right_Acc_50", 5,
				"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\" and TestStep.Id eq \"1\""));

		List<Node> measurements = np.getChildren(findNodeWithLabel(testSteps, "PBN_UNECE_R51_Right_Acc_50"));
		assertThat(measurements).extracting("source", "type", "id", "label", "level", "filter").containsExactly(tuple(
				"MDM", "Measurement", "1", "MyMeaResult1", 6,
				"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\" and TestStep.Id eq \"1\" and Measurement.Id eq \"1\""));

		List<Node> channelGroups = np.getChildren(findNodeWithLabel(measurements, "MyMeaResult1"));
		assertThat(channelGroups).extracting("source", "type", "id", "label", "level", "filter").containsExactly(tuple(
				"MDM", "ChannelGroup", "1", "MyChannelGroup", 7,
				"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\" and TestStep.Id eq \"1\" and Measurement.Id eq \"1\" and ChannelGroup.Id eq \"1\""));

		List<Node> channels = np.getChildren(findNodeWithLabel(channelGroups, "MyChannelGroup"));
		assertThat(channels).hasSize(2).extracting("source", "type", "id", "label", "level", "filter").containsExactly(
				tuple("MDM", "Channel", "2", "CHANNEL01", 8,
						"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\" and TestStep.Id eq \"1\" and Measurement.Id eq \"1\" and ChannelGroup.Id eq \"1\" and Channel.Id eq \"2\""),
				tuple("MDM", "Channel", "1", "X-Axis", 8,
						"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\" and TestStep.Id eq \"1\" and Measurement.Id eq \"1\" and ChannelGroup.Id eq \"1\" and Channel.Id eq \"1\""));

	}

	@Nested
	class WhenLoadParentNode {
		GenericNodeProvider np = getGenericNodeProvider(getNodeProviderRoot());

		@org.junit.jupiter.api.Test
		void testGetParentNodeEnvironment() {
			Node environment = createNode("MDM", "Environment", 1, "1", "", "MDM");

			assertThat(np.loadParentNode(context, environment, null)).isNull();
		}

		@org.junit.jupiter.api.Test
		void testGetParentNodeProject() {
			Node project = createNode("MDM", "Project", 2, "3", "Project.Id eq \"3\"", "PMV Summit");

			assertThat(np.loadParentNode(context, project, null))
					.isEqualTo(createNode("MDM", "Environment", 1, "1", "", "MDM"));
		}

		@org.junit.jupiter.api.Test
		void testGetParentNodeVehicle() {
			Node vehicle = createNode("MDM", "vehicle", 3, "Summit",
					"Project.Id eq \"3\" and vehicle.model eq \"Summit\"", "Summit");

			assertThat(np.loadParentNode(context, vehicle, null)).usingRecursiveComparison().ignoringFields("serial_")
					.isEqualTo(createNode("MDM", "Project", 2, "3", "Project.Id eq \"3\"", "PMV Summit"));
		}

		@org.junit.jupiter.api.Test
		void testGetParentNodeTest() {
			Node test = createNode("MDM", "Test", 4, "1", "Test.Id eq \"1\"", "");

			assertThat(np.loadParentNode(context, test, null)).usingRecursiveComparison().ignoringFields("serial_")
					.isEqualTo(createNode("MDM", "vehicle", 3, null, "Test.Id eq \"1\" and vehicle.model eq \"Summit\"",
							"Summit"));
		}

		@org.junit.jupiter.api.Test
		void testGetParentNodeTestStep() {
			Node testStep = createNode("MDM", "TestStep", 5, "1", "TestStep.Id eq \"1\"", "");

			assertThat(np.loadParentNode(context, testStep, null)).usingRecursiveComparison().ignoringFields("serial_")
					.isEqualTo(createNode("MDM", "Test", 4, "1", "Test.Id eq \"1\"", "PBN_UNECE_R51"));
		}
	}

	@Nested
	class WhenGetTreePathWithAmbigiousParent {
		GenericNodeProvider np = getGenericNodeProvider(getNodeProviderRoot(context, ContextState.MEASURED));

		private NodeProviderRoot getNodeProviderRoot(ApplicationContext context, ContextState contextState) {
			ModelManager modelManager = context.getModelManager().get();

			EntityType etEnv = modelManager.getEntityType(Environment.class);
			NodeLevel env = new NodeLevel(etEnv);

			EntityType vehicle = modelManager.getEntityType("vehicle");
			NodeLevel vehicleModel = new NodeLevel(vehicle, new FilterAttribute(vehicle.getAttribute("model")),
					vehicle.getAttribute("model"));
			vehicleModel.setVirtual(true);
			vehicleModel.setContextState(contextState);

			NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));
			NodeLevel test = new NodeLevel(modelManager.getEntityType(org.eclipse.mdm.api.base.model.Test.class));
			NodeLevel testStep = new NodeLevel(modelManager.getEntityType(TestStep.class));
			NodeLevel measurement = new NodeLevel(modelManager.getEntityType(Measurement.class));
			NodeLevel channelGroup = new NodeLevel(modelManager.getEntityType(ChannelGroup.class));
			NodeLevel channel = new NodeLevel(modelManager.getEntityType(Channel.class));

			env.setChild(vehicleModel);
			vehicleModel.setChild(project);
			project.setChild(test);
			test.setChild(testStep);
			testStep.setChild(measurement);
			measurement.setChild(channelGroup);
			channelGroup.setChild(channel);

			NodeProviderRoot npr = new NodeProviderRoot("ambigious_parent", "AmbigiousParent");
			npr.setContexts(ImmutableMap.of("*", SerializationUtil.convertToJsonNode(env)));

			return npr;
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathEnvironment() {
			Node environment = createNode("MDM", "Environment", 1, "1", "", "MDM");

			assertThat(np.getTreePath(environment)).containsExactly(environment);
		}

		@org.junit.jupiter.api.Test
		void testGetParentNodeVehicle() {
			Node vehicle = createNode("MDM", "vehicle", 2, "Summit", "vehicle.model eq \"Summit\"", "Summit");

			assertThat(np.getTreePath(vehicle)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "vehicle", 2, null, "vehicle.model eq \"Summit\"", "Summit"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathProject() {
			Node project = createNode("MDM", "Project", 3, "3", "Project.Id eq \"3\" and vehicle.model eq \"Summit\"",
					"PMV Summit");

			assertThat(np.getTreePath(project)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "vehicle", 2, null, "vehicle.model eq \"Summit\"", "Summit"), createNode("MDM",
							"Project", 3, "3", "vehicle.model eq \"Summit\" and Project.Id eq \"3\"", "PMV Summit"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathTest1() {
			Node test = createNode("MDM", "Test", 4, "1", "Test.Id eq \"1\"", "");

			assertThat(np.getTreePath(test)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "vehicle", 2, null, "vehicle.model eq \"Summit\"", "Summit"),
					createNode("MDM", "Project", 3, "3", "vehicle.model eq \"Summit\" and Project.Id eq \"3\"",
							"PMV Summit"),
					createNode("MDM", "Test", 4, "1",
							"vehicle.model eq \"Summit\" and Project.Id eq \"3\" and Test.Id eq \"1\"",
							"PBN_UNECE_R51"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathTest2() {
			Node test = createNode("MDM", "Test", 4, "2", "Test.Id eq \"2\"", "");

			assertThat(np.getTreePath(test)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "vehicle", 2, null, "vehicle.model eq \"Model P\"", "Model P"),
					createNode("MDM", "Project", 3, "3", "vehicle.model eq \"Model P\" and Project.Id eq \"3\"",
							"PMV Summit"),
					createNode("MDM", "Test", 4, "2",
							"vehicle.model eq \"Model P\" and Project.Id eq \"3\" and Test.Id eq \"2\"",
							"SVN_UNECE_R51"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathTestStep1() {
			Node testStep = createNode("MDM", "TestStep", 5, "1", "TestStep.Id eq \"1\"", "");

			assertThat(np.getTreePath(testStep)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "vehicle", 2, null, "vehicle.model eq \"Summit\"", "Summit"),
					createNode("MDM", "Project", 3, "3", "vehicle.model eq \"Summit\" and Project.Id eq \"3\"",
							"PMV Summit"),
					createNode("MDM", "Test", 4, "1",
							"vehicle.model eq \"Summit\" and Project.Id eq \"3\" and Test.Id eq \"1\"",
							"PBN_UNECE_R51"),
					createNode("MDM", "TestStep", 5, "1",
							"vehicle.model eq \"Summit\" and Project.Id eq \"3\" and Test.Id eq \"1\" and TestStep.Id eq \"1\"",
							"PBN_UNECE_R51_Right_Acc_50"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathTestStep2() {
			Node testStep = createNode("MDM", "TestStep", 5, "2", "TestStep.Id eq \"2\"", "");

			assertThat(np.getTreePath(testStep)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "vehicle", 2, null, "vehicle.model eq \"Model P\"", "Model P"),
					createNode("MDM", "Project", 3, "3", "vehicle.model eq \"Model P\" and Project.Id eq \"3\"",
							"PMV Summit"),
					createNode("MDM", "Test", 4, "2",
							"vehicle.model eq \"Model P\" and Project.Id eq \"3\" and Test.Id eq \"2\"",
							"SVN_UNECE_R51"),
					createNode("MDM", "TestStep", 5, "2",
							"vehicle.model eq \"Model P\" and Project.Id eq \"3\" and Test.Id eq \"2\" and TestStep.Id eq \"2\"",
							"SVN_UNECE_R51_Right_50"));
		}
	}

	@Nested
	class WhenGetTreePath {
		GenericNodeProvider np = getGenericNodeProvider(getNodeProviderRoot());

		@org.junit.jupiter.api.Test
		void testGetTreePathEnvironment() {
			Node environment = createNode("MDM", "Environment", "1", "Id", "", "MDM");

			assertThat(np.getTreePath(environment))
					.containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathProject() {
			Node project = createNode("MDM", "Project", "3", "Id", "", "PMV Summit");

			assertThat(np.getTreePath(project)).extracting("source", "type", "id", "label", "level", "filter")
					.containsExactly(tuple("MDM", "Environment", "1", "MDM", 1, ""),
							tuple("MDM", "Project", "3", "PMV Summit", 2, "Project.Id eq \"3\""));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathVehicle() {
			Node vehicle = Node.newBuilder().setSource("MDM").setType("vehicle").setId("").setLevel(3)
					.setLabel("Summit").setFilter("Project.Id eq \"3\" and vehicle.model eq \"Summit\"").build();

			assertThat(np.getTreePath(vehicle)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "Project", 2, "3", "Project.Id eq \"3\"", "PMV Summit"), createNode("MDM",
							"vehicle", 3, null, "Project.Id eq \"3\" and vehicle.model eq \"Summit\"", "Summit"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathTest() {
			Node test = createNode("MDM", "Test", "1", "", "", "");

			assertThat(np.getTreePath(test)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "Project", 2, "3", "Project.Id eq \"3\"", "PMV Summit"),
					createNode("MDM", "vehicle", 3, null, "Project.Id eq \"3\" and vehicle.model eq \"Summit\"",
							"Summit"),
					createNode("MDM", "Test", 4, "1",
							"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\"",
							"PBN_UNECE_R51"));
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathTestStep() {
			Node testStep = createNode("MDM", "TestStep", "1", "Id", "", "Name");

			assertThat(np.getTreePath(testStep)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "Project", 2, "3", "Project.Id eq \"3\"", "PMV Summit"),
					createNode("MDM", "vehicle", 3, null, "Project.Id eq \"3\" and vehicle.model eq \"Summit\"",
							"Summit"),
					createNode("MDM", "Test", 4, "1",
							"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\"",
							"PBN_UNECE_R51"),
					createNode("MDM", "TestStep", 5, "1",
							"Project.Id eq \"3\" and vehicle.model eq \"Summit\" and Test.Id eq \"1\" and TestStep.Id eq \"1\"",
							"PBN_UNECE_R51_Right_Acc_50"));
		}
	}

	@Nested
	class WhenGetTreePathWithAmbigiousParentWithNameFilter {
		GenericNodeProvider np = getGenericNodeProvider(getNodeProviderRoot(context, ContextState.MEASURED));

		private NodeProviderRoot getNodeProviderRoot(ApplicationContext context, ContextState contextState) {
			ModelManager modelManager = context.getModelManager().get();

			EntityType etEnv = modelManager.getEntityType(Environment.class);
			NodeLevel env = new NodeLevel(etEnv);

			EntityType vehicle = modelManager.getEntityType("vehicle");
			NodeLevel vehicleModel = new NodeLevel(vehicle, new FilterAttribute(vehicle.getAttribute("model")),
					vehicle.getAttribute("model"));
			vehicleModel.setVirtual(true);
			vehicleModel.setContextState(contextState);

			NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));

			Attribute poolName = modelManager.getEntityType(Pool.class).getAttribute("Name");
			NodeLevel pool = new NodeLevel(modelManager.getEntityType(Pool.class), new FilterAttribute(poolName),
					poolName);

			env.setChild(project);
			project.setChild(pool);
			pool.setChild(vehicleModel);

			NodeProviderRoot npr = new NodeProviderRoot("ambigious_parent", "AmbigiousParent");
			npr.setContexts(ImmutableMap.of("*", SerializationUtil.convertToJsonNode(env)));

			return npr;
		}

		@org.junit.jupiter.api.Test
		void testGetTreePathEnvironment() {
			Node environment = createNode("MDM", "Environment", 1, "1", "", "MDM");

			assertThat(np.getTreePath(environment)).containsExactly(environment);
		}

		@org.junit.jupiter.api.Test
		void testGetParentNodeVehicle() {
			Node vehicle = createNode("MDM", "vehicle", 4, "Summit", "vehicle.model eq \"Summit\"", "Summit");

			assertThat(np.getTreePath(vehicle)).containsExactly(createNode("MDM", "Environment", 1, "1", "", "MDM"),
					createNode("MDM", "Project", 2, "3", "Project.Id eq \"3\"", "PMV Summit"),
					createNode("MDM", "Pool", 3, "1", "Project.Id eq \"3\" and Pool.Name eq \"PBN Measurements\"",
							"PBN Measurements"),
					createNode("MDM", "vehicle", 4, null,
							"Project.Id eq \"3\" and Pool.Name eq \"PBN Measurements\" and vehicle.model eq \"Summit\"",
							"Summit"));
		}
	}

	@Nested
	@TestInstance(Lifecycle.PER_CLASS)
	class WhenNodeproviderContainsEnum {
		GenericNodeProvider np;

		@BeforeAll
		void init() {
			NodeLevel nl = createStructureWithEnum(context, ContextState.MEASURED);
			NodeProviderRoot npr = new NodeProviderRoot("generic_measured", "Measured");
			npr.setContexts(ImmutableMap.of("*", SerializationUtil.convertToJsonNode(nl)));

			np = getGenericNodeProvider(npr);
		}

		NodeLevel createStructureWithEnum(ApplicationContext context, ContextState contextState) {
			ModelManager modelManager = context.getModelManager().get();

			EntityType etEnv = modelManager.getEntityType(Environment.class);
			NodeLevel env = new NodeLevel(etEnv);

			NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));

			EntityType vehicle = modelManager.getEntityType("vehicle");
			NodeLevel vehicleType = new NodeLevel(vehicle, new FilterAttribute(vehicle.getAttribute("type")),
					vehicle.getAttribute("type"));
			vehicleType.setLabelExpression("${empty type ? 'undefined' : type}");
			vehicleType.setVirtual(true);
			vehicleType.setContextState(contextState);

			NodeLevel measurement = new NodeLevel(modelManager.getEntityType(Measurement.class));
			NodeLevel channelGroup = new NodeLevel(modelManager.getEntityType(ChannelGroup.class));
			NodeLevel channel = new NodeLevel(modelManager.getEntityType(Channel.class));

			env.setChild(project);
			project.setChild(vehicleType);
			vehicleType.setChild(measurement);
			measurement.setChild(channelGroup);
			channelGroup.setChild(channel);

			return env;
		}

		@org.junit.jupiter.api.Test
		void testGetChildren() {
			List<Node> roots = np.getRoots();

			List<Node> projects = np.getChildren(roots.get(0));
			assertThat(projects).extracting("source", "type", "id", "label", "level", "filter").containsExactly(
					tuple("MDM", "Project", "1", "PMV 2PV", 2, "Project.Id eq \"1\""),
					tuple("MDM", "Project", "2", "PMV Model P", 2, "Project.Id eq \"2\""),
					tuple("MDM", "Project", "3", "PMV Summit", 2, "Project.Id eq \"3\""),
					tuple("MDM", "Project", "4", "PMV Summit (Copy)", 2, "Project.Id eq \"4\""));

			List<Node> vehicleTypes = np.getChildren(findNodeWithLabel(projects, "PMV Summit"));
			assertThat(vehicleTypes).extracting("source", "type", "id", "label", "level", "filter").containsExactly(
					tuple("MDM", "vehicle", "", "ARCHIVED", 3, "Project.Id eq \"3\" and vehicle.type eq \"ARCHIVED\""),
					tuple("MDM", "vehicle", "", "VALID", 3, "Project.Id eq \"3\" and vehicle.type eq \"VALID\""),
					tuple("MDM", "vehicle", "", "undefined", 3,
							"Project.Id eq \"3\" and MEASURED.vehicle.type is_null"));

			List<Node> archived = np.getChildren(findNodeWithLabel(vehicleTypes, "VALID"));
			assertThat(archived).extracting("source", "type", "id", "label", "level", "filter")
					.containsExactly(tuple("MDM", "Measurement", "1", "MyMeaResult1", 4,
							"Project.Id eq \"3\" and vehicle.type eq \"VALID\" and Measurement.Id eq \"1\""));

			archived = np.getChildren(findNodeWithLabel(vehicleTypes, "ARCHIVED"));
			assertThat(archived).extracting("source", "type", "id", "label", "level", "filter")
					.containsExactly(tuple("MDM", "Measurement", "2", "MyMeaResult2", 4,
							"Project.Id eq \"3\" and vehicle.type eq \"ARCHIVED\" and Measurement.Id eq \"2\""));

			archived = np.getChildren(findNodeWithLabel(vehicleTypes, "undefined"));
			assertThat(archived).extracting("source", "type", "id", "label", "level", "filter")
					.containsExactly(tuple("MDM", "Measurement", "3", "MyMeaResult3", 4,
							"Project.Id eq \"3\" and MEASURED.vehicle.type is_null and Measurement.Id eq \"3\""));
		}
	}

	static NodeLevel createStructure(ApplicationContext context, ContextState contextState) {
		ModelManager modelManager = context.getModelManager().get();

		EntityType etEnv = modelManager.getEntityType(Environment.class);
		NodeLevel env = new NodeLevel(etEnv);

		NodeLevel project = new NodeLevel(modelManager.getEntityType(Project.class));

		EntityType vehicle = modelManager.getEntityType("vehicle");
		NodeLevel vehicleModel = new NodeLevel(vehicle, new FilterAttribute(vehicle.getAttribute("model")),
				vehicle.getAttribute("model"));
		vehicleModel.setVirtual(true);
		vehicleModel.setContextState(contextState);

		NodeLevel test = new NodeLevel(modelManager.getEntityType(org.eclipse.mdm.api.base.model.Test.class));
		NodeLevel testStep = new NodeLevel(modelManager.getEntityType(TestStep.class));
		NodeLevel measurement = new NodeLevel(modelManager.getEntityType(Measurement.class));
		NodeLevel channelGroup = new NodeLevel(modelManager.getEntityType(ChannelGroup.class));
		NodeLevel channel = new NodeLevel(modelManager.getEntityType(Channel.class));

		env.setChild(project);
		project.setChild(vehicleModel);
		vehicleModel.setChild(test);
		test.setChild(testStep);
		testStep.setChild(measurement);
		measurement.setChild(channelGroup);
		channelGroup.setChild(channel);

		return env;
	}
}
