/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.io.IOException;
import java.util.List;

import org.eclipse.mdm.api.base.model.Value;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Implementation to serialize DTOs to JSON.
 *
 * @since 5.2.0
 * @author MAF, Peak Solution GmbH
 */
public class MDMValueListSerializer extends JsonSerializer<List<Value>> {

	private final MDMValueSerializer valueSerializer = new MDMValueSerializer();

	@Override
	public void serialize(List<Value> values, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeStartArray();
		for (Value v : values) {
			valueSerializer.serialize(v, gen, serializers);
		}
		gen.writeEndArray();
	}
}
