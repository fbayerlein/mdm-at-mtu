/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ATTRIBUTENAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTTYPE;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCETYPE;

import java.io.InputStream;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.businessobjects.control.FileLinkActivity;
import org.eclipse.mdm.businessobjects.entity.MDMFileLink;
import org.eclipse.mdm.businessobjects.service.ContextService;
import org.eclipse.mdm.businessobjects.utils.Serializer;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Context resource
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Tag(name = "ContextComponent")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/contextcomponents")
public class ContextComponentResource {

	@EJB
	private FileLinkActivity<DescriptiveFile> fileLinkActivity;

	@EJB
	private ContextService contextService;

	@Context
	private ResourceContext resourceContext;

	/**
	 * Creates new {@link FileLink} for {@link TestStep}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link FileLink} to create.
	 * @return
	 */
	@POST
	@Operation(summary = "Creates a new file on a context attribute.", responses = {
			@ApiResponse(description = "The stored file link.", content = @Content(schema = @Schema(implementation = MDMFileLink.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("/{" + REQUESTPARAM_CONTEXTTYPE + "}/{" + REQUESTPARAM_SOURCETYPE + "}/{" + REQUESTPARAM_ID + "}/{"
			+ REQUESTPARAM_ATTRIBUTENAME + "}/files")
	public Response createFile(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ContextType", required = true) @PathParam(REQUESTPARAM_CONTEXTTYPE) String contextTypeParam,
			@Parameter(description = "Name of the attribute or relation holding the FileLink", required = true) @PathParam(REQUESTPARAM_SOURCETYPE) String sourceType,
			@Parameter(description = "ID of context component", required = true) @PathParam(REQUESTPARAM_ID) String contextComponentId,
			@Parameter(description = "Name of the attribute or relation holding the FileLink", required = true) @PathParam(REQUESTPARAM_ATTRIBUTENAME) String attributeName,
			@Parameter(description = "InputStream containing file data", required = true) @FormDataParam("file") InputStream fileInputStream,
			@Parameter(description = "Meta data describing file", required = true) @FormDataParam("file") FormDataContentDisposition cdh,
			@Parameter(description = "File modification date", required = true) @FormDataParam("lastModified") String lastModified,
			@Parameter(description = "File description", required = true) @FormDataParam("description") String description,
			@Parameter(description = "Mimetype of the file", required = true) @FormDataParam("mimeType") MimeType mimeType) {

		List<ContextComponent> contextComponents = contextService.getContextComponents(sourceName,
				ServiceUtils.getContextTypeSupplier(contextTypeParam).get(), sourceType,
				Collections.singletonList(contextComponentId));

		FileLink fileLink = fileLinkActivity.createFile(sourceName, contextComponents.get(0),
				ServiceUtils.convertIso8859toUtf8(cdh.getFileName()), Instant.ofEpochMilli(Long.valueOf(lastModified)),
				fileInputStream, description, mimeType, attributeName);
		return ServiceUtils.toResponse(Serializer.serializeFileLink(fileLink), Status.OK);

	}

}
