/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.server.Broadcaster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Event broadcast container which is used in the session container
 * to hold a reference of the user reference to the broadcaster
 *
 * @author Juergen Kleck, Peak Solution GmbH
 */
public class EventBroadcast implements Serializable {

	private String sourceName;
	private String referenceId;
	private EventOutput eventOutput;
	private Broadcaster<OutboundEvent> broadCaster;
	private List<String> messages;

	public EventBroadcast() {
		messages = new ArrayList<>();
	}

	public EventBroadcast(String sourceName, String referenceId, EventOutput eventOutput, Broadcaster<OutboundEvent> broadCaster) {
		this();
		this.sourceName = sourceName;
		this.referenceId = referenceId;
		this.eventOutput = eventOutput;
		this.broadCaster = broadCaster;
	}

	public boolean match(String sourceName, String referenceId) {
		return sourceName != null && referenceId != null && sourceName.equals(this.sourceName) && referenceId.equals(this.referenceId);
	}

	public void cleanUp() {
		messages.clear();
		broadCaster.closeAll();
	}

	public String getSourceName() {
		return sourceName;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public EventOutput getEventOutput() {
		return eventOutput;
	}

	public Broadcaster<OutboundEvent> getBroadCaster() {
		return broadCaster;
	}

	public void pushMessage(String message) {
		this.messages.add(message);
	}

	public List<String> getMessages() {
		return messages;
	}
}
