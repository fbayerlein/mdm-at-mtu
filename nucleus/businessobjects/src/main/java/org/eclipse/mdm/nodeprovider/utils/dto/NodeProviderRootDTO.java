/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.utils.dto;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.mdm.nodeprovider.entity.NodeProviderRoot;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * DTO for {@link NodeProviderRoot} used for serialization.
 *
 */
public class NodeProviderRootDTO {

	private String id;
	private String name;
	private Map<String, JsonNode> contexts = new HashMap<>();

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the contexts
	 */
	public Map<String, JsonNode> getContexts() {
		return contexts;
	}

	/**
	 * @param contexts the contexts to set
	 */
	public void setContexts(Map<String, JsonNode> contexts) {
		this.contexts = contexts;
	}

}
