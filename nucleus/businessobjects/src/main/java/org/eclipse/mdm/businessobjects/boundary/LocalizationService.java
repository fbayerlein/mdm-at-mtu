/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.MDMLocalization;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;

@Stateless
public class LocalizationService {

	@Inject
	private ConnectorService connectorService;

	public List<MDMLocalization> localize(String sourceName) {
		try {
			return this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"))
					.loadAll(MDMLocalization.class);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	public List<MDMLocalization> getLocalizations(String sourceName, Map<String, List<String>> localizations) {
		return this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"))
				.loadAll(MDMLocalization.class, localizations.keySet());
	}

	public List<MDMLocalization> saveLocalization(String sourceName, Map<String, List<String>> localizations) {
		List<MDMLocalization> newLocalization = new ArrayList<>();
		List<MDMLocalization> delLocalization = new ArrayList<>();
		List<MDMLocalization> updateLocalization = new ArrayList<>();
		List<MDMLocalization> localizationDB = this.getLocalizations(sourceName, localizations);
		for (MDMLocalization localization : localizationDB) {
			List<String> values = localizations.get(localization.getName());
			if (values.isEmpty()) {
				delLocalization.add(localization);
			} else if (values.size() == 2) {
				localization.getValue("AliasNames").set(values.toArray(new String[0]));
				updateLocalization.add(localization);
			} else {
				throw new MDMEntityAccessException("Wrong number for AliasNames!");
			}
			localizations.remove(localization.getName());
		}
		if (!localizations.isEmpty()) {
			EntityFactory ef = this.connectorService.getContextByName(sourceName).getEntityFactory()
					.orElseThrow(() -> new MDMEntityAccessException("Entity factory not present!"));
			for (Entry<String, List<String>> entry : localizations.entrySet()) {
				if (!entry.getValue().isEmpty() && entry.getValue().size() == 2) {
					newLocalization
							.add(ef.createMDMLocalization(entry.getKey(), entry.getValue().toArray(new String[0])));
				}
			}
		}
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		boolean localizationCreated = false;
		Transaction transaction = em.startTransaction();
		try {
			transaction.delete(delLocalization);
			transaction.update(updateLocalization);
			transaction.create(newLocalization);
			transaction.commit();
			localizationCreated = true;
		} finally {
			if (!localizationCreated) {
				transaction.abort();
				throw new MDMEntityAccessException("Failed to save localization!");
			}
		}
		newLocalization.addAll(updateLocalization);
		return newLocalization;
	}

	public Map<String, String> getDefaultMimeTypes(String sourceName) {
		try {
			ModelManager mm = this.connectorService.getContextByName(sourceName).getModelManager()
					.orElseThrow(() -> new MDMEntityAccessException("Model manager not present!"));

			Map<String, String> map = new HashMap<>();

			mm.listEntityTypes().stream()
					.forEach(et -> map.put(ServiceUtils.workaroundForTypeMapping(et.getName()), mm.getMimeType(et)));

			return map;

		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}
}
