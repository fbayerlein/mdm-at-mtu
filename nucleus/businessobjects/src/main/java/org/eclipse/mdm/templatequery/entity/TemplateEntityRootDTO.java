/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;

/**
 * DTOs for objects with parent - child relations.
 *
 * @since 5.2.0
 * @author MAF and JZ, Peak Solution GmbH
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class TemplateEntityRootDTO {
	private String contextType;

	private List<TemplateEntityDTO> templateRoots = new ArrayList<>();

	private transient Map<Key, TemplateEntityDTO> cache = new HashMap<>();

	public String getContextType() {
		return contextType;
	}

	public void setContextType(String contextType) {
		this.contextType = contextType;
	}

	@XmlTransient
	@JsonIgnore
	public List<TemplateEntityDTO> getTemplateRoots() {
		return templateRoots;
	}

	@XmlTransient
	public void setTemplateRoots(List<TemplateEntityDTO> templateEntities) {
		this.templateRoots = templateEntities;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public List<TemplateEntityDTO> getTplRoots() {
		return templateRoots.stream().filter(te -> te instanceof TemplateEntityTplRootDTO).collect(Collectors.toList());
	}

	public void setTplRoots(List<TemplateEntityTplRootDTO> templateEntities) {
		this.templateRoots.addAll(templateEntities);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("contextType", contextType).add("templateRoots", templateRoots)
				.toString();
	}

	private static class Key {
		private String type;
		private String id;

		public Key(String type, String id) {
			super();
			this.type = type;
			this.id = id;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper(this).add("type", type).add("id", id).toString();
		}

		@Override
		public int hashCode() {
			return Objects.hash(id, type);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Key other = (Key) obj;
			return Objects.equals(id, other.id) && Objects.equals(type, other.type);
		}
	}

	public void put(String type, String id, TemplateEntityDTO value) {
		cache.put(new Key(type, id), value);
	}

	public TemplateEntityDTO get(String key1, String key2) {
		return cache.get(new Key(key1, key2));
	}
}
