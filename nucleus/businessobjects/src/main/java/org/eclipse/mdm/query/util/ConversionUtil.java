/********************************************************************************
 * Copyright (c) 2015-2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.query.entity.Column;
import org.eclipse.mdm.query.entity.ContextColumn;
import org.eclipse.mdm.query.entity.Row;

import com.google.common.base.Strings;

/**
 * 
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
public final class ConversionUtil {

	private static final Set<String> nonContextTypes = new HashSet<String>(
			Arrays.asList("Project", "Pool", "Test", "TestStep", "Measurement", "ChannelGroup", "Channel"));

	private ModelManager modelManager;

	public ConversionUtil(ModelManager modelManager) {
		this.modelManager = modelManager;
	}

	public List<Row> convertResultList(Collection<Result> results, Class<? extends Entity> resultEntityClass,
			List<Attribute> requestedAttributes, EntityType type) {
		List<Row> rows = new ArrayList<>();
		results.forEach(row -> rows.add(convertResult(row, resultEntityClass, requestedAttributes, type)));
		return rows;
	}

	public Row convertResult(Result result, Class<? extends Entity> resultEntityClass,
			List<Attribute> requestedAttributes, EntityType type) {
		Row row = new Row();
		row.setSource(type.getSourceName());		
		row.setType(resultEntityClass.getSimpleName());
		Value id = result.getRecord(type).getIDValue();
		row.setId(id.isValid(ContextState.MEASURED)
				? id.extract(ContextState.MEASURED)
				: id.extract(ContextState.ORDERED));

		requestedAttributes.forEach(a -> row.addColumn(convertColumn(result, a)));
		return row;
	}

	public Column convertColumn(Result result, Attribute a) {
		Record record = getRecord(result, a);
		Value value = getValue(result, a);

		String typeName = ServiceUtils.workaroundForTypeMapping(record.getEntityType());
		String valueName = value.getName();

		if (isStatusAttr(a)) {
			typeName = a.getEntityType().getName();
			valueName = "Status";
		}

		if (nonContextTypes.contains(typeName)) {
			return new Column(typeName, valueName, value.getValueType().name(), value.extract(ContextState.MEASURED),
					Strings.emptyToNull(value.getUnit()));
		} else {
			return new ContextColumn(typeName, valueName, value.getValueType().name(),
					value.extract(ContextState.MEASURED), value.extract(ContextState.ORDERED),
					Strings.emptyToNull(value.getUnit()));
		}
	}

	private Record getRecord(Result result, Attribute a) {
		Record record = null;

		if (modelManager != null && isStatusAttr(a)) {
			EntityType entityTypeStatus = modelManager.getEntityType("Status");
			record = result.getRecord(entityTypeStatus);
		}

		if (record == null) {
			record = result.getRecord(a.getEntityType());
		}

		return record;
	}

	private Value getValue(Result result, Attribute a) {
		Value value = null;

		if (modelManager != null && isStatusAttr(a)) {
			EntityType entityTypeStatus = modelManager.getEntityType("Status");
			value = result.getValue(entityTypeStatus.getAttribute("Name"));
		}

		if (value == null) {
			value = result.getValue(a);
		}

		return value;
	}

	private boolean isStatusAttr(Attribute a) {
		boolean isStatusAttr = false;

		if (modelManager != null) {
			EntityType entityTypeTestStep = modelManager.getEntityType("TestStep");
			EntityType entityTypeTest = modelManager.getEntityType("Test");

			EntityType attrEntityType = a.getEntityType();

			if ("Status".equals(a.getName())
					&& (entityTypeTestStep.equals(attrEntityType) || entityTypeTest.equals(attrEntityType))) {
				isStatusAttr = true;
			}
		}

		return isStatusAttr;
	}
}
