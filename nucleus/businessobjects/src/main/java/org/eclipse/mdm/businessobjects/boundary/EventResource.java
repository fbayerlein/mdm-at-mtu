/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.notification.NotificationException;
import org.eclipse.mdm.businessobjects.entity.MDMNotificationFilter;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.SseFeature;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Event REST interface to handle ODS notifications events
 *
 * @author Juergen Kleck, Peak Solution GmbH
 */
@Tag(name = "Event")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/events/{" + REQUESTPARAM_SOURCENAME + "}")
public class EventResource {

	private static final String REFERENCE_ID = "referenceId";

	@EJB
	private EventService eventService;

	@POST
	@Operation(summary = "Register for ODS Events", description = "Register for ODS Events", responses = {
			@ApiResponse(responseCode = "200", description = ""),
			@ApiResponse(responseCode = "400", description = "Client error"),
			@ApiResponse(responseCode = "500", description = "Server error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REFERENCE_ID + "}")
	public Response register(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REFERENCE_ID) String referenceId, MDMNotificationFilter filter) throws ConnectionException, NotificationException {
		eventService.register(sourceName, referenceId, filter);

		return Response.ok().build();
	}

	@DELETE
	@Operation(summary = "De-register for ODS Events", description = "De-register for ODS Events", responses = {
			@ApiResponse(responseCode = "200", description = ""),
			@ApiResponse(responseCode = "400", description = "Client error"),
			@ApiResponse(responseCode = "500", description = "Server error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REFERENCE_ID + "}")
	public Response deRegister(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REFERENCE_ID) String referenceId) throws ConnectionException, NotificationException {

		eventService.deRegister(sourceName, referenceId);

		return Response.ok().build();
	}

	@GET
	@Produces(SseFeature.SERVER_SENT_EVENTS)
	@Operation(summary = "Retrieve ODS notification event stream", description = "Retrieve ODS notification event stream", responses = {
			@ApiResponse(responseCode = "200", description = ""),
			@ApiResponse(responseCode = "400", description = "Client error"),
			@ApiResponse(responseCode = "500", description = "Server error") })
	@Path("/{" + REFERENCE_ID + "}")
	public EventOutput getServerSentEvents(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REFERENCE_ID) String referenceId) throws ConnectionException, NotificationException {
		return eventService.getServerSentEvents(sourceName, referenceId);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Retrieve pooled ODS notification events", description = "Retrieve pooled ODS notification events", responses = {
			@ApiResponse(responseCode = "200", description = ""),
			@ApiResponse(responseCode = "400", description = "Client error"),
			@ApiResponse(responseCode = "500", description = "Server error") })
	@Path("/{" + REFERENCE_ID + "}")
	public Response getStoredEvents(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REFERENCE_ID) String referenceId) throws ConnectionException, NotificationException {
		String response = "{\"data\":[" + eventService.getStoredEvents(sourceName, referenceId) + "]}";

		return Response.ok(response).build();
	}

}
