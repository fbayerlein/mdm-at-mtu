/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.mdm.query.entity.Row;

import com.google.common.base.MoreObjects;

/**
 * The UI wrapper class is used in the web UI to provide a sorted list with name
 * and id
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NodeProviderUIWrapper {

	private int index;
	private String id;
	private String name;

	@SuppressWarnings("unused")
	private NodeProviderUIWrapper() {

	}

	public NodeProviderUIWrapper(int index, String id, String name) {
		this.index = index;
		this.id = id;
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(index, id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}
		final NodeProviderUIWrapper other = (NodeProviderUIWrapper) obj;
		return Objects.equals(this.index, other.index) && Objects.equals(this.id, other.id)
				&& Objects.equals(this.name, other.name);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(Row.class).add("index", index).add("id", id).add("name", name).toString();
	}
}
