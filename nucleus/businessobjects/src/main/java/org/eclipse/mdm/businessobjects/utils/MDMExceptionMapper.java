/*******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.businessobjects.utils;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.eclipse.mdm.api.base.query.ConnectionLostException;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.businessobjects.entity.MDMErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.base.Throwables;

@Provider
public class MDMExceptionMapper implements ExceptionMapper<Throwable> {
	private static final Logger LOGGER = LoggerFactory.getLogger(MDMExceptionMapper.class);
	private static final int STATUS_FAILED_DEPENDENCY = 424;

	@Context
	private HttpServletRequest request;

	@Override
	public Response toResponse(Throwable t) {
		List<String> causes = Throwables.getCausalChain(t).stream()
				.map(x -> Strings.isNullOrEmpty(x.getMessage()) ? x.getClass().getSimpleName() : x.getMessage())
				.collect(Collectors.toList());

		MDMErrorResponse mer = new MDMErrorResponse(t.getMessage(), causes);

		if (isIncludeStacktrace()) {
			mer.setStackTrace(Throwables.getStackTraceAsString(t));
		}

		if (Throwables.getCausalChain(t).stream().filter(x -> x instanceof ConnectionLostException).findAny()
				.isPresent()) {
			LOGGER.debug("Returning Status Failed Dependency Response with traceid {}", mer.getTraceID(), t);
			return Response.status(STATUS_FAILED_DEPENDENCY).entity(mer).build();
		} else if (Throwables.getCausalChain(t).stream().filter(x -> x instanceof DataAccessException).findAny()
				.isPresent()) {
			LOGGER.debug("Returning Client Error with traceid {}", mer.getTraceID(), t);
			return Response.status(Status.BAD_REQUEST).entity(mer).build();
		} else {
			LOGGER.debug("Returning Internal Server Error with traceid {}", mer.getTraceID(), t);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(mer).build();
		}
	}

	private boolean isIncludeStacktrace() {
		if (request == null) {
			return true;
		} else {
			return Boolean.valueOf(request.getHeader("X-OpenMDM-Stacktrace"));
		}
	}
}