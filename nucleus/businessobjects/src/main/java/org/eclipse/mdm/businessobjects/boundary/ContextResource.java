/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.businessobjects.entity.MultipleContextResponse;
import org.eclipse.mdm.businessobjects.service.ContextService;
import org.eclipse.mdm.businessobjects.service.DescribableContexts;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Context resource
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Tag(name = "Context")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/contexts")
public class ContextResource {

	@EJB
	private ContextService contextService;

	/**
	 * Updates the context of the given {@link TestStep}s with all parameters set in
	 * the given JSON body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       the body of the request containing the attributes to update
	 * @return the context map of the updated {@link TestStep}
	 */
	@PUT
	@Operation(summary = "Update context data of existing TestSteps and Measurements", description = "Updates the context data of all given TestSteps.", responses = {
			@ApiResponse(description = "The updated data", content = @Content(schema = @Schema(implementation = MultipleContextResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({ "Admin", "DescriptiveDataAuthor" })
	public Response updateContext(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName, String body) {
		java.util.List<DescribableContexts> updateContext = contextService.updateContext(sourceName, body);
		MultipleContextResponse contextResponse = new MultipleContextResponse(updateContext);
		Response response = ServiceUtils.toResponse(contextResponse);
		return response;
	}

	@PUT
	@Operation(summary = "Update context data of existing TestSteps and Measurements", description = "Updates the context data of all given TestSteps.", responses = {
			@ApiResponse(description = "Empty"),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces("application/json+empty")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({ "Admin", "DescriptiveDataAuthor" })
	public Response updateContextEmptyResponse(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName, String body) {
		contextService.updateMultiContextComponents(sourceName, body);
		return Response.ok().build();
	}

}
