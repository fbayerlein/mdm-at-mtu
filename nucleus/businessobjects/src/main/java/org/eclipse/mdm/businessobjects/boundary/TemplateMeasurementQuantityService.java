/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurementQuantity;
import org.eclipse.mdm.api.dflt.model.TemplateChannelGroup;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.connector.boundary.ConnectorService;

/**
 * {@link TemplateMeasurementQuantity} service
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Stateless
public class TemplateMeasurementQuantityService {

	@Inject
	private ConnectorService connectorService;

	public List<TemplateMeasurementQuantity> getTemplateMeasurementQuantities(String sourceName, String id, Class<?> sourceClass) {
		Entity entity = null;
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		if (TemplateChannelGroup.class.equals(sourceClass)) {
			entity = em.load(TemplateChannelGroup.class, id);
		}

		return em.loadRelatedEntities(entity, "TplMeaQuantity", TemplateMeasurementQuantity.class);
	}

	/**
	 * returns a {@link TemplateMeasurementQuantity} identified by the given id.
	 * 
	 * @param sourceName  name of the source (MDM {@link Environment} name)
	 * @param extSystemId id of the {@link TemplateMeasurementQuantity}
	 * @return the matching {@link TemplateMeasurementQuantity}
	 */
	public TemplateMeasurementQuantity getTplMeaQuantity(String sourceName, String tplMeaQuantityId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(TemplateMeasurementQuantity.class, tplMeaQuantityId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	public List<TemplateMeasurementQuantity> addTplMeaQuantity(String sourceName, String id, TemplateChannelGroup tplChannelGroup) {
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		Transaction t = em.startTransaction();
		TemplateMeasurementQuantity tplMeaQuantity = getTplMeaQuantity(sourceName, id);

		tplChannelGroup.addItem(tplMeaQuantity);
		t.update(Collections.singletonList(tplChannelGroup));
		t.commit();

		return getTemplateMeasurementQuantities(sourceName, tplChannelGroup.getID(), tplChannelGroup.getClass());
	}
}
