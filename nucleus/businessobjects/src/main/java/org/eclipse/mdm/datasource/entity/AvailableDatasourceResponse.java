/*******************************************************************************
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

/**
 * SearchDefinitionResponse (Container for {@link SearchAttribute}s)
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
package org.eclipse.mdm.datasource.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.mdm.connector.entity.ServiceConfiguration;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AvailableDatasourceResponse {


	/** transferable data content */
	private List<ServiceConfiguration> data;

	/**
	 * Constructor
	 * 
	 * @param searchAttributes list of {@link SearchAttribute}s to transfer
	 */
	public AvailableDatasourceResponse(List<ServiceConfiguration> serviceConfiguration) {
		data = new ArrayList<>(serviceConfiguration);
	}

	/**
	 * Constructor
	 */
	public AvailableDatasourceResponse() {
		data = new ArrayList<>();
	}

	public List<ServiceConfiguration> getData() {
		return Collections.unmodifiableList(this.data);
	}
}
