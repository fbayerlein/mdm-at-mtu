/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.MediaType;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.base.notification.NotificationListener;
import org.eclipse.mdm.businessobjects.entity.EventBroadcast;
import org.eclipse.mdm.businessobjects.entity.EventData;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.utils.JacksonConfig;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.vavr.control.Try;

/**
 * Event notification listener which will receive the asynchronous event from
 * the backend and forward it to the registered broadcaster
 *
 * @author Juergen Kleck, Peak Solution GmbH
 */
public class EventNotificationListener implements NotificationListener, Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(EventNotificationListener.class);

	private static final String EVENT_NAME = "ODS_notification";

	private final String id = UUID.randomUUID().toString();

	private final EventBroadcast broadcaster;
	private final String receiver;
	private final MediaType mediaType;

	private final ObjectMapper mapper = new JacksonConfig().getContext(null);
//	ObjectMapper().registerModule(new JavaTimeModule())
//			.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

	public EventNotificationListener(EventBroadcast eventBroadcast, String receiver, MediaType mediaType) {
		this.broadcaster = eventBroadcast;
		this.receiver = receiver;
		this.mediaType = mediaType;
	}

	private void registerMessage(EventData message) {
		try {
			final OutboundEvent event = new OutboundEvent.Builder().name(EVENT_NAME).id(id).mediaType(mediaType)
					.data(EventData.class, message).build();
			LOG.debug("Send notification: " + event);

			broadcaster.pushMessage(mapper.writeValueAsString(message));

			// send to streaming event
			if (broadcaster.getBroadCaster() != null) {
				broadcaster.getBroadCaster().broadcast(event);
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}

	}

	@Override
	public void instanceCreated(List<? extends Entity> entities, User user) {
		if (!entities.isEmpty()) {
			registerMessage(new EventData("instanceCreated",
					Try.of(() -> entities).map(io.vavr.collection.List::ofAll).map(e -> {
						Class<? extends Entity> entityClass = e.get().getClass();
						return new MDMEntityResponse(entityClass, e.asJava());
					}).get(), user, null, null)

			);
		}

	}

	@Override
	public void instanceModified(List<? extends Entity> entities, User user) {
		LOG.debug("instanceModified: entities.size: " + entities.size());

		if (!entities.isEmpty()) {
			registerMessage(new EventData("instanceModified",
					Try.of(() -> entities).map(io.vavr.collection.List::ofAll).map(e -> {
						Class<? extends Entity> entityClass = e.get().getClass();
						return new MDMEntityResponse(entityClass, e.asJava());
					}).get(), user, null, null));
		}

	}

	@Override
	public void instanceDeleted(EntityType entityType, List<String> ids, User user) {
		registerMessage(new EventData("instanceDeleted", null, user, entityType, ids));
	}

	@Override
	public void modelModified(EntityType entityType, User user) {
		registerMessage(new EventData("modelModified", null, user, entityType, null));
	}

	@Override
	public void securityModified(EntityType entityType, List<String> ids, User user) {
		registerMessage(new EventData("securityModified", null, user, entityType, ids));
	}
}