/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.boundary;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.nodeprovider.control.NodeProviderException;
import org.eclipse.mdm.nodeprovider.control.NodeProviderRepository;
import org.eclipse.mdm.nodeprovider.entity.NodeProvider;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderUIWrapper;

/**
 * NodeProvidertService Bean implementation with available {@link NodeProvider}
 * operations
 *
 */
@Stateless
public class NodeProviderService implements Serializable {

	private static final long serialVersionUID = -6499586790511058985L;

	@Inject
	private NodeProviderRepository nodeProviders;

	public NodeProviderService() {
	}

	// for unit tests
	protected NodeProviderService(NodeProviderRepository nodeProviders) {
		this.nodeProviders = nodeProviders;
	}

	/**
	 * @return list of nodeprovider IDs
	 */
	public List<String> getNodeProviderIDs() {
		return nodeProviders.getNodeProviderIDs();
	}

	/**
	 * @return list of nodeproviders
	 */
	public List<NodeProviderUIWrapper> getNodeProvidersSorted() {
		return nodeProviders.getNodeProvidersSorted();
	}

	/**
	 * @param nodeproviderId ID of the requested {@link NodeProvider}
	 * @return NodeProvider with requested ID
	 * @throws NodeProviderException if the requested ID does not exist.
	 */
	public NodeProvider getNodeProvider(String nodeproviderId) {
		NodeProvider nodeProvider = nodeProviders.getNodeProviders().get(nodeproviderId);
		if (nodeProvider == null) {
			throw new NodeProviderException("NodeProvider with name " + nodeproviderId + " does not exist!");
		}
		return nodeProvider;
	}
}
