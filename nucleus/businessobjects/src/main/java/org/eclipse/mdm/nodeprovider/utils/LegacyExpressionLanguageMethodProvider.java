/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * This class is for backwards compatibility only. Use
 * {@link ExpressionLanguageMethodProvider} and new 'mdm' namespace.
 * 
 * All functions in the {@link LegacyExpressionLanguageMethodProvider} are
 * reflected in the 'fn' namespace in the expression language context.
 * Therefore, they have to be static.
 *
 */

public class LegacyExpressionLanguageMethodProvider {

	/**
	 * Formats a date according to the fiven format string.
	 * 
	 * @param input  date to format
	 * @param format desired format pattern
	 * @return String with the formatted date
	 */
	public static String formatDate(Instant input, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of("UTC"));
		return formatter.format(input);
	}
}
