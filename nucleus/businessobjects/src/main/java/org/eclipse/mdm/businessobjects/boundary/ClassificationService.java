/*******************************************************************************
 * Copyright (c) 2019, 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Classification;
import org.eclipse.mdm.api.dflt.model.Domain;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.ProjectDomain;
import org.eclipse.mdm.api.dflt.model.Status;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import com.google.common.collect.Lists;

/**
 * @author akn
 *
 */
@Stateless
public class ClassificationService {

	@Inject
	private ConnectorService connectorService;

	@EJB
	private EntityService entityService;

	@EJB
	private ProjectService projectService;

	public List<Classification> findClassifications(String sourceName, ProjectDomain projectDomain) {
		Optional<QueryService> queryService = connectorService.getContextByName(sourceName).getQueryService();
		Optional<ModelManager> modelManager = connectorService.getContextByName(sourceName).getModelManager();

		EntityType entityType = modelManager.get().getEntityType(Classification.class);
		Condition c1 = ComparisonOperator.CASE_INSENSITIVE_EQUAL.create(entityType.getAttribute("ProjectDomain"),
				projectDomain.getID());

		java.util.List<Result> fetch = queryService.get().createQuery().selectID(entityType)
				.fetch(Filter.and().addAll(c1));

		java.util.List<String> collect = fetch.stream().map(r -> r.getRecord(entityType)).map(Record::getID)
				.collect(Collectors.toList());

		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

		return em.load(Classification.class, collect);
	}

	public Classification getClassification(String sourceName, Status status, ProjectDomain projectDomain,
			Domain domain) {
		Optional<QueryService> queryService = connectorService.getContextByName(sourceName).getQueryService();
		Optional<ModelManager> modelManager = connectorService.getContextByName(sourceName).getModelManager();

		EntityType entityType = modelManager.get().getEntityType(Classification.class);
		Condition c1 = ComparisonOperator.CASE_INSENSITIVE_EQUAL.create(entityType.getAttribute("ProjectDomain"),
				projectDomain.getID());
		Condition c2 = ComparisonOperator.CASE_INSENSITIVE_EQUAL.create(entityType.getAttribute("Domain"),
				domain.getID());
		Condition c3 = ComparisonOperator.CASE_INSENSITIVE_EQUAL.create(entityType.getAttribute("Status"),
				status.getID());

		java.util.List<Result> fetch = queryService.get().createQuery().selectID(entityType)
				.fetch(Filter.and().addAll(c1, c2, c3));

		java.util.List<String> collect = fetch.stream().map(r -> r.getRecord(entityType)).map(Record::getID)
				.collect(Collectors.toList());

		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

		Classification classification = null;
		if (!collect.isEmpty()) {

			classification = em.load(Classification.class, collect.get(0));
		}

		return classification;
	}

	public Classification getClassification(String sourceName, String statusName, String poolId) {
		String filter = "Pool.Id eq \"" + poolId + "\"";
		List<Project> projects = this.projectService.getProjects(sourceName, filter);
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("EntityManager not present!"));
		List<Domain> domains = em.loadAll(Domain.class);
		io.vavr.collection.List<Status> status = this.entityService
				.findAll(V(sourceName), Status.class, "Status.Name eq \"" + statusName + "\"").get();
		if (projects.size() == 1 && domains.size() == 1) {
			return this.getOrCreateClassification(sourceName, status.get(),
					this.getProjectDomain(sourceName, projects.get(0).getName(), em), domains.get(0));
		}
		return null;
	}

	public Classification getClassificationForTest(String sourceName, String statusName, String testId) {
		String filter = "Test.Id eq \"" + testId + "\"";
		List<Project> projects = this.projectService.getProjects(sourceName, filter);
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("EntityManager not present!"));
		List<Domain> domains = em.loadAll(Domain.class);
		io.vavr.collection.List<Status> status = this.entityService
				.findAll(V(sourceName), Status.class, "Status.Name eq \"" + statusName + "\"").get();
		if (projects.size() == 1 && domains.size() == 1) {
			return this.getOrCreateClassification(sourceName, status.get(),
					this.getProjectDomain(sourceName, projects.get(0).getName(), em), domains.get(0));
		}
		return null;
	}

	public Classification getClassification(String sourceName, String statusId, String testId, String teststepId) {
		String filter = testId != null ? "Test.Id eq \"" + testId + "\"" : "TestStep.Id eq \"" + teststepId + "\"";
		List<Project> projects = this.projectService.getProjects(sourceName, filter);
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("EntityManager not present!"));
		List<Domain> domains = em.loadAll(Domain.class);
		Status status = this.entityService.find(V(sourceName), Status.class, V(statusId)).get();
		if (projects.size() == 1 && domains.size() == 1) {
			return this.getOrCreateClassification(sourceName, status,
					this.getProjectDomain(sourceName, projects.get(0).getName(), em), domains.get(0));
		}
		return null;
	}

	private ProjectDomain getProjectDomain(String sourceName, String projectName, EntityManager em) {
		ModelManager mm = this.connectorService.getContextByName(sourceName).getModelManager()
				.orElseThrow(() -> new MDMEntityAccessException("ModelManagery not present!"));
		EntityType entityTypeProjectDomain = mm.getEntityType(ProjectDomain.class);
		Condition condProjectDomainName = ComparisonOperator.CASE_INSENSITIVE_EQUAL
				.create(entityTypeProjectDomain.getAttribute("Name"), projectName);

		QueryService queryService = this.connectorService.getContextByName(sourceName).getQueryService()
				.orElseThrow(() -> new MDMEntityAccessException("QueryService not present!"));
		List<Result> fetchProjectDomain = queryService.createQuery().selectID(entityTypeProjectDomain)
				.fetch(Filter.and().addAll(condProjectDomainName));

		List<String> projectDomainIds = fetchProjectDomain.stream().map(r -> r.getRecord(entityTypeProjectDomain))
				.map(Record::getID).collect(Collectors.toList());

		if (projectDomainIds.size() != 1) {
			throw new IllegalStateException(String.format(
					"More than one ProjectDomain instance with name \"%s\" found in destination!", projectName));
		} else {
			return em.load(ProjectDomain.class, projectDomainIds.get(0));
		}
	}

	private Classification getOrCreateClassification(String sourceName, Status status, ProjectDomain projectDomain,
			Domain domain) {
		Classification classification = this.getClassification(sourceName, status, projectDomain, domain);
		if (classification == null) {
			StringBuilder className = new StringBuilder();
			className.append("ProjDomainId_");
			className.append(projectDomain.getID());
			className.append(".DomainId_");
			className.append(domain.getID());
			className.append(".StatusId_");
			className.append(status.getID());

			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			try (Transaction transaction = em.startTransaction()) {

				EntityFactory ef = this.connectorService.getContextByName(sourceName).getEntityFactory()
						.orElseThrow(() -> new MDMEntityAccessException("Entity factory not present!"));
				classification = ef.createClassification(className.toString(), status, projectDomain, domain);
				transaction.create(Lists.newArrayList(classification));
				transaction.commit();
			} catch (Exception e) {
				throw new MDMEntityAccessException("Failed to create classification!", e);
			}
		}

		return classification;
	}
}
