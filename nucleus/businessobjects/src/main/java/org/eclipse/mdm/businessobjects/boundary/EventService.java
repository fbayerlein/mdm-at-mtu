/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.notification.NotificationException;
import org.eclipse.mdm.api.base.notification.NotificationFilter;
import org.eclipse.mdm.api.base.notification.NotificationService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.businessobjects.control.EventConnectionHandlerService;
import org.eclipse.mdm.businessobjects.entity.EventBroadcast;
import org.eclipse.mdm.businessobjects.entity.MDMNotificationFilter;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.glassfish.jersey.server.Broadcaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Event service class which will handle the broadcasts from the server side to
 * the client side
 *
 * @author Juergen Kleck, Peak Solution GmbH
 */
@Stateless
public class EventService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EventService.class);

	private static final String NOTIFICATION_REGISTRATION_NAME = "mdm";

	@Inject
	private ConnectorService connectorService;

	@Inject
	private EventConnectionHandlerService eventConnectionHandlerService;

	public EventService() {
	}

	private Optional<NotificationService> getManager(String sourceName) throws ConnectionException {
		return connectorService.getContextByName(sourceName).getNotificationService();
	}

	public void register(String sourceName, String referenceId, MDMNotificationFilter notificationFilter)
			throws ConnectionException, NotificationException {
		Optional<NotificationService> manager = getManager(sourceName);
		if (!manager.isPresent()) {
			LOGGER.debug("No notification manager present cannot register '{}' at source '{}'", referenceId,
					sourceName);
			return;
		}
		LOGGER.debug("Registering with name '{}' at source '{}'", referenceId, sourceName);
		EventOutput eventOutput = new EventOutput();
		Broadcaster<OutboundEvent> broadCaster = new SseBroadcaster();
		broadCaster.add(eventOutput);
		EventBroadcast eventBroadcast = null;
		if (eventConnectionHandlerService.getBroadcasters().stream()
				.anyMatch(eb -> eb.match(sourceName, referenceId))) {
			eventBroadcast = eventConnectionHandlerService.getBroadcasters().stream()
					.filter(eb -> eb.match(sourceName, referenceId)).findAny().get();
		} else {
			eventBroadcast = new EventBroadcast(sourceName, referenceId, eventOutput, broadCaster);
			eventConnectionHandlerService.getBroadcasters().add(eventBroadcast);
		}

		EventNotificationListener listener = new EventNotificationListener(eventBroadcast, referenceId,
				MediaType.APPLICATION_JSON_TYPE);
		manager.get().register(NOTIFICATION_REGISTRATION_NAME + referenceId, convert(sourceName, notificationFilter),
				listener);
	}

	public void deRegister(String sourceName, String referenceId) throws ConnectionException, NotificationException {
		Optional<NotificationService> manager = getManager(sourceName);
		if (!manager.isPresent()) {
			LOGGER.debug("No notification manager present cannot deregister '{}' at source '{}'", referenceId,
					sourceName);
			return;
		}
		LOGGER.debug("De-registering with name '{}' at source '{}'", referenceId, sourceName);
		manager.get().deregister(NOTIFICATION_REGISTRATION_NAME + referenceId);
		if (eventConnectionHandlerService.getBroadcasters().stream()
				.anyMatch(eb -> eb.match(sourceName, referenceId))) {
			EventBroadcast eventBroadcast = eventConnectionHandlerService.getBroadcasters().stream()
					.filter(eb -> eb.match(sourceName, referenceId)).findAny().get();
			eventBroadcast.cleanUp();
			eventConnectionHandlerService.getBroadcasters().removeIf(eb -> eb.match(sourceName, referenceId));
		}
	}

	public String getStoredEvents(String sourceName, String referenceId)
			throws ConnectionException, NotificationException {
		String response = "";
		Optional<NotificationService> manager = getManager(sourceName);
		if (!manager.isPresent()) {
			LOGGER.debug("No notification manager present cannot deregister '{}' at source '{}'", referenceId,
					sourceName);
			return response;
		}
		if (!eventConnectionHandlerService.getBroadcasters().stream().anyMatch(eb -> eb.match(sourceName, referenceId))
				&& !manager.get().getRegistrations().isEmpty()) {

			EventOutput eventOutput = new EventOutput();
			Broadcaster<OutboundEvent> broadCaster = new SseBroadcaster();
			broadCaster.add(eventOutput);
			EventBroadcast eventBroadcast = new EventBroadcast(sourceName, referenceId, eventOutput, broadCaster);
			eventConnectionHandlerService.getBroadcasters().add(eventBroadcast);
			for (Map.Entry<String, NotificationFilter> e : manager.get().getRegistrations().entrySet()) {
				EventNotificationListener listener = new EventNotificationListener(eventBroadcast, referenceId,
						MediaType.APPLICATION_JSON_TYPE);
				manager.get().reattach(e.getKey(), listener);
			}

		}

		if (eventConnectionHandlerService.getBroadcasters().stream()
				.anyMatch(eb -> eb.match(sourceName, referenceId))) {
			EventBroadcast eventBroadcast = eventConnectionHandlerService.getBroadcasters().stream()
					.filter(eb -> eb.match(sourceName, referenceId)).findAny().get();

			List<String> messages = eventBroadcast.getMessages();
			for (int i = 0; i < messages.size(); i++) {
				response = response + messages.get(i);
				if (i + 1 < messages.size()) {
					response = response + ",";
				}
			}

			eventBroadcast.getMessages().clear();
		} else {
			throw new ConnectionException(
					"No broadcaster registered for sourceName: " + sourceName + ", referenceId: " + referenceId);
		}

		return response;
	}

	public EventOutput getServerSentEvents(String sourceName, String referenceId)
			throws ConnectionException, NotificationException {
		LOGGER.debug("getServerSentEvents with name '{}' at source '{}'", referenceId, sourceName);
		if (eventConnectionHandlerService.getBroadcasters().stream()
				.anyMatch(eb -> eb.match(sourceName, referenceId))) {
			return eventConnectionHandlerService.getBroadcasters().stream()
					.filter(eb -> eb.match(sourceName, referenceId)).findAny().get().getEventOutput();
		}
		return null;
	}

	private NotificationFilter convert(String sourceName, MDMNotificationFilter filter) {
		if (filter == null) {
			return new NotificationFilter();
		} else {
			ApplicationContext context = connectorService.getContextByName(sourceName);
			ModelManager mm = context.getModelManager().get();
			NotificationFilter f = new NotificationFilter();
			f.setTypes(filter.getTypes());
			f.setEntityTypes(
					filter.getEntityTypes().stream().map(name -> mm.getEntityType(name)).collect(Collectors.toSet()));
			return f;
		}
	}

}
