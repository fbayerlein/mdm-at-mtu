/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.nodeprovider.control.GenericNodeProvider;
import org.eclipse.mdm.nodeprovider.control.NodeProviderException;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.nodeprovider.utils.dto.NodeLevelDTO;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Strings;

/**
 * A {@link NodeProviderRoot} defines the configuration of a
 * {@link GenericNodeProvider}. It consists of an ID to uniquely identify the
 * NodeProvider and a name that is displayed to the user. Furthermore it may
 * contain {@link NodeLevel}s for each data source name defining the roots of
 * the navigation trees. The special data source name
 * {@link NodeProviderRoot#WILDCARD} is used for the fallback {@link NodeLevel}
 * that is used if no other entry is present.
 *
 */
public class NodeProviderRoot {

	private static final Logger LOG = LoggerFactory.getLogger(NodeProviderRoot.class);
	private static ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	public static final String WILDCARD = "*";

	private String id;
	private String name;
	private Map<String, JsonNode> passiveContexts = new HashMap<>();

	private transient Map<String, NodeLevel> parsedContexts = new HashMap<>();

	/**
	 * Default constructor
	 */
	public NodeProviderRoot() {

	}

	public NodeProviderRoot(String id, String name) {
		this(id, name, Collections.emptyMap(), Collections.emptyMap());
	}

	public NodeProviderRoot(String id, String name, Map<String, NodeLevel> contexts) {
		this(id, name, contexts, Collections.emptyMap());
	}

	public NodeProviderRoot(String id, String name, Map<String, NodeLevel> contexts,
			Map<String, JsonNode> passiveContexts) {
		this.id = id;
		this.name = name;
		this.parsedContexts.putAll(contexts);
		this.passiveContexts.putAll(passiveContexts);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the contexts
	 */
	public Map<String, NodeLevel> getContexts() {
		return parsedContexts;
	}

	/**
	 * Only for testing!
	 * 
	 * @param contexts the contexts to set
	 */
	public void setParsedContexts(Map<String, NodeLevel> contexts) {
		this.parsedContexts = contexts;
	}

	public void setContexts(Map<String, JsonNode> contexts) {
		this.passiveContexts = contexts;
	}

	/**
	 * Find the NodeLevel for a given Node. If node is null, the top-most nodelevel
	 * is returned. If the Node does not have a valid node level (e.g.
	 * node.getLevel() == 0), this methods falls back to identifying the nodelevel
	 * based on type and ID-attribute, which might fail for virtual nodes.
	 * 
	 * @param context
	 * @param node
	 * @return child NodeLevel
	 */
	public NodeLevel getNodeLevel(ApplicationContext context, Node node) {
		if (node == null) {
			return getNodeLevel(context, 1);
		} else if (node.getLevel() == 0) {
			// No node level given, fallback to figure out NodeLevel by type and filter
			// attribute
			return getNodeLevel(context, node.getType(), node.getIdAttribute(), true);
		} else {
			return getNodeLevel(context, node.getLevel());
		}
	}

	/**
	 * Find the child NodeLevel for a given nodeLevel. If node is null, the top-most
	 * nodelevel is returned
	 * 
	 * @param context
	 * @param node
	 * @return child NodeLevel
	 */
	public Optional<NodeLevel> getChildNodeLevel(ApplicationContext context, Node node) {
		if (node == null) {
			return Optional.ofNullable(getNodeLevel(context, 1));
		} else {
			NodeLevel nodeLevel = getNodeLevel(context, node);
			if (nodeLevel == null) {
				return Optional.empty();
			}
			return Optional.ofNullable(nodeLevel.getChild());
		}
	}

	/**
	 * Find the parent NodeLevel for a given nodeLevel.
	 * 
	 * @param context
	 * @param nodeLevel
	 * @return parent NodeLevel
	 */
	public NodeLevel getParentNodeLevel(ApplicationContext context, NodeLevel nodeLevel) {
		NodeLevel n = getNodeLevel(context);

		if (n.equals(nodeLevel)) {
			return null;
		}
		while (n.getChild() != null && !nodeLevel.equals(n.getChild())) {
			n = n.getChild();
		}

		return n;
	}

	/**
	 * Get the {@link NodeLevel} for the given {@link ApplicationContext}. If the
	 * given sourceName of the ApplicationContext has no corresponding entry, the
	 * {@link NodeLevel} for the {@link NodeProviderRoot#WILDCARD} is returned. If
	 * not {@link NodeProviderRoot#WILDCARD} entry exists an exception is thrown.
	 * 
	 * @param context
	 * @return NodeLevel for the given sourceName
	 */
	public NodeLevel getNodeLevel(ApplicationContext context) {
		// try to find a parsed context matching the source name
		NodeLevel nodeLevel = parsedContexts.get(context.getSourceName());
		if (nodeLevel != null) {
			return nodeLevel;
		}

		// try to find a passive context matching the source name
		JsonNode node = passiveContexts.get(context.getSourceName());
		if (node != null) {
			return parseNode(context, node);
		}

		// try to find a parsed wild card context
		nodeLevel = parsedContexts.get(NodeProviderRoot.WILDCARD);
		if (nodeLevel != null) {
			return nodeLevel;
		}

		// try to find a parsed wild card context
		node = passiveContexts.get(NodeProviderRoot.WILDCARD);
		if (node == null) {
			throw new RuntimeException("No nodeprovider configuration found for source " + context.getSourceName()
					+ " and wildcard (*) configuration is not available!");
		} else {
			return parseNode(context, node);
		}
	}

	private NodeLevel parseNode(ApplicationContext context, JsonNode node) {
		try {
			NodeLevelDTO nlDTO = mapper.treeToValue(node, NodeLevelDTO.class);
			NodeLevel nodeLevel = SerializationUtil.convert(context, nlDTO);

			parsedContexts.put(context.getSourceName(), nodeLevel);
			return nodeLevel;
		} catch (Exception ex) {
			LOG.warn("Cannot use node provider definition {} for datasource {}", context.getSourceName(),
					context.getSourceName());
			throw new RuntimeException("No nodeprovider configuration found for source " + context.getSourceName(), ex);
		}
	}

	protected NodeLevel getNodeLevel(ApplicationContext context, int level) {
		NodeLevel n = getNodeLevel(context);

		while (n != null && n.getLevel() < level) {
			n = n.getChild();
		}

		if (n != null && n.getLevel() == level) {
			return n;
		} else {
			throw new NodeProviderException("No NodeLevel found with level=" + level + ".");
		}
	}

	/**
	 * @param context
	 * @param type
	 * @param filterAttribute
	 * @param ignoreVirtualNodes true to ignore virtual nodes, default false
	 * @return NodeLevel for given type and idAttribute
	 */
	private NodeLevel getNodeLevel(ApplicationContext context, String type, String filterAttribute,
			boolean ignoreVirtualNodes) {
		NodeLevel n = getNodeLevel(context);

		while (n != null && !nodeLevelMatches(n, type, filterAttribute, ignoreVirtualNodes)) {
			n = n.getChild();
		}

		return n;
	}

	/**
	 * @param nodeLevel
	 * @param type
	 * @param filterAttribute
	 * @return true, if the given type and idAttribute matches the nodeLevel
	 */
	/**
	 * TODO 05.02.2021, jst: Match should respect labelAttributes / value precision
	 * to work properly for nested virtual nodes of same type with different value
	 * precision.
	 * 
	 * Current fails in scenarios like:
	 * 
	 * (...) | |---Test.DateCreated - YEAR | |---Test.DateCreated - MONTH | |---
	 * Test.DateCreated - DAY
	 */
	private boolean nodeLevelMatches(NodeLevel nodeLevel, String type, String filterAttribute,
			boolean ignoreVirtualNodes) {

		String first = null;
		if (nodeLevel.getFilterAttributes() != null) {
			first = nodeLevel.getFilterAttributes().get(0).getAttribute().getName();
		}

		return ServiceUtils.workaroundForTypeMapping(type)
				.equals(ServiceUtils.workaroundForTypeMapping(nodeLevel.getEntityType()))
				&& (Strings.isNullOrEmpty(filterAttribute) || filterAttribute.equals(first))
				&& (!ignoreVirtualNodes || !nodeLevel.isVirtual());
	}
}
