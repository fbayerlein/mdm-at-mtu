/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.List;

import com.google.common.base.MoreObjects;

public class MDMEnum {

	private final String name;
	private List<MDMEnumValue> values;

	public MDMEnum(String name) {
		this.name = name;
	}

	public List<MDMEnumValue> getValues() {
		return values;
	}

	public void setValues(List<MDMEnumValue> values) {
		this.values = values;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(MDMEnum.class).add("name", name).add("values", values).toString();
	}
}
