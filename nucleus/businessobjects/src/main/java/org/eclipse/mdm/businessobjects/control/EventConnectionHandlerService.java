/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.control;

import org.eclipse.mdm.businessobjects.entity.EventBroadcast;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Dependent;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Connection handler service which stores the open broadcast events in a session scope
 *
 * @author Juergen Kleck, Peak Solution GmbH
 */
@Dependent
@SessionScoped
public class EventConnectionHandlerService implements Serializable {

	private static final long serialVersionUID = -5891142709182298511L;

	private static final Logger LOG = LoggerFactory.getLogger(EventConnectionHandlerService.class);

	private List<EventBroadcast> broadcasters = new ArrayList<>();

	public EventConnectionHandlerService() {
		// empty constructor
	}

	/**
	 * Initialize the session bean
	 */
	@PostConstruct
	public void init() {

	}

	/**
	 * Cleanup the session bean
	 */
	@PreDestroy
	public void close() {
	}

	public List<EventBroadcast> getBroadcasters() {
		return broadcasters;
	}
}
