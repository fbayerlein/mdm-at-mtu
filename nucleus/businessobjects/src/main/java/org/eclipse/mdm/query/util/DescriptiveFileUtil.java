/********************************************************************************
 * Copyright (c) 2015-2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.DefaultCore;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

public class DescriptiveFileUtil {

	private QueryService queryService;

	public DescriptiveFileUtil(ApplicationContext context) {
		this.queryService = context.getQueryService()
				.orElseThrow(() -> new ServiceNotProvidedException(QueryService.class));
	}

	public List<Attribute> addDescriptiveFilesToResult(List<String> columns, List<EntityType> searchableTypes,
			List<Result> result) {

		List<Attribute> requestedAttributes = columns.stream().map(c -> getAttribute(searchableTypes, c))
				.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

		Map<String, Optional<Relation>> colName2Relation = columns.stream()
				.collect(Collectors.toMap(c -> c, c -> getRelation(searchableTypes, c)));

		for (Map.Entry<String, Optional<Relation>> e : colName2Relation.entrySet()) {

			if (e.getValue().isPresent()
					&& DescriptiveFile.TYPE_NAME.equals(e.getValue().get().getTarget().getName())) {
				Relation relation = e.getValue().get();
				if (DescriptiveFile.TYPE_NAME.equals(relation.getTarget().getName())) {
					requestedAttributes.add(relation.getAttribute());

					Map<String, Value> values = loadFileRelationValues(relation, result);

					addValuesToResult(result, values, relation.getAttribute());
				}
			}
		}

		return requestedAttributes;
	}

	private void addValuesToResult(List<Result> result, Map<String, Value> values, Attribute attribute) {
		
		for (Result r : result) {
			Record record = r.getRecord(attribute.getEntityType());

			Value idValue = record.getIDValue();

			if (idValue.isValid(ContextState.MEASURED)) {
				String id = idValue.extract(ContextState.MEASURED);

				Value fileRelationValue = values.computeIfAbsent(id, x -> getDefaultValue(attribute));
				record.addValue(fileRelationValue);
			}
			if (idValue.isValid(ContextState.ORDERED)) {
				String id = idValue.extract(ContextState.ORDERED);

				Value fileRelationValue = values.computeIfAbsent(id, x -> getDefaultValue(attribute));
				fileRelationValue.swapContext();
				record.addValue(fileRelationValue);
			}
		}
	}

	private Value getDefaultValue(Attribute attribute) {
		return ValueType.FILE_RELATION.create(attribute.getName(), null, false, null);
	}

	private Filter getFilterBasedOnIds(List<Result> result, EntityType entityType) {
		
		List<String> ids = new ArrayList<>();
		for (Result r : result) {
			Value v = r.getRecord(entityType).getIDValue();
			if (v.isValid(ContextState.MEASURED)) {
				ids.add(v.extract(ContextState.MEASURED));
			}
			if (v.isValid(ContextState.ORDERED)) {
				ids.add(v.extract(ContextState.ORDERED));
			}
		}
		return Filter.idsOnly(entityType, ids);
	}

	private Map<String, Value> loadFileRelationValues(Relation descriptiveFileRelation, List<Result> result) {
		Map<String, Value> data = new HashMap<>();

		if (!result.isEmpty()) {
			Filter filter = getFilterBasedOnIds(result, descriptiveFileRelation.getSource());

			List<Result> results = queryService.createQuery()
					.select(descriptiveFileRelation.getSource().getIDAttribute())
					.selectAll(descriptiveFileRelation.getTarget()).join(descriptiveFileRelation).fetch(filter);

			// Group by component-Id collecting all descriptiveFile records as list in map
			// value
			Map<String, List<Record>> m = results.stream().collect(Collectors.groupingBy(
					r -> r.getRecord(descriptiveFileRelation.getSource()).getID(),
					Collectors.mapping(r -> r.getRecord(descriptiveFileRelation.getTarget()), Collectors.toList())));

			for (Map.Entry<String, List<Record>> e : m.entrySet()) {
				String componentId = e.getKey();

				Value val = ValueType.FILE_RELATION.create(descriptiveFileRelation.getName());
				val.set(ContextState.MEASURED, convertToFileLinks(e.getValue()));

				data.put(componentId, val);
			}
		}

		return data;
	}

	private static class StandaloneMDMFile extends MDMFile {

		protected StandaloneMDMFile(Record record) {
			super(new DefaultCore(record));
		}
	}

	private FileLink[] convertToFileLinks(List<Record> records) {
		List<FileLink> fileLinks = new ArrayList<>();
		for (Record record : records) {
			MDMFile file = new StandaloneMDMFile(record);
			fileLinks.add(FileLink.newRemote(file.getLocation(), new MimeType(file.getFileMimeType()),
					file.getDescription(), file.getSize(), file, FileServiceType.AOFILE));
		}
		return fileLinks.toArray(new FileLink[0]);
	}

	private Optional<Relation> getRelation(List<EntityType> searchableTypes, String column) {
		String[] parts = column.split("\\.");

		if (parts.length != 2) {
			throw new IllegalArgumentException("Cannot parse column " + column + "!");
		}

		String type = parts[0];
		String attributeName = parts[1];

		Optional<EntityType> entityType = searchableTypes.stream()
				.filter(e -> ServiceUtils.workaroundForTypeMapping(e).equalsIgnoreCase(type)).findFirst();

		if (entityType.isPresent()) {
			return entityType.get().getRelations().stream().filter(r -> r.getName().equalsIgnoreCase(attributeName))
					.findFirst();
		} else {
			return Optional.empty();
		}
	}

	private Optional<Attribute> getAttribute(List<EntityType> searchableTypes, String c) {
		String[] parts = c.split("\\.");

		if (parts.length != 2) {
			throw new IllegalArgumentException("Cannot parse column " + c + "!");
		}

		String type = parts[0];
		String attributeName = parts[1];

		Optional<EntityType> entityType = searchableTypes.stream()
				.filter(e -> ServiceUtils.workaroundForTypeMapping(e).equalsIgnoreCase(type)).findFirst();

		if (entityType.isPresent()) {
			return entityType.get().getAttributes().stream().filter(a -> a.getName().equalsIgnoreCase(attributeName))
					.findFirst();
		} else {
			return Optional.empty();
		}
	}

}
