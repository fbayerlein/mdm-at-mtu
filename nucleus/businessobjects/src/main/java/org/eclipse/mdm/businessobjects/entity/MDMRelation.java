/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.List;

import org.eclipse.mdm.api.base.model.ContextType;

import com.google.common.base.MoreObjects;

/**
 * Relation (Entity for relation informations)
 * 
 * @author Alexander Nehmer, science + computing (ATOS SE)
 *
 */
public class MDMRelation {

	/** name of the relation */
	private String name;
	/** type of the relation */
	private RelationType type;
	/** type of the related entity */
	private String entityType;
	/** ContextType if the entityType has one */
	private ContextType contextType;
	/** ids of the related entities */
	private List<String> ids;

	public enum RelationType {
		CHILDREN, MUTABLE, PARENT;
	}

	/**
	 * Default constructor used for entity deserialization
	 */
	public MDMRelation() {

	}

	/**
	 * Constructor
	 * 
	 * @param name        name of the relation
	 * @param type        type of the relation
	 * @param entityType  type of the related entity
	 * @param contextType ContextType of the entity if the entityType has one
	 * @param ids         ids of the related entities
	 */
	public MDMRelation(String name, RelationType type, String entityType, ContextType contextType, List<String> ids) {
		this.name = name;
		this.type = type;
		this.entityType = entityType;
		this.contextType = contextType;
		this.ids = ids;
	}

	/**
	 * Copy constructor
	 * 
	 * @param relation relation to copy
	 */
	public MDMRelation(MDMRelation relation) {
		this.name = relation.getName();
		this.type = relation.getType();
		this.entityType = relation.getEntityType();
		this.contextType = relation.getContextType();
		this.ids = relation.getIds();
	}

	public String getName() {
		return this.name;
	}

	public RelationType getType() {
		return type;
	}

	public String getEntityType() {
		return entityType;
	}

	public ContextType getContextType() {
		return contextType;
	}

	public List<String> getIds() {
		return ids;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("name", name).add("type", type).add("entityType", entityType)
				.add("contextType", contextType).add("ids", ids).toString();
	}
}