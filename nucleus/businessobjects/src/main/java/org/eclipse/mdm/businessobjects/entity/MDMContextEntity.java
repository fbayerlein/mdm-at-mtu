/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.dflt.model.TemplateAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.ValueList;

import com.google.common.base.MoreObjects;

/**
 * MDMEntity (Entity for a business object (contains a list of
 * {@link MDMContextAttribute}s)
 *
 * @author Juergen Kleck, Peak Solution GmbH
 */
public class MDMContextEntity extends MDMEntity {

	/**
	 * sort index of the MDM business object
	 */
	private final Integer sortIndex;
	/**
	 * list of attribute to transfer
	 */
	private List<MDMContextAttribute> attributes;
	/**
	 * list of relations to transfer
	 */
	private List<MDMContextRelation> relations;
	/** boolean flag if this attribute is optional */
	private final Boolean optional;
	/** boolean flag if this attribute is defaultActive */
	private final Boolean defaultActive;
	/** boolean flag if this attribute is teststepseriesvariabel */
	private final Boolean testStepSeriesVariable;

	/**
	 * Constructor.
	 *
	 * @param entity the business object
	 */
	public MDMContextEntity(Entity entity) {
		super(entity);

		// special handling for context component
		final TemplateComponent tplCmp = entity instanceof ContextComponent
				? TemplateComponent.of((ContextComponent) entity).orElse(null)
				: null;
		if (null != tplCmp) {
			this.sortIndex = tplCmp.getSortIndex();
			this.optional = tplCmp.isOptional();
			this.defaultActive = tplCmp.isDefaultActive();
			this.testStepSeriesVariable = tplCmp.isSeriesConstant();
			this.attributes = convertAttributeValues(entity.getValues(), tplCmp.getTemplateAttributes());
			this.relations = convertRelations(entity, tplCmp);
		} else {
			this.sortIndex = 0;
			this.optional = null;
			this.defaultActive = null;
			this.testStepSeriesVariable = null;
			this.attributes = convertAttributeValues(entity.getValues(), null);
			this.relations = convertRelations(entity, null);
		}
	}

	/**
	 * Constructor.
	 *
	 * @param name       Name of the Entity
	 * @param id         ID of the Entity
	 * @param type       Type of the Entity
	 * @param sourceType Source type of the Entity
	 * @param sourceName Source name of the Entity
	 * @param attributes Attributes of the Entity
	 */
	public MDMContextEntity(String name, String id, String type, String sourceType, String sourceName,
			List<MDMContextAttribute> attributes) {
		super(name, id, type, sourceType, sourceName, Collections.emptyList());
		this.sortIndex = 0;
		if (attributes != null) {
			this.attributes = new ArrayList<>(attributes);
		} else {
			this.attributes = new ArrayList<>();
		}
		this.relations = new ArrayList<>();
		this.optional = null;
		this.defaultActive = null;
		this.testStepSeriesVariable = null;
	}

	public Integer getSortIndex() {
		return sortIndex;
	}

	public List<MDMContextAttribute> getAttributes() {
		return Collections.unmodifiableList(this.attributes);
	}

	public List<MDMContextRelation> getRelations() {
		return Collections.unmodifiableList(this.relations);
	}

	public Boolean isOptional() {
		return optional;
	}

	public Boolean isDefaultActive() {
		return defaultActive;
	}

	public Boolean isTestStepSeriesVariable() {
		return testStepSeriesVariable;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("name", getName()).add("id", getId()).add("type", getType())
				.add("sourceType", getSourceType()).add("sourceName", getSourceName())
				.add("attributes", getAttributes()).add("relations", getRelations()).add("sortIndex", getSortIndex())
				.add("optional", isOptional()).add("defaultActive", isDefaultActive())
				.add("testStepSeriesVariable", isTestStepSeriesVariable()).toString();
	}

	/**
	 * converts the MDM business object values to string values
	 *
	 * @param values        values of a MDM business object
	 * @param tplAttributes optional list of template attributes
	 * @return list with converted attribute values
	 */
	private List<MDMContextAttribute> convertAttributeValues(Map<String, Value> values,
			List<TemplateAttribute> tplAttributes) {

		List<MDMContextAttribute> listAttrs = new ArrayList<>();
		for (MDMAttribute attr : super.convertAttributeValues(values)) {
			TemplateAttribute tplAttr = null;

			if (tplAttributes != null && !tplAttributes.isEmpty()) {
				tplAttr = tplAttributes.stream().filter(tpl -> tpl.getName().equals(attr.getName())).findFirst()
						.orElse(null);
			}

			listAttrs.add(toContextAttribute(attr, tplAttr));
		}

		// pre-sort attributes by sort index
		Collections.sort(listAttrs, Comparator.comparing(MDMContextAttribute::getSortIndex,
				Comparator.nullsFirst(Comparator.naturalOrder())));

		return listAttrs;
	}

	private MDMContextAttribute toContextAttribute(MDMAttribute attr, TemplateAttribute tplAttr) {
		Integer sortIndex = null;
		Boolean optional = null;
		Boolean readOnly = null;
		String description = null;
		Boolean valueListRef = null;
		String valueList = null;
		if (tplAttr != null) {
			sortIndex = tplAttr.getCatalogAttribute().getSortIndex();
			description = tplAttr.getCatalogAttribute().getDescription();
			valueListRef = tplAttr.getCatalogAttribute().getValue("ValueListRef").extract();
			valueList = tplAttr.getCatalogAttribute().getValueList().map(ValueList::getID).orElse(null);
			optional = (Boolean) tplAttr.isOptional();
			readOnly = (Boolean) tplAttr.isValueReadOnly();
		}
		return new MDMContextAttribute(attr, sortIndex, readOnly, optional, description, valueListRef, valueList);
	}

	/**
	 * Converts all relations to MDMContextRelations. The potential ContextType of
	 * the children is assumed to be the parent's one.
	 *
	 * @param entity to get {@link MDMContextRelation}s for
	 * @return a list of {@link MDMContextRelation}s
	 */
	private List<MDMContextRelation> convertRelations(Entity entity, TemplateComponent tplCmp) {

		String parentId = null == tplCmp ? null : tplCmp.getParentTemplateComponent().map(p -> p.getID()).orElse(null);

		return convertRelations(entity).stream().map(m -> new MDMContextRelation(m, parentId))
				.collect(Collectors.toList());
	}
}
