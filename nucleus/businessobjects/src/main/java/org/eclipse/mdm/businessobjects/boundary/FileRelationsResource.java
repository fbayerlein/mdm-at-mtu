/* Copyright (c) 2015-2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.businessobjects.control.FileLinkActivity;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.Serializer;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * @author jz
 *
 */
@Tag(name = "FileRelation")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/filerelations")
public class FileRelationsResource<T extends MDMFile> {

	@EJB
	private EntityService entityService;

	@EJB
	private FileRelationsService fileRelationsService;

	@EJB
	private FileLinkActivity<T> fileLinkActivity;

	@SuppressWarnings("unchecked")
	@POST
	@Produces("application/zip")
	@Path("/files")
	public Response streamFileLinks(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {
		RequestBody rqBody = RequestBody.create(body);

		List<FileLink> fileLinks = this.fileRelationsService.getFileLinks(sourceName,
				this.getStringIds(rqBody.getStringValueSupplier("FileRelations").get()));

		return Response.ok(this.fileLinkActivity.toStreamingOutput(sourceName, fileLinks), "application/zip")
				.header("content-disposition", this.getContentDisposition("zip")).build();
	}

	@SuppressWarnings("rawtypes")
	private Map<Class, List<String>> getStringIds(String valueList) {
		Map<Class, List<String>> mapIds = new HashMap<>();
		for (String fr : this.getStringValues(valueList)) {
			Triple<FileServiceType, Class<?>, String> tr = Serializer.getFileLinkComponents(fr);
			if (!mapIds.containsKey(tr.getMiddle())) {
				mapIds.put(tr.getMiddle(), new ArrayList<>());
			}
			mapIds.get(tr.getMiddle()).add(tr.getRight());
		}
		return mapIds;
	}

	private String[] getStringValues(String valueList) {
		return valueList.replace("[", "").replace("]", "").split(",");
	}

	private String getContentDisposition(String fileExt) {
		StringBuilder sb = new StringBuilder("attachment; filename = MDMExport_");
		sb.append(String.format("%1$tF_%1$tH-%1$tM-%1$tS.", new Date()));
		sb.append(fileExt);
		return sb.toString();
	}
}
