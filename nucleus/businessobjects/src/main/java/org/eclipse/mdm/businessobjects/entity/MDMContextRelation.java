/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.List;

import org.eclipse.mdm.api.base.model.ContextType;

/**
 * Relation (Entity for relation information)
 *
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
public class MDMContextRelation extends MDMRelation {

	private String parentId;

	/**
	 * Default constructor used for entity deserialization
	 */
	public MDMContextRelation() {

	}

	/**
	 * Constructor
	 *
	 * @param name        name of the relation
	 * @param type        type of the relation
	 * @param entityType  type of the related entity
	 * @param contextType ContextType of the entity if the entityType has one
	 * @param ids         ids of the related entities
	 * @param parentId    the parent template id
	 */
	public MDMContextRelation(String name, RelationType type, String entityType, ContextType contextType,
			List<String> ids, String parentId) {
		super(name, type, entityType, contextType, ids);
		this.parentId = parentId;
	}

	/**
	 * Constructor
	 *
	 * @param relation MDMRelation
	 * @param parentId parentId
	 */
	public MDMContextRelation(MDMRelation relation, String parentId) {
		super(relation);
		this.parentId = parentId;
	}

	public String getParentId() {
		return parentId;
	}
}