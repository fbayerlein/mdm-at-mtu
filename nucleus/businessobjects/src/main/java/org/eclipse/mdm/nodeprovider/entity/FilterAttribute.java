/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import java.util.List;

import javax.el.ValueExpression;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.nodeprovider.control.AttributeContainer;
import org.eclipse.mdm.nodeprovider.control.MDMExpressionLanguageService;
import org.eclipse.mdm.nodeprovider.control.NodeProviderException;

import com.google.common.base.MoreObjects;

public class FilterAttribute {

	private Attribute attribute;
	private ComparisonOperator operator;
	private String value;

	private ValueExpression cached;

	public FilterAttribute(Attribute attribute) {
		this(attribute, ComparisonOperator.EQUAL, "${" + attribute.getName() + "}");
	}

	public FilterAttribute(Attribute attribute, ComparisonOperator operator, String value) {
		this.attribute = attribute;
		if (attribute.getValueType().isSequence() && operator == ComparisonOperator.EQUAL) {
			this.operator = ComparisonOperator.IN_SET;
		} else {
			this.operator = operator;
		}
		this.value = value;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public ComparisonOperator getOperator() {
		return operator;
	}

	public String getValue() {
		return value;
	}

	public Condition getCondition(MDMExpressionLanguageService elService,
			List<AttributeContainer> attributeContainers) {

		if (cached == null) {
			cached = elService.parseValueExpression(value, attribute.getValueType().toSingleType().getValueClass());
		}

		Object value = elService.evaluateValueExpression(cached, attributeContainers);

		if (attribute.getValueType().isSequence()) {
			/*
			 * AttributeContainer only contains a single value, even for SEQUENCE
			 * attributes. But to create a condition, we have to convert the value to the
			 * datatype of the attribute.
			 */
			try {
				return operator.create(attribute, toArray(value));
			} catch (Exception e) {
				throw new NodeProviderException("Cannot create condition from value " + value, e);
			}
		} else {
			return operator.create(attribute, value);
		}
	}

	/**
	 * Converts the given single value to an array value, e.g. converting int to
	 * int[], String to String[] and so on. The resulting array will have a only one
	 * value, which is the given value.
	 * 
	 * @param value
	 * @return an array of the type of value, with value as its only entry.
	 */
	private Object toArray(Object value) {
		if (value == null) {
			return null;
		} else if (value instanceof Byte) {
			return new byte[] { (byte) value };
		} else if (value instanceof Short) {
			return new short[] { (short) value };
		} else if (value instanceof Integer) {
			return new int[] { (int) value };
		} else if (value instanceof Long) {
			return new long[] { (long) value };
		} else if (value instanceof Float) {
			return new float[] { (float) value };
		} else if (value instanceof Double) {
			return new double[] { (double) value };
		} else if (value instanceof Boolean) {
			return new boolean[] { (boolean) value };
		} else if (value instanceof String) {
			return new String[] { (String) value };
		} else {
			throw new NodeProviderException("Cannot convert value of type " + value.getClass().getSimpleName()
					+ " to array. Only Byte, Short, Integer, Long, Float, Double, Boolean and String are supported.");
		}
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(FilterAttribute.class).add("attribute", attribute.getName())
				.add("operator", operator).add("value", value).toString();
	}
}
