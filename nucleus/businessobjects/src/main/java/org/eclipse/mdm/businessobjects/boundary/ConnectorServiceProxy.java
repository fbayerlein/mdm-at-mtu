/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.connector.boundary.ConnectorService;

/**
 * The @SessionScoped ConnectorService cannot be directly injected into JAX-RS
 * resources. So this EJB can be used instead.
 *
 */
@Stateless
public class ConnectorServiceProxy {
	@Inject
	private ConnectorService connectorService;

	/**
	 * returns an {@link ApplicationContext} identified by the given name
	 *
	 * @param name source name (e.g. MDM {@link Environment} name)
	 * @return the matching {@link ApplicationContext}
	 * @see ConnectorService#getContextByName(String)
	 */
	public ApplicationContext getContextByName(String name) {
		return this.connectorService.getContextByName(name);
	}
}
