/* Copyright (c) 2015-2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.MeaResultFile;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestFile;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.TestStepFile;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.connector.boundary.ConnectorService;

/**
 * @author jz
 *
 */
@Stateless
public class FileRelationsService<T extends MDMFile> {

	@Inject
	private ConnectorService connectorService;

	@EJB
	private EntityService entityService;

	public FileRelationsService() {

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<FileLink> getFileLinks(String sourceName, Map<Class<T>, List<String>> mapIds) {
		List<FileLink> fileLinks = new ArrayList<>();
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		EntityManager em = context.getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		SearchService searchService = context.getSearchService()
				.orElseThrow(() -> new MDMEntityAccessException("Search service not present!"));
		ModelManager mm = context.getModelManager()
				.orElseThrow(() -> new MDMEntityAccessException("Model manager not present!"));

		for (Class entityClass : mapIds.keySet()) {
			if (entityClass.equals(DescriptiveFile.class)) {
				for (Object file : em.load(entityClass, mapIds.get(entityClass))) {
					fileLinks.add(FileLink.newRemote("dummy", new MimeType("application/x-openmdm.link"), "", 0, file,
							FileServiceType.AOFILE));
				}
			} else {
				String entityName = entityClass.getName().substring(entityClass.getName().lastIndexOf(".") + 1);
				Filter filter = Filter.and()
						.add(ComparisonOperator.IN_SET.create(mm.getEntityType(entityName).getAttribute("Id"),
								mapIds.get(entityClass).toArray(new String[0])));
				if (entityClass.equals(TestStepFile.class)) {
					List<TestStep> mdmFileObjs = searchService.fetch(TestStep.class, filter);
					if (!mdmFileObjs.isEmpty()) {
						mdmFileObjs.forEach(testStep -> testStep.getMDMFiles(em).forEach(file -> {
							if (mapIds.get(entityClass).contains(file.getID())) {
								fileLinks.add(FileLink.newRemote("dummy", new MimeType("application/x-openmdm.link"),
										"", 0, file, FileServiceType.AOFILE));
							}
						}));
					}
				} else if (entityClass.equals(TestFile.class)) {
					List<Test> mdmFileObjs = searchService.fetch(Test.class, filter);
					if (!mdmFileObjs.isEmpty()) {
						mdmFileObjs.forEach(test -> test.getMDMFiles(em).forEach(file -> {
							if (mapIds.get(entityClass).contains(file.getID())) {
								fileLinks.add(FileLink.newRemote("dummy", new MimeType("application/x-openmdm.link"),
										"", 0, file, FileServiceType.AOFILE));
							}
						}));
					}
				} else if (entityClass.equals(MeaResultFile.class)) {
					List<Measurement> mdmFileObjs = searchService.fetch(Measurement.class, filter);
					if (!mdmFileObjs.isEmpty()) {
						mdmFileObjs.forEach(meaResult -> meaResult.getMDMFiles(em).forEach(file -> {
							if (mapIds.get(entityClass).contains(file.getID())) {
								fileLinks.add(FileLink.newRemote("dummy", new MimeType("application/x-openmdm.link"),
										"", 0, file, FileServiceType.AOFILE));
							}
						}));
					}
				}
			}
		}
		return fileLinks;
	}
}
