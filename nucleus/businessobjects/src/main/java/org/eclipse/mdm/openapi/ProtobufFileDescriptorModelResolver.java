/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.openapi;

import static io.swagger.v3.core.util.RefUtils.constructRef;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.google.protobuf.AbstractMessage;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.DescriptorProtos.DescriptorProto;
import com.google.protobuf.DescriptorProtos.EnumDescriptorProto;
import com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto;
import com.google.protobuf.DescriptorProtos.FieldDescriptorProto;
import com.google.protobuf.Descriptors.FieldDescriptor;

import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.jackson.ModelResolver;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.BooleanSchema;
import io.swagger.v3.oas.models.media.ByteArraySchema;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.MapSchema;
import io.swagger.v3.oas.models.media.NumberSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;

/**
 * Swagger {@link ModelResolver} for protobuf messages. This class reads the
 * protobuf descriptor file and resolves the types to a Swagger {@link Schema}.
 *
 */
@SuppressWarnings("rawtypes")
public class ProtobufFileDescriptorModelResolver implements ModelConverter {
	private static final Logger LOG = LoggerFactory.getLogger(ProtobufFileDescriptorModelResolver.class);

	private static final String DESCRIPTOR_URL = "/mdm.desc";

	private DescriptorProtos.FileDescriptorSet fileDescSet;

	public ProtobufFileDescriptorModelResolver() throws IOException {
		super();
		this.fileDescSet = loadDescriptor();
	}

	public ProtobufFileDescriptorModelResolver(File descriptor) throws IOException {
		super();
		try (InputStream in = new FileInputStream(descriptor)) {
			this.fileDescSet = DescriptorProtos.FileDescriptorSet.parseFrom(in);
		}
	}

	private DescriptorProtos.FileDescriptorSet loadDescriptor() throws IOException {
		InputStream in = ProtobufFileDescriptorModelResolver.class.getResourceAsStream(DESCRIPTOR_URL);
		if (in == null) {
			throw new RuntimeException(
					"FileDescriptor " + DESCRIPTOR_URL + " not found! Swagger UI schema may be incomplete");
		} else {
			return DescriptorProtos.FileDescriptorSet.parseFrom(in);
		}
	}

	private boolean shouldIgnoreClass(Type type) {
		if (type instanceof Class) {
			return shouldIgnoreClass((Class<?>) type);
		} else if (type instanceof SimpleType) {
			return shouldIgnoreClass(((SimpleType) type).getRawClass());
		}
		return false;
	}

	private boolean shouldIgnoreClass(Class<?> clazz) {
		return clazz.equals(javax.ws.rs.core.Response.class) || clazz.getPackage() == null
				|| (!clazz.getPackage().getName().startsWith("org.eclipse.mdm.protobuf")
						&& !clazz.getPackage().getName().startsWith("com.google.protobuf"));
	}

	@Override
	public Schema resolve(AnnotatedType type, ModelConverterContext context, Iterator<ModelConverter> chain) {
		if (!this.shouldIgnoreClass(type.getType())) {
			Optional<EnumDescriptorProto> ed = getEnumDescriptor(type.getType());

			if (ed.isPresent()) {
				return resolveEnum(ed.get(), type, context, chain);
			}

			DescriptorProto d = getDescriptor(type.getType());
			if (d != null) {
				return resolve(d, context, chain);
			}
		}

		if (chain != null && chain.hasNext()) {
			return chain.next().resolve(type, context, chain);
		} else {
			return null;
		}
	}

	private Schema resolveEnum(EnumDescriptorProto desc, AnnotatedType type, ModelConverterContext context,
			Iterator<ModelConverter> next) {
		List<Schema> list = new ArrayList<>();
		for (EnumValueDescriptorProto e : desc.getValueList()) {
			Schema item = new Schema<>();
			item.setTitle(e.getName());
			item.description("Ordinal value:" + e.getNumber());
			list.add(item);
		}

		Schema<String> enumSchema = new Schema<>();
		enumSchema.name(desc.getName()).type("string");
		enumSchema.oneOf(list);

		context.defineModel(desc.getName(), enumSchema, type, null);
		return enumSchema;
	}

	private Schema resolve(DescriptorProto desc, ModelConverterContext context, Iterator<ModelConverter> chain) {
		if (desc == null) {
			return null;
		}

		Schema<?> model = new Schema<>().name(desc.getName()).type("object");

		for (FieldDescriptorProto fieldDesc : desc.getFieldList()) {
			Schema prop = resolveField(fieldDesc, context, chain, desc);

			if (isRepeated(fieldDesc) && !isMapField(fieldDesc)) {
				// Map fields are repeated, but are modeled as message, not as array
				model.addProperty(fieldDesc.getName(), new ArraySchema().items(prop));
			} else {
				model.addProperty(fieldDesc.getName(), prop);
			}

			if (isRequired(fieldDesc)) {
				model.addRequiredItem(fieldDesc.getName());
			}
		}

		return model;
	}

	private Schema resolveField(FieldDescriptorProto desc, ModelConverterContext context, Iterator<ModelConverter> next,
			DescriptorProto parent) {
		switch (getType(desc)) {
		case BOOL:
			return new BooleanSchema();
		case BYTES:
			return new ByteArraySchema();
		case DOUBLE:
			return new NumberSchema();
		case ENUM:
			try {
				Schema s = context.resolve(new AnnotatedType(findType(desc.getTypeName())));
				return new Schema().$ref(constructRef(s.getName()));
			} catch (ClassNotFoundException e) {
				// fallback to string
				return new StringSchema();
			}
		case FLOAT:
			return new NumberSchema();
		case STRING:
			return new StringSchema();
		case MESSAGE:
			return resolveMessage(desc, context, next);
		case FIXED32:
		case FIXED64:
		case INT32:
		case INT64:
		case SFIXED32:
		case SFIXED64:
		case SINT32:
		case SINT64:
		case UINT32:
		case UINT64:
			return new IntegerSchema();
		default:
			throw new RuntimeException("Not supported yet: " + desc.getType() + "." + desc.getType());
		}
	}

	private Schema resolveMessage(FieldDescriptorProto desc, ModelConverterContext context,
			Iterator<ModelConverter> next) {
		if (desc.getTypeName().endsWith("SelectStatement")) {
			return null;
		}

		try {
			if (isMapField(desc)) {
				DescriptorProto mapEntry = findByProtoName(desc.getTypeName());

				FieldDescriptorProto mapValue = mapEntry.getFieldList().stream()
						.filter(f -> f.getName().equals("value")).findFirst().get();

				Schema addPropertiesSchema = resolveField(mapValue, context, next, mapEntry);

				String pName = "XXX";
				if (addPropertiesSchema != null) {
					if (StringUtils.isNotBlank(addPropertiesSchema.getName())) {
						pName = addPropertiesSchema.getName();
					}
					if ("object".equals(addPropertiesSchema.getType()) && pName != null) {
						// create a reference for the items
						if (context.getDefinedModels().containsKey(pName)) {
							addPropertiesSchema = new Schema().$ref(constructRef(pName));
						}
					} else if (addPropertiesSchema.get$ref() != null) {
						addPropertiesSchema = new Schema().$ref(
								StringUtils.isNotEmpty(addPropertiesSchema.get$ref()) ? addPropertiesSchema.get$ref()
										: addPropertiesSchema.getName());
					}
				}
				Schema mapModel = new MapSchema().additionalProperties(addPropertiesSchema);
				mapModel.name(desc.getName());
				return mapModel;
			} else {
				// array
				Type type = findType(desc.getTypeName());
				Schema prop = context.resolve(new AnnotatedType(type));
				return new Schema().$ref(constructRef(prop.getName()));
			}
		} catch (Exception e) {
			LOG.warn("Error resolving field {}.{}!", desc.getType(), desc.getName());
			throw new RuntimeException("Error resolving field " + desc.getType() + "." + desc.getName(), e);
		}
	}

	private boolean isRepeated(FieldDescriptorProto proto) {
		return proto.getLabel() == FieldDescriptorProto.Label.LABEL_REPEATED;
	}

	private boolean isRequired(FieldDescriptorProto proto) {
		return proto.getLabel() == FieldDescriptorProto.Label.LABEL_REQUIRED;
	}

	private boolean isMapField(FieldDescriptorProto proto) {
		return getType(proto) == FieldDescriptor.Type.MESSAGE && isRepeated(proto)
				&& findByProtoName(proto.getTypeName()).getOptions().getMapEntry();
	}

	private FieldDescriptor.Type getType(FieldDescriptorProto proto) {
		if (proto.hasType()) {
			return FieldDescriptor.Type.valueOf(proto.getType());
		}
		return null;
	}

	private Optional<EnumDescriptorProto> getEnumDescriptor(Type type) {
		for (DescriptorProtos.FileDescriptorProto fd : fileDescSet.getFileList()) {
			String outerClassName;

			if (fd.getPackage().equals("org.eclipse.mdm.protobuf")) {
				outerClassName = "Mdm";
			} else {
				outerClassName = "Mdm";
			}
			for (EnumDescriptorProto ed : fd.getEnumTypeList()) {
				String enumClassName = fd.getPackage() + "." + outerClassName + "$" + ed.getName();
				if (type.getTypeName().equals(enumClassName)) {
					return Optional.of(ed);
				}
				if (type instanceof SimpleType && ((SimpleType) type).getTypeName().equals(enumClassName)) {
					return Optional.of(ed);
				}
			}
			for (DescriptorProto d : fd.getMessageTypeList()) {
				String newOuterClassName = fd.getPackage() + "." + outerClassName + "$" + d.getName();
				Optional<EnumDescriptorProto> o = getEnumDescriptor(d, type, newOuterClassName);
				if (o.isPresent()) {
					return o;
				}
			}
		}
		return Optional.empty();
	}

	private Optional<EnumDescriptorProto> getEnumDescriptor(DescriptorProto desc, Type type, String outerClassName) {
		for (EnumDescriptorProto ed : desc.getEnumTypeList()) {
			String enumClassName = outerClassName + "$" + ed.getName();
			if (type.getTypeName().equals(enumClassName)) {
				return Optional.of(ed);
			}
		}

		for (DescriptorProto d : desc.getNestedTypeList()) {
			String newOuterClassName = outerClassName + "$" + d.getName();
			Optional<EnumDescriptorProto> o = getEnumDescriptor(d, type, newOuterClassName);
			if (o.isPresent()) {
				return o;
			}
		}

		return Optional.empty();
	}

	@SuppressWarnings("unchecked")
	private DescriptorProto getDescriptor(Type type) {
		try {
			final Class<AbstractMessage> cls;

			if (type instanceof SimpleType) {
				cls = (Class<AbstractMessage>) ((SimpleType) type).getRawClass();
			} else if (type instanceof CollectionType) {
				// cls = (Class<AbstractMessage>) ((CollectionType) type).getRawClass();
				return null;
			} else {
				try {
					cls = (Class<AbstractMessage>) Class.forName(type.getTypeName());
				} catch (ClassNotFoundException e) {
					return null;
				}
			}

			return findByClassName(cls.getName());
		} catch (Exception e) {
			LOG.warn("Cannot get Descriptor for type {}!", type, e);
			return null;
		}
	}

	protected DescriptorProto findByProtoName(String name) {
		if (name.startsWith(".")) {
			name = name.replaceFirst("\\.", "");
		}
		for (DescriptorProtos.FileDescriptorProto fd : fileDescSet.getFileList()) {

			DescriptorProto found = findByProtoName(fd.getMessageTypeList(), fd.getPackage(), name);
			if (found != null) {
				return found;
			}
		}
		throw new RuntimeException("Descriptor for '" + name + "' not found!");
	}

	private DescriptorProto findByProtoName(List<DescriptorProto> descList, String prefix, String name) {
		for (DescriptorProtos.DescriptorProto desc : descList) {
			if (name.equals(prefix + "." + desc.getName())) {
				return desc;
			}

			if (name.startsWith(prefix + "." + desc.getName())) {
				DescriptorProto found = findByProtoName(desc.getNestedTypeList(), prefix + "." + desc.getName(), name);
				if (found != null) {
					return found;
				}
			}
		}

		return null;
	}

	protected DescriptorProto findByClassName(String name) {
		for (DescriptorProtos.FileDescriptorProto fd : fileDescSet.getFileList()) {
			if (name.startsWith("org.eclipse.mdm.protobuf") && fd.getPackage().equals("org.eclipse.mdm.protobuf")) {
				return findByClassName(fd.getMessageTypeList(), fd.getPackage() + ".Mdm$", name);
			} else if (name.startsWith("com.google.protobuf") && fd.getPackage().equals("google.protobuf")) {
				return findByClassName(fd.getMessageTypeList(), "com.google.protobuf.", name);
			}
		}

		throw new RuntimeException("Descriptor for " + name + " not found!");
	}

	private DescriptorProto findByClassName(List<DescriptorProto> descList, String prefix, String name) {
		for (DescriptorProtos.DescriptorProto desc : descList) {
			if (name.equals(prefix + desc.getName())) {
				return desc;
			}
			for (DescriptorProtos.DescriptorProto nestedDesc : desc.getNestedTypeList()) {
				if (name.startsWith(prefix + desc.getName() + "$" + nestedDesc.getName())) {
					return findByClassName(desc.getNestedTypeList(), prefix + desc.getName() + "$", name);
				}
			}
		}

		return null;
	}

	protected Type findType(String protoName) throws ClassNotFoundException {
		if (protoName.startsWith(".")) {
			protoName = protoName.replaceFirst("\\.", "");
		}

		if (protoName.startsWith("org.eclipse.mdm.protobuf")) {
			protoName = protoName.replace("org.eclipse.mdm.protobuf", "");
			protoName = "org.eclipse.mdm.protobuf.Mdm" + protoName.replaceAll("\\.", "\\$");
		} else if (protoName.startsWith("google.protobuf.")) {
			protoName = protoName.replace("google.protobuf.", "");
			protoName = "com.google.protobuf." + protoName.replaceAll("\\.", "\\$");
		} else {
			protoName = protoName.replace("org.eclipse.mdm.protobuf", "");
			protoName = "org.eclipse.mdm.protobuf.Mdm" + protoName.replaceAll("\\.", "\\$");
		}

		return Class.forName(protoName);
	}
}