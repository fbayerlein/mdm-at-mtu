/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.dflt.model.SystemParameter;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.collection.List;
import io.vavr.control.Try;

/**
 * {@link SystemParameter} resource
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Tag(name = "SystemParameter")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/systemparameters")
public class SystemParameterResource {

	@EJB
	private SystemParameterService systemParameterService;

	@EJB
	private EntityService entityService;

	/**
	 * delegates the request to the {@link ParameterSetService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link ParameterSet} result
	 * @return the result of the delegated request as {@link Response}
	 */

	@GET
	@Operation(summary = "Find Par by filter", description = "Get list of systemparameter", responses = {
			@ApiResponse(description = "The systemparameter", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getParameters(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.Parameter(description = "Filter expression", required = false) @QueryParam("filter") String filter) {

		return Try.of(() -> systemParameterService.getParameters(sourceName, filter)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, SystemParameter.class)).get();
	}
}
