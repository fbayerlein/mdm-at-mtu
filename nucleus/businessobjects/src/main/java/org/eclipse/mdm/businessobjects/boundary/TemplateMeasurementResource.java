/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurement;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.control.Try;

/**
 * {@link TemplateMeasurement} resource
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Tag(name = "Template")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/tplmearesults")
public class TemplateMeasurementResource {

	@EJB
	private ConnectorServiceProxy connectorService;

	@EJB
	private EntityService entityService;

	/**
	 * Returns the found {@link TemplateMeasurement}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateMeasurement}
	 * @return the found {@link TemplateMeasurement} as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response find(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName, @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), TemplateMeasurement.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the found {@link TemplateMeasurement}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateMeasurement}
	 * @return the found {@link TemplateMeasurement} as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@QueryParam("filter") String filter) {
		return entityService.findAll(V(sourceName), TemplateMeasurement.class, filter)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created {@link TemplateMeasurement}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link TemplateMeasurement} to create.
	 * @return the created {@link TemplateMeasurement} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new TemplateMeasurement", responses = {
			@ApiResponse(description = "The created TemplateMeasurement", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {

		return entityService
				.create(V(sourceName), TemplateMeasurement.class, entityService.extractRequestBody(body, sourceName))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link TemplateMeasurement} with all parameters set in the given
	 * JSON body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TemplateMeasurement} to
	 *                   update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link TemplateMeasurement}
	 */
	@PUT
	@Operation(summary = "Update an existing TemplateMeasurement", description = "Updates the TemplateMeasurement with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated TemplateMeasurement", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateMeasurement", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		TemplateMeasurement tplMea = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class))
				.loadWithoutChildren(TemplateMeasurement.class, id);

		return entityService.update(V(sourceName), Try.of(() -> tplMea), requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}
