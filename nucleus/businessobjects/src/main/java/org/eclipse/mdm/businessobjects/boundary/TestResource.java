/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ATTRIBUTENAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTCOMPONENTNAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTGROUP;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTTYPE;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestFile;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Classification;
import org.eclipse.mdm.api.dflt.model.Domain;
import org.eclipse.mdm.api.dflt.model.MDMTag;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.ProjectDomain;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.control.FileLinkActivity;
import org.eclipse.mdm.businessobjects.entity.ContextResponse;
import org.eclipse.mdm.businessobjects.entity.I18NResponse;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.SearchAttributeResponse;
import org.eclipse.mdm.businessobjects.service.ContextService;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.Value;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.collection.Vector;
import io.vavr.control.Try;

/**
 * {@link Test} resource
 *
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Tag(name = "Test")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/tests")
public class TestResource {

	private static final String ATTR_WITHTESTSTEPS = "WithTestSteps";

	@EJB
	private TestService testService;

	@EJB
	private ClassificationService classificationService;

	@EJB
	private EntityService entityService;

	@EJB
	private ContextService contextService;

	@EJB
	private FileLinkActivity fileLinkActivity;

	@EJB
	private MDMTagService mdmTagService;

	@Context
	private ResourceContext resourceContext;

	@EJB
	private ConnectorServiceProxy connectorService;

	/**
	 * delegates the request to the {@link TestService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link Test} result
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find Tests by filter", description = "Get list of Tests", responses = {
			@ApiResponse(description = "The projects", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTests(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "Filter expression", required = false) @QueryParam("filter") String filter) {

		return Try.of(() -> testService.getTests(sourceName, filter)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, Test.class)).get();
	}

	/**
	 * delegates the request to the {@link TestService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the search attributes", description = "Get a list of search attributes", responses = {
			@ApiResponse(description = "The search attributes", content = @Content(schema = @Schema(implementation = SearchAttributeResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchattributes")
	public Response getSearchAttributes(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try.of(() -> testService.getSearchAttributes(sourceName))
				.map(attrs -> ServiceUtils.toResponse(new SearchAttributeResponse(attrs), Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link TestService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link Test}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a Test by ID", description = "Returns Test based on ID", responses = {
			@ApiResponse(description = "The Test", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response findTest(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Test", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), Test.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link TestService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the Test localizations", description = "Returns Test localizations", responses = {
			@ApiResponse(description = "The Test localizations", content = @Content(schema = @Schema(implementation = I18NResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/localizations")
	@Deprecated
	public Response localize(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		Map<Attribute, String> localizedAttributeMap = this.testService.localizeAttributes(sourceName);
		Map<EntityType, String> localizedEntityTypeMap = this.testService.localizeType(sourceName);
		return ServiceUtils.toResponse(new I18NResponse(localizedEntityTypeMap, localizedAttributeMap), Status.OK);

	}

	/**
	 * Returns the created {@link Test}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link Test} to create.
	 * @return the created {@link Test} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new Test", responses = {
			@ApiResponse(description = "The created Test", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {
		// TODO

		Seq<Value<?>> extractRequestBody = entityService.extractRequestBody(body, sourceName,
				io.vavr.collection.List.of(Pool.class, org.eclipse.mdm.api.dflt.model.Status.class, TemplateTest.class,
						ProjectDomain.class, Domain.class, Classification.class));

		java.util.List<Value<?>> asJavaMutable = extractRequestBody.asJavaMutable();

		org.eclipse.mdm.api.dflt.model.Status status = null;
		ProjectDomain projectDomain = null;
		Domain domain = null;
		Pool pool = null;
		Classification classification = null;
		TemplateTest templateTest = null;

		Seq<Value<?>> finalAttrSeq = Vector.empty();

		for (Value<?> v : asJavaMutable) {
			if (v.get() instanceof org.eclipse.mdm.api.dflt.model.Status) {
				status = (org.eclipse.mdm.api.dflt.model.Status) v.get();
				extractRequestBody.remove(v);
			} else if (v.get() instanceof ProjectDomain) {
				projectDomain = (ProjectDomain) v.get();
				extractRequestBody.remove(v);
			} else if (v.get() instanceof Domain) {
				domain = (Domain) v.get();
				extractRequestBody.remove(v);
			} else if (v.get() instanceof Classification) {
				classification = (Classification) v.get();
				extractRequestBody.remove(v);
			} else {
				finalAttrSeq = finalAttrSeq.append(v);
				if (v.get() instanceof Pool) {
					pool = (Pool) v.get();
				} else if (v.get() instanceof TemplateTest) {
					templateTest = (TemplateTest) v.get();
				}
			}
		}

		if (status != null && projectDomain != null && domain != null && classification == null) {
			classification = testService.getClassification(sourceName, status, projectDomain, domain);
		} else if (templateTest != null && pool != null && classification == null) {
			classification = classificationService.getClassification(sourceName, "Defining", pool.getID());
		}

		if (classification != null) {
			finalAttrSeq = finalAttrSeq.append(V(classification));
		}

		RequestBody requestBody = RequestBody.create(body);

		try {
			Try<String> stringValueSupplier = requestBody.getStringValueSupplier(ATTR_WITHTESTSTEPS);
			finalAttrSeq = finalAttrSeq.append(V(Boolean.valueOf(stringValueSupplier.get())));
		} catch (NoSuchElementException e) {
			// If with TestStep not exist, do nothing
		}

		return entityService.create(V(sourceName), Test.class, finalAttrSeq)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link Test} with all parameters set in the given JSON body of
	 * the request.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TestValue} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link Test}
	 */
	@PUT
	@Operation(summary = "Update an existing Test", description = "Updates the Test with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated TestStep", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	@RolesAllowed({ "Admin", "DescriptiveDataAuthor" })
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Test", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		Test test = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class))
				.loadWithoutChildren(Test.class, id);

		return entityService.update(V(sourceName), Try.of(() -> test), requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the status of a {@link Test}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link Test} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link Test}
	 */
	@PUT
	@Operation(summary = "Update the status of an existing Test", description = "Updates the Status of the Test.", responses = {
			@ApiResponse(description = "The updated Test", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/status")
	@RolesAllowed({ "Admin", "DescriptiveDataAuthor" })
	public Response updateStatus(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Test", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);
		String statusId = requestBody.getStringValueSupplier("Status").get().toString();

		if (statusId != null && !statusId.isEmpty()) {
			Classification classification = this.classificationService.getClassification(sourceName, statusId, id,
					null);
			requestBody = RequestBody.create("{\"Classification\":\"" + classification.getID() + "\"}");
		}

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), Test.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link Test}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link Test} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing Test", responses = {
			@ApiResponse(description = "The deleted Test", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response delete(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Test", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), Test.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	@Path("/{" + REQUESTPARAM_ID + "}/contexts/{" + REQUESTPARAM_CONTEXTTYPE + "}/{" + REQUESTPARAM_CONTEXTGROUP + "}/{"
			+ REQUESTPARAM_CONTEXTCOMPONENTNAME + "}/{" + REQUESTPARAM_ATTRIBUTENAME + "}/files")
	public ContextFilesSubresource<DescriptiveFile> getContextFilesSubresource() {
		ContextFilesSubresource<DescriptiveFile> resource = resourceContext.getResource(ContextFilesSubresource.class);
		resource.setEntityClass(TestStep.class);
		resource.setReferringEntityClass(Test.class);
		return resource;
	}

	@Path("/{" + REQUESTPARAM_ID + "}/files/")
	public FilesAttachableSubresource<TestFile> getFilesAttachableSubresource() {
		FilesAttachableSubresource<TestFile> resource = resourceContext.getResource(FilesAttachableSubresource.class);
		resource.setEntityClass(Test.class);
		return resource;
	}

	/**
	 * delegates the request to the {@link TestStepService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TestStep}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the test constant context data for a Test", description = "Returns the test constant context", responses = {
			@ApiResponse(responseCode = "200", description = "The test constant context data", content = @Content(schema = @Schema(implementation = ContextResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts")
	public Response findContext(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id) {
		return contextService.getTestContext(V(sourceName), V(id), false).map(ServiceUtils::contextMapToJava)
				.map(ContextResponse::new).map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK))
				.get();
	}

	/**
	 * Updates the context of {@link TestStep} with all parameters set in the given
	 * JSON body of the request.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TestStep} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the context map of the updated {@link TestStep}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts")
	@RolesAllowed({ "Admin", "DescriptiveDataAuthor" })
	public Response updateContext(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id, String body) {

		return entityService.find(V(sourceName), TestStep.class, V(id))
				.map(testStep -> contextService.updateContext(body, testStep)).map(ContextResponse::new)
				.map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link TestService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link Test}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get MDMTags of a Test by ID", description = "Returns MDMTags  based on ID of Test", responses = {
			@ApiResponse(description = "The MDMTags", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/mdmtags")
	public Response getMDMTags(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Test", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return Try.of(() -> mdmTagService.getMDMTags(sourceName, id, Test.class)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, MDMTag.class)).get();
	}

}
