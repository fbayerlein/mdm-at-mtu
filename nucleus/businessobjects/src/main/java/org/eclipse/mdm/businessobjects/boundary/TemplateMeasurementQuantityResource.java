/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurement;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurementQuantity;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.Value;
import io.vavr.collection.Seq;
import io.vavr.collection.Vector;

/**
 * {@link TemplateMeasurementQuantity} resource
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Tag(name = "Template")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/tplmeaquantity")
public class TemplateMeasurementQuantityResource {

	@EJB
	private EntityService entityService;

	/**
	 * Returns the found {@link TemplateMeasurementQuantity}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateMeasurementQuantity}
	 * @return the found {@link TemplateMeasurementQuantity} as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response find(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName, @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), TemplateMeasurementQuantity.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the found {@link TemplateMeasurementQuantity}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateMeasurementQuantity}
	 * @return the found {@link TemplateMeasurementQuantity} as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@QueryParam("filter") String filter) {
		return entityService.findAll(V(sourceName), TemplateMeasurementQuantity.class, filter)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created {@link TemplateMeasurementQuantity}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link TemplateMeasurementQuantity} to create.
	 * @return the created {@link TemplateMeasurementQuantity} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new TemplateMeasurementQuantity", responses = {
			@ApiResponse(description = "The created TemplateMeasurementQuantity", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {
		Seq<Value<?>> extractRequestBody = entityService.extractRequestBody(body, sourceName,
				io.vavr.collection.List.of(Quantity.class));

		java.util.List<Value<?>> asJavaMutable = extractRequestBody.asJavaMutable();
		Seq<Value<?>> finalAttrSeq = Vector.empty();

		for (Value<?> v : asJavaMutable) {
			finalAttrSeq = finalAttrSeq.append(v);
		}

		return entityService.create(V(sourceName), TemplateMeasurementQuantity.class, finalAttrSeq)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Deletes and returns the deleted {@link TemplateMeasurementQuantity}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link TemplateMeasurementQuantity}
	 *                   to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing TemplateMeasurementQuantity", responses = {
			@ApiResponse(description = "The deleted TemplateMeasurementQuantity", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	@RolesAllowed({ "Admin", "DescriptiveDataAuthor" })
	public Response delete(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateMeasurementQuantity", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService
				.delete(V(sourceName), entityService.find(V(sourceName), TemplateMeasurementQuantity.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the {@link TemplateMeasurement} with all parameters set in the given
	 * JSON body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TemplateMeasurement} to
	 *                   update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link TemplateMeasurement}
	 */
	@PUT
	@Operation(summary = "Update an existing TemplateMeasurementQuantity", description = "Updates the TemplateMeasurementQuantity with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated TemplateMeasurementQuantity", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateMeasurement", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), TemplateMeasurementQuantity.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}
