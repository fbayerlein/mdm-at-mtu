/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.utils.dto;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.nodeprovider.entity.NodeLevel;
import org.eclipse.mdm.nodeprovider.entity.Order;
import org.eclipse.mdm.nodeprovider.entity.ValuePrecision;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * DTO for {@link NodeLevel} used for serialization.
 *
 */
@JsonInclude(value = Include.NON_EMPTY)
public class NodeLevelDTO {
	private String type;

	@JsonDeserialize(contentUsing = StringOrAttributeFilterDeserializer.class)
	private List<FilterAttributeDTO> filterAttributes;
	private List<String> labelAttributes;
	private String labelExpression;
	private ValuePrecision valuePrecision;
	private Map<String, Order> orderAttributes = new HashMap<>();

	private ContextState contextState = null;

	private boolean isVirtual;

	private NodeLevelDTO child;

	public static class StringOrAttributeFilterDeserializer extends JsonDeserializer<FilterAttributeDTO> {
		@Override
		public FilterAttributeDTO deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			JsonToken jsonToken = p.getCurrentToken();
			if (jsonToken == JsonToken.VALUE_STRING) {
				return new FilterAttributeDTO(p.getValueAsString(), "eq", "${" + p.getValueAsString() + "}");
			} else {
				return p.readValueAs(FilterAttributeDTO.class);
			}
		}
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the idAttribute
	 */
	public List<FilterAttributeDTO> getFilterAttributes() {
		return filterAttributes;
	}

	/**
	 * @param filterAttributes the idAttribute to set
	 */
	public void setFilterAttributes(List<FilterAttributeDTO> filterAttribute) {
		this.filterAttributes = filterAttribute;
	}

	/**
	 * @return the labelAttribute
	 */
	public List<String> getLabelAttributes() {
		return labelAttributes;
	}

	/**
	 * @param labelAttributes the labelAttribute to set
	 */
	public void setLabelAttributes(List<String> labelAttributes) {
		this.labelAttributes = labelAttributes;
	}

	/**
	 * @return the orderAttributes
	 */
	public Map<String, Order> getOrderAttributes() {
		return orderAttributes;
	}

	/**
	 * @param orderAttributes the orderAttributes to set
	 */
	public void setOrderAttributes(Map<String, Order> orderAttributes) {
		this.orderAttributes = orderAttributes;
	}

	/**
	 * @return the contextState
	 */
	public ContextState getContextState() {
		return contextState;
	}

	/**
	 * @param contextState the contextState to set
	 */
	public void setContextState(ContextState contextState) {
		this.contextState = contextState;
	}

	/**
	 * @return the isVirtual
	 */
	public boolean isVirtual() {
		return isVirtual;
	}

	/**
	 * @param isVirtual the isVirtual to set
	 */
	public void setVirtual(boolean isVirtual) {
		this.isVirtual = isVirtual;
	}

	/**
	 * @return the child
	 */
	public NodeLevelDTO getChild() {
		return child;
	}

	/**
	 * @param child the child to set
	 */
	public void setChild(NodeLevelDTO child) {
		this.child = child;
	}

	/**
	 * 
	 * @return
	 */
	public String getLabelExpression() {
		return labelExpression;
	}

	/**
	 * 
	 * @param labelExpression
	 */
	public void setLabelExpression(String labelExpression) {
		this.labelExpression = labelExpression;
	}

	/**
	 * @return the valuePrecision
	 */
	public ValuePrecision getValuePrecision() {
		return valuePrecision;
	}

	/**
	 * @param valuePrecision the valuePrecision to set
	 */
	public void setValuePrecision(ValuePrecision valuePrecision) {
		this.valuePrecision = valuePrecision;
	}
}
