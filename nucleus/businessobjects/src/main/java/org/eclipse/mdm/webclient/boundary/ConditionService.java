/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.webclient.boundary;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.FilterItem;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.businessobjects.control.FilterParser;
import org.eclipse.mdm.connector.boundary.ConnectorService;

/**
 * 
 * @author jst
 *
 */
@Stateless
public class ConditionService {

	@Inject
	ConnectorService connectorService;

	public List<Condition> toCondition(String sourceName, String filterString) {
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		ModelManager modelManager = context.getModelManager()
				.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));
		Filter filter = FilterParser.parseFilterString(modelManager.listEntityTypes(), filterString);
		return toCondition(filter);
	}
	
	private List<Condition> toCondition(Filter filter) {
		return filter.stream().filter(FilterItem::isCondition).map(FilterItem::getCondition).collect(toList());
	}

}
