/*******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.businessobjects.utils;

import org.eclipse.mdm.api.base.model.ContextDescribable;
import org.eclipse.mdm.api.base.model.FilesAttachable;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;

public class EntityTypeUtil {
	
	private static final String ENTITYTYPE_TEST = "TEST";
	private static final String ENTITYTYPE_TESTSTEP = "TESTSTEP";
	private static final String ENTITYTYPE_MEASUREMENT = "MEASUREMENT";

	public static Class<? extends FilesAttachable> getFilesAttachableClassByEntityType(String entityType) {
		Class<? extends FilesAttachable> response;
		switch (entityType.toUpperCase()) {
			case ENTITYTYPE_TEST:
				response = Test.class;
				break;
			case ENTITYTYPE_TESTSTEP:
				response = TestStep.class;
				break;
			case ENTITYTYPE_MEASUREMENT:
				response = Measurement.class;
				break;
			default:
				throw new MDMEntityAccessException("Given entity type is unknown or not implementing FilesAttachable interface.");
		}
		return response;
	}
	
	public static Class<? extends ContextDescribable> getContextDescribableClassByEntityType(String entityType) {
		Class<? extends ContextDescribable> response;
		switch (entityType.toUpperCase()) {
			case ENTITYTYPE_TESTSTEP:
				response = TestStep.class;
				break;
			case ENTITYTYPE_MEASUREMENT:
				response = Measurement.class;
				break;
			default:
				throw new MDMEntityAccessException("Given entity type is unknown or not implementing ContextDescribable interface.");
		}
		return response;
	}
}
