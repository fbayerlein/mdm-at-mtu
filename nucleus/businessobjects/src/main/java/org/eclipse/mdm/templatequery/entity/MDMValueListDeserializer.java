/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.mdm.api.base.model.Value;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Implementation for converting JSON to structured DTOs.
 *
 * @since 5.2.0
 * @author JZ, Peak Solution GmbH
 */
public class MDMValueListDeserializer extends JsonDeserializer<List<Value>> {

	private MDMValueDeserializer valueDeserializer = new MDMValueDeserializer();

	@Override
	public List<Value> deserialize(JsonParser pars, DeserializationContext ctx)
			throws IOException, JsonProcessingException {
		if (!JsonToken.START_ARRAY.equals(pars.getCurrentToken())) {
			throw new JsonParseException(pars, "Expected array, but got " + pars.getCurrentToken());
		}

		List<Value> values = new ArrayList<>();
		while (pars.nextToken() != null && !JsonToken.END_ARRAY.equals(pars.getCurrentToken())) {
			values.add(valueDeserializer.deserialize(pars, ctx));
		}

		return values;
	}
}
