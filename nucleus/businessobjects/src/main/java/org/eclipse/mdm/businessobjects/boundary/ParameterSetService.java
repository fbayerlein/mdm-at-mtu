/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.businessobjects.boundary.ParameterSetResource.CreateParameterSet;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.control.NavigationActivity;
import org.eclipse.mdm.businessobjects.control.SearchActivity;
import org.eclipse.mdm.businessobjects.entity.SearchAttribute;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import com.google.common.base.Strings;

/**
 * ParameterSetService Bean implementation with available {@link ParameterSet}
 * operations
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Stateless
public class ParameterSetService {

	@Inject
	private ConnectorService connectorService;

	@EJB
	private SearchActivity searchActivity;

	@EJB
	private NavigationActivity navigationActivity;

	/**
	 * returns the matching {@link ParameterSet}s using the given filter or all
	 * {@link ParameterSet}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link ParameterSet} result
	 * @return the found {@link ParameterSet}s
	 */
	public List<ParameterSet> getParameterSets(String sourceName, String filter) {

		try {
			ApplicationContext context = this.connectorService.getContextByName(sourceName);
			EntityManager em = context.getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			if (filter == null || filter.trim().length() <= 0) {
				return em.loadAll(ParameterSet.class);
			}

			if (ServiceUtils.isParentFilter(context, filter, Measurement.class)) {
				String id = ServiceUtils.extactIdFromParentFilter(context, filter, Measurement.class);
				return this.navigationActivity.getParameterSetsByMeasurement(sourceName, id);
			}

			if (ServiceUtils.isParentFilter(context, filter, Channel.class)) {
				String id = ServiceUtils.extactIdFromParentFilter(context, filter, Channel.class);
				return this.navigationActivity.getParameterSetsByChannel(sourceName, id);
			}

			return this.searchActivity.search(context, ParameterSet.class, filter);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Returns the {@link SearchAttribute} for the entity type Test in the given
	 * data source.
	 * 
	 * @param sourceName The name of the data source.
	 * @return the found {@link SearchAttribute}s
	 */
	public List<SearchAttribute> getSearchAttributes(String sourceName) {
		return this.searchActivity.listAvailableAttributes(this.connectorService.getContextByName(sourceName),
				ParameterSet.class);
	}

	/**
	 * returns a {@link ParameterSet} identified by the given id.
	 * 
	 * @param parameterSetId id of the {@link ParameterSet}
	 * @param sourceName     name of the source (MDM {@link Environment} name)
	 * @param testStepId     id of the {@link ParameterSet}
	 * @return the matching {@link ParameterSet}
	 */
	public ParameterSet getParameterSet(String sourceName, String parameterSetId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(ParameterSet.class, parameterSetId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}
	
	/**
	 * Creates alle given parametersets
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param parameterSets
	 * @return
	 */
	public List<ParameterSet> createParameterSets(String sourceName, List<CreateParameterSet> parameterSets) {
		ApplicationContext context = connectorService.getContextByName(sourceName);
		EntityManager em = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		EntityFactory factory = context.getEntityFactory()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityFactory.class));


		List<String> channelIds = parameterSets.stream().filter(p -> !Strings.isNullOrEmpty(p.channel)).map(p -> p.channel).distinct()
				.collect(Collectors.toList());
		Map<String, List<Channel>> channels = em.load(Channel.class, channelIds).stream()
				.collect(Collectors.groupingBy(c -> c.getID()));

		List<String> measurementIds = parameterSets.stream().filter(p -> !Strings.isNullOrEmpty(p.measurement)).map(p -> p.measurement).filter(u -> !Strings.isNullOrEmpty(u)).distinct()
				.collect(Collectors.toList());
		Map<String, List<Measurement>> measurements = em.load(Measurement.class, measurementIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<ParameterSet> createdParameterSets = new ArrayList<ParameterSet>();

		for (CreateParameterSet parameterSet : parameterSets) {
			if (Strings.isNullOrEmpty(parameterSet.channel) && Strings.isNullOrEmpty(parameterSet.measurement)) {
				throw new DataAccessException("Measurement or Channel have to be set!");
			}
			
			if (!Strings.isNullOrEmpty(parameterSet.channel) && !Strings.isNullOrEmpty(parameterSet.measurement)) {
				throw new DataAccessException("Only on of following Objects have to be set: Measurement, Channel!");
			}
			
			
			Channel channel = null;
			Measurement measurement = null;
			
			if (!Strings.isNullOrEmpty(parameterSet.channel)) {
				List<Channel> channelList = channels.get(parameterSet.channel);
				if (channelList == null || channelList.isEmpty()) {
					throw new DataAccessException(
							"No Channel with ID " + parameterSet.channel + " found in source " + sourceName);
				} else {
					channel = channelList.get(0);
				}
				
			}
			
			if (!Strings.isNullOrEmpty(parameterSet.measurement)) {
				List<Measurement> measurementList = measurements.get(parameterSet.measurement);
				if (measurementList == null || measurementList.isEmpty()) {
					throw new DataAccessException(
							"No Measurement with ID " + parameterSet.channel + " found in source " + sourceName);
				} else {
					measurement = measurementList.get(0);
				}
				
			}
			
		
			if (channel == null) {
				createdParameterSets.add(factory.createParameterSet(parameterSet.name, measurement));
			} else {
				createdParameterSets.add(factory.createParameterSet(parameterSet.name, channel));
			}

		}

		try (Transaction t = em.startTransaction()) {
			t.create(createdParameterSets);
			t.commit();
		}

		return createdParameterSets;
	}

}
