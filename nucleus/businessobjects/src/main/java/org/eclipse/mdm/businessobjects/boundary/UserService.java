/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Role;
import org.eclipse.mdm.businessobjects.entity.Profile;
import org.eclipse.mdm.businessobjects.entity.UserDetails;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.connector.entity.MDMPrincipal;

@Stateless
public class UserService {

	@Context
	public SecurityContext securityContext;

	@Inject
	private ConnectorService connectorService;

	/**
	 * Reads all user details for current user.
	 * 
	 * 1) Loads user and role information from ODS for all connected sources. 2)
	 * Reads user and role information from servers security context.
	 * 
	 * @param queriedServerRoles the roles to check in servers security context
	 * @return The merged user/role details.
	 */
	public Profile readCurrentProfile(List<String> queriedServerRoles) {
		Map<String, UserDetails> dataSourceUserDetails = connectorService.getContexts().stream()
				.collect(Collectors.toMap(ApplicationContext::getSourceName, this::readDataSourceUser));
		UserDetails applicationUser = getApplicationUserDetails(queriedServerRoles);
		return new Profile(securityContext.getUserPrincipal().getName(), applicationUser, dataSourceUserDetails);
	}

	/**
	 * Reads user and role information from servers security context.
	 *
	 * @param queriedServerRoles the roles to check in servers security context
	 * @return The merged user/role details.
	 */
	private UserDetails getApplicationUserDetails(List<String> queriedServerRoles) {

		UserDetails user = new UserDetails();
		user.setUsername(securityContext.getUserPrincipal().getName());
		
		if (queriedServerRoles.contains("SpecialistAdmin")) {
			connectorService.getContexts().forEach(c -> queriedServerRoles.add("SpecialistAdmin_"+ c.getSourceName()));			
		}
		
		user.setRoles(filterApplicationRoles(queriedServerRoles));

		if (securityContext.getUserPrincipal() instanceof MDMPrincipal) {
			MDMPrincipal userPrincipal = (MDMPrincipal) securityContext.getUserPrincipal();
			user.setMail(userPrincipal.getEmail());
			user.setSurname(userPrincipal.getSurname());
			user.setGivenName(userPrincipal.getGivenName());
			user.setDepartment(userPrincipal.getDepartment());
			user.setTelephone(userPrincipal.getTelephone());
		} else {
			// do nothing! In case of basic authentication no MdMPrincipal is mappable.
			// user.setMail("no.mail@peak.de");
		}
		return user;
	}

	/**
	 * Reads current users roles from servers security context.
	 *
	 * Note: There is no reliable way of getting the (possibly mapped) roles of a
	 * user. Thus it is only checked if the user has the queried roles.
	 * 
	 * @param queriedServerRoles
	 * @return
	 */
	private List<String> filterApplicationRoles(List<String> queriedServerRoles) {
		return queriedServerRoles.stream().filter(securityContext::isUserInRole).collect(Collectors.toList());
	}

	/**
	 * Reads user and roles informations from ODS for the current user in the given
	 * {@link ApplicationContext}.
	 *
	 * @param context
	 * @return
	 */
	private UserDetails readDataSourceUser(ApplicationContext context) {
		EntityManager em = context.getEntityManager().get();
		User userEntity = getUserByNameIgnoreCase(securityContext.getUserPrincipal().getName(), em);
		List<String> roles = em.loadRelatedEntities(userEntity, User.REL_USERS2GROUPS, Role.class).stream()
				.map(Role::getName).collect(Collectors.toList());		
		return toUserDetails(userEntity, roles);
	}

	/**
	 * @TODO jst 22.01.2024: remove when not needed anymore.
	 *
	 * @param userName
	 * @param em
	 * @return
	 */
	private User getUserByNameIgnoreCase(String userName, EntityManager em) {
		User returnVal = null;
		for (User user : em.loadAll(User.class)) {
			if (user.getName().equalsIgnoreCase(userName)) {
				returnVal = user;
				break;
			}
		}
		return returnVal;
	}

	/**
	 * Maps ODS-{@link org.eclipse.mdm.api.base.model.User}-Entity to an
	 * {@link org.eclipse.mdm.businessobjects.entity.UserDetails} DTO. The DTO
	 * additionally holds the roles information.
	 *
	 * @param userEntity
	 * @param roles
	 * @return
	 */
	private UserDetails toUserDetails(User userEntity, List<String> roles) {
		UserDetails dto = new UserDetails();
		dto.setGivenName(userEntity.getGivenName());
		dto.setSurname(userEntity.getSurname());
		dto.setMail(userEntity.getMail());
		dto.setTelephone(userEntity.getPhone());
		dto.setDepartment(userEntity.getDepartment());
		dto.setRoles(roles);
		dto.setUsername(userEntity.getName());
		return dto;
	}
}
