/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.businessobjects.utils.Serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;

/**
 * Implementation for converting JSON to structured DTOs.
 *
 * @since 5.2.0
 */
public class MDMValueDeserializer extends JsonDeserializer<Value> {

	private static final String NAME = "name";
	private static final String DATA_TYPE = "dataType";
	private static final String ENUMERATION_NAME = "enumerationName";
	private static final String UNIT = "unit";
	private static final String VALUE = "value";

	@Override
	public Value deserialize(JsonParser pars, DeserializationContext ctx) throws IOException, JsonProcessingException {
		JsonNode node = pars.getCodec().readTree(pars);

		String name = node.get(NAME).asText();
		ValueType<?> valueType = ValueType.valueTypeByName(node.get(DATA_TYPE).asText());
		String unit = node.get(UNIT).asText();
		JsonNode v = node.get(VALUE);

		if (valueType.isEnumerationType()) {

			String sourceName = (String) ctx.getAttribute("sourceName");
			if (sourceName == null) {
				throw new MDMDeserializerException("Expecting attribute 'sourceName' in DeserializationContext.");
			}
			Enumeration<?> enumeration = getEnumeration(node, sourceName);

			return valueType.create(name, unit, true, getEnumerationValue(valueType, enumeration, v),
					enumeration.getName());
		} else {
			return valueType.create(name, unit, true, deserializeValue(valueType, v));
		}
	}

	private Object deserializeValue(ValueType<?> valueType, JsonNode nv) {
		switch (valueType.name()) {
		case "STRING":
			return nv.asText();
		case "STRING_SEQUENCE":
			List<String> strings = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				strings.add((String) deserializeValue(valueType.toSingleType(), n));
			}
			return strings.toArray(new String[0]);
		case "DATE":
			return Serializer.parseDate(nv.asText());
		case "DATE_SEQUENCE":
			List<Instant> dates = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				dates.add((Instant) deserializeValue(valueType.toSingleType(), n));
			}
			return dates.toArray(new Instant[0]);
		case "BOOLEAN":
			return Serializer.deserializeValue(ValueType.BOOLEAN, nv.asText());
		case "BOOLEAN_SEQUENCE":
			List<Boolean> booleans = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				booleans.add((boolean) deserializeValue(valueType.toSingleType(), n));
			}
			return Booleans.toArray(booleans);
		case "BYTE":
			return Serializer.deserializeValue(ValueType.BYTE, nv.asText());
		case "BYTE_SEQUENCE":
			List<Byte> bytes = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				bytes.add((byte) deserializeValue(valueType.toSingleType(), n));
			}
			return Bytes.toArray(bytes);
		case "SHORT":
			return Serializer.deserializeValue(ValueType.SHORT, nv.asText());
		case "SHORT_SEQUENCE":
			List<Short> shorts = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				shorts.add((short) deserializeValue(valueType.toSingleType(), n));
			}
			return Shorts.toArray(shorts);
		case "INTEGER":
			return Serializer.deserializeValue(ValueType.INTEGER, nv.asText());
		case "INTEGER_SEQUENCE":
			List<Integer> ints = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				ints.add((int) deserializeValue(valueType.toSingleType(), n));
			}
			return Ints.toArray(ints);
		case "LONG":
			return Serializer.deserializeValue(ValueType.LONG, nv.asText());
		case "LONG_SEQUENCE":
			List<Long> longs = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				longs.add((long) deserializeValue(valueType.toSingleType(), n));
			}
			return Longs.toArray(longs);
		case "FLOAT":
			return Serializer.deserializeValue(ValueType.FLOAT, nv.asText());
		case "FLOAT_SEQUENCE":
			List<Float> floats = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				floats.add((float) deserializeValue(valueType.toSingleType(), n));
			}
			return Floats.toArray(floats);
		case "DOUBLE":
			return Serializer.deserializeValue(ValueType.DOUBLE, nv.asText());
		case "DOUBLE_SEQUENCE":
			List<Double> doubles = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				doubles.add((double) deserializeValue(valueType.toSingleType(), n));
			}
			return Doubles.toArray(doubles);
//		case "ENUMERATION":
//			return Serializer.deserializeEnumerationValue(nv.asText());
//		case "ENUMERATION_SEQUENCE":
//			List<EnumerationValue> enums = new ArrayList<>();
//			for (JsonNode n : ((ArrayNode) nv)) {
//				enums.add((EnumerationValue) deserializeValue(ValueType.ENUMERATION, n));
//			}
//			return enums.toArray(new EnumerationValue[0]);
		case "FILE_LINK":
			return Serializer.deserializeFileLink(nv);
		case "FILE_LINK_SEQUENCE":
			List<FileLink> fileLinks = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) nv)) {
				fileLinks.add((FileLink) deserializeValue(valueType.toSingleType(), n));
			}
			return fileLinks.toArray(new FileLink[0]);
		case "FILE_RELATION":
			return new FileLink[0];
		default:
			throw new RuntimeException("ValueType " + valueType.name() + " not implemented!");
		}
	}

	private Object getEnumerationValue(ValueType<?> valueType, Enumeration<?> enumeration, JsonNode v) {
		if (valueType.isEnumeration()) {
			return enumeration.valueOf(v.asText());
		} else {
			List<EnumerationValue> enumerationValues = new ArrayList<>();
			for (JsonNode n : ((ArrayNode) v)) {
				enumerationValues.add(enumeration.valueOf(n.asText()));
			}
			return enumerationValues.toArray(new EnumerationValue[0]);
		}
	}

	private Enumeration<?> getEnumeration(JsonNode node, String sourceName) {
		if (node.has(ENUMERATION_NAME)) {
			return EnumRegistry.getInstance().get(sourceName, node.get(ENUMERATION_NAME).asText());
		} else {
			// if no enumerationName is set, fallback to ValueType
			return EnumRegistry.getInstance().get(sourceName, "ValueType");
		}
	}
}
