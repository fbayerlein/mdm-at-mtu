/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * All functions in the {@link ExpressionLanguageMethodProvider} are reflected
 * in the 'mdm' namespace in the expression language context. Therefore, they
 * have to be static.
 *
 */
public class ExpressionLanguageMethodProvider {

	/**
	 * Formats a date according to the fiven format string or returns a default
	 * value if the given date was null.
	 * 
	 * @param input              date to format
	 * @param format             desired format pattern
	 * @param nullOrEmptyDefault default value to return, if input == null
	 * @return String with the formatted date or default value, if input == null
	 */
	public static String formatDate(Instant input, String format, String nullOrEmptyDefault) {
		if (input == null) {
			return nullOrEmptyDefault;
		} else {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of("UTC"));
			return formatter.format(input);
		}
	}

	/**
	 * Splits a string by the given delimiter and returns the part with the
	 * specified index.
	 * 
	 * @param label              string to split
	 * @param regex              the delimiting regular expression
	 * @param index              the index of the part of the splitted string to
	 *                           return
	 * @param nullOrEmptyDefault default value to return, if the label is null or
	 *                           the index does not exist in the splitted string
	 * @return the part of the splitted string specified by inde or the default
	 *         value.
	 */
	public static String substring(String label, String regex, int index, String nullOrEmptyDefault) {
		if (label == null) {
			return nullOrEmptyDefault;
		} else {
			String[] splitted = label.split(regex);
			if (index < splitted.length) {
				return splitted[index];
			} else {
				return nullOrEmptyDefault;
			}
		}
	}

}
