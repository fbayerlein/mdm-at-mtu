/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.nodeprovider.control.GenericNodeProvider;

import com.google.common.base.MoreObjects;

/**
 * A {@link NodeLevel} describes one level in the hierarchy of the navigator
 * tree. A {@link NodeLevel} references another {@link NodeLevel} defining the
 * level below or in other words its children. Together with
 * {@link NodeProviderRoot} it describes the hierarchy of provided by
 * {@link GenericNodeProvider}.
 *
 */
public class NodeLevel {

	private int level = 1;

	private EntityType type;

	private List<FilterAttribute> filterAttributes;
	private List<Attribute> labelAttributes;
	private String labelExpression;
	private ValuePrecision valuePrecision;
	private List<SortAttribute> orderAttributes;

	private ContextState contextState = null;

	private boolean isVirtual;

	private NodeLevel child;

	/**
	 * Constructs NodeLevel for the given {@link EntityType}. Using default ID and
	 * Name attributes.
	 * 
	 * @param type
	 */
	public NodeLevel(EntityType type) {
		this(type, Arrays.asList(new FilterAttribute(type.getIDAttribute())), Arrays.asList(type.getNameAttribute()),
				Collections.emptyList());
	}

	public NodeLevel(EntityType type, Attribute attribute) {
		this(type, Arrays.asList(new FilterAttribute(attribute)), Arrays.asList(attribute),
				Arrays.asList(new SortAttribute(attribute)));
	}

	/**
	 * Constructs NodeLevel for the given {@link EntityType} using the given ID and
	 * label attributes with the given list of order attributes.
	 *
	 * @param type
	 * @param filterAttributes
	 * @param labelAttribute
	 */
	public NodeLevel(EntityType type, FilterAttribute filterAttribute, Attribute labelAttribute) {
		this(type, Arrays.asList(filterAttribute), Arrays.asList(labelAttribute));
	}

	public NodeLevel(EntityType type, List<FilterAttribute> filterAttributes, List<Attribute> labelAttributes) {
		this(type, filterAttributes, labelAttributes, labelAttributes == null ? Collections.emptyList()
				: labelAttributes.stream().map(SortAttribute::new).collect(toList()));
	}

	/**
	 * Constructs NodeLevel for the given {@link EntityType} using the given ID and
	 * label attributes with the given list of order attributes.
	 * 
	 * @param type
	 * @param filterAttribute
	 * @param labelAttribute
	 * @param orderAttributes
	 */
	public NodeLevel(EntityType type, List<FilterAttribute> filterAttributes, List<Attribute> labelAttributes,
			List<SortAttribute> orderAttributes) {
		this.type = type;

		if (filterAttributes == null || filterAttributes.isEmpty()) {
			this.filterAttributes = Arrays.asList(new FilterAttribute(type.getIDAttribute()));
		} else {
			this.filterAttributes = filterAttributes;
		}

		if (labelAttributes == null || labelAttributes.isEmpty()) {
			this.labelAttributes = Arrays.asList(type.getNameAttribute());
		} else {
			this.labelAttributes = labelAttributes;
		}

		if (orderAttributes == null || orderAttributes.isEmpty()) {
			this.orderAttributes = this.labelAttributes.stream().map(SortAttribute::new).collect(Collectors.toList());
		} else {
			this.orderAttributes = orderAttributes;
		}

		this.valuePrecision = ValuePrecision.EXACT;
	}

	/**
	 * @param child to set
	 */
	public void setChild(NodeLevel child) {
		child.setLevel(this.level + 1);
		this.child = child;
	}

	/**
	 * @param level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param contextState to set
	 */
	public void setContextState(ContextState contextState) {
		this.contextState = contextState;
	}

	/**
	 * @param isVirtual
	 */
	public void setVirtual(boolean isVirtual) {
		this.isVirtual = isVirtual;
	}

	/**
	 * @return the entity type
	 */
	public EntityType getEntityType() {
		return type;
	}

	/**
	 * @return the entity type's name
	 */
	public String getType() {
		return type.getName();
	}

	/**
	 * @return isVirtual
	 */
	public boolean isVirtual() {
		return isVirtual;
	}

	/**
	 * @return the ID attribute
	 */
	public List<FilterAttribute> getFilterAttributes() {
		return filterAttributes;
	}

	/**
	 * @return the label attribute
	 */
	public List<Attribute> getLabelAttributes() {
		return labelAttributes;
	}

	/**
	 * @return the context state
	 */
	public ContextState getContextState() {
		return contextState;
	}

	/**
	 * @return the child node level
	 */
	public NodeLevel getChild() {
		return child;
	}

	/**
	 * @return the list with order attributes
	 */
	public List<SortAttribute> getOrderAttributes() {
		return orderAttributes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(NodeLevel.class).add("type", type).add("level", level)
				.add("filterAttributes", filterAttributes).add("labelAttributes", labelAttributes)
				.add("labelExpression", labelExpression).add("orderAttribute", orderAttributes)
				.add("contextState", contextState).add("isVirtual", isVirtual).add("valuePrecision", valuePrecision)
				.add("child", child).toString();
	}

	/**
	 * @return the labelExpression
	 */
	public String getLabelExpression() {
		if (labelExpression == null) {
			return labelAttributes.stream().map(a -> "${" + a.getName() + "}").collect(Collectors.joining(" "));
		} else {
			return labelExpression;
		}
	}

	/**
	 * @param labelExpression the labelExpression to set
	 */
	public void setLabelExpression(String labelExpression) {
		this.labelExpression = labelExpression;
	}

	/**
	 * @return the valuePrecision
	 */
	public ValuePrecision getValuePrecision() {
		return valuePrecision;
	}

	/**
	 * @param valuePrecision the valuePrecision to set
	 */
	public void setValuePrecision(ValuePrecision valuePrecision) {
		this.valuePrecision = valuePrecision != null ? valuePrecision : ValuePrecision.EXACT;
	}
}
