/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.webclient.entity;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

public class ConditionDTO {

	private String type;
	private String attribute;
	private ComparisonOperator operator;
	private List<String> value;
	private String valueType;

	public ConditionDTO(final Condition condition) {
		this.attribute = condition.getAttribute().getName();
		this.valueType = condition.getAttribute().getValueType().toString();
		this.type = ServiceUtils.workaroundForTypeMapping(condition.getAttribute().getEntityType());
		if (condition.getValue().isValid()) {
			if (condition.getValue().getValueType().isSequence()) {
				this.value = Stream.of(condition.getValue().extract()).map(Object::toString).collect(toList());
			} else {
				this.value = new ArrayList<>();
				this.value.add(condition.getValue().extract().toString());
			}
		} else {
			this.value = new ArrayList<>();
		}
		this.operator = condition.getComparisonOperator();
	}

	/**
	 * @return the type
	 */
	public final String getType() {
		return type;
	}

	/**
	 * @return the attribute
	 */
	public final String getAttribute() {
		return attribute;
	}

	/**
	 * @return the operator
	 */
	public final ComparisonOperator getOperator() {
		return operator;
	}

	/**
	 * @return the value
	 */
	public final Object getValue() {
		return value;
	}

	/**
	 * @return the valueType
	 */
	public final String getValueType() {
		return valueType;
	}

	/**
	 * Set for type
	 * @param type the type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Set for attribute
	 * @param attribute the attribute
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	/**
	 * Set for operator
	 * @param operator the comparison operator
	 */
	public void setOperator(ComparisonOperator operator) {
		this.operator = operator;
	}

	/**
	 * Set for value
	 * @param value the value
	 */
	public void setValue(List<String> value) {
		this.value = value;
	}

	/**
	 * Set for value type
	 * @param valueType the value type
	 */
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
}
