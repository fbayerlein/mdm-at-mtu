/*******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ATTRIBUTENAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTCOMPONENTNAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTGROUP;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTTYPE;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_FILELINKIDENTIFIER;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.io.InputStream;
import java.time.Instant;
import java.util.Collections;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextDescribable;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.businessobjects.control.FileLinkActivity;
import org.eclipse.mdm.businessobjects.control.MDMFileAccessException;
import org.eclipse.mdm.businessobjects.entity.FileSize;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.MDMFileLink;
import org.eclipse.mdm.businessobjects.entity.MDMFileLinkExt;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.Serializer;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.common.base.Strings;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.vavr.Tuple;

/**
 * context files subresource
 * 
 * @author Johannes Stamm, peak-Solution GmbH
 *
 */
public class ContextFilesSubresource<T extends MDMFile> {

	@EJB
	private FileLinkActivity<T> fileLinkActivity;

	@EJB
	private EntityService entityService;

	private Class<? extends ContextDescribable> contextDescribableClass;

	private Class<? extends Entity> referringEntityClass = null;

	/**
	 * Set the Entity class of the parent resource. This method has to be called
	 * right after the resource was initialized by the container. The entity class
	 * is used to distinguish the type of the parent resource.
	 * 
	 * This method is package-private to workaround an issue in WELD complaining
	 * about: Parameter 1 of type ... is not resolvable to a concrete type.
	 * 
	 * @param contextDescribableClass the Entity class of the parent resource
	 */
	void setEntityClass(Class<? extends ContextDescribable> contextDescribableClass) {
		this.contextDescribableClass = contextDescribableClass;
	}

	/**
	 * Set the referring Entity class of the parent resource.
	 * 
	 * This method is package-private to workaround an issue in WELD complaining
	 * about: Parameter 1 of type ... is not resolvable to a concrete type.
	 * 
	 * @param contextDescribableClass the Entity class of the parent resource
	 */
	void setReferringEntityClass(Class<? extends Entity> referringEntityClass) {
		this.referringEntityClass = referringEntityClass;
	}

	/**
	 * Gets all MDMFiles attaches to a ContextComponent.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return A list of all MDMFiles attached to the {@link ContextComponent}.
	 */
	@GET
	@Operation(summary = "Get MDMFiles", description = "Get a list of all MDMFiles attached to a ContextComponent", responses = {
			@ApiResponse(description = "The MDMFiles", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMDMFileLinksExt(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the parent ContextDescribable", required = true) @PathParam(REQUESTPARAM_ID) String contextDescribableId,
			@Parameter(description = "ContextType of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTTYPE) ContextType contextType,
			@Parameter(description = "ContextGroup of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTGROUP) String contextGroup,
			@Parameter(description = "Name of the ContextComponent holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTCOMPONENTNAME) String contextComponentName,
			@Parameter(description = "Name of the attribute or relation holding the FileLink", required = true) @PathParam(REQUESTPARAM_ATTRIBUTENAME) String attributeName) {
		String id = resolveReferringInstanceId(sourceName, contextDescribableId);
		if (Strings.isNullOrEmpty(id)) {
			return ServiceUtils.toResponse(Collections.emptyList(), Status.OK);
		} else {
			return entityService.find(V(sourceName), contextDescribableClass, V(id))
					.map(contextDescribable -> fileLinkActivity
							.findAllFileLinksInContext(sourceName, contextDescribable, contextType, contextGroup,
									contextComponentName, attributeName)
							.stream().map(fl -> Serializer.serializeFileLink(fl)).collect(Collectors.toList()))
					.map(list -> ServiceUtils.toResponse(list, Status.OK)).get();
		}
	}

	/**
	 * Creates new {@link FileLink} for {@link TestStep}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link FileLink} to create.
	 * @return
	 */
	@POST
	@Operation(summary = "Creates a new file on a context attribute.", responses = {
			@ApiResponse(description = "The stored file link.", content = @Content(schema = @Schema(implementation = MDMFileLink.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response createFile(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the parent ContextDescribable", required = true) @PathParam(REQUESTPARAM_ID) String contextDescribableId,
			@Parameter(description = "ContextType of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTTYPE) ContextType contextType,
			@Parameter(description = "ContextGroup of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTGROUP) String contextGroup,
			@Parameter(description = "Name of the ContextComponent holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTCOMPONENTNAME) String contextComponentName,
			@Parameter(description = "Name of the attribute or relation holding the FileLink", required = true) @PathParam(REQUESTPARAM_ATTRIBUTENAME) String attributeName,
			@Parameter(description = "InputStream containing file data", required = true) @FormDataParam("file") InputStream fileInputStream,
			@Parameter(description = "Meta data describing file", required = true) @FormDataParam("file") FormDataContentDisposition cdh,
			@Parameter(description = "File modification date", required = true) @FormDataParam("lastModified") String lastModified,
			@Parameter(description = "File description", required = true) @FormDataParam("description") String description,
			@Parameter(description = "Mimetype of the file", required = true) @FormDataParam("mimeType") MimeType mimeType) {

		return entityService.find(V(sourceName), contextDescribableClass, V(contextDescribableId), false)
				.map(contextDescribable -> fileLinkActivity.createFile(sourceName, contextDescribable,
						ServiceUtils.convertIso8859toUtf8(cdh.getFileName()),
						Instant.ofEpochMilli(Long.valueOf(lastModified)), fileInputStream, description, mimeType,
						contextType, contextGroup, contextComponentName, attributeName))
				.map(fileLink -> ServiceUtils.toResponse(Serializer.serializeFileLink(fileLink), Status.OK)).get();
	}

	@GET
	@Operation(summary = "Stream the contents of a file link", responses = {
			@ApiResponse(description = "The content of the file link as a stream. MimeType is set to that of the file link.", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID or remote path supplied") })
	@Path("/{" + REQUESTPARAM_FILELINKIDENTIFIER + "}")
	public Response streamFileLink(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the parent ContextDescribable", required = true) @PathParam(REQUESTPARAM_ID) String contextDescribableId,
			@Parameter(description = "ContextType of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTTYPE) ContextType contextType,
			@Parameter(description = "ContextGroup of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTGROUP) String contextGroup,
			@Parameter(description = "Name of the ContextComponent holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTCOMPONENTNAME) String contextComponentName,
			@Parameter(description = "Name of the Attribute holding the FileLink", required = true) @PathParam(REQUESTPARAM_ATTRIBUTENAME) String attributeName,
			@Parameter(description = "The remote path of the file link whose content is to be retrieved", required = true) @PathParam(REQUESTPARAM_FILELINKIDENTIFIER) String fileLinkIdentifier) {
		String id = resolveReferringInstanceId(sourceName, contextDescribableId);
		if (Strings.isNullOrEmpty(id)) {
			throw new MDMFileAccessException(String.format("No related %s instance found for %s instance ID %s.",
					contextDescribableClass.getName(), referringEntityClass.getName(), contextDescribableId));
		} else {
			return entityService.find(V(sourceName), contextDescribableClass, V(id))
					.map(contextDescribable -> Tuple.of(contextDescribable,
							fileLinkActivity.findFileLinkInContext(sourceName, fileLinkIdentifier, contextDescribable,
									contextType, contextGroup, contextComponentName, attributeName)))
					.map(tuple -> Tuple.of(fileLinkActivity.toStreamingOutput(sourceName, tuple._1, tuple._2),
							fileLinkActivity.toMediaType(tuple._2)))
					.map(tuple -> Response.ok(tuple._1, tuple._2).build()).get();
		}
	}

	@GET
	@Operation(summary = "Returns the size of a file in bytes.", responses = {
			@ApiResponse(description = "The file size of the requested remote path.", content = @Content(schema = @Schema(implementation = FileSize.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/size/{" + REQUESTPARAM_FILELINKIDENTIFIER + "}")
	public Response loadFileSize(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the parent ContextDescribable", required = true) @PathParam(REQUESTPARAM_ID) String contextDescribableId,
			@Parameter(description = "ContextType of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTTYPE) ContextType contextType,
			@Parameter(description = "ContextGroup of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTGROUP) String contextGroup,
			@Parameter(description = "Name of the ContextComponent holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTCOMPONENTNAME) String contextComponentName,
			@Parameter(description = "Name of the Attribute holding the FileLink", required = true) @PathParam(REQUESTPARAM_ATTRIBUTENAME) String attributeName,
			@Parameter(description = "The remote path of the file link whose content is to be retrieved", required = true) @PathParam(REQUESTPARAM_FILELINKIDENTIFIER) String fileLinkIdentifier) {
		return entityService.find(V(sourceName), contextDescribableClass, V(contextDescribableId))
				.map(contextDescribable -> Tuple.of(contextDescribable,
						fileLinkActivity.findFileLinkInContext(sourceName, fileLinkIdentifier, contextDescribable,
								contextType, contextGroup, contextComponentName, attributeName)))
				.map(tuple -> fileLinkActivity.loadFileSize(sourceName, tuple._1, tuple._2))
				.map(fileSize -> Response.ok(fileSize).build()).get();
	}

	@GET
	@Operation(summary = "Returns the size of all files attached to a context component attribute or relation in bytes.", responses = {
			@ApiResponse(description = "The file sizes of the requested context component attribute or relation.", content = @Content(schema = @Schema(implementation = FileSize.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sizes")
	public Response loadFileSizes(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the parent ContextDescribable", required = true) @PathParam(REQUESTPARAM_ID) String contextDescribableId,
			@Parameter(description = "ContextType of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTTYPE) ContextType contextType,
			@Parameter(description = "ContextGroup of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTGROUP) String contextGroup,
			@Parameter(description = "Name of the ContextComponent holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTCOMPONENTNAME) String contextComponentName,
			@Parameter(description = "Name of the Attribute holding the FileLink", required = true) @PathParam(REQUESTPARAM_ATTRIBUTENAME) String attributeName) {
		return entityService.find(V(sourceName), contextDescribableClass, V(contextDescribableId))
				.map(contextDescribable -> Tuple.of(contextDescribable,
						fileLinkActivity.findAllFileLinksInContext(sourceName, contextDescribable, contextType,
								contextGroup, contextComponentName, attributeName)))
				.map(tuple -> tuple._2.stream()
						.map(fileLink -> fileLinkActivity.loadFileSize(sourceName, tuple._1, fileLink))
						.collect(Collectors.toList()))
				.map(fileSizes -> Response.ok(fileSizes).build()).get();
	}

	/**
	 * Deletes attached file from {@link ContextDescirbable}.
	 * 
	 * @param sourceName         name of the source (MDM {@link Environment} name)
	 * @param id                 The identifier of the {@link ContextDescirbable}
	 *                           which contains the {@link FileLink}
	 * @param fileLinkIdentifier The remote path of the {@link FileLink} to delete.
	 * @return
	 */
	@DELETE
	@Operation(summary = "Deletes an attached file from a context attribute.", responses = {
			@ApiResponse(description = "The deleted file link", content = @Content(schema = @Schema(implementation = MDMFileLink.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_FILELINKIDENTIFIER + "}")
	public Response deleteFile(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the parent ContextDescribable", required = true) @PathParam(REQUESTPARAM_ID) String contextDescribableId,
			@Parameter(description = "ContextType of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTTYPE) ContextType contextType,
			@Parameter(description = "ContextGroup of the Component holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTGROUP) String contextGroup,
			@Parameter(description = "Name of the ContextComponent holding the attribute with the FileLink", required = true) @PathParam(REQUESTPARAM_CONTEXTCOMPONENTNAME) String contextComponentName,
			@Parameter(description = "Name of the Attribute holding the FileLink", required = true) @PathParam(REQUESTPARAM_ATTRIBUTENAME) String attributeName,
			@Parameter(description = "The file link identifier of the file link whose content is to be retrieved", required = true) @PathParam(REQUESTPARAM_FILELINKIDENTIFIER) String fileLinkIdentifier) {

		return entityService.find(V(sourceName), contextDescribableClass, V(contextDescribableId))
				.map(contextDescribable -> fileLinkActivity.deleteFileLink(sourceName, contextDescribable, contextType,
						contextGroup, contextComponentName, attributeName, fileLinkIdentifier))
				// Use fileLinkIdentifier as passed to method since fileLink's remoteObject is
				// no longer valid
				// and thus cannot be used to generate identifier for AoFiles:
				.map(fileLink -> ServiceUtils.toResponse(new MDMFileLinkExt(fileLinkIdentifier,
						fileLink.getMimeType().toString(), fileLink.getDescription(), fileLink.getFileName()),
						Status.OK))
				.get();
	}

	private String resolveReferringInstanceId(String sourceName, String id) {
		if (referringEntityClass != null) {
			EntityManager em = fileLinkActivity.getEntityManager(sourceName);
			for (ContextDescribable cd : em.loadChildren(
					entityService.find(V(sourceName), referringEntityClass, V(id)).get(), contextDescribableClass)) {
				return cd.getID();
			}
		} else {
			return id;
		}

		return "";
	}
}
