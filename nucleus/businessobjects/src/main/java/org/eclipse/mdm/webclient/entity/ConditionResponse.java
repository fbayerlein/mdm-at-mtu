/*******************************************************************************
 *  Copyright (c) 2021 Contributors to the Eclipse Foundation
 *  
 *  See the NOTICE file(s) distributed with this work for additional
 *  information regarding copyright ownership.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v. 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 *  SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.webclient.entity;

import org.eclipse.mdm.api.base.query.Condition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ConditionResponse {

	/** transferable data content */
	private List<ConditionDTO> data;

	/**
	 * Constructor
	 * 
	 * @param conditions list of {@link Condition}s to transfer
	 */
	public ConditionResponse(final List<Condition> conditions) {
		this.data = conditions.stream().map(ConditionDTO::new).collect(Collectors.toList());
	}

	public ConditionResponse() {
		this.data = new ArrayList<>();
	}

	public List<ConditionDTO> getData() {
		return this.data;
	}

	public void setData(List<ConditionDTO> data) {
		this.data = data;
	}
}
