/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Parameter;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.collection.List;
import io.vavr.control.Try;

/**
 * {@link Parameter} resource
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Tag(name = "Parameter")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/parameters")
public class ParameterResource {

	@EJB
	private ParameterService parameterService;

	@EJB
	private EntityService entityService;

	@Context
	javax.ws.rs.ext.Providers providers;

	/**
	 * delegates the request to the {@link ParameterSetService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link ParameterSet} result
	 * @return the result of the delegated request as {@link Response}
	 */

	@GET
	@Operation(summary = "Find Par by filter", description = "Get list of parameters", responses = {
			@ApiResponse(description = "The parameters", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getParameters(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.Parameter(description = "Filter expression", required = false) @QueryParam("filter") String filter) {

		return Try.of(() -> parameterService.getParameters(sourceName, filter)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, Parameter.class)).get();
	}

	/**
	 * delegates the request to the {@link ParameterSetService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link Pool}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a Parameter by ID", description = "Returns Parameter based on ID", responses = {
			@ApiResponse(description = "The Parameter", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response findParameterSet(
			@io.swagger.v3.oas.annotations.Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.Parameter(description = "ID of the Parameter", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), Parameter.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created {@link Parameter}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link Parameter} to create.
	 * @return the created {@link Parameter} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new Parameter / multiple Parameters", responses = {
			@ApiResponse(description = "The created Parameter(s)", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(
			@io.swagger.v3.oas.annotations.Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
					@ExampleObject(name = "Create a single Parameter", value = "{ \"Name\": \"MyParameter\", \"stringValue\": \"abc\", \"ParameterSet\": \"123\" }"),
					@ExampleObject(name = "Create multiple Parameters", value = "[\n{ \"Name\": \"MyParameter1\", \"stringValue\": \"abc\", \"ParameterSet\": \"123\" }, "
							+ "{ \"Name\": \"MyParameter2\", \"integerValue\": 4711, \"ParameterSet\": \"123\" }\n]") })) String body) {

		if (body != null && body.trim().startsWith("[")) {
			try {
				ContextResolver<ObjectMapper> resolver = providers.getContextResolver(ObjectMapper.class,
						MediaType.APPLICATION_JSON_TYPE);
				ObjectMapper mapper = resolver.getContext(ObjectMapper.class);

				List<Parameter> list = List.ofAll(parameterService.createParameters(sourceName,
						mapper.readValue(body, new TypeReference<java.util.List<CreateParameter>>() {
						})));

				return ServiceUtils.buildEntityResponse(list, Status.CREATED);
			} catch (IOException e) {
				throw new DataAccessException("Could not create multiple Parameters.", e);
			}
		} else {
			return entityService
					.create(V(sourceName), Parameter.class,
							entityService.extractRequestBody(body, sourceName,
									io.vavr.collection.List.of(Unit.class, ParameterSet.class)))
					.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class CreateParameter {
		@JsonProperty("Name")
		public String name;
		@JsonProperty("Unit")
		public String unit;
		@JsonProperty("ParameterSet")
		public String parameterSet;
		@JsonProperty("stringValue")
		public String stringValue;
		@JsonProperty("dateValue")
		public String dateValue;
		@JsonProperty("booleanValue")
		public String booleanValue;
		@JsonProperty("byteValue")
		public String byteValue;
		@JsonProperty("shortValue")
		public String shortValue;
		@JsonProperty("integerValue")
		public String integerValue;
		@JsonProperty("longValue")
		public String longValue;
		@JsonProperty("floatValue")
		public String floatValue;
		@JsonProperty("doubleValue")
		public String doubleValue;
		@JsonProperty("floatComplexValue")
		public String floatComplexValue;
		@JsonProperty("doubleComplexValue")
		public String doubleComplexValue;
	}

	/**
	 * Updates the {@link Parameter} with all parameters set in the given JSON body
	 * of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link Parameter} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link Parameter}
	 */
	@PUT
	@Operation(summary = "Update an existing Parameter", description = "Updates the Parameter with all parameters set in the given body of the request.", responses = {
			@ApiResponse(description = "The updated Parameter", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(
			@io.swagger.v3.oas.annotations.Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.Parameter(description = "ID of the Parameter", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), Parameter.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the {@link Parameter} with all parameters set in the given JSON body
	 * of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link Parameter} to update.
	 * @param body       the body of the request containing the attributes to
	 *                   update. Example: { "4711": { "stringValue": "My first
	 *                   value" }, "4712": { "stringValue": "My second value" } }
	 * @return the updated {@link Parameter}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Operation(summary = "Updates the Parameters with all values set in the body of the request", responses = {
			@ApiResponse(responseCode = "200", description = "Successfully updated Parameters", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response updateParameters(
			@io.swagger.v3.oas.annotations.Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
					@ExampleObject(name = "Update the value of two Parameters", value = "{\n" + "	\"4711\": {\n"
							+ "		\"Value\": \"My first value\"\n" + "	},\n" + "	\"4712\": {\n"
							+ "		\"Value\": \"My second value\"\n" + "	}\n" + "}") })) String body) {
		RequestBody requestBody = RequestBody.create(body);

		final io.vavr.collection.Map<String, Object> entityIdToAttributesMap = requestBody.getValueMapSupplier().get();
		final List<Parameter> updatedEntities = List
				.ofAll(entityService.updateEntitiesAndPersist(Parameter.class, entityIdToAttributesMap, V(sourceName)));
		return ServiceUtils.buildEntityResponse(updatedEntities, Status.OK);
	}

	/**
	 * Deletes and returns the deleted {@link Parameter}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link Pool} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing Parameter", responses = {
			@ApiResponse(description = "The deleted Parameter", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response delete(
			@io.swagger.v3.oas.annotations.Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.Parameter(description = "ID of the Parameter", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), Parameter.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}
