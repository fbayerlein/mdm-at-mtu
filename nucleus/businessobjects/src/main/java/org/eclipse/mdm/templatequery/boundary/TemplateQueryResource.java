/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTTYPE;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.templatequery.entity.TemplateEntityRootDTO;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * @since 5.2.0
 * @author MAF and JZ, Peak Solution GmbH
 */
@Tag(name = "TemplateQuery")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/templatequery/{" + REQUESTPARAM_CONTEXTTYPE + "}")
public class TemplateQueryResource {

	@EJB
	TemplateQueryService templateQueryService;

	/**
	 * Returns the found template entities.
	 * 
	 * @param sourceName  name of the source (MDM {@link Environment} name)
	 * @param contextType {@link ContextType} of the template entities to load
	 * @return the found template entities as a {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTemplate(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_CONTEXTTYPE) String contextType,
			@Parameter(description = "Filter expression", required = false) @QueryParam("filter") String filterString,
			@Parameter(description = "Attributes", required = false) @QueryParam("attributes") String attributeString) {

		List<String> attrList = new ArrayList<>();

		if (!Strings.isNullOrEmpty(attributeString)) {
			attrList.addAll(Arrays.asList(attributeString.split(",")));
		}

		return Response.status(Status.OK)
				.entity(templateQueryService.getTemplate(sourceName,
						ServiceUtils.getContextTypeSupplier(contextType).get(), filterString, attrList))
				.type(MediaType.APPLICATION_JSON).build();
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUpdateTemplate(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_CONTEXTTYPE) String contextType, String body) {
		try {
			TemplateEntityRootDTO tplRoot = new ObjectMapper().readValue(body, TemplateEntityRootDTO.class);
			return Response.status(Status.OK)
					.entity(templateQueryService.createUpdateTemplate(tplRoot, sourceName,
							ServiceUtils.getContextTypeSupplier(contextType).get(), tplRoot))
					.type(MediaType.APPLICATION_JSON).build();
		} catch (Throwable e) {
			throw new MDMEntityAccessException(e.getLocalizedMessage(), e);
		}
	}

}
