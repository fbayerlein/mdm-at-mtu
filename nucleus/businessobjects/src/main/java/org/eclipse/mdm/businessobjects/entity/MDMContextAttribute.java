/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

/**
 * Attribute (Entity for attribute information)
 *
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
public class MDMContextAttribute extends MDMAttribute {

	/** The sort index of the template */
	private Integer sortIndex;
	/** boolean flag if this attribute is readonly or writeable in edit mode */
	private Boolean readOnly;
	/** boolean flag if this attribute is optional in edit mode */
	private Boolean optional;
	/** description text of this attribute */
	private String description;
	/** boolean flag value list ref */
	private Boolean valueListRef;
	/** value list of this attribute */
	private String valueList;

	/**
	 * Default Constructor
	 */
	public MDMContextAttribute() {
		super();
		this.sortIndex = 0;
		this.readOnly = false;
		this.optional = false;
		this.description = "";
		this.valueListRef = false;
		this.valueList = "";
	}

	/**
	 * Constructor
	 *
	 * @param name      name of the attribute value
	 * @param value     value of the attribute value
	 * @param unit      unit of the attribute value
	 * @param dataType  data type of the attribute value
	 * @param enumName  name of the enumeration of the attribute, if its datatype is
	 *                  an enumeration
	 * @param sortIndex optional sort index of the attribute value
	 * @param readOnly  optional flag if it is readonly in edit mode
	 * @param optional  optional flag if it is optinal in edit mode
	 */
	public MDMContextAttribute(String name, Object value, String unit, String dataType, String enumName,
			Integer sortIndex, Boolean readOnly, Boolean optional, String description, Boolean valueListRef,
			String valueList) {
		super(name, value, unit, dataType, enumName);
		this.sortIndex = sortIndex;
		this.readOnly = readOnly;
		this.optional = optional;
		this.description = description;
		this.valueListRef = valueListRef;
		this.valueList = valueList;
	}

	public MDMContextAttribute(MDMAttribute attribute, Integer sortIndex, Boolean readOnly, Boolean optinal,
			String description, Boolean valueListRef, String valueList) {
		super(attribute);
		this.sortIndex = sortIndex;
		this.readOnly = readOnly;
		this.optional = optinal;
		this.description = description;
		this.valueListRef = valueListRef;
		this.valueList = valueList;
	}

	public Integer getSortIndex() {
		return sortIndex;
	}

	public Boolean getReadOnly() {
		return readOnly;
	}

	public Boolean isOptional() {
		return optional;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * @return the valueListRef
	 */
	public Boolean getValueListRef() {
		return valueListRef;
	}

	/**
	 * @return the valueList
	 */
	public String getValueList() {
		return valueList;
	}

}
