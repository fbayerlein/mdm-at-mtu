/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.entity;

import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.base.MoreObjects;

/**
 * 
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SuggestionRequest {

	private List<String> sourceNames;
	private String type;
	private String attrName;
	private String prefix;
	private int maxResults = 100;

	public SuggestionRequest() {
		// empty no arg constructor
	}

	public SuggestionRequest(List<String> sourceNames, String type, String name) {
		this.sourceNames = sourceNames;
		this.type = type;
		this.attrName = name;
	}

	public void setSourceNames(List<String> sourceNames) {
		this.sourceNames = sourceNames;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public List<String> getSourceNames() {
		return sourceNames;
	}

	public String getType() {
		return type;
	}

	public String getAttrName() {
		return attrName;
	}

	public String getPrefix() {
		return prefix;
	}

	public int getMaxResults() {
		return maxResults;
	}

	@Override
	public int hashCode() {
		return Objects.hash(sourceNames, type, attrName, prefix, maxResults);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}
		final SuggestionRequest other = (SuggestionRequest) obj;
		return Objects.equals(this.sourceNames, other.sourceNames) && Objects.equals(this.type, other.type)
				&& Objects.equals(this.attrName, other.attrName) && Objects.equals(this.prefix, other.prefix)
				&& Objects.equals(this.maxResults, other.maxResults);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(SuggestionRequest.class).add("sourceNames", sourceNames).add("type", type)
				.add("attrName", attrName).add("prefix", prefix).add("maxResults", maxResults).toString();
	}
}
