/********************************************************************************
 * Copyright (c) 2015-2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.boundary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.DefaultCore;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.ContextState;

public class DefaultFileUtil {

	public DefaultFileUtil() {
	}

	public void addDefaultFiles(Relation descriptiveFileRelation, List<Result> results, List<Result> defaultFiles) {
		// Group by component-Id collecting all descriptiveFile records as list in map
		// value
		Map<String, List<Record>> m = defaultFiles.stream().collect(Collectors.groupingBy(
				r -> r.getRecord(descriptiveFileRelation.getSource()).getID(),
				Collectors.mapping(r -> r.getRecord(descriptiveFileRelation.getTarget()), Collectors.toList())));

		Map<String, Value> data = new HashMap<>();
		for (Map.Entry<String, List<Record>> e : m.entrySet()) {
			String componentId = e.getKey();

			Value val = ValueType.FILE_RELATION.create(descriptiveFileRelation.getName());
			val.set(ContextState.MEASURED, convertToFileLinks(e.getValue()));

			data.put(componentId, val);
		}

		addValuesToResult(results, data, descriptiveFileRelation.getAttribute());
	}

	private void addValuesToResult(List<Result> result, Map<String, Value> values, Attribute attribute) {
		String idAttribute = attribute.getEntityType().getIDAttribute().getName();

		for (Result r : result) {
			Record record = r.getRecord(attribute.getEntityType());

			Value idValue = record.getValues().get(idAttribute);

			if (idValue.isValid(ContextState.MEASURED)) {
				String id = idValue.extract(ContextState.MEASURED);

				Value fileRelationValue = values.computeIfAbsent(id, x -> getDefaultValue(attribute));
				record.addValue(fileRelationValue);
			}
			if (idValue.isValid(ContextState.ORDERED)) {
				String id = idValue.extract(ContextState.ORDERED);

				Value fileRelationValue = values.computeIfAbsent(id, x -> getDefaultValue(attribute));
				fileRelationValue.swapContext();
				record.addValue(fileRelationValue);
			}
		}
	}

	private Value getDefaultValue(Attribute attribute) {
		return ValueType.FILE_RELATION.create(attribute.getName(), null, false, null);
	}

	private static class StandaloneMDMFile extends MDMFile {

		protected StandaloneMDMFile(Record record) {
			super(new DefaultCore(record));
		}
	}

	private FileLink[] convertToFileLinks(List<Record> records) {
		List<FileLink> fileLinks = new ArrayList<>();
		for (Record record : records) {
			MDMFile file = new StandaloneMDMFile(record);
			fileLinks.add(FileLink.newRemote(file.getLocation(), new MimeType(file.getFileMimeType()),
					file.getDescription(), file.getSize(), file, FileServiceType.AOFILE));
		}
		return fileLinks.toArray(new FileLink[0]);
	}
}
