/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.model.Classification;
import org.eclipse.mdm.api.dflt.model.Domain;
import org.eclipse.mdm.api.dflt.model.ProjectDomain;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.Value;
import io.vavr.collection.List;
import io.vavr.collection.Seq;

/**
 * @author Alexander Knoblauch
 *
 */
@Tag(name = "Status")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/classifications")
public class ClassificationResource {

	@EJB
	private EntityService entityService;

	@EJB
	private ClassificationService classificationService;

	@Parameter(description = "Name of the MDM datasource", required = true)
	@PathParam(REQUESTPARAM_SOURCENAME)
	private String sourceName;

	/**
	 * delegates the request to the {@link EntityService}
	 * 
	 * @param id id of the {@link Classification}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a Classification by ID", responses = {
			@ApiResponse(description = "The Classification", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response findClassification(
			@Parameter(description = "ID of the Classification", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), Classification.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created/existing {@link Classification}.
	 * 
	 * @param body The {@link Classification} to create.
	 * @return the created {@link TestStep} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new Classification or return a existing Classification which references the same Status, ProjectDomain and Domain.", responses = {
			@ApiResponse(description = "The created Classification", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response createOrFind(String body) {

		Seq<Value<?>> extractRequestBody = entityService.extractRequestBody(body, sourceName, io.vavr.collection.List
				.of(org.eclipse.mdm.api.dflt.model.Status.class, ProjectDomain.class, Domain.class));

		java.util.List<Value<?>> asJavaMutable = extractRequestBody.asJavaMutable();

		org.eclipse.mdm.api.dflt.model.Status status = null;
		ProjectDomain projectDomain = null;
		Domain domain = null;

		for (Value<?> v : asJavaMutable) {
			if (v.get() instanceof org.eclipse.mdm.api.dflt.model.Status) {
				status = (org.eclipse.mdm.api.dflt.model.Status) v.get();
			} else if (v.get() instanceof ProjectDomain) {
				projectDomain = (ProjectDomain) v.get();
			} else if (v.get() instanceof Domain) {
				domain = (Domain) v.get();
			}
		}

		Classification classification = classificationService.getClassification(sourceName, status, projectDomain,
				domain);

		Response response = null;

		if (classification == null) {
			response = entityService
					.create(V(sourceName), Classification.class,
							entityService.extractRequestBody(body, sourceName,
									List.of(Domain.class, ProjectDomain.class,
											org.eclipse.mdm.api.dflt.model.Status.class)))
					.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
		} else {
			response = ServiceUtils.buildEntityResponse(classification, Status.OK);
		}

		return response;
	}

}
