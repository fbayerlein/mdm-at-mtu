package org.eclipse.mdm.templatequery.entity;

public class MDMDeserializerException extends RuntimeException {

	private static final long serialVersionUID = -3450501069334231340L;

	public MDMDeserializerException(String message) {
		super(message);
	}
}
