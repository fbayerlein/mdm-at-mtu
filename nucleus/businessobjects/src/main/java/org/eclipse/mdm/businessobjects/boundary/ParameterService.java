/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.DoubleComplex;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.FloatComplex;
import org.eclipse.mdm.api.base.model.Parameter;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.businessobjects.boundary.ParameterResource.CreateParameter;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.control.NavigationActivity;
import org.eclipse.mdm.businessobjects.control.SearchActivity;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import com.google.common.base.Strings;

/**
 * ParameterService Bean implementation with available {@link Parameter}
 * operations
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Stateless
public class ParameterService {

	private static final java.util.Map<Integer, DateTimeFormatter> ODS_DATE_FORMATTERS = new java.util.HashMap<>();

	static {
		ODS_DATE_FORMATTERS.put(4,
				new DateTimeFormatterBuilder().appendPattern("yyyy").parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
						.parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter());
		ODS_DATE_FORMATTERS.put(6, new DateTimeFormatterBuilder().appendPattern("yyyyMM")
				.parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter());
		ODS_DATE_FORMATTERS.put(8, DateTimeFormatter.ofPattern("yyyyMMdd"));
		ODS_DATE_FORMATTERS.put(10, DateTimeFormatter.ofPattern("yyyyMMddHH"));
		ODS_DATE_FORMATTERS.put(12, DateTimeFormatter.ofPattern("yyyyMMddHHmm"));
		ODS_DATE_FORMATTERS.put(14, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		/*
		 * JDK-8031085: DateTimeFormatter won't parse dates with custom format
		 * "yyyyMMddHHmmssSSS"
		 * 
		 * @see http://bugs.java.com/bugdatabase/view_bug.do?bug_id=8031085
		 */
		ODS_DATE_FORMATTERS.put(17, new DateTimeFormatterBuilder().appendPattern("yyyyMMddHHmmss")
				.appendValue(ChronoField.MILLI_OF_SECOND, 3).toFormatter());
	}

	@Inject
	private ConnectorService connectorService;

	@EJB
	private NavigationActivity navigationActivity;

	@EJB
	private SearchActivity searchActivity;

	/**
	 * returns the matching {@link Parameters}s using the given filter or all
	 * {@link Parameters}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link Parameter} result
	 * @return the found {@link Parameter}s
	 */
	public List<Parameter> getParameters(String sourceName, String filter) {

		try {
			ApplicationContext context = this.connectorService.getContextByName(sourceName);
			EntityManager em = context.getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			if (filter == null || filter.trim().length() <= 0) {
				return em.loadAll(Parameter.class);
			}

			if (ServiceUtils.isParentFilter(context, filter, ParameterSet.class)) {
				String id = ServiceUtils.extactIdFromParentFilter(context, filter, ParameterSet.class);
				return this.navigationActivity.getParameters(sourceName, id);
			}
			return this.searchActivity.search(context, Parameter.class, filter);
//			throw new MDMEntityAccessException("Only ParameterSet Root filter supported!");
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * returns a {@link Parameter} identified by the given id.
	 * 
	 * @param parameterId id of the {@link Parameter}
	 * @param sourceName  name of the source (MDM {@link Environment} name)
	 * @param parameterId id of the {@link Parameter}
	 * @return the matching {@link Parameter}
	 */
	public Parameter getParameter(String sourceName, String parameterId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(Parameter.class, parameterId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Creates alle given parameters
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param parameters
	 * @return
	 */
	public List<Parameter> createParameters(String sourceName, List<CreateParameter> parameters) {
		ApplicationContext context = connectorService.getContextByName(sourceName);
		EntityManager em = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		EntityFactory factory = context.getEntityFactory()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityFactory.class));

		List<String> parameterSetIds = parameters.stream().map(p -> p.parameterSet).distinct()
				.collect(Collectors.toList());
		Map<String, List<ParameterSet>> parameterSets = em.load(ParameterSet.class, parameterSetIds).stream()
				.collect(Collectors.groupingBy(p -> p.getID()));

		List<String> unitIds = parameters.stream().map(p -> p.unit).filter(u -> !Strings.isNullOrEmpty(u)).distinct()
				.collect(Collectors.toList());
		Map<String, List<Unit>> units = em.load(Unit.class, unitIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		ZoneId timezone = context.getModelManager()
				.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class)).getTimeZone();

		List<Parameter> createdParameters = new ArrayList<Parameter>();

		for (CreateParameter parameter : parameters) {
			List<ParameterSet> parameterSet = parameterSets.get(parameter.parameterSet);
			if (parameterSet == null || parameterSet.isEmpty()) {
				throw new DataAccessException(
						"No ParameterSet with ID " + parameter.parameterSet + " found in source " + sourceName);
			}

			List<Unit> unit = units.get(parameter.unit);
			Object value = getValue(parameter, timezone);

			if (unit == null) {
				createdParameters.add(factory.createParameter(parameter.name, value, parameterSet.get(0)));
			} else {
				createdParameters.add(factory.createParameter(parameter.name, value, unit.get(0), parameterSet.get(0)));
			}

		}

		try (Transaction t = em.startTransaction()) {
			t.create(createdParameters);
			t.commit();
		}

		return createdParameters;
	}

	private Object getValue(CreateParameter parameter, ZoneId timezone) {

		if (!Strings.isNullOrEmpty(parameter.stringValue)) {
			return parameter.stringValue;
		} else if (!Strings.isNullOrEmpty(parameter.dateValue)) {
			return parseDate(parameter.dateValue, timezone);
		} else if (!Strings.isNullOrEmpty(parameter.booleanValue)) {
			return Boolean.valueOf(parameter.booleanValue);
		} else if (!Strings.isNullOrEmpty(parameter.byteValue)) {
			return Byte.valueOf(parameter.byteValue);
		} else if (!Strings.isNullOrEmpty(parameter.shortValue)) {
			return Short.valueOf(parameter.shortValue);
		} else if (!Strings.isNullOrEmpty(parameter.integerValue)) {
			return Integer.valueOf(parameter.integerValue);
		} else if (!Strings.isNullOrEmpty(parameter.longValue)) {
			return Long.valueOf(parameter.longValue);
		} else if (!Strings.isNullOrEmpty(parameter.floatValue)) {
			return Float.valueOf(parameter.floatValue);
		} else if (!Strings.isNullOrEmpty(parameter.doubleValue)) {
			return Double.valueOf(parameter.doubleValue);
		} else if (!Strings.isNullOrEmpty(parameter.floatComplexValue)) {
			return FloatComplex.valueOf(parameter.floatComplexValue);
		} else if (!Strings.isNullOrEmpty(parameter.doubleComplexValue)) {
			return DoubleComplex.valueOf(parameter.doubleComplexValue);
		}

		throw new DataAccessException("Cannot retrieve a value for parameter: " + parameter.name);
	}

	public static Instant parseDate(String value, ZoneId timezone) {
		return LocalDateTime.parse(value, ODS_DATE_FORMATTERS.get(value.length())).atZone(timezone).toInstant();
	}
}
