/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.mdm.api.base.model.Value;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * DTOs for objects with parent - child relations.
 *
 * @since 5.2.0
 * @author MAF and JZ, Peak Solution GmbH
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@JsonDeserialize(using = TemplateEntityDTODeserializer.class)
public class TemplateEntityDTO {
	protected String name;
	protected String id;
	protected String type;
	protected String sourceType;
	protected String sourceName;

	protected List<Value> templateAttributes = new ArrayList<>();

	protected List<TemplateEntityDTO> subentities = new ArrayList<>();

	protected TemplateEntityDTO parentDTO;

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonSerialize(using = MDMValueListSerializer.class)
	public List<Value> getAttributes() {
		return templateAttributes;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonDeserialize(using = MDMValueListDeserializer.class)
	public void setAttributes(List<Value> templateAttributes) {
		this.templateAttributes = templateAttributes;
	}

	@XmlTransient
	@JsonIgnore
	public List<TemplateEntityDTO> getSubentities() {
		return subentities;
	}

	@XmlTransient
	public void setSubentities(List<TemplateEntityDTO> subentities) {
		this.subentities = subentities;
	}

	@XmlTransient
	@JsonIgnore
	public TemplateEntityDTO getParent() {
		return this.parentDTO;
	}

	@XmlTransient
	public void setParent(TemplateEntityDTO parentDTO) {
		this.parentDTO = parentDTO;
	}
}
