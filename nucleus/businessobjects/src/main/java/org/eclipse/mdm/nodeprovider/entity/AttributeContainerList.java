package org.eclipse.mdm.nodeprovider.entity;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mdm.nodeprovider.control.AttributeContainer;

public class AttributeContainerList extends ArrayList<AttributeContainer> {

	private static final long serialVersionUID = 6462183621412901415L;

	public static AttributeContainerList of(AttributeContainer attribute, AttributeContainer... attributes) {
		AttributeContainerList list = new AttributeContainerList();
		list.add(attribute);
		for (AttributeContainer ac : attributes) {
			list.add(ac);
		}
		return list;
	}

	public static AttributeContainerList of(List<AttributeContainer> otherList) {
		AttributeContainerList list = new AttributeContainerList();
		for (AttributeContainer ac : otherList) {
			list.add(ac);
		}
		return list;
	}

}
