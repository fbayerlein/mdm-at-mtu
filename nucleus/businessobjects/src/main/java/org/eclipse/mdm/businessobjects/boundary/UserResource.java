/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.mdm.businessobjects.entity.User;
import org.eclipse.mdm.connector.entity.MDMPrincipal;

import com.google.common.base.Splitter;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 
 * @deprecated Use {@link ProfileResource} instead.
 *
 */
@Deprecated
@Tag(name = "User")
@Path("/user")
@PermitAll
public class UserResource {

	@Context
	public SecurityContext securityContext;

	@GET
	@Path("/current")
	@Operation(summary = "Read authenticated user information", description = "Read the current user name and which of the given roles the user has.", responses = {
			@ApiResponse(description = "The authenticated user.", content = @Content(schema = @Schema(implementation = User.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response currentUser(
			@Parameter(description = "Comma separated list of roles. This method checks, if the user belongs to each role. If the user does not belong to a role or the role is not queried, it will not be returned.", required = true) @QueryParam("roles") String roles) {
		/*
		 * There is no reliable way in getting the (possibly mapped) roles of a user.
		 * Thus the check if a user is in a role is only against the given list of
		 * roles.
		 */
		List<String> roleList = new ArrayList<>();
		if (roles != null) {
			for (String role : Splitter.on(",").trimResults().split(roles)) {
				if (securityContext.isUserInRole(role)) {
					roleList.add(role);
				}
			}
		}

		User user = new User();
		user.setUsername(securityContext.getUserPrincipal().getName());
		user.setRoles(roleList);

		if (securityContext.getUserPrincipal() instanceof MDMPrincipal) {
			MDMPrincipal userPrincipal = (MDMPrincipal) securityContext.getUserPrincipal();
			user.setMail(userPrincipal.getEmail());
			user.setSurname(userPrincipal.getSurname());
			user.setGivenName(userPrincipal.getGivenName());
			user.setDepartment(userPrincipal.getDepartment());
			user.setTelephone(userPrincipal.getTelephone());
		} else {
			// do nothing! In case of basic authentication no MdMPrincipal is mappable.
			// user.setMail("no.mail@peak.de");
		}
		return Response.ok(user).build();
	}
}
