package org.eclipse.mdm.businessobjects.entity;

import java.util.List;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.v3.oas.annotations.media.Schema;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Schema(description = "Representation of a MDM error")
public class MDMErrorResponse {

	private String message;
	private String traceID;
	private List<String> causes;

	@XmlElement(nillable = true, required = false)
	private String stackTraceAsString;

	public MDMErrorResponse() {

	}

	public MDMErrorResponse(String message, List<String> causes) {
		this.message = message;
		this.traceID = UUID.randomUUID().toString();
		this.causes = causes;
	}

	public void setStackTrace(String stackTraceAsString) {
		this.stackTraceAsString = stackTraceAsString;
	}

	public String getMessage() {
		return message;
	}

	public String getTraceID() {
		return traceID;
	}

	public List<String> getCauses() {
		return causes;
	}

	public String getStackTraceAsString() {
		return stackTraceAsString;
	}

}
