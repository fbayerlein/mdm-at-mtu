/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTOs for objects with parent - child relations.
 *
 * @since 5.2.0
 * @author MAF and JZ, Peak Solution GmbH
 */
public class TemplateEntityTplCompDTO extends TemplateEntityDTO {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public List<TemplateEntityDTO> getTplComps() {
		return subentities.stream().filter(te -> te instanceof TemplateEntityTplCompDTO).collect(Collectors.toList());
	}

	public void setTplComps(List<TemplateEntityTplCompDTO> subentities) {
		this.subentities.addAll(subentities);
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public List<TemplateEntityDTO> getTplAttrs() {
		return subentities.stream().filter(te -> te instanceof TemplateEntityTplAttrDTO).collect(Collectors.toList());
	}

	public void setTplAttrs(List<TemplateEntityTplAttrDTO> subentities) {
		this.subentities.addAll(subentities);
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public List<TemplateEntityDTO> getTplSensors() {
		return subentities.stream().filter(te -> te instanceof TemplateEntityTplSensorDTO).collect(Collectors.toList());
	}

	public void setTplSensors(List<TemplateEntityTplSensorDTO> subentities) {
		this.subentities.addAll(subentities);
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public TemplateEntityDTO getCatComp() {
		return subentities.stream().filter(te -> te instanceof TemplateEntityCatCompDTO).findFirst().orElse(null);
	}

	public void setCatComp(TemplateEntityCatCompDTO subentity) {
		this.subentities.add(subentity);
	}
}
