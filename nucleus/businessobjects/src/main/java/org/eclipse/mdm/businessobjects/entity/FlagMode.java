/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.Arrays;

/**
 * Enum describing which representation of flags is requested.
 *
 */
public enum FlagMode {

	/**
	 * Flags are provided as boolean values
	 */
	BOOLEAN,
	/**
	 * Flags are provided as short values
	 */
	FULL;

	public static FlagMode parse(String name) {
		if (BOOLEAN.name().equalsIgnoreCase(name)) {
			return BOOLEAN;
		} else if (FULL.name().equalsIgnoreCase(name)) {
			return FULL;
		} else {
			throw new IllegalArgumentException("'" + name + "' is no valid value for FlagMode. Expected one of "
					+ Arrays.asList(FlagMode.values()));
		}
	}
}
