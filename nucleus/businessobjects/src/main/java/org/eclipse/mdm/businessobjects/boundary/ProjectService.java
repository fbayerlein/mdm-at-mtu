/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Classification;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.ProjectDomain;
import org.eclipse.mdm.businessobjects.control.I18NActivity;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.control.SearchActivity;
import org.eclipse.mdm.businessobjects.entity.SearchAttribute;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import io.vavr.control.Try;

/**
 * ProjectService Bean implementation with available {@link Project} operations
 * 
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
@Stateless
public class ProjectService {

	@Inject
	private ConnectorService connectorService;
	@EJB
	private I18NActivity i18nActivity;
	@EJB
	private SearchActivity searchActivity;
	@EJB
	private EntityService entityService;
	@EJB
	private ClassificationService classificationService;

	/**
	 * Default no-arg constructor for EJB
	 */
	public ProjectService() {
		// Default no-arg constructor for EJB
	}

	/**
	 * Contructor for unit testing
	 * 
	 * @param connectorService {@link ConnectorService} to use
	 * @param searchActivity   {@link SearchActivity} to use
	 * @param i18nActivity     {@link I18NActivity} to use
	 */
	ProjectService(ConnectorService connectorService, SearchActivity searchActivity, I18NActivity i18nActivity) {
		this.connectorService = connectorService;
		this.searchActivity = searchActivity;
		this.i18nActivity = i18nActivity;
	}

	/**
	 * returns the matching {@link Project}s using the given filter or all
	 * {@link Project}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link Project} result
	 * @return the found {@link Project}s
	 */
	public List<Project> getProjects(String sourceName, String filter) {

		try {
			ApplicationContext context = this.connectorService.getContextByName(sourceName);
			EntityManager em = context.getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			if (filter == null || filter.trim().length() <= 0) {
				return em.loadAll(Project.class);
			}

			return this.searchActivity.search(context, Project.class, filter);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Returns the {@link SearchAttribute} for the entity type {@link Project} in
	 * the given data source.
	 * 
	 * @param sourceName The name of the data source.
	 * @return the found {@link SearchAttribute}s
	 */
	public List<SearchAttribute> getSearchAttributes(String sourceName) {
		return this.searchActivity.listAvailableAttributes(this.connectorService.getContextByName(sourceName),
				Project.class);
	}

	/**
	 * returns a {@link Project} identified by the given id.
	 * 
	 * @param projectId  id of the {@link Project}
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link Project}
	 * @return the matching {@link Project}
	 */
	public Project getProject(String sourceName, String projectId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(Project.class, projectId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * returns localized {@link Project} attributes
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link Project} attributes
	 */
	@Deprecated
	public Map<Attribute, String> localizeAttributes(String sourceName) {
		return this.i18nActivity.localizeAttributes(sourceName, Project.class);
	}

	/**
	 * returns the localized {@link Project} type name
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link Project} type name
	 */
	@Deprecated
	public Map<EntityType, String> localizeType(String sourceName) {
		return this.i18nActivity.localizeType(sourceName, Project.class);
	}

	public String findProjectDomain(String sourceName, String projectName) {
		QueryService queryService = this.connectorService.getContextByName(sourceName).getQueryService()
				.orElseThrow(() -> new MDMEntityAccessException("QueryService not present!"));
		ModelManager mm = this.connectorService.getContextByName(sourceName).getModelManager()
				.orElseThrow(() -> new MDMEntityAccessException("ModelManagery not present!"));

		EntityType entityTypeProjectDomain = mm.getEntityType(ProjectDomain.class);
		Condition condProjectDomainName = ComparisonOperator.CASE_INSENSITIVE_EQUAL
				.create(entityTypeProjectDomain.getAttribute("Name"), projectName);

		List<Result> fetchProjectDomain = queryService.createQuery().selectID(entityTypeProjectDomain)
				.fetch(Filter.and().addAll(condProjectDomainName));

		List<String> projectDomainIds = fetchProjectDomain.stream().map(r -> r.getRecord(entityTypeProjectDomain))
				.map(Record::getID).collect(Collectors.toList());

		if (projectDomainIds.size() > 1) {
			throw new IllegalStateException(
					String.format("More than one ProjectDomain instance with name \"%s\" found in destination!",
							projectName));
		} else if (projectDomainIds.size()  == 1) {
			return projectDomainIds.get(0);
		} else {
			return null;
		}
	}
	
	public Try<Project> deleteProject(String sourceName, Try<Project> project) {

		String projectDomainId = findProjectDomain(sourceName, project.get().getName());
		if (projectDomainId != null) {
			Try<ProjectDomain> projectDomain = entityService.find(V(sourceName), ProjectDomain.class,
					V(projectDomainId));

			// find classifications
			java.util.List<Classification> classifications = this.classificationService.findClassifications(sourceName,
					projectDomain.get());
			for (Classification c : classifications) {
				entityService.delete(V(sourceName), Try.of(() -> c));
			}
			entityService.delete(V(sourceName), projectDomain);
		}
		return entityService.delete(V(sourceName), project);
	}
}
