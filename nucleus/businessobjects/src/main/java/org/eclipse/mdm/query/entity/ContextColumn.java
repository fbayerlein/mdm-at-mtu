/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.entity;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.base.MoreObjects;

public class ContextColumn extends Column {
	@JsonInclude(Include.ALWAYS)
	private Object orderedValue;

	public ContextColumn(String type, String attribute, String valueType, Object value, Object orderedValue,
			String unit) {
		super(type, attribute, valueType, value, unit);
		this.orderedValue = orderedValue;
	}

	public Object getOrderedValue() {
		return orderedValue;
	}

	public void setOrderedValue(String orderedValue) {
		this.orderedValue = orderedValue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getType(), getAttribute(), getValueType(), getValue(), getUnit(), orderedValue);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}
		final ContextColumn other = (ContextColumn) obj;
		return Objects.equals(this.getType(), other.getType())
				&& Objects.equals(this.getAttribute(), other.getAttribute())
				&& Objects.equals(this.getValueType(), other.getValueType())
				&& Objects.equals(this.getValue(), other.getValue()) && Objects.equals(this.getUnit(), other.getUnit())
				&& Objects.equals(this.getOrderedValue(), other.getOrderedValue());
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(Column.class).add("type", getType()).add("attribute", getAttribute())
				.add("valueType", getValueType()).add("value", getValue()).add("orderedValue", getOrderedValue())
				.add("unit", getUnit()).toString();
	}
}
