/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.businessobjects.boundary.ChannelResource;
import org.eclipse.mdm.nodeprovider.entity.NodeProvider;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.eclipse.mdm.protobuf.Mdm.NodeProviderResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * {@link NodeProvider} resource
 * 
 *
 */
@Tag(name = "NodeProvder")
@Path("/nodeprovider")
@Produces({ MediaType.APPLICATION_JSON, MEDIATYPE_APPLICATION_PROTOBUF })
public class NodeProviderResource {

	private static final Logger LOG = LoggerFactory.getLogger(ChannelResource.class);

	@EJB
	private NodeProviderService nodeProviderService;

	public NodeProviderResource() {
	}

	public NodeProviderResource(NodeProviderService nodeProviderService) {
		this.nodeProviderService = nodeProviderService;
	}

	@GET
	@Operation(summary = "Load nodeprovider IDs", description = "Get list of available nodeprovider IDs", responses = {
			@ApiResponse(description = "The IDs of the available nodeproviders", content = {
					@Content(array = @ArraySchema(schema = @Schema(implementation = String.class))) }),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response getNodeProviders() {
		return Response.ok(this.nodeProviderService.getNodeProvidersSorted()).build();
	}

	@GET
	@Path("{NODEPROVIDER}")
	@Operation(summary = "Load root nodes", description = "Get list of root nodes of a nodeprovider", responses = {
			@ApiResponse(description = "The nodes at root level", content = @Content(schema = @Schema(implementation = NodeProviderResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response getRoots(
			@Parameter(description = "ID of the nodeprovider", required = true) @PathParam("NODEPROVIDER") String nodeProviderId) {
		try {
			java.util.List<Node> nodes = this.nodeProviderService.getNodeProvider(nodeProviderId).getRoots();
			return Response.status(Status.OK).entity(NodeProviderResponse.newBuilder().addAllData(nodes).build())
					.build();

		} catch (RuntimeException e) {
			LOG.error(e.getMessage(), e);
			throw new WebApplicationException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
		}
	}

	@GET
	@Path("{NODEPROVIDER}/{PARENT_ID}")
	@Operation(summary = "Load children", description = "Get children of given node for a nodeprovider", responses = {
			@ApiResponse(description = "The requested child nodes", content = @Content(schema = @Schema(implementation = NodeProviderResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response getChildren(
			@Parameter(description = "ID of the nodeprovider", required = true) @PathParam("NODEPROVIDER") String nodeProviderId,
			@Parameter(description = "serial of the parent node", required = true) @PathParam("PARENT_ID") String parentId) {
		try {
			java.util.List<Node> nodes = this.nodeProviderService.getNodeProvider(nodeProviderId)
					.getChildren(SerializationUtil.deserializeNode(parentId));
			return Response.status(Status.OK).entity(NodeProviderResponse.newBuilder().addAllData(nodes).build())
					.build();

		} catch (RuntimeException e) {
			LOG.error(e.getMessage(), e);
			throw new WebApplicationException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{NODEPROVIDER}")
	@Operation(summary = "Load tree path", description = "Get a list all parent nodes including the current node", responses = {
			@ApiResponse(description = "List of all parent nodes including the given node", content = @Content(schema = @Schema(implementation = NodeProviderResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response getTreePath(
			@Parameter(description = "ID of the nodeprovider", required = true) @PathParam("NODEPROVIDER") String nodeProviderId,
			@RequestBody(description = "Node that is the child of the returned TreePath.") Node node) {
		try {
			java.util.List<Node> nodes = this.nodeProviderService.getNodeProvider(nodeProviderId).getTreePath(node);
			return Response.status(Status.OK).entity(NodeProviderResponse.newBuilder().addAllData(nodes).build())
					.build();

		} catch (RuntimeException e) {
			LOG.error(e.getMessage(), e);
			throw new WebApplicationException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
		}
	}
}
