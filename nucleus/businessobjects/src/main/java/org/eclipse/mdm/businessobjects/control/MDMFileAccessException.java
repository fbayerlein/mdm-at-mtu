/*******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.businessobjects.control;

public class MDMFileAccessException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2670077604805809256L;

	public MDMFileAccessException(String message) {
		super(message);
	}

	public MDMFileAccessException(String message, Throwable t) {
		super(message, t);
	}
}
