/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;

/**
 * ContextCollection (Entity for context data (ordered and measured))
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
public class ContextCollection {

	// consistent naming to ContextActivity.CONTEXT_GROUP_MEASURED
	public Map<ContextType, List<MDMContextEntity>> measured = new HashMap<>();
	// consistent naming to ContextActivity.CONTEXT_GROUP_ORDERED
	public Map<ContextType, List<MDMContextEntity>> ordered = new HashMap<>();

	/**
	 * set the measured context data map
	 * 
	 * @param contextMap the measured context data map
	 */
	public void setMeasured(Map<ContextType, ContextRoot> contextMap) {

		for (java.util.Map.Entry<ContextType, ContextRoot> setEntry : contextMap.entrySet()) {

			ContextType contextType = setEntry.getKey();
			ContextRoot contextRoot = setEntry.getValue();

			this.measured.put(contextType, new ArrayList<>());

			for (ContextComponent contextComponent : contextRoot.getContextComponents()) {
				MDMContextEntity entity = new MDMContextEntity(contextComponent);
				this.measured.get(contextType).add(entity);
			}

			// sort by SortIndex
			Collections.sort(this.measured.get(contextType), Comparator.comparing(MDMContextEntity::getSortIndex,
					Comparator.nullsFirst(Comparator.naturalOrder())));

		}
	}

	/**
	 * sets the ordered context data map
	 * 
	 * @param contextMap the ordered context data map
	 */
	public void setOrdered(Map<ContextType, ContextRoot> contextMap) {

		for (java.util.Map.Entry<ContextType, ContextRoot> setEntry : contextMap.entrySet()) {

			ContextType contextType = setEntry.getKey();
			ContextRoot contextRoot = setEntry.getValue();

			this.ordered.put(contextType, new ArrayList<>());

			for (ContextComponent contextComponent : contextRoot.getContextComponents()) {
				MDMContextEntity entity = new MDMContextEntity(contextComponent);
				this.ordered.get(contextType).add(entity);
			}

			// sort by SortIndex
			Collections.sort(this.ordered.get(contextType), Comparator.comparing(MDMContextEntity::getSortIndex,
					Comparator.nullsFirst(Comparator.naturalOrder())));
		}
	}

}
