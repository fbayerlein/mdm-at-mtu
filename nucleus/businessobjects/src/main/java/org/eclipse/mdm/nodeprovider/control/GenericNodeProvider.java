/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.control;

import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.el.ValueExpression;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Aggregation;
import org.eclipse.mdm.api.base.query.BooleanOperator;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.FilterItem;
import org.eclipse.mdm.api.base.query.Query;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.businessobjects.control.FilterParser;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.nodeprovider.entity.AttributeContainerList;
import org.eclipse.mdm.nodeprovider.entity.FilterAttribute;
import org.eclipse.mdm.nodeprovider.entity.NodeLevel;
import org.eclipse.mdm.nodeprovider.entity.NodeProvider;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderRoot;
import org.eclipse.mdm.nodeprovider.entity.Order;
import org.eclipse.mdm.nodeprovider.entity.ValuePrecision;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm.Node;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * Configurable implementation of a {@link NodeProvider}. A
 * {@link NodeProviderRoot} must be provided, which defines the structure of the
 * tree defined by the returned {@link Node}s.
 *
 */
public class GenericNodeProvider implements NodeProvider {

	private final ConnectorService connectorService;

	private final NodeProviderRoot root;

	private final MDMExpressionLanguageService elService;

	private final ZoneId zoneId;

	/**
	 * Construct a new {@link GenericNodeProvider} using the given
	 * {@link ConnectorService} and {@link NodeProviderRoot}.
	 * 
	 * @param connectorService
	 * @param root
	 */
	public GenericNodeProvider(ConnectorService connectorService, NodeProviderRoot root,
			MDMExpressionLanguageService expressionLanguageService) {
		this.connectorService = connectorService;
		this.root = root;
		this.elService = expressionLanguageService;
		// TODO mkoller: Here instead of UTC the timezone of the client is needed and
		// the HTTP API has to be extended so the client can provide it.
		zoneId = ZoneId.of("UTC");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return root.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Node> getRoots() {
		return getChildren(null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Node> getChildren(Node parent) {
		List<Node> nodes = new ArrayList<>();

		for (ApplicationContext context : this.connectorService.getContexts()) {
			root.getChildNodeLevel(context, parent).map(nl -> loadChildNodes(context, parent, nl))
					.ifPresent(nodes::addAll);
		}

		// Sort the complete list again, as only Nodes from each context are sorted.
		// TODO 30.11.2020: sort by defined OrderAttributes
		nodes.sort(compareByLabel);

		return nodes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Node> getTreePath(Node node) {
		ApplicationContext context = this.connectorService.getContextByName(node.getSource());
		NodeLevel nodeLevel = root.getNodeLevel(context, node);

		if (nodeLevel == null) {
			return Collections.emptyList();
		}

		List<Node> nodes = new ArrayList<>();
		Node parent;
		if (nodeLevel.isVirtual()) {
			// convert the current virtual node
			Filter childFilter = getFilter(context, node);
			addVirtualNodes(context, node, nodeLevel, childFilter, nodes);
			parent = node;
		} else {
			// reload node to get correct label
			Query query = createQueryForNodeLevel(context, nodeLevel);

			Filter filter = createFilterFromIdOrLabel(node, nodeLevel);
			for (Result r : query.fetch(filter)) {
				nodes.add(convertNode(r, node.getSource(), nodeLevel, Filter.and(), Aggregation.NONE, true));
			}

			if (nodes.size() != 1) {
				throw new NodeProviderException("Expected exactly one node, but got " + nodes.size() + "!");
			}

			parent = nodes.get(0);
		}

		while ((parent = loadParentNode(context, parent, nodes)) != null) {
			nodes.add(parent);
		}

		return reverseListAndFilters(context, nodes);
	}

	/**
	 * Loads the child node of the given parent Node and NodeLevel.
	 * 
	 * @param context        ApplicationContext
	 * @param parent         parent node
	 * @param childNodeLevel nodeLevel of the child nodes
	 * @return the children of the given node
	 */
	private List<Node> loadChildNodes(ApplicationContext context, Node parent, NodeLevel childNodeLevel) {
		String sourceName = context.getSourceName();

		Filter filter = getFilter(context, parent);

		if (isEnvironment(childNodeLevel.getEntityType())) {
			return createQueryForNodeLevel(context, childNodeLevel).fetch().stream()
					.map(result -> convertNode(result, sourceName, childNodeLevel, filter, Aggregation.NONE, false))
					.collect(Collectors.toList());
		}

		if (!sourceName.equalsIgnoreCase(parent.getSource()) || !sourceNameMatches(filter, sourceName)) {
			return Collections.emptyList();
		}

		if (childNodeLevel.isVirtual()) {
			// We use the Project entity as query root to request the values for the label
			List<Result> results = getSearchService(context).getFilterResults(Project.class,
					childNodeLevel.getLabelAttributes(), filter, childNodeLevel.getContextState());

			// Filter values are always in measured
			return results.stream()
					.map(result -> convertNodeByLabel(result, sourceName, childNodeLevel, filter, Aggregation.DISTINCT,
							() -> null))
					.filter(node -> !Strings.isNullOrEmpty(node.getLabel())).filter(distinctByKey(Node::getLabel))
					.collect(Collectors.toList());
		} else {
			return fetchNodes(context, childNodeLevel, filter);
		}
	}

	/**
	 * Loads the parent node of the given Node and NodeLevel.
	 * 
	 * @param context        ApplicationContext
	 * @param node           The given node
	 * @param remainingNodes To filter on virtual nodes we need more than just
	 *                       parent filter, so we need other nodes as well.
	 * @return the parent node of the given node, or null if the given node is a
	 *         root node.
	 */
	protected Node loadParentNode(ApplicationContext context, Node node, List<Node> remainingNodes) {
		NodeLevel nodeLevel = root.getNodeLevel(context, node);
		NodeLevel parentLevel = root.getParentNodeLevel(context, nodeLevel);

		if (parentLevel == null) {
			return null;
		}

		Filter childFilter = getFilter(context, node);
		if (childFilter.isEmtpty()) {
			throw new NodeProviderException("Filter cannot be empty! Received node '" + node + "' with empty filter.");
		}

		if (isEnvironment(parentLevel.getEntityType())) {
			Query query = createQueryForNodeLevel(context, parentLevel);

			for (Result r : query.fetch()) {
				return convertNode(r, context.getSourceName(), parentLevel, childFilter, Aggregation.NONE, false);
			}
			return null;
		}

		List<Node> nodes = new ArrayList<>();
		if (parentLevel.isVirtual()) {
			final Filter filter;
			if (remainingNodes == null) {
				// Fallback to childFilter
				filter = childFilter;
			} else {
				// If we have virtual nodes the filter criteria must contain all filters
				filter = combineAllFiltersWithAnd(context, remainingNodes);
			}

			addVirtualNodes(context, node, parentLevel, filter, nodes);
		} else {
			Filter childFilterId;
			if (nodeLevel.isVirtual()) {
				childFilterId = childFilter;
			} else {
				// if we have a real node, we fetch the parent based on the id of the child
				// instead of the child filter. This way we are sure, that only one parent is
				// found.
				// For example, if there are 2 pools with name 'MyPool' under different Projects
				// with the same name, the child filter "Pool.Name eq 'MyPool'" will return both
				// Projects.
				childFilterId = Filter.idOnly(nodeLevel.getEntityType(), node.getId());
			}
			for (Node n : fetchNodes(context, parentLevel, childFilterId)) {
				Filter filter = Filter.and();
				// only add Filter conditions that refer to the parent level
				for (FilterItem i : getFilter(context, n)) {
					if (i.isCondition()
							&& i.getCondition().getAttribute().getEntityType().equals(parentLevel.getEntityType())) {
						addConditionIfNotPresent(filter, i.getCondition());
					}
				}

				nodes.add(n.toBuilder().setFilter(FilterParser.toString(filter)).build());
			}
		}

		if (nodes.size() != 1) {
			throw new NodeProviderException("Expected exactly one node but got " + nodes.size() + "!");
		}
		return nodes.get(0);
	}

	/**
	 * Combines all filters of the given nodes with AND and returns the filter.
	 * 
	 * @param context ApplicationContext
	 * @param nodes   The nodes to add the filter
	 * @return A filter that combines all filters from given nodes with AND
	 */
	private Filter combineAllFiltersWithAnd(ApplicationContext context, List<Node> nodes) {
		Filter filter = Filter.and();
		for (Node node : nodes) {
			filter = filter.merge(getFilter(context, node));
		}
		return filter;
	}

	/**
	 * Add nodes and virtual nodes to the list of nodes
	 *
	 * @param context    Application context
	 * @param node       The given node
	 * @param nodeLevel  The node level element of the given node
	 * @param nodeFilter The node filter options
	 * @param nodes      List of nodes to add the converted nodes to
	 */
	private void addVirtualNodes(ApplicationContext context, Node node, NodeLevel nodeLevel, Filter nodeFilter,
			List<Node> nodes) {

		// We use the TestStep entity as query root to request the values for the label
		List<Result> results = getSearchService(context).fetchResults(TestStep.class, nodeLevel.getLabelAttributes(),
				nodeFilter, "");

		for (Result r : results) {
			nodes.add(convertNode(r, node.getSource(), nodeLevel, nodeFilter, Aggregation.NONE, false));
			break;
		}
	}

	/**
	 * Creates an ID filter for non virtual nodes or a filter based on the label for
	 * virtual nodes.
	 * 
	 * @param node      Node to build the Filter for
	 * @param nodeLevel NodeLevel of the Node
	 * @return created Filter
	 */
	private Filter createFilterFromIdOrLabel(Node node, NodeLevel nodeLevel) {
		if (nodeLevel.isVirtual()) {
			return Filter.and().add(ComparisonOperator.EQUAL.create(nodeLevel.getContextState(),
					getLabelAttribute(nodeLevel), node.getLabel()));
		} else {
			return Filter.and().id(nodeLevel.getEntityType(), node.getId());
		}
	}

	/**
	 * Reverse the Nodes and build up the filters of the individual Node from root
	 * to leaf.
	 * 
	 * @param context  ApplicationContext
	 * @param treePath tree path build from leaf to root.
	 * @return list of Nodes build from root to leaf, e.g. the root will be the
	 *         first Node in the returned list and the leaf will be the last Node.
	 */
	private List<Node> reverseListAndFilters(ApplicationContext context, List<Node> treePath) {
		if (treePath == null || treePath.isEmpty()) {
			return Collections.emptyList();
		}

		List<Node> finalList = new ArrayList<>();

		Filter filter = Filter.and();
		for (Node node : Lists.reverse(treePath)) {
			filter = mergeFilter(filter, getFilter(context, node));

			NodeLevel nodeLevel = root.getNodeLevel(context, node);
			finalList.add(SerializationUtil.createNode(node.getSource(), nodeLevel, node.getId(),
					FilterParser.toString(filter), node.getLabel()));
		}

		return finalList;
	}

	/**
	 * Merges Filter toMerge into filter. Only works for Filter with operator 'and'
	 * and not nested conditions
	 * 
	 * @param filter
	 * @param toMerge
	 * @return
	 */
	private Filter mergeFilter(Filter filter, Filter toMerge) {
		Filter newFilter = filter;

		for (FilterItem item : toMerge) {
			if (item.isCondition()) {
				newFilter = mergeFilter(filter, item.getCondition());
			}
		}
		return newFilter;
	}

	/**
	 * Merges the condition toMerge into Filter filter. Only works, if filter has
	 * operator 'and'.
	 * 
	 * @param filter
	 * @param toMerge
	 * @return
	 */
	private Filter mergeFilter(Filter filter, Condition toMerge) {
		Filter newFilter = filter.copy();

		if (filter.isEmtpty()) {
			newFilter.add(toMerge);
			return newFilter;
		}

		boolean found = false;
		for (FilterItem item : filter) {
			if (item.isCondition()) {
				if (item.getCondition().equals(toMerge)) {
					found = true;
				}
			}
		}
		if (!found) {
			newFilter.add(toMerge);
		}
		return newFilter;
	}

	/**
	 * @param context   ApplicationContext
	 * @param nodeLevel nodeLevel to search for nodes
	 * @param filter    filter used for the search
	 * @return list with the loaded nodes
	 */
	private List<Node> fetchNodes(ApplicationContext context, NodeLevel nodeLevel, Filter filter) {

		Class<? extends Entity> entityClass = getEntityClass(getFilterAttributes(nodeLevel).get(0).getEntityType());

		List<Attribute> attributes = getAttributesFromNodeLevel(nodeLevel);
		List<Result> results = getSearchService(context).fetchResults(entityClass, attributes, filter, "");

		return results.stream()
				.map(r -> convertNode(r, context.getSourceName(), nodeLevel, filter, Aggregation.NONE, false))
				.filter(distinctByKey(Node::getLabel)).collect(Collectors.toList());
	}

	/**
	 * @param entityType
	 * @return entity class of the given entityType
	 * @throws NodeProviderException if entity class is not found
	 */
	private Class<? extends Entity> getEntityClass(EntityType entityType) {
		String entityName = ServiceUtils.workaroundForTypeMapping(entityType);

		switch (entityName) {
		case "Environment":
			return Environment.class;
		case "Project":
			return Project.class;
		case "Pool":
			return Pool.class;
		case "Test":
			return Test.class;
		case "TestStep":
			return TestStep.class;
		case "Measurement":
			return Measurement.class;
		case "ChannelGroup":
			return ChannelGroup.class;
		case "Channel":
			return Channel.class;
		case "Quantity":
			return Quantity.class;
		default:
			throw new NodeProviderException("Could not find entity class for entity with name '" + entityName
					+ "'. Maybe you forgot to set virtual=true?");
		}
	}

	/**
	 * Checks if the given filter matches the given sourceName
	 * 
	 * @param filter
	 * @param sourceName
	 * @return true, if the sourceName could match the filter.
	 */
	private boolean sourceNameMatches(Filter filter, String sourceName) {
		if (filter.isEmtpty()) {
			return true;
		}
		for (FilterItem filterItem : filter) {
			if (filterItem.isCondition()) {
				Attribute a = filterItem.getCondition().getAttribute();
				if (isEnvironment(a.getEntityType()) && "Id".equals(a.getName())) {
					return sourceName.equals(filterItem.getCondition().getValue().extract(ValueType.STRING));
				}
			} else if (filterItem.isBooleanOperator() && filterItem.getBooleanOperator() == BooleanOperator.AND) {
				continue;
			} else {
				throw new NodeProviderException("Filter not supported yet: " + filter);
			}
		}
		return true;
	}

	/**
	 * Returns the filter of the given node.
	 * 
	 * @param context application context
	 * @param node
	 * @return the filter of the node or an empty Filter
	 */
	private Filter getFilter(ApplicationContext context, Node node) {

		Filter filter = Filter.and();
		if (node != null) {
			String filterStr = node.getFilter();

			if (!filterStr.isEmpty()) {
				ModelManager mm = context.getModelManager()
						.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));
				filter = FilterParser.parseFilterString(mm.listEntityTypes(), filterStr);
			}
		}
		return filter;
	}

	/**
	 * Converts a Result to a Node
	 * 
	 * @param result          Result to convert
	 * @param sourceName      name of the source
	 * @param nodeLevel       NodeLevel
	 * @param parentFilter    Filter of the parent Node
	 * @param aggregation     Aggregation enumeration
	 * @param filterAttribute True to use the filter attribute, false to not use it
	 *                        for non-virtual nodes
	 * @return Result converted to a Node
	 */
	private Node convertNode(Result result, String sourceName, NodeLevel nodeLevel, Filter parentFilter,
			Aggregation aggregation, boolean filterAttribute) {

		return convertNodeByLabel(result, sourceName, nodeLevel, parentFilter, aggregation,
				() -> idSupplier(result, nodeLevel, aggregation, filterAttribute));
	}

	private String idSupplier(Result result, NodeLevel nodeLevel, Aggregation aggregation, boolean filterAttribute) {

		Value idValue;
		if (nodeLevel.isVirtual()) {
			return null;
		} else if (filterAttribute && !nodeLevel.getFilterAttributes().isEmpty()) {
			idValue = result.getValue(nodeLevel.getFilterAttributes().get(0).getAttribute(), aggregation);
		} else {
			// default to ID attribute if we don't have another identifier
			idValue = result.getRecord(nodeLevel.getEntityType()).getIDValue();
		}
		if (idValue.isValid()) {
			return idValue.extract().toString();
		} else {
			return null;
		}
	}

	/**
	 * Converts a Result to a Node
	 *
	 * @param result       Result to convert
	 * @param sourceName   name of the source
	 * @param nodeLevel    NodeLevel
	 * @param parentFilter Filter of the parent Node
	 * @return Result converted to a Node
	 */
	private Node convertNodeByLabel(Result result, String sourceName, NodeLevel nodeLevel, Filter parentFilter,
			Aggregation aggregation, Supplier<String> idSupplier) {
		ContextState contextState = nodeLevel.getContextState();

		AttributeContainerList labelAttrContainers = convertToAttributeContainers(contextState, result, aggregation,
				nodeLevel.getLabelAttributes(), nodeLevel.getValuePrecision());

		String label = createLabel(nodeLevel, labelAttrContainers);

		List<Pair<FilterAttribute, AttributeContainer>> filterAttrContainers = convertToAttributeContainers2(
				contextState, result, aggregation, nodeLevel.getFilterAttributes(), nodeLevel.getValuePrecision());

		Filter newFilter = getNewNodeFilter(contextState, nodeLevel, parentFilter, filterAttrContainers);

		return SerializationUtil.createNode(sourceName, nodeLevel, idSupplier.get(), FilterParser.toString(newFilter),
				label);
	}

	/**
	 * Creates label for {@link NodeLevel} respecting the NodeLevels
	 * {@link ValueExpression}.
	 *
	 * @param nodeLevel           the {@link NodeLevel}
	 * @param attributeContainers the label attributes
	 * @return
	 */
	private String createLabel(NodeLevel nodeLevel, AttributeContainerList attributeContainers) {

		return (String) elService.evaluateValueExpression(
				elService.parseValueExpression(nodeLevel.getLabelExpression(), String.class), attributeContainers);
	}

	private List<Pair<FilterAttribute, AttributeContainer>> convertToAttributeContainers2(ContextState contextState,
			Result result, Aggregation aggregation, List<FilterAttribute> attributes, ValuePrecision precision) {
		if (attributes == null) {
			return Collections.emptyList();
		}
		return attributes.stream().map(attribute -> Pair.of(attribute,
				converToAttributeContainer(attribute.getAttribute(), contextState, result, aggregation, precision)))
				.collect(Collectors.toList());
	}

	private AttributeContainerList convertToAttributeContainers(ContextState contextState, Result result,
			Aggregation aggregation, List<Attribute> attributes, ValuePrecision precision) {
		return attributes.stream()
				.map(attribute -> converToAttributeContainer(attribute, contextState, result, aggregation, precision))
				.collect(Collectors.collectingAndThen(Collectors.toList(), AttributeContainerList::of));
	}

	private AttributeContainer converToAttributeContainer(Attribute attribute, ContextState contextState, Result result,
			Aggregation aggregation, ValuePrecision precision) {
		Value value = result.getValue(attribute, aggregation);
		precision.applyOn(value, zoneId);

		Object extractedValue = value.extract(contextState);

		// for DS_* attributes we get a sequence of values, but we only can use one
		// value, so we use the first value
		if (value.getValueType().isStringSequence()) {
			String[] strings = value.extract(ValueType.STRING_SEQUENCE);
			if (strings == null || strings.length == 0) {
				extractedValue = "";
			} else {
				extractedValue = strings[0];
			}
		} else if (value.getValueType().isDateSequence()) {
			Instant[] dates = value.extract(ValueType.DATE_SEQUENCE);
			if (dates == null || dates.length == 0) {
				extractedValue = Instant.MIN;
			} else {
				extractedValue = dates[0];
			}
		} else if (value.getValueType().isLongSequence()) {
			long[] longs = value.extract(ValueType.LONG_SEQUENCE);
			if (longs == null || longs.length == 0) {
				extractedValue = 0L;
			} else {
				extractedValue = longs[0];
			}
		} else if (value.getValueType().isIntegerSequence()) {
			int[] ints = value.extract(ValueType.INTEGER_SEQUENCE);
			if (ints == null || ints.length == 0) {
				extractedValue = 0;
			} else {
				extractedValue = ints[0];
			}
		} else if (value.getValueType().isShortSequence()) {
			short[] shorts = value.extract(ValueType.SHORT_SEQUENCE);
			if (shorts == null || shorts.length == 0) {
				extractedValue = (short) 0;
			} else {
				extractedValue = shorts[0];
			}
		} else if (value.getValueType().isByteSequence()) {
			byte[] bytes = value.extract(ValueType.BYTE_SEQUENCE);
			if (bytes == null || bytes.length == 0) {
				extractedValue = (byte) 0;
			} else {
				extractedValue = bytes[0];
			}
		} else if (value.getValueType().isFloatSequence()) {
			float[] floats = value.extract(ValueType.FLOAT_SEQUENCE);
			if (floats == null || floats.length == 0) {
				extractedValue = 0f;
			} else {
				extractedValue = floats[0];
			}
		} else if (value.getValueType().isDoubleSequence()) {
			double[] doubles = value.extract(ValueType.DOUBLE_SEQUENCE);
			if (doubles == null || doubles.length == 0) {
				extractedValue = 0.0;
			} else {
				extractedValue = doubles[0];
			}
		}

		boolean valid = value.isValid(contextState);
		return new AttributeContainer(contextState, attribute, valid, extractedValue);
	}

	/**
	 * Creates a Query to load nodes for the given NodeLevel
	 * 
	 * @param context   ApplicationContext in which the query is created
	 * @param nodeLevel the NodeLevel of the requested nodes.
	 * @return Query for the given NodeLevel
	 */
	private Query createQueryForNodeLevel(ApplicationContext context, NodeLevel nodeLevel) {
		List<Attribute> attributes = getAttributesFromNodeLevel(nodeLevel);
		Query query = context.getQueryService().orElseThrow(() -> new ServiceNotProvidedException(QueryService.class))
				.createQuery().select(attributes).group(attributes);

		nodeLevel.getOrderAttributes().forEach(oa -> query.order(oa.getAttribute(), oa.getOrder() == Order.ASCENDING));

		return query;
	}

	private List<Attribute> getAttributesFromNodeLevel(NodeLevel nodeLevel) {
		Set<Attribute> attributes = new HashSet<>();
		attributes.addAll(getFilterAttributes(nodeLevel));
		attributes.addAll(nodeLevel.getLabelAttributes());

		return new ArrayList<>(attributes);
	}

	private Filter getNewNodeFilter(ContextState contextState, NodeLevel nodeLevel, Filter parentFilter,
			List<Pair<FilterAttribute, AttributeContainer>> filterAttrContainers) {
		Filter newFilter = null;

		if (isEnvironment(nodeLevel.getEntityType())) {
			// only 1 environment per context -> no parent filter needed
			newFilter = Filter.and();
		} else if (parentFilter != null) {
			newFilter = parentFilter.copy();

			for (Pair<FilterAttribute, AttributeContainer> entry : filterAttrContainers) {
				FilterAttribute filterAttribute = entry.getKey();
				AttributeContainer attributeContainer = entry.getValue();

				if (attributeContainer.isValid()) {
					if (nodeLevel.getValuePrecision() == ValuePrecision.EXACT) {
						Condition newCondition = filterAttribute.getCondition(elService,
								filterAttrContainers.stream().map(p -> p.getValue()).collect(
										Collectors.collectingAndThen(Collectors.toList(), AttributeContainerList::of)));

						addConditionIfNotPresent(newFilter, newCondition);
					} else {
						newFilter.add(nodeLevel.getValuePrecision().getCondition(contextState,
								filterAttribute.getAttribute(), attributeContainer.getValue(), zoneId));
					}
				} else {
					newFilter
							.add(ComparisonOperator.IS_NULL.create(contextState, filterAttribute.getAttribute(), null));
				}
			}
		}
		return newFilter;
	}

	private Filter addConditionIfNotPresent(Filter filter, Condition condition) {
		if (!filter.stream().filter(FilterItem::isCondition).anyMatch(f -> f.getCondition().equals(condition))) {
			filter.add(condition);
		}

		return filter;
	}

	/**
	 * Returns the SearchService
	 * 
	 * @param context ApplicationContext
	 * @return the SearchService
	 * @throws ServiceNotProvidedException if no SearchService is available from the
	 *                                     context.
	 */
	private SearchService getSearchService(ApplicationContext context) {
		return context.getSearchService().orElseThrow(() -> new ServiceNotProvidedException(SearchService.class));
	}

	/**
	 * Checks if entity type is an Environment
	 * 
	 * @param entityType entityType to check
	 * @return true, if entity type is an Environment
	 */
	private boolean isEnvironment(EntityType entityType) {
		return Environment.class.getSimpleName().equals(entityType.getName());
	}

	private List<Attribute> getFilterAttributes(NodeLevel nodeLevel) {
		if (nodeLevel.getFilterAttributes() == null) {
			return Collections.emptyList();
		}
		return nodeLevel.getFilterAttributes().stream().map(FilterAttribute::getAttribute).collect(Collectors.toList());
	}

	private Attribute getLabelAttribute(NodeLevel nodeLevel) {
		return nodeLevel.getLabelAttributes().stream().findFirst()
				.orElseThrow(() -> new IllegalStateException("woops?!"));
	}

	private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

	/**
	 * Compares {@link Node}s be {@link Node#getLabel()}.
	 */
	public static final Comparator<Node> compareByLabel = Comparator.comparing(Node::getLabel);

}
