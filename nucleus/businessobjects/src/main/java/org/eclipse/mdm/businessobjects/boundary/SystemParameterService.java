/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.SystemParameter;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.control.SearchActivity;
import org.eclipse.mdm.connector.boundary.ConnectorService;

/**
 * SystemParameterService Bean implementation with available {@link SystemParameter}
 * operations
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Stateless
public class SystemParameterService {

	@Inject
	private ConnectorService connectorService;

	@EJB
	private SearchActivity searchActivity;

	/**
	 * returns the matching {@link SystemParameters}s using the given filter or all
	 * {@link SystemParameters}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link SystemParameters} result
	 * @return the found {@link SystemParameters}s
	 */
	public List<SystemParameter> getParameters(String sourceName, String filter) {

		try {
			ApplicationContext context = this.connectorService.getContextByName(sourceName);
			EntityManager em = context.getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			if (filter == null || filter.trim().length() <= 0) {
				return em.loadAll(SystemParameter.class);
			}

//			if (ServiceUtils.isParentFilter(context, filter, ParameterSet.class)) {
//				String id = ServiceUtils.extactIdFromParentFilter(context, filter, ParameterSet.class);
//				return this.navigationActivity.getParameters(sourceName, id);
//			}
			return this.searchActivity.search(context, SystemParameter.class, filter);
//			throw new MDMEntityAccessException("Only ParameterSet Root filter supported!");
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}
}
