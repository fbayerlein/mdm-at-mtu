/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import org.eclipse.mdm.api.base.adapter.Attribute;

import com.google.common.base.MoreObjects;

/**
 * Represents an {@link Attribute} and its ordering.
 *
 */
public final class SortAttribute {
	private Attribute attribute;
	private Order order;

	/**
	 * Constructs an OrderAttribute with given attribute and ascending ordering
	 * 
	 * @param attribute
	 */
	public SortAttribute(Attribute attribute) {
		this(attribute, Order.ASCENDING);
	}

	/**
	 * Constructs an OrderAttribute with given attribute and ordering
	 * 
	 * @param attribute
	 * @param ascending
	 */
	public SortAttribute(Attribute attribute, Order ascending) {
		this.attribute = attribute;
		this.order = ascending;
	}

	/**
	 * @return the attribute
	 */
	public Attribute getAttribute() {
		return attribute;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return order;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(SortAttribute.class).add("attribute", attribute.getName()).add("order", order)
				.toString();
	}
}