/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.massdata.ValuesFilter;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarBaseVisitor;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarLexer;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.AndExpressionContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.ComparatorExpressionContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.ListComparatorExpressionContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.NotExpressionContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.OrExpressionContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.PairComparatorExpressionContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.UnaryComparatorExpressionContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.ValueContext;
import org.eclipse.mdm.businessobjects.filter.ValuesFilterGrammarParser.ValuesContext;

import com.google.common.base.Strings;

/**
 * Class for parsing value filter strings.
 * 
 * @author Matthias Koller
 *
 */
public class ValuesFilterParser {

	private ValuesFilterParser() {
	}

	/**
	 * Visitor class to convert the parsed tree into a {@link Filter}.
	 */
	private static final class ValuesFilterVisitor extends ValuesFilterGrammarBaseVisitor<ValuesFilter> {

		/**
		 * Constructs a new Visitor operating on the given search attributes.
		 * 
		 */
		private ValuesFilterVisitor() {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.mdm.businessobjects.filter.FilterGrammarBaseVisitor#
		 * visitAndExpression(org.eclipse.mdm.businessobjects.filter.FilterGrammarParser
		 * .AndExpressionContext)
		 */
		@Override
		public ValuesFilter visitAndExpression(AndExpressionContext ctx) {
			return ValuesFilter.ValuesConjunction.and(visit(ctx.left), visit(ctx.right));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.mdm.businessobjects.filter.FilterGrammarBaseVisitor#
		 * visitOrExpression(org.eclipse.mdm.businessobjects.filter.FilterGrammarParser.
		 * OrExpressionContext)
		 */
		@Override
		public ValuesFilter visitOrExpression(OrExpressionContext ctx) {
			return ValuesFilter.ValuesConjunction.or(visit(ctx.left), visit(ctx.right));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.mdm.businessobjects.filter.FilterGrammarBaseVisitor#
		 * visitNotExpression(org.eclipse.mdm.businessobjects.filter.FilterGrammarParser
		 * .NotExpressionContext)
		 */
		@Override
		public ValuesFilter visitNotExpression(NotExpressionContext ctx) {
			return ValuesFilter.ValuesConjunction.not(super.visitNotExpression(ctx));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.mdm.businessobjects.filter.FilterGrammarBaseVisitor#
		 * visitComparatorExpression(org.eclipse.mdm.businessobjects.filter.
		 * FilterGrammarParser.ComparatorExpressionContext)
		 */
		@Override
		public ValuesFilter visitComparatorExpression(ComparatorExpressionContext ctx) {
			String channelName = getChannelName(ctx.left.getText());
			return new ValuesFilter.ValuesCondition(channelName, getOperator(ctx.op), getValue(ctx.right));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.mdm.businessobjects.filter.FilterGrammarBaseVisitor#
		 * visitListComparatorExpression(org.eclipse.mdm.businessobjects.filter.
		 * FilterGrammarParser.ListComparatorExpressionContext)
		 */
		@Override
		public ValuesFilter visitListComparatorExpression(ListComparatorExpressionContext ctx) {
			return new ValuesFilter.ValuesCondition(ctx.left.getText(), getOperator(ctx.op), getValues(ctx.right));
		}

		@Override
		public ValuesFilter visitPairComparatorExpression(PairComparatorExpressionContext ctx) {
			return new ValuesFilter.ValuesCondition(ctx.left.getText(), getOperator(ctx.op),
					Arrays.asList(ctx.right1.getText(), ctx.right2.getText()));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.mdm.businessobjects.filter.FilterGrammarBaseVisitor#
		 * visitUnaryComparatorExpression(org.eclipse.mdm.businessobjects.filter.
		 * FilterGrammarParser.UnaryComparatorExpressionContext)
		 */
		@Override
		public ValuesFilter visitUnaryComparatorExpression(UnaryComparatorExpressionContext ctx) {
			return new ValuesFilter.ValuesCondition(ctx.left.getText(), getOperator(ctx.op), Collections.emptyList());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.antlr.v4.runtime.tree.AbstractParseTreeVisitor#aggregateResult(java.lang.
		 * Object, java.lang.Object)
		 */
		@Override
		protected ValuesFilter aggregateResult(ValuesFilter aggregate, ValuesFilter nextResult) {
			if (nextResult == null) {
				return aggregate;
			}
			return super.aggregateResult(aggregate, nextResult);
		}

		private String getChannelName(String text) {
			// Remove start and ending quotes, if any
			if (text.startsWith("\"") && text.endsWith("\"")) {
				return unescape(text.substring(1, text.length() - 1));
			}
			return unescape(text);
		}

		private String unescape(String text) {
			return text.replace("\\\"", "\"").replace("\\\\", "\\");
		}

		/**
		 * Extract a string from the given {@link ValueContext}. Basically this methods
		 * returns a string representation of the value without enclosing quotes (if
		 * there were any).
		 * 
		 * @param ctx {@link ValueContext} containing the parsed value.
		 * @return string representation of the value given by {@link ValueContext}
		 */
		private String getValue(ValueContext ctx) {

			TerminalNode typeNode = (TerminalNode) ctx.getChild(0);
			switch (typeNode.getSymbol().getType()) {
			case ValuesFilterGrammarLexer.STRINGLITERAL:
				String str = ctx.STRINGLITERAL().getText();
				if (!str.isEmpty() && str.charAt(0) == '\'') {
					// replace leading and trailing ' and unescape '
					return ctx.STRINGLITERAL().getText().replaceAll("(\\A')|('\\z)", "").replaceAll("\\\\'", "'");
				} else if (!str.isEmpty() && str.charAt(0) == '\"') {
					// replace leading and trailing " and unescape "
					return ctx.STRINGLITERAL().getText().replaceAll("(\\A\")|(\"\\z)", "").replaceAll("\\\\\\\"", "\"");
				} else {
					return str;
				}
			case ValuesFilterGrammarLexer.DECIMAL:
			case ValuesFilterGrammarLexer.LONG:
			case ValuesFilterGrammarLexer.BOOL:
				return ctx.getText();
			default:
				throw new RuntimeException("Unsupported Symbol: " + typeNode.getSymbol().getType());
			}
		}

		/**
		 * Extract a list string from the given {@link ValuesContext}. Basically this
		 * methods returns a list of string representations of the values without
		 * enclosing quotes (if there were any).
		 * 
		 * @param ctx {@link ValuesContext} containing the parsed values.
		 * @return string representations of the values given by {@link ValuesContext}
		 */
		private List<String> getValues(ValuesContext ctx) {
			List<String> values = new ArrayList<>();
			for (org.antlr.v4.runtime.tree.ParseTree child : ctx.children) {
				if (child instanceof ValueContext) {
					values.add(getValue((ValueContext) child));
				}
			}
			return values;
		}

		/**
		 * Converts a {@link ParserRuleContext} containing a {@link TerminalNode} into a
		 * {@link ComparisonOperator}.
		 * 
		 * @param ctx {@link UnaryComparatorExpressionContext},
		 *            {@link ComparatorExpressionContext} or
		 *            {@link ListComparatorExpressionContext} or
		 * @return converted {@link ComparisonOperator}
		 * @throws IllegalArgumentException if the operator given by <code>ctx</code> is
		 *                                  unknown or cannot be converted.
		 */
		private ComparisonOperator getOperator(ParserRuleContext ctx) {
			TerminalNode typeNode = (TerminalNode) ctx.getChild(0);
			switch (typeNode.getSymbol().getType()) {
			case ValuesFilterGrammarLexer.EQUAL:
				return ComparisonOperator.EQUAL;
			case ValuesFilterGrammarLexer.NOT_EQUAL:
				return ComparisonOperator.NOT_EQUAL;
			case ValuesFilterGrammarLexer.LESS_THAN:
				return ComparisonOperator.LESS_THAN;
			case ValuesFilterGrammarLexer.LESS_THAN_OR_EQUAL:
				return ComparisonOperator.LESS_THAN_OR_EQUAL;
			case ValuesFilterGrammarLexer.GREATER_THAN:
				return ComparisonOperator.GREATER_THAN;
			case ValuesFilterGrammarLexer.GREATER_THAN_OR_EQUAL:
				return ComparisonOperator.GREATER_THAN_OR_EQUAL;
			case ValuesFilterGrammarLexer.IN_SET:
				return ComparisonOperator.IN_SET;
			case ValuesFilterGrammarLexer.NOT_IN_SET:
				return ComparisonOperator.NOT_IN_SET;
			case ValuesFilterGrammarLexer.LIKE:
				return ComparisonOperator.LIKE;
			case ValuesFilterGrammarLexer.NOT_LIKE:
				return ComparisonOperator.NOT_LIKE;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_EQUAL:
				return ComparisonOperator.CASE_INSENSITIVE_EQUAL;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_NOT_EQUAL:
				return ComparisonOperator.CASE_INSENSITIVE_NOT_EQUAL;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_LESS_THAN:
				return ComparisonOperator.CASE_INSENSITIVE_LESS_THAN;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
				return ComparisonOperator.CASE_INSENSITIVE_LESS_THAN_OR_EQUAL;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_GREATER_THAN:
				return ComparisonOperator.CASE_INSENSITIVE_GREATER_THAN;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
				return ComparisonOperator.CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_IN_SET:
				return ComparisonOperator.CASE_INSENSITIVE_IN_SET;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_NOT_IN_SET:
				return ComparisonOperator.CASE_INSENSITIVE_NOT_IN_SET;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_LIKE:
				return ComparisonOperator.CASE_INSENSITIVE_LIKE;
			case ValuesFilterGrammarLexer.CASE_INSENSITIVE_NOT_LIKE:
				return ComparisonOperator.CASE_INSENSITIVE_NOT_LIKE;
			case ValuesFilterGrammarLexer.IS_NULL:
				return ComparisonOperator.IS_NULL;
			case ValuesFilterGrammarLexer.IS_NOT_NULL:
				return ComparisonOperator.IS_NOT_NULL;
			case ValuesFilterGrammarLexer.BETWEEN:
				return ComparisonOperator.BETWEEN;
			default:
				throw new IllegalArgumentException(
						"Operator " + typeNode.getSymbol().getType() + " not supported yet!");
			}
		}
	}

	/**
	 * Class to convert a antlr syntax error into a unchecked
	 * ParserCancellationException.
	 */
	private static class ThrowingErrorListener extends BaseErrorListener {

		public static final ThrowingErrorListener INSTANCE = new ThrowingErrorListener();

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.antlr.v4.runtime.BaseErrorListener#syntaxError(org.antlr.v4.runtime.
		 * Recognizer, java.lang.Object, int, int, java.lang.String,
		 * org.antlr.v4.runtime.RecognitionException)
		 */
		@Override
		public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
				String msg, RecognitionException e) throws ParseCancellationException {
			throw new ParseCancellationException("line " + line + ":" + charPositionInLine + " " + msg);
		}
	}

	/**
	 * Parses the given filter string. The filter string must conform to the ANTLR
	 * grammer defined in FilterGrammar.g4.
	 * 
	 * @param possibleEntityTypes The possible {@link EntityType}s /
	 *                            {@link Attribute}s
	 * @param filterString        The filter string to parse.
	 * @return the parsed {@link Filter}
	 * @throws IllegalArgumentExceptionThrown if parsing fails.
	 */
	public static ValuesFilter parseFilterString(String filterString) throws IllegalArgumentException {

		if (Strings.isNullOrEmpty(filterString)) {
			return null;
		}

		try {
			ValuesFilterGrammarLexer lexer = new ValuesFilterGrammarLexer(new ANTLRInputStream(filterString));
			lexer.removeErrorListeners();
			lexer.addErrorListener(ThrowingErrorListener.INSTANCE);

			ValuesFilterGrammarParser parser = new ValuesFilterGrammarParser(new CommonTokenStream(lexer));
			parser.removeErrorListeners();
			parser.addErrorListener(ThrowingErrorListener.INSTANCE);

			return new ValuesFilterVisitor().visit(parser.parse());
		} catch (ParseCancellationException e) {
			throw new IllegalArgumentException(
					"Could not parse filter string '" + filterString + "'. Error: " + e.getMessage(), e);
		}
	}
}
