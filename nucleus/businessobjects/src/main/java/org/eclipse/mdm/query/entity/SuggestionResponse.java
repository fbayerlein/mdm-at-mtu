/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.base.MoreObjects;

/**
 * 
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SuggestionResponse {

	/** transferable data content */
	private List<String> data;

	/**
	 * Constructor
	 * 
	 * @param searchDefinitions list of {@link Suggestion}s to transfer
	 */
	public SuggestionResponse(List<String> suggestions) {
		this.data = new ArrayList<>(suggestions);
	}

	public SuggestionResponse() {
		this.data = new ArrayList<>();
	}

	public List<String> getData() {
		return Collections.unmodifiableList(this.data);
	}

	@Override
	public int hashCode() {
		return Objects.hash(data);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}
		final SuggestionResponse other = (SuggestionResponse) obj;
		return Objects.equals(this.data, other.data);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(SuggestionResponse.class).add("data", data).toString();
	}
}
