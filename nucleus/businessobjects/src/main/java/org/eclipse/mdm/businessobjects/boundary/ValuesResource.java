/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.mdm.businessobjects.entity.FlagMode;
import org.eclipse.mdm.protobuf.Mdm;

import com.google.common.base.Strings;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Values")
@Consumes({ MEDIATYPE_APPLICATION_PROTOBUF, MediaType.APPLICATION_JSON })
@Produces({ MEDIATYPE_APPLICATION_PROTOBUF, MediaType.APPLICATION_JSON })
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/values")
public class ValuesResource {

	@EJB
	private ValuesService valuesService;

	@POST
	@Path("data-read")
	@Operation(summary = "Read channel value data", description = "Read channel value data. The channels to read can be specified by the given filter. The returned channel values can be reduced by specifying a valuesFilter. Be aware that the channels are filtered in openMDM and the complete channel data has to be loaded from the downstream adapter. So this method is not suitable to reduce a large amount of measurement data.", responses = {
			@ApiResponse(description = "A list with ChannelValues. Each ChannelValues represents the values of a Channel.", content = @Content(schema = @Schema(implementation = Mdm.ChannelValuesList.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response readRaw(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@RequestBody(description = "ReadChannelValuesRequest specifying the Channels to load and the portion of the Channel's data to read.", required = true, content = @Content(schema = @Schema(implementation = Mdm.ReadChannelValuesRequest.class))) Mdm.ReadChannelValuesRequest protoReadRequest,
			@Parameter(description = "'boolean' (default) for boolean flags or 'full' for full ODS flags.") @QueryParam("flags") String flags) {
		FlagMode flagMode = null;
		if (Strings.isNullOrEmpty(flags)) {
			flagMode = FlagMode.BOOLEAN;
		} else {
			flagMode = FlagMode.parse(flags);
		}
		return Response.ok(valuesService.loadChannelValues(sourceName, protoReadRequest, flagMode)).build();
	}

	@POST
	@Path("read")
	@Operation(summary = "Read measurement data", description = "Read measurement specified by a ReadRequest.", responses = {
			@ApiResponse(description = "A list with MeasuredValues. Each MeasuredValues represents the values of a Channel.", content = @Content(schema = @Schema(implementation = Mdm.MeasuredValuesList.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response read(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@RequestBody(description = "ReadRequest specifying the Channels and the portion of the Channel's data to read.", required = true, content = @Content(schema = @Schema(implementation = Mdm.ReadRequest.class))) Mdm.ReadRequest protoReadRequest,
			@Parameter(description = "'boolean' (default) for boolean flags or 'full' for full ODS flags.") @QueryParam("flags") String flags) {
		FlagMode flagMode = null;
		if (Strings.isNullOrEmpty(flags)) {
			flagMode = FlagMode.BOOLEAN;
		} else {
			flagMode = FlagMode.parse(flags);
		}
		return Response.ok(valuesService.load(sourceName, protoReadRequest, flagMode)).build();
	}

	@POST
	@Path("write")
	@Operation(summary = "Write measurement data", description = "Write measurement data for the specified ChannelGroup and Channels. Both ChannelGroup and Channels must exist.", responses = {
			@ApiResponse(description = "An empty response"),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response write(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@RequestBody(description = "WriteRequestList specifying the Channels and measurement data to write.", required = true, content = @Content(schema = @Schema(implementation = Mdm.WriteRequestList.class))) Mdm.WriteRequestList protoWriteRequestList) {

		valuesService.write(sourceName, protoWriteRequestList);

		return Response.noContent().build();
	}

	@POST
	@Path("preview")
	@Operation(summary = "Read preview data", description = "Read preview data for the specified ReadRequest. The number of chunks defines the number of returned values per channel. The original measured values are chunked and for each chunk the average, minimum and maximum values are calculated.", responses = {
			@ApiResponse(description = "Average, minimum and maximum values for each chunk are returned as MeasuredValues. Each MeasuredValues represents the preview of a Channel where the lenght of MeasuredValues equals numberOfChunks.", content = @Content(schema = @Schema(implementation = Mdm.PreviewValuesList.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response preview(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@RequestBody(description = "PreviewRequest specifying the Channels and number of chunks to read.", required = true, content = @Content(schema = @Schema(implementation = Mdm.PreviewRequest.class))) Mdm.PreviewRequest previewRequest) {

		return Response.ok(valuesService.preview(sourceName, previewRequest)).build();
	}

	@POST
	@Path("append")
	@Operation(summary = "Write measurement data", description = "Write measurement data for the specified ChannelGroup and Channels. Both ChannelGroup and Channels must exist.", responses = {
			@ApiResponse(description = "An empty response"),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response append(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@RequestBody(description = "WriteRequestList specifying the Channels and measurement data to write.", required = true, content = @Content(schema = @Schema(implementation = Mdm.WriteRequestList.class))) Mdm.WriteRequestList protoWriteRequestList) {

		valuesService.append(sourceName, protoWriteRequestList);

		return Response.noContent().build();
	}

	@POST
	@Path("update")
	@Operation(summary = "Update measurement data", description = "Update measurement data (axisType:0, independent:false, globalFlag:false and flags:[]) with default values for the specified ChannelGroup and Channels. Both ChannelGroup and Channel must exist.", responses = {
			@ApiResponse(description = "An empty response"),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@RequestBody(description = "UpdateRequest specifying the Channel and measurement data to write.", required = true, content = @Content(schema = @Schema(implementation = Mdm.UpdateRequest.class))) Mdm.UpdateRequest protoUpdateRequest) {

		valuesService.update(sourceName, protoUpdateRequest);

		return Response.noContent().build();
	}
}
