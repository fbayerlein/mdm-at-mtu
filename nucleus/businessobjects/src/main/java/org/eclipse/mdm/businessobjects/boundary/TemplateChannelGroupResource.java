/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.dflt.model.TemplateChannelGroup;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurement;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurementQuantity;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.Value;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.collection.Vector;
import io.vavr.control.Try;

/**
 * {@link TemplateChannelGroup} resource
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Tag(name = "Template")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/tplchannelgroup")
public class TemplateChannelGroupResource {

	@EJB
	private EntityService entityService;

	@EJB
	private TemplateMeasurementQuantityService tplMeasurementQuantityService;

	/**
	 * Returns the found {@link TemplateChannelGroup}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateChannelGroup}
	 * @return the found {@link TemplateChannelGroup} as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response find(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName, @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), TemplateChannelGroup.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the found {@link TemplateChannelGroup}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateChannelGroup}
	 * @return the found {@link TemplateChannelGroup} as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@QueryParam("filter") String filter) {
		return entityService.findAll(V(sourceName), TemplateChannelGroup.class, filter)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link TemplateMeasurementQuantity}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateMeasurementQuantity}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get TplMeasurementQuantity of a TemplateChannelGroup by ID", description = "Returns TplMeasurementQuantity based on ID of TemplateChannelGroup", responses = {
			@ApiResponse(description = "The TplMeasurementQuantity", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/tplmeaquantity")
	public Response getTplMeaQauntities(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateChannelGroup", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return Try
				.of(() -> tplMeasurementQuantityService.getTemplateMeasurementQuantities(sourceName, id,
						TemplateChannelGroup.class))
				.map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, TemplateMeasurementQuantity.class)).get();
	}

	/**
	 * Returns the created {@link TemplateChannelGroup}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link TemplateChannelGroup} to create.
	 * @return the created {@link TemplateChannelGroup} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new TemplateChannelGroup", responses = {
			@ApiResponse(description = "The created TemplateChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {

		Seq<Value<?>> extractRequestBody = entityService.extractRequestBody(body, sourceName,
				io.vavr.collection.List.of(TemplateMeasurement.class));

		java.util.List<Value<?>> asJavaMutable = extractRequestBody.asJavaMutable();
		Seq<Value<?>> finalAttrSeq = Vector.empty();

		for (Value<?> v : asJavaMutable) {
			finalAttrSeq = finalAttrSeq.append(v);
		}
		return entityService.create(V(sourceName), TemplateChannelGroup.class, finalAttrSeq)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link TemplateChannelGroup} with all parameters set in the given
	 * JSON body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TemplateChannelGroup} to
	 *                   update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link TemplateChannelGroup}
	 */
	@PUT
	@Operation(summary = "Update an existing TemplateChannelGroup", description = "Updates the TemplateChannelGroup with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated TemplateChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateChannelGroup", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), TemplateChannelGroup.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the {@link TemplateChannelGroup} with all parameters set in the given
	 * JSON body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TemplateChannelGroup} to
	 *                   update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link TemplateChannelGroup}
	 */
	@PUT
	@Operation(summary = "Update an existing TemplateChannelGroup", description = "Updates the relation of TemplateChannelGroup to TemplateMeasurementQuantity.", responses = {
			@ApiResponse(description = "The updated TemplateChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/tplmeaquantity")
	public Response updateMeaQuantity(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateChannelGroup", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return Try
				.of(() -> tplMeasurementQuantityService.addTplMeaQuantity(sourceName,
						requestBody.getStringValueSupplier("TemplateMeasurementQuantity").get(),
						entityService.find(V(sourceName), TemplateChannelGroup.class, V(id)).get()))
				.map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, TemplateMeasurementQuantity.class)).get();
	}

	/**
	 * Deletes and returns the deleted {@link TemplateChannelGroup}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link TemplateChannelGroup} to
	 *                   delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing TemplateChannelGroup", responses = {
			@ApiResponse(description = "The deleted TemplateChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	@RolesAllowed({ "Admin", "DescriptiveDataAuthor" })
	public Response delete(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateChannelGroup", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), TemplateChannelGroup.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}
