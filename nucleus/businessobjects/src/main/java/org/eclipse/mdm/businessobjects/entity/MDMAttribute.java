/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import com.google.common.base.MoreObjects;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Attribute (Entity for attribute informations)
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Schema(description = "Representation of a MDM attribute")
public class MDMAttribute {

	/** name of the attribute value */
	private String name;
	/** value of the attribute value */
	private Object value;
	/** unit of the attribute value */
	private String unit;
	/** data type of the attribute value */
	private String dataType;

	/**
	 * name of the enumeration of the attribute value, if dataType is an enumeration
	 */
	private String enumName;

	/**
	 * Default Constructor
	 */
	public MDMAttribute() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param name     name of the attribute value
	 * @param value    string value of the attribute value
	 * @param unit     unit of the attribute value
	 * @param dataType data type of the attribute value
	 */
	public MDMAttribute(String name, Object value, String unit, String dataType) {
		this(name, value, unit, dataType, null);
	}

	/**
	 * Constructor
	 * 
	 * @param name     name of the attribute value
	 * @param value    string value of the attribute value
	 * @param unit     unit of the attribute value
	 * @param dataType data type of the attribute value
	 * @param enumName name of the enumeration of the attribute, if its datatype is
	 *                 an enumeration
	 */
	public MDMAttribute(String name, Object value, String unit, String dataType, String enumName) {
		this.name = name;
		this.value = value;
		this.unit = unit;
		this.dataType = dataType;
		this.enumName = enumName;
	}

	/**
	 * Copy constructor
	 * 
	 * @param attribute attribute to copy
	 */
	public MDMAttribute(MDMAttribute attribute) {
		this.name = attribute.getName();
		this.value = attribute.getValue();
		this.unit = attribute.getUnit();
		this.dataType = attribute.getDataType();
		this.enumName = attribute.getEnumName();
	}

	@Schema(description = "name of the attribute")
	public String getName() {
		return this.name;
	}

	@Schema(description = "value of the attribute")
	public Object getValue() {
		return this.value;
	}

	@Schema(description = "unit of the attribute value")
	public String getUnit() {
		return this.unit;
	}

	@Schema(description = "data of the attribute", example = "FLOAT_SEQUENCE")
	public String getDataType() {
		return this.dataType;
	}

	@Schema(description = "name of the enum of the attribute")
	public String getEnumName() {
		return enumName;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("name", name).add("value", value).add("unit", unit)
				.add("dataType", dataType).add("enumName", enumName).toString();
	}
}
