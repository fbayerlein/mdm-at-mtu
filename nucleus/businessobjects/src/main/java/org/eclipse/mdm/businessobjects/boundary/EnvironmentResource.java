/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.businessobjects.entity.I18NResponse;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.control.Try;

/**
 * {@link Environment} resource
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Tag(name = "Environment")
@Path("/environments")
public class EnvironmentResource {

	@EJB
	private EnvironmentService environmentService;

	@EJB
	private EntityService entityService;

	/**
	 * delegates the request to the {@link EnvironmentService}
	 *
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the Environments", description = "Returns a list of the Environments", responses = {
			@ApiResponse(description = "The environments", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEnvironments() {
		List<Environment> environments = this.environmentService.getEnvironments();
		return ServiceUtils.toResponse(new MDMEntityResponse(Environment.class, environments), Status.OK);

	}

	/**
	 * delegates the request to the {@link EnvironmentService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get an Environment by name", description = "Returns Environment ", responses = {
			@ApiResponse(description = "The project", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_SOURCENAME + "}")
	public Response getEnvironment(
			@Parameter(description = "Name of the MDM environment", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		Environment environment = this.environmentService.getEnvironment(sourceName);
		return ServiceUtils.toResponse(new MDMEntityResponse(Environment.class, environment), Status.OK);

	}

	/**
	 * Updates the {@link Environment} with all parameters set in the given JSON
	 * body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link Environment}
	 */
	@PUT
	@Operation(summary = "Update an existing Environment", description = "Updates the Environment with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The Environment", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_SOURCENAME + "}")
	public Response update(
			@Parameter(description = "Name of the MDM environment", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), Try.of(() -> this.environmentService.getEnvironment(sourceName)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link EnvironmentService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the Environment localizations", description = "Returns Environment localizations", responses = {
			@ApiResponse(description = "The Environment localizations", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_SOURCENAME + "}/localizations")
	@Deprecated
	public Response localize(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "Get all localized attributes?", required = true) @QueryParam("all") boolean all) {

		if (all) {
			Map<Attribute, String> localizedAttributeMap = this.environmentService.localizeAllAttributes(sourceName);
			Map<EntityType, String> localizedEntityTypeMap = this.environmentService.localizeAllTypes(sourceName);
			return ServiceUtils.toResponse(new I18NResponse(localizedEntityTypeMap, localizedAttributeMap), Status.OK);
		}

		Map<Attribute, String> localizedAttributeMap = this.environmentService.localizeAttributes(sourceName);
		Map<EntityType, String> localizedEntityTypeMap = this.environmentService.localizeType(sourceName);
		return ServiceUtils.toResponse(new I18NResponse(localizedEntityTypeMap, localizedAttributeMap), Status.OK);

	}

	@GET
	@Operation(summary = "Execute a search query", description = "Returns a list of entities matching the search query.", responses = {
			@ApiResponse(description = "The Environment localizations", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_SOURCENAME + "}/search")
	public Response search(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "Query string", required = true) @QueryParam("q") String query) {
		List<Entity> searchResults = environmentService.search(sourceName, query);
		return ServiceUtils.toResponse(new MDMEntityResponse(Environment.class, searchResults), Status.OK);
	}

}
