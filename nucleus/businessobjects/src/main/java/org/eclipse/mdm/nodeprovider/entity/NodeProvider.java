/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import java.util.List;

import org.eclipse.mdm.protobuf.Mdm.Node;

/**
 * NodeProvider is responsible for providing an hierarchical view on the
 * instances in openMDM.
 *
 */
public interface NodeProvider {

	/**
	 * Returns the NodeProviders human readable name.
	 * 
	 * @return name of the NodeProvider
	 */
	String getName();

	/**
	 * Returns the list of root nodes of the NodeProvider.
	 * 
	 * @return list of root nodes
	 */
	List<Node> getRoots();

	/**
	 * Returns the child nodes of the given {@link Node}.
	 * 
	 * @param parent Parent node to retrieve its children from
	 * @return list of child nodes
	 */
	List<Node> getChildren(Node parent);

	/**
	 * Returns the list of ancestors for the given node. So the last node in the
	 * list is the given nodes parent and so on until a root node is reached, which
	 * will be the first node in the list.
	 * 
	 * @param node
	 * @return a list of ancestors o
	 */
	List<Node> getTreePath(Node node);
}
