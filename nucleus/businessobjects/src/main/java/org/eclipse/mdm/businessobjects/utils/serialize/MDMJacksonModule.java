/*******************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.mdm.businessobjects.utils.serialize;

import java.time.Instant;

import org.eclipse.mdm.api.base.model.DoubleComplex;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.FloatComplex;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class MDMJacksonModule extends SimpleModule {

	private static final long serialVersionUID = -9194980172466418819L;

	public MDMJacksonModule() {
		addSerializer(Instant.class, new InstantSerializer());
		addSerializer(FileLink.class, new FileLinkSerializer());
		addSerializer(EnumerationValue.class, new EnumerationValueSerializer());
		addSerializer(FloatComplex.class, new FloatComplexSerializer());
		addSerializer(DoubleComplex.class, new DoubleComplexSerializer());
	}
}
