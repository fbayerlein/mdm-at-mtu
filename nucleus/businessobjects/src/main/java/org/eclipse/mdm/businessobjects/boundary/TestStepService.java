/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextSensor;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.businessobjects.control.ContextActivity;
import org.eclipse.mdm.businessobjects.control.I18NActivity;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.control.NavigationActivity;
import org.eclipse.mdm.businessobjects.control.SearchActivity;
import org.eclipse.mdm.businessobjects.entity.SearchAttribute;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import com.google.common.collect.Lists;

/**
 * TestStepService Bean implementation with available {@link TestStep}
 * operations
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Stateless
public class TestStepService {

	@Inject
	private ConnectorService connectorService;
	@EJB
	private I18NActivity i18nActivity;
	@EJB
	private NavigationActivity navigationActivity;
	@EJB
	private ContextActivity contextActivity;
	@EJB
	private SearchActivity searchActivity;

	/**
	 * returns the matching {@link TestStep}s using the given filter or all
	 * {@link TestStep}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the TestStep result
	 * @return the found {@link TestStep}s
	 */
	public List<TestStep> getTestSteps(String sourceName, String filter) {
		try {

			ApplicationContext context = this.connectorService.getContextByName(sourceName);
			EntityManager em = context.getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			if (filter == null || filter.trim().length() <= 0) {
				return em.loadAll(TestStep.class);
			}

			if (ServiceUtils.isParentFilter(context, filter, TestStep.PARENT_TYPE_TEST)) {
				String id = ServiceUtils.extactIdFromParentFilter(context, filter, TestStep.PARENT_TYPE_TEST);
				return this.navigationActivity.getTestSteps(sourceName, id);
			}

			return this.searchActivity.search(context, TestStep.class, filter);

		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Returns the {@link SearchAttribute} for the entity type TestStep in the given
	 * data source.
	 * 
	 * @param sourceName The name of the data source.
	 * @return the found {@link SearchAttribute}s
	 */
	public List<SearchAttribute> getSearchAttributes(String sourceName) {
		return this.searchActivity.listAvailableAttributes(this.connectorService.getContextByName(sourceName),
				TestStep.class);
	}

	/**
	 * returns a {@link TestStep} identified by the given id.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link TestStep}
	 * @return the matching {@link TestStep}
	 */
	public TestStep getTestStep(String sourceName, String testStepId) {
		try {
			return this.connectorService.getContextByName(sourceName).getEntityManager()
					.map(em -> em.load(TestStep.class, testStepId))
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * returns the complete context data (ordered and measured) for a
	 * {@link TestStep}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link TestStep}
	 * @return a map with the complete context data (ordered and measured)
	 */
	public Map<String, Map<ContextType, ContextRoot>> getContext(String sourceName, String testStepId) {
		return this.contextActivity.getTestStepContext(sourceName, testStepId);
	}

	/**
	 * returns the UnitUnderTest context data (ordered and measured) for a
	 * {@link TestStep}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link TestStep}
	 * @return a map with the UnitUnderTest context data (ordered and measured)
	 */
	public Map<String, Map<ContextType, ContextRoot>> getContextUUT(String sourceName, String testStepId) {
		return this.contextActivity.getTestStepContext(sourceName, testStepId, ContextType.UNITUNDERTEST);
	}

	/**
	 * returns the TestSequence context data (ordered and measured) for a
	 * {@link TestStep}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link TestStep}
	 * @return a map with the TestSequence context data (ordered and measured)
	 */
	public Map<String, Map<ContextType, ContextRoot>> getContextTSQ(String sourceName, String testStepId) {
		return this.contextActivity.getTestStepContext(sourceName, testStepId, ContextType.TESTSEQUENCE);
	}

	/**
	 * returns the TestEquipment context data (ordered and measured) for a
	 * {@link TestStep}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link TestStep}
	 * @return a map with the TestEquipment context data (ordered and measured)
	 */
	public Map<String, Map<ContextType, ContextRoot>> getContextTEQ(String sourceName, String testStepId) {
		return this.contextActivity.getTestStepContext(sourceName, testStepId, ContextType.TESTEQUIPMENT);
	}

	/**
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param ctxRoot       RootContext
	 * @param componentName Name of the TemplateComponent
	 * @return
	 */
	public void addContextComponent(String sourceName, ContextRoot ctxRoot, String[] componentNames) {
		if (ctxRoot == null) {
			return;
		}
		EntityFactory ef = this.connectorService.getContextByName(sourceName).getEntityFactory()
				.orElseThrow(() -> new MDMEntityAccessException("Entity factory not present!"));
		List<ContextComponent> cc = new ArrayList<>();
		for (String componentName : componentNames) {
			cc.add(ef.createContextComponent(componentName.trim(), ctxRoot));
		}

		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		boolean classificationCreated = false;
		Transaction transaction = em.startTransaction();

		try {
			transaction.create(cc);
			transaction.commit();
			classificationCreated = true;
		} finally {
			if (!classificationCreated) {
				transaction.abort();
				throw new MDMEntityAccessException("Failed to create classification!");
			}
		}

	}

	public ContextComponent deleteContextComponent(String sourceName, ContextRoot ctxRoot, String componentName) {
		ContextComponent cc = null;
		Optional<ContextComponent> ctx = ctxRoot.getContextComponent(componentName);
		if (ctx.isPresent()) {
			cc = ctx.get();
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			boolean classificationDeleted = false;
			Transaction transaction = em.startTransaction();

			try {
				transaction.delete(Lists.newArrayList(cc));
				transaction.commit();
				classificationDeleted = true;
			} finally {
				if (!classificationDeleted) {
					transaction.abort();
					throw new MDMEntityAccessException("Failed to create classification!");
				}
			}
		}
		return cc;
	}

	/**
	 * returns all sensor context data of TestEquipment sensor configuration
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link TestStep}
	 * @return a map with the TestEquipment sensor context data (ordered and
	 *         measured)
	 */
	@Deprecated
	public Map<String, List<ContextSensor>> getSensors(String sourceName, String testStepId) {
		return this.contextActivity.getTestStepSensorContext(sourceName, testStepId);
	}

	/**
	 * returns localized {@link TestStep} attributes
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link TestStep} attributes
	 */
	@Deprecated
	public Map<Attribute, String> localizeAttributes(String sourceName) {
		return this.i18nActivity.localizeAttributes(sourceName, TestStep.class);
	}

	/**
	 * returns the localized {@link TestStep} type name
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link TestStep} type name
	 */
	public Map<EntityType, String> localizeType(String sourceName) {
		return this.i18nActivity.localizeType(sourceName, TestStep.class);
	}

}
