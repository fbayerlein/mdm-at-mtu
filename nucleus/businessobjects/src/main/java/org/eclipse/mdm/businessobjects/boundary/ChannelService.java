/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.businessobjects.boundary.ChannelResource.CreateChannel;
import org.eclipse.mdm.businessobjects.control.I18NActivity;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.control.NavigationActivity;
import org.eclipse.mdm.businessobjects.control.SearchActivity;
import org.eclipse.mdm.businessobjects.entity.SearchAttribute;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import com.google.common.base.Strings;

/**
 * ChannelService Bean implementation with available {@link Channel} operations
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Stateless
public class ChannelService {

	@Inject
	private ConnectorService connectorService;
	@EJB
	private I18NActivity i18nActivity;
	@EJB
	private NavigationActivity navigationActivity;
	@EJB
	private SearchActivity searchActivity;

	/**
	 * returns the matching {@link Channel}s using the given filter or all
	 * {@link Channel}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the Channel result
	 * @param limit      optional limit the results
	 * @param offset     optional offset where the results begin
	 * @return the found {@link Channel}s
	 */
	public List<Channel> getChannels(String sourceName, String filter, String limit, String offset) {
		List<Channel> results = null;
		try {
			ApplicationContext context = this.connectorService.getContextByName(sourceName);
			EntityManager em = context.getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			if (filter == null || filter.trim().length() <= 0) {
				return em.loadAll(Channel.class);
			}

			if (ServiceUtils.isParentFilter(context, filter, Channel.PARENT_TYPE_CHANNELGROUP)) {
				String id = ServiceUtils.extactIdFromParentFilter(context, filter, Channel.PARENT_TYPE_CHANNELGROUP);
				results = this.navigationActivity.getChannels(sourceName, id);
			} else {

				results = this.searchActivity.search(context, Channel.class, filter);
			}
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}

		if (results != null && limit != null) {
			int limitSize = Integer.parseInt(limit);
			int offsetValue = offset != null ? Integer.parseInt(offset) : 0;
			if (results.size() > limitSize && limitSize > 0) {
				List<Channel> sortedList = new ArrayList<>(results);
				sortedList.sort(Comparator.comparing(Entity::getName));
				int max = limitSize + offsetValue;
				if (max > sortedList.size()) {
					max = sortedList.size();
				}
				if (offsetValue < sortedList.size() && max <= sortedList.size()) {
					results = sortedList.subList(offsetValue, max);
				}
			}
		}

		return results;
	}

	/**
	 * returns a {@link Channel} identified by the given id.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param channelId  id of the {@link Channel}
	 * @return the matching {@link Channel}
	 */
	public Channel getChannel(String sourceName, String channelId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(Channel.class, channelId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Returns the {@link SearchAttribute} for the entity type Channel in the given
	 * data source.
	 *
	 * @param sourceName The name of the data source.
	 * @return the found {@link SearchAttribute}s
	 */
	public List<SearchAttribute> getSearchAttributes(String sourceName) {
		return this.searchActivity.listAvailableAttributes(this.connectorService.getContextByName(sourceName),
				Channel.class);
	}

	/**
	 * Creates multiple Channels from a list of {@link CreateChannel}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param channels
	 * @return list of created {@link Channel}
	 */
	public List<Channel> createChannels(String sourceName, List<CreateChannel> channels) {
		ApplicationContext context = connectorService.getContextByName(sourceName);
		EntityManager em = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		EntityFactory factory = context.getEntityFactory()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityFactory.class));

		List<String> measurementIds = channels.stream().map(cc -> cc.measurement).distinct()
				.collect(Collectors.toList());

		Map<String, List<Measurement>> measurements = em.load(Measurement.class, measurementIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<String> quantityIds = channels.stream().map(cc -> cc.quantity).distinct().collect(Collectors.toList());
		Map<String, List<Quantity>> quantities = em.load(Quantity.class, quantityIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<String> unitIds = channels.stream().map(cc -> cc.unit).filter(u -> !Strings.isNullOrEmpty(u)).distinct()
				.collect(Collectors.toList());
		Map<String, List<Unit>> units = em.load(Unit.class, unitIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<Channel> createdChannels = new ArrayList<>();
		for (CreateChannel cc : channels) {
			List<Measurement> measurement = measurements.get(cc.measurement);
			List<Quantity> quantity = quantities.get(cc.quantity);
			List<Unit> unit = units.get(cc.unit);

			if (measurement == null || measurement.isEmpty()) {
				throw new DataAccessException(
						"No Measurement with ID " + cc.measurement + " found in source " + sourceName);
			}
			if (quantity == null || quantity.isEmpty()) {
				throw new DataAccessException("No Quantity with ID " + cc.quantity + " found in source " + sourceName);
			}
			if (unit == null || unit.isEmpty()) {
				createdChannels.add(factory.createChannel(cc.name, measurement.get(0), quantity.get(0)));
			} else {
				createdChannels.add(factory.createChannel(cc.name, measurement.get(0), quantity.get(0), unit.get(0)));
			}
		}

		try (Transaction t = em.startTransaction()) {
			t.create(createdChannels);
			t.commit();
		}

		return createdChannels;
	}

	/**
	 * returns localized {@link Channel} attributes
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link Channel} attributes
	 */
	@Deprecated
	public Map<Attribute, String> localizeAttributes(String sourceName) {
		return this.i18nActivity.localizeAttributes(sourceName, Channel.class);
	}

	/**
	 * returns the localized {@link Channel} type name
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link Channel} type name
	 */
	@Deprecated
	public Map<EntityType, String> localizeType(String sourceName) {
		return this.i18nActivity.localizeType(sourceName, Channel.class);
	}
}
