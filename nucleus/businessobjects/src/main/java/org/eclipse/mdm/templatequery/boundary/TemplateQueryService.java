/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.boundary;

import static org.eclipse.mdm.businessobjects.service.EntityService.SL;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.adapter.RelationType;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.FilterItem;
import org.eclipse.mdm.api.base.query.JoinType;
import org.eclipse.mdm.api.base.query.Query;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.CatalogSensor;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.TemplateAttribute;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateSensor;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.control.FilterParser;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.templatequery.entity.TemplateEntityCatAttrDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityCatCompDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityCatSensorAttrDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityCatSensorDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityRootDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityTplAttrDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityTplCompDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityTplRootDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityTplSensorAttrDTO;
import org.eclipse.mdm.templatequery.entity.TemplateEntityTplSensorDTO;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import io.vavr.collection.Seq;

/**
 * Implementation for creating, updating and querying templates and catalogs.
 *
 * @since 5.2.0
 * @author MAF and JZ, Peak Solution GmbH
 */
@Stateless
public class TemplateQueryService {

	private static final String ATTR_NAME_DEFAULT_FILES = "DefaultFiles";

	@Inject
	private ConnectorService connectorService;

	@EJB
	private EntityService entityService;

	private EntityManager entityManager;

	public static class EntityNames {
		final String tplRootName;
		final String tplCompName;
		final String tplAttrName;
		final String catCompName;
		final String catAttrName;
		static final String tplSensorCompName = "TplSensor";
		static final String tplSensorAttrName = "TplSensorAttr";
		static final String catSensorCompName = "CatSensor";
		static final String catSensorAttrName = "CatSensorAttr";

		EntityNames(String contextTypeName) {
			this.tplRootName = String.format("Tpl%sRoot", contextTypeName);
			this.tplCompName = String.format("Tpl%sComp", contextTypeName);
			this.tplAttrName = String.format("Tpl%sAttr", contextTypeName);
			this.catCompName = String.format("Cat%sComp", contextTypeName);
			this.catAttrName = String.format("Cat%sAttr", contextTypeName);
		}
	}

	public static final ImmutableList<String> RELATIONATTR_NAMES = ImmutableList.of("TplUnitUnderTestComp",
			"TplUnitUnderTestRoot", "TplCompParent", "CatUnitUnderTestComp", "TplTestEquipmentComp",
			"TplTestEquipmentRoot", "CatTestEquipmentComp", "TplTestSequenceComp", "TplTestSequenceRoot",
			"CatTestSequenceComp");

	public TemplateEntityRootDTO getTemplate(String sourceName, ContextType contextType, String filterString,
			List<String> attrList) {

		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		ModelManager modelManager = context.getModelManager()
				.orElseThrow(() -> new MDMEntityAccessException("Model manager not present!"));
		QueryService queryService = context.getQueryService()
				.orElseThrow(() -> new MDMEntityAccessException("Query service not present!"));

		List<EntityType> tplEntityTypesTypes = new ArrayList<>();
		List<EntityType> catEntityTypesTypes = new ArrayList<>();

		EntityNames entityNames = new EntityNames(contextType.typeName());

		// TplRoot -> TplComp
		EntityType etTplRoot = modelManager.getEntityType(TemplateRoot.class, contextType);
		Relation rTplRoot2TplComp = etTplRoot.getChildRelations().stream()
				.filter(r -> entityNames.tplCompName.equals(r.getName())).findAny().get();
		EntityType etTplComp = rTplRoot2TplComp.getTarget();
		Relation rTplComp2TplRoot = etTplComp.getRelation(etTplRoot);

		// TplComp -> TplAttr
		Relation rTplComp2TplAttr = etTplComp.getChildRelations().stream()
				.filter(r -> entityNames.tplAttrName.equals(r.getName())).findAny().get();
		EntityType etTplAttr = rTplComp2TplAttr.getTarget();

		// TplAttr -> CatAttr
		Relation rTplAttr2CatAttr = etTplAttr.getRelations().stream()
				.filter(r -> entityNames.catAttrName.equals(r.getName())).findAny().get();
		EntityType etCatAttr = rTplAttr2CatAttr.getTarget();
		catEntityTypesTypes.add(etCatAttr);

		// TplAttr -> DefaultFile
		Optional<Relation> rTplAttrDefaultFiles = etTplAttr.getRelations().stream()
				.filter(r -> "DefaultFiles".equalsIgnoreCase(r.getName())).findAny();

		// CatAttr -> CatComp
		Relation rCatAttr2CatComp = etCatAttr.getParentRelations().stream()
				.filter(r -> entityNames.catCompName.equals(r.getName())).findAny().get();
		EntityType etCatComp = modelManager.getEntityType(CatalogComponent.class, contextType);
		catEntityTypesTypes.add(etCatComp);

		// CatAttr -> ValueList
		EntityType etValueList = modelManager.getEntityType(ValueList.class);
		Relation rCatAttr2ValueList = etCatAttr.getRelation(etValueList);

		Relation relTplCompToTplComp = etTplComp.getRelation(etTplComp);
		Relation relTplAttrToTplComp = etTplAttr.getRelation(etTplComp);
		Relation relCatAttrToCatComp = etCatAttr.getRelation(etCatComp);

		Relation rTplComp2TplSensor = null;
		Relation rTplSensor2TplCompRoot = null;
		Relation rParentTplSensorComp = null;
		Relation rParentSensorAttr = null;
		Relation rParentCatSensorAttr = null;
		Relation rCatSensor2CatComp = null;
		Filter filter;
		Query query;

		tplEntityTypesTypes.add(etTplRoot);
		tplEntityTypesTypes.add(etTplComp);
		tplEntityTypesTypes.add(etTplAttr);

		List<EntityType> allEntityTypes = new ArrayList<>();
		allEntityTypes.addAll(new ArrayList<>(tplEntityTypesTypes));
		allEntityTypes.addAll(new ArrayList<>(catEntityTypesTypes));

		List<Attribute> attributes = attrList.stream().map(c -> getAttribute(allEntityTypes, c, false))
				.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
		filter = FilterParser.parseFilterString(allEntityTypes, filterString);

		List<EntityType> relevantSearchableTypes = getRelevantSearchableTypes(filter, attributes, tplEntityTypesTypes,
				catEntityTypesTypes, attrList.stream().filter(s -> s.endsWith(ATTR_NAME_DEFAULT_FILES)).findFirst());

		attributes
				.addAll(getMissingMandatoryAttrs(relevantSearchableTypes, attributes, isDefaultFilesSearch(attrList)));

		boolean readSensor = isSensorInQuery(contextType, attributes);

		List<Result> results;

		if (readSensor) {
			// TplComp -> TplSensor
			rTplComp2TplSensor = etTplComp.getChildRelations().stream()
					.filter(r -> EntityNames.tplSensorCompName.equals(r.getName())).findAny().get();
			EntityType etTplSensor = rTplComp2TplSensor.getTarget();
			rTplSensor2TplCompRoot = etTplSensor.getRelation(etTplComp);
			// TplSensor -> TplSensorAttr
			Relation rTplSensor2TplSensorAttr = etTplSensor.getChildRelations().stream()
					.filter(r -> EntityNames.tplSensorAttrName.equals(r.getName())).findAny().get();
			EntityType etTplSensorAttr = rTplSensor2TplSensorAttr.getTarget();
			// TplSensorAttr -> CatSensorAttr
			Relation rTplAttr2SenAttr = etTplSensorAttr.getRelations().stream()
					.filter(r -> EntityNames.catSensorAttrName.equals(r.getName())).findAny().get();
			EntityType etCatSensorAttr = rTplAttr2SenAttr.getTarget();

			// TplSensor -> Quantity
			EntityType etQuantity = modelManager.getEntityType(Quantity.class);
			Relation rTolSensor2Quantity = etTplSensor.getRelation(etQuantity);

			// CatAttr -> CatComp
			Relation rCatAttr2CatSensorComp = etCatSensorAttr.getParentRelations().stream()
					.filter(r -> EntityNames.catSensorCompName.equals(r.getName())).findAny().get();
			EntityType etCatSensorComp = rCatAttr2CatSensorComp.getTarget();

			// CatAttr -> CatSensorComp
			rCatSensor2CatComp = etCatComp.getChildRelations().stream()
					.filter(r -> EntityNames.catSensorCompName.equals(r.getName())).findAny().get();
			EntityType etCatSensor = rCatSensor2CatComp.getTarget();

			// CatSensorAttr -> ValueList
			EntityType etValueLst = modelManager.getEntityType(ValueList.class);
			Relation rCatSensorAttr2ValueList = etCatSensorAttr.getRelation(etValueLst);

			rParentTplSensorComp = etTplSensor.getRelation(etTplComp);
			rParentSensorAttr = etTplSensorAttr.getRelation(etTplSensor);
			rParentCatSensorAttr = etCatSensorAttr.getRelation(etCatSensorComp);

			List<EntityType> searchableEntities = Arrays.asList(etTplRoot, etTplComp, etTplAttr, etCatComp, etCatAttr,
					etTplSensor, etTplSensorAttr, etCatSensorAttr, etCatSensorComp, etCatSensor);

			query = queryService.createQuery().selectAll(searchableEntities).select(relCatAttrToCatComp.getAttribute())
					.select(relTplAttrToTplComp.getAttribute()).select(relTplCompToTplComp.getAttribute())
					.select(rCatAttr2ValueList.getAttribute()).select(rTplComp2TplRoot.getAttribute())
					.select(rParentTplSensorComp.getAttribute()).select(rTplComp2TplSensor.getAttribute())
					.select(rParentSensorAttr.getAttribute()).select(rParentCatSensorAttr.getAttribute())
					.select(rCatSensorAttr2ValueList.getAttribute()).select(rTolSensor2Quantity.getAttribute())
					.select(rCatSensor2CatComp.getAttribute()).join(rTplComp2TplRoot, JoinType.OUTER)
					.join(rTplComp2TplSensor, JoinType.OUTER).join(rTplSensor2TplSensorAttr, JoinType.OUTER)
					.join(rTplAttr2SenAttr, JoinType.OUTER).join(rCatAttr2CatSensorComp, JoinType.OUTER)
					.join(rCatSensor2CatComp, JoinType.OUTER).join(rTplComp2TplAttr, JoinType.OUTER)
					.join(rTplAttr2CatAttr, JoinType.OUTER).join(rCatAttr2CatComp, JoinType.OUTER);

			results = query.fetch(filter);

			if (isDefaultFilesSearch(attrList) && !results.isEmpty() && rTplAttrDefaultFiles.isPresent()) {
				DefaultFileUtil defaultFileUtil = new DefaultFileUtil();

				Query defaultFilesQuery = queryService.createQuery()
						.select(rTplAttrDefaultFiles.get().getSource().getIDAttribute())
						.selectAll(rTplAttrDefaultFiles.get().getTarget()).join(rTplComp2TplRoot, JoinType.OUTER)
						.join(rTplComp2TplSensor, JoinType.OUTER).join(rTplSensor2TplSensorAttr, JoinType.OUTER)
						.join(rTplAttr2SenAttr, JoinType.OUTER).join(rCatAttr2CatSensorComp, JoinType.OUTER)
						.join(rCatSensor2CatComp, JoinType.OUTER).join(rTplComp2TplAttr, JoinType.OUTER)
						.join(rTplAttr2CatAttr, JoinType.OUTER).join(rCatAttr2CatComp, JoinType.OUTER)
						.join(rTplAttrDefaultFiles.get());

				List<Result> defaultFiles = defaultFilesQuery.fetch(filter);

				defaultFileUtil.addDefaultFiles(rTplAttrDefaultFiles.get(), results, defaultFiles);
			}
		} else {
			List<EntityType> searchableEntities = Arrays.asList(etTplRoot, etTplComp, etTplAttr, etCatComp, etCatAttr);

			if (attrList == null || attrList.isEmpty()) {
				query = queryService.createQuery().selectAll(searchableEntities);
			} else {
				query = queryService.createQuery().select(attributes);
			}

			query = query.join(rTplComp2TplRoot, JoinType.OUTER).join(rTplComp2TplAttr, JoinType.OUTER)
					.join(rTplAttr2CatAttr, JoinType.OUTER).join(rCatAttr2CatComp, JoinType.OUTER);

			if (isCatalogSearch(relevantSearchableTypes, catEntityTypesTypes)) {
				query = query.select(relCatAttrToCatComp.getAttribute()).select(relTplAttrToTplComp.getAttribute())
						.select(relTplCompToTplComp.getAttribute()).select(rCatAttr2ValueList.getAttribute())
						.select(rTplComp2TplRoot.getAttribute());
			} else {
				query = query.select(relTplAttrToTplComp.getAttribute()).select(relTplCompToTplComp.getAttribute())
						.select(rTplComp2TplRoot.getAttribute());
			}

			results = query.fetch(filter);

			if (isDefaultFilesSearch(attrList) && !results.isEmpty() && rTplAttrDefaultFiles.isPresent()) {
				DefaultFileUtil defaultFileUtil = new DefaultFileUtil();

				Query defaultFilesQuery = queryService.createQuery()
						.select(rTplAttrDefaultFiles.get().getSource().getIDAttribute())
						.selectAll(rTplAttrDefaultFiles.get().getTarget()).join(rTplComp2TplRoot, JoinType.OUTER)
						.join(rTplComp2TplAttr, JoinType.OUTER).join(rTplAttr2CatAttr, JoinType.OUTER)
						.join(rCatAttr2CatComp, JoinType.OUTER).join(rTplAttrDefaultFiles.get());

				List<Result> defaultFiles = defaultFilesQuery.fetch(filter);

				defaultFileUtil.addDefaultFiles(rTplAttrDefaultFiles.get(), results, defaultFiles);
			}
		}

		Set<String> loadedTplCompIds = extractIds(results, etTplComp.getIDAttribute());
		Set<String> neededTplCompIds = extractIds(results, relTplCompToTplComp.getAttribute());

		int maxRecursions = 10;
		do {
			if (loadedTplCompIds.isEmpty() && neededTplCompIds.isEmpty()) {
				break;
			}
			Filter f = Filter.or();

			if (!neededTplCompIds.isEmpty()) {
				f = f.ids(etTplComp, neededTplCompIds);
			}

			if (!loadedTplCompIds.isEmpty()) {
				f = f.ids(relTplCompToTplComp, loadedTplCompIds);
			}

			List<Result> nextResults = query.fetch(f);

			loadedTplCompIds.addAll(extractIds(nextResults, etTplComp.getIDAttribute()));
			neededTplCompIds.addAll(extractIds(nextResults, relTplCompToTplComp.getAttribute()));

			neededTplCompIds.removeAll(loadedTplCompIds);
			results.addAll(nextResults);
		} while (!neededTplCompIds.isEmpty() && maxRecursions-- > 0);

		TemplateEntityRootDTO root = new TemplateEntityRootDTO();
		root.setContextType(contextType.name());

		// if the filter has only TplRoot Conditions query only the TemplateRoot to get
		// Roots without components
		if (isOnlyTplRootFilter(filter, modelManager)) {
			List<EntityType> searchableEntities = Arrays.asList(etTplRoot);

			query = queryService.createQuery().selectAll(searchableEntities).join(rTplRoot2TplComp, JoinType.OUTER);
			filter.add(ComparisonOperator.IS_NULL.create(rTplComp2TplRoot.getAttribute(), null));

			List<Result> fetch = query.fetch(filter);
			results.addAll(fetch);
		}

		// Create all TplRoots:
		for (Result r : results) {
			for (Record rec : r) {
				if (entityNames.tplRootName.equals(rec.getEntityType().getName())) {
					if (rec.getID() != null && !rec.getID().isEmpty() && !"0".equals(rec.getID())
							&& findEntity(root, entityNames.tplRootName, rec.getID()) == null) {
						root.getTemplateRoots().add(createEntity(root, sourceName, entityNames, rec, null));
					}
				}
			}
		}

		// Create all "top level" TplComps:
		createSubordinateEntities(sourceName, entityNames, root, results, entityNames.tplCompName,
				entityNames.tplRootName, rTplComp2TplRoot);

		// Create all "subordinate" TplComps:
		List<Pair<String, TemplateEntityDTO>> listComps = new ArrayList<>();
		for (Result r : results) {
			for (Record rec : r) {
				String relId = "";
				if (entityNames.tplCompName.equals(rec.getEntityType().getName())
						&& !Strings.isNullOrEmpty(relId = rec.getID(relTplCompToTplComp).orElse(""))
						&& findEntity(root, entityNames.tplCompName, rec.getID()) == null
						&& findEntity(listComps.stream().map(p -> p.getRight()).collect(Collectors.toList()),
								entityNames.tplCompName, rec.getID()) == null) {
					TemplateEntityDTO parentTe = findEntity(root, entityNames.tplCompName, relId);
					TemplateEntityDTO newTe = createEntity(root, sourceName, entityNames, rec, parentTe);
					if (parentTe == null) {
						parentTe = findEntity(listComps.stream().map(p -> p.getRight()).collect(Collectors.toList()),
								entityNames.tplCompName, relId);
					}

					if (parentTe == null) {
						listComps.add(new ImmutablePair<String, TemplateEntityDTO>(relId, newTe));
					} else {
						parentTe.getSubentities().add(newTe);
					}

				}
			}
		}

		while (listComps.size() > 0) {
			int origSize = listComps.size();
			for (int i = origSize - 1; i >= 0; --i) {
				Pair<String, TemplateEntityDTO> pair = listComps.get(i);
				TemplateEntityDTO parentTe = findEntity(root, entityNames.tplCompName, pair.getLeft());
				if (parentTe != null) {
//					if (findEntity(root, entityNames.tplCompName,
//							pair.getRight().getId()) == null) {
					parentTe.getSubentities().add(pair.getRight());
//					}

					listComps.remove(i);
				}
			}

			if (origSize == listComps.size()) {
				break;
			}
		}

		// Create all TplAttrs:
		createSubordinateEntities(sourceName, entityNames, root, results, entityNames.tplAttrName,
				entityNames.tplCompName, relTplAttrToTplComp);

		// Create all CatComps:
		List<Pair<String, TemplateEntityDTO>> listCatComps = new ArrayList<>();
		for (Result r : results) {
			MutablePair<String, TemplateEntityDTO> pair = new MutablePair<>();
			for (Record rec : r) {
				if (entityNames.catCompName.equals(rec.getEntityType().getName())) {
					Optional<TemplateEntityDTO> opt = listCatComps.stream()
							.filter(e -> e.getRight().getId().equals(rec.getID())).map(e -> e.getRight()).findFirst();
					pair.setRight(opt.isPresent() ? opt.get() : createEntity(root, sourceName, entityNames, rec, null));
				} else if (entityNames.tplCompName.equals(rec.getEntityType().getName())) {
					pair.setLeft(rec.getID());
				}
			}
			if (pair.getLeft() != null && pair.getRight() != null) {
				listCatComps.add(pair);
			}
		}

		for (Pair<String, TemplateEntityDTO> pair : listCatComps) {
			TemplateEntityDTO parentTe = findEntity(root, entityNames.tplCompName, pair.getLeft());
			if (parentTe != null && !parentTe.getSubentities().stream().filter(
					s -> (s.getType().equals(entityNames.catCompName) && s.getId().equals(pair.getRight().getId())))
					.findFirst().isPresent()) {
				parentTe.getSubentities().add(pair.getRight());
			}
		}

		// Create all CatAttrs:
		createSubordinateEntities(sourceName, entityNames, root, results, entityNames.catAttrName,
				entityNames.catCompName, relCatAttrToCatComp);

		// Create all CatSensor:
		if (rCatSensor2CatComp != null) {
			// Create all CatSensorComps:
			List<Pair<String, TemplateEntityDTO>> listCatSensor = new ArrayList<>();
			for (Result r : results) {
				MutablePair<String, TemplateEntityDTO> pair = new MutablePair<>();
				for (Record rec : r) {
					if (EntityNames.catSensorCompName.equals(rec.getEntityType().getName())) {
						Optional<TemplateEntityDTO> opt = listCatSensor.stream()
								.filter(e -> e.getRight().getId().equals(rec.getID())).map(e -> e.getRight())
								.findFirst();
						pair.setRight(
								opt.isPresent() ? opt.get() : createEntity(root, sourceName, entityNames, rec, null));
					} else if (entityNames.catCompName.equals(rec.getEntityType().getName())) {
						pair.setLeft(rec.getID());
					}
				}
				if (pair.getLeft() != null && pair.getRight() != null) {
					listCatSensor.add(pair);
				}
			}

			for (Pair<String, TemplateEntityDTO> pair : listCatSensor) {
				TemplateEntityDTO parentTe = findEntity(root, entityNames.catCompName, pair.getLeft());
				if (parentTe != null && !parentTe.getSubentities().stream()
						.filter(s -> (s.getType().equals(EntityNames.catSensorCompName)
								&& s.getId().equals(pair.getRight().getId())))
						.findFirst().isPresent()) {
					parentTe.getSubentities().add(pair.getRight());
				}
			}

			// Create all CatSensorAttrs:
			createSubordinateEntities(sourceName, entityNames, root, results, EntityNames.catSensorAttrName,
					EntityNames.catSensorCompName, rParentCatSensorAttr);
		}

		// Sensor
		if (rTplComp2TplSensor != null) {
			// Create all "top level" TplSensors:
			createSubordinateEntities(sourceName, entityNames, root, results, EntityNames.tplSensorCompName,
					entityNames.tplCompName, rTplSensor2TplCompRoot);
			// Create all "subordinate" TplComps:
			List<Pair<String, TemplateEntityDTO>> listSensorComps = new ArrayList<>();
			for (Result r : results) {
				for (Record rec : r) {
					String relId = "";
					if (EntityNames.tplSensorCompName.equals(rec.getEntityType().getName())
							&& rParentTplSensorComp != null
							&& !Strings.isNullOrEmpty(relId = rec.getID(rParentTplSensorComp).orElse(""))
							&& findEntity(root, EntityNames.tplSensorCompName, rec.getID()) == null
							&& findEntity(listComps.stream().map(p -> p.getRight()).collect(Collectors.toList()),
									EntityNames.tplSensorCompName, rec.getID()) == null) {
						TemplateEntityDTO parentTe = findEntity(root, EntityNames.tplSensorCompName, relId);
						TemplateEntityDTO newTe = createEntity(root, sourceName, entityNames, rec, parentTe);
						if (parentTe == null) {
							parentTe = findEntity(
									listComps.stream().map(p -> p.getRight()).collect(Collectors.toList()),
									EntityNames.tplSensorCompName, relId);
						}

						if (parentTe == null) {
							listComps.add(new ImmutablePair<String, TemplateEntityDTO>(relId, newTe));
						} else {
							parentTe.getSubentities().add(newTe);
						}

					}
				}
			}

			while (listSensorComps.size() > 0) {
				int origSize = listSensorComps.size();
				for (int i = origSize - 1; i >= 0; --i) {
					Pair<String, TemplateEntityDTO> pair = listSensorComps.get(i);
					TemplateEntityDTO parentTe = findEntity(root, EntityNames.tplSensorCompName, pair.getLeft());
					if (parentTe != null) {
						if (findEntity(root, EntityNames.tplSensorCompName, pair.getRight().getId()) == null) {
							parentTe.getSubentities().add(pair.getRight());
						}

						listSensorComps.remove(i);
					}
				}

				if (origSize == listSensorComps.size()) {
					break;
				}
			}

			// Create all TplSensorAttrs:
			createSubordinateEntities(sourceName, entityNames, root, results, EntityNames.tplSensorAttrName,
					EntityNames.tplSensorCompName, rParentSensorAttr);

			// Create all CatSensorComps:
			List<Pair<String, TemplateEntityDTO>> listCatSensorComps = new ArrayList<>();
			for (Result r : results) {
				MutablePair<String, TemplateEntityDTO> pair = new MutablePair<>();
				for (Record rec : r) {
					if (EntityNames.catSensorCompName.equals(rec.getEntityType().getName())) {
						Optional<TemplateEntityDTO> opt = listCatSensorComps.stream()
								.filter(e -> e.getRight().getId().equals(rec.getID())).map(e -> e.getRight())
								.findFirst();
						pair.setRight(
								opt.isPresent() ? opt.get() : createEntity(root, sourceName, entityNames, rec, null));
					} else if (EntityNames.tplSensorCompName.equals(rec.getEntityType().getName())) {
						pair.setLeft(rec.getID());
					}
				}
				if (pair.getLeft() != null && pair.getRight() != null) {
					listCatSensorComps.add(pair);
				}
			}

			for (Pair<String, TemplateEntityDTO> pair : listCatSensorComps) {
				TemplateEntityDTO parentTe = findEntity(root, EntityNames.tplSensorCompName, pair.getLeft());
				if (parentTe != null && !parentTe.getSubentities().stream()
						.filter(s -> (s.getType().equals(EntityNames.catSensorCompName)
								&& s.getId().equals(pair.getRight().getId())))
						.findFirst().isPresent()) {
					parentTe.getSubentities().add(pair.getRight());
				}
			}

			// Create all CatSensorAttrs:
			createSubordinateEntities(sourceName, entityNames, root, results, EntityNames.catSensorAttrName,
					EntityNames.catSensorCompName, rParentCatSensorAttr);
		}

		return root;
	}

	private boolean isDefaultFilesSearch(List<String> attrList) {
		Optional<String> defaultFilesAttr = attrList.stream().filter(s -> s.endsWith(ATTR_NAME_DEFAULT_FILES))
				.findFirst();

		if (attrList.isEmpty() || defaultFilesAttr.isPresent()) {
			return true;
		}

		return false;
	}

	private boolean isCatalogSearch(List<EntityType> relevantSearchableTypes, List<EntityType> catEntityTypes) {

		for (EntityType et : relevantSearchableTypes) {
			if (catEntityTypes.contains(et)) {
				return true;
			}
		}

		return false;
	}

	private TreeMap<String, EntityType> getEntityTypes(ModelManager modelManager, ContextType contextType) {
		EntityNames entityNames = new EntityNames(contextType.typeName());
		EntityType etTplRoot = modelManager.getEntityType(TemplateRoot.class, contextType);
		Relation rTplRoot2TplComp = etTplRoot.getChildRelations().stream()
				.filter(r -> entityNames.tplCompName.equals(r.getName())).findAny().get();
		EntityType etTplComp = rTplRoot2TplComp.getTarget();

		// TplComp -> TplAttr
		Relation rTplComp2TplAttr = etTplComp.getChildRelations().stream()
				.filter(r -> entityNames.tplAttrName.equals(r.getName())).findAny().get();
		EntityType etTplAttr = rTplComp2TplAttr.getTarget();

		// TplAttr -> CatAttr
		Relation rTplAttr2CatAttr = etTplAttr.getRelations().stream()
				.filter(r -> entityNames.catAttrName.equals(r.getName())).findAny().get();
		EntityType etCatAttr = rTplAttr2CatAttr.getTarget();

		// CatAttr -> CatComp
		EntityType etCatComp = modelManager.getEntityType(CatalogComponent.class, contextType);

		TreeMap<String, EntityType> map = new TreeMap<>();

		map.put(etTplRoot.getName(), etTplRoot);
		map.put(etTplComp.getName(), etTplComp);
		map.put(etTplAttr.getName(), etTplComp);
		map.put(etCatComp.getName(), etCatComp);
		map.put(etCatAttr.getName(), etCatAttr);

		return map;

	}

	private List<EntityType> getRelevantSearchableTypes(Filter filter, List<Attribute> attributes,
			List<EntityType> tplEntityTypesTypes, List<EntityType> catEntityTypesTypes,
			Optional<String> defaultValuesAttrName) {

		if (attributes.isEmpty()) {
			List<EntityType> etList = new ArrayList<>();
			etList.addAll(tplEntityTypesTypes);
			if (!defaultValuesAttrName.isPresent()) {
				etList.addAll(catEntityTypesTypes);
			}

			return etList;
		}

		Set<EntityType> collect = filter.stream().filter(fi -> fi.isCondition())
				.map(r -> r.getCondition().getAttribute().getEntityType()).collect(Collectors.toSet());
		collect.addAll(attributes.stream().map(r -> r.getEntityType()).collect(Collectors.toSet()));

		boolean tplJointTree = false;
		boolean catJointTree = false;

		for (EntityType et : collect) {
			if (tplEntityTypesTypes.contains(et)) {
				tplJointTree = true;
			}

			if (catEntityTypesTypes.contains(et)) {
				catJointTree = true;
			}
		}

		List<EntityType> etList = new ArrayList<>();

		if (tplJointTree) {
			etList.addAll(tplEntityTypesTypes);
		}

		if (catJointTree) {
			etList.addAll(catEntityTypesTypes);
		}

		return etList;
	}

	private List<Attribute> getMissingMandatoryAttrs(List<EntityType> searchableTypes, List<Attribute> attributes,
			boolean isDefaultFilesSearch) {

		if (attributes.isEmpty() && !isDefaultFilesSearch) {
			return new ArrayList<>();
		}

		List<Attribute> attrs = new ArrayList<>();

		for (EntityType et : searchableTypes) {
			attrs.add(et.getIDAttribute());
			attrs.add(et.getNameAttribute());
		}

		return attrs;
	}

	private boolean isSensorInQuery(ContextType contextType, List<Attribute> attributes) {

		boolean isSensorInQuery = ContextType.TESTEQUIPMENT.equals(contextType);

		if (!attributes.isEmpty()) {
			isSensorInQuery = false;

			for (Attribute attr : attributes) {
				if (EntityNames.catSensorCompName.equals(attr.getEntityType().getName())
						|| EntityNames.catSensorAttrName.equals(attr.getEntityType().getName())
						|| EntityNames.tplSensorAttrName.equals(attr.getEntityType().getName())
						|| EntityNames.tplSensorCompName.equals(attr.getEntityType().getName())) {
					isSensorInQuery = true;
					break;
				}
			}
		}

		return isSensorInQuery;
	}

	private Optional<Attribute> getAttribute(Collection<EntityType> searchableTypes, String c, boolean convertStatus) {
		String[] parts = c.split("\\.");

		if (parts.length != 2) {
			throw new IllegalArgumentException("Cannot parse attribute " + c + "!");
		}

		String type = parts[0];
		String attributeName = parts[1];

		Optional<EntityType> entityType = searchableTypes.stream()
				.filter(e -> ServiceUtils.workaroundForTypeMapping(e).equalsIgnoreCase(type)).findFirst();

		if (entityType.isPresent() && !attributeName.equalsIgnoreCase(ATTR_NAME_DEFAULT_FILES)) {
			Optional<Attribute> attr = entityType.get().getAttributes().stream()
					.filter(a -> a.getName().equalsIgnoreCase(attributeName)).findFirst();
			if (!attr.isPresent()) {
				Optional<Relation> rel = entityType.get().getRelations().stream()
						.filter(r -> r.getName().equalsIgnoreCase(attributeName)).findFirst();
				if (rel.isPresent()) {
					attr = rel.get().getTarget().getAttributes().stream()
							.filter(a -> a.getName().equalsIgnoreCase("Id")).findFirst();
				}
			}
			if (!attr.isPresent() && entityType.get().getRelations().stream()
					.filter(r -> r.getName().equalsIgnoreCase(attributeName)).findFirst().isPresent()) {
				attr = this.getParentIdAttribute(entityType.get().getParentRelations());
			}
			return attr;
		} else {
			return Optional.empty();
		}
	}

	private Optional<Attribute> getParentIdAttribute(List<Relation> relations) {
		Optional<Relation> rel = relations.stream().filter(r -> "TestStep".indexOf(r.getTarget().getName()) >= 0)
				.findFirst();
		if (rel.isPresent()) {
			return rel.get().getTarget().getAttributes().stream().filter(a -> a.getName().equalsIgnoreCase("Id"))
					.findFirst();
		} else {
			for (Relation relation : relations) {
				return this.getParentIdAttribute(relation.getTarget().getRelations(RelationType.INFO));
			}
		}
		return Optional.empty();
	}

	private boolean isOnlyTplRootFilter(Filter filter, ModelManager modelManager) {
		boolean returnVal = true;

		Iterator<FilterItem> iterator = filter.iterator();
		while (iterator.hasNext()) {
			FilterItem fi = iterator.next();
			if (fi.isCondition() && fi.getCondition().getAttribute() != null) {
				EntityType entityTypeTplUUTRoot = modelManager.getEntityType("TplUnitUnderTestRoot");
				EntityType entityTypeTplTSRoot = modelManager.getEntityType("TplTestSequenceRoot");
				EntityType entityTypeTplTERoot = modelManager.getEntityType("TplTestEquipmentRoot");
				EntityType entityTypeCondition = fi.getCondition().getAttribute().getEntityType();

				if (!entityTypeCondition.equals(entityTypeTplTERoot) && !entityTypeCondition.equals(entityTypeTplTSRoot)
						&& !entityTypeCondition.equals(entityTypeTplUUTRoot)) {
					returnVal = false;
					break;
				}
			}
		}
		return returnVal;
	}

	/**
	 * @param sourceName
	 * @param contextType
	 * @param tplRoot
	 * @return
	 * @throws Exception
	 */
	public TemplateEntityRootDTO createUpdateTemplate(TemplateEntityRootDTO root, String sourceName,
			ContextType contextType, TemplateEntityRootDTO tplRoot) throws Exception {
		entityManager = getEntityManager(sourceName);
		Transaction transaction = entityManager.startTransaction();
		try {
			Map<TemplateEntityDTO, Entity> dtoEntity = new HashMap<>();
			this.createUpdateTemplates(root, sourceName, contextType, tplRoot.getTplRoots(), null, null, dtoEntity,
					transaction);
			transaction.commit();
		} catch (Exception exc) {
			transaction.abort();
			throw exc;
		}
		return tplRoot;
	}

	private List<TemplateEntityDTO> createUpdateTemplates(TemplateEntityRootDTO root, String sourceName,
			ContextType contextType, List<TemplateEntityDTO> tpls, Entity parent, TemplateEntityDTO parentDTO,
			Map<TemplateEntityDTO, Entity> dtoEntity, Transaction transaction) throws Exception {
		List<TemplateEntityDTO> listDTOs = new ArrayList<>();
		List<TemplateEntityDTO> updateList = new ArrayList<>();
		for (TemplateEntityDTO dto : tpls) {
			if (dto.getId() == null || dto.getId().isEmpty()) {
				listDTOs.add(dto);
			} else if (dtoEntity.get(dto) == null) {
				updateList.add(dto);
			}
			// An update of a Component needs in some cases the identifier of the parents
			if (!(dto instanceof TemplateEntityTplRootDTO)) {
				dto.setParent(parentDTO);
			}
		}
		if (!listDTOs.isEmpty()) {
			dtoEntity.putAll(
					this.createEntities(root, sourceName, contextType, listDTOs, parent, dtoEntity, transaction));
		}
		if (!updateList.isEmpty()) {
			dtoEntity.putAll(this.updateEntities(sourceName, contextType, updateList, parentDTO, transaction));
		}
		for (TemplateEntityDTO dto : tpls) {
			updateList.addAll(this.createUpdateTemplates(root, sourceName, contextType, dto.getSubentities(),
					dtoEntity.isEmpty() ? null : dtoEntity.get(dto), dto, dtoEntity, transaction));
		}
		return updateList;
	}

	private Set<String> extractIds(List<Result> results, Attribute attribute) {
		return results.stream().map(r -> r.getValue(attribute).extract(ValueType.STRING)).filter(s -> !"0".equals(s))
				.collect(Collectors.toSet());
	}

	private void createSubordinateEntities(String sourceName, EntityNames entityNames, TemplateEntityRootDTO root,
			List<Result> results, String entityName, String parentEntityName, Relation parentRel) {
		for (Result r : results) {
			for (Record rec : r) {
				String relId = "";
				if (entityName.equals(rec.getEntityType().getName())
						&& !Strings.isNullOrEmpty(relId = rec.getID(parentRel).orElse(""))) {
					if (findEntity(root, entityName, rec.getID()) == null) {
						TemplateEntityDTO parentTe = null;
						if ((parentTe = findEntity(root, parentEntityName, relId)) != null
								&& !parentTe.getSubentities().stream()
										.filter(te -> te.getType().equals(entityName) && te.getId().equals(rec.getID()))
										.findFirst().isPresent()) {
							TemplateEntityDTO newTe = createEntity(root, sourceName, entityNames, rec, parentTe);
							parentTe.getSubentities().add(newTe);
							root.put(entityName, rec.getID(), newTe);
						}
					}
				}

			}
		}
	}

	private TemplateEntityDTO createEntity(TemplateEntityRootDTO root, String sourceName, EntityNames entityNames,
			Record rec, TemplateEntityDTO parent) {
		TemplateEntityDTO te = null;
		String typeName = rec.getEntityType().getName();
		String type = null;
		if (entityNames.tplRootName.equals(typeName)) {
			te = new TemplateEntityTplRootDTO();
			type = TemplateRoot.class.getSimpleName();
		} else if (entityNames.tplCompName.equals(typeName)) {
			te = new TemplateEntityTplCompDTO();
			type = TemplateComponent.class.getSimpleName();
		} else if (entityNames.tplAttrName.equals(typeName)) {
			te = new TemplateEntityTplAttrDTO();
			type = TemplateAttribute.class.getSimpleName();
		} else if (entityNames.catCompName.equals(typeName)) {
			te = new TemplateEntityCatCompDTO();
			type = CatalogComponent.class.getSimpleName();
		} else if (entityNames.catAttrName.equals(typeName)) {
			te = new TemplateEntityCatAttrDTO();
			type = CatalogAttribute.class.getSimpleName();
		} else if (EntityNames.tplSensorCompName.equals(typeName)) {
			te = new TemplateEntityTplSensorDTO();
			type = TemplateSensor.class.getSimpleName();
		} else if (EntityNames.tplSensorAttrName.equals(typeName)) {
			te = new TemplateEntityTplSensorAttrDTO();
			type = TemplateAttribute.class.getSimpleName();
		} else if (EntityNames.catSensorCompName.equals(typeName)) {
			te = new TemplateEntityCatSensorDTO();
			type = CatalogComponent.class.getSimpleName();
		} else if (EntityNames.catSensorAttrName.equals(typeName)) {
			te = new TemplateEntityCatSensorAttrDTO();
			type = CatalogAttribute.class.getSimpleName();
		} else {
			throw new MDMEntityAccessException(String.format("Unsupported entity type %s!", typeName));
		}

		te.setSourceName(sourceName);
		te.setSourceType(typeName);
		te.setType(type);
		if (rec.getValues().get("Name") != null) {
			te.setName((String) rec.getValues().get("Name").extract());
		}
		te.setId(rec.getID());
		te.getAttributes()
				.addAll(rec.getValues().entrySet().stream()
						.filter(me -> !"Name".equals(me.getKey()) && !"Id".equals(me.getKey())
								&& !RELATIONATTR_NAMES.contains(me.getKey()))
						.map(me -> me.getValue()).collect(Collectors.toList()));

		if ((entityNames.catAttrName.equals(typeName) || EntityNames.catSensorAttrName.equals(typeName))
				&& parent != null) {

			addDataTypeForCatAttr(sourceName, parent, te);
		}

		root.put(typeName, rec.getID(), te);
		return te;
	}

	private void addDataTypeForCatAttr(String sourceName, TemplateEntityDTO catComp, TemplateEntityDTO catAttr) {
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		ModelManager modelManager = context.getModelManager()
				.orElseThrow(() -> new MDMEntityAccessException("Model manager not present!"));

		EntityType entityType = modelManager.getEntityType(catComp.getName());

		ValueType<?> dataType = entityType.getAttribute(catAttr.getName()).getValueType();

		if (entityType.getRelations().stream().filter(r -> (r.getName().equals(catAttr.getName()) && r.isNtoM()
				&& DescriptiveFile.TYPE_NAME.contentEquals(r.getTarget().getName()))).findFirst().isPresent()) {
			dataType = ValueType.FILE_RELATION;
		}

		EnumRegistry er = EnumRegistry.getInstance();

		Value datatypeAttribute = ValueType.ENUMERATION.create(er.get(sourceName, EnumRegistry.VALUE_TYPE), "DataType");
		datatypeAttribute.set(dataType);
		catAttr.getAttributes().add(datatypeAttribute);
	}

	private TemplateEntityDTO findEntity(TemplateEntityRootDTO rootDTO, String type, String id) {
		return rootDTO.get(type, id);
	}

	private TemplateEntityDTO findEntity(Collection<TemplateEntityDTO> entities, String type, String id) {

		for (TemplateEntityDTO te : entities) {
			if (te.getSourceType().equals(type) && te.getId().equals(id)) {
				return te;
			}

			TemplateEntityDTO subTe = findEntity(te.getSubentities(), type, id);
			if (subTe != null) {
				return subTe;
			}
		}

		return null;
	}

	private Map<TemplateEntityDTO, Entity> createEntities(TemplateEntityRootDTO root, String sourceName,
			ContextType contextType, List<TemplateEntityDTO> entities, Entity parent,
			Map<TemplateEntityDTO, Entity> dtoEntity, Transaction transaction) throws Exception {
		Map<TemplateEntityDTO, Entity> mapDTONewEntities = new HashMap<>();
		Map<TemplateEntityDTO, Entity> mapDTOTplAttrsExisting = new HashMap<>();
		EntityFactory ef = this.getEntityFactory(sourceName);
		Entity entity = null;
		for (TemplateEntityDTO dto : entities) {
			if (mapDTONewEntities.get(dto) != null) {
				// the TemplateEntityDTO is already processed
				break;
			}

			boolean isExistingTplAttr = false;

			if (dto instanceof TemplateEntityTplRootDTO) {
				entity = ef.createTemplateRoot(contextType, dto.getName());
			} else if (dto instanceof TemplateEntityTplCompDTO) {
				TemplateEntityTplCompDTO tplCompDTO = (TemplateEntityTplCompDTO) dto;

				if (Strings.isNullOrEmpty(tplCompDTO.getId()) && tplCompDTO.getTplAttrs().isEmpty()) {
					throw new MDMEntityAccessException("TplComponent should have minimum one TemplateAttribute!");
				}

				entity = this.createTemplateComponent(root, sourceName, contextType, tplCompDTO, parent, ef, dtoEntity,
						transaction);
			} else if (dto instanceof TemplateEntityTplAttrDTO) {

				TemplateComponent tplCompParent = (TemplateComponent) parent;

				Optional<TemplateAttribute> templateAttributeOpt = tplCompParent.getTemplateAttribute(dto.getName());
				if (templateAttributeOpt.isPresent()) {
					entity = templateAttributeOpt.get();
					isExistingTplAttr = true;
				} else {
					entity = this.createTemplateComponentAttr(root, sourceName, contextType,
							(TemplateEntityTplAttrDTO) dto, tplCompParent, ef, dtoEntity, transaction);
				}

			} else if (dto instanceof TemplateEntityCatCompDTO) {
				entity = ef.createCatalogComponent(contextType, dto.getName());
			} else if (dto instanceof TemplateEntityCatAttrDTO || dto instanceof TemplateEntityCatSensorAttrDTO) {
				entity = this.createCatalogAttribute(root, sourceName, contextType, dto, parent, ef);
			} else if (dto instanceof TemplateEntityTplSensorDTO) {
				entity = this.createTemplateSensor(root, sourceName, contextType, (TemplateEntityTplSensorDTO) dto,
						parent, ef, dtoEntity, transaction);
			} else if (dto instanceof TemplateEntityCatSensorDTO && parent instanceof CatalogComponent) {
				entity = ef.createCatalogSensor(dto.getName(), (CatalogComponent) parent);
			}
			if (entity != null) {
				for (Value val : dto.getAttributes()) {
					if ("ValueList".equals(val.getName()) && entity instanceof CatalogAttribute) {
						String valueListId = val.extract(ValueType.STRING);
						if (!Strings.isNullOrEmpty(valueListId) && !"0".equals(valueListId)) {
							((CatalogAttribute) entity)
									.setValueList(this.entityManager.load(ValueList.class, valueListId));
						}
					} else if (!"DataType".equals(val.getName()) && !"Unit".equals(val.getName())
							&& !"Quantity".equals(val.getName())) {
						entity.getValue(val.getName()).set(val.extract(ValueType.STRING));
					}
				}

				if (isExistingTplAttr) {
					mapDTOTplAttrsExisting.put(dto, entity);
				} else {
					mapDTONewEntities.put(dto, entity);
				}
			}
		}
		if (!mapDTONewEntities.isEmpty()) {
			// Upload Entities.
			transaction.create(mapDTONewEntities.values());
			mapDTONewEntities.forEach((key, value) -> {
				key.setId(value.getID());
			});
		}

		if (!mapDTOTplAttrsExisting.isEmpty()) {
			// Upload Entities.
			transaction.update(mapDTOTplAttrsExisting.values());
			mapDTOTplAttrsExisting.forEach((key, value) -> {
				key.setId(value.getID());
			});
		}

		mapDTONewEntities.putAll(mapDTOTplAttrsExisting);

		return mapDTONewEntities;
	}

	private TemplateComponent createTemplateComponent(TemplateEntityRootDTO root, String sourceName,
			ContextType contextType, TemplateEntityTplCompDTO dto, Entity parent, EntityFactory ef,
			Map<TemplateEntityDTO, Entity> dtoEntity, Transaction transaction) throws Exception {
		TemplateEntityCatCompDTO catComp = (TemplateEntityCatCompDTO) dto.getCatComp();
		if (catComp == null || catComp.getId() == null || catComp.getId().isEmpty()) {
			for (TemplateEntityDTO entityDTO : dtoEntity.keySet()) {
				if (entityDTO instanceof TemplateEntityCatCompDTO
						&& (catComp == null || entityDTO.getName().equals(catComp.getName()))) {
					catComp = (TemplateEntityCatCompDTO) entityDTO;
					dto.setCatComp(catComp);
					break;
				}
			}
		}
		CatalogComponent entityCatComp = null;
		if (catComp != null) {
			if (catComp.getId() == null || catComp.getId().isEmpty()) {
				dtoEntity.putAll(this.createEntities(root, sourceName, contextType, Arrays.asList(catComp), parent,
						dtoEntity, transaction));
				entityCatComp = (CatalogComponent) dtoEntity.get(catComp);
				catComp.setId(entityCatComp.getID());
				dtoEntity.putAll(this.createEntities(root, sourceName, contextType, catComp.getSubentities(),
						entityCatComp, dtoEntity, transaction));
			} else {
				if (catComp.getParent() == null) {
					catComp.setParent(findCatComp(Arrays.asList(dto)));
				}
				dtoEntity
						.putAll(this.updateEntities(sourceName, contextType, Arrays.asList(catComp), dto, transaction));
				entityCatComp = (CatalogComponent) dtoEntity.get(catComp);
				List<TemplateEntityDTO> createDTOs = new ArrayList<>();
				List<TemplateEntityDTO> updateDTOs = new ArrayList<>();
				for (TemplateEntityDTO tempDTO : catComp.getSubentities()) {
					if (tempDTO.getId() == null || tempDTO.getId().isEmpty()) {
						createDTOs.add(tempDTO);
					} else {
						updateDTOs.add(tempDTO);
					}
					tempDTO.setParent(catComp);
				}
				if (!createDTOs.isEmpty()) {
					dtoEntity.putAll(this.createEntities(root, sourceName, contextType, createDTOs, entityCatComp,
							dtoEntity, transaction));
				}
				if (!updateDTOs.isEmpty()) {
					dtoEntity.putAll(this.updateEntities(sourceName, contextType, updateDTOs, null, transaction));
				}
			}
		}
		if (parent instanceof TemplateRoot) {
			return ef.createTemplateComponent(dto.getName(), (TemplateRoot) parent, entityCatComp, false);
		} else {
			return ef.createTemplateComponent(dto.getName(), (TemplateComponent) parent, entityCatComp, false);
		}
	}

	private TemplateAttribute createTemplateComponentAttr(TemplateEntityRootDTO root, String sourceName,
			ContextType contextType, TemplateEntityTplAttrDTO dto, TemplateComponent parent, EntityFactory ef,
			Map<TemplateEntityDTO, Entity> dtoEntity, Transaction transaction) throws Exception {
		TemplateEntityTplCompDTO tplComp = (TemplateEntityTplCompDTO) dto.getParent();
		TemplateEntityCatCompDTO catComp = tplComp == null ? null : (TemplateEntityCatCompDTO) (tplComp).getCatComp();
		TemplateEntityCatAttrDTO catCompAttr = null;
		if (catComp != null) {
			for (TemplateEntityDTO attrDTO : catComp.getCatAttrs()) {
				if (attrDTO.getName().equals(dto.getName())) {
					catCompAttr = (TemplateEntityCatAttrDTO) attrDTO;
					break;
				}
			}
		}
		if (catCompAttr != null && (catCompAttr.getId() == null || catCompAttr.getId().isEmpty())) {
			CatalogComponent entityCatComp = (CatalogComponent) dtoEntity.get(catComp);
			if (entityCatComp == null) {
				dtoEntity.putAll(
						this.updateEntities(sourceName, contextType, Arrays.asList(catComp), tplComp, transaction));
				entityCatComp = (CatalogComponent) dtoEntity.get(catComp);
			}
			dtoEntity.putAll(this.createEntities(root, sourceName, contextType, Arrays.asList(catCompAttr),
					entityCatComp, dtoEntity, transaction));
			return ef.createTemplateAttribute((CatalogAttribute) dtoEntity.get(catCompAttr), parent);
		}
		return ef.createTemplateAttribute(dto.getName(), parent);
	}

	private CatalogAttribute createCatalogAttribute(TemplateEntityRootDTO root, String sourceName,
			ContextType contextType, TemplateEntityDTO dto, Entity parent, EntityFactory ef) {
		CatalogAttribute entity = null;

		Unit unit = null;
		Value val = dto.getAttributes().stream().filter(attr -> attr.getName().equals("Unit")).findFirst().orElse(null);
		if (val != null && val.isValid()) {
			String unitId = null;

			if (ValueType.STRING.equals(val.getValueType())) {
				unitId = val.extract();
			} else if (ValueType.INTEGER.equals(val.getValueType())) {
				unitId = ((Integer) val.extract()).toString();
			} else if (ValueType.LONG.equals(val.getValueType())) {
				unitId = ((Long) val.extract()).toString();
			} else {
				throw new MDMEntityAccessException(
						String.format("Unexpected value type %s for Unit attribute!", val.getValueType().name()));
			}

			if (unitId != null && !unitId.isEmpty() && !"0".equals(unitId)) {
				ApplicationContext context = connectorService.getContextByName(sourceName);
				unit = context.getEntityManager().get().load(Unit.class, unitId);
			}
		}

		val = dto.getAttributes().stream().filter(attr -> attr.getName().equals("DataType")).findFirst().get();
		String valueTypeString = val.extract().toString();
		ValueType<?> type = ValueType.valueTypeByName(valueTypeString);
		if (dto instanceof TemplateEntityCatAttrDTO) {//
//YYY			if (ValueType.ENUMERATION.equals(type)) {
//				entity = ef.createCatalogAttribute(dto.getName(), type.getOwner(), (CatalogComponent) parent, unit);
			if (type.isEnumeration()) {
				entity = ef.createCatalogAttribute(dto.getName(), type, type.getOwner(), (CatalogComponent) parent);
			} else {
				entity = ef.createCatalogAttribute(dto.getName(), type, (CatalogComponent) parent, unit);
			}
		} else if (dto instanceof TemplateEntityCatSensorAttrDTO) {
			entity = ef.createCatalogSensorAttribute(dto.getName(), type, (CatalogSensor) parent, unit);
		}
		return entity;
	}

	private TemplateEntityDTO findCatComp(List<TemplateEntityDTO> dtos) {
		TemplateEntityDTO result = null;
		for (TemplateEntityDTO dto : dtos) {
			if (dto instanceof TemplateEntityCatCompDTO) {
				result = dto;
				break;
			} else if (dto instanceof TemplateEntityTplCompDTO) {
				result = this.findCatComp(Arrays.asList(((TemplateEntityTplCompDTO) dto).getCatComp()));
				if (result == null) {
					result = this.findCatComp(((TemplateEntityTplCompDTO) dto).getTplComps());
				}
			} else if (dto.getParent() instanceof TemplateEntityTplRootDTO) {
				result = this.findCatComp(((TemplateEntityTplRootDTO) dto).getTplComps());
			} else {
				result = this.findCatComp(Arrays.asList(dto.getParent()));
			}
		}
		return result;
	}

	private TemplateSensor createTemplateSensor(TemplateEntityRootDTO root, String sourceName, ContextType contextType,
			TemplateEntityTplSensorDTO dto, Entity parent, EntityFactory ef, Map<TemplateEntityDTO, Entity> dtoEntity,
			Transaction transaction) throws Exception {
		TemplateEntityCatSensorDTO catSensor = (TemplateEntityCatSensorDTO) dto.getCatSensor();
		if (catSensor == null || catSensor.getId() == null || catSensor.getId().isEmpty()) {
			for (TemplateEntityDTO entityDTO : dtoEntity.keySet()) {
				if (entityDTO instanceof TemplateEntityCatSensorDTO
						&& (catSensor == null || entityDTO.getName().equals(catSensor.getName()))) {
					catSensor = (TemplateEntityCatSensorDTO) entityDTO;
					dto.setCatSensor(catSensor);
					break;
				}
			}
		}
		CatalogSensor entityCatSensor = null;
		if (catSensor != null) {
			if (catSensor.getId() == null || catSensor.getId().isEmpty()) {
				Entity tempParent = null;
				Optional<Entry<TemplateEntityDTO, Entity>> entry = dtoEntity.entrySet().stream()
						.filter(me -> me.getValue().equals(parent)).findFirst();
				if (entry.isPresent()) {
					for (TemplateEntityDTO dtoTemp : entry.get().getKey().getSubentities()) {
						if (dtoTemp instanceof TemplateEntityCatCompDTO) {
							tempParent = dtoEntity.get(dtoTemp);
						}
					}
				}
				dtoEntity.putAll(this.createEntities(root, sourceName, contextType, Arrays.asList(catSensor),
						tempParent, dtoEntity, transaction));
				entityCatSensor = (CatalogSensor) dtoEntity.get(catSensor);
				catSensor.setId(entityCatSensor.getID());
				dtoEntity.putAll(this.createEntities(root, sourceName, contextType, catSensor.getSubentities(),
						entityCatSensor, dtoEntity, transaction));
			} else {
				if (!dtoEntity.containsKey(catSensor)) {
					if (catSensor.getParent() == null) {
						catSensor.setParent(findCatComp(Arrays.asList(dto)));
					}
					dtoEntity.putAll(
							this.updateEntities(sourceName, contextType, Arrays.asList(catSensor), dto, transaction));
				}
				entityCatSensor = (CatalogSensor) dtoEntity.get(catSensor);
				List<TemplateEntityDTO> createDTOs = new ArrayList<>();
				List<TemplateEntityDTO> updateDTOs = new ArrayList<>();
				for (TemplateEntityDTO tempDTO : catSensor.getSubentities()) {
					if (tempDTO.getId() == null || tempDTO.getId().isEmpty()) {
						createDTOs.add(tempDTO);
					} else {
						updateDTOs.add(tempDTO);
					}
					tempDTO.setParent(catSensor);
				}
				if (!createDTOs.isEmpty()) {
					dtoEntity.putAll(this.createEntities(root, sourceName, contextType, createDTOs, entityCatSensor,
							dtoEntity, transaction));
				}
				if (!updateDTOs.isEmpty()) {
					dtoEntity.putAll(this.updateEntities(sourceName, contextType, updateDTOs, null, transaction));
				}
			}
		}
		Optional<Value> quantityId = dto.getAttributes().stream().filter(v -> v.getName().equals("Quantity"))
				.findFirst();
		if (quantityId.isPresent()) {
			return ef.createTemplateSensor(dto.getName(), (TemplateComponent) parent, entityCatSensor,
					this.entityManager.load(Quantity.class, quantityId.get().extract(ValueType.STRING)));
		}
		return null;
	}

	private Seq<io.vavr.Value<String>> getTemplateEntityTplRootId(TemplateEntityDTO dto) {
		Seq<io.vavr.Value<String>> result = SL(dto.getParent().getId());
		if (dto.getParent() instanceof TemplateEntityTplRootDTO) {
			return result;
		} else {
			return getTemplateEntityTplRootId(dto.getParent()).append(result.get());
		}
	}

	private Seq<io.vavr.Value<String>> getTemplateEntityCatCompId(TemplateEntityDTO dto) {
		Seq<io.vavr.Value<String>> result = SL(dto.getParent().getId());
		if (dto.getParent() instanceof TemplateEntityCatCompDTO) {
			return result;
		} else {
			return getTemplateEntityCatCompId(dto.getParent()).append(result.get());
		}
	}

	private Map<TemplateEntityDTO, Entity> updateEntities(String sourceName, ContextType contextType,
			List<TemplateEntityDTO> entities, TemplateEntityDTO parentDTO, Transaction transaction) throws Exception {
		Map<TemplateEntityDTO, Entity> mapDTOs = new HashMap<>();
		Entity entity = null;
		for (TemplateEntityDTO dto : entities) {
			if (dto instanceof TemplateEntityTplRootDTO) {
				entity = this.entityManager.loadWithoutChildren(TemplateRoot.class, contextType, dto.getId());
			} else if (dto instanceof TemplateEntityTplCompDTO) {
				entity = this.entityManager.loadWithoutChildren(TemplateComponent.class, contextType, dto.getId());
			} else if (dto instanceof TemplateEntityTplAttrDTO) {
				entity = entityService.find(V(sourceName), TemplateAttribute.class, V(dto.getId()), V(contextType),
						getTemplateEntityTplRootId(dto), false).get();
			} else if (dto instanceof TemplateEntityCatCompDTO) {
				/*
				 * CatalogComponent is loaded with children otherwise
				 * org.eclipse.mdm.api.dflt.model.EntityFactory.createTemplateAttribute(String,
				 * TemplateComponent) will throw Exception
				 */
				entity = this.entityManager.load(CatalogComponent.class, contextType, dto.getId());
			} else if (dto instanceof TemplateEntityCatAttrDTO) {
				entity = entityService.find(V(sourceName), CatalogAttribute.class, V(dto.getId()), V(contextType),
						getTemplateEntityCatCompId(dto), false).get();
			} else if (dto instanceof TemplateEntityTplSensorDTO) {
				entity = this.entityManager.loadWithoutChildren(TemplateSensor.class, contextType, dto.getId());
			} else if (dto instanceof TemplateEntityTplSensorAttrDTO) {
				entity = entityService.find(V(sourceName), TemplateAttribute.class, V(dto.getId()), V(contextType),
						getTemplateEntityTplRootId(dto), false).get();
			} else if (dto instanceof TemplateEntityCatSensorDTO) {
				if (dto.getParent() == null) {
					dto.setParent(this.findCatComp(parentDTO.getSubentities()));
				}
				/*
				 * CatalogSensor is loaded with children otherwise
				 * org.eclipse.mdm.api.dflt.model.EntityFactory.createTemplateSensor(String,
				 * TemplateComponent, CatalogSensor, Quantity) will throw Exception
				 */
				entity = this.entityManager.load(CatalogSensor.class, contextType, dto.getId());
			} else if (dto instanceof TemplateEntityCatSensorAttrDTO) {
				if (dto.getParent() == null) {
					dto.setParent(this.findCatComp(parentDTO.getSubentities()));
				}
				entity = this.entityManager.loadWithoutChildren(CatalogAttribute.class, contextType, dto.getId());
			}
			if (entity != null) {
				Map<String, Object> werte = new HashMap<>();
				for (Value val : dto.getAttributes()) {
					if (!"DataType".equals(val.getName()) && !"Unit".equals(val.getName())) {
						werte.put(val.getName(), val.extract());
					}
				}
				io.vavr.collection.Map<String, Object> mapWerte = io.vavr.collection.HashMap.ofAll(werte);
				mapDTOs.put(dto, entityService.updateEntityValues(entity, mapWerte, V(sourceName)).get());
			}
		}
		if (!mapDTOs.isEmpty()) {
			// Upload Entities.
			transaction.update(mapDTOs.values());
		}
		return mapDTOs;
	}

	/**
	 * Helper function to load an {@link EntityManager}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @return the {@link EntityManager}
	 */
	public EntityManager getEntityManager(String sourceName) {
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		return context.getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present in '" + sourceName + "'."));
	}

	/**
	 * Helper function to load an {@link EntityFactory}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @return the {@link EntityFactory}
	 */
	private EntityFactory getEntityFactory(String sourceName) {
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		return context.getEntityFactory()
				.orElseThrow(() -> new MDMEntityAccessException("Entity factory not present in '" + sourceName + "'."));
	}
}
