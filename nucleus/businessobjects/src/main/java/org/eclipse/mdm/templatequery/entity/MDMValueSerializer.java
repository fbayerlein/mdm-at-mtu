/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.io.IOException;

import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.businessobjects.utils.Serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Implementation to serialize DTOs to JSON.
 *
 * @since 5.2.0
 * @author MAF, Peak Solution GmbH
 */
public class MDMValueSerializer extends JsonSerializer<Value> {

	private static final String NAME = "name";
	private static final String DATA_TYPE = "dataType";
	private static final String ENUMERATION_NAME = "enumerationName";
	private static final String UNIT = "unit";
	private static final String VALUE = "value";

	@Override
	public void serialize(Value value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeStartObject();
		gen.writeStringField(NAME, value.getName());
		gen.writeStringField(DATA_TYPE, value.getValueType().name());
		gen.writeStringField(UNIT, value.getUnit());
		gen.writeObjectField(VALUE, Serializer.serializeValue(value));
		if (value.getValueType().isEnumerationType()) {
			gen.writeObjectField(ENUMERATION_NAME, value.getEnumName());
		}
		gen.writeEndObject();
	}

}
