/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import org.eclipse.mdm.api.base.model.Entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * EntryResponse (Container for a collection of parent MDMEntity ids and children {@link MDMEntity}s)
 * 
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MDMEntityArrayResponse {

	/** type of all content entries (e.g. TestStep) */
	private final String type;
	/** transferable data content */
	private final Map<String, List<MDMEntity>> data;

	/**
	 * Constructor (for a list of business objects {@link MDMEntity}s)
	 *
	 * @param type    type of all containging {@link MDMEntity}s
	 * @param businessObjects map of parent ids with a list of children {@link MDMEntity}
	 */
	public <T extends Entity> MDMEntityArrayResponse(Class<? extends Entity> type, Map<String, List<T>> businessObjects) {
		this.type = type.getSimpleName();
		this.data = toTransferable(businessObjects);
	}

	public String getType() {
		return this.type;
	}

	public Map<String, List<MDMEntity>> getData() {
		return Collections.unmodifiableMap(this.data);
	}

	private <T extends Entity> Map<String, List<MDMEntity>> toTransferable(Map<String, List<T>> businessObjects) {
		Map<String, List<MDMEntity>> mdmEntityList = new HashMap<>();
		businessObjects.forEach((String key, List<T> values) -> {
			List<MDMEntity> valueList = new ArrayList<>();
			for (Entity businessObject : values) {
				MDMEntity mdmEntity = new MDMEntity(businessObject);
				valueList.add(mdmEntity);
			}
			mdmEntityList.put(key, valueList);
		});
		return mdmEntityList;
	}

}
