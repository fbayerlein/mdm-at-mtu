/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.MDMEnum;
import org.eclipse.mdm.businessobjects.entity.MDMEnumValue;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * @author jz
 *
 */
@Tag(name = "Enumeration")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/enums")
public class EnumResource {

	@EJB
	private EnumService enumService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Get all enums", description = "Returns all enums for a datasource", responses = {
			@ApiResponse(description = "list with enums", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid sourceName supplied") })
	public Response findEnum(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {
		return Response.ok().entity(convert(this.enumService.allEnums(sourceName))).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{ENUMNAME}")
	@Operation(summary = "Find an enum by name", description = "Returns enum based on name", responses = {
			@ApiResponse(description = "The enum", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid name supplied") })
	public Response findEnum(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "Name of the enum", required = true) @PathParam("ENUMNAME") String name) {
		return Response.ok().entity(convert(this.enumService.findEnum(sourceName, name))).build();
	}

	private List<MDMEnum> convert(Collection<Enumeration<?>> enumerations) {
		return enumerations.stream().map(this::convert).collect(Collectors.toList());
	}

	private MDMEnum convert(Enumeration<? extends EnumerationValue> e) {
		MDMEnum me = new MDMEnum(e.getName());
		me.setValues(e.getValues().values().stream().map(this::convert).collect(Collectors.toList()));
		return me;
	}

	private MDMEnumValue convert(EnumerationValue ev) {
		return new MDMEnumValue(ev.name(), ev.ordinal());
	}
}
