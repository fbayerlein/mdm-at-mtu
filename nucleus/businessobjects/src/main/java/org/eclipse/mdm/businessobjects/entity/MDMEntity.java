/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.model.CatalogAttribute;
import org.eclipse.mdm.businessobjects.utils.Serializer;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import com.google.common.base.MoreObjects;

import io.swagger.v3.oas.annotations.media.Schema;
import io.vavr.collection.HashMap;

/**
 * MDMEntity (Entity for a business object (contains a list of
 * {@link MDMAttribute}s)
 *
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 */
@Schema(description = "Representation of a MDM entity")
public class MDMEntity {

	/**
	 * name of the MDM business object
	 */
	private String name;
	/**
	 * id of the MDM business object
	 */
	private String id;
	/**
	 * type as String of the MDM business object (e.g. TestStep)
	 */
	private String type;
	/**
	 * source type name of the business object at the data source
	 */
	private String sourceType;
	/**
	 * source name (e.g. MDM Environment name)
	 */
	private String sourceName;
	/**
	 * list of attribute to transfer
	 */
	private List<MDMAttribute> attributes;

	/**
	 * list of relations to transfer
	 */
	private List<MDMRelation> relations;

	public MDMEntity() {

	}

	/**
	 * Constructor.
	 *
	 * @param entity the business object
	 */
	public MDMEntity(Entity entity) {
		this.name = entity.getName();
		this.id = entity.getID();
		this.type = entity.getClass().getSimpleName();
		this.sourceType = entity.getTypeName();
		this.sourceName = entity.getSourceName();
		this.attributes = convertAttributeValues(entity.getValues());
		if (entity instanceof CatalogAttribute) {
			CatalogAttribute ca = (CatalogAttribute) entity;
			this.attributes.add(new MDMAttribute("DataType", ca.getValueType().name(), "", ValueType.ENUMERATION.name(),
					ca.getValueType().getOwner().getName()));
			this.attributes.add(new MDMAttribute("Unit", (ca.getUnit().isPresent() ? ca.getUnit().get().getID() : ""),
					"", ValueType.STRING.name()));
			if (ca.getValueType().isEnumerationType()) {
				this.attributes.add(new MDMAttribute("EnumerationName", ca.getEnumerationObject().getName(), "",
						ValueType.ENUMERATION.name()));
			}
		}
		this.relations = convertRelations(entity);
	}

	/**
	 * Constructor.
	 *
	 * @param name       Name of the Entity
	 * @param id         ID of the Entity
	 * @param type       Type of the Entity
	 * @param sourceType Source type of the Entity
	 * @param sourceName Source name of the Entity
	 * @param attributes Attributes of the Entity
	 */
	public MDMEntity(String name, String id, String type, String sourceType, String sourceName,
			List<MDMAttribute> attributes) {
		this(name, id, type, sourceType, sourceName, attributes, Collections.emptyList());
	}

	/**
	 * Constructor.
	 *
	 * @param name       Name of the Entity
	 * @param id         ID of the Entity
	 * @param type       Type of the Entity
	 * @param sourceType Source type of the Entity
	 * @param sourceName Source name of the Entity
	 * @param attributes Attributes of the Entity
	 * @param relations  Relations of the Entity
	 */
	public MDMEntity(String name, String id, String type, String sourceType, String sourceName,
			List<MDMAttribute> attributes, List<? extends MDMRelation> relations) {
		this.name = name;
		this.id = id;
		this.type = type;
		this.sourceType = sourceType;
		this.sourceName = sourceName;
		if (attributes != null) {
			this.attributes = new ArrayList<>(attributes);
		} else {
			this.attributes = new ArrayList<>();
		}
		if (relations != null) {
			this.relations = new ArrayList<>(relations);
		} else {
			this.relations = new ArrayList<>();
		}
	}

	public String getName() {
		return this.name;
	}

	public String getId() {
		return this.id;
	}

	public String getType() {
		return this.type;
	}

	public String getSourceType() {
		return this.sourceType;
	}

	public String getSourceName() {
		return this.sourceName;
	}

	public List<? extends MDMAttribute> getAttributes() {
		return Collections.unmodifiableList(this.attributes);
	}

	public List<? extends MDMRelation> getRelations() {
		return Collections.unmodifiableList(this.relations);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("name", name).add("id", id).add("type", type)
				.add("sourceType", sourceType).add("sourceName", sourceName).add("attributes", attributes)
				.add("relations", relations).toString();
	}

	/**
	 * converts the MDM business object values to string values
	 *
	 * @param values values of a MDM business object
	 * @return list with converted attribute values
	 */
	protected List<MDMAttribute> convertAttributeValues(Map<String, Value> values) {
		List<MDMAttribute> listAttrs = new ArrayList<>();
		Set<java.util.Map.Entry<String, Value>> set = values.entrySet();

		for (java.util.Map.Entry<String, Value> entry : set) {

			if (entry.getKey().equals(BaseEntity.ATTR_ID)) {
				continue;
			}

			if (!entry.getValue().isValid()) {
				String dt = entry.getValue().getValueType().toString();
				listAttrs.add(new MDMAttribute(entry.getKey(), "", "", dt, entry.getValue().getEnumName()));
				continue;
			}

			if (entry.getValue().getValueType().isSequence()) {
				listAttrs.add(singleType2Attribute(entry.getKey(), entry.getValue()));
			} else {
				listAttrs.add(singleType2Attribute(entry.getKey(), entry.getValue()));
			}
		}

		return listAttrs;
	}

	/**
	 * converts a single type MDM business object value to a attribute
	 *
	 * @param name        name of the attribute value
	 * @param singleValue single MDM business object value
	 * @return the converted attribute value
	 */
	protected MDMAttribute singleType2Attribute(String name, Value singleValue) {
		Object value = Serializer.serializeValue(singleValue);
		String unit = singleValue.getUnit();
		String dt = singleValue.getValueType().toString();

		return new MDMAttribute(name, value, unit, dt, singleValue.getEnumName());
	}

	/**
	 * converts a sequence type MDM business object value to a attribute
	 *
	 * @param name          name of the attribute value
	 * @param sequenceValue sequence MDM business object value
	 * @return the converted attribute value
	 */
	protected MDMAttribute sequenceType2Attribute(String name, Value sequenceValue) {

		String dt = sequenceValue.getValueType().toString();
		String unit = sequenceValue.getUnit();
		Object value = Serializer.serializeValue(sequenceValue);

		return new MDMAttribute(name, value, unit, dt, sequenceValue.getEnumName());
	}

	/**
	 * converts a string sequence MDM business object value to a attribute The
	 * result is a separated string (separator: ';')
	 *
	 * @param name  name of the attribute value
	 * @param value string sequence MDM business object value
	 * @return the converted attribute value
	 */
	protected MDMAttribute stringSeq2Attribute(String name, Value value) {
		int[] stringSeq = value.extract();
		String unit = value.getUnit();
		String dt = value.getValueType().toString();
		return new MDMAttribute(name, stringSeq, unit, dt);
	}

	/**
	 * Converts all relations to MDMRelations. The potential ContextType of the
	 * children is assumed to be the parent's one.
	 *
	 * @param entity to get
	 *               {@link org.eclipse.mdm.businessobjects.entity.MDMRelation}s for
	 * @return a list of {@link org.eclipse.mdm.businessobjects.entity.MDMRelation}s
	 */
	protected List<MDMRelation> convertRelations(Entity entity) {
		// write out children
		Core core = ServiceUtils.getCore(entity);
		// get children hash (type is key)
		return HashMap.ofAll(core.getChildrenStore().getCurrent())
				// if we don't check the size we crash, some elements may not have a relation
				.filter(entry -> entry._2.size() > 0)
				// create child relations (the ContextType is assumed to be same as the parent's
				// one
				.filterValues(l -> l.size() > 0)
				.map(entry -> new MDMRelation(null, MDMRelation.RelationType.CHILDREN, entry._1.getSimpleName(),
						ServiceUtils.getContextType(entry._2.get(0)),
						// call get id on all related entities
						io.vavr.collection.List.ofAll(entry._2).map(Deletable::getID).asJava()))
				// append related entities from the mutable store
				.appendAll(io.vavr.collection.List.ofAll(core.getMutableStore().getCurrentMap().entrySet()).
				// convert list to map: use simple class name as this is the MDM entity type in
				// conjunction with the ContextType
						groupBy(e -> e.getValue().getClass().getSimpleName() + "_"
								+ ServiceUtils.getContextType(e.getValue()))
						// create relation for every entry
						.map(entry -> new MDMRelation(null, MDMRelation.RelationType.MUTABLE,
								entry._2.get(0).getValue().getClass().getSimpleName(),
								ServiceUtils.getContextType(entry._2.get(0).getValue()),
								entry._2.map(Map.Entry::getValue).map(Entity::getID).asJava())))
				.appendAll(io.vavr.collection.List.ofAll(core.getPermanentStore().getCurrentMap().entrySet())
						.groupBy(e -> e.getKey())
						// create relation for every entry
						.map(entry -> new MDMRelation(null, MDMRelation.RelationType.PARENT, entry._1,
								ServiceUtils.getContextType(entry._2.get(0).getValue()),
								entry._2.map(Map.Entry::getValue).map(Entity::getID).asJava())))
				.asJava();
	}

}
