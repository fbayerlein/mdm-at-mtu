/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.control;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.nodeprovider.entity.NodeProvider;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderRoot;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderUIWrapper;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.preferences.controller.PreferenceService;
import org.eclipse.mdm.preferences.entity.PreferenceMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository for NodeProviders in a specific session.
 *
 */
@SessionScoped
public class NodeProviderRepository implements Serializable {

	private static final long serialVersionUID = -3081666937067348673L;

	private static final Logger LOG = LoggerFactory.getLogger(NodeProviderRepository.class);

	private static final String DEFAULT_NODEPROVIDER = "default";

	private Map<String, NodeProvider> nodeProviders = new HashMap<>();

	private String defaultNodeProvider;

	@Inject
	private ConnectorService connectorService;

	@Inject
	private PreferenceService preferenceService;

	@Inject
	Principal principal;

	/**
	 * Default constructor
	 */
	public NodeProviderRepository() {
	}

	/**
	 * Constructor for unit tests.
	 * 
	 * @param connectorService
	 * @param preferenceService
	 */
	public NodeProviderRepository(ConnectorService connectorService, PreferenceService preferenceService,
			Principal principal) {
		this.connectorService = connectorService;
		this.preferenceService = preferenceService;
		this.principal = principal;
		init();
	}

	List<String> loadedSourceNames;

	/**
	 * Initializes the NodeProviderRepository
	 */
	@PostConstruct
	public void init() {
		LOG.debug("Initializing NodeProviderRepository for user ", principal.getName());

		loadedSourceNames = connectorService.getContexts().stream().map(c -> c.getSourceName())
				.collect(Collectors.toList());
		MDMExpressionLanguageService expressionLanguageService = new MDMExpressionLanguageService();
		List<PreferenceMessage> msgs = preferenceService.getPreferences("system", "nodeprovider.", null);

		PreferenceMessage defaultActive = msgs.stream()
				.filter(pm -> "nodeprovider.config.default_is_active".equals(pm.getKey())).findAny().orElse(null);
		PreferenceMessage userDefaultNP = msgs.stream()
				.filter(pm -> "nodeprovider.config.use_as_default".equals(pm.getKey())).findAny().orElse(null);

		// Activate the default nodeprovider
		if (defaultActive == null || Boolean.parseBoolean(defaultActive.getValue())) {
			DefaultNodeProvider defaultNP = new DefaultNodeProvider(connectorService);
			putNodeProvider(DEFAULT_NODEPROVIDER, defaultNP);
			LOG.trace("Registered default nodeprovider.");
			defaultNodeProvider = DEFAULT_NODEPROVIDER;
		}

		// Use a custom default nodeprovider instead of the internal default one
		if (userDefaultNP != null && !userDefaultNP.getValue().isEmpty()) {
			defaultNodeProvider = userDefaultNP.getValue();
		}

		for (PreferenceMessage msg : msgs) {
			registerNodeProvider(msg, expressionLanguageService);
		}
	}

	private void checkForReload() {

		if (!loadedSourceNames.containsAll(
				connectorService.getContexts().stream().map(c -> c.getSourceName()).collect(Collectors.toList()))) {
			LOG.trace("Reloading nodeprovider.");
			init();
		}
	}

	/**
	 * Register a nodeprovider with its configuration
	 *
	 * @param msg                       The nodeprovider configuration
	 * @param expressionLanguageService the MDM language expression service
	 */
	private void registerNodeProvider(PreferenceMessage msg, MDMExpressionLanguageService expressionLanguageService) {
		if (msg != null && !msg.getKey().startsWith("nodeprovider.config.")) {
			try {
				NodeProviderRoot root = parsePreference(msg.getValue());
				putNodeProvider(root.getId(),
						new GenericNodeProvider(connectorService, root, expressionLanguageService));
				LOG.trace("Registered generic nodeprovider '{}'.", root.getId());
			} catch (RuntimeException e) {
				e.printStackTrace();
				LOG.warn("Could not deserialize nodeprovider configuration: {}", msg.getValue(), e);
			}
		}
	}

	/**
	 * Add a nodeprovider to the repository.
	 * 
	 * @param id           ID of the nodeprovider
	 * @param nodeProvider {@link NodeProvider}
	 */
	private void putNodeProvider(String id, NodeProvider nodeProvider) {
		nodeProviders.put(id, nodeProvider);
	}

	/**
	 * @return a map of all registered nodeproviders indexed by ID
	 */
	public Map<String, NodeProvider> getNodeProviders() {
		checkForReload();
		return nodeProviders;
	}

	/**
	 * Parse a JSON string representing a {@link NodeProviderRoot}
	 * 
	 * @param json JSON string
	 * @return parsed {@link NodeProviderRoot}
	 */
	private NodeProviderRoot parsePreference(String json) {
		return SerializationUtil.deserializeNodeProviderRoot(connectorService, json);
	}

	/**
	 * Get a sorted list of nodeprovider ids with the default nodeprovider on the
	 * first index
	 *
	 * @return list of nodeprovider ids
	 */
	public List<String> getNodeProviderIDs() {
		checkForReload();
		List<String> list = new ArrayList<>(nodeProviders.keySet());

		// change the default nodeprovider from the generic to the standard if
		// - the generic does not exist
		// - the standard default is activated
		if (!list.contains(defaultNodeProvider) && !DEFAULT_NODEPROVIDER.equals(defaultNodeProvider)
				&& list.contains(DEFAULT_NODEPROVIDER)) {
			defaultNodeProvider = DEFAULT_NODEPROVIDER;
		}

		// only re-sort the default nodeprovider if it exists in the list
		if (list.contains(defaultNodeProvider)) {
			list.remove(defaultNodeProvider);
			Collections.sort(list);
			list.add(0, defaultNodeProvider);
		}
		return list;
	}

	/**
	 * Get a sorted list of nodeprovider ids with the default nodeprovider on the
	 * first index
	 *
	 * @return list of nodeprovider ids
	 */
	public List<NodeProviderUIWrapper> getNodeProvidersSorted() {
		checkForReload();
		List<NodeProviderUIWrapper> list = new ArrayList<>();
		List<String> keySet = getNodeProviderIDs();
		for (int i = 0; i < keySet.size(); i++) {
			list.add(new NodeProviderUIWrapper(i, keySet.get(i), nodeProviders.get(keySet.get(i)).getName()));
		}
		return list;
	}
}
